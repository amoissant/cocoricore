﻿namespace CocoriCore.Authentication.Http
{
    public enum LocationsFlags
    {
        AuthorizationHeader = 1 << 1,
        Cookie = 1 << 2
    }
}
