﻿using Castle.Core.Internal;
using CocoriCore.Common;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace CocoriCore.Authentication.Http
{
    /// <summary>
    /// Define a list of trusted ips then verify <see cref="HttpRequest"/>'s ip belong this list.
    /// Support configuration behind reverse proxy.
    /// <para>See <see cref="IpFilterOptions"/> for all configurations options.</para>
    /// </summary>
    /// <remarks>Register this type in singleton scope.</remarks>
    public class IpFilter
    {
        private readonly IpFilterOptions _options;
        private readonly IEnumerable<Regex> _ipRegexs;
        private readonly Regex _pathRegex;

        public IpFilter(IpFilterOptions options)
        {
            ValidateOptions(options);
            _options = options;
            _pathRegex = BuildPathRegex(options.Path);
            _ipRegexs = BuildAllowedIpsRegex(options);
        }

        private void ValidateOptions(IpFilterOptions options)
        {
            if (options == null)
            {
                throw new ArgumentNullException(nameof(options));
            }
            options.AllowedIps = options.AllowedIps ?? new string[0];
            new IpFilterOptionsValidator().ValidateAndThrow(options);            
        }

        private Regex BuildPathRegex(string path)
        {
            if (path == null) return null;

            var pattern = path.Replace(".", "\\.").Replace("*", ".*");
            return new Regex(pattern, RegexOptions.IgnoreCase | RegexOptions.Compiled);
        }

        private IEnumerable<Regex> BuildAllowedIpsRegex(IpFilterOptions options)
        {
            var regex = options
                .AllowedIps?
                .Select(x => x.Trim())
                .Where(x => !x.IsNullOrEmpty())
                .Select(x => BuildIpRegex(x))
                .ToArray();
            return regex ?? new Regex[0];
        }

        private Regex BuildIpRegex(string ip)
        {
            var pattern = $"^{ip.Replace(".", "\\.").Replace("*", ".*")}$";
            return new Regex(pattern, RegexOptions.IgnoreCase | RegexOptions.Compiled);
        }

        /// <summary>
        /// When request match <see cref="IpFilterOptions.Path"/> then check request ip belong <see cref="IpFilterOptions.AllowedIps"/> 
        /// else throws <see cref="ForbiddenAccessException"/>.
        /// </summary>
        /// <param name="context">The http context that contains the request to check.</param>
        //TODO: pas sûr que ce soit utile et ça ajoute de la complexité avec le Path optionnel dans les options
        public void CheckIpWhenMatchOptionPath(HttpContext context)
        {
            if (_pathRegex?.IsMatch(context.Request.Path) == true)
            {
                CheckIpIsAllowed(context);
            }
        }

        /// <summary>
        /// Check request ip belong <see cref="IpFilterOptions.AllowedIps"/>.
        /// </summary>
        /// <param name="context">The http context that contains the request to check.</param>
        public void CheckIpIsAllowed(HttpContext context)
        {
            var ip = context.Connection.RemoteIpAddress?.ToString() ?? "(null)";
            var reverseProxyIp = _options.ReverseProxyIp;

            if (_options.ReverseProxyHeader != null && reverseProxyIp == ip)
            {
                var forwardHeader = _options.ReverseProxyHeader;
                var forwardedIp = context.Request.Headers[forwardHeader].ToString() ?? "(null)";
                if (!IpIsAllowed(forwardedIp))
                {
                    var exception = new ForbiddenAccessException("Unauthorized forwarded ip.");
                    exception.Data["RequestIp"] = ip;
                    exception.Data["ForwardedIp"] = forwardedIp;
                    throw exception;
                }
            }
            else
            {
                if (!IpIsAllowed(ip))
                {
                    var exception = new ForbiddenAccessException("Unauthorized ip.");
                    exception.Data["RequestIp"] = ip;
                    throw exception;
                }
            }
        }

        private bool IpIsAllowed(string ip)
        {
            return _ipRegexs.Any(x => x.IsMatch(ip));
        }
    }
}
