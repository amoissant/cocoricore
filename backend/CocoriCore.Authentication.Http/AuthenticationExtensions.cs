﻿using CocoriCore.AspNetCore;
using CocoriCore.Common;
using Microsoft.AspNetCore.Http;

namespace CocoriCore.Authentication.Http
{
    public static class AuthenticationExtensions
    {
        private static string __HTTP_CONTEXT = nameof(__HTTP_CONTEXT);

        /// <summary>
        /// Add an instance of <see cref="HttpContext"/> into current <see cref="AuthenticationContext"/> in order to get it later.
        /// </summary>
        /// <param name="context">Currrent authentication context</param>
        /// <param name="httpContext">Http context to add</param>
        /// <returns>Currrent authentication context</returns>
        public static AuthenticationContext SetHttpContext(this AuthenticationContext context, HttpContext httpContext)
        {
            context[__HTTP_CONTEXT] = httpContext;
            return context;
        }

        /// <summary>
        /// Get <see cref="HttpRequest"/> from current authentication context.
        /// </summary>
        /// <remarks>You nee to set <see cref="HttpContext"/> first using <see cref="SetHttpContext(AuthenticationContext, HttpContext)"/></remarks>
        /// <param name="context">Currrent authentication context</param>
        public static HttpRequest GetHttpRequest(this AuthenticationContext context)
        {
            return context.GetHttpContext().Request;
        }

        /// <summary>
        /// Get <see cref="HttpContext"/> from current authentication context.
        /// </summary>
        /// <remarks>You nee to set <see cref="HttpContext"/> first using <see cref="SetHttpContext(AuthenticationContext, HttpContext)"/></remarks>
        /// <param name="context">Currrent authentication context</param>
        public static HttpContext GetHttpContext(this AuthenticationContext context)
        {
            if (!context.TryGetValue(__HTTP_CONTEXT, out var httpContext))
            {
                throw new ConfigurationException($"No http context present in {nameof(AuthenticationContext)}.\n" +
                    $"Configure {nameof(AuthenticationOptionsBuilder)} to add {nameof(HttpContext)} into {nameof(AuthenticationContext)}:\n" +
                    $"builder.{nameof(AuthenticationOptionsBuilder.BeforeAuthenticate)}<{nameof(IHttpContextProvider)}>(" +
                    $"(x, c) => c.{nameof(SetHttpContext)}(x.{nameof(IHttpContextProvider.HttpContext)}));");
            }
            return (HttpContext)context[__HTTP_CONTEXT];
        }

        /// <summary>
        /// Get whether path of current authentication context's <see cref="HttpRequest"/> starts with <paramref name="path"/>.
        /// </summary>
        /// <remarks>You nee to set <see cref="HttpContext"/> first using <see cref="SetHttpContext(AuthenticationContext, HttpContext)"/></remarks>
        /// <param name="context">Currrent authentication context</param>
        /// <param name="path">The path to compare</param>
        public static bool HttpRequestStartsWith(this AuthenticationContext context, string path)
        {
            return context.GetHttpRequest().Path.StartsWithSegments(path);
        }
    }
}
