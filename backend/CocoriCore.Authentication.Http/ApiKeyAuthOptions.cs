﻿using FluentValidation;

namespace CocoriCore.Authentication.Http
{
    /// <summary>
    /// Options class used for <see cref="ApiKeyAuthenticator"/>.
    /// </summary>
    public class ApiKeyAuthOptions
    {
        /// <summary>
        /// Get or set pattern used to match http request's path. Not mandatory, can contain wilcard.
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// Get or set hash used to verify api key from http request.
        /// </summary>
        public string ApiKeyHash { get; set; }
        /// <summary>
        /// Get or set location of http request where to find api key.
        /// </summary>
        public ApiKeyLocation LocationType { get; set; }
        /// <summary>
        /// Get or set the header name when <see cref="LocationType"/> is <see cref="ApiKeyLocation.Header"/> 
        /// or the name of the query parameter when <see cref="LocationType"/> is <see cref="ApiKeyLocation.QueryString"/> 
        /// </summary>
        public string LocationName { get; set; }

        public enum ApiKeyLocation
        {
            Header,
            QueryString
        }
    }

    public class ApiKeyOptionsValidator : AbstractValidator<ApiKeyAuthOptions>
    {
        public ApiKeyOptionsValidator()
        {
            RuleFor(x => x.ApiKeyHash).NotNull();
            RuleFor(x => x.LocationName).NotEmpty();
        }
    }
}
