﻿using CocoriCore.Common;
using FluentValidation;

namespace CocoriCore.Authentication.Http
{
    /// <summary>
    /// Options class used for <see cref="IpFilter"/>.
    /// </summary>
    public class IpFilterOptions
    {

        /// <summary>
        /// Get or set pattern used to match http request's path. Not mandatory, can contain wilcard.
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// Get or set allowed ips, null is like an empty array.
        /// </summary>
        public string[] AllowedIps { get; set; }
        /// <summary>
        /// Get or set the name of the header containing forwarded ip when behind a reverse proxy
        /// </summary>
        public string ReverseProxyHeader { get; set; }
        /// <summary>
        /// Get or set the ip of the reverse proxy when behind a reverse proxy
        /// </summary>
        public string ReverseProxyIp { get; set; }
    }

    public class IpFilterOptionsValidator : AbstractValidator<IpFilterOptions>
    {
        public IpFilterOptionsValidator()
        {
            RuleFor(x => x.AllowedIps).NotNull();
            RuleFor(x => x.ReverseProxyHeader).NotEmpty().When(x => x.ReverseProxyIp.IsNotNullOrEmpty());
            RuleFor(x => x.ReverseProxyIp).NotEmpty().When(x => x.ReverseProxyHeader.IsNotNullOrEmpty());
        }
    }
}
