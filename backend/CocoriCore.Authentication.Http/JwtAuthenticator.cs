﻿using CocoriCore.Common;
using CocoriCore.Jwt;
using Microsoft.AspNetCore.Http;
using System;
using System.Linq;

namespace CocoriCore.Authentication.Http
{
    /// <summary>
    /// Authenticate a <see cref="HttpRequest"/> searching for a valid JWT in Bearer header then retrieve claims object.
    /// </summary>
    /// <remarks>Register this type in the scope of an unit of work.</remarks>
    public class JwtAuthenticator
    {
        private readonly ITokenService _tokenService;
        private readonly string _cookiePrefix;
        private string _validJWT;

        public JwtAuthenticator(ITokenService tokenService, string cookiePrefix = "jwt")
        {
            _tokenService = tokenService;
            _cookiePrefix = cookiePrefix;
        }

        /// <summary>
        /// Authenticate searching for a valid JWT in <see cref="HttpRequest"/>.
        /// Throws <see cref="AuthenticationException"/> if no token found in http request or 
        /// <see cref="InvalidTokenException"/> when token is present but invalid.
        /// </summary>
        /// <param name="httpRequest">http request where to search for JWT</param>
        /// <param name="locations">http request locations where to search for JWT.</param>
        /// <exception cref="InvalidTokenException"></exception>
        /// <exception cref="AuthenticationException"></exception>
        public void Authenticate(HttpRequest httpRequest, LocationsFlags locations = LocationsFlags.AuthorizationHeader)
        {
            if(!TryAuthenticate(httpRequest, locations))
            {
                throw new AuthenticationException("JWT authentication failed.");
            }
        }

        /// <summary>
        /// Try authenticate searching for a valid JWT in <see cref="HttpRequest"/>.
        /// Throws <see cref="InvalidTokenException"/> when token is present but invalid.
        /// </summary>
        /// <returns><c>true</c> if a valid JWT was found in <see cref="HttpRequest"/>, else return <c>false</c></returns>
        /// <param name="httpRequest">http request where to search for JWT</param>
        /// <param name="locations">http request locations where to search for JWT.</param>
        /// <exception cref="InvalidTokenException"></exception>
        /// <exception cref="AuthenticationException"></exception>
        public bool TryAuthenticate(HttpRequest httpRequest, LocationsFlags locations = LocationsFlags.AuthorizationHeader)
        {
            if (!IsAuthenticated && locations.HasFlag(LocationsFlags.AuthorizationHeader))
            {
                TryAuthenticateUsingAuthorizationHeader(httpRequest);
            }
            if (!IsAuthenticated && locations.HasFlag(LocationsFlags.Cookie))
            {
                TryAuthenticateUsingCookie(httpRequest);
            }
            return IsAuthenticated;
        }

        private bool TryAuthenticateUsingAuthorizationHeader(HttpRequest httpRequest)
        {
            if (httpRequest != null && TryGetJwtFromAuthrorizationHeader(httpRequest, out var jwt))
            {
                _validJWT = ValidateJWT(jwt);
                return true;
            }
            return false;
        }

        private bool TryGetJwtFromAuthrorizationHeader(HttpRequest httpRequest, out string jwt)
        {
            jwt = null;
            var headerPrefix = "Bearer ";
            var authorization = httpRequest.Headers["Authorization"].FirstOrDefault();
            if (authorization != null && authorization.StartsWith(headerPrefix, StringComparison.InvariantCultureIgnoreCase))
            {
                jwt = authorization.Substring(headerPrefix.Length);
            }
            return jwt != null;
        }

        private bool TryAuthenticateUsingCookie(HttpRequest httpRequest)
        {
            if (httpRequest != null && TryGetJwtFromCookies(httpRequest, out var jwt))
            {
                _validJWT = ValidateJWT(jwt);
                return true;
            }
            return false;
        }

        public bool TryGetJwtFromCookies(HttpRequest httpRequest, out string jwt)
        {
            jwt = null;
            foreach (var cookie in httpRequest.Cookies)
            {
                if (cookie.Key.StartsWith(_cookiePrefix))
                {
                    jwt = cookie.Value;
                    break;
                }
            }
            return jwt != null;
        }

        public string ValidateJWT(string jwt)
        {
            _tokenService.CheckAuthenticityAndExpiration(jwt);
            return jwt;
        }

        /// <summary>
        /// Get whether authenticated or not.
        /// </summary>
        public bool IsAuthenticated => _validJWT != null;

        /// <summary>
        /// The valid JWT found in <see cref="HttpRequest"/>
        /// </summary>
        public string JWT => _validJWT;

        /// <summary>
        /// Get claims for current <see cref="HttpContext"/> or throw <see cref="InvalidOperationException"/> when no claims are present.
        /// </summary>
        /// <param name="claimsType">Type of the claims</param>
        /// <returns>Claims associated with <see cref="HttpContext"/></returns>
        /// <exception cref="InvalidOperationException"></exception>
        public object GetClaims(Type claimsType)
        {
            if (_validJWT == null)
            {
                throw new InvalidOperationException($"Must be authenticated. Ensure you called an authentication method before and that it succeed.");
            }
            return _tokenService.GetPayload(_validJWT, claimsType);
        }

        public void AddCookiesForPaths(HttpResponse httpResponse, string accessToken, TimeSpan duration, params string[] paths)
        {
            foreach (var path in paths)
            {
                var suffix = string.Join("-", path.Split('/').Where(x => !string.IsNullOrWhiteSpace(x)));
                httpResponse.Cookies.Append($"{_cookiePrefix}-{suffix}", accessToken, new CookieOptions()
                {
                    HttpOnly = true,
                    Secure = true,
                    Path = path,
                    SameSite = SameSiteMode.Strict,
                    MaxAge = duration
                });
            }
        }
    }
}
