﻿using CocoriCore.Common;
using CocoriCore.Security;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using System;
using System.Text.RegularExpressions;
using static CocoriCore.Authentication.Http.ApiKeyAuthOptions;

namespace CocoriCore.Authentication.Http
{
    /// <summary>
    /// <para>Verify <see cref="HttpRequest"/> contains a valid api key (int header or queryString).</para>
    /// <para>See <see cref="ApiKeyAuthOptions"/> for all configurations options.</para>
    /// </summary>
    /// <remarks>Register this type in singleton scope.</remarks>
    public class ApiKeyAuthenticator
    {
        private readonly IHashService _hashService;
        private readonly ApiKeyAuthOptions _options;
        private readonly Regex _pathRegex;

        public ApiKeyAuthenticator(IHashService hashService, ApiKeyAuthOptions options)
        {
            _hashService = hashService;
            _options = options;
            ValidateOptions(options);
            _pathRegex = BuildPathRegex(options.Path);
        }

        private void ValidateOptions(ApiKeyAuthOptions options)
        {
            if(options == null)
            {
                throw new ArgumentNullException(nameof(options));
            }
            new ApiKeyOptionsValidator().ValidateAndThrow(options);
        }

        private Regex BuildPathRegex(string path)//TODO factoriser avec IpFiltering et cors
        {
            if (path == null) return null;

            var pattern = path.Replace(".", "\\.").Replace("*", ".*");
            return new Regex(pattern, RegexOptions.IgnoreCase | RegexOptions.Compiled);
        }

        /// <summary>
        /// When request match <see cref="ApiKeyAuthOptions.Path"/> then check request contains an api key
        /// conform to <see cref="ApiKeyAuthOptions.ApiKeyHash"/> else throws <see cref="AuthenticationException"/>.
        /// </summary>
        /// <param name="httpRequest">The http context that contains the request to check.</param>
        //TODO: pas sûr que ce soit utile et ça ajoute de la complexité avec le Path optionnel dans les options
        public void CheckApiKeyWhenMatchOptionPath(HttpRequest httpRequest)
        {
            if (_pathRegex?.IsMatch(httpRequest.Path) == true)
            {
                CheckApiKey(httpRequest);
            }
        }

        /// <summary>
        /// Check request contains an api key conform to <see cref="ApiKeyAuthOptions.ApiKeyHash"/>.
        /// </summary>
        /// <param name="httpRequest">The http request to check.</param>
        public void CheckApiKey(HttpRequest httpRequest)
        {
            var requestApiKey = GetRequestApiKeyOrDefault(httpRequest);
            if (string.IsNullOrWhiteSpace(requestApiKey))
            {
                throw new AuthenticationException("Request api key is null or empty.");
            }
            if (!_hashService.ValueMatchHash(requestApiKey, _options.ApiKeyHash))
            {
                throw new AuthenticationException("Request api key doesn't match correct hash.");
            }
        }

        /// <summary>
        /// Try authenticate checking api key from <see cref="HttpRequest"/>'s header or query 
        /// according to <see cref="ApiKeyAuthOptions.LocationType"/>
        /// </summary>
        /// <param name="httpRequest">http request where to search for api key</param>
        /// <returns>
        /// <c>true</c> when api key match <see cref="ApiKeyAuthOptions.ApiKeyHash"/>, <c>false</c> when no api key is present in <see cref="HttpRequest"/> 
        /// or throw <see cref="AuthenticationException"/> when api key is present but invalid.
        /// </returns>
        /// <exception cref="AuthenticationException"></exception>
        public bool TryAuthenticateUsingApiKey(HttpRequest httpRequest)
        {
            var requestApiKey = GetRequestApiKeyOrDefault(httpRequest);
            if (requestApiKey != null)
            {
                if(!_hashService.ValueMatchHash(requestApiKey, _options.ApiKeyHash))
                {
                    throw new AuthenticationException("Request api key doesn't match correct hash.");
                }
                return true;
            }
            return false;
        }

        private string GetRequestApiKeyOrDefault(HttpRequest httpRequest)
        {
            if(_options.LocationType == ApiKeyLocation.Header)
            {
                return httpRequest.Headers[_options.LocationName];
            }
            else if (_options.LocationType == ApiKeyLocation.QueryString)
            {
                return httpRequest.Query[_options.LocationName];
            }
            else
            {
                throw new NotImplementedException($"{_options.LocationType}");
            }
        }
    }
}
