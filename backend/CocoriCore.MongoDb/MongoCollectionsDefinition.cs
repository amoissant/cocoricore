using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using CocoriCore.Common;
using CocoriCore.Types.Extension;
using MongoDB.Driver;

namespace CocoriCore.MongoDb
{
    public class MongoCollectionsDefinition
    {
        private HashSet<string> _collectionNames;

        public MongoCollectionsDefinition()
        {
            _collectionNames = new HashSet<string>();
        }

        public MongoCollectionsDefinition DefineCollection(string collectionName)
        {
            _collectionNames.Add(collectionName);
            return this;
        }

        public MongoCollectionsDefinition DefineCollection(Type entityType)
        {
            _collectionNames.Add(entityType.Name);
            return this;
        }

        public MongoCollectionsDefinition DefineCollections<TEntity>(Assembly assembly, params Assembly[] assemblies)
        {
            assemblies = assemblies.Append(assembly).ToArray();
            var entityNames = assemblies
                .SelectMany(x => x.GetTypes())
                .Where(t => typeof(TEntity).IsAssignableFrom(t) && t.IsConcrete())
                .Select(x => x.Name);
            foreach (var name in entityNames)
            {
                if (!_collectionNames.Add(name))
                {
                    throw new ConfigurationException($"Duplicate collection name : {name}, ensure all entities have different names.");
                }
            }
            return this;
        }

        public async Task EnsureAllCollectionsExistAsync(IMongoDatabase database)
        {
            var cursor = await database.ListCollectionNamesAsync();
            var existingCollectioNames = await cursor.ToListAsync();
            var collectionToCreate = _collectionNames.Except(existingCollectioNames);
            foreach (var name in collectionToCreate)
            {
                await database.CreateCollectionAsync(name);
            }
        }
    }
}
