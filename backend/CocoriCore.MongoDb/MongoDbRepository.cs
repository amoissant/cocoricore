﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using CocoriCore.Common;
using CocoriCore.Expressions.Extension;
using CocoriCore.Reflection.Extension;
using CocoriCore.Repository;
using CocoriCore.Types.Extension;
using MongoDB.Driver;

namespace CocoriCore.MongoDb
{
    public class MongoDBRepository : IRepository, ITransactionHolder
    {
        private IClientSessionHandle _session;
        private IMongoClient _client;
        private IMongoDatabase _database;

        public MongoDBRepository(IMongoDatabase database)
        {
            _client = database.Client;
            _database = database;
        }

        public async Task<bool> ExistsAsync(Type entityType, Guid id)
        {
            var anyAsyncMethod = typeof(IAsyncCursorSourceExtensions)
                .GetMethod("AnyAsync")
                .MakeGenericMethod(new[] { entityType });

            var findCollection = FindById(entityType, id);
            var task = (Task)anyAsyncMethod.Invoke(findCollection, new[] { findCollection, null });
            return await task.GetResultAsync<bool>();
        }

        private object FindById(Type entityType, Guid id)
        {
            var getCollectionMethod = _database
                .GetType()
                .GetMethod(nameof(IMongoDatabase.GetCollection))
                .MakeGenericMethod(new Type[] { entityType });

            var findMethod = typeof(IMongoCollectionExtensions)
                .GetMethods()
                .Where(x =>
                    x.Name == "Find" &&
                    x.GetParameters().Count() == 3 &&
                    x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IMongoCollection<>)) &&
                    x.GetParameters()[1].ParameterType.IsAssignableToGeneric(typeof(Expression<>)) &&
                    x.GetParameters()[2].ParameterType == typeof(FindOptions))
                .Single()
                .MakeGenericMethod(new[] { entityType });

            var parameter = Expression.Parameter(entityType, "x");
            var idExpression = Expression.MakeMemberAccess(parameter, entityType.GetProperty(nameof(IEntity.Id)));
            var valueExpression = Expression.Constant(id);
            var equalExpression = Expression.Equal(idExpression, valueExpression);
            var findLambda = Expression.Lambda(equalExpression, parameter);

            var collection = getCollectionMethod.Invoke(_database, new[] { entityType.Name, null });
            return findMethod.Invoke(collection, new[] { collection, findLambda, null });
        }

        public async Task<bool> ExistsAsync(Type entityType, MemberInfo member, object value)
        {
            var getCollectionMethod = _database
                .GetType()
                .GetMethod(nameof(IMongoDatabase.GetCollection))
                .MakeGenericMethod(new Type[] { entityType });

            var findMethod = typeof(IMongoCollectionExtensions)
                .GetMethods()
                .Where(x =>
                    x.Name == "Find" &&
                    x.GetParameters().Count() == 3 &&
                    x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IMongoCollection<>)) &&
                    x.GetParameters()[1].ParameterType.IsAssignableToGeneric(typeof(Expression<>)) &&
                    x.GetParameters()[2].ParameterType == typeof(FindOptions))
                .Single()
                .MakeGenericMethod(new[] { entityType });

            var parameter = Expression.Parameter(entityType, "x");
            var idExpression = Expression.MakeMemberAccess(parameter, member);
            var valueExpression = Expression.Constant(value);
            var equalExpression = Expression.Equal(idExpression, valueExpression);
            var findLambda = Expression.Lambda(equalExpression, parameter);

            var collection = getCollectionMethod.Invoke(_database, new[] { entityType.Name, null });
            var findCollection = findMethod.Invoke(collection, new[] { collection, findLambda, null });

            var anyAsyncMethod = typeof(IAsyncCursorSourceExtensions)
                .GetMethod("AnyAsync")
                .MakeGenericMethod(new[] { entityType });

            var task = (Task)anyAsyncMethod.Invoke(findCollection, new[] { findCollection, null });
            return await task.GetResultAsync<bool>();
        }

        public async Task<bool> ExistsAsync(Type entityType, IEnumerable<Guid> collection)
        {
            foreach (var id in collection)
            {
                var exists = await ExistsAsync(entityType, id);
                if (!exists)
                    return false;
            }
            return true;
        }

        public async Task<IEntity> LoadAsync(Type entityType, Guid id)
        {
            var singleAsyncMethod = typeof(IAsyncCursorSourceExtensions)
                 .GetMethod("SingleAsync")
                 .MakeGenericMethod(new[] { entityType });

            var findCollection = FindById(entityType, id);
            var task = (Task)singleAsyncMethod.Invoke(findCollection, new[] { findCollection, null });
            return await task.GetResultAsync<IEntity>();
        }

        public Task<IEnumerable<TEntity>> LoadAsync<TEntity>(IEnumerable<Guid> collection)
             where TEntity : class, IEntity
        {
            throw new NotImplementedException();

        }
        public Task<IEnumerable<IEntity>> LoadAsync(Type entityType, IEnumerable<Guid> collection)
        {
            throw new NotImplementedException();
        }

        private async Task<object> TryLoadAsync(Type entityType, Guid id)
        {
            var singleOrDefaultAsyncMethod = typeof(IAsyncCursorSourceExtensions)
                 .GetMethod("SingleOrDefaultAsync")
                 .MakeGenericMethod(new[] { entityType });

            var findCollection = FindById(entityType, id);
            var task = (Task)singleOrDefaultAsyncMethod.Invoke(findCollection, new[] { findCollection, null });
            return await task.GetResultAsync<object>();
        }

        public async Task DeleteAsync<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            if (_session == null)
            {
                _session = await _client.StartSessionAsync();
                _session.StartTransaction();
            }
            await _database
               .GetCollection<TEntity>(entity.GetType().Name)
               .DeleteOneAsync(x => x.Id == entity.Id);
        }

        public Task<bool> ExistsAsync<TEntity>(Guid id) where TEntity : class, IEntity
        {
            return _database
                .GetCollection<TEntity>(typeof(TEntity).Name)
                .Find(x => x.Id == id)
                .AnyAsync();
        }

        public async Task InsertAsync<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            if (_session == null)
            {
                _session = await _client.StartSessionAsync();
                _session.StartTransaction();
            }
            try
            {
                await _database
                    .GetCollection<TEntity>(entity.GetType().Name)
                    .InsertOneAsync(_session, entity);
            }
            catch (MongoCommandException e)
            {
                if (e.Code == 263)
                {
                    throw new ConfigurationException("Ensure all collections are existing before insert when using transaction, "
                        + $"use {nameof(MongoCollectionsDefinition)} to declare all needed collections and ensure they are created.", e);
                }
            }
        }

        public async Task<TEntity> LoadAsync<TEntity>(Guid id) where TEntity : class, IEntity
        {
            return await _database
                .GetCollection<TEntity>(typeof(TEntity).Name)
                .Find(x => x.Id == id)
                .SingleAsync();
        }

        public Task<IEntity> LoadAsync(Type type, MemberInfo uniqueMember, object value)
        {
            throw new NotImplementedException();
        }

        public async Task<TEntity> LoadAsync<TEntity>(Expression<Func<TEntity, object>> uniqueMember, object value) where TEntity : class, IEntity
        {
            var parameter = Expression.Parameter(typeof(TEntity), "x");
            var memberInfo = uniqueMember.GetMemberInfo();
            value = value.ImplicitConvertTo(memberInfo.GetMemberType());
            var equalsExpression = Expression.Equal(Expression.MakeMemberAccess(parameter, memberInfo), Expression.Constant(value));
            var lambda = Expression.Lambda<Func<TEntity, bool>>(equalsExpression, parameter);
            return await _database
                .GetCollection<TEntity>(typeof(TEntity).Name)
                .Find(lambda)
                .SingleAsync();
        }

        public IQueryable<TEntity> Query<TEntity>() where TEntity : class, IEntity
        {
            return _database.GetCollection<TEntity>(typeof(TEntity).Name).AsQueryable();
        }

        public async Task<TEntity> ReloadAsync<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            return await _database
               .GetCollection<TEntity>(entity.GetType().Name)
               .Find(x => x.Id == entity.Id)
               .FirstAsync();
        }

        public async Task<TEntity> ReloadAsync<TEntity>(Guid id) where TEntity : class, IEntity
        {
            return await _database
               .GetCollection<TEntity>(typeof(TEntity).Name)
               .Find(x => x.Id == id)
               .FirstAsync();
        }

        public async Task UpdateAsync<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            if (_session == null)
            {
                _session = await _client.StartSessionAsync();
                _session.StartTransaction();
            }
            await _database
                .GetCollection<TEntity>(entity.GetType().Name)
                .ReplaceOneAsync(_session, x => x.Id == entity.Id, entity);
        }

        public async Task RollbackAsync()
        {
            if (_session != null)
            {
                await _session.AbortTransactionAsync();
                _session.Dispose();
                _session = null;
            }
        }

        public async Task CommitAsync()
        {
            if (_session != null)
            {
                await _session.CommitTransactionAsync();
                _session.Dispose();
                _session = null;
            }
        }

        public void Dispose()
        {
            if (_session != null)
            {
                _session.Dispose();
            }
        }

        public IQueryable Query(Type entityType)
        {
            throw new NotImplementedException();
        }

        public Task FlushAsync()
        {
            throw new NotImplementedException();
        }

        Task<bool> IReadRepository.ExistsAsync<TEntity>(IEnumerable<Guid> ids)
        {
            throw new NotImplementedException();
        }

        Task<bool> IReadRepository.ExistsAsync<TEntity>(Expression<Func<TEntity, object>> uniqueMember, object value)
        {
            throw new NotImplementedException();
        }
    }
}
