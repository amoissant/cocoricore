using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace CocoriCore.MongoDb
{
    public static class MongoAsyncCusorExtensions
    {
        public static async Task<List<T>> ToListAsync<T>(this IMongoQueryable<T> queryable, CancellationToken cancellationToken)
        {
            return await IAsyncCursorSourceExtensions.ToListAsync(queryable, cancellationToken);
        }
    }
}