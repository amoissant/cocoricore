﻿using FluentAssertions;
using FluentAssertions.Collections;
using FluentAssertions.Primitives;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using CocoriCore.Dynamic.Extension;
using System.Linq;
using System.Collections;
using FluentAssertions.Execution;
using CocoriCore.Clock;
using CocoriCore.Resource;
using CocoriCore.Common.Diagnostic;
using System;
using System.Reflection;
using System.Linq.Expressions;
using CocoriCore.Expressions.Extension;
using System.Dynamic;
using Microsoft.Extensions.Logging;
using CocoriCore.Common;

namespace CocoriCore.TestUtils
{
    public static class FluentAssertionExtensions
    {
        public static AndConstraint<GenericCollectionAssertions<TCollection, T>> NotContain<TCollection, T>(this GenericCollectionAssertions<TCollection, T> assertion,
            params T[] unexpectedElements)
            where TCollection : IEnumerable<T>
        {
            return assertion.NotContain(unexpectedElements.AsEnumerable());
        }

        public static AndConstraint<DateTimeAssertions> BeCloseToNow(this DateTimeAssertions assertion, IClock clock)
        {
            return assertion.BeCloseTo(clock.Now, TimeSpan.FromMilliseconds(10000));
        }

        public static AndConstraint<StringAssertions> NotContainVariables(this StringAssertions assertion)
        {
            var lines = assertion.Subject.Split('\n');
            var linesWithVariable = GetLinesWithVariable(lines);
            Execute.Assertion
                .ForCondition(linesWithVariable.Count == 0)
                .FailWith(BuildFailMessage(lines, linesWithVariable));
            return new AndConstraint<StringAssertions>(assertion);
        }

        private static List<(int lineNumber, string variable, string content)> GetLinesWithVariable(IList<string> lines)
        {
            var variableRegex = new Regex(@"\{\{([^\}]+)\}\}", RegexOptions.Compiled);
            var result = new List<(int lineNumber, string variable, string line)>();
            for (var i = 0; i < lines.Count(); i++)
            {
                var line = lines[i];
                var matches = variableRegex.Matches(line);
                if (matches.Count > 0)
                {
                    var lineNumber = i;
                    var variable = matches[0].Groups[1].Value;
                    var content = line.EscapeBraces().Trim();
                    result.Add((lineNumber, variable, content));
                }
            }
            return result;
        }

        private static string EscapeBraces(this string text)
        {
            return text.Replace("{", "{{").Replace("}", "}}");
        }

        private static string BuildFailMessage(IEnumerable<string> allLines, 
            List<(int lineNumber, string variable, string content)> linesWithVariable)
        {
            var message = "Expected rendered content to not contain any variable but found:\n";
            foreach (var line in linesWithVariable)
            {
                message += $"line {line.lineNumber} contains variable '{line.variable}' : \t\n\t{line.content}\n";
            }
            var numberedLines = string.Join("\n", allLines.Select((x, i) => $"{i}\t{x}")).EscapeBraces();
            message += $"\nRendered content: \n{numberedLines}";
            return message;
        }

        public static AndConstraint<StringAssertions> UseTemplate(this StringAssertions assertion,
            object templateKey, IResourceProvider resourceProvider)
        {
            var template = resourceProvider.ReadAsTextAsync(templateKey).Result;
            var regex = new Regex("\\{\\{[^\\}]+\\}\\}");
            var segments = regex.Split(template);
            return assertion.ContainAll(segments);
        }

        public static AndConstraint<StringAssertions> BeResource(this StringAssertions assertion,
            object resourceKey, IResourceProvider resourceProvider)
        {
            return assertion.Be(resourceProvider.ReadAsTextAsync(resourceKey).Result);
        }

        public static MemberInfoAssertions Should(this MemberInfo memberInfo)
        {
            return new MemberInfoAssertions(memberInfo);
        }

        public static void BeEquivalentToMembers<T>(this GenericCollectionAssertions<MemberInfo> assertion,
            params Expression<Func<T, object>>[] expressions)
        {
            assertion.Subject.Select(x => x.Name).Should().BeEquivalentTo(expressions.Select(x => x.GetMemberInfo().Name));
        }

        public static MethodCallAssertions Should(this IMethodCall methodCall)
        {
            return new MethodCallAssertions(methodCall);
        }

        public static ExpressionAssertions Should(this LambdaExpression expression)
        {
            return new ExpressionAssertions(expression);
        }

        public static AndConstraint<EnumAssertions<TEnum>> HasFlag<TEnum>(this EnumAssertions<TEnum> assertion, object expected) where TEnum : struct, Enum
        {
            var subjectType = assertion.Subject.GetType();
            object subjectValue = Enum.Parse(subjectType, assertion.Subject.ToString());
            object expectedValue = Enum.Parse(subjectType, expected.ToString());
            bool hasFlag = ((Enum)subjectValue).HasFlag((Enum)expectedValue);

            Execute.Assertion
                .ForCondition(hasFlag)
                .FailWith($"{subjectType} enum value {subjectValue} has not flag {expectedValue}");

            return new AndConstraint<EnumAssertions<TEnum>>(assertion);
        }
    }

    public static class ObjectAssertionExtensions
    {
        /// <summary>
        /// Check <paramref name="assertion"/>'s subject and <paramref name="expected"/> have the same structure by converting 
        /// them to <see cref="ExpandoObject"/> then comparing recursively every member's name and type.
        /// </summary>
        /// <param name="assertion"></param>
        /// <param name="expected">An object representing the expected structure, can be an anonymous class instance</param>
        /// <param name="ignoreMissingMembers">If <c>true</c> <paramref name="expected"/> can be subset of <paramref name="assertion"/>'s subject members 
        /// else <paramref name="expected"/> must contain all members present in <paramref name="assertion"/>'s subject</param>
        public static void MatchStructure(this GenericDictionaryAssertions<IDictionary<string, object>, string, object> assertion, 
            object expected, bool ignoreMissingMembers = false)
        {
            assertion.Subject.ToDynamic().Should().MatchStructure(expected, ignoreMissingMembers);
        }

        /// <summary>
        /// Check <paramref name="assertion"/>'s subject and <paramref name="expected"/> have the same structure by converting 
        /// them to <see cref="ExpandoObject"/> then comparing recursively every member's name and type.
        /// </summary>
        /// <param name="assertion"></param>
        /// <param name="expected">An object representing the expected structure, can be an anonymous class instance</param>
        /// <param name="ignoreMissingMembers">If <c>true</c> <paramref name="expected"/> can be subset of <paramref name="assertion"/>'s subject members 
        /// else <paramref name="expected"/> must contain all members present in <paramref name="assertion"/>'s subject</param>
        public static void MatchStructure(this ObjectAssertions assertion, object expected, bool ignoreMissingMembers = false)
        {
            var subject = assertion.Subject.ToDynamic();
            expected = expected.ToDynamic();

            if (ignoreMissingMembers)
            {
                RemoveMissingMembersFromSubjectRecursively(subject, expected);
            }
            try
            {
                subject.Should().BeEquivalentTo(expected);
            }
            catch (Exception e)
            {
                var errorMessage = $"\nExpected \n{subject.ToJson(indent: true)}\n" +
                    $"to be equivalent to\n{expected.ToJson(indent: true)}\n" + 
                    $"{e.Message}";
                throw new Exception(errorMessage, e);
            }
        }
        
        private static void RemoveMissingMembersFromSubjectRecursively(object subject, object expected)
        {
            if (subject is IDictionary<string, object> dynamicSubject &&
                expected is IDictionary<string, object> dynamicExpected)
            {
                foreach (var kvp in dynamicSubject.ToArray())
                {
                    if (!dynamicExpected.ContainsKey(kvp.Key))
                    {
                        dynamicSubject.Remove(kvp.Key);
                        continue;
                    }
                    RemoveMissingMembersFromSubjectRecursively(kvp.Value, dynamicExpected[kvp.Key]);
                }
            }
            else if (subject is IEnumerable && expected is IEnumerable)
            {
                var subjectEnumerable = ((IEnumerable)subject).Cast<object>().ToArray();
                var expectedEnumerable = ((IEnumerable)expected).Cast<object>().ToArray();
                if(subjectEnumerable.Length == expectedEnumerable.Length)
                {
                    for(var i = 0; i < expectedEnumerable.Length; i++)
                    {
                        RemoveMissingMembersFromSubjectRecursively(subjectEnumerable[i], expectedEnumerable[i]);
                    }
                }
            }
        }
    }

    public class MemberInfoAssertions
    {
        private MemberInfo _actualMemberInfo;

        public MemberInfoAssertions(MemberInfo memberInfo)
        {
            _actualMemberInfo = memberInfo;
        }

        public void BeMember<T>(Expression<Func<T, object>> expression)
        {
            var expectedMemberInfo = expression.GetMemberInfo();
            _actualMemberInfo.Name.Should().Be(expectedMemberInfo.Name);
        }
    }

    public static class LogEntryCollectionAssertions
    {
        /// <summary>
        /// Assert that an <see cref="LogEntry"/> in collection matches parameters.
        /// </summary>
        /// <param name="logEntries">The instance for this extension method</param>
        /// <param name="level">LogLevel of the log entry</param>
        /// <param name="text">Text to search in log entry's message, case insensitive, can contain wildcard.</param>
        public static void Contain(this GenericCollectionAssertions<LogEntry> logEntries, LogLevel level, string text)
        {
            Execute
                .Assertion
                .ForCondition(logEntries.Subject.Any(x => x.Level == level && x.Contains(text)))
                .FailWith(() =>
                {
                    var entriesText = string.Join("\n", logEntries.Subject);
                    return new FailReason("No log entry found for level {0} and text {1}.\nAvailable entries :\n{2}", level, text, entriesText);
                });
        }

        /// <summary>
        /// Assert that an <see cref="LogEntry"/> in collection matches parameters.
        /// </summary>
        /// <param name="logEntries">The instance for this extension method</param>
        /// <param name="text">Text to search in log entry's message, case insensitive, can contain wildcard.</param>
        public static void Contain(this GenericCollectionAssertions<LogEntry> logEntries, string text)
        {
            Execute
                .Assertion
                .ForCondition(logEntries.Subject.Any(x => x.Contains(text)))
                .FailWith(() =>
                {
                    var entriesText = string.Join("\n", logEntries.Subject);
                    return new FailReason("No log entry found for text {0}.\nAvailable entries :\n{1}", text, entriesText);
                });
        }
    }

    public class MethodCallAssertions : ObjectAssertions
    {
        private IMethodCall _actual;

        public MethodCallAssertions(object value) : base(value)
        {
            _actual = (IMethodCall)value;
        }

        public void WrapCall(Delegate @delegate)
        {
            Execute
                .Assertion
                .ForCondition(Equals(_actual.Delegate, @delegate))
                .FailWith("Actual IMethodCall doesn't wrap excepected delegate.");
        }
    }

    public class ExpressionAssertions
    {
        private Expression _actual;

        public ExpressionAssertions(Expression actual)
        {
            _actual = actual;
        }

        public void Be(Expression expected)
        {
            _actual.Should().Be(expected);
        }

        public void NotBe(Expression expected)
        {
            _actual.Should().NotBe(expected);
        }

        public void BeNull()
        {
            _actual.Should().BeNull();
        }

        public void NotBeNull()
        {
            _actual.Should().NotBeNull();
        }

        public void BeEquivalentTo(Expression expression)
        {
            Format(_actual).Should().Be(Format(expression));
        }

        public void BeEquivalentTo<T>(Expression<Action<T>> expression)
        {
            Format(_actual).Should().Be(Format(expression));
        }

        public void BeEquivalentTo<T1, T2>(Expression<Action<T1, T2>> expression)
        {
            Format(_actual).Should().Be(Format(expression));
        }

        public void BeEquivalentTo<T, TResult>(Expression<Func<T, TResult>> expression)
        {
            Format(_actual).Should().Be(Format(expression));
        }

        public void BeEquivalentTo<T1, T2, TResult>(Expression<Func<T1, T2, TResult>> expression)
        {
            Format(_actual).Should().Be(Format(expression));
        }

        public void BeEquivalentTo<T1, T2, T3, TResult>(Expression<Func<T1, T2, T3, TResult>> expression)
        {
            Format(_actual).Should().Be(Format(expression));
        }

        private static string Format(Expression expression)
        {
            var result = expression?.ToString();
            if (result != null)
            {
                result = Regex.Replace(result, @"value\(([^\)]+\)\.)", string.Empty);
            }
            return result;
        }
    }
}