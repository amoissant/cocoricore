using System;
using Autofac;

namespace CocoriCore.TestUtils
{
    public class TestWithDataSets<TDataSets, TDependencyInjection> : IDisposable
        where TDataSets : IDataSet
        where TDependencyInjection : IDependencyInjection, new()
    {
        protected TDataSets _dataSets;
        protected TDependencyInjection _di;

        public TestWithDataSets()
        {
            _di = new TDependencyInjection();
        }

        protected virtual TDataSets _
        {
            get
            {
                if (_dataSets == null)
                    _dataSets = (TDataSets)_di.RootScope.Resolve<IDataSet>();
                return _dataSets;
            }
        }

        public void Dispose()
        {
            _dataSets?.Dispose();
            _di?.Dispose();
        }
    }
}