﻿using Castle.DynamicProxy;
using CocoriCore.Common;
using CocoriCore.Repository;
using CocoriCore.Types.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace CocoriCore.TestUtils
{
    public class DataSetDecorator : IRepositoryDecorator
    {
        private readonly static ProxyGenerator _generator = new ProxyGenerator();

        private readonly IDataSet _dataSets;
        private readonly Dictionary<IEntity, IEntity> _cache;

        public DataSetDecorator(IDataSet dataSets)
        {
            _dataSets = dataSets;
            _cache = new Dictionary<IEntity, IEntity>();
        }

        public Task InsertAsync(IEntity entity, InsertAsync next)
        {
            return next(entity);
        }

        public Task UpdateAsync(IEntity entity, UpdateAsync next)
        {
            entity = UnproxifyEntity(entity);
            return next(entity);
        }

        public Task DeleteAsync(IEntity entity, DeleteAsync next)
        {
            entity = UnproxifyEntity(entity);
            return next(entity);
        }

        public async Task<IEnumerable<IEntity>> LoadAsync(Type type, Guid id, LoadIdAsync next)
        {
            var entities = await next(type, id);
            return entities.Select(e => GetOrCreateDataSetProxy(e));
        }

        public async Task<IEnumerable<IEntity>> LoadAsync(Type type, IEnumerable<Guid> ids, LoadIdCollectionAsync next)
        {
            var entities = await next(type, ids);
            return entities.Select(e => GetOrCreateDataSetProxy(e));
        }

        public async Task<IEnumerable<IEntity>> LoadAsync(Type type, MemberInfo uniqueMember, object value, LoadUniqueAsync next)
        {
            var entities = await next(type, uniqueMember, value);
            return entities.Select(e => GetOrCreateDataSetProxy(e));
        }

        public IQueryable<TEntity> Query<TEntity>(Query<TEntity> next) where TEntity : IEntity
        {
            return new ExtendedEnumerable<TEntity>(next(), e => GetOrCreateDataSetProxy(e));
        }

        public TEntity GetOrCreateDataSetProxy<TEntity>(TEntity entity) where TEntity : IEntity
        {
            //TODO TECH si on oublie un membre virtual alors on a des valeur par défaut dans les proxy ce qui ets source d'erreurs.
            if (_cache.ContainsKey(entity))
            {
                var proxy = (IWithDataSet)_cache[entity];
                proxy.Entity = entity;
                return (TEntity)proxy;
            }
            else
            {
                var entityType = entity.GetType();
                var interceptor = typeof(DataSetInterceptor<>).CreateGenericInstance<IInterceptor>(entityType, _dataSets, entity);
                var proxy = (TEntity)_generator.CreateClassProxyWithTarget(entityType, new Type[] { typeof(IWithDataSet) }, entity, interceptor);
                _cache[entity] = proxy;
                return proxy;
            }
        }

        public TEntity UnproxifyEntity<TEntity>(TEntity entity)
        {
            if (entity is IWithDataSet)
            {
                IWithDataSet proxy = (IWithDataSet)entity;
                return (TEntity)proxy.Entity;
            }
            else
            {
                return entity;
            }
        }

        public Task FlushAsync(FlushAsync next)
        {
            return next();
        }
    }
}
