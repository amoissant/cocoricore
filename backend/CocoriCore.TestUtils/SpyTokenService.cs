﻿using CocoriCore.Clock;
using CocoriCore.Jwt;
using System.Collections.Generic;
using System.Linq;

namespace CocoriCore.TestUtils
{
    public class SpyTokenService : TokenService
    {
        private Dictionary<string, TokenObject> _tokens;

        public SpyTokenService(IClock clock, TokenConfiguration configuration, IJwtSerializer jwtSerializer)
            : base(clock, configuration, jwtSerializer)
        {
            _tokens = new Dictionary<string, TokenObject>();
        }

        public TokenObject LastToken => _tokens.Last().Value;

        public IEnumerable<TokenObject> Tokens => _tokens.Values;

        public override string GenerateToken<T>(T payload, string extraSalt = null)
        {
            var token = base.GenerateToken(payload, extraSalt);
            _tokens[token] = new TokenObject(token, payload, extraSalt);
            return token;
        }

        public TokenObject<T> GetToken<T>(string token)
        {
            return _tokens[token].CastPayload<T>();
        }
    }
}
