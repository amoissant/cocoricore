﻿using System;
using Xunit.Abstractions;
using Microsoft.Extensions.Logging;
using System.Diagnostics;

namespace CocoriCore.TestUtils
{
    public class XUnitOutputAdapter : ILogger
    {
        private readonly ITestOutputHelper _output;
        private readonly string _categoryName;

        public XUnitOutputAdapter(ITestOutputHelper output, string categoryName)
        {
            _output = output;
            _categoryName = categoryName;
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            return null;
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return true;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            try
            {
                var message = formatter(state, exception);
                _output.WriteLine($"{_categoryName} - [{logLevel}] {message}");
            }
            catch(Exception e)
            {
                Debug.Write(e.ToString());
            }
        }
    }
}

