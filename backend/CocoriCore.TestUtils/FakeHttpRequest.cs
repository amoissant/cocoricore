using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace CocoriCore.TestUtils
{
    public class FakeHttpRequest: HttpRequest
    {
        public FakeHttpRequest()
        {
            Body = new MemoryStream();
            Headers = new FakeHeaderDictionary();
            Query = new FakeQueryCollection();
            Cookies = new FakeRequestCookieCollection();
            HttpContext = new FakeHttpContext(httpRequest: this);
        }

        public FakeHttpRequest(HttpContext context)
        {
            Body = new MemoryStream();
            Headers = new FakeHeaderDictionary();
            Query = new FakeQueryCollection();
            Cookies = new FakeRequestCookieCollection();
            HttpContext = context;
        }

        public override HttpContext HttpContext { get; }
        public override string Method { get; set; }
        public override string Scheme { get; set; }
        public override bool IsHttps { get; set; }
        public override HostString Host { get; set; }
        public override PathString PathBase { get; set; }
        public override PathString Path { get; set; }
        public override QueryString QueryString 
        {
            get => QueryString.Create(Query);
            set => Query = new FakeQueryCollection(value);
        }
        public override IQueryCollection Query { get; set; }
        public override string Protocol { get; set; }
        public override IHeaderDictionary Headers { get; }
        public override IRequestCookieCollection Cookies { get; set; }
        public override long? ContentLength { get; set; }
        public override string ContentType { get; set; }
        public override Stream Body { get; set; }

        public override bool HasFormContentType => throw new NotImplementedException();

        public override IFormCollection Form { get; set; }

        public void SetTextBody(string content)
        {
            Body.WriteBodyAsString(content);
        }

        public string GetTextBody()
        {
            return Body.ReadBodyAsString();
        }
        
        public FakeHttpRequest Default(params Action<FakeHttpRequest>[] actions)
        {
            Scheme = "http";
            Host = new HostString("localhost:8080");
            Path = "/";
            PathBase = new PathString("/");
            Method = HttpMethods.Get;
            SetTextBody("{}");
            Protocol = "HTTP/1.1";
            actions.ApplyOn(this);
            return this;
        }

        public FakeHttpRequest CorsPreflight(params Action<FakeHttpRequest>[] actions)
        {
            Scheme = "http";
            Host = new HostString("localhost:8080");
            Path = "/";
            PathBase = new PathString("/");
            Method = HttpMethods.Options;
            SetTextBody("{}");
            Protocol = "HTTP/1.1";
            Headers["Access-Control-Request-Method"] = HttpMethods.Post;
            Headers["Access-Control-Request-Headers"] = "Content-Type";
            actions.ApplyOn(this);
            return this;
        }

        public override Task<IFormCollection> ReadFormAsync(CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }
    }
}