﻿using Xunit.Abstractions;
using Microsoft.Extensions.Logging;
using System.Text.RegularExpressions;

namespace CocoriCore.TestUtils
{
    public class XUnitLoggerProvider : ILoggerProvider
    {
        private readonly ITestOutputHelper _output;
        private readonly Regex _filter;
        private readonly ITestOutputHelper _silentOutput;

        public XUnitLoggerProvider(ITestOutputHelper output, string filter = null)
        {
            _filter = filter != null ? new Regex(filter, RegexOptions.IgnoreCase | RegexOptions.Compiled) : null;
            _silentOutput = new SilentOutput();
            _output = output ?? _silentOutput;
        }

        public ILogger CreateLogger(string categoryName)
        {
            var output = _filter?.IsMatch(categoryName) == false ? _silentOutput : _output;
            return new XUnitOutputAdapter(output, categoryName);
        }

        public void Dispose()
        {
        }
    }

    public static class LoggingBuilderXunitExtension
    {
        public static ILoggingBuilder AddXunit (this ILoggingBuilder builder, ITestOutputHelper output, string filter = null)
        {
            return builder.AddProvider(new XUnitLoggerProvider(output, filter));
        }
    }
}

