﻿using System;
using System.Collections.Concurrent;
using CocoriCore.Common;
using CocoriCore.Types.Extension;

namespace CocoriCore.TestUtils
{
    public class DataSetEntities
    {
        private readonly ConcurrentDictionary<(Type, Guid), IEntity> entities;

        public DataSetEntities()
        {
            entities = new ConcurrentDictionary<(Type, Guid), IEntity>();
        }

        public TEntity Get<TEntity>(Guid id)
        {
            if (entities.TryGetValue((typeof(TEntity), id), out var entity))
            {
                return (TEntity)entity;
            }
            throw new InvalidOperationException($"Unable to retrieve entity of type {typeof(TEntity)} with id {id} from cache.");
        }

        public bool TryGet(Type type, Guid id, out IEntity entity)
        {
            return entities.TryGetValue((type, id), out entity);
        }

        public bool ContainsKey(Type type, Guid id)
        {
            return entities.ContainsKey((type, id));
        }

        public void Remove(IEntity entity)
        {
            var typeHierarchy = entity.GetTypeHierarchy();
            foreach (var type in typeHierarchy)
            {
                entities.TryRemove((type, entity.Id), out _);
            }
        }

        public void Add(IEntity entity)
        {
            entity = entity ?? throw new ArgumentNullException(nameof(entity));
            if (entity.Id == default)
                throw new InvalidOperationException($"Provided entity of type {entity.GetType()} has an empty id.");
            var typeHierarchy = entity.GetTypeHierarchy();
            foreach (var type in typeHierarchy)
            {
                entities[(type, entity.Id)] = entity;
            }
        }
    }
}