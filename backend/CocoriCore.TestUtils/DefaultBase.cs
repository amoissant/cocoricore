﻿using System;
using System.Collections.Generic;

namespace CocoriCore.TestUtils
{
    public abstract class DefaultBase : IDefault
    {
        private Dictionary<Type, Action<object>> _defaults;

        public DefaultBase()
        {
            _defaults = new Dictionary<Type, Action<object>>();
        }

        protected DefaultBase DefineDefault<T>(Action<T> defaultModifications)
        {
            _defaults[typeof(T)] = x => defaultModifications((T)x);
            return this;
        }

        public IEnumerable<KeyValuePair<Type, Action<object>>> GetDefaults()
        {
            return _defaults;
        }
    }
}
