﻿using FluentAssertions;
using Flurl.Http.Testing;

namespace CocoriCore.TestUtils
{
    public static class HttpTestExtensions
    {
        public static HttpCallAssertion WithRequestBody(this HttpCallAssertion assertion, object expectedJson, bool ignoreMissingMembers = false)
        {
            return assertion.WithRequestBody(expectedJson, out _, ignoreMissingMembers);
        }

        public static HttpCallAssertion WithRequestBody(this HttpCallAssertion assertion, object expectedJson,
            out string requestBody, bool ignoreMissingMembers = false)
        {
            string json = null;
            assertion.With(c => { json = c.RequestBody; return true; });
            var actualJson = json == null ? null : Utf8Json.JsonSerializer.Deserialize<object>(json);
            actualJson.Should().MatchStructure(expectedJson, ignoreMissingMembers);
            requestBody = json;
            return assertion;
        }
    }
}

