using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CocoriCore.TestUtils
{
    public static class ActionsExtension
    {
        public static void ApplyOn<T>(this IEnumerable<Action<T>> actions, T obj)
        {
            foreach (var action in actions)
            {
                if (action != null)
                {
                    action(obj);
                }
            }
        }

        public static Exception CatchException(this Action action)
        {
            try
            {
                action();
            }
            catch (Exception e)
            {
                return e;
            }
            return null;
        }

        public static async Task<Exception> CatchExceptionAsync(this Func<Task> action)
        {
            try
            {
                await action();
            }
            catch (Exception e)
            {
                return e;
            }
            return null;
        }
    }
}