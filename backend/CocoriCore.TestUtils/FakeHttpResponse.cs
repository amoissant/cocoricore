using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;

namespace CocoriCore.TestUtils
{
    public class FakeHttpResponse : HttpResponse
    {
        private bool _hasStarted;

        public override IResponseCookies Cookies { get; }
        public FakeResponseCookies FakeCookies => (FakeResponseCookies)Cookies;

        public FakeHttpResponse()
        {
            StatusCode = 200;
            Body = new MemoryStream();
            Headers = new FakeHeaderDictionary();
            Cookies = new FakeResponseCookies();
            HttpContext = new FakeHttpContext(httpResponse: this);
        }

        public FakeHttpResponse(HttpContext context)
        {
            StatusCode = 200;
            Body = new MemoryStream();
            Headers = new FakeHeaderDictionary();
            Cookies = new FakeResponseCookies();
            HttpContext = context;
        }

        public void SetTextBody(string content)
        {
            Body.WriteBodyAsString(content);
        }

        public string GetTextBody()
        {
            return Body.ReadBodyAsString();
        }

        public FakeHttpResponse Default(params Action<FakeHttpResponse>[] actions)
        {
            StatusCode = 200;
            SetTextBody(string.Empty);
            ContentLength = Body.Length;
            ContentType = "application/json";
            Headers.Add("ContentType", new StringValues(ContentType));
            Headers.Add("ContentLength", new StringValues(string.Empty + ContentLength));
            actions.ApplyOn(this);
            return this;
        }

        public override bool HasStarted => _hasStarted;
        public override HttpContext HttpContext { get; }
        public override int StatusCode { get ; set; }
        public override IHeaderDictionary Headers { get; }
        public override Stream Body { get; set; }
        public override long? ContentLength { get; set; }
        public override string ContentType { get; set; }

        public void SetHasStarted(bool hasStarted)
        {
            _hasStarted = hasStarted;
        }

        public override void OnStarting(Func<object, Task> callback, object state)
        {
            throw new NotImplementedException();
        }

        public override void OnCompleted(Func<object, Task> callback, object state)
        {
            throw new NotImplementedException();
        }

        public override void Redirect(string location, bool permanent)
        {
            throw new NotImplementedException();
        }
    }
}