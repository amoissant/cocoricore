﻿using System;
using System.Collections.Generic;

namespace CocoriCore.TestUtils
{
    public interface IDefault
    {
        IEnumerable<KeyValuePair<Type, Action<object>>> GetDefaults();
    }
}
