﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;

namespace CocoriCore.TestUtils
{
    public class FakeHeaderDictionary : IHeaderDictionary
    {
        private Dictionary<string, StringValues> _dictionary;

        public FakeHeaderDictionary()
        {
            _dictionary = new Dictionary<string, StringValues>();
        }

        public StringValues this[string key] 
        { 
            get => _dictionary.ContainsKey(key.ToLower()) ? _dictionary[key.ToLower()] : StringValues.Empty;
            set 
            {
                _dictionary.Remove(key.ToLower());
                if (!string.IsNullOrEmpty(value))
                {
                    _dictionary[key.ToLower()] = value;
                }
            }
        }

        public long? ContentLength { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public ICollection<string> Keys => _dictionary.Keys;

        public ICollection<StringValues> Values => _dictionary.Values;

        public int Count => _dictionary.Count;

        public bool IsReadOnly => false;

        public void Add(string key, StringValues value)
        {
            _dictionary.Add(key.ToLower(), value);
        }

        public void Add(KeyValuePair<string, StringValues> item)
        {
            _dictionary.Add(item.Key.ToLower(), item.Value);
        }

        public void Clear()
        {
            _dictionary.Clear();
        }

        public bool Contains(KeyValuePair<string, StringValues> item)
        {
            return _dictionary.AsEnumerable().Contains(item);
        }

        public bool ContainsKey(string key)
        {
            return _dictionary.ContainsKey(key.ToLower());
        }

        public void CopyTo(KeyValuePair<string, StringValues>[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<KeyValuePair<string, StringValues>> GetEnumerator()
        {
            return _dictionary.GetEnumerator();
        }

        public bool Remove(string key)
        {
            return _dictionary.Remove(key.ToLower());
        }

        public bool Remove(KeyValuePair<string, StringValues> item)
        {
            return _dictionary.Remove(item.Key.ToLower());
        }

        public bool TryGetValue(string key, out StringValues value)
        {
            return _dictionary.TryGetValue(key.ToLower(), out value);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _dictionary.GetEnumerator();
        }
    }
}