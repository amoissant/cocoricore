﻿using System.Collections;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Primitives;

namespace CocoriCore.TestUtils
{
    public class FakeQueryCollection : IQueryCollection
    {
        private Dictionary<string, StringValues> _dictionary;

        public FakeQueryCollection()
        {
            _dictionary = new Dictionary<string, StringValues>();
        }
        public FakeQueryCollection(QueryString queryString)
        {
            _dictionary = QueryHelpers.ParseQuery(queryString.Value);
        }

        public StringValues this[string key]
        {
            get => _dictionary.ContainsKey(key.ToLower()) ? _dictionary[key.ToLower()] : StringValues.Empty;
            set => _dictionary[key.ToLower()] = value;
        }

        public ICollection<string> Keys => _dictionary.Keys;

        public int Count => _dictionary.Count;

        public bool ContainsKey(string key)
        {
            return _dictionary.ContainsKey(key.ToLower());
        }

        public IEnumerator<KeyValuePair<string, StringValues>> GetEnumerator()
        {
            return _dictionary.GetEnumerator();
        }

        public bool TryGetValue(string key, out StringValues value)
        {
            return _dictionary.TryGetValue(key.ToLower(), out value);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _dictionary.GetEnumerator();
        }
    }
}