﻿using System.Collections;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace CocoriCore.TestUtils
{
    public class FakeRequestCookieCollection : IRequestCookieCollection
    {
        private Dictionary<string, string> _dictionary;

        public FakeRequestCookieCollection()
        {
            _dictionary = new Dictionary<string, string>();
        }

        public string this[string key]
        {
            get => _dictionary.ContainsKey(key.ToLower()) ? _dictionary[key.ToLower()] : null;
            set => _dictionary[key.ToLower()] = value;
        }

        public void Add(string key, string value)
        {
            _dictionary.Add(key, value);
        }

        public ICollection<string> Keys => _dictionary.Keys;

        public int Count => _dictionary.Count;

        public bool ContainsKey(string key)
        {
            return _dictionary.ContainsKey(key.ToLower());
        }

        public IEnumerator<KeyValuePair<string, string>> GetEnumerator()
        {
            return _dictionary.GetEnumerator();
        }

        public bool TryGetValue(string key, out string value)
        {
            return _dictionary.TryGetValue(key.ToLower(), out value);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _dictionary.GetEnumerator();
        }
    }
}