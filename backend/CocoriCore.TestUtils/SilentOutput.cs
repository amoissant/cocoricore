﻿using Xunit.Abstractions;

namespace CocoriCore.TestUtils
{
    public class SilentOutput : ITestOutputHelper
    {
        public void WriteLine(string message)
        {
        }

        public void WriteLine(string format, params object[] args)
        {
        }
    }
}

