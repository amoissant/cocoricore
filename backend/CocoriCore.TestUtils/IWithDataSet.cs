using CocoriCore.Common;

namespace CocoriCore.TestUtils
{
    public interface IWithDataSet
    {
        IDataSet DataSet { get; set; }
        IEntity Entity { get; set; }
    }
}
