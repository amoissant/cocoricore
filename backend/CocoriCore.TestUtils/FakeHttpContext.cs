using System;
using Microsoft.AspNetCore.Http;

namespace CocoriCore.TestUtils
{
    public class FakeHttpContext : DefaultHttpContext
    {
        public FakeHttpContext(HttpRequest httpRequest = null, HttpResponse httpResponse = null)
        {
            Request = httpRequest ?? new FakeHttpRequest(this).Default();
            Response = httpResponse ?? new FakeHttpResponse(this).Default();
        }

        public override HttpRequest Request { get; }
        public override HttpResponse Response { get; }

        public FakeHttpRequest FakeRequest => (FakeHttpRequest)Request;
        public FakeHttpResponse FakeResponse => (FakeHttpResponse)Response;

        public FakeHttpContext Default(params Action<HttpContext>[] actions)
        {
            actions.ApplyOn(this);
            return this;
        }
    }
}