﻿using System.Collections.Generic;
using System.Linq;

namespace CocoriCore.TestUtils
{
    public static class IEnumerableExtensions
    {
        public static IEnumerable<T> Reverse<T>(this IEnumerable<T> enumerable, bool reverse)
        {
            return reverse ? enumerable.Reverse() : enumerable;
        }
    }
}