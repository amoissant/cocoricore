﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using CocoriCore.Common;
using CocoriCore.Reflection.Extension;
using CocoriCore.Repository;

namespace CocoriCore.TestUtils
{
    public class DataSetEntitiesDecorator : IRepositoryDecorator
    {
        private readonly DataSetEntities _entityCache;

        public DataSetEntitiesDecorator(DataSetEntities entityCache)
        {
            _entityCache = entityCache;
        }

        public async Task<IEnumerable<IEntity>> LoadAsync(Type type, Guid id, LoadIdAsync next)
        {
            return await next(type, id);
        }

        public async Task<IEnumerable<IEntity>> LoadAsync(Type type, IEnumerable<Guid> ids, LoadIdCollectionAsync next)
        {
            return await next(type, ids);
        }

        public async Task<IEnumerable<IEntity>> LoadAsync(Type type, MemberInfo uniqueMember, object value, LoadUniqueAsync next)
        {
            return await next(type, uniqueMember, value);
        }

        public IQueryable<TEntity> Query<TEntity>(Query<TEntity> next) where TEntity : IEntity
        {
            return next();
        }

        public async Task InsertAsync(IEntity entity, InsertAsync next)
        {
            await next(entity);
            CacheOrUpdateEntity(entity);
        }

        public async Task UpdateAsync(IEntity entity, UpdateAsync next)
        {
            await next(entity);
            CacheOrUpdateEntity(entity);
        }

        private void CacheOrUpdateEntity(IEntity entity)
        {
            var entityType = entity.GetType();
            if (_entityCache.TryGet(entityType, entity.Id, out var cachedEntity) && cachedEntity != entity)
            {
                entity.ShallowCopyTo(cachedEntity);
            }
            else
            {
                _entityCache.Add(entity);
            }
        }

        public async Task DeleteAsync(IEntity entity, DeleteAsync next)
        {
            await next(entity);
            _entityCache.Remove(entity);
        }

        public Task FlushAsync(FlushAsync next)
        {
            return next();
        }
    }
}