using CocoriCore.Security;

namespace CocoriCore.TestUtils
{
    public class SpyPasswordService : PasswordService
    {
        public SpyPasswordService(IHashService hashService, PasswordOptions options = null) 
            : base(hashService, options)
        {
        }

        public string LastPasswordHash { get; private set; }

        public override string HashPassword(string password)
        {
            var hash = base.HashPassword(password);
            LastPasswordHash = hash;
            return hash;
        }
    }
}