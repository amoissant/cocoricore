﻿
namespace CocoriCore.TestUtils
{
    public class TokenObject
    {
        public TokenObject(string value, object payload, string extraSalt = null)
        {
            Payload = payload;
            ExtraSalt = extraSalt;
            Value = value;
        }

        public object Payload { get; }
        public string ExtraSalt { get; }
        public string Value { get; }

        public TokenObject<T> CastPayload<T>()
        {
            return new TokenObject<T>(Value, (T)Payload, ExtraSalt);
        }
    }

    public class TokenObject<T> : TokenObject
    {
        public TokenObject(string value, T payload, string extraSalt = null)
            : base(value, payload, extraSalt)
        {
        }

        public new T Payload => (T)base.Payload;
    }
}
