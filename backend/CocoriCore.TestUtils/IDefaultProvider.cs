﻿using System;

namespace CocoriCore.TestUtils
{
    public interface IDefaultProvider
    {
        T ApplyDefaults<T>(T message);
        T GetDefault<T>(Action<T> modifications = null) where T : class, new();
    }
}
