﻿using System;

namespace CocoriCore.TestUtils
{
    public class InvalidImplementationException : Exception
    {
        public InvalidImplementationException(string message) : base(message)
        {
        }
    }
}
