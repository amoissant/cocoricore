﻿using Microsoft.Extensions.Logging;
using System;

namespace CocoriCore.TestUtils
{
    public class SpyLoggerProvider : ILoggerProvider
    {
        private readonly LogEntryCollection _logEntries;

        public SpyLoggerProvider(LogEntryCollection logEntries)
        {
            _logEntries = logEntries;
        }

        public ILogger CreateLogger(string categoryName)
        {
            return new SpyLogger(categoryName, _logEntries);
        }

        public void Dispose()
        {
        }
    }


    public class SpyLogger : ILogger
    {
        private readonly string _categoryName;
        private readonly LogEntryCollection _logEntries;

        public SpyLogger(string categoryName, LogEntryCollection logEntries)
        {
            _categoryName = categoryName;
            _logEntries = logEntries;
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            throw new NotImplementedException();
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return true;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            _logEntries.Add(new LogEntry(logLevel, _categoryName, formatter(state, exception)));
        }
    }

    public static class LoggingBuilderSpyExtension
    {
        public static ILoggingBuilder AddSpy(this ILoggingBuilder builder, LogEntryCollection logEntries = null)
        {
            if (logEntries != null)
            {
                builder.AddProvider(new SpyLoggerProvider(logEntries));
            }
            return builder;
        }
    }
}

