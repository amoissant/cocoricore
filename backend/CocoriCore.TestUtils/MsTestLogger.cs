﻿using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;

namespace CocoriCore.TestUtils
{
    public class MsTestLogger : ILogger
    {
        private readonly LogLevel _level;

        public MsTestLogger(LogLevel level)
        {
            _level = level;
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            return null;
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return logLevel >= _level;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            var t = formatter(state, exception);
            Trace.WriteLine($"[{logLevel}] - {t}");//TODO à mettre au propre
        }
    }

    public class MsTestLogger<T> : MsTestLogger, ILogger<T>
    {
        public MsTestLogger(LogLevel level = LogLevel.Debug) 
            : base(level)
        {
        }
    }
}

