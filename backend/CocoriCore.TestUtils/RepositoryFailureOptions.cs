using CocoriCore.Common;
using CocoriCore.Repository;
using System;

namespace CocoriCore.Test
{
    public class RepositoryFailureOptions
    {
        public int DelayInMilliseconds { get; set; }
        public Func<CUDOperation, IEntity, bool> ErrorCondition { get; set; }
        public Func<Type, bool> QueryErrorCondition { get; set; }
        public int TotalNbQueryCall { get; set; }
        public int TotalNbUpdateCall { get; set; }
        public ConcurrentHashSet<(Type, Guid)> LoadedEntities { get; set; }

        public RepositoryFailureOptions()
        {
            Reset();
        }

        public RepositoryFailureOptions WithDelay(uint delayInMilliseconds)
        {
            DelayInMilliseconds = (int)delayInMilliseconds;
            return this;
        }

        public RepositoryFailureOptions ErrorWhen(Func<CUDOperation, IEntity, bool> errorCondition)
        {
            ErrorCondition = errorCondition;
            return this;
        }

        public RepositoryFailureOptions ErrorWhenQuery(Func<Type, bool> errorCondition)
        {
            QueryErrorCondition = errorCondition;
            return this;
        }

        public RepositoryFailureOptions Reset()
        {
            DelayInMilliseconds = 0;
            ErrorCondition = (o, e) => false;
            QueryErrorCondition = t => false;
            TotalNbQueryCall = 0;
            TotalNbUpdateCall = 0;
            LoadedEntities = new ConcurrentHashSet<(Type, Guid)>();
            return this;
        }
    }
}