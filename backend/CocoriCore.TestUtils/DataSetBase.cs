using System;
using System.Threading.Tasks;
using CocoriCore.Common;
using CocoriCore.Messaging;

namespace CocoriCore.TestUtils
{
    public class DataSetBase : IDataSet, IDisposable
    {
        protected IUnitOfWorkFactory _unitOfWorkFactory;
        protected IUnitOfWork _defaultUnitOfWork;

        public DataSetBase(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public virtual T Get<T>()
        {
            if (_defaultUnitOfWork == null)
            {
                _defaultUnitOfWork = _unitOfWorkFactory.NewUnitOfWork();
            }
            return _defaultUnitOfWork.Resolve<T>();
        }

        public virtual async Task<object> ExecuteAsync<TMessage>(params Action<TMessage>[] modifications)
            where TMessage : new()
        {
            using (var unitOfWork = _unitOfWorkFactory.NewUnitOfWork())
            {
                var messageBus = unitOfWork.Resolve<IMessageBus>();
                var message = new TMessage();
                modifications.ApplyOn(message);
                var response = await messageBus.ExecuteAsync(message);
                await unitOfWork.FinishAsync();
                return response;
            }
        }

        public virtual async Task<object> ExecuteAsync(object message)
        {
            using (var unitOfWork = _unitOfWorkFactory.NewUnitOfWork())
            {
                var messageBus = unitOfWork.Resolve<IMessageBus>();
                var response = await messageBus.ExecuteAsync(message);
                await unitOfWork.FinishAsync();
                return response;
            }
        }

        public virtual async Task ExecuteInUnitOfWorkAsync(Func<IUnitOfWork, Task> action)
        {
            using (var unitOfWork = _unitOfWorkFactory.NewUnitOfWork())
            {
                await action(unitOfWork);
                await unitOfWork.FinishAsync();
            }
        }

        public virtual void Dispose()
        {
            _defaultUnitOfWork?.Dispose();
        }
    }
}