using System;
using Autofac;

namespace CocoriCore.TestUtils
{
    public interface IDependencyInjection : IDisposable
    {
        ILifetimeScope RootScope { get; }
    }
}