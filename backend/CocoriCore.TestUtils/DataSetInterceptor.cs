using Castle.DynamicProxy;
using CocoriCore.Common;

namespace CocoriCore.TestUtils
{
    public class DataSetInterceptor<TEntity> : IInterceptor
        where TEntity : IEntity
    {
        private IDataSet _dataSets;
        private TEntity _entity;

        public DataSetInterceptor(IDataSet dataSets, TEntity entity)
        {
            _dataSets = dataSets;
            _entity = entity;
        }

        public void Intercept(IInvocation invocation)
        {
            if (invocation.Method.Name == "get_DataSet")
            {
                invocation.ReturnValue = _dataSets;
            }
            else if (invocation.Method.Name == "get_Entity")
            {
                invocation.ReturnValue = _entity;
            }
            else if (invocation.Method.Name == "set_Entity")
            {
                _entity = (TEntity)invocation.Arguments[0];
            }
            else
            {
                invocation.Proceed();
            }
        }
    }
}
