﻿using System;
using System.Text;

namespace CocoriCore.TestUtils
{
    public struct Tuid
    {
        private byte[] _bytes;

        public Tuid(string value)
        {
            _bytes = ComputeBytes(value);
        }

        public override string ToString()
        {
            return $"{nameof(Tuid)} {new Guid(_bytes).ToString()}";
        }

        private static byte[] ComputeBytes(string value)
        {
            var valueBytes = Encoding.UTF8.GetBytes(value);
            var bytes = new byte[16];
            for (var i = 0; i < 16; i++)
            {
                if (valueBytes.Length > i)
                    bytes[i] = valueBytes[i];
            }
            return bytes;
        }

        public static Guid Guid(string value) => new Guid(ComputeBytes(value));

        public static implicit operator Guid(Tuid tuid) => new Guid(tuid._bytes);
    }
}
