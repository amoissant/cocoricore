﻿using Xunit.Abstractions;
using Microsoft.Extensions.Logging;
using Autofac;
using CocoriCore.Autofac;

namespace CocoriCore.TestUtils
{
    public static class ContainerBuilderExtension
    {
        public static ILoggerFactory ConfigureXunitLogger(this ContainerBuilder builder, 
            ITestOutputHelper output = null, LogLevel level = LogLevel.Debug, string logFilter = null,
            LogEntryCollection logEntries = null)
        {
            logEntries = logEntries ?? new LogEntryCollection();
            output = output ?? new SilentOutput();
            var loggerFactory = LoggerFactory.Create(x => x
                .AddXunit(output, logFilter)
                .AddSpy(logEntries)
                .SetMinimumLevel(level));
            builder.RegisterModule<LoggerModule>();
            builder.RegisterInstance(logEntries);
            builder.RegisterInstance(loggerFactory);
            return loggerFactory;
        }
    }
}

