﻿using CocoriCore.Common;
using System;
using System.Collections.Generic;

namespace CocoriCore.TestUtils
{
    [Obsolete("Set default value into Datasets extensions methods instead")]
    public class DefaultProvider : IDefaultProvider
    {
        private readonly Dictionary<Type, Action<object>> _defaultActions;

        public DefaultProvider(IEnumerable<IDefault> defaults)
        {
            _defaultActions = LoadDefaults(defaults);
        }

        private Dictionary<Type, Action<object>> LoadDefaults(IEnumerable<IDefault> defaults)
        {
            var result = new Dictionary<Type, Action<object>>();
            foreach (var d in defaults)
            {
                foreach (var kvp in d.GetDefaults())
                {
                    if (!result.ContainsKey(kvp.Key))
                    {
                        result[kvp.Key] = kvp.Value;
                    }
                    else
                    {
                        throw new ConfigurationException($"Duplicate default found for {kvp.Key.FullName}.");
                    }
                }
            }
            return result;
        }

        public T GetDefault<T>(Action<T> modifications = null) where T : class, new()
        {
            var message = new T();
            message = ApplyDefaults(message);
            modifications?.Invoke(message);
            return message;
        }

        public T ApplyDefaults<T>(T message)
        {
            if (_defaultActions.TryGetValue(typeof(T), out var action))
            {
                action.Invoke(message);
            }
            return message;
        }
    }
}
