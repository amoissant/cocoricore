using CocoriCore.Common;
using System;

namespace CocoriCore.TestUtils
{
    public static class EntityExtension
    {
        public static TEntity WithId<TEntity>(this TEntity entity) where TEntity : IEntity
        {
            entity.Id = Guid.NewGuid();
            return entity;
        }
    }
}