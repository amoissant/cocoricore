﻿using System.Reflection;

namespace CocoriCore.TestUtils
{
    public static class AssemblyInfo
    {
        public static Assembly Assembly => typeof(AssemblyInfo).Assembly;
    }
}
