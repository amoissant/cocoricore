﻿using CocoriCore.Common;
using CocoriCore.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace CocoriCore.Test
{
    public class FailureDecorator : IRepositoryDecorator
    {
        private RepositoryFailureOptions _configuration;

        public FailureDecorator(RepositoryFailureOptions configuration)
        {
            _configuration = configuration;
        }

        private bool HaveToThrowError(CUDOperation operation, IEntity entity)
        {
            return _configuration.ErrorCondition(operation, entity);
        }
    
        public async Task<IEnumerable<IEntity>> LoadAsync(Type type, Guid id, LoadIdAsync next)
        {
            await Task.Delay(_configuration.DelayInMilliseconds);
            var entities = await next(type, id);
            _configuration.LoadedEntities.AddRange(entities.Select(e => (e.GetType(), e.Id)));
            return entities;
        }

        public async Task<IEnumerable<IEntity>> LoadAsync(Type type, IEnumerable<Guid> ids, LoadIdCollectionAsync next)
        {
            await Task.Delay(_configuration.DelayInMilliseconds);
            var entities = await next(type, ids);
            _configuration.LoadedEntities.AddRange(entities.Select(e => (e.GetType(), e.Id)));
            return entities;
        }

        public async Task<IEnumerable<IEntity>> LoadAsync(Type type, MemberInfo uniqueMember, object value, LoadUniqueAsync next)
        {
            await Task.Delay(_configuration.DelayInMilliseconds);
            var entities = await next(type, uniqueMember, value);
            _configuration.LoadedEntities.AddRange(entities.Select(e => (e.GetType(), e.Id)));
            return entities;
        }

        public IQueryable<TEntity> Query<TEntity>(Query<TEntity> next) where TEntity : IEntity
        {
            var queryable = next();
            return new ExtendedEnumerable<TEntity>(queryable, e =>
            {
                if (_configuration.QueryErrorCondition(typeof(TEntity)))
                {
                    throw new Exception("Fake query error.");
                }
                else
                {
                    _configuration.TotalNbQueryCall++;
                }
                return e;
            });
        }

        public async Task InsertAsync(IEntity entity, InsertAsync next)
        {
            await Task.Delay(_configuration.DelayInMilliseconds);

            if (HaveToThrowError(CUDOperation.CREATE, entity))
            {
                throw new Exception("Fake insert error.");
            }
            else
            {
                await next(entity);
            }
        }

        public async Task UpdateAsync(IEntity entity, UpdateAsync next)
        {
            await Task.Delay(_configuration.DelayInMilliseconds);

            if (HaveToThrowError(CUDOperation.UPDATE, entity))
            {
                throw new Exception("Fake update error.");
            }
            else
            {
                _configuration.TotalNbUpdateCall++;
                await next(entity);
            }
        }

        public async Task DeleteAsync(IEntity entity, DeleteAsync next)
        {
            await Task.Delay(_configuration.DelayInMilliseconds);

            if (HaveToThrowError(CUDOperation.DELETE, entity))
            {
                throw new Exception("Fake delete error.");
            }
            else
            {
                await next(entity);
            }
        }

        public Task FlushAsync(FlushAsync next)
        {
            return next();
        }
    }
}