﻿using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace CocoriCore.TestUtils
{
    public class LogEntryCollection : List<LogEntry>
    {
        /// <summary>
        /// Determines wether <see cref="LogEntryCollection"/> contains entries matching parameters.
        /// </summary>
        /// <param name="level">LogLevel of the log entry</param>
        /// <param name="text">Text to search in log entries's message, case insensitive, can contain wildcard.</param>
        /// <returns>true is an entry match <paramref name="level"/> and <paramref name="text"/> else return false</returns>
        public bool Contains(LogLevel level, string text)
        {
            return this.Any(x => x.Level == level && x.Contains(text));
        }

        /// <summary>
        /// Determines wether <see cref="LogEntryCollection"/> contains entries matching parameters.
        /// </summary>
        /// <param name="text">Text to search in log entries's message, case insensitive, can contain wildcard.</param>
        /// <returns>true is an entry match <paramref name="text"/> else return false</returns>
        public bool Contains(string text)
        {
            return this.Any(x => x.Contains(text));
        }
    }

    public class LogEntry
    {
        public LogEntry(LogLevel level, string category, string message)
        {
            Level = level;
            Category = category;
            Message = message;
        }

        public LogLevel Level { get; }
        public string Category { get; }
        public string Message { get; }

        public override string ToString()
        {
            return $"[{Level}] {Category} - {Message}";
        }

        public bool Contains(string text)
        {
            var pattern = text.Replace(".", "\\.").Replace("*", ".*");
            var regex = new Regex(pattern, RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
            return regex.IsMatch(Message);
        }
    }
}

