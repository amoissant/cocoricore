﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace CocoriCore.TestUtils
{
    public class FakeResponseCookies : IResponseCookies
    {
        public List<(string key, string value, CookieOptions options)> Values { get; }

        public FakeResponseCookies()
        {
            Values = new List<(string, string, CookieOptions)>();
        }

        public void Append(string key, string value)
        {
            Values.Add((key, value, null));
        }

        public void Append(string key, string value, CookieOptions options)
        {
            Values.Add((key, value, options));
        }

        public void Delete(string key)
        {
            throw new NotImplementedException();
        }

        public void Delete(string key, CookieOptions options)
        {
            throw new NotImplementedException();
        }
    }
}