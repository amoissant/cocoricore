<#
.SYNOPSIS
Plug or Unplug Cocoricore's sources instead of nugets package into current project.

.DESCRIPTION
Plug or Unplug Cocoricore's sources instead of nugets package into current project.

.PARAMETER PlugSources
Modify .sln and .csproj to references CocoriCore's sources instead of nugets packages.
Add CocoriCore projects under 'CocoriCore' solution folder for each referenced CocoriCore nuget package.
PackagesReference in .csproj are replaced by ProjectReference pointing to corresponding CocoriCore source project.
Modified files are before backuped into .orig file and can be retored using 'Restore' command.

.PARAMETER PlugPackages
Modify .sln and .csproj to references CocoriCore's packages (default 0.0.0-last) instead of sources.
Remove CocoriCore projects from 'CocoriCore' solution folder.
Replace CocoriCore's ProjectReference in .csproj by PackagesReference pointing to corresponding nuget package.
Modified files are before backuped into .orig file and can be retored using 'Restore' command.

.PARAMETER UpdatePackages
Modify .csproj to update CocoriCore's packages to specified version (default 0.0.0-last)

.PARAMETER Restore
Restore .sln and .csproj backups made using 'PlugSources' command.

.PARAMETER DeleteBackups
Delete all .orig files recursively.

.PARAMETER Version
Say which package version to use when plug or update packages (default is 0.0.0-last).

.PARAMETER CocoriCoreSourcePath
Path tho the folder containing CocoriCore.sln.

#>

[CmdletBinding(DefaultParameterSetName = 'Plug/Sources')]
param (
    [Parameter(ParameterSetName = 'Plug/Sources')]
    [switch]$PlugSources,
    [Parameter(ParameterSetName = 'Plug/Package')]
    [switch]$PlugPackages,
    [Parameter(ParameterSetName = 'Update/Package')]
    [switch]$UpdatePackages,
    [Parameter(ParameterSetName = 'Plug/Package')]
    [Parameter(ParameterSetName = 'Update/Package')]
    [String]$Version = "0.0.0-last",
    [Parameter(ParameterSetName = 'Recover')]
    [switch]$Restore,
    [Parameter(ParameterSetName = 'Recover')]
    [switch]$DeleteBackups,
    # Path to CocoriCore's sources : the directory were the CocoriCore.sln live.
    [string]$CocoriCoreSourcePath,
    [switch]$Dumb = $false
)

function Get-CocoriCoreHome
{
    [CmdletBinding()]
    param ([string]$CocoriCoreHome)

    $suffix="CocoriCore.sln"
    If (Test-Path "$CocoricoreHome/$suffix") {
	$result=(Resolve-Path $CocoriCoreHome)
    }
    ElseIf (Test-Path "$ENV:COCORICORE_HOME/$suffix") {
	$result=(Resolve-Path $ENV:COCORICORE_HOME)
    }
    ElseIf (Test-Path "./$suffix") {
	$result=(Resolve-Path "./")
    }
    Else {
	$result=''
    }

    $result
}

$CocoriCoreSourcePath=(Get-CocoriCoreHome -CocoriCoreHome "$CocoriCoreSourcePath")
$InvalidCocoricoreSourcesPath = (($PlugSources) -And (-not "$CocoriCoreSourcePath"))
$IncompatiblesOptions = (($PlugPackages -Or $PlugSources) -And ($Restore -Or $DeleteBackups))
$NoOptions = ($PSBoundParameters.Values.Count -eq 0)

if ( $NoOptions -Or $IncompatiblesOptions -Or $InvalidCocoricoreSourcesPath) {
    If ($IncompatiblesOptions) {
	Write-Host "Incompatible options specified"
    }
    If ($InvalidCocoricoreSourcesPath) {
	Write-Host "CocoriCore's sources not found on $CocoriCoreSourcePath"
    }
    Get-Help $MyInvocation.MyCommand.Definition
    return
}

# TODO: rewrite with dotnet tool

$currDir = (Get-Item $PSScriptRoot).FullName
# Find topmost  .sln in current directory
$slnFile = Get-ChildItem -Path $currDir "*.sln" | Select-Object -First 1
# Ensure this path is an absolute one
$CocoriCoreSourcePath = (Resolve-Path -Path $CocoriCoreSourcePath)

Write-Host ""
Write-Host "Current directory is : $currDir"
Write-Host "Found solution file : $($slnFile.FullName)"
Write-Host "Path to CocoriCore's sources : $CocoriCoreSourcePath"
Write-Host ""

if ($PlugSources) {
    $packageRegex = '<PackageReference Include="([\w\.]*CocoriCore[\w\.]*)".*/>'
    $packageReplace = "<ProjectReference Include=`"$($CocoriCoreSourcePath)\`$1\`$1.csproj`" />"
    Write-Host "$packageRegex => $packageReplace"

    # Replace PackageReference with ProjectReference in .csproj's files where needed
    Get-ChildItem "*.csproj" -Recurse | Select-String -Pattern $packageRegex -List | Get-ChildItem |
      Foreach-Object {
	  $fileNameRelative = Resolve-Path $_.FullName -Relative
	  if(!(Test-Path "$_.orig")) {
	      Write-Host "Create backup file : $fileNameRelative.orig"
	      If ( -not $Dumb) {
		  Copy-Item -Path $_ -Destination "$_.orig"
	      }
	  }
	  Write-Host "Replace PackageReference by ProjectReference into : $fileNameRelative"
	  If ( -not $Dumb) {
	      (Get-Content $_ -Raw ) -replace $packageRegex, $packageReplace | Set-Content -Path $_ -Encoding utf8 -NoNewline
	  }
      }

    # First make backups of sln's files
    Write-Host "Create backup file : $slnFile.orig"
    If ( -not $Dumb) {
	Copy-Item -Path $slnFile -Destination $slnFile".orig"
    }

    # Add all CocoriCore's projects into solution file
    Get-ChildItem -Path "$CocoriCoreSourcePath\*\*.csproj" |
      ForEach-Object {
	  # Write-Host "Adding $($_.ProjectName) to $(Resolve-Path $_ -Relative)"
	  If ( -not $Dumb) {
	      dotnet sln $slnFile add --solution-folder Cocoricore $_.FullName
	  }
      }
}

if($PlugPackages) {
    $projectRegex = '<ProjectReference Include=".*\\(CocoriCore[\w\.]*)\.csproj" />'
    $packageReplace = "<PackageReference Include=`"`$1`" Version=`"$Version`" />"
    Write-Host "$packageReplace => $projectRegex"

    # Replace PackageReference with ProjectReference in .csproj's files where needed
    Get-ChildItem "*.csproj" -Recurse | Select-String -Pattern $projectRegex -List | Get-ChildItem |
      Foreach-Object {
	  $fileNameRelative = Resolve-Path $_.FullName -Relative
	  if(!(Test-Path $_".orig")) {
	      Write-Host "Create backup file : $fileNameRelative.orig"
	      If ( -not $Dumb) {
		  Copy-Item -Path $_ -Destination "$_.orig"
	      }
	  }
	  Write-Host "Replace ProjectReference by PackageReference into : $fileNameRelative"
	  If ( -not $Dumb) {
	      (Get-Content $_ -Raw ) -replace $projectRegex, $packageReplace | Set-Content -Path $_ -Encoding utf8 -NoNewline
	  }
      }

    # First make backups
    Write-Host "Create backup file : $slnFile.orig"
    If ( -not $Dumb) {
	Copy-Item -Path $slnFile -Destination $slnFile".orig"
    }

    # Remove all CocoriCore's projects from solution file
    Get-ChildItem -Path "$CocoriCoreSourcePath\*\*.csproj" |
      ForEach-Object {
	  # Write-Host "Removing $($_.ProjectName) from $(Resolve-Path $slnFile -Relative)"
	  If ( -not $Dumb) {
	      dotnet sln $slnFile remove $_.FullName
	  }
	  }
      }

if ($UpdatePackages) {
    $packageRegex   = '<PackageReference Include=".*(CocoriCore[\w\.]*)".*/>'
    $packageReplace = "<PackageReference Include=`"`$1`" Version=`"$Version`" />"
    Write-Host "$packageRegex => $packageReplace"

    # Update versions of PackageReference .csproj's files where needed
    Get-ChildItem "*.csproj" -Recurse | Select-String -Pattern $packageRegex -List | Get-ChildItem |
      Foreach-Object {
	  $fileNameRelative = Resolve-Path $_.FullName -Relative
	  if(!(Test-Path $_".orig")) {
	      Write-Host "Create backup file : $fileNameRelative.orig"
	      If ( -not $Dumb) {
		  Copy-Item -Path $_ -Destination "$_.orig"
	      }
	  }
	  Write-Host "Replace ProjectReference by PackageReference into : $fileNameRelative"
	  If ( -not $Dumb) {
	      (Get-Content $_ -Raw ) -replace $packageRegex, $packageReplace | Set-Content -Path $_ -Encoding utf8 -NoNewline
	  }
      }
}

if ($Restore) {
    Write-Host "Restoring .orig files ..."
    If ( -not $Dumb) {
	# Restore .orig's files back to their origin
	Get-ChildItem "*.orig" -Recurse |
	  Foreach-Object {
	      $newName = $_.FullName -replace '\.orig$', ''
	      Move-Item -Path $_ -Destination $newName -Force
	  }
    }
}

if ($DeleteBackups) {
    Write-Host "Deleting .orig files ..."
    If ( -not $Dumb) {
	# trash .orig and .tmp files
	Get-ChildItem -Path . -File -Recurse |
	  Where-Object { $_.Name -match '.*\.(orig|tmp)$' } |
	  Remove-Item -Verbose
    }
}
