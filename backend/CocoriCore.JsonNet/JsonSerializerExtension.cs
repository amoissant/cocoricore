﻿using Newtonsoft.Json;
using System;
using System.IO;

namespace CocoriCore.JsonNet
{
    public static class JsonSerializerExtension
    {
        public static string Serialize(this JsonSerializer serializer, object obj)
        {
            using (var sw = new StringWriter())
            {
                serializer.Serialize(sw, obj);
                return sw.ToString();
            }
        }

        public static T Deserialize<T>(this JsonSerializer serializer, string json)
        {
            using (var sr = new StringReader(json))
            using (var jtr = new JsonTextReader(sr))
            {
                return serializer.Deserialize<T>(jtr);
            }
        }

        public static object Deserialize(this JsonSerializer serializer, string json, Type type)
        {
            using (var sr = new StringReader(json))
            using (var jtr = new JsonTextReader(sr))
            {
                return serializer.Deserialize(jtr, type);
            }
        }

        public static object Deserialize(this JsonSerializer serializer, string json)
        {
            using (var sr = new StringReader(json))
            using (var jtr = new JsonTextReader(sr))
            {
                return serializer.Deserialize(jtr);
            }
        }
    }
}
