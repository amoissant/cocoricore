﻿using System;
using System.Collections.Concurrent;
using System.IO;
using CocoriCore.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace CocoriCore.JsonNet
{
    public class ObjectSerializer : IObjectSerializer
    {
        private readonly ObjectConverter _converter;
        private readonly ConcurrentDictionary<object, JsonSerializer> _cache;

        public ObjectSerializer()
        {
            _cache = new ConcurrentDictionary<object, JsonSerializer>();
            _converter = new ObjectConverter();
        }

        //TODO a mieux tester
        public virtual string Serialize<T>(T obj, bool useCamelCase = true, bool withTypeInfos = true)
        {
            var serializer = GetJsonSerializer(useCamelCase, withTypeInfos);
            if (typeof(T) == typeof(object) && withTypeInfos)
            {
                //TODO pas besoin de type infos si ExpandoObject
                //TODO trasnformer en expando les IEnumerable<KeyValuePair et les IDicionary pour éviter de le faire au niveau de l'appelant
                using (var sw = new StringWriter())
                using (var jw = new JsonTextWriter(sw))
                {
                    _converter.WriteJson(jw, obj, serializer);
                    return sw.ToString();
                }
            }
            else
            {
                return serializer.Serialize(obj);
            }
        }

        public virtual T Deserialize<T>(string json, bool useCamelCase = true, bool withTypeInfos = true)
        {
            try
            {
                var serializer = GetJsonSerializer(useCamelCase, withTypeInfos);
                if (typeof(T) == typeof(object) && withTypeInfos)
                {
                    using (var sr = new StringReader(json))
                    using (var jr = new JsonTextReader(sr))
                    {
                        return (T)_converter.ReadJson(jr, null, null, serializer);
                    }
                }
                else
                {
                    return serializer.Deserialize<T>(json);
                }
            }
            catch (Exception e)
            {
                e.Data["Json"] = json;
                throw;
            }
        }

        private JsonSerializer GetJsonSerializer(bool useCamelCase = true, bool withTypeInfos = true)
        {
            var key = (useCamelCase, withTypeInfos);
            if (!_cache.TryGetValue(key, out var jsonSerializer))
            {
                var settings = GetSettings(useCamelCase, withTypeInfos);
                jsonSerializer = JsonSerializer.Create(settings);
                _cache[key] = jsonSerializer;
            }
            return jsonSerializer;
        }

        private JsonSerializerSettings GetSettings(bool useCamelCase, bool withTypeInfos)
        {
            var settings = new JsonSerializerSettings();
            if (useCamelCase && withTypeInfos)
            {
                settings.ContractResolver = new CamelCaseObjectResolver(_converter);
            }
            else if (!useCamelCase && withTypeInfos)
            {
                settings.ContractResolver = new DefaultObjectResolver(_converter);
            }
            else if (useCamelCase && !withTypeInfos)
            {
                settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                settings.Converters.Add(new DynamicObjectConverter());
            }
            else if (!useCamelCase && !withTypeInfos)
            {
                settings.ContractResolver = new DefaultContractResolver();
                settings.Converters.Add(new DynamicObjectConverter());
            }
            return settings;
        }   
    }
}