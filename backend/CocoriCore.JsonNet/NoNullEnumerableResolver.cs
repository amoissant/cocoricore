﻿using CocoriCore.Reflection.Extension;
using CocoriCore.Types.Extension;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CocoriCore.JsonNet
{
    public class NoNullEnumerableResolver : CamelCasePropertyNamesContractResolver
    {
        protected override JsonContract CreateContract(Type objectType)
        {
            return base.CreateContract(objectType).EnsureNotNullEnumerables();
        }
    }

    public static class JsonContractExtension
    {
        public static JsonContract EnsureNotNullEnumerables(this JsonContract jsonContract)
        {
            jsonContract.OnDeserializedCallbacks.Add((o, c) =>
            {
                var type = o.GetType();
                if (type.IsAssignableTo<IEnumerable>()) return;

                foreach (var memberInfo in o.GetType().GetPropertiesAndFields())
                {
                    var memberType = memberInfo.GetMemberType();
                    if (memberType.IsAssignableTo<IEnumerable>() && memberInfo.CanRead() && memberInfo.CanWrite())
                    {
                        var value = memberInfo.InvokeGetter(o);
                        value = GetEmptyCollectionIfNull(memberType, value);
                        memberInfo.InvokeSetter(o, value);
                    }
                }
            });
            return jsonContract;
        }

        private static object GetEmptyCollectionIfNull(Type objectType, object existingValue)
        {
            if (existingValue == null)
            {
                if (objectType.IsArray)
                    return Array.CreateInstance(objectType.GetElementType(), 0);
                if (objectType.IsConcrete())
                {
                    if (objectType.GetConstructors().Any(x => x.GetParameters().Length == 0))
                        return Activator.CreateInstance(objectType);
                    else
                        return existingValue;
                }
                if (objectType.IsAssignableToGeneric(typeof(IList<>)))
                    return Activator.CreateInstance(typeof(List<>).MakeGenericType(objectType.GetGenericArguments()[0]));
                if (objectType.IsAssignableTo(typeof(IEnumerable)))
                    return Array.CreateInstance(objectType.GetGenericArguments()[0], 0);
            }
            return existingValue;
        }
    }
}
