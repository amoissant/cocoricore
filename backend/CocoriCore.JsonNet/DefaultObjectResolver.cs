﻿using System;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace CocoriCore.JsonNet
{
    public class DefaultObjectResolver : DefaultContractResolver
    {
        private readonly ObjectConverter _converter;

        public DefaultObjectResolver(ObjectConverter objectConverter)
        {
            _converter = objectConverter;
        }

        protected override JsonContract CreateContract(Type objectType)
        {
            var contract = base.CreateContract(objectType);
            if(objectType == typeof(object))
            {
                contract.Converter = _converter;
            }
            return contract;
        }

        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            var property = base.CreateProperty(member, memberSerialization);
            if(property.PropertyType == typeof(object))
            {
                property.Converter = _converter;
            }
            return property;
        }
    }
}
