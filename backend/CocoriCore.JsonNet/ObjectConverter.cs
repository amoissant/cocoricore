﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Reflection;
using Newtonsoft.Json;

namespace CocoriCore.JsonNet
{
    public class ObjectConverter : JsonConverter
    {
        private IEnumerable<Assembly> _assemblies;
        private ConcurrentDictionary<string, Type> _cache;

        public ObjectConverter()
        {
            _assemblies = AppDomain.CurrentDomain.GetAssemblies();
            _cache = new ConcurrentDictionary<string, Type>();
        }

        public override bool CanConvert(Type objectType)
        {
            return true;
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if(reader.TokenType == JsonToken.None)
            {
                reader.Read();
            }
            if (reader.TokenType == JsonToken.Null)
            {
                return null;
            }
            else if (reader.TokenType == JsonToken.StartObject)
            {
                reader.DateParseHandling = DateParseHandling.None;
                var readContent = new List<(JsonToken type, object value)>();
                if (reader.Read()) readContent.Add((reader.TokenType, reader.Value));
                if (reader.Read()) readContent.Add((reader.TokenType, reader.Value));
                if (reader.Read()) readContent.Add((reader.TokenType, reader.Value));
                if (reader.Read()) readContent.Add((reader.TokenType, reader.Value));

                if (readContent.Count == 4 &&
                    readContent[0].type == JsonToken.PropertyName &&
                    Equals(readContent[0].value, "_t") &&
                    readContent[1].type == JsonToken.String &&
                    readContent[1].value != null &&
                    readContent[2].type == JsonToken.PropertyName &&
                    Equals(readContent[2].value, "_v"))
                {
                    var type = GetType((string)readContent[1].value);
                    return serializer.Deserialize(reader, type);
                }
                throw ConstructInvalidOperationException(readContent);
            }
            else
            {
                throw new NotSupportedException($"Unsupported token type {reader.TokenType}.");
            }
        }

        private Type GetType(string name)
        {
            if (!_cache.TryGetValue(name, out Type type))
            {
                type = GetTypeFromAssemblies(name);
                _cache[name] = type;
            }
            return type;
        }

        private Type GetTypeFromAssemblies(string name)
        {
            Type type = null;
            foreach (var assembly in _assemblies)
            {
                type = assembly.GetType(name);
                if (type != null)
                {
                    break;
                }
            }
            if(type == null)
            {
                throw new InvalidOperationException($"Type '{name}' not found among assemblies :\n{string.Join("\n", _assemblies)}");
            }
            return type;
        }

        private InvalidOperationException ConstructInvalidOperationException(List<(JsonToken type, object value)> readContent)
        {
            var exception = new InvalidOperationException("Unable to parse json object, see exception datas for more informations");
            for (var i = 0; i < readContent.Count; i++)
            {
                exception.Data[$"readContent[{i}]"] = $"{readContent[i].type}, {readContent[i].value}";
            }
            return exception;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value != null)
            {
                writer.WriteStartObject();
                writer.WritePropertyName("_t");
                writer.WriteValue(value.GetType().FullName);
                writer.WritePropertyName("_v");
                serializer.Serialize(writer, value);
                writer.WriteEndObject();
            }
            else
            {
                writer.WriteNull();
            }
        }
    }
}