﻿using Newtonsoft.Json;
using System;
using System.Globalization;

namespace CocoriCore.JsonNet
{
    public class TimeSpanConverter : JsonConverter
    {
        private readonly string _timeSpanFormat;

        public TimeSpanConverter(string timeSpanFormat)
        {
            _timeSpanFormat = timeSpanFormat;
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(TimeSpan);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.String)
            {
                var value = reader.Value;
                try
                {
                    return TimeSpan.ParseExact((string)value, _timeSpanFormat, CultureInfo.InvariantCulture);
                }
                catch (Exception e)
                {
                    var lineInfo = reader as IJsonLineInfo;
                    throw new JsonSerializationException($"Error converting value \"{value}\" to type '{objectType}'."
                        + $" Path '{reader.Path}', line {lineInfo.LineNumber}, position {lineInfo.LinePosition}.", e);
                }
            }
            return existingValue;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(((TimeSpan)value).ToString(_timeSpanFormat));
        }
    }
}
