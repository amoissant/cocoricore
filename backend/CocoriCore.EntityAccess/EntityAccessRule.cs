﻿using CocoriCore.Common;
using CocoriCore.Expressions.Extension;
using CocoriCore.Reflection.Extension;
using CocoriCore.Types.Extension;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace CocoriCore.EntityAccess
{
    [DebuggerDisplay("{ToString()}")]
    public class EntityAccessRule : Dictionary<string, object>
    {
        private IEnumerable<MemberInfo> _entityGroupKeyMembers;
        private IEnumerable<MemberInfo> _stepGroupKeyMembers;

        public Enum AuthorizationType { get; set; }
        public Type EntityType { get; set; }
        public MemberInfo AuthorizationMember { get; set; }
        public Type StepType { get; set; }
        public LambdaExpression JoinExpression { get; set; }
        public AuthorizationCardinality Cardinality { get; set; }
        public bool HasPrefilter { get; set; }
        public bool AllowNullOrEmpty { get; set; }

        public bool IsDirect => StepType == null;

        private IEnumerable<MemberInfo> EntityGroupKeyMembers
        {
            get
            {
                if (_entityGroupKeyMembers == null)
                {
                    InitGroupKeyMembers();
                }
                return _entityGroupKeyMembers;
            }
        }

        private IEnumerable<MemberInfo> StepGroupKeyMembers
        {
            get
            {
                if (_stepGroupKeyMembers == null)
                {
                    InitGroupKeyMembers();
                }
                return _stepGroupKeyMembers;
            }
        }

        private void InitGroupKeyMembers()
       
        {
            var visitor = new EqualityMemberVisitor(JoinExpression.GetParameter(EntityType), JoinExpression.GetParameter(StepType));
            visitor.Visit(JoinExpression.Body);
            _entityGroupKeyMembers = visitor.Equalities.Select(e => e.DependencyMember);//TODO renommer DependencyMember
            _stepGroupKeyMembers = visitor.Equalities.Select(e => e.ProjectionMember);
        }

        public object GetEntityGroupKey(object entity)
        {
            return new CompositeKey(EntityGroupKeyMembers.Select(m => m.InvokeGetter(entity)));
        }

        public object GetThroughGroupKey(object entity)
        {
            return new CompositeKey(StepGroupKeyMembers.Select(m => m.InvokeGetter(entity)));
        }

        public override string ToString()
        {
            var filterMethod = IsDirect ? $"x.{AuthorizationMember.Name}" : $"{StepType.GetPrettyName()}.{JoinExpression}";
            return $"{EntityType.GetPrettyName()}:{AuthorizationType} -> {filterMethod}";
        }
    }
}
