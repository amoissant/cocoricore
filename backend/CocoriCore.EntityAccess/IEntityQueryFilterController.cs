﻿using CocoriCore.Common;
using System;
using System.Linq;

namespace CocoriCore.EntityAccess
{
    public interface IEntityQueryFilterController
    {
        IQueryable<TEntity> Prefilter<TEntity>(IQueryable<TEntity> queryable, IndexedSets<Enum, object> userAuthorizations);
        bool HasFilterRule<TEntity>();
    }
}
