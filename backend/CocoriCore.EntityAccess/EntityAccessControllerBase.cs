﻿using CocoriCore.Common;
using CocoriCore.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CocoriCore.Collection.Extensions;

namespace CocoriCore.EntityAccess
{
    public abstract class EntityAccessControllerBase : IEntityAccessController
    {
        private readonly IEntityQueryFilterController _queryFilterController;
        private readonly IEntityReadWriteAccessController _readWriteAccessController;
        private readonly IStateStore _stateStore;
        private readonly HashSet<object> _manipulatedEntities;

        public EntityAccessControllerBase(IEntityQueryFilterController queryFilterController,
            IEntityReadWriteAccessController readWriteAccessController, IStateStore stateStore)
        {
            _queryFilterController = queryFilterController;
            _readWriteAccessController = readWriteAccessController;
            _stateStore = stateStore;
            _manipulatedEntities = new HashSet<object>();
        }

        public TEntity NotifyLoad<TEntity>(TEntity entity)
        {
            if (MustRestrictUserAccess())
            {
                _manipulatedEntities.Add(entity);
            }
            return entity;
        }

        public IEnumerable<TEntity> NotifyLoad<TEntity>(IEnumerable<TEntity> entities)
        {
            if (MustRestrictUserAccess())
            {
                _manipulatedEntities.AddRange(entities.Cast<object>());
            }
            return entities;
        }

        public TEntity NotifyInsert<TEntity>(TEntity entity)
        {
            if (MustRestrictUserAccess())
            {
                _manipulatedEntities.Add(entity);
            }
            return entity;
        }

        public TEntity NotifyUpdate<TEntity>(TEntity entity)
        {
            if (MustRestrictUserAccess())
            {
                var previousEntity = _stateStore.GetState(entity).PreviousObject;
                _manipulatedEntities.Add(previousEntity);
                _manipulatedEntities.Add(entity);
            }
            return entity;
        }

        public TEntity NotifyDelete<TEntity>(TEntity entity)
        {
            if (MustRestrictUserAccess())
            {
                var previousEntity = _stateStore.GetState(entity).PreviousObject;
                _manipulatedEntities.Add(previousEntity);
            }
            return entity;
        }

        public IQueryable<TEntity> PreFilter<TEntity>(IQueryable<TEntity> queryable, Enum enabledAuthorizations = null)
        {
            //TODO implémenter les enabledAuthorizations en utilisant enabledAuthorizations.HasFlag(AuthorizationType.Country) 
            //passer les authorizations actives au controller qui va modifier la requête en fonction des rules actives ou pas.
            if (MustRestrictUserAccess())
            {
                if (_queryFilterController.HasFilterRule<TEntity>())
                {
                    var userAuthorizations = GetUserAuthorizations();
                    queryable = _queryFilterController.Prefilter(queryable, userAuthorizations);
                }
                else
                {
                    queryable = new ExtendedEnumerable<TEntity>(queryable, x => { _manipulatedEntities.Add(x); return x; });
                }
            }
            return queryable;
        }

        protected virtual bool MustRestrictUserAccess()
        {
            return true;
        }

        protected abstract IndexedSets<Enum, object> GetUserAuthorizations();

        public async Task CkeckEntitiesAccessForUserAsync()
        {
            if (MustRestrictUserAccess())
            {
                var userAuthorizations = GetUserAuthorizations();
                await _readWriteAccessController.CheckEntitiesAccessForUserAsync(_manipulatedEntities, userAuthorizations);
            }
        }
    }
}
