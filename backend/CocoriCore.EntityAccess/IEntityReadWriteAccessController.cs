﻿using CocoriCore.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CocoriCore.EntityAccess
{
    public interface IEntityReadWriteAccessController
    {
        Task CheckEntitiesAccessForUserAsync(IEnumerable<object> entities, IndexedSets<Enum, object> userAuthorizations);
    }
}
