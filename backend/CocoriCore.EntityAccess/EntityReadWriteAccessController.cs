﻿using CocoriCore.Common;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CocoriCore.Linq.Async;
using CocoriCore.Types.Extension;
using CocoriCore.Reflection.Extension;
using CocoriCore.Expressions.Extension;
using CocoriCore.Collection.Extensions;

namespace CocoriCore.EntityAccess
{
    public class EntityReadWriteAccessController : IEntityReadWriteAccessController
    {
        private readonly EntityAccessOptions _options;
        private readonly IEntityAccessRepository _repository;
        private readonly IMemoryCache _memoryCache;

        public EntityReadWriteAccessController(EntityAccessOptions options, IEntityAccessRepository repository, IMemoryCache memoryCache)
        {
            _options = options;
            _repository = repository;
            _memoryCache = memoryCache;
        }

        public virtual async Task CheckEntitiesAccessForUserAsync(IEnumerable<object> entities, IndexedSets<Enum, object> userAuthorizations)
        {
            foreach(var (type, sameTypeEntities) in GroupEntitiesByType(entities))
            {
                await CheckAuthorizationsForOneEntityType(sameTypeEntities, type, userAuthorizations);
            }
        }

        private IEnumerable<(Type type, IEnumerable<object> entities)> GroupEntitiesByType(IEnumerable<object> entities)
        {
            return entities
                .GroupBy(x => x.GetType())
                .Select(x => (x.Key, x.AsEnumerable()));
        }

        private async Task CheckAuthorizationsForOneEntityType(IEnumerable<object> entities, Type entityType, 
            IndexedSets<Enum, object> userAuthorizations)
        {
            foreach (var authorizationType in userAuthorizations.Keys)
            {
                await CheckAccessRightsAsync(entities, entityType, userAuthorizations, authorizationType);
            }
        }
      
        private async Task CheckAccessRightsAsync(IEnumerable<object> entities, Type entityType, 
            IndexedSets<Enum, object> userAuthorizations, Enum authorizationType)
        {
            var authorizedEntities = await GetAuthorizedEntities(entities, entityType, userAuthorizations, authorizationType);
            if (authorizedEntities.Count < entities.Count())
            {
                string errorMessage = await ConstructUnauthorizedAccessMessageAsync();
                var exception = new ForbiddenAccessException(errorMessage);
                var firstUnauthorizedEntity = entities.First(x => !authorizedEntities.Contains(x));
                exception.Data["AuthorizationType"] = authorizationType;
                exception.Data["UnauthorizedEntityType"] = firstUnauthorizedEntity.GetType().Name;
                exception.Data["UnauthorizedEntity"] = firstUnauthorizedEntity;
                throw exception;
            }
        }

        private async Task<ISet<object>> GetAuthorizedEntities(IEnumerable<object> entities, Type entityType,
            IndexedSets<Enum, object> userAuthorizations, Enum authorizationType)
        {
            var rule = TryGetRule(entityType, authorizationType);
            if(rule == null)
            {
                return new HashSet<object>(entities);
            }
            else if (rule.IsDirect)
            {
                return GetAuthtorizedEntitiesDirectly(entities, userAuthorizations, rule);
            }
            else
            {
                return await GetAuthorizedEntitiesThroughStepAsync(entities, userAuthorizations, rule);
            }
        }

        private EntityAccessRule TryGetRule(Type entityType, Enum authorizationType)
        {
            return _memoryCache.GetCached(this, x => x.TryGetRule_(entityType, authorizationType));
        }

        private EntityAccessRule TryGetRule_(Type entityType, object authorizationType)
        {
            var typeHierarchy = entityType.GetTypeHierarchy();
            foreach (var type in typeHierarchy)
            {
                var rule = _options
                    .Rules
                    .Where(x => x.EntityType == type && Equals(x.AuthorizationType, authorizationType))
                    .SingleOrDefault();
                if (rule != null)
                {
                    return rule;
                }
            }
            return null;
        }

        private ISet<object> GetAuthtorizedEntitiesDirectly(IEnumerable<object> entities, IndexedSets<Enum, object> userAuthorizations, EntityAccessRule rule)
        {
            var authorizedValues = userAuthorizations[rule.AuthorizationType];
            var authorizedEntities = new HashSet<object>();
            foreach (var entity in entities)
            {
                var value = rule.AuthorizationMember.InvokeGetter(entity);
                if (ValueIsAuthorized(value, authorizedValues, rule))
                {
                    authorizedEntities.Add(entity);
                }
            }
            return authorizedEntities;
        }

        private bool ValueIsAuthorized(object value, HashSet<object> authorizedValues, EntityAccessRule rule)
        {
            if (rule.AllowNullOrEmpty)
            {
                var memberType = rule.AuthorizationMember.GetMemberType();
                if (memberType.IsNullable(out var nonNullableType) && value == null)
                {
                    return true;
                }
                else if (_options.EmptyValues.ContainsKey(nonNullableType) && Equals(_options.EmptyValues[nonNullableType], value))
                {
                    return true;
                }
            }
            if (authorizedValues.Contains(value))
            {
                return true;
            }

            return false;
        }

        private async Task<ISet<object>> GetAuthorizedEntitiesThroughStepAsync(IEnumerable<object> entities, 
            IndexedSets<Enum, object> userAuthorizations, EntityAccessRule rule)
        {
            var groupedStepEntities = await LoadStepEntitiesAsync(entities, rule);
            var authorizedEntities = new HashSet<object>();
            foreach (var (entity, stepEntities) in groupedStepEntities)
            {
                if(await EntitiesGiveAuthorizationForRuleAsync(stepEntities, userAuthorizations, rule))
                {
                    authorizedEntities.AddRange(entity);
                }
            }
            return authorizedEntities;
        }

        private async Task<IEnumerable<(object entity, ICollection<object> stepEntities)>> LoadStepEntitiesAsync(
            IEnumerable<object> entities, EntityAccessRule rule)
        {
            var stepParameter = rule.JoinExpression.GetParameter(rule.StepType);
            var predicate = new ExpressionTransformer().GetPredicate(stepParameter, rule.JoinExpression, entities);
            var stepEntities = await _repository
                .Query(rule.StepType)
                .Where(predicate, stepParameter)
                .Cast<object>()
                .ToArrayAsync();
            var indexedStepEntities = stepEntities
                .GroupBy(x => rule.GetThroughGroupKey(x))
                .ToDictionary(x => x.Key, x => x.ToCollection());
            return entities
                .Select(x => (x, indexedStepEntities.GetValueOrEmpty(rule.GetEntityGroupKey(x))));
        }

        private async Task<bool> EntitiesGiveAuthorizationForRuleAsync(ICollection<object> entities, 
            IndexedSets<Enum, object> userAuthorizations, EntityAccessRule rule)
        {
            if (entities.Count == 0)
            {
                return true;
            }
            var authorizedEntities = await GetAuthorizedEntitiesAsync(entities, userAuthorizations, rule.AuthorizationType);
            if (rule.Cardinality == AuthorizationCardinality.AlLeastOne && authorizedEntities.Count > 0)
            {
                return true;
            }
            if (rule.Cardinality == AuthorizationCardinality.All && authorizedEntities.Count == entities.Count)
            {
                return true;
            }
            return false;
        }

        private async Task<ICollection<object>> GetAuthorizedEntitiesAsync(IEnumerable<object> entities, 
            IndexedSets<Enum, object> userAuthorizations, Enum authorizationType)
        {
            var authorizedEntities = new List<object>();
            foreach (var (type, sameTypeEntities) in GroupEntitiesByType(entities))
            {
                var authorizedStepEntities = await GetAuthorizedEntities(sameTypeEntities, type, userAuthorizations, authorizationType);
                authorizedEntities.AddRange(authorizedStepEntities);
            }
            return authorizedEntities;
        }

        protected virtual Task<string> ConstructUnauthorizedAccessMessageAsync()
        {
            return Task.FromResult("Access forbidden : user not allowed to access this resource.");
        }
    }
}
