﻿using System;
using System.Linq;

namespace CocoriCore.EntityAccess
{
    public interface IEntityAccessRepository
    {
        IQueryable Query(Type type);
    }
}
