﻿using CocoriCore.Expressions.Extension;
using System;
using System.Linq.Expressions;

namespace CocoriCore.EntityAccess
{
    public class EntityAccessRuleBuilder<TEntity>
    {
        public EntityAccessOptions Options { get; }

        public EntityAccessRuleBuilder(EntityAccessOptions options)
        {
            Options = options;
        }

        public EntityAccessRuleBuilder<TEntity> By(Enum authorizationType, Expression<Func<TEntity, object>> accessMember, 
            bool hasPrefilter = true, bool allowNullOrEmpty = false)
        {
            var rule = new EntityAccessRule
            {
                AuthorizationMember = accessMember.GetMemberInfo(),
                AuthorizationType = authorizationType,
                EntityType = typeof(TEntity),
                HasPrefilter = hasPrefilter,
                AllowNullOrEmpty = allowNullOrEmpty
            };
            Options.Rules.Add(rule);
            return this;
        }

        public EntityAccessRuleBuilder<TEntity> Through<TStep>(Enum authorizationType, Expression<Func<TEntity, TStep, bool>> joinExpression,
            AuthorizationCardinality cardinality = AuthorizationCardinality.All)
        {
            var rule = new EntityAccessRule
            {
                EntityType = typeof(TEntity),
                StepType = typeof(TStep),
                JoinExpression = joinExpression,
                Cardinality = cardinality,
                AuthorizationType = authorizationType,
                HasPrefilter = false,
            };
            Options.Rules.Add(rule);
            return this;
        }
    }
}
