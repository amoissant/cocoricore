﻿using System;

namespace CocoriCore.EntityAccess
{
    public class EntityAccessOptionsBuilder
    {
        private EntityAccessOptions _options;
        public bool ValidateOptions { get; set; }

        public virtual EntityAccessOptions Options
        {
            get
            {
                if (ValidateOptions)
                {
                    _options.ValidateConfiguration();
                }
                return _options;
            }
        }

        public EntityAccessOptionsBuilder()
        {
            _options = new EntityAccessOptions();
            ValidateOptions = true;
        }

        public EntityAccessRuleBuilder<T> RestrictAccess<T>()
        {
            return new EntityAccessRuleBuilder<T>(_options);
        }

        public void DefineEmptyValue(Type type, object emptyValue)
        {
            _options.EmptyValues[type] = emptyValue;
        }
    }
}
