﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocoriCore.EntityAccess
{
    public interface IEntityAccessController
    {
        Task CkeckEntitiesAccessForUserAsync();
        TEntity NotifyInsert<TEntity>(TEntity entity);
        IEnumerable<TEntity> NotifyLoad<TEntity>(IEnumerable<TEntity> entities);
        TEntity NotifyLoad<TEntity>(TEntity entity);
        TEntity NotifyUpdate<TEntity>(TEntity entity);
        TEntity NotifyDelete<TEntity>(TEntity entity);
        IQueryable<TEntity> PreFilter<TEntity>(IQueryable<TEntity> queryable, Enum enabledAuthorizations = null);
    }
}