﻿using CocoriCore.Common;
using CocoriCore.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace CocoriCore.EntityAccess
{
    public class EntityAccessDecorator : IRepositoryDecorator
    {
        private readonly IEntityAccessController _accessController;

        public EntityAccessDecorator(IEntityAccessController accessController)
        {
            _accessController = accessController;
        }

        public async Task<IEnumerable<IEntity>> LoadAsync(Type type, Guid id, LoadIdAsync next)
        {
            var entities = await next(type, id);
            _accessController.NotifyLoad(entities);
            return entities;
        }

        public async Task<IEnumerable<IEntity>> LoadAsync(Type type, IEnumerable<Guid> ids, LoadIdCollectionAsync next)
        {
            var entities = await next(type, ids);
            _accessController.NotifyLoad(entities);
            return entities;
        }

        public async Task<IEnumerable<IEntity>> LoadAsync(Type type, MemberInfo uniqueMember, object value, LoadUniqueAsync next)
        {
            var entities = await next(type, uniqueMember, value);
            _accessController.NotifyLoad(entities);
            return entities;
        }

        public IQueryable<TEntity> Query<TEntity>(Query<TEntity> next) where TEntity : IEntity
        {
            var queryable = next();
            return _accessController.PreFilter(queryable);
        }

        public async Task InsertAsync(IEntity entity, InsertAsync next)
        {
            _accessController.NotifyInsert(entity);
            await next(entity);
        }

        public async Task UpdateAsync(IEntity entity, UpdateAsync next)
        {
            _accessController.NotifyUpdate(entity);
            await next(entity);
        }

        public async Task DeleteAsync(IEntity entity, DeleteAsync next)
        {
            _accessController.NotifyDelete(entity);
            await next(entity);
        }

        public Task FlushAsync(FlushAsync next)
        {
            return next();
        }
    }
}
