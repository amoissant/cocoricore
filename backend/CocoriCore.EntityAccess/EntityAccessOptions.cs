﻿using CocoriCore.Common;
using CocoriCore.Reflection.Extension;
using CocoriCore.Types.Extension;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CocoriCore.EntityAccess
{
    public class EntityAccessOptions
    {
        public List<EntityAccessRule> Rules { get; }
        public Dictionary<Type, object> EmptyValues { get; }

        public EntityAccessOptions()
        {
            Rules = new List<EntityAccessRule>();
            EmptyValues = new Dictionary<Type, object>
            {
                { typeof(Guid), Guid.Empty },
                { typeof(string), string.Empty }
            };
        }

        public virtual void ValidateConfiguration()
        {
            ValidateEmptyValues();
            ValidateDuplicateRules();
            ValidateStepRules();
        }

        private void ValidateEmptyValues()
        {
            var rulesWithAllowEmpty = Rules.Where(x => x.AllowNullOrEmpty);
            foreach (var rule in rulesWithAllowEmpty)
            {
                var nonNullableMemberType = rule.AuthorizationMember.GetMemberType().UnboxIfNullable();
                if (!EmptyValues.ContainsKey(nonNullableMemberType))
                {
                    var errorMessage = $"Rule {rule} allows empty values but none is defined for type {nonNullableMemberType}.\n" + 
                        $"Define one using builder.{nameof(EntityAccessOptionsBuilder.DefineEmptyValue)}(typeof({nonNullableMemberType.Name}), <empty value>).";
                    throw new ConfigurationException(errorMessage);
                }
            }
        }

        private void ValidateStepRules()
        {
            var dictionary = Rules.ToDictionary(x => (x.EntityType, x.AuthorizationType));
            foreach (var kvp in dictionary)
            {
                var rule = kvp.Value;
                if (!rule.IsDirect && !dictionary.ContainsKey((rule.StepType, rule.AuthorizationType)))
                {
                    var errorMessage = $"Next step rule '{rule.StepType.Name}:{rule.AuthorizationType}' is missing for rule {rule}.'";
                    throw new ConfigurationException(errorMessage);
                }

            }
        }

        private void ValidateDuplicateRules()
        {
            var dictionnary = new Dictionary<(Type, Enum), EntityAccessRule>();
            foreach(var rule in Rules)
            {
                if(dictionnary.ContainsKey((rule.EntityType, rule.AuthorizationType)))
                {
                    var errorMessage = $"Duplicate rules : \n" +
                        $"rule 1 : {dictionnary[(rule.EntityType, rule.AuthorizationType)]}\n" +
                        $"rule 2 : {rule}";
                    throw new ConfigurationException(errorMessage);
                }
                else
                {
                    dictionnary[(rule.EntityType, rule.AuthorizationType)] = rule;
                }
            }
        }
    }
}
