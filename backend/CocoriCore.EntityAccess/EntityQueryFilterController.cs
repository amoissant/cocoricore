﻿using CocoriCore.Common;
using CocoriCore.Expressions.Extension;
using CocoriCore.Types.Extension;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace CocoriCore.EntityAccess
{
    public class EntityQueryFilterController : IEntityQueryFilterController
    {
        private readonly EntityAccessOptions _options;
        private readonly ILogger _logger;
        private readonly IMemoryCache _memoryCache;

        public EntityQueryFilterController(EntityAccessOptions options, IMemoryCache memoryCache, ILogger logger)
        {
            _options = options;
            _logger = logger;
            _memoryCache = memoryCache;
        }

        public virtual bool HasFilterRule<TEntity>()
        {
            //TODO la règle ByNothing() devra retourner false pour HasPreFilter sinon on na va pas contrôler les entités manipulées
            return GetFilterRules(typeof(TEntity)).Any();
        }

        private IEnumerable<EntityAccessRule> GetFilterRules(Type entityType)
        {
            return _memoryCache.GetCached(this, x => x.GetFilterRulesInternal(entityType));
        }

        private IEnumerable<EntityAccessRule> GetFilterRulesInternal(Type entityType)
        {
            return _options
                .Rules
                .Where(x => x.EntityType == entityType && x.HasPrefilter)
                .ToArray();
        }

        public virtual IQueryable<TEntity> Prefilter<TEntity>(IQueryable<TEntity> queryable,
            IndexedSets<Enum, object> authorizations)
        {
            var entityType = typeof(TEntity);
            var parameter = Expression.Parameter(entityType, "x");
            Expression filterExpression = null;
            foreach (var rule in GetFilterRules(entityType))
            {
                var currentRuleExpresion = ConstructRuleFilterExpression(rule, authorizations, parameter);
                filterExpression = filterExpression.AndAlso(currentRuleExpresion);
            }

            _logger.LogDebug("Query<{0}> with prefilter:\n{1}\nAnd user authorizations:\n{2}", 
                typeof(TEntity).Name, filterExpression, authorizations);
            return queryable.Where(filterExpression, parameter);
        }

        private Expression ConstructRuleFilterExpression(EntityAccessRule rule, IndexedSets<Enum, object> userAuthorizations, ParameterExpression parameter)
        {
            try
            {
                var currentRuleExpression = ConstructFilterExpression(rule, userAuthorizations, parameter);
                return currentRuleExpression;
            }
            catch (Exception e)
            {
                e.Data["rule"] = rule;
                throw;
            }
        }

        protected virtual MemberInfo GetFilterMember(EntityAccessRule rule)
        {
            return rule.AuthorizationMember;
        }

        private Expression ConstructFilterExpression(EntityAccessRule rule,
            IndexedSets<Enum, object> userAuthorizations, ParameterExpression parameter)
        {
            var filterMember = GetFilterMember(rule);
            var authorizedKeys = userAuthorizations[rule.AuthorizationType];
            var memberExp = Expression.MakeMemberAccess(parameter, filterMember);

            if (rule.AllowNullOrEmpty)
            {
                return ConstructNullOrEmptyOrContainsExpression(memberExp, authorizedKeys);
            }
            else
            {
                return ConstructNotNullAndContainsExpression(memberExp, authorizedKeys);
            }
        }

        private Expression ConstructNullOrEmptyOrContainsExpression(MemberExpression memberExp, IEnumerable<object> authorizedValues)
        {
            Expression filterExp = null;
            if (memberExp.Type.IsNullable(out Type nonNullableType))
            {
                filterExp = filterExp.OrElse(memberExp.Equal(null));
            }
            if (_options.EmptyValues.ContainsKey(nonNullableType))
            {
                var emptyValue = _options.EmptyValues[nonNullableType];
                filterExp = filterExp.OrElse(memberExp.Equal(emptyValue));
            }
            var containsExp = ConstructContainsExp(memberExp, authorizedValues);
            return filterExp.OrElse(containsExp);
        }   
        
        private Expression ConstructNotNullAndContainsExpression( MemberExpression memberExp, IEnumerable<object> authorizedValues)
        {
            Expression filterExp = null;
            if (memberExp.Type.IsNullable())
            {
                filterExp = filterExp.AndAlso(memberExp.NotEqual(null));
            }
            var containsExpr = ConstructContainsExp(memberExp, authorizedValues);
            return filterExp.AndAlso(containsExpr);
        }

        private Expression ConstructContainsExp(MemberExpression fieldExp, IEnumerable<object> collection)
        {
            //TODO à rendre plus souple et paramétrable concernant la méthode de comparaison à utiliser (In/ContainsText/...)
            var memberType = fieldExp.Type;
            if (memberType.UnboxIfNullable() == typeof(Guid))
            {
                return fieldExp.In(collection);
            }
            else if (memberType == typeof(string))
            {
                return fieldExp.ContainsText(collection);
            }
            else
            {
                throw new NotSupportedException($"{memberType} is not supported as a filter member ({fieldExp.Member.DeclaringType}.{fieldExp.Member.Name}).");
            }
        }
    }
}
