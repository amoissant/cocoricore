﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace CocoriCore.Email
{
    public class FakeEmailService : IEmailService
    {
        private readonly List<MailMessage> _sentEmails;

        public FakeEmailService()
        {
            _sentEmails = new List<MailMessage>();
        }

        public MailMessage LastEmail => SentEmails.Last();
        public IList<MailMessage> SentEmails => _sentEmails;

        public Task SendAsync(MailMessage message)
        {
            _sentEmails.Add(message);
            return Task.CompletedTask;
        }
    }
}
