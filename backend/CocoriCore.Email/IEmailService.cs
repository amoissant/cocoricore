﻿using System.Net.Mail;
using System.Threading.Tasks;

namespace CocoriCore.Email
{
    public interface IEmailService
    {
        Task SendAsync(MailMessage message);
    }
}
