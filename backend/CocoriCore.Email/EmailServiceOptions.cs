﻿namespace CocoriCore.Email
{
    public class EmailServiceOptions
    {
        public string Host { get; set; }
        public string DefaultFrom { get; set; }
        public int? Port { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string RedirectTo { get; set; }
    }
}