﻿using System.Net.Mail;
using System.Threading.Tasks;
using MimeKit;

namespace CocoriCore.Email
{
    public class MailKitEmailService : IEmailService
    {
        private EmailServiceOptions _options;

        public MailKitEmailService(EmailServiceOptions options)
        {
            _options = options;
        }

        public virtual async Task SendAsync(MailMessage message)
        {
            if (!string.IsNullOrWhiteSpace(_options.RedirectTo))
            {
                message.Subject = $"(To : {message.To}) {message.Subject}";
                message.To.Clear();
                message.To.Add(_options.RedirectTo);
            }
            var mimeMessage = BuildMimeMessage(message);
            using (var client = new MailKit.Net.Smtp.SmtpClient())
            {
                client.Connect(_options.Host, _options.Port ?? 0, false);
                if (!string.IsNullOrEmpty(_options.User))
                {
                    client.Authenticate(_options.User, _options.Password);
                }
                await client.SendAsync(mimeMessage);
                client.Disconnect(true);
            }
        }

        private MimeMessage BuildMimeMessage(MailMessage message)
        {
            var mimeMessage = new MimeMessage();
            if (message.From != null)
            {
                mimeMessage.From.Add(new MailboxAddress(message.From.DisplayName, message.From.Address));
            }

            foreach (var mailTo in message.To)
            {
                mimeMessage.To.Add(new MailboxAddress(mailTo.DisplayName, mailTo.Address));
            }

            foreach (var mailBcc in message.Bcc)
            {
                mimeMessage.Bcc.Add(new MailboxAddress(mailBcc.DisplayName, mailBcc.Address));
            }

            mimeMessage.Subject = message.Subject;

            if (message.IsBodyHtml)
            {
                mimeMessage.Body = new TextPart("html") { Text = message.Body };
            }
            else
            {
                mimeMessage.Body = new TextPart("plain") { Text = message.Body };
            }

            return mimeMessage;
        }
    }
}
