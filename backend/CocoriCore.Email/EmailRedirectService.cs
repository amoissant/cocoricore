﻿//using System.Net.Mail;
//using System.Threading.Tasks;

//namespace CocoriCore.Email
//{
//    //TODO rediriger si précisé dans les options et suppr cette classe
//    public class EmailRedirectService : MailKitEmailService
//    {
//        private EmailServiceOptions _options;

//        public EmailRedirectService(EmailServiceOptions options) 
//            : base(options)
//        {
//            _options = options;
//        }

//        public override Task SendAsync(MailMessage message)
//        {
//			if(!string.IsNullOrWhiteSpace(_options.RedirectTo))
//			{
//				message.Subject += $" (To : {message.To})";
//				message.To.Clear();
//				message.To.Add(_options.RedirectTo);
//			}
//            return base.SendAsync(message);
//        }
//    }
//}
