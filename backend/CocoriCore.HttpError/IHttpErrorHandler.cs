﻿using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;

namespace CocoriCore.HttpError
{
    public interface IHttpErrorHandler
    {
        Task HandleAsync(Exception exception, HttpContext context);
        HttpErrorRule GetRule(Exception exception);
        Task<object> BuildResponseAsync(Exception exception, HttpContext context, HttpErrorRule rule);
    }
}
