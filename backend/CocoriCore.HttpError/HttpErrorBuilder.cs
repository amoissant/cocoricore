﻿using System;

namespace CocoriCore.HttpError
{
    public class HttpErrorBuilder<TException> where TException : class
    {
        public HttpErrorOptions Options { get; }
        public HttpErrorRule Rule { get; }

        public HttpErrorBuilder(HttpErrorOptions options, HttpErrorRule rule)
        {
            Options = options;
            Rule = rule;
        }

        public HttpErrorBuilder<TException> Use<TResponse>()
        {
            Rule.ResponseType = typeof(TResponse);
            Rule.StringRepresentation += $".{nameof(Use)}<{typeof(TResponse).Name}>()";
            return this;
        }

        public HttpErrorBuilder<TException> When(Func<TException, bool> condition)
        {
            Rule.Condition = e => condition((TException)e);
            Rule.StringRepresentation += $".{nameof(When)}({nameof(condition)})";
            return this;
        }

        public HttpErrorBuilder<TException> WithStatusCode(float statusCode)
        {
            Rule.StatusCode = statusCode;
            Rule.StringRepresentation += $".{nameof(WithStatusCode)}({statusCode})";
            return this;
        }
    }
}