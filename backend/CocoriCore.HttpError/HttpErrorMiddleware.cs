﻿using CocoriCore.AspNetCore;
using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;

namespace CocoriCore.HttpError
{
    public class HttpErrorMiddleware : IMiddleware
    {
        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            try
            {
                await next(context);
            }
            catch (Exception e)
            {
                await HandleExceptionAsync(e, context);
            }
        }

        private async Task HandleExceptionAsync(Exception exception, HttpContext context)
        {
            try
            {
                var errorHandler = context.GetUnitOfWork().Resolve<IHttpErrorHandler>();
                await errorHandler.HandleAsync(exception, context);
            }
            catch (Exception unexpectedException)
            {
                Console.Error.Write(unexpectedException.ToString());
                context.Response.StatusCode = 500;
                await context.Response.WriteAsync("Unexpected exception occured during error handling, see error output for more informations.");
            }
        }
    }
}