using CocoriCore.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CocoriCore.HttpError
{
    public class HttpErrorOptions : Dictionary<string, object>
    {
        private readonly List<HttpErrorRule> _rules;
        private readonly List<HttpErrorMapper> _mappers;

        public HttpErrorOptions()
        {
            _rules = new List<HttpErrorRule>();
            _mappers = new List<HttpErrorMapper>();
            DefaultRule = new HttpErrorRule(this, typeof(Exception));
            DefaultRule.ResponseType = typeof(ErrorResponse);
            DefaultRule.StatusCode = 500;
            DefaultMapper = new HttpErrorMapper(typeof(Exception), typeof(ErrorResponse), 
                (e, r, u) => Task.FromResult((object)new ErrorResponse(e, r.StatusCode, r.IsDebug)));
        }

        public HttpErrorOptions AddRule(HttpErrorRule rule)
        {
            _rules.Add(rule);
            return this;
        }

        public HttpErrorOptions AddMappers(IEnumerable<HttpErrorMapper> mappers)
        {
            _mappers.AddRange(mappers);
            return this;
        }

        public HttpErrorOptions AddMapper(HttpErrorMapper mapper)
        {
            _mappers.Add(mapper);
            return this;
        }
        public bool IsDebug { get; set; }

        public IList<HttpErrorRule> Rules => _rules;
        public IList<HttpErrorMapper> Mappers => _mappers;
        public HttpErrorRule DefaultRule { get; }
        public HttpErrorMapper DefaultMapper { get; }
    }
}