﻿using CocoriCore.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CocoriCore.HttpError
{
    //TODO uniformiser les noms avec mappings derivation
    public abstract class HttpErrorMapperProviderBase : IHttpErrorMapperProvider
    {
        private readonly List<HttpErrorMapper> _mappers;

        public HttpErrorMapperProviderBase()
        {
            _mappers = new List<HttpErrorMapper>();
        }

        public IEnumerable<HttpErrorMapper> Mappers => _mappers;

        protected void AddMapper<TException, TResponse>(Func<TException, HttpErrorRule, TResponse> mapFunc)
            where TException : class
        {
            var mapper = new HttpErrorMapper(typeof(TException), typeof(TResponse),
                (e, r, u) => Task.FromResult((object)mapFunc(((TException)(object)e), r)));
            _mappers.Add(mapper);
        }

        protected void AddMapper<TException, TResponse>(Func<TException, HttpErrorRule, IUnitOfWork, Task<TResponse>> mapFunc)
            where TException : class
        {
            var mapper = new HttpErrorMapper(typeof(TException), typeof(TResponse),
                async (e, r, u) => await mapFunc((TException)(object)e, r, u));
            _mappers.Add(mapper);
        }
    }
}