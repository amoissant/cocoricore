﻿using CocoriCore.Common;
using System;
using System.Threading.Tasks;

namespace CocoriCore.HttpError
{
    public class HttpErrorMapper
    {
        public HttpErrorMapper(Type exceptionType, Type responseType, Func<Exception, HttpErrorRule, IUnitOfWork, Task<object>> mapFunc)
        {
            ExceptionType = exceptionType;
            ResponseType = responseType;
            MapFunc = mapFunc;
        }

        public Type ExceptionType { get; }
        public Type ResponseType { get; }
        public Func<Exception, HttpErrorRule, IUnitOfWork, Task<object>> MapFunc { get; }
    }
}