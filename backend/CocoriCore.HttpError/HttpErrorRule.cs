﻿using System;
using System.Collections.Generic;

namespace CocoriCore.HttpError
{
    public class HttpErrorRule : Dictionary<string, object>
    {
        private readonly HttpErrorOptions _options; 
        public Type ExceptionType { get; }
        public Type ResponseType { get; set; }
        public float StatusCode { get; set; }
        public bool IsDebug => _options.IsDebug;
        public string StringRepresentation { get; set; }
        public Func<object, bool> Condition { get; set; }

        public HttpErrorRule(HttpErrorOptions options, Type targetType)
        {
            _options = options;
            ExceptionType = targetType;
        }

        public override string ToString()
        {
            return StringRepresentation;
        }
    }
}