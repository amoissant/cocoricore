﻿using System.Collections.Generic;

namespace CocoriCore.HttpError
{
    public interface IHttpErrorMapperProvider
    {
        IEnumerable<HttpErrorMapper> Mappers { get; }
    }
}