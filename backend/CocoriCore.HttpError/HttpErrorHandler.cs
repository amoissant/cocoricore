﻿using CocoriCore.AspNetCore;
using CocoriCore.Common;
using CocoriCore.Types.Extension;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CocoriCore.HttpError
{
    public class HttpErrorHandler : IHttpErrorHandler
    {
        private readonly HttpErrorOptions _options;
        private readonly IMemoryCache _memoryCache;
        private readonly IUnitOfWork _unitOfWork;
        private JsonSerializer _jsonSerializer;//TODO faire une interface pour les serializer pour se découpler de NewtonSoft
        private readonly ILogger _logger;

        public HttpErrorHandler(HttpErrorOptions options, IMemoryCache memoryCache, IUnitOfWork iunitOfWork, 
            JsonSerializer jsonSerializer, ILogger logger)
        {
            _options = options;
            _memoryCache = memoryCache;
            _jsonSerializer = jsonSerializer;
            _unitOfWork = iunitOfWork;
            _logger = logger;
        }

        public virtual async Task HandleAsync(Exception exception, HttpContext context)
        {
            if (context.Response.HasStarted)
            {
                _logger.LogError(exception);
                return;
            }
            var rule = GetRule(exception);
            exception.EnsureHasId();
            var response = await BuildResponseAsync(exception, context, rule);
            context.Response.ContentType = "application/json; charset=utf-8";
            context.Response.StatusCode = (int)rule.StatusCode;
            context.SetErrorId(exception.GetId());

            //TODO factoriser code avec HttpResponseWriterBase, faire apparaitre le json serializer capable de read/write async
            using (var ms = new MemoryStream())
            {
                using (var sw = new StreamWriter(ms, new UTF8Encoding(false), 1024, true))
                {
                    _jsonSerializer.Serialize(sw, response);
                }
                ms.Position = 0;

                await ms.CopyToAsync(context.Response.Body);
            }
        }

        public virtual HttpErrorRule GetRule(Exception exception)
        {
            var matchingRules = _memoryCache.GetCached(this, x => x.GetRuleInternal(exception.GetType()));
            var resultRules = matchingRules.Where(x => x.Condition == null || x.Condition(exception));
            if (resultRules.Any(x => x.Condition != null))
            {
                resultRules = resultRules.Where(x => x.Condition != null);
            }
            if (resultRules.Count() == 1)
            {
                return resultRules.Single();
            }
            throw BuildConfigurationException(exception, matchingRules, resultRules);
        }

        private IEnumerable<HttpErrorRule> GetRuleInternal(Type exceptionType)
        {
            List<HttpErrorRule> matchingRules = new List<HttpErrorRule>();

            foreach (var type in exceptionType.GetTypeHierarchy(HierarchyOrder.Self_Interfaces_Base))
            {
                var rules = _options.Rules.Where(x => x.ExceptionType == type);
                if (rules.Any())
                {
                    matchingRules.AddRange(rules);
                    break;
                }
            }
            if (!matchingRules.Any())
            {
                matchingRules.Add(_options.DefaultRule);
            }
            return matchingRules;
        }

        private Exception BuildConfigurationException(Exception exception, IEnumerable<HttpErrorRule> matchingRules, IEnumerable<HttpErrorRule> resultRules)
        {
            var exceptionType = exception.GetType().Name;
            var whenMethod = $"builder.{nameof(HttpErrorOptionsBuilder.For)}<{exceptionType}>().{nameof(HttpErrorBuilder<object>.When)}(<condition>)";
            var helpMessage = $"for exception {exceptionType} and matching acceptance conditions.\n" +
                $"Review your configuration to ensure at least one rule is defined for this type.\n" +
                $"If several rules can match this type please use {whenMethod} to specify which rule to apply.\n" +
                $"Matching rules for {exceptionType} :\n{string.Join("\n", matchingRules)}\n" +
                $"Matching rules after applying conditions :\n{string.Join("\n", resultRules)}\n";
            if (!resultRules.Any())
            {
                return new ConfigurationException("No rule found " + helpMessage);
            }
            else
            {
                return new ConfigurationException($"Several rules found " + helpMessage);
            }
        }

        public virtual async Task<object> BuildResponseAsync(Exception exception, HttpContext context, HttpErrorRule rule)
        {
            var mapper = GetMapper(exception);
            var response = await mapper.MapFunc(exception, rule, _unitOfWork);
            if (context.IsOData())
            {
                response = BuildODataErrorResponse(exception, rule, response);
            }
            return response;
        }

        public virtual HttpErrorMapper GetMapper(Exception exception)
        {
            var exceptionType = exception.GetType();
            return _memoryCache.GetCached(this, x => x.GetMapper(exception.GetType()));
        }

        private HttpErrorMapper GetMapper(Type exceptionType)
        {
            foreach (var type in exceptionType.GetTypeHierarchy(HierarchyOrder.Self_Interfaces_Base))
            {
                var mapper = _options.Mappers.SingleOrDefault(x => x.ExceptionType == type);
                if (mapper != null)
                {
                    return mapper;
                }
            }
            return _options.DefaultMapper;
        }

        private object BuildODataErrorResponse(Exception exception, HttpErrorRule rule, object response)
        {
            response = new
            {
                error = new
                {
                    code = rule.StatusCode,
                    message = exception.Message,
                    innererror = response
                }
            };
            return response;
        }
    }
}
