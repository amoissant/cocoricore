using CocoriCore.Common;
using CocoriCore.Types.Extension;
using System;
using System.Linq;
using System.Threading.Tasks;
using CocoriCore.Collection.Extensions;

namespace CocoriCore.HttpError
{
    public class HttpErrorOptionsBuilder
    {
        private readonly HttpErrorOptions _options;

        public HttpErrorOptionsBuilder()
        {
            _options = new HttpErrorOptions();
        }

        public HttpErrorOptions Options
        {
            get
            {
                if(!_options.Rules.Where(x => x.Condition == null).IsUnique(x => x.ExceptionType, out var duplicateRuleType))
                {
                    throw new ConfigurationException($"Duplicate rule for {duplicateRuleType}.\n" + 
                        "Please review your configuration to define one and only one rule for each exception type.");
                }
                if (!_options.Mappers.IsUnique(x => x.ExceptionType, out var duplicateMapperType))
                {
                    throw new ConfigurationException($"Duplicate mapper for {duplicateMapperType}.\n" + 
                        "Please review your configuration to define one and only one mapper for each exception type.");
                }
                foreach(var rule in _options.Rules)
                {
                    var exceptionTypeHierachy = rule.ExceptionType.GetTypeHierarchy();
                    var mappers = _options.Mappers.Append(_options.DefaultMapper);
                    if (!mappers.Any(x => exceptionTypeHierachy.Contains(x.ExceptionType) && rule.ResponseType == x.ResponseType))
                    {
                        throw new ConfigurationException($"No mapper found for rule {rule}.\n" +
                            $"Please review your configuration to define a mapper from {rule.ExceptionType} to {rule.ResponseType}.");
                    }
                }
                return _options;
            }
        }

        public HttpErrorOptionsBuilder SetIsDebug(bool isDebug)
        {
            _options.IsDebug = isDebug;
            return this;
        }

        public HttpErrorOptionsBuilder AddMappersFromProvider<TProvider>() where TProvider : IHttpErrorMapperProvider, new()
        {
            var mapperProvider = Activator.CreateInstance<TProvider>();
            _options.AddMappers(mapperProvider.Mappers);
            return this;
        }

        public HttpErrorOptionsBuilder AddMapper<TException, TResponse>(Func<TException, HttpErrorRule, TResponse> mapFunc)
            where TException : class
        {
            var mapper = new HttpErrorMapper(typeof(TException), typeof(TResponse), 
                (e, r, u) => Task.FromResult((object)mapFunc(((TException)(object)e), r)));
            return AddMapper(mapper);
        }

        public HttpErrorOptionsBuilder AddMapper<TException, TResponse>(Func<TException, HttpErrorRule, IUnitOfWork, Task<TResponse>> mapFunc)
            where TException : class
        {
            var mapper = new HttpErrorMapper(typeof(TException), typeof(TResponse), 
                async (e, r, u) => await mapFunc((TException)(object)e, r, u));
            return AddMapper(mapper);
        }

        public HttpErrorOptionsBuilder AddMapper(HttpErrorMapper mapper)
        {
            _options.AddMapper(mapper);
            return this;
        }

        public HttpErrorBuilder<TException> For<TException>() where TException : class
        {
            var rule = new HttpErrorRule(_options, typeof(TException));
            rule.StringRepresentation += $"{nameof(For)}<{typeof(TException).Name}>()";
            _options.AddRule(rule);
            return new HttpErrorBuilder<TException>(_options, rule);
        }
    }
}