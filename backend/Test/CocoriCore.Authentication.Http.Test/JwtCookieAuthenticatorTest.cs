﻿using CocoriCore.Clock;
using CocoriCore.Common;
using CocoriCore.Jwt;
using CocoriCore.TestUtils;
using FluentAssertions;
using Jose;
using System;
using Xunit;

namespace CocoriCore.Authentication.Http.Test
{
    public class JwtCookieAuthenticatorTest
    {
        private readonly JwtAuthenticator _authenticator;
        private readonly ITokenService _tokenService;
        private readonly FakeClock _clock;

        public JwtCookieAuthenticatorTest()
        {
            var tokenConfiguration = new TokenConfiguration()
                .UseSecret("secret")
                .UseAlgorithm(JwsAlgorithm.HS256)
                .SetTokenDuration("accessToken", TimeSpan.FromMinutes(5));
            var serializer = new JwtSerializer(nameof(AccessClaims.ExpireAt));
            _clock = new FakeClock();
            _tokenService = new TokenService(_clock, tokenConfiguration, serializer);
            _authenticator = new JwtAuthenticator(_tokenService);
        }

        [Fact]
        public void ReturnFalseWhenNoCookie()
        {
            var httpRequest = new FakeHttpRequest();

            _authenticator.TryAuthenticate(httpRequest, LocationsFlags.Cookie).Should().Be(false);

            _authenticator.IsAuthenticated.Should().Be(false);
        }

        [Fact]
        public void AuthenticationExceptionWhenNoCookie()
        {
            var httpRequest = new FakeHttpRequest();

            Action action = () => _authenticator.Authenticate(httpRequest, LocationsFlags.Cookie);

            action.Should().Throw<AuthenticationException>();
        }

        [Fact]
        public void ReturnFalseWhenCookieContainsInvalidValue()
        {
            var httpRequest = new FakeHttpRequest();
            httpRequest.Cookies = new FakeRequestCookieCollection { { "invalid-cookie-name", "" } };

            _authenticator.TryAuthenticate(httpRequest, LocationsFlags.Cookie).Should().Be(false);
        }

        [Fact]
        public void AuthenticationExceptionWhenCookieContainsInvalidValue()
        {
            var httpRequest = new FakeHttpRequest();
            httpRequest.Cookies = new FakeRequestCookieCollection { { "invalid-cookie-name", "" } };

            Action action = () => _authenticator.Authenticate(httpRequest, LocationsFlags.Cookie);
            
            action.Should().Throw<AuthenticationException>();
        }

        [Theory]
        [InlineData("")]
        [InlineData("not.valid.jwt")]
        [InlineData("zzzzzzzzzzz")]
        public void ExceptionWhenTokenIsInvalid(string cookieValue)
        {
            var httpRequest = new FakeHttpRequest();
            httpRequest.Cookies = new FakeRequestCookieCollection { { "jwt-1", cookieValue } };

            Action action1 = () => _authenticator.TryAuthenticate(httpRequest, LocationsFlags.Cookie);
            Action action2 = () => _authenticator.Authenticate(httpRequest, LocationsFlags.Cookie);

            _authenticator.IsAuthenticated.Should().Be(false);
            action1.Should().Throw<InvalidTokenException>();
            action2.Should().Throw<InvalidTokenException>();
        }

        [Theory]
        [InlineData("jwt-whatever")]
        [InlineData("jwt")]
        public void GetClaimsWhenTryAuthenticationHeaderContainsValidJWT(string cookieName)
        {
            _clock.Now = new DateTime(2020, 6, 23, 18, 30, 10);
            var payload = new AccessClaims { UserId = Tuid.Guid("1"), ExpireAt = new DateTime(2020, 6, 23, 18, 30, 59) };
            var jwt = _tokenService.GenerateToken(payload);

            var httpRequest = new FakeHttpRequest();
            httpRequest.Cookies = new FakeRequestCookieCollection { { cookieName, jwt } };

            _authenticator.TryAuthenticate(httpRequest, LocationsFlags.Cookie).Should().Be(true);

            _authenticator.IsAuthenticated.Should().Be(true);
            _authenticator.GetClaims(typeof(AccessClaims)).Should().BeEquivalentTo(payload);
        }

        [Theory]
        [InlineData("jwt-whatever")]
        [InlineData("jwt")]
        public void GetClaimsWhenAuthenticationHeaderContainsValidJWT(string cookieName)
        {
            _clock.Now = new DateTime(2020, 6, 23, 18, 30, 10);
            var payload = new AccessClaims { UserId = Tuid.Guid("1"), ExpireAt = new DateTime(2020, 6, 23, 18, 30, 59) };
            var jwt = _tokenService.GenerateToken(payload);

            var httpRequest = new FakeHttpRequest();
            httpRequest.Cookies = new FakeRequestCookieCollection { { cookieName, jwt } };

            _authenticator.Authenticate(httpRequest, LocationsFlags.Cookie);

            _authenticator.IsAuthenticated.Should().Be(true);
            _authenticator.GetClaims(typeof(AccessClaims)).Should().BeEquivalentTo(payload);
        }

        [Fact]
        public void ExceptionWhenGetClaimsWhileNotAuthenticated()
        {
            _authenticator.IsAuthenticated.Should().Be(false);

            Action action = () => _authenticator.GetClaims(typeof(AccessClaims));

            action.Should().Throw<InvalidOperationException>();
        }

        [Fact]
        public void AuthenticateUsingCookieIfNoAuthorizationHeader()
        {
            _clock.Now = new DateTime(2020, 6, 23, 18, 30, 10);
            var payload = new AccessClaims { UserId = Tuid.Guid("1"), ExpireAt = new DateTime(2020, 6, 23, 18, 30, 59) };
            var jwt = _tokenService.GenerateToken(payload);

            var httpRequest = new FakeHttpRequest();
            httpRequest.Cookies = new FakeRequestCookieCollection { { "jwt", jwt } };

            Action action1 = () => _authenticator.Authenticate(httpRequest, LocationsFlags.AuthorizationHeader);
            Action action2 = () => _authenticator.Authenticate(httpRequest, LocationsFlags.AuthorizationHeader | LocationsFlags.Cookie);

            action1.Should().Throw<AuthenticationException>();
            action2.Should().NotThrow<Exception>();
        }

        public class AccessClaims
        {
            public Guid UserId { get; set; }
            public DateTime ExpireAt { get; set; }
        }
    }
}