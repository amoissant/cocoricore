﻿using FluentAssertions;
using System;
using Xunit;
using CocoriCore.TestUtils;
using Microsoft.AspNetCore.Http;
using CocoriCore.Common;
using CocoriCore.Security;
using static CocoriCore.Authentication.Http.ApiKeyAuthOptions;

namespace CocoriCore.Authentication.Http.Test
{
    public class ApiKeyAuthenticatorTest
    {
        private IHashService _hashService;
        private string _apiKey;
        private string _apiKeyHash;

        public ApiKeyAuthenticatorTest()
        {
            _hashService = new HashService();
            _apiKey = "1234";
            _apiKeyHash = _hashService.Hash(_apiKey);
        }

        [Fact]
        public void ExceptionWhenOptionIsNull()
        {
            Action action = () => new ApiKeyAuthenticator(_hashService, null);

            var exception = action.Should().Throw<ArgumentNullException>().Which.ParamName.Should().Be("options");
        }

        #region apiKey query string
        [Theory]
        [InlineData("/admin", "/admin/test")]
        [InlineData("admin", "/admin/test")]
        [InlineData("/admin", "/root/administration/test")]
        [InlineData("/admin*st", "/admin/test/toto")]
        public void NoExceptionWhenApiKeyQueryStringMatchHash(string restrictedPath, string requestPath)
        {
            var options = new ApiKeyAuthOptions();
            options.Path = restrictedPath;
            options.ApiKeyHash = _apiKeyHash;
            options.LocationType = ApiKeyLocation.QueryString;
            options.LocationName = "apiKey";

            var authenticator = new ApiKeyAuthenticator(_hashService, options);

            var httpRequest = new FakeHttpRequest();
            httpRequest.Path = requestPath;
            httpRequest.QueryString = new QueryString($"?apiKey={_apiKey}");

            Action action = () => authenticator.CheckApiKeyWhenMatchOptionPath(httpRequest);

            action.Should().NotThrow<Exception>();
        }

        [Fact]
        public void NoExceptionWhenPathIsNotUnderApiKeyQueryStringRestriction()
        {
            var options = new ApiKeyAuthOptions();
            options.Path = "/admin";
            options.ApiKeyHash = _apiKeyHash;
            options.LocationType = ApiKeyLocation.QueryString;
            options.LocationName = "apiKey";

            var authenticator = new ApiKeyAuthenticator(_hashService, options);

            var httpRequest = new FakeHttpRequest();
            httpRequest.Path = "/test";
            httpRequest.QueryString = new QueryString($"?apiKey=zzzzz");

            Action action = () => authenticator.CheckApiKeyWhenMatchOptionPath(httpRequest);

            action.Should().NotThrow<Exception>();
        }

        [Theory]
        [InlineData("?otherValue=1")]
        [InlineData("?apiKey=&otherValue=1")]
        [InlineData("?apiKey= &otherValue=1")]
        public void ExceptionWhenApiKeyQueryStringIsEmpty(string queryString)
        {
            var options = new ApiKeyAuthOptions();
            options.Path = "/admin";
            options.ApiKeyHash = _apiKeyHash;
            options.LocationType = ApiKeyLocation.QueryString;
            options.LocationName = "apiKey";

            var authenticator = new ApiKeyAuthenticator(_hashService, options);

            var httpRequest = new FakeHttpRequest();
            httpRequest.Path = "/admin/test";
            httpRequest.QueryString = new QueryString(queryString);

            Action action = () => authenticator.CheckApiKeyWhenMatchOptionPath(httpRequest);

            action.Should().Throw<AuthenticationException>().WithMessage("Request api key is null or empty.");
        }

        [Theory]
        [InlineData("/admin", "/admin/test")]
        [InlineData("admin", "/admin/test")]
        [InlineData("/admin", "/root/administration/test")]
        [InlineData("/admin*st", "/admin/test/toto")]
        public void ExceptionWhenApiKeyQueryStringDoesntMatchHash(string restrictedPath, string requestPath)
        {
            var options = new ApiKeyAuthOptions();
            options.Path = restrictedPath;
            options.ApiKeyHash = _apiKeyHash;
            options.LocationType = ApiKeyLocation.QueryString;
            options.LocationName = "apiKey";

            var authenticator = new ApiKeyAuthenticator(_hashService, options);

            var httpRequest = new FakeHttpRequest();
            httpRequest.Path = requestPath;
            httpRequest.QueryString = new QueryString("?apiKey=abcd");

            Action action = () => authenticator.CheckApiKeyWhenMatchOptionPath(httpRequest);

            action.Should().Throw<AuthenticationException>().WithMessage("Request api key doesn't match correct hash.");
        }
        #endregion

        #region apiKey header
        [Theory]
        [InlineData("/admin", "/admin/test")]
        [InlineData("admin", "/admin/test")]
        [InlineData("/admin", "/root/administration/test")]
        [InlineData("/admin*st", "/admin/test/toto")]
        public void NoExceptionWhenApiKeyHeaderMatchHash(string restrictedPath, string requestPath)
        {
            var options = new ApiKeyAuthOptions();
            options.Path = restrictedPath;
            options.ApiKeyHash = _apiKeyHash;
            options.LocationType = ApiKeyLocation.Header;
            options.LocationName = "x-api-key";

            var authenticator = new ApiKeyAuthenticator(_hashService, options);

            var httpRequest = new FakeHttpRequest();
            httpRequest.Path = requestPath;
            httpRequest.Headers["X-API-Key"] = _apiKey;

            Action action = () => authenticator.CheckApiKeyWhenMatchOptionPath(httpRequest);

            action.Should().NotThrow<Exception>();
        }

        [Fact]
        public void NoExceptionWhenPathIsNotUnderApiKeyHeaderRestriction()
        {
            var options = new ApiKeyAuthOptions();
            options.Path = "/admin";
            options.ApiKeyHash = _apiKeyHash;
            options.LocationType = ApiKeyLocation.Header;
            options.LocationName = "x-api-key";

            var authenticator = new ApiKeyAuthenticator(_hashService, options);

            var httpRequest = new FakeHttpRequest();
            httpRequest.Path = "/test";
            httpRequest.Headers["X-API-Key"] = "zzzzz";

            Action action = () => authenticator.CheckApiKeyWhenMatchOptionPath(httpRequest);

            action.Should().NotThrow<Exception>();
        }

        [Fact]
        public void ExceptionWhenApiKeyHeaderIsEmpty()
        {
            var options = new ApiKeyAuthOptions();
            options.Path = "/admin";
            options.ApiKeyHash = _apiKeyHash;
            options.LocationType = ApiKeyLocation.Header;
            options.LocationName = "x-api-key";

            var authenticator = new ApiKeyAuthenticator(_hashService, options);

            var httpRequest = new FakeHttpRequest();
            httpRequest.Path = "/admin/test";
            httpRequest.Headers["X-API-Key"] = "";

            Action action = () => authenticator.CheckApiKeyWhenMatchOptionPath(httpRequest);

            action.Should().Throw<AuthenticationException>().WithMessage("Request api key is null or empty.");
        }

        [Theory]
        [InlineData("/admin", "/admin/test")]
        [InlineData("admin", "/admin/test")]
        [InlineData("/admin", "/root/administration/test")]
        [InlineData("/admin*st", "/admin/test/toto")]
        public void ExceptionWhenApiKeyHeaderDoesntMatchHash(string restrictedPath, string requestPath)
        {
            var options = new ApiKeyAuthOptions();
            options.Path = restrictedPath;
            options.ApiKeyHash = _apiKeyHash;
            options.LocationType = ApiKeyLocation.Header;
            options.LocationName = "x-api-key";

            var authenticator = new ApiKeyAuthenticator(_hashService, options);

            var httpRequest = new FakeHttpRequest();
            httpRequest.Path = requestPath;
            httpRequest.Headers["X-API-Key"] = "abcd";

            Action action = () => authenticator.CheckApiKeyWhenMatchOptionPath(httpRequest);

            action.Should().Throw<AuthenticationException>().WithMessage("Request api key doesn't match correct hash.");
        }

        [Fact]
        public void ReturnTrueWhenApiKeyHeaderIsValid()
        {
            var options = new ApiKeyAuthOptions();
            options.ApiKeyHash = _apiKeyHash;
            options.LocationType = ApiKeyLocation.Header;
            options.LocationName = "x-api-key";

            var authenticator = new ApiKeyAuthenticator(_hashService, options);

            var httpRequest = new FakeHttpRequest();
            httpRequest.Headers["X-API-Key"] = _apiKey;

            var result = authenticator.TryAuthenticateUsingApiKey(httpRequest);

            result.Should().Be(true);
        }

        [Fact]
        public void ReturnTrueWhenApiKeyQueryParameterIsValid()
        {
            var options = new ApiKeyAuthOptions();
            options.ApiKeyHash = _apiKeyHash;
            options.LocationType = ApiKeyLocation.QueryString;
            options.LocationName = "apiKey";

            var authenticator = new ApiKeyAuthenticator(_hashService, options);

            var httpRequest = new FakeHttpRequest();
            httpRequest.QueryString = new QueryString($"?apiKey={_apiKey}");

            var result = authenticator.TryAuthenticateUsingApiKey(httpRequest);

            result.Should().Be(true);
        }

        [Fact]
        public void ReturnFalseWhenApiKeyHeaderIsEmpty()
        {
            var options = new ApiKeyAuthOptions();
            options.ApiKeyHash = _apiKeyHash;
            options.LocationType = ApiKeyLocation.Header;
            options.LocationName = "x-api-key";

            var authenticator = new ApiKeyAuthenticator(_hashService, options);

            var httpRequest = new FakeHttpRequest();
            httpRequest.Headers["X-API-Key"] = "";

            var result = authenticator.TryAuthenticateUsingApiKey(httpRequest);

            result.Should().Be(false);
        }

        [Fact]
        public void ThrowExceptionWhenApiKeyHeaderDoesntMatchHash()
        {
            var options = new ApiKeyAuthOptions();
            options.ApiKeyHash = _apiKeyHash;
            options.LocationType = ApiKeyLocation.Header;
            options.LocationName = "x-api-key";

            var authenticator = new ApiKeyAuthenticator(_hashService, options);

            var httpRequest = new FakeHttpRequest();
            httpRequest.Headers["X-API-Key"] = "abcd";

            Action action = () => authenticator.TryAuthenticateUsingApiKey(httpRequest);

            action.Should().Throw<AuthenticationException>().WithMessage("Request api key doesn't match correct hash.");
        }
        #endregion
    }
}
