﻿using FluentAssertions;
using System;
using Xunit;
using CocoriCore.TestUtils;
using CocoriCore.Common;
using System.Net;

namespace CocoriCore.Authentication.Http.Test
{
    public class IpFilterTest
    {
        [Fact]
        public void ExceptionWhenOptionIsNull()
        {
            Action action = () => new IpFilter(null);

            var exception = action.Should().Throw<ArgumentNullException>().Which.ParamName.Should().Be("options");
        }

        #region ip filtering
        [Fact]
        public void NoIpAllowedWhenAllowedIpsIsNull()
        {
            var options = new IpFilterOptions();
            options.Path = "/admin";
            options.AllowedIps = null;

            var ipFilter = new IpFilter(options);

            var httpContext = new FakeHttpContext();
            httpContext.Request.Path = "/admin";
            httpContext.Connection.RemoteIpAddress = IPAddress.Parse("81.46.77.117");

            Action action = () => ipFilter.CheckIpWhenMatchOptionPath(httpContext);

            action.Should().Throw<ForbiddenAccessException>();
        }

        [Theory]
        [InlineData("235.1.2.4", "235.1.2.4")]
        [InlineData("235.1.2.4", "235.1.2.4, 235.1.2.5")]
        [InlineData("235.1.2.5", "235.1.2.4, 235.1.2.5")]
        [InlineData("235.1.2.4", "235.1.2.*")]
        public void NoExceptionWhenIpAllowed(string requestIp, string allowedIps)
        {
            var options = new IpFilterOptions();
            options.Path = "/admin";
            options.AllowedIps = allowedIps.Split(", ");

            var ipFilter = new IpFilter(options);

            var httpContext = new FakeHttpContext();
            httpContext.Request.Path = "/admin";
            httpContext.Connection.RemoteIpAddress = IPAddress.Parse(requestIp);

            Action action = () => ipFilter.CheckIpWhenMatchOptionPath(httpContext);

            action.Should().NotThrow<Exception>();
        }

        [Fact]
        public void NoExceptionWhenPathIsNotUnderIpRestriction()
        {
            var options = new IpFilterOptions();
            options.Path = "/admin";
            options.AllowedIps = new[] { "235.1.2.4" };

            var ipFilter = new IpFilter(options);

            var httpContext = new FakeHttpContext();
            httpContext.Request.Path = "/test";
            httpContext.Connection.RemoteIpAddress = IPAddress.Parse("81.46.77.117");

            Action action = () => ipFilter.CheckIpWhenMatchOptionPath(httpContext);

            action.Should().NotThrow<Exception>();
        }

        [Fact]
        public void ExceptionWhenIpIsNotAllowed()
        {
            var options = new IpFilterOptions();
            options.Path = "/admin";
            options.AllowedIps = new[] { "235.1.2.4" };

            var ipFilter = new IpFilter(options);

            var httpContext = new FakeHttpContext();
            httpContext.Request.Path = "/admin";
            httpContext.Connection.RemoteIpAddress = IPAddress.Parse("81.46.77.117");

            Action action = () => ipFilter.CheckIpWhenMatchOptionPath(httpContext);

            action.Should().Throw<ForbiddenAccessException>().Which
                .Data["RequestIp"].Should().Be("81.46.77.117");
        }
        #endregion

        #region ip filtering behind proxy
        [Theory]
        [InlineData("192.168.1.123", "192.168.1.123", "235.1.2.4", "235.1.2.4")]
        [InlineData("192.168.1.123", "192.168.1.123", "235.1.2.4", "235.1.2.4, 235.1.2.5")]
        [InlineData("192.168.1.123", "192.168.1.123", "235.1.2.5", "235.1.2.4, 235.1.2.5")]
        [InlineData("192.168.1.123", "192.168.1.123", "235.1.2.4", "235.1.2.*")]
        [InlineData("192.168.1.123", "235.1.2.4", null, "235.1.2.4")]
        [InlineData("192.168.1.123", "235.1.2.4", null, "235.1.2.4, 235.1.2.5")]
        [InlineData("192.168.1.123", "235.1.2.5", null, "235.1.2.4, 235.1.2.5")]
        [InlineData("192.168.1.123", "235.1.2.4", null, "235.1.2.*")]
        [InlineData(null, "235.1.2.4", null, "235.1.2.4")]
        [InlineData(null, "235.1.2.4", null, "235.1.2.4, 235.1.2.5")]
        [InlineData(null, "235.1.2.5", null, "235.1.2.4, 235.1.2.5")]
        [InlineData(null, "235.1.2.4", null, "235.1.2.*")]
        public void NoExceptionWhenIpAllowedBehindProxy(string proxyIp, string requestIp, string forwardedIp, string allowedIps)
        {
            var options = new IpFilterOptions();
            options.Path = "/admin";
            options.AllowedIps = allowedIps.Split(", ");
            options.ReverseProxyIp = proxyIp;
            options.ReverseProxyHeader = proxyIp == null ? null : "X-Forwarded-For";

            var ipFilter = new IpFilter(options);

            var httpContext = new FakeHttpContext();
            httpContext.Request.Path = "/admin";
            httpContext.Connection.RemoteIpAddress = IPAddress.Parse(requestIp);
            httpContext.Request.Headers["X-Forwarded-For"] = forwardedIp;

            Action action = () => ipFilter.CheckIpWhenMatchOptionPath(httpContext);

            action.Should().NotThrow<Exception>();
        }

        [Theory]
        [InlineData("192.168.1.123", "192.168.1.123", "81.46.77.117")]
        [InlineData(null, "81.46.77.117", null)]
        [InlineData(null, "192.168.1.123", "81.46.77.117")]
        public void NoExceptionWhenPathIsNotUnderIpRestrictionBehindProxy(string proxyIp, string requestIp, string forwardedIp)
        {
            var options = new IpFilterOptions();
            options.Path = "/admin";
            options.AllowedIps = new[] { "235.1.2.4" };
            options.ReverseProxyIp = proxyIp;
            options.ReverseProxyHeader = proxyIp == null ? null : "X-Forwarded-For";

            var ipFilter = new IpFilter(options);

            var httpContext = new FakeHttpContext();
            httpContext.Request.Path = "/test";
            httpContext.Connection.RemoteIpAddress = IPAddress.Parse(requestIp);
            httpContext.Request.Headers["X-Forwarded-For"] = forwardedIp;

            Action action = () => ipFilter.CheckIpWhenMatchOptionPath(httpContext);

            action.Should().NotThrow<Exception>();
        }

        [Theory]
        [InlineData("235.1.2.4")]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("  ")]
        public void ExceptionWhenIpIsNotAllowedBehindProxy(string allowedIps)
        {
            var options = new IpFilterOptions();
            options.Path = "/admin";
            options.AllowedIps = allowedIps?.Split(", ");
            options.ReverseProxyIp = "192.168.1.123";
            options.ReverseProxyHeader = "X-Forwarded-For";

            var ipFilter = new IpFilter(options);

            var httpContext = new FakeHttpContext();
            httpContext.Request.Path = "/admin";
            httpContext.Connection.RemoteIpAddress = IPAddress.Parse("192.168.1.123");
            httpContext.Request.Headers["X-Forwarded-For"] = "81.46.77.117";

            Action action = () => ipFilter.CheckIpWhenMatchOptionPath(httpContext);

            var exception = action.Should().Throw<ForbiddenAccessException>().Which;
            exception.Data["RequestIp"].Should().Be("192.168.1.123");
            exception.Data["ForwardedIp"].Should().Be("81.46.77.117");
        }

        [Fact]
        public void ExceptionWhenTryToSpoofForwardedIpBehindProxy()
        {
            var options = new IpFilterOptions();
            options.Path = "/admin";
            options.AllowedIps = new string[] { "235.1.2.4" };
            options.ReverseProxyIp = "192.168.1.123";
            options.ReverseProxyHeader = "X-Forwarded-For";

            var ipFilter = new IpFilter(options);

            var httpContext = new FakeHttpContext();
            httpContext.Request.Path = "/admin";
            httpContext.Connection.RemoteIpAddress = IPAddress.Parse("192.168.1.123");
            httpContext.Request.Headers["X-Forwarded-For"] = "235.1.2.4,81.46.77.117";//reverseproxy concat ips when X-Forwarded-For header is already present in the request

            Action action = () => ipFilter.CheckIpWhenMatchOptionPath(httpContext);

            var exception = action.Should().Throw<ForbiddenAccessException>().Which;
            exception.Data["RequestIp"].Should().Be("192.168.1.123");
            exception.Data["ForwardedIp"].Should().Be("235.1.2.4,81.46.77.117");
        }
        #endregion
    }
}
