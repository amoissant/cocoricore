﻿using CocoriCore.Clock;
using CocoriCore.Common;
using CocoriCore.Jwt;
using CocoriCore.TestUtils;
using FluentAssertions;
using Jose;
using Microsoft.Net.Http.Headers;
using System;
using Xunit;

namespace CocoriCore.Authentication.Http.Test
{
    public class JwtBearerAuthenticatorTest
    {
        private readonly JwtAuthenticator _authenticator;
        private readonly ITokenService _tokenService;
        private readonly FakeClock _clock;

        public JwtBearerAuthenticatorTest()
        {
            var tokenConfiguration = new TokenConfiguration()
                .UseSecret("secret")
                .UseAlgorithm(JwsAlgorithm.HS256);
            var serializer = new JwtSerializer(nameof(AccessClaims.ExpireAt));
            _clock = new FakeClock();
            _tokenService = new TokenService(_clock, tokenConfiguration, serializer);
            _authenticator = new JwtAuthenticator(_tokenService);
        }

        [Fact]
        public void ReturnFalseWhenNoBearerHeader()
        {
            var httpRequest = new FakeHttpRequest();

            _authenticator.TryAuthenticate(httpRequest, LocationsFlags.AuthorizationHeader).Should().Be(false);

            _authenticator.IsAuthenticated.Should().Be(false);
        }

        [Fact]
        public void AuthenticationExceptionWhenNoBearerHeader()
        {
            var httpRequest = new FakeHttpRequest();

            Action action = () => _authenticator.Authenticate(httpRequest, LocationsFlags.AuthorizationHeader);

            action.Should().Throw<AuthenticationException>();
        }

        [Theory]
        [InlineData("Bearer")]
        [InlineData("bearer")]
        [InlineData("Basic")]
        [InlineData("Bearerzzzz")]
        public void ReturnFalseWhenBearerHeaderContainsInvalidValue(string headerValue)
        {
            var httpRequest = new FakeHttpRequest();
            httpRequest.Headers[HeaderNames.Authorization] = headerValue;

            _authenticator.TryAuthenticate(httpRequest, LocationsFlags.AuthorizationHeader).Should().Be(false);
        }

        [Theory]
        [InlineData("Bearer")]
        [InlineData("bearer")]
        [InlineData("Basic")]
        [InlineData("Bearerzzzz")]
        public void AuthenticationExceptionWhenBearerHeaderContainsInvalidValue(string headerValue)
        {
            var httpRequest = new FakeHttpRequest();
            httpRequest.Headers[HeaderNames.Authorization] = headerValue;

            Action action = () => _authenticator.Authenticate(httpRequest, LocationsFlags.AuthorizationHeader);

            action.Should().Throw<AuthenticationException>();
        }

        [Theory]
        [InlineData("Bearer ")]
        [InlineData("Bearer not.valid.jwt")]
        [InlineData("Bearer zzzzzzzzzzz")]
        public void ExceptionWhenTokenIsInvalid(string headerValue)
        {
            var httpRequest = new FakeHttpRequest();
            httpRequest.Headers[HeaderNames.Authorization] = headerValue;

            Action action1 = () => _authenticator.TryAuthenticate(httpRequest, LocationsFlags.AuthorizationHeader);
            Action action2 = () => _authenticator.Authenticate(httpRequest, LocationsFlags.AuthorizationHeader);

            _authenticator.IsAuthenticated.Should().Be(false);
            action1.Should().Throw<InvalidTokenException>();
            action2.Should().Throw<InvalidTokenException>();
        }

        [Theory]
        [InlineData("Bearer ")]
        [InlineData("bearer ")]
        public void GetClaimsWhenTryAuthenticationHeaderContainsValidJWT(string prefix)
        {
            _clock.Now = new DateTime(2020, 6, 23, 18, 30, 10);
            var payload = new AccessClaims { UserId = Tuid.Guid("1"), ExpireAt = new DateTime(2020, 6, 23, 18, 30, 59) };
            var jwt = _tokenService.GenerateToken(payload);

            var httpRequest = new FakeHttpRequest();
            httpRequest.Headers[HeaderNames.Authorization] = $"{prefix}{jwt}";

            _authenticator.TryAuthenticate(httpRequest, LocationsFlags.AuthorizationHeader).Should().Be(true);
            
            _authenticator.IsAuthenticated.Should().Be(true);
            _authenticator.GetClaims(typeof(AccessClaims)).Should().BeEquivalentTo(payload);
        }

        [Theory]
        [InlineData("Bearer ")]
        [InlineData("bearer ")]
        public void GetClaimsWhenAuthenticationHeaderContainsValidJWT(string prefix)
        {
            _clock.Now = new DateTime(2020, 6, 23, 18, 30, 10);
            var payload = new AccessClaims { UserId = Tuid.Guid("1"), ExpireAt = new DateTime(2020, 6, 23, 18, 30, 59) };
            var jwt = _tokenService.GenerateToken(payload);

            var httpRequest = new FakeHttpRequest();
            httpRequest.Headers[HeaderNames.Authorization] = $"{prefix}{jwt}";

            _authenticator.Authenticate(httpRequest, LocationsFlags.AuthorizationHeader);

            _authenticator.IsAuthenticated.Should().Be(true);
            _authenticator.GetClaims(typeof(AccessClaims)).Should().BeEquivalentTo(payload);
        }

        [Fact]
        public void ExceptionWhenGetClaimsWhileNotAuthenticated()
        {
            _authenticator.IsAuthenticated.Should().Be(false);

            Action action = () => _authenticator.GetClaims(typeof(AccessClaims));

            action.Should().Throw<InvalidOperationException>();
        }

        public class AccessClaims
        {
            public Guid UserId { get; set; }
            public DateTime ExpireAt { get; set; }
        }
    }
}