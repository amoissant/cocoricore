﻿using Autofac;
using CocoriCore.Clock;
using CocoriCore.Common;
using CocoriCore.FileSystem;
using CocoriCore.Secret;
using CocoriCore.Security;
using CocoriCore.TestUtils;
using FluentAssertions;
using System;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace CocoriCore.Test
{
    public class SecretManagerTest
    {
        private readonly ContainerBuilder _builder;
        private readonly SecretOptionsBuilder _secretBuilder;
        private readonly Lazy<ILifetimeScope> _rootScope;

        public SecretManagerTest(ITestOutputHelper output)
        {
            _builder = new ContainerBuilder();
            _builder.RegisterType<AESCryptoService>().AsImplementedInterfaces();
            _builder.RegisterType<InMemoryFileSystem>().AsImplementedInterfaces().SingleInstance();
            _builder.RegisterType<SystemClock>().AsImplementedInterfaces();
            _builder.RegisterType<ConfigurationFileEncryptor>().AsSelf();
            _builder.RegisterType<SecretManager>().AsImplementedInterfaces().InstancePerLifetimeScope();
            _builder.RegisterType<MasterKeyLoader>().AsSelf();
            _builder.Register(c => _secretBuilder.Build()).SingleInstance();
       _builder.ConfigureXunitLogger(output);

            _rootScope = new Lazy<ILifetimeScope>(() => _builder.Build());
            _secretBuilder = new SecretOptionsBuilder();
        }

        private ILifetimeScope RootScope => _rootScope.Value;

        private IFileSystem FileSystem => RootScope.Resolve<IFileSystem>();

        private ConfigurationFileEncryptor FileEncryptor => RootScope.Resolve<ConfigurationFileEncryptor>();
        private ISecretManager SecretManager => RootScope.Resolve<ISecretManager>();

        [Fact]
        public async Task ExceptionIfDidNotCallLoadSecretsBeforeGetSecret()
        {
            _secretBuilder.ForMasterKey().UseValue("FdckPXWj90ZrDmNkh3s9SNdi97CsI4qs8UN1ZOBvC/I=");
            _secretBuilder.For("mysecret").UseXPath(x => x.AppKey("apiKey"));
            var content = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n" +
                "<configuration>\r\n" +
                "  <appSettings>\r\n" +
                "    <add key=\"apiKey\" value=\"abcd\" />\r\n" +
                "  </appSettings>\r\n" +
                "</configuration>";
            await FileSystem.CreateTextFileAsync("web.config", content);
            await FileEncryptor.EncryptSecretsAsync("web.config");

            Action action = () => SecretManager.Get("mysecret");

            action.Should().Throw<InvalidOperationException>().Which.Message
                .Should().Contain(nameof(SecretManager.LoadSecretsAsync));
        }

        [Theory]
        [InlineData("abcd", "abcd")]
        [InlineData("abcd\\\\toto", @"abcd\toto")]
        public async Task CanRetrieveEncryptedSecrets(string secret, string expected)
        {
            _secretBuilder.ForMasterKey().UseXPath(x => x.AppKey("masterKeyPath"));
            _secretBuilder.For("mysecret").UseXPath(x => x.AppKey("apiKey"));
            var content = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n" +
                "<configuration>\r\n" +
                "  <appSettings>\r\n" +
                "    <add key=\"masterKeyPath\" value=\"keys\\master.key\" />\r\n" +
               $"    <add key=\"apiKey\" value=\"{secret}\" />\r\n" +
                "  </appSettings>\r\n" +
                "</configuration>";
            await FileSystem.CreateTextFileAsync("web.config", content);
            await FileSystem.CreateDirectoryAsync("keys");
            await FileSystem.CreateTextFileAsync("master.key", "FdckPXWj90ZrDmNkh3s9SNdi97CsI4qs8UN1ZOBvC/I=");
            await FileEncryptor.EncryptSecretsAsync("web.config");

            await SecretManager.LoadSecretsAsync("web.config");
            var secretValue = SecretManager.Get("mysecret");

            secretValue.Should().Be(expected);
        }

        [Fact]
        public async Task CanRetrieveNotEncryptedSecrets()
        {
            _secretBuilder.For("mysecret").UseXPath(x => x.AppKey("apiKey"));
            var content = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n" +
                "<configuration>\r\n" +
                "  <appSettings>\r\n" +
                "    <add key=\"apiKey\" value=\"abcd\" />\r\n" +
                "  </appSettings>\r\n" +
                "</configuration>";
            await FileSystem.CreateTextFileAsync("web.config", content);

            await SecretManager.LoadSecretsAsync("web.config");
            var secretValue = SecretManager.Get("mysecret");

            secretValue.Should().Be("abcd");
        }

        [Theory]
        [InlineData("abcd", "abcd")]
        [InlineData("ab\\\\cd", "ab\\cd")]
        public async Task CanRetrieveNotEncryptedSecretsEvenIfNoMasterKeyFile(string secret, string expected)
        {
            _secretBuilder.For("mysecret").UseXPath(x => x.AppKey("apiKey"));
            var content = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n" +
                "<configuration>\r\n" +
                "  <appSettings>\r\n" +
                "    <add key=\"masterKeyPath\" value=\"keys\\master.key\" />\r\n" +
               $"    <add key=\"apiKey\" value=\"{secret}\" />\r\n" +
                "  </appSettings>\r\n" +
                "</configuration>";
            await FileSystem.CreateTextFileAsync("web.config", content);

            await SecretManager.LoadSecretsAsync("web.config");
            SecretManager.Get("mysecret").Should().Be(expected);
        }

        [Fact]
        public async Task ExceptionIfSeveralValuesForOneSecret()
        {
            _secretBuilder.For("mysecret").UseXPath(x => x.AppKey("apiKey"));
            var content = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n" +
                "<configuration>\r\n" +
                "  <appSettings>\r\n" +
                "    <add key=\"apiKey\" value=\"abcd\" />\r\n" +
                "    <add key=\"apiKey\" value=\"test\" />\r\n" +
                "  </appSettings>\r\n" +
                "</configuration>";
            await FileSystem.CreateTextFileAsync("web.config", content);

            Func<Task> action = () => SecretManager.LoadSecretsAsync("web.config");

            var assertion = await action.Should().ThrowAsync<ConfigurationException>();
            assertion.Which.Message
                .Should().Contain("A value was previously set")
                .And.Contain("mysecret")
                .And.Contain("web.config");
        }

        [Fact]
        public async Task AllowSeveralSecretsMatchOneLine()
        {
            _secretBuilder.For("mysecret1").UseXPath(x => x.AppKey("apiKey"));
            _secretBuilder.For("mysecret2").UseXPath(x => x.AppKey("apiKey"));
            var content = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n" +
                "<configuration>\r\n" +
                "  <appSettings>\r\n" +
                "    <add key=\"apiKey\" value=\"abcd\" />\r\n" +
                "  </appSettings>\r\n" +
                "</configuration>";
            await FileSystem.CreateTextFileAsync("web.config", content);

            await SecretManager.LoadSecretsAsync("web.config");

            SecretManager.Get("mysecret1").Should().Be("abcd");
            SecretManager.Get("mysecret2").Should().Be("abcd");
        }

        [Fact]
        public async Task ExceptionIfMissingSecretNotOptionnal()
        {
            _secretBuilder.ForMasterKey().UseValue("FdckPXWj90ZrDmNkh3s9SNdi97CsI4qs8UN1ZOBvC/I=");
            _secretBuilder.For("mysecret").UseXPath(x => x.AppKey("apiKey"));
            var content = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n" +
                "<configuration>\r\n" +
                "  <appSettings>\r\n" +
                "  </appSettings>\r\n" +
                "</configuration>";
            await FileSystem.CreateTextFileAsync("web.config", content);

            Func<Task> action = () => SecretManager.LoadSecretsAsync("web.config");

            var assertion = await action.Should().ThrowAsync<ConfigurationException>();
            assertion.Which.Message
                .Should().Contain("no match")
                .And.Contain("mysecret")
                .And.Contain("optionnal");
        }

        [Fact]
        public async Task CanDefineOptionalSecrets()
        {
            _secretBuilder.ForMasterKey().UseValue("FdckPXWj90ZrDmNkh3s9SNdi97CsI4qs8UN1ZOBvC/I=");
            _secretBuilder.For("mysecret").UseXPath(x => x.AppKey("apiKey")).Optional();
            var content = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n" +
                "<configuration>\r\n" +
                "  <appSettings>\r\n" +
                "  </appSettings>\r\n" +
                "</configuration>";
            await FileSystem.CreateTextFileAsync("web.config", content);

            Func<Task> action = () => SecretManager.LoadSecretsAsync("web.config");

            await action.Should().NotThrowAsync<Exception>();
        }

        [Fact]
        public async Task DontCreateMasterKeyIfFileIsMissingWhenLoadSecrets()
        {
            _secretBuilder.ForMasterKey().UseXPath(x => x.AppKey("masterKeyPath"));
            _secretBuilder.For("mysecret").UseXPath(x => x.AppKey("apiKey"));
            var content = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n" +
                "<configuration>\r\n" +
                "  <appSettings>\r\n" +
                "    <add key=\"masterKeyPath\" value=\"keys\\master.key\" />\r\n" +
                "    <add key=\"apiKey\" value=\"abcd\" />\r\n" +
                "  </appSettings>\r\n" +
                "</configuration>";
            await FileSystem.CreateTextFileAsync("web.config", content);
            await FileEncryptor.EncryptSecretsAsync("web.config");

            await SecretManager.LoadSecretsAsync("web.config");

            SecretManager.Get("mysecret").Should().Be("abcd");
            var fileExists = await FileSystem.FileExistsAsync(".keys\\master.key");
            fileExists.Should().BeFalse();
        }

        [Fact]
        public async Task ExceptionIfMasterKeyFileExistButWhithAnInvalidKey()
        {
            _secretBuilder.ForMasterKey().UseXPath(x => x.AppKey("masterKeyPath"));
            _secretBuilder.For("mysecret").UseXPath(x => x.AppKey("apiKey"));
            var content = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n" +
                "<configuration>\r\n" +
                "  <appSettings>\r\n" +
                "    <add key=\"masterKeyPath\" value=\"keys\\master.key\" />\r\n" +
                "    <add key=\"apiKey\" value=\"__UXP6fpthWedRBfhGQBv6gAkAAAB11DfxQ19XN86LUt4F67yZ\" />\r\n" +
                "  </appSettings>\r\n" +
                "</configuration>";
            await FileSystem.CreateTextFileAsync("web.config", content);
            await FileSystem.CreateDirectoryAsync("keys");
            await FileSystem.CreateTextFileAsync("keys/master.key", "invalid key");

            Func<Task> action = () => SecretManager.LoadSecretsAsync("web.config");

            var assertion = await action.Should().ThrowAsync<CryptographicException>();
            assertion.Which.Message.Should().Contain("master.key");
        }

        [Fact]
        public async Task ExceptionIfSecretNotPresent()
        {
            _secretBuilder.For("mysecret").UseXPath(x => x.AppKey("apiKey"));
            var content = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n" +
                "<configuration>\r\n" +
                "  <appSettings>\r\n" +
                "    <add key=\"apiKey\" value=\"abcd\" />\r\n" +
                "  </appSettings>\r\n" +
                "</configuration>";
            await FileSystem.CreateTextFileAsync("web.config", content);
            await SecretManager.LoadSecretsAsync("web.config");

            Action action = () => SecretManager.Get("mysecretZ");

            action.Should().Throw<ConfigurationException>().Which.Message
                .Should().Contain("mysecretZ")
                .And.Contain("not defined");
        }

        [Fact]
        public async Task ReturnEmptyIfSecretNotPresentButOptionnal()
        {
            _secretBuilder.ForMasterKey().UseValue("FdckPXWj90ZrDmNkh3s9SNdi97CsI4qs8UN1ZOBvC/I=");
            _secretBuilder.For("mysecret").UseXPath(x => x.AppKey("apiKey")).Optional();
            var content = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n" +
                "<configuration>\r\n" +
                "  <appSettings>\r\n" +
                "  </appSettings>\r\n" +
                "</configuration>";
            await FileSystem.CreateTextFileAsync("web.config", content);
            await FileEncryptor.EncryptSecretsAsync("web.config");

            await SecretManager.LoadSecretsAsync("web.config");

            SecretManager.Get("mysecret").Should().BeEmpty();
        }

        [Fact]
        public async Task ExceptionIfNoWayToRetrieveMasterKey()
        {
            _secretBuilder.For("mysecret").UseXPath(x => x.AppKey("apiKey"));
            var content = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n" +
                "<configuration>\r\n" +
                "  <appSettings>\r\n" +
                "    <add key=\"apiKey\" value=\"__UXP6fpthWedRBfhGQBv6gAkAAAB11DfxQ19XN86LUt4F67yZ\" />\r\n" +
                "  </appSettings>\r\n" +
                "</configuration>";
            await FileSystem.CreateTextFileAsync("web.config", content);

            Func<Task> action = () => SecretManager.LoadSecretsAsync("web.config");

            var assertion = await action.Should().ThrowAsync<ConfigurationException>();
            assertion.Which.Message.Should().Contain("Master key configuration error.")
                .And.Contain(nameof(SecretOptionsBuilder.ForMasterKey));
        }

        [Fact]
        public async Task CanOverrideSecretWithClearValue()
        {
            _secretBuilder.ForMasterKey().UseValue("FdckPXWj90ZrDmNkh3s9SNdi97CsI4qs8UN1ZOBvC/I=");
            _secretBuilder.For("mysecret").UseJsonPath("$.apiKey");
            var content = "{\r\n" +
                "  \"apiKey\": \"__zDi4GbVVrsJdIWPxPfVn+mPYLlUxU/YfQx9IZGkv7VY=\"\r\n" +
                "}";
            await FileSystem.CreateTextFileAsync("appsettings.json", content);

            await SecretManager.LoadSecretsAsync("appsettings.json");

            var secretValue = SecretManager.Get("mysecret");
            secretValue.Should().Be("abcd");

            SecretManager.Set("mysecret", "wxyz");
            secretValue = SecretManager.Get("mysecret");

            secretValue.Should().Be("wxyz");
        }

        [Fact]
        public async Task CanOverrideSecretWithEncryptedValue()
        {
            _secretBuilder.ForMasterKey().UseValue("FdckPXWj90ZrDmNkh3s9SNdi97CsI4qs8UN1ZOBvC/I=");
            _secretBuilder.For("mysecret").UseJsonPath("$.apiKey");
            var content = "{\r\n" +
                "  \"apiKey\": \"__zDi4GbVVrsJdIWPxPfVn+mPYLlUxU/YfQx9IZGkv7VY=\"\r\n" +
                "}";
            await FileSystem.CreateTextFileAsync("appsettings.json", content);

            await SecretManager.LoadSecretsAsync("appsettings.json");

            var secretValue = SecretManager.Get("mysecret");
            secretValue.Should().Be("abcd");

            SecretManager.Set("mysecret", "__JQVyPF1ysDZfZGT9OZzu8FZj6cA6ilHj74p1d4/ki+o=");
            secretValue = SecretManager.Get("mysecret");

            secretValue.Should().Be("wxyz");
        }
    }
}
