﻿using Autofac;
using CocoriCore.Clock;
using CocoriCore.Common;
using CocoriCore.FileSystem;
using CocoriCore.Secret;
using CocoriCore.Security;
using CocoriCore.TestUtils;
using FluentAssertions;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace CocoriCore.Test
{
    public class ConfigurationFileEncryptorTest
    {
        private readonly ContainerBuilder _builder;
        private readonly SecretOptionsBuilder _secretBuilder;

        private readonly Lazy<ILifetimeScope> _rootScope;

        public ConfigurationFileEncryptorTest(ITestOutputHelper output)
        {
            _builder = new ContainerBuilder();
            _builder.RegisterType<FakeCryptoService>().AsImplementedInterfaces();
            _builder.RegisterType<InMemoryFileSystem>().AsImplementedInterfaces().SingleInstance();
            _builder.RegisterType<SystemClock>().AsImplementedInterfaces();
            _builder.RegisterType<ConfigurationFileEncryptor>().AsSelf();
            _builder.RegisterType<MasterKeyLoader>().AsSelf();
            _builder.Register(c => _secretBuilder.Build()).SingleInstance();
            _builder.ConfigureXunitLogger(output);

            _rootScope = new Lazy<ILifetimeScope>(() => _builder.Build());
            _secretBuilder = new SecretOptionsBuilder();
        }

        private ILifetimeScope RootScope => _rootScope.Value;

        private IFileSystem FileSystem => RootScope.Resolve<IFileSystem>();

        private ConfigurationFileEncryptor FileEncryptor => RootScope.Resolve<ConfigurationFileEncryptor>();

        [Fact]
        public async Task ExceptionIfWrongMasterKeyPathFromAppKey()
        {
            _secretBuilder.ForMasterKey().UseXPath(x => x.AppKey("masterKeyPathZZZ"));
            var content = $"<?xml version=\"1.0\" encoding=\"utf-8\"?>{Environment.NewLine}" +
                $"<configuration>{Environment.NewLine}" +
                $"  <appSettings>{Environment.NewLine}" +
                $"    <add key=\"masterKeyPath\" value=\"master.key\"/>{Environment.NewLine}" +
                $"  </appSettings>{Environment.NewLine}" +
                "</configuration>";
            await FileSystem.CreateTextFileAsync("web.config", content);
            
            Func<Task> action = () => FileEncryptor.EncryptSecretsAsync("web.config");

            var assertion = await action.Should().ThrowAsync<ConfigurationException>();
            assertion.Which.Message
                .Should().Contain("No match found for master key")
                .And.Contain("masterKeyPathZZZ")
                .And.Contain("web.config");
        }

        [Fact]
        public async Task ExceptionIfSecretMatchNoLine()
        {
            _secretBuilder.ForMasterKey().UseValue("QVpFUlRZ");
            _secretBuilder.For("mysecret").UseXPath(x => x.AppKey("apiKeyZZ"));
            var content = $"<?xml version=\"1.0\" encoding=\"utf-8\"?>{Environment.NewLine}" +
                $"<configuration>{Environment.NewLine}" +
                $"  <appSettings>{Environment.NewLine}" +
                $"    <add key=\"apiKey\" value=\"abcd\"/>{Environment.NewLine}" +
                $"  </appSettings>{Environment.NewLine}" +
                "</configuration>";
            await FileSystem.CreateTextFileAsync("web.config", content);

            Func<Task> action = () => FileEncryptor.EncryptSecretsAsync("web.config");

            var assertion = await action.Should().ThrowAsync<ConfigurationException>();
            assertion.Which.Message.Should()
                .Contain("apiKeyZZ")
                .And.Contain("No match found")
                .And.Contain("web.config");
        }

        [Fact]
        public async Task ExceptionIfSecretMatchSeveralLines()
        {
            _secretBuilder.ForMasterKey().UseValue("QVpFUlRZ");
            _secretBuilder.For("mysecret").UseXPath(x => x.AppKey("apiKey"));
            var content = $"<?xml version=\"1.0\" encoding=\"utf-8\"?>{Environment.NewLine}" +
                $"<configuration>{Environment.NewLine}" +
                $"  <appSettings>{Environment.NewLine}" +
                $"    <add key=\"apiKey\" value=\"abcd\"/>{Environment.NewLine}" +
                $"    <add key=\"apiKey\" value=\"1234\"/>{Environment.NewLine}" +
                $"  </appSettings>{Environment.NewLine}" +
                "</configuration>";
            await FileSystem.CreateTextFileAsync("web.config", content);

            Func<Task> action = () => FileEncryptor.EncryptSecretsAsync("web.config");

            var assertion = await action.Should().ThrowAsync<ConfigurationException>();
            assertion.Which.Message.Should()
                .Contain("Duplicate match for secret")
                .And.Contain("mysecret")
                .And.Contain("web.config");
        }

        [Fact]
        public void ExceptionIfSeveralSecretsWithSameName()
        {
            _secretBuilder.ForMasterKey().UseValue("QVpFUlRZ");
            _secretBuilder.For("mysecret").UseXPath(x => x.AppKey("apiKey"));
            _secretBuilder.For("mysecret").UseXPath(x => x.AppKey("apiKey"));

            Action action = () => _secretBuilder.Build();

            action.Should().Throw<ConfigurationException>().Which.Message
                .Should().Contain("Duplicate secret name")
                .And.Contain("mysecret");
        }

        [Fact]
        public async Task CanReplaceSecretInFileUsingXpathExpression()
        {
            _secretBuilder.ForMasterKey().UseValue("QVpFUlRZ");
            _secretBuilder.For("mysecret").UseXPath("/settings/add[@key='apiKey']/@value");
            var content = $"<?xml version=\"1.0\" encoding=\"utf-8\"?>{Environment.NewLine}" +
                $"<settings>{Environment.NewLine}" +
                $"  <add key=\"apiKey\" value=\"abcd\" otherAttr=\"toto\" />{Environment.NewLine}" +
                "</settings>";
            await FileSystem.CreateTextFileAsync("web.config", content);

            await FileEncryptor.EncryptSecretsAsync("web.config");

            var newContent = await FileSystem.ReadAsTextAsync("web.config");
            newContent.Should().Be(content.Replace("abcd", "__ZGNiYVlUUkVaQQ=="));
        }

        [Fact]
        public async Task CanReplaceSecretInFileUsingXPathBuilder()
        {
            _secretBuilder.ForMasterKey().UseValue("QVpFUlRZ");
            _secretBuilder.For("mysecret").UseXPath(x => x.AppKey("apiKey"));
            var content = $"<?xml version=\"1.0\" encoding=\"utf-8\"?>{Environment.NewLine}" +
                $"<configuration>{Environment.NewLine}" +
                $"  <appSettings>{Environment.NewLine}" +
                $"    <add key=\"apiKey\" value=\"abcd\" />{Environment.NewLine}" +
                $"  </appSettings>{Environment.NewLine}" +
                "</configuration>";
            await FileSystem.CreateTextFileAsync("web.config", content);

            await FileEncryptor.EncryptSecretsAsync("web.config");

            var newContent = await FileSystem.ReadAsTextAsync("web.config");
            newContent.Should().Be(content.Replace("abcd", "__ZGNiYVlUUkVaQQ=="));
        }

        [Fact]
        public async Task CanReplaceSecretInFileUsingXPathBuilder_BOM()
        {
            _secretBuilder.ForMasterKey().UseValue("QVpFUlRZ");
            _secretBuilder.For("mysecret").UseXPath(x => x.AppKey("apiKey"));
            var content = $"<?xml version=\"1.0\" encoding=\"utf-8\"?>{Environment.NewLine}" +
                $"<configuration>{Environment.NewLine}" +
                $"  <appSettings>{Environment.NewLine}" +
                $"    <add key=\"apiKey\" value=\"abcd\" />{Environment.NewLine}" +
                $"  </appSettings>{Environment.NewLine}" +
                "</configuration>";
            content = content.AddUTF8BOM();
            await FileSystem.CreateTextFileAsync("web.config", content);

            await FileEncryptor.EncryptSecretsAsync("web.config");

            var newContent = await FileSystem.ReadAsTextAsync("web.config");
            newContent.Should().Be(content.Replace("abcd", "__ZGNiYVlUUkVaQQ=="));
        }

        [Fact]
        public async Task CanReplaceConnectionStringInFile()
        {
            _secretBuilder.ForMasterKey().UseValue("QVpFUlRZ");
            _secretBuilder.For("myDb").UseXPath(x => x.ConnectionString("myApp"));
            var connectionString = "Server=127.0.0.1;Port=5432;database=dn_name;User Id=db_user;Password=1234";
            var content = $"<?xml version=\"1.0\" encoding=\"utf-8\"?>{Environment.NewLine}" +
                $"<configuration>{Environment.NewLine}" +
                $"  <connectionStrings>{Environment.NewLine}" +
                $"    <add name=\"myApp\" connectionString=\"{connectionString}\" providerName=\"Npgsql\" />{Environment.NewLine}" +
                $"  </connectionStrings>{Environment.NewLine}" +
                "</configuration>";
            await FileSystem.CreateTextFileAsync("web.config", content);

            await FileEncryptor.EncryptSecretsAsync("web.config");

            var newContent = await FileSystem.ReadAsTextAsync("web.config");
            newContent.Should().Be(content.Replace(connectionString, "__NDMyMT1kcm93c3NhUDtyZXN1X2JkPWRJIHJlc1U7ZW1hbl9uZD1lc2FiYXRhZDsyMzQ1PXRyb1A7MS4wLjAuNzIxPXJldnJlU1lUUkVaQQ=="));
        }

        [Fact]
        public async Task CanReplaceSeveralSecretsInXmlFile()
        {
            _secretBuilder.ForMasterKey().UseXPath(x => x.AppKey("masterKeyPath"));
            _secretBuilder.For("mysecret").UseXPath(x => x.AppKey("apiKey"));
            _secretBuilder.For("myDb").UseXPath(x => x.ConnectionString("myApp"));
            var connectionString = "Server=127.0.0.1;Port=5432;database=dn_name;User Id=db_user;Password=1234";
            var content = $"<?xml version=\"1.0\" encoding=\"utf-8\"?>{Environment.NewLine}" +
                $"<configuration>{Environment.NewLine}" +
                $"  <connectionStrings>{Environment.NewLine}" +
                $"    <add name=\"myApp\" connectionString=\"{connectionString}\" providerName=\"Npgsql\" />{Environment.NewLine}" +
                $"  </connectionStrings>{Environment.NewLine}" +
                $"  <appSettings>{Environment.NewLine}" +
                $"    <add key=\"masterKeyPath\" value=\"keys\\master.key\" />{Environment.NewLine}" +
                $"    <add key=\"apiKey\" value=\"abcd\" />{Environment.NewLine}" +
                $"  </appSettings>{Environment.NewLine}" +
                "</configuration>";
            await FileSystem.CreateTextFileAsync("web.config", content);
            await FileSystem.CreateDirectoryAsync("keys");
            await FileSystem.CreateTextFileAsync("keys\\master.key", "QVpFUlRZ");

            await FileEncryptor.EncryptSecretsAsync("web.config");

            var newContent = await FileSystem.ReadAsTextAsync("web.config");
            newContent.Should().Be(content
                .Replace("abcd", "__ZGNiYVlUUkVaQQ==")
                .Replace(connectionString, "__NDMyMT1kcm93c3NhUDtyZXN1X2JkPWRJIHJlc1U7ZW1hbl9uZD1lc2FiYXRhZDsyMzQ1PXRyb1A7MS4wLjAuNzIxPXJldnJlU1lUUkVaQQ=="));
        }

        [Fact]
        public async Task NoExceptionIfNoMatchAndSecretOptionnal()
        {
            _secretBuilder.ForMasterKey().UseValue("QVpFUlRZ");
            _secretBuilder.For("mysecret").UseXPath(x => x.AppKey("apiKeyOptional")).Optional();
            var content = $"<?xml version=\"1.0\" encoding=\"utf-8\"?>{Environment.NewLine}" +
                $"<configuration>{Environment.NewLine}" +
                $"  <appSettings>{Environment.NewLine}" +
                $"    <add key=\"apiKey\" value=\"abcd\" />{Environment.NewLine}" +
                $"  </appSettings>{Environment.NewLine}" +
                "</configuration>";
            await FileSystem.CreateTextFileAsync("web.config", content);

            await FileEncryptor.EncryptSecretsAsync("web.config");

            var newContent = await FileSystem.ReadAsTextAsync("web.config");
            newContent.Should().Be(content);
        }

        [Fact]
        public async Task CanDefineOutputFile()
        {
            _secretBuilder.ForMasterKey().UseValue("QVpFUlRZ");
            _secretBuilder.For("mysecret").UseXPath(x => x.AppKey("apiKey"));
            var content = $"<?xml version=\"1.0\" encoding=\"utf-8\"?>{Environment.NewLine}" +
                $"<configuration>{Environment.NewLine}" +
                $"  <appSettings>{Environment.NewLine}" +
                $"    <add key=\"apiKey\" value=\"abcd\" />{Environment.NewLine}" +
                $"  </appSettings>{Environment.NewLine}" +
                "</configuration>";
            await FileSystem.CreateTextFileAsync("web.config", content);

            await FileEncryptor.EncryptSecretsAsync("web.config", "web.config.crypt");

            var oldContent = await FileSystem.ReadAsTextAsync("web.config");
            oldContent.Should().Be(content);
            var newContent = await FileSystem.ReadAsTextAsync("web.config.crypt");
            newContent.Should().Be(content.Replace("abcd", "__ZGNiYVlUUkVaQQ=="));
        }

        [Fact]
        public async Task CanDefineMasterKeyValue()
        {
            _secretBuilder.ForMasterKey().UseValue("QVpFUlRZ");
            _secretBuilder.For("mysecret").UseXPath(x => x.AppKey("apiKey"));
            var content = $"<?xml version=\"1.0\" encoding=\"utf-8\"?>{Environment.NewLine}" +
                $"<configuration>{Environment.NewLine}" +
                $"  <appSettings>{Environment.NewLine}" +
                $"    <add key=\"apiKey\" value=\"abcd\" />{Environment.NewLine}" +
                $"  </appSettings>{Environment.NewLine}" +
                "</configuration>";
            await FileSystem.CreateTextFileAsync("web.config", content);

            await FileEncryptor.EncryptSecretsAsync("web.config");

            var newContent = await FileSystem.ReadAsTextAsync("web.config");
            newContent.Should().Be(content.Replace("abcd", "__ZGNiYVlUUkVaQQ=="));
        }

        [Fact]
        public async Task CanDefineMasterKeyPath()
        {
            _secretBuilder.ForMasterKey().UsePath("master.key");
            _secretBuilder.For("mysecret").UseXPath(x => x.AppKey("apiKey"));
            var content = $"<?xml version=\"1.0\" encoding=\"utf-8\"?>{Environment.NewLine}" +
                $"<configuration>{Environment.NewLine}" +
                $"  <appSettings>{Environment.NewLine}" +
                $"    <add key=\"apiKey\" value=\"abcd\" />{Environment.NewLine}" +
                $"  </appSettings>{Environment.NewLine}" +
                "</configuration>";
            await FileSystem.CreateTextFileAsync("web.config", content);
            await FileSystem.CreateTextFileAsync("master.key", "QVpFUlRZ");

            await FileEncryptor.EncryptSecretsAsync("web.config");

            var newContent = await FileSystem.ReadAsTextAsync("web.config");
            newContent.Should().Be(content.Replace("abcd", "__ZGNiYVlUUkVaQQ=="));
        }

        [Fact]
        public async Task CanDefineMasterKeyPathUsingXPath()
        {
            _secretBuilder.ForMasterKey().UseXPath("configuration/masterKey/@path");
            _secretBuilder.For("mysecret").UseXPath(x => x.AppKey("apiKey"));
            var content = $"<?xml version=\"1.0\" encoding=\"utf-8\"?>{Environment.NewLine}" +
                $"<configuration>{Environment.NewLine}" +
                $"  <masterKey path=\"keys\\master.key\" />{Environment.NewLine}" +
                $"  <appSettings>{Environment.NewLine}" +
                $"    <add key=\"apiKey\" value=\"abcd\" />{Environment.NewLine}" +
                "  </appSettings>{Environment.NewLine}" +
                "</configuration>";
            await FileSystem.CreateTextFileAsync("web.config", content);
            await FileSystem.CreateDirectoryAsync("keys");
            await FileSystem.CreateTextFileAsync("keys\\master.key", "QVpFUlRZ");

            await FileEncryptor.EncryptSecretsAsync("web.config");

            var newContent = await FileSystem.ReadAsTextAsync("web.config");
            newContent.Should().Be(content.Replace("abcd", "__ZGNiYVlUUkVaQQ=="));
        }

        [Fact]
        public async Task CanDefineMasterKeyPathUsingAppKey()
        {
            _secretBuilder.ForMasterKey().UseXPath(x => x.AppKey("masterKeyPath"));
            _secretBuilder.For("mysecret").UseXPath(x => x.AppKey("apiKey"));
            var content = $"<?xml version=\"1.0\" encoding=\"utf-8\"?>{Environment.NewLine}" +
                $"<configuration>{Environment.NewLine}" +
                $"  <appSettings>{Environment.NewLine}" +
                $"    <add key=\"masterKeyPath\" value=\"keys\\master.key\" />{Environment.NewLine}" +
                $"    <add key=\"apiKey\" value=\"abcd\" />{Environment.NewLine}" +
                $"  </appSettings>{Environment.NewLine}" +
                "</configuration>";
            await FileSystem.CreateTextFileAsync("web.config", content);
            await FileSystem.CreateDirectoryAsync("keys");
            await FileSystem.CreateTextFileAsync("keys\\master.key", "QVpFUlRZ");

            await FileEncryptor.EncryptSecretsAsync("web.config");

            var newContent = await FileSystem.ReadAsTextAsync("web.config");
            newContent.Should().Be(content.Replace("abcd", "__ZGNiYVlUUkVaQQ=="));
        }

        [Fact]
        public async Task GenerateMasterKeyFileIfMissing()
        {
            _secretBuilder.ForMasterKey().UseXPath(x => x.AppKey("masterKeyPath"));
            _secretBuilder.For("mysecret").UseXPath(x => x.AppKey("apiKey"));
            var content = $"<?xml version=\"1.0\" encoding=\"utf-8\"?>{Environment.NewLine}" +
                $"<configuration>{Environment.NewLine}" +
                $"  <appSettings>{Environment.NewLine}" +
                $"    <add key=\"masterKeyPath\" value=\"keys\\master.key\" />{Environment.NewLine}" +
                $"    <add key=\"apiKey\" value=\"abcd\" />{Environment.NewLine}" +
                $"  </appSettings>{Environment.NewLine}" +
                "</configuration>";
            await FileSystem.CreateTextFileAsync("web.config", content);

            await FileEncryptor.EncryptSecretsAsync("web.config");

            var newContent = await FileSystem.ReadAsTextAsync("web.config");
            var masterKey = await FileSystem.ReadAsTextAsync("keys/master.key");
            newContent.Should().Be(content.Replace("abcd", "__ZGNiYXllayBla2FmIGE="));
            masterKey.Should().Be("YSBmYWtlIGtleQ==");
        }

        [Theory]
        [InlineData("")]
        [InlineData("\n")]
        public async Task ExceptionIfMasterKeyInFileInvalid(string base64Key)
        {
            _secretBuilder.ForMasterKey().UseXPath(x => x.AppKey("masterKeyPath"));
            _secretBuilder.For("mysecret").UseXPath(x => x.AppKey("apiKey"));
            var content = $"<?xml version=\"1.0\" encoding=\"utf-8\"?>{Environment.NewLine}" +
                $"<configuration>{Environment.NewLine}" +
                $"  <appSettings>{Environment.NewLine}" +
                $"    <add key=\"masterKeyPath\" value=\"keys\\master.key\" />{Environment.NewLine}" +
                $"    <add key=\"apiKey\" value=\"abcd\" />{Environment.NewLine}" +
                $"  </appSettings>{Environment.NewLine}" +
                "</configuration>";
            await FileSystem.CreateTextFileAsync("web.config", content);
            await FileSystem.CreateDirectoryAsync("keys");
            await FileSystem.CreateTextFileAsync("keys\\master.key", base64Key);

            Func<Task> action = () => FileEncryptor.EncryptSecretsAsync("web.config");

            await action.Should().ThrowAsync<CryptographicException>().WithMessage("*keys*master.key*not a valid key*");
        }

        [Theory]
        [InlineData("abcd", "__ZGNiYVlUUkVaQQ==")]//can encrypt a password
        [InlineData("dG90bw==", "__PT13YjA5R2RZVFJFWkE=")]//can encrypt a base64 secret
        [InlineData("__PT13YjA5R2RZVFJFWkE=", "__PT13YjA5R2RZVFJFWkE=")]//already encrypted secret
        public async Task ReplaceSecretIfNotAlreadyEncrypted(string secret, string encryptedSecret)
        {
            _secretBuilder.ForMasterKey().UseValue("QVpFUlRZ");
            _secretBuilder.For("mysecret").UseXPath(x => x.AppKey("apiKey"));
            var content = $"<?xml version=\"1.0\" encoding=\"utf-8\"?>{Environment.NewLine}" +
                $"<configuration>{Environment.NewLine}" +
                $"  <appSettings>{Environment.NewLine}" +
                $"    <add key=\"apiKey\" value=\"{secret}\" />{Environment.NewLine}" +
                $"  </appSettings>{Environment.NewLine}" +
                "</configuration>";
            await FileSystem.CreateTextFileAsync("web.config", content);

            await FileEncryptor.EncryptSecretsAsync("web.config");

            var newContent = await FileSystem.ReadAsTextAsync("web.config");
            newContent.Should().Be(content.Replace(secret, encryptedSecret));
        }

        [Fact]
        public async Task CanReplaceEscapedStringInFile()
        {
            _secretBuilder.ForMasterKey().UseValue("QVpFUlRZ");
            _secretBuilder.For("mysecret").UseXPath(x => x.AppKey("apiKey"));
            var content = $"<?xml version=\"1.0\" encoding=\"utf-8\"?>{Environment.NewLine}" +
                $"<configuration>{Environment.NewLine}" +
                $"  <appSettings>{Environment.NewLine}" +
                $"    <add key=\"apiKey\" value=\"valueWith\\\\EscapedChar\" />{Environment.NewLine}" +
                $"  </appSettings>{Environment.NewLine}" +
                "</configuration>";
            await FileSystem.CreateTextFileAsync("web.config", content);

            await FileEncryptor.EncryptSecretsAsync("web.config");

            var newContent = await FileSystem.ReadAsTextAsync("web.config");
            newContent.Should().Be(content.Replace("valueWith\\\\EscapedChar", "__cmFoQ2RlcGFjc0VcaHRpV2V1bGF2WVRSRVpB"));
        }

        [Fact]
        public async Task CanDecryptXmlFile()
        {
            _secretBuilder.ForMasterKey().UseValue("QVpFUlRZ");
            _secretBuilder.For("mysecret").UseXPath(x => x.AppKey("apiKey"));
            _secretBuilder.For("myDb").UseXPath(x => x.ConnectionString("myApp"));
            var connectionString = "Server=127.0.0.1;Port=5432;database=dn_name;User Id=db_user;Password=1234";
            var content = $"<?xml version=\"1.0\" encoding=\"utf-8\"?>{Environment.NewLine}" +
                $"<configuration>{Environment.NewLine}" +
                $"  <connectionStrings>{Environment.NewLine}" +
                $"    <add name=\"myApp\" connectionString=\"{connectionString}\" providerName=\"Npgsql\" />{Environment.NewLine}" +
                $"  </connectionStrings>{Environment.NewLine}" +
                $"  <appSettings>{Environment.NewLine}" +
                $"    <add key=\"apiKey\" value=\"abcd\" />{Environment.NewLine}" +
                $"  </appSettings>{Environment.NewLine}" +
                "</configuration>";
            await FileSystem.CreateTextFileAsync("web.config", content);

            await FileEncryptor.EncryptSecretsAsync("web.config");

            var newContent = await FileSystem.ReadAsTextAsync("web.config");
            newContent.Should().Be(content
                .Replace("abcd", "__ZGNiYVlUUkVaQQ==")
                .Replace(connectionString, "__NDMyMT1kcm93c3NhUDtyZXN1X2JkPWRJIHJlc1U7ZW1hbl9uZD1lc2FiYXRhZDsyMzQ1PXRyb1A7MS4wLjAuNzIxPXJldnJlU1lUUkVaQQ=="));

            await FileEncryptor.DecryptSecretsAsync("web.config");

            newContent = await FileSystem.ReadAsTextAsync("web.config");
            newContent.Should().Be(content);
        }

        [Fact]
        public async Task CanReplaceEmptySecrets()
        {
            _secretBuilder.ForMasterKey().UseValue("QVpFUlRZ");
            _secretBuilder.For("mysecret").UseXPath(x => x.AppKey("apiKey"));
            var content = $"<?xml version=\"1.0\" encoding=\"utf-8\"?>{Environment.NewLine}" +
                $"<configuration>{Environment.NewLine}" +
                $"  <appSettings>{Environment.NewLine}" +
                $"    <add key=\"apiKey\" value=\"\" />{Environment.NewLine}" +
                $"  </appSettings>{Environment.NewLine}" +
                "</configuration>";
            await FileSystem.CreateTextFileAsync("web.config", content);

            await FileEncryptor.EncryptSecretsAsync("web.config");

            var newContent = await FileSystem.ReadAsTextAsync("web.config");
            newContent.Should().Be(content.Replace("value=\"\"", "value=\"\""));
        }

        [Fact]
        public async Task CanReplaceSecretInFileUsingJsonPathBuilder_BOM()
        {
            _secretBuilder.ForMasterKey().UseValue("QVpFUlRZ");
            _secretBuilder.For("mysecret").UseJsonPath("$.apiKey");
            var content = $"{{{Environment.NewLine}" +
                $"  \"masterKeyPath\": \"keys\\\\master.key\",{Environment.NewLine}" +
                $"  \"apiKey\": \"abcd\"{Environment.NewLine}" +
                $"}}";
            content = content.AddUTF8BOM();
            await FileSystem.CreateTextFileAsync("appsettings.json", content);

            await FileEncryptor.EncryptSecretsAsync("appsettings.json");

            var newContent = await FileSystem.ReadAsTextAsync("appsettings.json");
            newContent.Should().Be(content.Replace("abcd", "__ZGNiYVlUUkVaQQ=="));
        }

        [Fact]
        public async Task CanReplaceSeveralSecretsInJsonFile()
        {
            _secretBuilder.ForMasterKey().UseJsonPath("$.masterKeyPath");
            _secretBuilder.For("mysecret").UseJsonPath("$.apiKey");
            _secretBuilder.For("myDb").UseJsonPath("$.db.connectionString");
            var connectionString = "Server=127.0.0.1;Port=5432;database=dn_name;User Id=db_user;Password=1234";
            var content = $"{{{Environment.NewLine}" +
                $"  \"masterKeyPath\": \"keys\\\\master.key\",{Environment.NewLine}" +
                $"  \"apiKey\": \"abcd\",{Environment.NewLine}" +
                $"  \"db\": {{{Environment.NewLine}" +
                $"    \"connectionString\": \"{connectionString}\"{Environment.NewLine}" +
                $"  }}{Environment.NewLine}" +
                $"}}";
            await FileSystem.CreateTextFileAsync("appsettings.json", content);
            await FileSystem.CreateDirectoryAsync("keys");
            await FileSystem.CreateTextFileAsync("keys\\master.key", "QVpFUlRZ");

            await FileEncryptor.EncryptSecretsAsync("appsettings.json");

            var newContent = await FileSystem.ReadAsTextAsync("appsettings.json");
            newContent.Should().Be(content
                .Replace("abcd", "__ZGNiYVlUUkVaQQ==")
                .Replace(connectionString, "__NDMyMT1kcm93c3NhUDtyZXN1X2JkPWRJIHJlc1U7ZW1hbl9uZD1lc2FiYXRhZDsyMzQ1PXRyb1A7MS4wLjAuNzIxPXJldnJlU1lUUkVaQQ=="));
        }

        [Fact]
        public async Task CanDecryptJsonFile()
        {
            _secretBuilder.ForMasterKey().UseJsonPath("$.masterKeyPath");
            _secretBuilder.For("mysecret").UseJsonPath("$.apiKey");
            _secretBuilder.For("myDb").UseJsonPath("$.db.connectionString");
            var connectionString = "Server=127.0.0.1;Port=5432;database=dn_name;User Id=db_user;Password=1234";
            var content = $"{{{Environment.NewLine}" +
                $"  \"masterKeyPath\": \"keys\\\\master.key\",{Environment.NewLine}" +
                $"  \"apiKey\": \"abcd\",{Environment.NewLine}" +
                $"  \"db\": {{{Environment.NewLine}" +
                $"    \"connectionString\": \"{connectionString}\"{Environment.NewLine}" +
                $"  }}{Environment.NewLine}" +
                $"}}";
            await FileSystem.CreateTextFileAsync("appsettings.json", content);
            await FileSystem.CreateDirectoryAsync("keys");
            await FileSystem.CreateTextFileAsync("keys\\master.key", "QVpFUlRZ");

            await FileEncryptor.EncryptSecretsAsync("appsettings.json");

            var newContent = await FileSystem.ReadAsTextAsync("appsettings.json");
            newContent.Should().Be(content
                .Replace("abcd", "__ZGNiYVlUUkVaQQ==")
                .Replace(connectionString, "__NDMyMT1kcm93c3NhUDtyZXN1X2JkPWRJIHJlc1U7ZW1hbl9uZD1lc2FiYXRhZDsyMzQ1PXRyb1A7MS4wLjAuNzIxPXJldnJlU1lUUkVaQQ=="));

            await FileEncryptor.DecryptSecretsAsync("appsettings.json");

            newContent = await FileSystem.ReadAsTextAsync("appsettings.json");
            newContent.Should().Be(content);
        }

        [Fact]
        public async Task ExceptionIfDecriptWithoutMasterKey()
        {
            _secretBuilder.ForMasterKey().UseJsonPath("$.masterKeyPath");
            _secretBuilder.For("mysecret").UseJsonPath("$.apiKey");
            var content = $"{{{Environment.NewLine}" +
                $"  \"masterKeyPath\": \"keys\\\\master.key\",{Environment.NewLine}" +
                $"  \"apiKey\": \"__ZGNiYVlUUkVaQQ==\",{Environment.NewLine}" +
                $"  }}{Environment.NewLine}" +
                $"}}";
            await FileSystem.CreateTextFileAsync("appsettings.json", content);

            Func<Task> action = () => FileEncryptor.DecryptSecretsAsync("appsettings.json");

            await action.Should().ThrowAsync<FileNotFoundException>().WithMessage("*keys/master.key*");

            var masterKeyFileExists = await FileSystem.FileExistsAsync("keys\\master.key");
            masterKeyFileExists.Should().BeFalse();
        }
    }
}
