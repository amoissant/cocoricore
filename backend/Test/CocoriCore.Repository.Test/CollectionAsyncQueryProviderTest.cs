﻿using CocoriCore.Common;
using CocoriCore.Linq.Async;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace CocoriCore.Repository.Test
{
    public class CollectionAsyncQueryProviderTest
    {
        [Fact]
        public async Task ExceptionIfIncompatibleQueryableAsync()
        {
            var datas = new[]
            {
                new AnEntity { Id = Guid.NewGuid(), Name = "toto" },
                new AnEntity { Id = Guid.NewGuid(), Name = "titi" },
            };

            Func<Task> action = () => datas.AsQueryable().Where(x => x.Name == "titi").ToListAsync();

            var assertion = await action.Should().ThrowAsync<InvalidOperationException>();
            assertion.Which
                .Message.Should().Contain(nameof(AsyncQueryableExtensions))
                .And.Contain("with queryable implementing")
                .And.Contain(nameof(ICocoriCoreQueryProvider));
        }

        [Fact]
        public async Task ToListAsync()
        {
            var datas = new[]
            {
                new AnEntity { Id = Guid.NewGuid(), Name = "toto" },
                new AnEntity { Id = Guid.NewGuid(), Name = "titi" },
            };
            IQueryable<AnEntity> queryable = new CollectionAsyncQueryProvider<AnEntity>(datas);

            var results = await queryable.Where(x => x.Name == "titi").ToListAsync();

            results.Should().BeOfType<List<AnEntity>>();
            results.Should().ContainSingle().Which.Id.Should().Be(datas[1].Id);
        }

        [Fact]
        public async Task ToArrayAsync()
        {
            var datas = new[]
            {
                new AnEntity { Id = Guid.NewGuid(), Name = "toto" },
                new AnEntity { Id = Guid.NewGuid(), Name = "titi" },
            };
            IQueryable<AnEntity> queryable = new CollectionAsyncQueryProvider<AnEntity>(datas);

            var results = await queryable.Where(x => x.Name == "titi").ToArrayAsync();

            results.Should().BeOfType<AnEntity[]>();
            results.Should().ContainSingle().Which.Id.Should().Be(datas[1].Id);
        }

        [Fact]
        public async Task ToDictionaryAsync()
        {
            var datas = new[]
            {
                new AnEntity { Id = Guid.NewGuid(), Name = "toto" },
                new AnEntity { Id = Guid.NewGuid(), Name = "titi" },
            };
            IQueryable<AnEntity> queryable = new CollectionAsyncQueryProvider<AnEntity>(datas);

            var results = await queryable.ToDictionaryAsync(x => x.Id, x => x.Name);

            results.Should().BeOfType<Dictionary<Guid, string>>();
            results.Should().ContainKey(datas[0].Id).WhoseValue.Should().Be("toto");
            results.Should().ContainKey(datas[1].Id).WhoseValue.Should().Be("titi");
        }

        [Fact]
        public async Task AnyAsync()
        {
            var datas = new[]
            {
                new AnEntity { Id = Guid.NewGuid(), Name = "toto" },
                new AnEntity { Id = Guid.NewGuid(), Name = "titi" },
            };
            IQueryable<AnEntity> queryable = new CollectionAsyncQueryProvider<AnEntity>(datas);

            var result1 = await queryable.AnyAsync();
            var result2 = await queryable.Where(x => x.Name == "tutu").AnyAsync();

            result1.Should().BeTrue();
            result2.Should().BeFalse();
        }

        [Fact]
        public async Task CountAsync()
        {
            var datas = new[]
            {
                new AnEntity { Id = Guid.NewGuid(), Name = "toto" },
                new AnEntity { Id = Guid.NewGuid(), Name = "titi" },
            };
            IQueryable<AnEntity> queryable = new CollectionAsyncQueryProvider<AnEntity>(datas);

            var result = await queryable.CountAsync();

            result.Should().Be(2);
        }

        [Fact]
        public async Task FirstAsync()
        {
            var datas = new[]
            {
                new AnEntity { Id = Guid.NewGuid(), Name = "toto" },
                new AnEntity { Id = Guid.NewGuid(), Name = "titi" },
            };
            IQueryable<AnEntity> queryable = new CollectionAsyncQueryProvider<AnEntity>(datas);

            var result = await queryable.FirstAsync();
            Func<Task<AnEntity>> action = () => queryable.Where(x => x.Name == "tutu").FirstAsync();

            result.Should().BeEquivalentTo(datas[0]);
            await action.Should().ThrowAsync<InvalidOperationException>();
        }

        [Fact]
        public async Task FirstOrDefaultAsync()
        {
            var datas = new[]
            {
                new AnEntity { Id = Guid.NewGuid(), Name = "toto" },
                new AnEntity { Id = Guid.NewGuid(), Name = "titi" },
            };
            IQueryable<AnEntity> queryable = new CollectionAsyncQueryProvider<AnEntity>(datas);

            var result1 = await queryable.FirstOrDefaultAsync();
            var result2 = await queryable.Where(x => x.Name == "tutu").FirstOrDefaultAsync();

            result1.Should().BeEquivalentTo(datas[0]);
            result2.Should().BeNull();
        }

        [Fact]
        public async Task SingleAsync()
        {
            var datas = new[]
            {
                new AnEntity { Id = Guid.NewGuid(), Name = "toto" },
                new AnEntity { Id = Guid.NewGuid(), Name = "titi" },
            };
            IQueryable<AnEntity> queryable = new CollectionAsyncQueryProvider<AnEntity>(datas);

            var result = await queryable.Where(x => x.Name == "toto").SingleAsync();
            Func<Task<AnEntity>> action = () => queryable.Where(x => x.Name == "tutu").SingleAsync();

            result.Should().BeEquivalentTo(datas[0]);
            await action.Should().ThrowAsync<InvalidOperationException>();
        }

        [Fact]
        public async Task SingleOrDefaultAsync()
        {
            var datas = new[]
            {
                new AnEntity { Id = Guid.NewGuid(), Name = "toto" },
                new AnEntity { Id = Guid.NewGuid(), Name = "titi" },
            };
            IQueryable<AnEntity> queryable = new CollectionAsyncQueryProvider<AnEntity>(datas);

            var result1 = await queryable.Where(x => x.Name == "toto").SingleOrDefaultAsync();
            var result2 = await queryable.Where(x => x.Name == "tutu").SingleOrDefaultAsync();

            result1.Should().BeEquivalentTo(datas[0]);
            result2.Should().BeNull();
        }

        [Fact]
        public async Task SumIntAsync()
        {
            var datas = new[]
            {
                new AnEntity { Id = Guid.NewGuid(), Int = 1 },
                new AnEntity { Id = Guid.NewGuid(), Int = 2 },
            };
            IQueryable<AnEntity> queryable = new CollectionAsyncQueryProvider<AnEntity>(datas);

            var result = await queryable.Select(x => x.Int).SumAsync();

            result.Should().Be(3);
        }

        [Fact]
        public async Task SumNullableIntAsync()
        {
            var datas = new[]
            {
                new AnEntity { Id = Guid.NewGuid(), NullableInt = 1 },
                new AnEntity { Id = Guid.NewGuid(), NullableInt = 2 },
                new AnEntity { Id = Guid.NewGuid(), NullableInt = null },
            };
            IQueryable<AnEntity> queryable = new CollectionAsyncQueryProvider<AnEntity>(datas);

            var result = await queryable.Select(x => x.NullableInt).SumAsync();

            result.Should().Be(3);
        }

        [Fact]
        public async Task SumLongAsync()
        {
            var datas = new[]
            {
                new AnEntity { Id = Guid.NewGuid(), Long = 1 },
                new AnEntity { Id = Guid.NewGuid(), Long = 2 },
            };
            IQueryable<AnEntity> queryable = new CollectionAsyncQueryProvider<AnEntity>(datas);

            var result = await queryable.Select(x => x.Long).SumAsync();

            result.Should().Be(3);
        }

        [Fact]
        public async Task SumNullableLongAsync()
        {
            var datas = new[]
            {
                new AnEntity { Id = Guid.NewGuid(), NullableLong = 1 },
                new AnEntity { Id = Guid.NewGuid(), NullableLong = 2 },
                new AnEntity { Id = Guid.NewGuid(), NullableLong = null },
            };
            IQueryable<AnEntity> queryable = new CollectionAsyncQueryProvider<AnEntity>(datas);

            var result = await queryable.Select(x => x.NullableLong).SumAsync();

            result.Should().Be(3);
        }

        [Fact]
        public async Task SumFloatAsync()
        {
            var datas = new[]
            {
                new AnEntity { Id = Guid.NewGuid(), Float = 1 },
                new AnEntity { Id = Guid.NewGuid(), Float = 2 },
            };
            IQueryable<AnEntity> queryable = new CollectionAsyncQueryProvider<AnEntity>(datas);

            var result = await queryable.Select(x => x.Float).SumAsync();

            result.Should().Be(3);
        }

        [Fact]
        public async Task SumNullableFloatAsync()
        {
            var datas = new[]
            {
                new AnEntity { Id = Guid.NewGuid(), NullableFloat = 1 },
                new AnEntity { Id = Guid.NewGuid(), NullableFloat = 2 },
                new AnEntity { Id = Guid.NewGuid(), NullableFloat = null },
            };
            IQueryable<AnEntity> queryable = new CollectionAsyncQueryProvider<AnEntity>(datas);

            var result = await queryable.Select(x => x.NullableFloat).SumAsync();

            result.Should().Be(3);
        }

        [Fact]
        public async Task SumDoubleAsync()
        {
            var datas = new[]
            {
                new AnEntity { Id = Guid.NewGuid(), Double = 1 },
                new AnEntity { Id = Guid.NewGuid(), Double = 2 },
            };
            IQueryable<AnEntity> queryable = new CollectionAsyncQueryProvider<AnEntity>(datas);

            var result = await queryable.Select(x => x.Double).SumAsync();

            result.Should().Be(3);
        }

        [Fact]
        public async Task SumNullableDoubleAsync()
        {
            var datas = new[]
            {
                new AnEntity { Id = Guid.NewGuid(), NullableDouble = 1 },
                new AnEntity { Id = Guid.NewGuid(), NullableDouble = 2 },
                new AnEntity { Id = Guid.NewGuid(), NullableDouble = null },
            };
            IQueryable<AnEntity> queryable = new CollectionAsyncQueryProvider<AnEntity>(datas);

            var result = await queryable.Select(x => x.NullableDouble).SumAsync();

            result.Should().Be(3);
        }

        [Fact]
        public async Task SumDecimalAsync()
        {
            var datas = new[]
            {
                new AnEntity { Id = Guid.NewGuid(), Decimal = 1 },
                new AnEntity { Id = Guid.NewGuid(), Decimal = 2 },
            };
            IQueryable<AnEntity> queryable = new CollectionAsyncQueryProvider<AnEntity>(datas);

            var result = await queryable.Select(x => x.Decimal).SumAsync();

            result.Should().Be(3);
        }

        [Fact]
        public async Task SumNullableDecimalAsync()
        {
            var datas = new[]
            {
                new AnEntity { Id = Guid.NewGuid(), NullableDecimal = 1 },
                new AnEntity { Id = Guid.NewGuid(), NullableDecimal = 2 },
                new AnEntity { Id = Guid.NewGuid(), NullableDecimal = null },
            };
            IQueryable<AnEntity> queryable = new CollectionAsyncQueryProvider<AnEntity>(datas);

            var result = await queryable.Select(x => x.NullableDecimal).SumAsync();

            result.Should().Be(3);
        }

        [Fact]
        public async Task MinAsync()
        {
            var datas = new[]
            {
                new AnEntity { Id = Guid.NewGuid(), Int = 1 },
                new AnEntity { Id = Guid.NewGuid(), Int = 2 },
            };
            IQueryable<AnEntity> queryable = new CollectionAsyncQueryProvider<AnEntity>(datas);

            var result = await queryable.Select(x => x.Int).MinAsync();

            result.Should().Be(1);
        }

        [Fact]
        public async Task MaxAsync()
        {
            var datas = new[]
            {
                new AnEntity { Id = Guid.NewGuid(), Int = 1 },
                new AnEntity { Id = Guid.NewGuid(), Int = 2 },
            };
            IQueryable<AnEntity> queryable = new CollectionAsyncQueryProvider<AnEntity>(datas);

            var result = await queryable.Select(x => x.Int).MaxAsync();

            result.Should().Be(2);
        }

        public class AnEntity : IEntity
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public int Int { get; set; }
            public int? NullableInt { get; set; }
            public long Long { get; set; }
            public long? NullableLong { get; set; }
            public float Float { get; set; }
            public float? NullableFloat { get; set; }
            public double Double { get; set; }
            public double? NullableDouble { get; set; }
            public decimal Decimal { get; set; }
            public decimal? NullableDecimal { get; set; }
        }
    }
}
