﻿using CocoriCore.Linq.Async;
using FluentAssertions;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace CocoriCore.Repository.Test
{
    public class AsyncQueryableExtensionsTest
    {
        [Fact]
        public async Task ToArrayAsync_exceptionWhileEnumerate_disposeEnumeratorAsync()
        {
            var cpt = 0;
            var queryableMock = new Mock<IQueryable<object>>();
            var enumerableMock = new Mock<IAsyncEnumerable<object>>();
            var enumeratorMock = new Mock<IAsyncEnumerator<object>>();
            queryableMock.As<ICocoriCoreQueryProvider>().Setup(x => x.AsAsyncEnumerable<object>(It.IsAny<Expression>())).Returns(enumerableMock.Object);
            enumerableMock.Setup(x => x.GetAsyncEnumerator(It.IsAny<CancellationToken>())).Returns(enumeratorMock.Object);
            enumeratorMock.Setup(x => x.MoveNextAsync()).Callback(() =>
            {
                if (cpt++ >= 2) throw new Exception("error");
            })
            .Returns(ValueTask.FromResult(true));

            Func<Task> action = () => queryableMock.Object.ToArrayAsync();

            await action.Should().ThrowAsync<Exception>().WithMessage("error");
            enumeratorMock.Verify(x => x.MoveNextAsync(), Times.Exactly(3));
            enumeratorMock.Verify(x => x.DisposeAsync(), Times.Once());
        }

        [Fact]
        public async Task ToArrayAsync_noExceptionWhileEnumerate_disposeEnumeratorAsync()
        {
            var cpt = 0;
            var queryableMock = new Mock<IQueryable<object>>();
            var enumerableMock = new Mock<IAsyncEnumerable<object>>();
            var enumeratorMock = new Mock<IAsyncEnumerator<object>>();
            queryableMock.As<ICocoriCoreQueryProvider>().Setup(x => x.AsAsyncEnumerable<object>(It.IsAny<Expression>())).Returns(enumerableMock.Object);
            enumerableMock.Setup(x => x.GetAsyncEnumerator(It.IsAny<CancellationToken>())).Returns(enumeratorMock.Object);
            enumeratorMock.Setup(x => x.MoveNextAsync()).Returns(() => ValueTask.FromResult(cpt++ < 2));

            Func<Task> action = () => queryableMock.Object.ToArrayAsync();

            await action.Should().NotThrowAsync<Exception>();
            enumeratorMock.Verify(x => x.MoveNextAsync(), Times.Exactly(3));
            enumeratorMock.Verify(x => x.DisposeAsync(), Times.Once());
        }
    }
}
