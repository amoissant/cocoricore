﻿using Autofac;
using System;
using CocoriCore.Common;
using CocoriCore.Autofac;
using FluentAssertions;
using System.Threading.Tasks;
using System.Linq;
using Xunit;
using CocoriCore.TestUtils;
using System.Collections.Generic;
using CocoriCore.Linq.Async;

namespace CocoriCore.Repository.Test
{
    public class CacheDecoratorTest
    {
        private Lazy<ILifetimeScope> _rootScope;
        private ContainerBuilder _builder;
        private RepositoryOptionsBuilder _repositoryOptionBuilder;

        public CacheDecoratorTest()
        {
            _builder = new ContainerBuilder();
            _rootScope = new Lazy<ILifetimeScope>(() => _builder.Build());
            _repositoryOptionBuilder = new RepositoryOptionsBuilder();
            _builder.Register(c => _repositoryOptionBuilder.Options).SingleInstance();
            
            _builder.RegisterType<TransactionalInMemoryBaseRepository>()
                .AsImplementedInterfaces()
                .SingleInstance();
            _builder.RegisterType<InMemoryEntityStore>().As<IInMemoryEntityStore>().SingleInstance();
            _builder.RegisterType<Utf8JsonCopier>().As<ICopier>().InstancePerLifetimeScope();
            _builder.RegisterType<PersistenceCallDecorator>().AsSelf().InstancePerLifetimeScope();
            _builder.RegisterType<CacheDecorator>().AsSelf().InstancePerLifetimeScope();

            _builder.RegisterType<AppRepository>()
                .As<IRepository>()
                .WithParameter(new TypedParameter<IEnumerable<Type>>(new[] { typeof(CacheDecorator), typeof(PersistenceCallDecorator) }))
                .InstancePerLifetimeScope();

            _builder.Register(c => new UnitOfWorkOptionsBuilder().Options).SingleInstance();
            _builder.RegisterType<AutofacUnitOfWorkFactory>().As<IUnitOfWorkFactory>().SingleInstance();
            _builder.RegisterType<AutofacUnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();
        }

        private ILifetimeScope RootScope => _rootScope.Value;

        private IRepository PersitentStore => RootScope.Resolve<IRepository>();//TODO retourner le IPersistentStore et faire des méthodes d'extension spéciales pour les tests (et synchornes) pour aider aux assertions

        private IUnitOfWorkFactory UnitOfWorkFactory => RootScope.Resolve<IUnitOfWorkFactory>();


        [Fact]
        public async Task CacheLoad()
        {
            await PersitentStore.InsertManyAsync(new[]
            {
                new AnEntity { Id = Tuid.Guid("1") }
            });
            await PersitentStore.FlushAsync();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var counter = uow.Resolve<PersistenceCallDecorator>();
                counter.LoadCount.Should().Be(0);

                var entity1 = await repository.LoadAsync<AnEntity>(Tuid.Guid("1"));
                entity1.Id.Should().Be(Tuid.Guid("1"));
                counter.LoadCount.Should().Be(1);

                var entity2 = await repository.LoadAsync<AnEntity>(Tuid.Guid("1"));
                entity2.Id.Should().Be(Tuid.Guid("1"));
                counter.LoadCount.Should().Be(1);
                entity2.Should().BeSameAs(entity1);
            }
        }

        [Fact]
        public async Task CacheLoadMany()
        {
            await PersitentStore.InsertManyAsync(new[]
            {
                new AnEntity { Id = Tuid.Guid("1") }
            });
            await PersitentStore.FlushAsync();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var counter = uow.Resolve<PersistenceCallDecorator>();

                counter.LoadCount.Should().Be(0);

                var entities1 = await repository.LoadAsync<AnEntity>(new[] { Tuid.Guid("1") });
                entities1.Single().Id.Should().Be(Tuid.Guid("1"));
                counter.LoadCount.Should().Be(1);

                var entities2 = await repository.LoadAsync<AnEntity>(new[] { Tuid.Guid("1") });
                entities2.Single().Id.Should().Be(Tuid.Guid("1"));
                counter.LoadCount.Should().Be(1);
                entities2.Single().Should().BeSameAs(entities1.Single());
            }
        }

        [Fact]
        public async Task CacheLoadUnique()
        {
            await PersitentStore.InsertManyAsync(new[]
            {
                new AnEntity { Id = Tuid.Guid("1") }
            });
            await PersitentStore.FlushAsync();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var counter = uow.Resolve<PersistenceCallDecorator>();
                counter.LoadCount.Should().Be(0);

                var entity1 = await repository.LoadAsync<AnEntity>(x => x.Id, Tuid.Guid("1"));
                entity1.Id.Should().Be(Tuid.Guid("1"));
                counter.LoadCount.Should().Be(1);

                var entity2 = await repository.LoadAsync<AnEntity>(x => x.Id, Tuid.Guid("1"));
                entity2.Id.Should().Be(Tuid.Guid("1"));
                counter.LoadCount.Should().Be(1);
                entity2.Should().BeSameAs(entity1);

                var entity3 = await repository.LoadAsync<AnEntity>(Tuid.Guid("1"));
                entity3.Id.Should().Be(Tuid.Guid("1"));
                counter.LoadCount.Should().Be(1);
                entity3.Should().BeSameAs(entity1);
            }
        }

        [Fact]
        public async Task CacheQuery()
        {
            await PersitentStore.InsertManyAsync(new[]
            {
                new AnEntity { Id = Tuid.Guid("1") }
            });
            await PersitentStore.FlushAsync();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var counter = uow.Resolve<PersistenceCallDecorator>();
                counter.LoadCount.Should().Be(0);

                var entities1 = await repository.Query<AnEntity>().ToArrayAsync();
                entities1.Single().Id.Should().Be(Tuid.Guid("1"));
                counter.QueryCount.Should().Be(1);

                var entities2 = await repository.Query<AnEntity>().ToArrayAsync();
                entities2.Single().Id.Should().Be(Tuid.Guid("1"));
                counter.QueryCount.Should().Be(2);

                var entity3 = await repository.LoadAsync<AnEntity>(Tuid.Guid("1"));
                entity3.Id.Should().Be(Tuid.Guid("1"));
                counter.LoadCount.Should().Be(0);
                entity3.Should().BeSameAs(entities1.Single());
            }
        }

        [Fact]
        public async Task CacheInsert()
        {
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var counter = uow.Resolve<PersistenceCallDecorator>();
                counter.LoadCount.Should().Be(0);

                var entity1 = new AnEntity { Id = Tuid.Guid("1") };
                await repository.InsertAsync(entity1);
                await repository.FlushAsync();
                counter.InsertCount.Should().Be(1);

                var entity2 = await repository.LoadAsync<AnEntity>(Tuid.Guid("1"));
                entity2.Id.Should().Be(Tuid.Guid("1"));
                counter.LoadCount.Should().Be(0);
                entity2.Should().BeSameAs(entity1);
            }
        }

        [Fact]
        public async Task CacheHandleTypeHierarchy()
        {
            await PersitentStore.InsertManyAsync(new[]
            {
                new AnEntity { Id = Tuid.Guid("1") }
            });
            await PersitentStore.FlushAsync();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var counter = uow.Resolve<PersistenceCallDecorator>();
                counter.LoadCount.Should().Be(0);

                var entity1 = await repository.LoadAsync<AnEntity>(Tuid.Guid("1"));
                entity1.Id.Should().Be(Tuid.Guid("1"));
                counter.LoadCount.Should().Be(1);

                var entity2 = await repository.LoadAsync<IEntity>(Tuid.Guid("1"));
                entity2.Id.Should().Be(Tuid.Guid("1"));
                counter.LoadCount.Should().Be(1);
                entity2.Should().BeSameAs(entity1);
            }
        }

        public class AppRepository : DecorableRepository, IRepository
        {
            public AppRepository(IBaseRepository baseRepository, IUnitOfWork unitOfWork, IEnumerable<Type> decoratorTypes)
                : base(baseRepository, unitOfWork, decoratorTypes)
            {
            }
        }

        public class AnEntity : IEntity
        {
            public Guid Id { get; set; }
        }
    }
}
