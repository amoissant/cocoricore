﻿using Autofac;
using CocoriCore.Autofac;
using CocoriCore.Common;
using CocoriCore.TestUtils;
using FluentAssertions;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace CocoriCore.Repository.Test
{
    public class TransactionalMemoryRepositoryTest
    {
        private readonly Lazy<ILifetimeScope> _rootScope;
        private readonly ContainerBuilder _builder;

        public TransactionalMemoryRepositoryTest()
        {
            _builder = new ContainerBuilder();
            _rootScope = new Lazy<ILifetimeScope>(() => _builder.Build());

            _builder.RegisterType<InMemoryEntityStore>().As<IInMemoryEntityStore>().SingleInstance();
            _builder.RegisterType<TransactionalInMemoryBaseRepository>().AsSelf().InstancePerLifetimeScope();
            _builder.RegisterType<Utf8JsonCopier>().As<ICopier>().InstancePerLifetimeScope();

            _builder.Register(c => new UnitOfWorkOptionsBuilder().Options).As<UnitOfWorkOptions>().SingleInstance();
            _builder.RegisterType<AutofacUnitOfWorkFactory>().As<IUnitOfWorkFactory>().SingleInstance();
            _builder.RegisterType<AutofacUnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();
        }

        private IInMemoryEntityStore PersistentStore => RootScope.Resolve<IInMemoryEntityStore>();

        private ILifetimeScope RootScope => _rootScope.Value;

        private TransactionalInMemoryBaseRepository CreateRepository()
        {
            return RootScope.Resolve<TransactionalInMemoryBaseRepository>();
        }

        [Fact]
        public async Task CanInsertEntity()
        {
            var id = Guid.NewGuid();
            using (var repository = CreateRepository())
            {
                var entity = new AnEntity();
                entity.Id = id;
                entity.Name = "toto";
                await repository.InsertAsync(entity);

                PersistentStore.Contains(typeof(AnEntity), id).Should().BeFalse();

                await repository.FlushAsync();

                PersistentStore.Contains(typeof(AnEntity), id).Should().BeTrue();
            }
        }

        [Fact]
        public async Task CanUpdateEntity()
        {
            var entity = new AnEntity().WithId();
            entity.Name = string.Empty;
            PersistentStore.Add(entity);

            using (var repository = CreateRepository())
            {
                var entities = await repository.LoadAsync(typeof(AnEntity), entity.Id);
                var loadedEntity = (AnEntity)entities.Single();
                loadedEntity.Name = "toto";

                loadedEntity.Should().NotBeSameAs(entity);
                entity.Name.Should().BeEmpty();

                await repository.UpdateAsync(loadedEntity);

                PersistentStore.Get(typeof(AnEntity), entity.Id).Should().ContainSingle().Which
                    .Should().BeOfType<AnEntity>().Which
                    .Name.Should().BeEmpty();

                await repository.FlushAsync();

                PersistentStore.Get(typeof(AnEntity), entity.Id).Should().ContainSingle().Which
                    .Should().BeOfType<AnEntity>().Which
                    .Name.Should().Be("toto");
            }
        }

        [Fact]
        public async Task CanDeleteEntity()
        {
            var entity = new AnEntity().WithId();
            entity.Name = string.Empty;
            PersistentStore.Add(entity);

            using (var repository = CreateRepository())
            {
                var entities = await repository.LoadAsync(typeof(AnEntity), entity.Id);
                var loadedEntity = (AnEntity)entities.Single();
                loadedEntity.Name = "toto";

                loadedEntity.Should().NotBeSameAs(entity);
                entity.Name.Should().BeEmpty();

                await repository.DeleteAsync(loadedEntity);

                PersistentStore.Contains(typeof(AnEntity), entity.Id).Should().BeTrue();

                await repository.FlushAsync();

                PersistentStore.Contains(typeof(AnEntity), entity.Id).Should().BeFalse();
            }
        }

        [Fact]
        public async Task CanRollbackAfterInsertNoFlush()
        {
            var id = Guid.NewGuid();
            using (var repository = CreateRepository())
            {
                var entity = new AnEntity();
                entity.Id = id;
                entity.Name = "toto";
                await repository.InsertAsync(entity);

                PersistentStore.Contains(typeof(AnEntity), entity.Id).Should().BeFalse();

                await repository.RollbackAsync();

                PersistentStore.Contains(typeof(AnEntity), id).Should().BeFalse();
            }
        }

        [Fact]
        public async Task CanRollbackAfterInsertAndFlush()
        {
            var id = Guid.NewGuid();
            using (var repository = CreateRepository())
            {
                var entity = new AnEntity();
                entity.Id = id;
                entity.Name = "toto";
                await repository.InsertAsync(entity);
                await repository.FlushAsync();

                PersistentStore.Contains(typeof(AnEntity), entity.Id).Should().BeTrue();

                await repository.RollbackAsync();

                PersistentStore.Contains(typeof(AnEntity), id).Should().BeFalse();
            }
        }

        [Fact]
        public async Task CanRollbackAfterUpdateNoFlush()
        {
            var entity = new AnEntity().WithId();
            PersistentStore.Add(entity);

            using (var repository = CreateRepository())
            {
                var entities = await repository.LoadAsync(typeof(AnEntity), entity.Id);
                var loadedEntity = (AnEntity)entities.Single();
                loadedEntity.Name = "toto";
                await repository.UpdateAsync(loadedEntity);

                PersistentStore.Get(typeof(AnEntity), entity.Id).Should().ContainSingle().Which
                    .Should().BeOfType<AnEntity>().Which
                    .Name.Should().BeNull();

                await repository.RollbackAsync();

                PersistentStore.Get(typeof(AnEntity), entity.Id).Should().ContainSingle().Which
                    .Should().BeOfType<AnEntity>().Which
                    .Name.Should().BeNull();
            }
        }

        [Fact]
        public async Task CanRollbackAfterUpdateAndFlush()
        {
            var entity = new AnEntity().WithId();
            PersistentStore.Add(entity);

            using (var repository = CreateRepository())
            {
                var entities = await repository.LoadAsync(typeof(AnEntity), entity.Id);
                var loadedEntity = (AnEntity)entities.Single();
                loadedEntity.Name = "toto";
                await repository.UpdateAsync(loadedEntity);
                await repository.FlushAsync();

                PersistentStore.Get(typeof(AnEntity), entity.Id).Should().ContainSingle().Which
                    .Should().BeOfType<AnEntity>().Which
                    .Name.Should().Be("toto");

                await repository.RollbackAsync();

                PersistentStore.Get(typeof(AnEntity), entity.Id).Should().ContainSingle().Which
                    .Should().BeOfType<AnEntity>().Which
                    .Name.Should().BeNull();
            }
        }

        [Fact]
        public async Task CanRollbackAfterDeleteNoFlush()
        {
            var entity = new AnEntity().WithId();
            PersistentStore.Add(entity);

            using (var repository = CreateRepository())
            {
                var entities = await repository.LoadAsync(typeof(AnEntity), entity.Id);
                var loadedEntity = (AnEntity)entities.Single();
                loadedEntity.Name = "toto";
                await repository.DeleteAsync(loadedEntity);

                PersistentStore.Contains(typeof(AnEntity), entity.Id).Should().BeTrue();

                await repository.RollbackAsync();

                PersistentStore.Contains(typeof(AnEntity), entity.Id).Should().BeTrue();
            }
        }

        [Fact]
        public async Task CanRollbackAfterDeleteAndFlush()
        {
            var entity = new AnEntity().WithId();
            PersistentStore.Add(entity);

            using (var repository = CreateRepository())
            {
                var entities = await repository.LoadAsync(typeof(AnEntity), entity.Id);
                var loadedEntity = (AnEntity)entities.Single();
                loadedEntity.Name = "toto";
                await repository.DeleteAsync(loadedEntity);
                await repository.FlushAsync();

                PersistentStore.Contains(typeof(AnEntity), entity.Id).Should().BeFalse();

                await repository.RollbackAsync();

                PersistentStore.Contains(typeof(AnEntity), entity.Id).Should().BeTrue();
            }
        }

        public class AnEntity : IEntity
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public Guid? NullableId { get; set; }
        }

        public abstract class AnAbstractEntity : IEntity
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
        }

        public class AConcreteEntity1 : AnAbstractEntity
        {
            public DateTime Date { get; set; }
        }

        public class AConcreteEntity2 : AnAbstractEntity
        {
            public int Age { get; set; }
        }

        public class AnotherEntity : IEntity
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
        }
    }
}