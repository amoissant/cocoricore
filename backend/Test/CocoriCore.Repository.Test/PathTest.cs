﻿using CocoriCore.Common;
using FluentAssertions;
using System;
using Xunit;

namespace CocoriCore.Repository.Test
{
    public class PathTest
    {
        [Theory]
        [InlineData("dir1/dir2/dir3")]
        [InlineData("dir1\\dir2\\dir3")]
        public void CanCreatePathFromRelativeDirPath(string stringPath)
        {
            var path = new Path(stringPath);
            path.ToString().Should().Be("dir1/dir2/dir3");
            path.GetExtension().Should().BeNull();
            path.GetParent().ToString().Should().Be("dir1/dir2");
            path.GetName().Should().Be("dir3");
            path.GetMimeType().Should().BeNull();
            path.IsEmpty().Should().BeFalse();
            path.Segments.Should().ContainInOrder("dir1", "dir2", "dir3");
        }

        [Theory]
        [InlineData("dir1/dir2/filename.txt")]
        [InlineData("dir1\\dir2\\filename.txt")]
        public void CanCreatePathFromRelativeFilePath(string stringPath)
        {
            var path = new Path(stringPath);
            path.ToString().Should().Be("dir1/dir2/filename.txt");
            path.GetExtension().Should().Be(".txt");
            path.GetParent().ToString().Should().Be("dir1/dir2");
            path.GetFileName().Should().Be("filename.txt");
            path.GetMimeType().Should().Be("text/plain");
            path.IsEmpty().Should().BeFalse();
            path.Segments.Should().ContainInOrder("dir1", "dir2", "filename.txt");
        }

        [Theory]
        [InlineData("dir1/dir2/filename.ext")]
        [InlineData("dir1\\dir2\\filename.ext")]
        public void CanImplicitlyConvertStringToPath(string stringPath)
        {
            Path path = stringPath;
            path.ToString().Should().Be("dir1/dir2/filename.ext");
            path.GetExtension().Should().Be(".ext");
            path.GetParent().ToString().Should().Be("dir1/dir2");
            path.GetFileName().Should().Be("filename.ext");
            path.IsEmpty().Should().BeFalse();

            ((string)path).Should().Be("dir1/dir2/filename.ext");

            Path emptyPath = null;
            emptyPath.Should().BeNull();
            ((string)emptyPath).Should().BeNull();
        }

        [Fact]
        public void CanCheckEqualityBetweenPath()
        {
            var path1 = new Path("dir1/dir2/filename.ext");
            var path2 = new Path("dir1/dir2/filename.ext");
            var path3 = new Path("dir1/dir2");
            var path4 = new Path("DIR1/DIR2");

            path1.Equals(path2).Should().BeTrue();
            path1.Equals(path3).Should().BeFalse();
            (path1 == path2).Should().BeTrue();
            (path1 == path3).Should().BeFalse();
            (path1 != path2).Should().BeFalse();
            (path1 != path3).Should().BeTrue();
            path3.Equals(path4, true).Should().BeFalse();
            path3.Equals(path4, false).Should().BeTrue();
            (path1 != null).Should().BeTrue();
            (path1 == null).Should().BeFalse();
            (null != path1).Should().BeTrue();
            (null == path1).Should().BeFalse();
        }

        [Fact]
        public void CanCreateEmptyPath()
        {
            var path1 = new Path();

            path1.IsEmpty().Should().BeTrue();
            path1.ToString().Should().Be("");
            path1.Segments.Should().BeEmpty();

            Action action = () => path1.GetParent();
            action.Should().Throw<InvalidOperationException>();
        }

        [Fact]
        public void RemoveInvalidSegments()
        {
            var path = new Path("dir0/dir1//dir2/ \\dir3\\\\dir4");

            path.ToString().Should().Be("dir0/dir1/dir2/dir3/dir4");
            path.Segments.Should().ContainInOrder("dir0", "dir1", "dir2", "dir3", "dir4");
        }

        [Fact]
        public void CanCreatePathFromOtherPathsOrObject()
        {
            var path1 = new Path("dir0/dir1");
            var path2 = new Path("dir2/dir3");
            var path = new Path(path1, path2, 2, "dir4", null, MyEnum.Val1, new[] { "a", "b" });

            path.ToString().Should().Be("dir0/dir1/dir2/dir3/2/dir4/Val1/a/b");
            path.Segments.Should().ContainInOrder("dir0", "dir1", "dir2", "dir3", "2", "dir4", "Val1", "a", "b");
        }

        [Fact]
        public void CanRemoveFirstSegment()
        {
            var path = new Path("dir0/dir1/dir2");

            path.RemoveFirstSegment().ToString().Should().Be("dir1/dir2");

            var emptyPath = new Path();

            Action action = () => emptyPath.RemoveFirstSegment();

            action.Should().Throw<InvalidOperationException>();
        }

        [Fact]
        public void CanRemoveExtension()
        {
            var dirPath = new Path("dir1/dir2");

            dirPath.RemoveExtension().ToString().Should().Be("dir1/dir2");

            var filePath = new Path("dir1/test.png");

            filePath.RemoveExtension().ToString().Should().Be("dir1/test");
        }

        [Fact]
        public void CanAddExtension()
        {
            var path = new Path("dir1/filename");

            path.AddExtension(".png").ToString().Should().Be("dir1/filename.png");
            path.AddExtension("png").ToString().Should().Be("dir1/filename.png");
            path.AddExtension("png").AddExtension("back").ToString().Should().Be("dir1/filename.png.back");
        }

        [Fact]
        public void CanAddExtensionFromMimeType()
        {
            var path = new Path("dir1/filename");

            path.AddExtensionFromMimeType("image/png").ToString().Should().Be("dir1/filename.png");
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void CanAddExtensionFromEmptyOrNullMimeType(string mimeType)
        {
            var path = new Path("dir1/filename");

            path.AddExtensionFromMimeType(mimeType).ToString().Should().Be("dir1/filename");
        }

        [Fact]
        public void CanGetPathRelativeToAnother()
        {
            var path1 = new Path("dir1/dir2/dir3/filename");
            var path2 = new Path("dir1/dir2");

            path1.RelativeTo(path2).ToString().Should().Be("dir3/filename");

            Action action = () => path2.RelativeTo(path1);
            action.Should().Throw<InvalidOperationException>();
        }

        [Fact]
        public void CanUseAbsoluteWindowsPath()
        {
            var path1 = new Path("c:\\dir1");
            var path2 = new Path("dir1");
            var path3 = new Path(path1, "toto");

            path1.IsAbsolute.Should().BeTrue();
            path1.Root.Should().Be("c:");
            path1.Segments.Should().ContainInOrder("dir1");
            path1.ToString().Should().Be("c:/dir1");
            path2.IsAbsolute.Should().BeFalse();
            path2.Root.Should().Be(string.Empty);
            path3.IsAbsolute.Should().BeTrue();
            path3.Root.Should().Be("c:");
            path3.Segments.Should().ContainInOrder("dir1", "toto");
            path3.ToString().Should().Be("c:/dir1/toto");
        }

        [Fact]
        public void CanUseAbsoluteLinuxPath()
        {
            var path1 = new Path("/dir1");
            var path2 = new Path("dir1");
            var path3 = new Path(path1, "toto");

            path1.IsAbsolute.Should().BeTrue();
            path1.Root.Should().Be("/");
            path1.ToString().Should().Be("/dir1");
            path2.IsAbsolute.Should().BeFalse();
            path2.Root.Should().Be(string.Empty);
            path2.ToString().Should().Be("dir1");
            path3.IsAbsolute.Should().BeTrue();
            path3.Root.Should().Be("/");
            path3.ToString().Should().Be("/dir1/toto");
        }

        [Fact]
        public void CanUseAbsoluteUNCPath()
        {
            var path1 = new Path("\\\\dir1");
            var path2 = new Path("dir1");
            var path3 = new Path("a");
            var path4 = new Path(path1, "toto");

            path1.IsAbsolute.Should().BeTrue();
            path1.Root.Should().Be("\\\\");
            path2.IsAbsolute.Should().BeFalse();
            path2.Root.Should().Be(string.Empty);
            path3.IsAbsolute.Should().BeFalse();
            path3.Root.Should().Be(string.Empty);
            path4.IsAbsolute.Should().BeTrue();
            path4.Root.Should().Be("\\\\");
        }

        [Fact]
        public void CanAppendToRelativeOrAbsolutePath()
        {
            var path1 = new Path("\\\\dir1");
            var path2 = new Path("/dir1");
            var path3 = new Path("c:\\dir1");
            var path4 = new Path("dir1");

            path1.Append("test").ToString().Should().Be("\\\\dir1/test");
            path2.Append("test").ToString().Should().Be("/dir1/test");
            path3.Append("test").ToString().Should().Be("c:/dir1/test");
            path4.Append("test").ToString().Should().Be("dir1/test");
        }

        public enum MyEnum
        {
            Val1,
            Val2
        }
    }
}
