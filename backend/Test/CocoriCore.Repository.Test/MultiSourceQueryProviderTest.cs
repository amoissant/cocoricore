﻿using Autofac;
using System;
using CocoriCore.Common;
using FluentAssertions;
using System.Threading.Tasks;
using System.Linq;
using Xunit;
using CocoriCore.TestUtils;
using CocoriCore.Linq.Async;

namespace CocoriCore.Repository.Test
{
    public class MultiSourceQueryProviderTest
    {
        private InMemoryEntityStore _entityStore;

        public MultiSourceQueryProviderTest()
        {
            _entityStore = new InMemoryEntityStore();
        }

        private IInMemoryEntityStore EntityStore => _entityStore;

        [Fact]
        public async Task ListElementsFromSeveralQueryables()
        {
            EntityStore.Add(new AnEntityA { Id = Tuid.Guid("1") });
            EntityStore.Add(new AnEntityB { Id = Tuid.Guid("2") });

            IQueryable<IEntity> queryable = new MultiSourceQueryProvider<IEntity>(
                GetQueryable<AnEntityA>(),
                GetQueryable<AnEntityB>(),
                GetQueryable<AnEntityC>());

            var results = queryable.ToList();
            var asyncResult = await queryable.ToListAsync();

            results.Should().HaveCount(2);
            results.Select(x => x.Id).Should().Contain(Tuid.Guid("1")).And.Contain(Tuid.Guid("2"));
            asyncResult.Should().HaveCount(2);
            asyncResult.Select(x => x.Id).Should().Contain(Tuid.Guid("1")).And.Contain(Tuid.Guid("2"));
        }

        [Fact]
        public async Task GetFirstElementFromSeveralQueryables()
        {
            EntityStore.Add(new AnEntityA { Id = Tuid.Guid("1") });
            EntityStore.Add(new AnEntityB { Id = Tuid.Guid("2") });

            IQueryable<INamedEntity> queryable = new MultiSourceQueryProvider<INamedEntity>(
                GetQueryable<AnEntityA>(),
                GetQueryable<AnEntityB>(),
                GetQueryable<AnEntityC>());

            var result = queryable.Where(x => x.Name == null).First();
            Action action = () => queryable.Where(x => x.Name != null).First();
            var asyncResult = await queryable.Where(x => x.Name == null).FirstAsync();
            Func<Task> asyncAction = () => queryable.Where(x => x.Name != null).FirstAsync();

            result.Id.Should().Be(Tuid.Guid("1"));
            asyncResult.Id.Should().Be(Tuid.Guid("1"));
            action.Should().Throw<InvalidOperationException>().WithMessage("Sequence contains no elements");
            await asyncAction.Should().ThrowAsync<InvalidOperationException>().WithMessage("Sequence contains no elements");
        }

        [Fact]
        public async Task GetFirstOrDefaultElementFromSeveralQueryables()
        {
            EntityStore.Add(new AnEntityA { Id = Tuid.Guid("1") });
            EntityStore.Add(new AnEntityB { Id = Tuid.Guid("2") });

            IQueryable<INamedEntity> queryable = new MultiSourceQueryProvider<INamedEntity>(
                GetQueryable<AnEntityC>(),
                GetQueryable<AnEntityA>(),
                GetQueryable<AnEntityB>());

            var result1 = queryable.Where(x => x.Name == null).FirstOrDefault();
            var result2 = queryable.Where(x => x.Name != null).FirstOrDefault();
            var asyncResult1 = await queryable.Where(x => x.Name == null).FirstOrDefaultAsync();
            var asyncResult2 = await queryable.Where(x => x.Name != null).FirstOrDefaultAsync();

            result1.Id.Should().Be(Tuid.Guid("1"));
            result2.Should().BeNull();
            asyncResult1.Id.Should().Be(Tuid.Guid("1"));
            asyncResult2.Should().BeNull();            
        }

        [Fact]
        public async Task GetSingleElementFromSeveralQueryables()
        {
            EntityStore.Add(new AnEntityA { Id = Tuid.Guid("1") });
            EntityStore.Add(new AnEntityB { Id = Tuid.Guid("2") });

            IQueryable<INamedEntity> queryable = new MultiSourceQueryProvider<INamedEntity>(
                GetQueryable<AnEntityA>(),
                GetQueryable<AnEntityB>(),
                GetQueryable<AnEntityC>());

            var result = queryable.Where(x => x.Id == Tuid.Guid("2")).Single();
            Action action = () => queryable.Where(x => x.Id != Guid.Empty).Single();
            var asyncResult = await queryable.Where(x => x.Id == Tuid.Guid("2")).SingleAsync();
            Func<Task> asyncAction = () => queryable.Where(x => x.Id != Guid.Empty).SingleAsync();

            result.Id.Should().Be(Tuid.Guid("2"));
            asyncResult.Id.Should().Be(Tuid.Guid("2"));
            action.Should().Throw<InvalidOperationException>().WithMessage("Sequence contains more than one element");
            await asyncAction.Should().ThrowAsync<InvalidOperationException>().WithMessage("Sequence contains more than one element");
        }

        [Fact]
        public async Task GetSingleOrDefaultElementFromSeveralQueryables()
        {
            EntityStore.Add(new AnEntityA { Id = Tuid.Guid("1") });
            EntityStore.Add(new AnEntityB { Id = Tuid.Guid("2") });

            IQueryable<INamedEntity> queryable = new MultiSourceQueryProvider<INamedEntity>(
                GetQueryable<AnEntityA>(),
                GetQueryable<AnEntityB>(),
                GetQueryable<AnEntityC>());

            var result1 = queryable.Where(x => x.Id == Tuid.Guid("2")).SingleOrDefault();
            var result2 = queryable.Where(x => x.Id == Guid.Empty).SingleOrDefault();
            Action action = () => queryable.Where(x => x.Id != Guid.Empty).SingleOrDefault();
            var asyncResult1 = await queryable.Where(x => x.Id == Tuid.Guid("2")).SingleOrDefaultAsync();
            var asyncResult2 = await queryable.Where(x => x.Id == Guid.Empty).SingleOrDefaultAsync();
            Func<Task> asyncAction = () => queryable.Where(x => x.Id != Guid.Empty).SingleOrDefaultAsync();

            result1.Id.Should().Be(Tuid.Guid("2"));
            result2.Should().BeNull();
            action.Should().Throw<InvalidOperationException>().WithMessage("Sequence contains more than one element");
            asyncResult1.Id.Should().Be(Tuid.Guid("2"));
            asyncResult2.Should().BeNull();
            await asyncAction.Should().ThrowAsync<InvalidOperationException>().WithMessage("Sequence contains more than one element");
        }

        [Fact]
        public async Task CountElementFromSeveralQueryables()
        {
            EntityStore.Add(new AnEntityA { Id = Tuid.Guid("1") });
            EntityStore.Add(new AnEntityB { Id = Tuid.Guid("2") });

            IQueryable<INamedEntity> queryable = new MultiSourceQueryProvider<INamedEntity>(
                GetQueryable<AnEntityA>(),
                GetQueryable<AnEntityB>(),
                GetQueryable<AnEntityC>());

            var result = queryable.Count();
            var asyncResult = await queryable.CountAsync();

            result.Should().Be(2);
            asyncResult.Should().Be(2);
        }

        [Fact]
        public async Task GetMinFromSeveralQueryables()
        {
            EntityStore.Add(new AnEntityA { Age = 17 });
            EntityStore.Add(new AnEntityB { Age = 12 });

            IQueryable<INamedEntity> queryable = new MultiSourceQueryProvider<INamedEntity>(
                GetQueryable<AnEntityA>(),
                GetQueryable<AnEntityB>(),
                GetQueryable<AnEntityC>());

            var result = queryable.Select(x => x.Age).Min();
            var asyncResult = await queryable.Select(x => x.Age).MinAsync();

            result.Should().Be(12);
            asyncResult.Should().Be(12);
        }

        [Fact]
        public async Task GetMaxFromSeveralQueryables()
        {
            EntityStore.Add(new AnEntityA { Age = 17 });
            EntityStore.Add(new AnEntityB { Age = 12 });

            IQueryable<INamedEntity> queryable = new MultiSourceQueryProvider<INamedEntity>(
                GetQueryable<AnEntityA>(),
                GetQueryable<AnEntityB>(),
                GetQueryable<AnEntityC>());

            var result = queryable.Select(x => x.Age).Max();
            var asyncResult = await queryable.Select(x => x.Age).MaxAsync();

            result.Should().Be(17);
            asyncResult.Should().Be(17);
        }

        [Fact]
        public async Task SumFromSeveralQueryables()
        {
            EntityStore.Add(new AnEntityA { Age = 17 });
            EntityStore.Add(new AnEntityB { Age = 12 });

            IQueryable<INamedEntity> queryable = new MultiSourceQueryProvider<INamedEntity>(
                GetQueryable<AnEntityA>(),
                GetQueryable<AnEntityB>(),
                GetQueryable<AnEntityC>());

            var result = queryable.Select(x => x.Age).Sum();
            var asyncResult = await queryable.Select(x => x.Age).SumAsync();

            result.Should().Be(29);
            asyncResult.Should().Be(29);
        }

        private IQueryable<T> GetQueryable<T>()
        {
            return new CollectionAsyncQueryProvider<T>(EntityStore.Query(typeof(T)));
        }

        public class AppRepository : DecorableRepository, IRepository
        {
            public AppRepository(IBaseRepository baseRepository, IUnitOfWork unitOfWork)
                : base(baseRepository, unitOfWork)
            {
            }
        }

        public interface INamedEntity : IEntity
        {
            string Name { get; set; }
            int Age { get; set; }
        }

        public class AnEntityA : INamedEntity
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public int Age { get; set; }
        }

        public class AnEntityB : INamedEntity
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public int Age { get; set; }
        }

        public class AnEntityC : INamedEntity
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public int Age { get; set; }
        }
    }
}
