using Autofac;
using System;
using System.Threading.Tasks;
using Xunit;
using CocoriCore.TestUtils;
using CocoriCore.Common;
using CocoriCore.Autofac;
using FluentAssertions;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using CocoriCore.Types.Extension;

namespace CocoriCore.Repository.Test
{
    public class DecorableRepositoryTest
    {
        private Lazy<ILifetimeScope> _rootScope;
        private ContainerBuilder _builder;
        private RepositoryOptionsBuilder _repositoryOptionBuilder;

        public DecorableRepositoryTest()
        {
            _builder = new ContainerBuilder();
            _rootScope = new Lazy<ILifetimeScope>(() => _builder.Build());
            _repositoryOptionBuilder = new RepositoryOptionsBuilder();
            _builder.Register(c => _repositoryOptionBuilder.Options).SingleInstance();
            _builder.RegisterType<AppRepository>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
            _builder.RegisterType<TransactionalInMemoryBaseRepository>()
                .AsImplementedInterfaces()
                .SingleInstance();
            _builder.RegisterType<InMemoryEntityStore>().As<IInMemoryEntityStore>().SingleInstance();
            _builder.RegisterType<StateStore>().As<IStateStore>().InstancePerLifetimeScope();
            _builder.RegisterType<Utf8JsonCopier>().As<ICopier>().InstancePerLifetimeScope();

            _builder.Register(c => new UnitOfWorkOptionsBuilder().Options).SingleInstance();
            _builder.RegisterType<AutofacUnitOfWorkFactory>().As<IUnitOfWorkFactory>().SingleInstance();
            _builder.RegisterType<AutofacUnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();
        }

        private ILifetimeScope RootScope => _rootScope.Value;

        private IBaseRepository BaseRepository => RootScope.Resolve<IBaseRepository>();//TODO d�finir une interface commune pour les persitent store qui permet de manipuler la bdd directement, on pourra s'en servir pour les tests unitaires aussi pour faire des assertions

        private IUnitOfWorkFactory UnitOfWorkFactory => RootScope.Resolve<IUnitOfWorkFactory>();

        [Fact]
        public async Task LoadAsyncNoDecorator()
        {
            var entity = new AnEntity().WithId();
            entity.Name = "myEntity";
            await BaseRepository.InsertAsync(entity);
            await BaseRepository.FlushAsync();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();

                var entity1 = await repository.LoadAsync(typeof(AnEntity), entity.Id);
                entity1.Should().BeOfType<AnEntity>()
                    .Which.Name.Should().Be("myEntity");

                var entity2 = await repository.LoadAsync<AnEntity>(entity.Id);
                entity2.Name.Should().Be("myEntity");

                var entity3 = await repository.LoadAsync<AnEntity>(x => x.Id, entity.Id);
                entity3.Name.Should().Be("myEntity");

                var entities4 = await repository.LoadAsync<AnEntity>(new[] { entity.Id });
                entities4.Should().ContainSingle()
                    .Which.Name.Should().Be("myEntity");

                var entities5 = await repository.LoadAsync(typeof(AnEntity), new[] { entity.Id });
                entities5.Should().ContainSingle()
                    .Which.Should().BeOfType<AnEntity>()
                    .Which.Name.Should().Be("myEntity");
            }
        }

        [Fact]
        public async Task LoadOneAsyncOneDecorator()
        {
            _repositoryOptionBuilder.AddDecorator<SpyDecorator1>();
            var callTraces = new List<string>();
            _builder.RegisterInstance(new SpyDecorator1(callTraces));

            var entity = new AnEntity().WithId();
            entity.Name = "myEntity";
            await BaseRepository.InsertAsync(entity);
            await BaseRepository.FlushAsync();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();

                var entity1 = await repository.LoadAsync(typeof(AnEntity), entity.Id);
                entity1.Should().BeOfType<AnEntity>()
                    .Which.Name.Should().Be("myEntity");
                callTraces[0].Should().Be("SpyDecorator1.LoadAsync(Type, Guid, LoadIdAsync)");

                var entity2 = await repository.LoadAsync<AnEntity>(entity.Id);
                entity2.Name.Should().Be("myEntity");
                callTraces[1].Should().Be("SpyDecorator1.LoadAsync(Type, Guid, LoadIdAsync)");
            }
        }

        [Fact]
        public async Task LoadUniqueAsyncOneDecorator()
        {
            _repositoryOptionBuilder.AddDecorator<SpyDecorator1>();
            var callTraces = new List<string>();
            _builder.RegisterInstance(new SpyDecorator1(callTraces));

            var entity = new AnEntity().WithId();
            entity.Name = "myEntity";
            await BaseRepository.InsertAsync(entity);
            await BaseRepository.FlushAsync();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();

                var entity1 = await repository.LoadAsync<AnEntity>(x => x.Id, entity.Id);
                entity1.Name.Should().Be("myEntity");
                callTraces[0].Should().Be("SpyDecorator1.LoadAsync(Type, MemberInfo, Object, LoadUniqueAsync)");
            }
        }

        [Fact]
        public async Task LoadManyAsyncOneDecorator()
        {
            _repositoryOptionBuilder.AddDecorator<SpyDecorator1>();
            var callTraces = new List<string>();
            _builder.RegisterInstance(new SpyDecorator1(callTraces));

            var entity = new AnEntity().WithId();
            entity.Name = "myEntity";
            await BaseRepository.InsertAsync(entity);
            await BaseRepository.FlushAsync();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();

                var entities1 = await repository.LoadAsync<AnEntity>(new[] { entity.Id });
                entities1.Should().ContainSingle()
                    .Which.Name.Should().Be("myEntity");
                callTraces[0].Should().Be("SpyDecorator1.LoadAsync(Type, IEnumerable<Guid>, LoadIdCollectionAsync)");

                var entities2 = await repository.LoadAsync(typeof(AnEntity), new[] { entity.Id });
                entities2.Should().ContainSingle()
                    .Which.Should().BeOfType<AnEntity>()
                    .Which.Name.Should().Be("myEntity");
                callTraces[1].Should().Be("SpyDecorator1.LoadAsync(Type, IEnumerable<Guid>, LoadIdCollectionAsync)");
            }
        }

        [Fact]
        public async Task LoadAsyncTwoDecorators()
        {
            _repositoryOptionBuilder
                .AddDecorator<SpyDecorator1>()
                .AddDecorator<SpyDecorator2>();
            var callTraces = new List<string>();
            _builder.RegisterInstance(new SpyDecorator1(callTraces));
            _builder.RegisterInstance(new SpyDecorator2(callTraces));

            var entity = new AnEntity().WithId();
            await BaseRepository.InsertAsync(entity);
            await BaseRepository.FlushAsync();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();

                var entity1 = await repository.LoadAsync(typeof(AnEntity), entity.Id);
                callTraces[0].Should().Be("SpyDecorator1.LoadAsync(Type, Guid, LoadIdAsync)");
                callTraces[1].Should().Be("SpyDecorator2.LoadAsync(Type, Guid, LoadIdAsync)");

                var entity2 = await repository.LoadAsync<AnEntity>(entity.Id);
                callTraces[2].Should().Be("SpyDecorator1.LoadAsync(Type, Guid, LoadIdAsync)");
                callTraces[3].Should().Be("SpyDecorator2.LoadAsync(Type, Guid, LoadIdAsync)");

                var entity3 = await repository.LoadAsync<AnEntity>(x => x.Id, entity.Id);
                callTraces[4].Should().Be("SpyDecorator1.LoadAsync(Type, MemberInfo, Object, LoadUniqueAsync)");
                callTraces[5].Should().Be("SpyDecorator2.LoadAsync(Type, MemberInfo, Object, LoadUniqueAsync)");

                var entities4 = await repository.LoadAsync<AnEntity>(new[] { entity.Id });
                callTraces[6].Should().Be("SpyDecorator1.LoadAsync(Type, IEnumerable<Guid>, LoadIdCollectionAsync)");
                callTraces[7].Should().Be("SpyDecorator2.LoadAsync(Type, IEnumerable<Guid>, LoadIdCollectionAsync)");

                var entities5 = await repository.LoadAsync(typeof(AnEntity), new[] { entity.Id });
                callTraces[8].Should().Be("SpyDecorator1.LoadAsync(Type, IEnumerable<Guid>, LoadIdCollectionAsync)");
                callTraces[9].Should().Be("SpyDecorator2.LoadAsync(Type, IEnumerable<Guid>, LoadIdCollectionAsync)");
            }
        }

        [Fact]
        public async Task ExistsOneWithDecorators()
        {
            _repositoryOptionBuilder
                .AddDecorator<SpyDecorator1>()
                .AddDecorator<SpyDecorator2>();
            var callTraces = new List<string>();
            _builder.RegisterInstance(new SpyDecorator1(callTraces));
            _builder.RegisterInstance(new SpyDecorator2(callTraces));

            var entity = new AnEntity().WithId();
            await BaseRepository.InsertAsync(entity);
            await BaseRepository.FlushAsync();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();

                var exists1 = await repository.ExistsAsync<AnEntity>(entity.Id);
                exists1.Should().BeTrue();
                callTraces[0].Should().Be("SpyDecorator1.LoadAsync(Type, Guid, LoadIdAsync)");
                callTraces[1].Should().Be("SpyDecorator2.LoadAsync(Type, Guid, LoadIdAsync)");

                var exists2 = await repository.ExistsAsync<AnEntity>(Guid.NewGuid());
                exists2.Should().BeFalse();
                callTraces[2].Should().Be("SpyDecorator1.LoadAsync(Type, Guid, LoadIdAsync)");
                callTraces[3].Should().Be("SpyDecorator2.LoadAsync(Type, Guid, LoadIdAsync)");

                var exists3 = await repository.ExistsAsync<AnEntity>(entity.Id);
                exists3.Should().BeTrue();
                callTraces[4].Should().Be("SpyDecorator1.LoadAsync(Type, Guid, LoadIdAsync)");
                callTraces[5].Should().Be("SpyDecorator2.LoadAsync(Type, Guid, LoadIdAsync)");

                var exists4 = await repository.ExistsAsync<AnEntity>(Guid.NewGuid());
                exists4.Should().BeFalse();
                callTraces[6].Should().Be("SpyDecorator1.LoadAsync(Type, Guid, LoadIdAsync)");
                callTraces[7].Should().Be("SpyDecorator2.LoadAsync(Type, Guid, LoadIdAsync)");
            }
        }

        [Fact]
        public async Task ExistsUniqueWithDecorators()
        {
            _repositoryOptionBuilder
                .AddDecorator<SpyDecorator1>()
                .AddDecorator<SpyDecorator2>();
            var callTraces = new List<string>();
            _builder.RegisterInstance(new SpyDecorator1(callTraces));
            _builder.RegisterInstance(new SpyDecorator2(callTraces));

            var entity = new AnEntity().WithId();
            await BaseRepository.InsertAsync(entity);
            await BaseRepository.FlushAsync();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();

                var exists1 = await repository.ExistsAsync<AnEntity>(x => x.Id, entity.Id);
                exists1.Should().BeTrue();
                callTraces[0].Should().Be("SpyDecorator1.LoadAsync(Type, MemberInfo, Object, LoadUniqueAsync)");
                callTraces[1].Should().Be("SpyDecorator2.LoadAsync(Type, MemberInfo, Object, LoadUniqueAsync)");

                var exists2 = await repository.ExistsAsync<AnEntity>(x => x.Id, Guid.NewGuid());
                exists2.Should().BeFalse();
                callTraces[2].Should().Be("SpyDecorator1.LoadAsync(Type, MemberInfo, Object, LoadUniqueAsync)");
                callTraces[3].Should().Be("SpyDecorator2.LoadAsync(Type, MemberInfo, Object, LoadUniqueAsync)");
            }
        }

        [Fact]
        public async Task ExistsManyWithDecorators()
        {
            _repositoryOptionBuilder
                .AddDecorator<SpyDecorator1>()
                .AddDecorator<SpyDecorator2>();
            var callTraces = new List<string>();
            _builder.RegisterInstance(new SpyDecorator1(callTraces));
            _builder.RegisterInstance(new SpyDecorator2(callTraces));

            var entity = new AnEntity().WithId();
            await BaseRepository.InsertAsync(entity);
            await BaseRepository.FlushAsync();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();

                var exists3 = await repository.ExistsAsync<AnEntity>(new[] { entity.Id });
                exists3.Should().BeTrue();
                callTraces[0].Should().Be("SpyDecorator1.LoadAsync(Type, IEnumerable<Guid>, LoadIdCollectionAsync)");
                callTraces[1].Should().Be("SpyDecorator2.LoadAsync(Type, IEnumerable<Guid>, LoadIdCollectionAsync)");

                var exists4 = await repository.ExistsAsync<AnEntity>(new[] { Guid.NewGuid() });
                exists4.Should().BeFalse();
                callTraces[2].Should().Be("SpyDecorator1.LoadAsync(Type, IEnumerable<Guid>, LoadIdCollectionAsync)");
                callTraces[3].Should().Be("SpyDecorator2.LoadAsync(Type, IEnumerable<Guid>, LoadIdCollectionAsync)");
            }
        }

        [Fact]
        public async Task QueryWithDecorators()
        {
            _repositoryOptionBuilder
                .AddDecorator<SpyDecorator1>()
                .AddDecorator<SpyDecorator2>();
            var callTraces = new List<string>();
            _builder.RegisterInstance(new SpyDecorator1(callTraces));
            _builder.RegisterInstance(new SpyDecorator2(callTraces));

            var entity = new AnEntity().WithId();
            await BaseRepository.InsertAsync(entity);
            await BaseRepository.FlushAsync();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();

                var query1 = repository.Query(typeof(AnEntity));
                query1.Cast<AnEntity>().ToArray().Should().ContainSingle().Which.Id.Should().Be(entity.Id);
                callTraces[0].Should().Be("SpyDecorator1.Query(Query<TEntity>)");
                callTraces[1].Should().Be("SpyDecorator2.Query(Query<TEntity>)");

                var query2 = repository.Query<AnEntity>();
                query2.ToArray().Should().ContainSingle().Which.Id.Should().Be(entity.Id);
                callTraces[2].Should().Be("SpyDecorator1.Query(Query<TEntity>)");
                callTraces[3].Should().Be("SpyDecorator2.Query(Query<TEntity>)");
            }
        }

        [Fact]
        public async Task InsertWithDecorators()
        {
            _repositoryOptionBuilder
                .AddDecorator<SpyDecorator1>()
                .AddDecorator<SpyDecorator2>();
            var callTraces = new List<string>();
            _builder.RegisterInstance(new SpyDecorator1(callTraces));
            _builder.RegisterInstance(new SpyDecorator2(callTraces));

            var id = Guid.NewGuid();
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var transaction = uow.Resolve<ITransactionHolder>();

                var entity = new AnEntity { Id = id };
                await repository.InsertAsync(entity);
                callTraces[0].Should().Be("SpyDecorator1.InsertAsync(IEntity, InsertAsync)");
                callTraces[1].Should().Be("SpyDecorator2.InsertAsync(IEntity, InsertAsync)");

                await transaction.CommitAsync();
            }

            var loadedEntity = await BaseRepository.LoadAsync(typeof(AnEntity), id);
            loadedEntity.Should().NotBeNull();
        }

        [Fact]
        public async Task UpdateWithDecorators()
        {
            _repositoryOptionBuilder
                .AddDecorator<SpyDecorator1>()
                .AddDecorator<SpyDecorator2>();
            var callTraces = new List<string>();
            _builder.RegisterInstance(new SpyDecorator1(callTraces));
            _builder.RegisterInstance(new SpyDecorator2(callTraces));

            var entity = new AnEntity().WithId();
            entity.Name = null;
            await BaseRepository.InsertAsync(entity);
            await BaseRepository.FlushAsync();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var transaction = uow.Resolve<ITransactionHolder>();

                entity = await repository.LoadAsync<AnEntity>(entity.Id);
                entity.Name = "toto";
                await repository.UpdateAsync(entity);
                callTraces[0].Should().Be("SpyDecorator1.LoadAsync(Type, Guid, LoadIdAsync)");
                callTraces[1].Should().Be("SpyDecorator2.LoadAsync(Type, Guid, LoadIdAsync)");
                callTraces[2].Should().Be("SpyDecorator1.UpdateAsync(IEntity, UpdateAsync)");
                callTraces[3].Should().Be("SpyDecorator2.UpdateAsync(IEntity, UpdateAsync)");

                await transaction.CommitAsync();
            }

            var loadedEntity = await BaseRepository.LoadAsync(typeof(AnEntity), entity.Id);
            loadedEntity.Should().ContainSingle()
                .Which.Should().BeOfType<AnEntity>()
                .Which.Name.Should().Be("toto");
        }

        [Fact]
        public async Task DeleteWithDecorators()
        {
            _repositoryOptionBuilder
                .AddDecorator<SpyDecorator1>()
                .AddDecorator<SpyDecorator2>();
            var callTraces = new List<string>();
            _builder.RegisterInstance(new SpyDecorator1(callTraces));
            _builder.RegisterInstance(new SpyDecorator2(callTraces));

            var entity = new AnEntity().WithId();
            await BaseRepository.InsertAsync(entity);
            await BaseRepository.FlushAsync();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var transaction = uow.Resolve<ITransactionHolder>();

                entity = await repository.LoadAsync<AnEntity>(entity.Id);
                await repository.DeleteAsync(entity);
                callTraces[0].Should().Be("SpyDecorator1.LoadAsync(Type, Guid, LoadIdAsync)");
                callTraces[1].Should().Be("SpyDecorator2.LoadAsync(Type, Guid, LoadIdAsync)");
                callTraces[2].Should().Be("SpyDecorator1.DeleteAsync(IEntity, DeleteAsync)");
                callTraces[3].Should().Be("SpyDecorator2.DeleteAsync(IEntity, DeleteAsync)");

                await transaction.CommitAsync();
            }

            var loadedEntity = await BaseRepository.LoadAsync(typeof(AnEntity), entity.Id);
            loadedEntity.Should().BeEmpty();
        }

        [Fact]
        public async Task FlushWithDecorators()
        {
            _repositoryOptionBuilder
                .AddDecorator<SpyDecorator1>()
                .AddDecorator<SpyDecorator2>();
            var callTraces = new List<string>();
            _builder.RegisterInstance(new SpyDecorator1(callTraces));
            _builder.RegisterInstance(new SpyDecorator2(callTraces));

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();

                await repository.FlushAsync();
                callTraces[0].Should().Be("SpyDecorator1.FlushAsync(FlushAsync)");
                callTraces[1].Should().Be("SpyDecorator2.FlushAsync(FlushAsync)");
            }
        }


        public class SpyDecorator1 : IRepositoryDecorator
        {
            public SpyDecorator1(List<string> callTraces)
            {
                CallTraces = callTraces;
            }

            public List<string> CallTraces { get; }

            public Task<IEnumerable<IEntity>> LoadAsync(Type type, Guid id, LoadIdAsync next)
            {
                TraceCall(MethodBase.GetCurrentMethod());
                return next(type, id);
            }

            public Task<IEnumerable<IEntity>> LoadAsync(Type type, IEnumerable<Guid> ids, LoadIdCollectionAsync next)
            {
                TraceCall(MethodBase.GetCurrentMethod());
                return next(type, ids);
            }

            public Task<IEnumerable<IEntity>> LoadAsync(Type type, MemberInfo uniqueMember, object value, LoadUniqueAsync next)
            {
                TraceCall(MethodBase.GetCurrentMethod());
                return next(type, uniqueMember, value);
            }

            public IQueryable<TEntity> Query<TEntity>(Query<TEntity> next) where TEntity : IEntity
            {
                TraceCall(MethodBase.GetCurrentMethod());
                return next();
            }

            public Task InsertAsync(IEntity entity, InsertAsync next)
            {
                TraceCall(MethodBase.GetCurrentMethod());
                return next(entity);
            }

            public Task UpdateAsync(IEntity entity, UpdateAsync next)
            {
                TraceCall(MethodBase.GetCurrentMethod());
                return next(entity);
            }

            public Task DeleteAsync(IEntity entity, DeleteAsync next)
            {
                TraceCall(MethodBase.GetCurrentMethod());
                return next(entity);
            }

            public Task FlushAsync(FlushAsync next)
            {
                TraceCall(MethodBase.GetCurrentMethod());
                return next();
            }

            private void TraceCall(MethodBase method)
            {
                var parameterTypes = method.GetParameters().Select(x => x.ParameterType.GetPrettyName());
                CallTraces.Add($"{this.GetType().Name}.{method.Name}({string.Join(", ", parameterTypes)})");
            }
        }

        public class SpyDecorator2 : SpyDecorator1
        {
            public SpyDecorator2(List<string> callTraces) : base(callTraces)
            {
            }
        }

        public class AppRepository : DecorableRepository, IRepository
        {
            public AppRepository(IBaseRepository baseRepository, IUnitOfWork unitOfWork, RepositoryOptions options) 
                : base(baseRepository, unitOfWork, options)
            {
            }
        }

        public class AnEntity : IEntity
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
        }
    }
}
