using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Autofac;
using CocoriCore.Autofac;
using CocoriCore.Common;
using CocoriCore.TestUtils;
using FluentAssertions;
using Xunit;
using CocoriCore.Linq.Async;

namespace CocoriCore.Repository.Test
{

    public class MemoryRepositoryTest
    {
        private ILifetimeScope _scope;

        public MemoryRepositoryTest()
        {
            var containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterType<InMemoryEntityStore>().As<IInMemoryEntityStore>().SingleInstance();
            containerBuilder.RegisterType<InMemoryBaseRepository>().AsImplementedInterfaces().SingleInstance();
            containerBuilder.RegisterType<AppRepository>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            containerBuilder.Register(c => new UnitOfWorkOptionsBuilder().Options).As<UnitOfWorkOptions>().SingleInstance();
            containerBuilder.RegisterType<AutofacUnitOfWork>().As<IUnitOfWork>().SingleInstance();
            containerBuilder.RegisterType<AutofacUnitOfWorkFactory>().As<IUnitOfWorkFactory>().SingleInstance();

            _scope = containerBuilder.Build();
        }

        [Fact]
        public async Task InsertEntity()
        {
            var entity = new AnEntity().WithId();

            var repository = _scope.Resolve<IRepository>();
            await repository.InsertAsync(entity);

            var anEntity = await repository.LoadAsync<AnEntity>(entity.Id);
            var anAbstractEntity = await repository.LoadAsync<AnAbstractEntity>(entity.Id);
            var iEntity = await repository.LoadAsync<IEntity>(entity.Id);

            anEntity.Should().BeEquivalentTo(entity);
            anAbstractEntity.Should().BeEquivalentTo(entity);
            iEntity.Should().BeEquivalentTo(entity);
            (await repository.Query<IEntity>().ToListAsync()).Should().HaveCount(1);
        }

        [Fact]
        public async Task InsertTwoEntitiesDifferentTypeButSameId()
        {
            var id = Guid.NewGuid();
            var entity = new AnEntity { Id = id };
            var projection = new AProjection { Id = id };

            var repository = _scope.Resolve<IRepository>();
            await repository.InsertAsync(entity);
            await repository.InsertAsync(projection);

            (await repository.LoadAsync<AnEntity>(id)).Should().BeEquivalentTo(entity);
            (await repository.LoadAsync<AnAbstractEntity>(id)).Should().BeEquivalentTo(entity);
            (await repository.LoadAsync<AProjection>(id)).Should().BeEquivalentTo(projection);
            (await repository.Query<IEntity>().ToListAsync()).Should().HaveCount(2);
            (await repository.ExistsAsync<IEntity>(id)).Should().BeTrue();
        }

        [Fact]
        public async Task UpdateEntity()
        {
            AnEntity anEntity = new AnEntity().WithId();
            IEntity iEntity = anEntity;
            AnAbstractEntity abstractEntity = anEntity;

            var repository = _scope.Resolve<IRepository>();
            await repository.InsertAsync(anEntity);

            anEntity.Name = "toto";
            await repository.UpdateAsync(anEntity);
            await repository.UpdateAsync(abstractEntity);
            await repository.UpdateAsync(iEntity);

            (await repository.LoadAsync<AnEntity>(anEntity.Id)).Should().BeEquivalentTo(anEntity);
            (await repository.LoadAsync<AnAbstractEntity>(anEntity.Id)).Should().BeEquivalentTo(anEntity);
            (await repository.LoadAsync<IEntity>(anEntity.Id)).Should().BeEquivalentTo(anEntity);
            (await repository.Query<AnEntity>().ToListAsync()).Should().ContainSingle().Which.Name.Should().Be("toto");
            (await repository.Query<AnAbstractEntity>().ToListAsync()).Should().ContainSingle();
            (await repository.Query<IEntity>().ToListAsync()).Should().ContainSingle();
        }

        [Fact]
        public async Task RemoveAnEntity()
        {
            AnEntity anEntity = new AnEntity().WithId();
            AProjection projection = new AProjection { Id = anEntity.Id };

            var repository = _scope.Resolve<IRepository>();
            await repository.InsertAsync(anEntity);
            await repository.InsertAsync(projection);

            await repository.DeleteAsync<AnEntity>(anEntity);

            (await repository.ExistsAsync<AnEntity>(anEntity.Id)).Should().BeFalse();
            (await repository.ExistsAsync<IEntity>(anEntity.Id)).Should().BeTrue();
            (await repository.LoadAsync<IEntity>(anEntity.Id)).Should().BeEquivalentTo(projection);
        }

        [Fact]
        public async Task RemoveAnAbstractEntity()
        {
            AnEntity anEntity = new AnEntity().WithId();
            AProjection projection = new AProjection { Id = anEntity.Id };

            var repository = _scope.Resolve<IRepository>();
            await repository.InsertAsync(anEntity);
            await repository.InsertAsync(projection);

            await repository.DeleteAsync<AnAbstractEntity>(anEntity);

            (await repository.ExistsAsync<AnEntity>(anEntity.Id)).Should().BeFalse();
            (await repository.ExistsAsync<IEntity>(anEntity.Id)).Should().BeTrue();
            (await repository.LoadAsync<IEntity>(anEntity.Id)).Should().BeEquivalentTo(projection);
        }

        [Fact]
        public async Task RemoveIEntity()
        {
            AnEntity anEntity = new AnEntity().WithId();

            var repository = _scope.Resolve<IRepository>();
            await repository.InsertAsync(anEntity);

            await repository.DeleteAsync<IEntity>(anEntity);

            (await repository.ExistsAsync<AnEntity>(anEntity.Id)).Should().BeFalse();
            (await repository.ExistsAsync<IEntity>(anEntity.Id)).Should().BeFalse();
            (await repository.Query<IEntity>().ToListAsync()).Should().BeEmpty();
        }

        [Fact]
        public async Task QueryEntitiesAsync()
        {
            var entity1 = new AnEntity().WithId();
            var entity2 = new AnotherEntity().WithId();

            var repository = _scope.Resolve<IRepository>();
            await repository.InsertAsync(entity1);
            await repository.InsertAsync(entity2);

            (await repository.Query<AnEntity>().ToListAsync()).Should().ContainSingle().Which.Should().BeEquivalentTo(entity1);
            (await repository.Query<AnotherEntity>().ToListAsync()).Should().ContainSingle().Which.Should().BeEquivalentTo(entity2);
            (await repository.Query<IEntity>().ToListAsync()).Should().HaveCount(2);
        }

        [Fact]
        public async Task QueryEntities()
        {
            var entity1 = new AnEntity().WithId();
            var entity2 = new AnotherEntity().WithId();

            var repository = _scope.Resolve<IRepository>();
            await repository.InsertAsync(entity1);
            await repository.InsertAsync(entity2);

            repository.Query<AnEntity>().Should().ContainSingle().Which.Should().BeEquivalentTo(entity1);
            repository.Query<AnotherEntity>().Should().ContainSingle().Which.Should().BeEquivalentTo(entity2);
            repository.Query<IEntity>().Should().HaveCount(2);
        }

        [Fact]
        public async Task QueryWhenNoEntity()
        {
            var repository = _scope.Resolve<IRepository>();

            (await repository.Query<IEntity>().ToListAsync()).Should().BeEmpty();
        }

        [Fact]
        public async Task ExistsEntity()
        {
            var entity = new AnEntity().WithId();

            var repository = _scope.Resolve<IRepository>();
            await repository.InsertAsync(entity);

            (await repository.ExistsAsync<AnEntity>(entity.Id)).Should().BeTrue();
            (await repository.ExistsAsync<AnAbstractEntity>(entity.Id)).Should().BeTrue();
            (await repository.ExistsAsync<IEntity>(entity.Id)).Should().BeTrue();
        }


        [Fact]
        public async Task ExceptionIfInsertTwoEntitiesSameTypeAsync()
        {
            var id = Guid.NewGuid();
            var entity1 = new AnEntity { Id = id };
            var entity2 = new AnEntity { Id = id };

            var repository = _scope.Resolve<IRepository>();
            Func<Task> action = async () =>
            {
                await repository.InsertAsync(entity1);
                await repository.InsertAsync(entity2);
            };

            await action.Should().ThrowAsync<InvalidOperationException>()
                .WithMessage($"There is already an entity of type {typeof(AnEntity)} with id {id}");
        }

        [Fact]
        public async Task ExceptionIfInsertTwoEntitiesSameTypeButDifferentDeclaredTypeAsync()
        {
            var id = Guid.NewGuid();
            AnEntity entity1 = new AnEntity { Id = id };
            IEntity entity2 = new AnEntity { Id = id };

            var repository = _scope.Resolve<IRepository>();
            Func<Task> action = async () =>
            {
                await repository.InsertAsync(entity1);
                await repository.InsertAsync(entity2);
            };

            await action.Should().ThrowAsync<InvalidOperationException>()
                .WithMessage($"There is already an entity of type {typeof(AnEntity)} with id {id}");
        }

        [Fact]
        public async Task ExceptionIfUpdateNonExistingEntityAsync()
        {
            var entity = new AnEntity().WithId();

            var repository = _scope.Resolve<IRepository>();
            Func<Task> action = () => repository.UpdateAsync(entity);

            await action.Should().ThrowAsync<InvalidOperationException>()
                .WithMessage($"There is no entity of type {typeof(AnEntity)} with id {entity.Id}");
        }

        [Fact]
        public async Task ExceptionIfDeleteNonExistingEntityAsync()
        {
            var entity = new AnEntity().WithId();

            var repository = _scope.Resolve<IRepository>();
            Func<Task> action = () => repository.DeleteAsync(entity);

            await action.Should().ThrowAsync<InvalidOperationException>()
                .WithMessage($"There is no entity of type {typeof(AnEntity)} with id {entity.Id}");
        }

        [Fact]
        public async Task ExceptionIfDeleteNonExistingTypeAndIdAsync()
        {
            var entity = new AnEntity().WithId();

            var repository = _scope.Resolve<IRepository>();
            Func<Task> action = () => repository.DeleteAsync(entity);

            await action.Should().ThrowAsync<InvalidOperationException>()
                .WithMessage($"There is no entity of type {typeof(AnEntity)} with id {entity.Id}");
        }

        [Fact]
        public async Task ExceptionIfLoadTwoEntitiesSameId()
        {
            var id = Guid.NewGuid();
            var entity = new AnEntity { Id = id };
            var projection = new AProjection { Id = id };

            var repository = _scope.Resolve<IRepository>();
            await repository.InsertAsync(entity);
            await repository.InsertAsync(projection);

            Func<Task> action = () => repository.LoadAsync<IEntity>(id);

            await action.Should().ThrowAsync<InvalidOperationException>()
                .WithMessage($"There are several entities of type {typeof(IEntity)} with id '{id}', use Query() method instead.");
        }

        [Fact]
        public async Task ExceptionIfLoadTwoEntitiesSameUniqueValue()
        {
            var id = Guid.NewGuid();
            var entity = new AnEntity { Id = id };
            var projection = new AProjection { Id = id };

            var repository = _scope.Resolve<IRepository>();
            await repository.InsertAsync(entity);
            await repository.InsertAsync(projection);

            Func<Task> action = () => repository.LoadAsync<IEntity>(x => x.Id, id);

            await action.Should().ThrowAsync<InvalidOperationException>().WithMessage($"There are several entities of type {typeof(IEntity)} " +
                $"with value '{id}' for unique field Id, use Query() method instead.");
        }

        [Fact]
        public async Task ThrowExceptionIfNoEntityMatchingId()
        {
            var entity = new AnEntity().WithId();

            var repository = _scope.Resolve<IRepository>();
            await repository.InsertAsync(entity);

            var invalidId = Guid.NewGuid();
            Func<Task> action = () => repository.LoadAsync<AnEntity>(invalidId);

            await action.Should().ThrowAsync<InvalidOperationException>()
                .WithMessage($"There is no entity of type {typeof(AnEntity)} with id '{invalidId}'.");
        }

        [Fact]
        public async Task ThrowExceptionIfNoEntityMatchingUniqueValue()
        {
            var entity = new AnEntity().WithId();

            var repository = _scope.Resolve<IRepository>();
            await repository.InsertAsync(entity);

            var invalidId = Guid.NewGuid();
            Func<Task> action = () => repository.LoadAsync<AnEntity>(x => x.Id, invalidId);

            await action.Should().ThrowAsync<InvalidOperationException>()
                .WithMessage($"There is no entity of type {typeof(AnEntity)} with value '{invalidId}' for unique field Id.");
        }

        public class AppRepository : DecorableRepository, IRepository
        {
            public AppRepository(IBaseRepository baseRepository, IUnitOfWork unitOfWork, IEnumerable<Type> decoratorTypes)
                : base(baseRepository, unitOfWork, decoratorTypes)
            {
            }
        }

        public class AnEntity : AnAbstractEntity
        {
        }

        public abstract class AnAbstractEntity : IEntity
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
        }

        public class AnotherEntity : IEntity
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
        }

        public class AProjection : IEntity
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
        }
    }
}