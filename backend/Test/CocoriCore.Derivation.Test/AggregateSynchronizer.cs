﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocoriCore.Derivation.Test
{
    public class AggregateSynchronizer : SynchronizerBase<AssocEntityProjection, EntityAProjection>,
        IAutoRegister
    {
        public AggregateSynchronizer(DependencyCoupler coupler)
            : base(coupler)
        {
        }

        public override Task SynchonizeAsync(DependencyInfo info, IEnumerable<AssocEntityProjection> dependencies, IEnumerable<EntityAProjection> projections)
        {
            var groups = Coupler.GroupDependenciesByProjection(info, dependencies, projections);
            foreach (var group in groups)
            {
                group.projection.BNames = group.dependencies.Select(x => x.BName).ToArray();
            }
            return Task.CompletedTask;
        }
    }
}