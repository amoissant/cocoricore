﻿using System;
using Xunit;
using FluentAssertions;
using System.Threading.Tasks;
using Autofac;
using System.Collections.Generic;
using CocoriCore.Test;
using Microsoft.Extensions.Caching.Memory;
using CocoriCore.Autofac;
using CocoriCore.TestUtils;
using CocoriCore.Common;
using CocoriCore.Repository;
using CocoriCore.JsonNet;

namespace CocoriCore.Derivation.Test
{
    public class DerivationBusErrorTest : BusTestBase
    {
        private DerivationBusOptionsBuilder _derivationBuilder;
        private RepositoryFailureOptions _fakeRepositoryConfiguration;

        public DerivationBusErrorTest()
        {
            _derivationBuilder = new DerivationBusOptionsBuilder();
            _derivationBuilder.ConfigureMappings(this.GetType().Assembly);
            _derivationBuilder.EntityAssemblies = new[] { CocoriCore.Test.AssemblyInfo.Assembly, CocoriCore.Derivation.Test.AssemblyInfo.Assembly };
            _unitOfWorkBuilder
                .Call<IRepository>(r => r.FlushAsync())
                .Call<IDerivationBus>(b => b.ApplyRulesAsync())
                .Call<ITransactionHolder>(b => b.CommitAsync())
                .Call<IDerivationBus>(b => b.ApplyDifferedRulesAsync());
            _fakeRepositoryConfiguration = new RepositoryFailureOptions();

            ContainerBuilder.RegisterInstance(new MemoryCache(new MemoryCacheOptions())).As<IMemoryCache>();
            ContainerBuilder.RegisterInstance(_fakeRepositoryConfiguration).AsSelf();
            ContainerBuilder.RegisterType<Factory>().As<IFactory>().SingleInstance();
            ContainerBuilder.RegisterType<InMemoryEntityStore>().As<IInMemoryEntityStore>().SingleInstance();
            ContainerBuilder.RegisterType<TransactionalInMemoryBaseRepository>()
                .AsSelf()
                .As<IBaseRepository>()
                .As<ITransactionHolder>()
                .InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<MyRepository>()
               .As<IDerivationRepository, IRepository>()
               .WithParameter(new TypedParameter<IEnumerable<Type>>(new[] { typeof(EntityEventDecorator), typeof(StateStoreDecorator), typeof(FailureDecorator) }))
               .InstancePerLifetimeScope();
            ContainerBuilder.Register(c => _derivationBuilder.Build()).As<DerivationBusOptions>().SingleInstance();
            ContainerBuilder.RegisterType<DerivationBus>().AsSelf().As<IDerivationBus>().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<StateStore>().As<IStateStore>().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<EntityEventStore>().AsImplementedInterfaces().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<Utf8JsonCopier>().As<ICopier>().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<StateStoreDecorator>().AsSelf().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<EntityEventDecorator>().AsSelf().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<FailureDecorator>().AsSelf().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<SpyFnFExceptionHandler>().AsSelf().InstancePerLifetimeScope();

            ContainerBuilder.RegisterType<DependencyManager>().AsSelf().SingleInstance();
            ContainerBuilder.RegisterType<ObjectSerializer>().AsImplementedInterfaces().SingleInstance();

            ContainerBuilder.RegisterAssemblyTypes(CocoriCore.Derivation.AssemblyInfo.Assembly, this.GetType().Assembly)
                .AssignableTo<IAutoRegister>()
                .AsImplementedInterfaces()
                .AsSelf();
            ContainerBuilder.RegisterAssemblyTypes(this.GetType().Assembly)
                .AssignableTo<IEntity>()
                .AsSelf();

            ContainerBuilder.Register(c => new DefaultErrorBusOptionsBuilder().Options).SingleInstance();
            ContainerBuilder.RegisterType<ErrorBus>().AsImplementedInterfaces().InstancePerLifetimeScope();

            ContainerBuilder.ConfigureXunitLogger();
        }

        [Fact]
        public async Task ThrowExceptionIfEntityNotLoadedBeforeUpdate()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();

            AnEntity entity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            Func<Task> action = async () =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    await repository.UpdateAsync(entity);
                    await uow.FinishAsync();
                }
            };

            await action.Should().ThrowAsync<ArgumentException>()
                .WithMessage("Unknown entity reference, ensure to pass the same instance returned by Query or Load methods.");
        }

        [Fact]
        public async Task NoExceptionIfNoRuleDefinedForEntityAsync()
        {
            Func<Task> action = async () =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    var entity = new AnEntity().WithId();
                    await repository.InsertAsync(entity);
                    await uow.FinishAsync();
                }
            };

            await action.Should().NotThrowAsync<Exception>();
        }

        [Fact]
        public async Task ThrowErrorWhenInsertEntityAsync()
        {
            _fakeRepositoryConfiguration.ErrorWhen((o, e) => o == CUDOperation.CREATE && e is AProjection);
            var ruleBuilder = _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();

            AnEntity entity = null;
            Func<Task> action = async () =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    entity = new AnEntity().WithId();
                    await repository.InsertAsync(entity);
                    await uow.FinishAsync();
                }
            };

            var assertion = await action.Should().ThrowAsync<DerivationException>();
            var exception = assertion.Which;
            exception.Message.Should().Contain("Error while executing the following derivation rule")
                .And.Contain(nameof(AnEntity))
                .And.Contain(nameof(AProjection));
            var datas = (Dictionary<string, object>)exception.Data["Derivation"];
            datas.Should().HaveCount(2);
            datas["DependencyIds"].Should().Be($"[{entity.Id}]");
            datas["Rule"].Should().Be(ruleBuilder.OriginRule.StringRepresentation);
            exception.InnerException.Message.Should().Be("Fake insert error.");
        }

        [Fact]
        public async Task ThrowErrorWhenUpdateEntity()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();

            AnEntity entity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            _fakeRepositoryConfiguration.ErrorWhen((o, e) => o == CUDOperation.UPDATE && e is AProjection);
            Func<Task> action = async () =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    entity = await repository.LoadAsync<AnEntity>(entity.Id);
                    entity.Name = "toto";
                    await repository.UpdateAsync(entity);
                    await uow.FinishAsync();
                }
            };

            var assertion = await action.Should().ThrowAsync<DerivationException>();
            var exception = assertion.Which;
            exception.Message.Should().Contain("Error while executing the following derivation rule")
                .And.Contain(nameof(AnEntity))
                .And.Contain(nameof(AProjection));
            exception.InnerException.Message.Should().Be("Fake update error.");
        }

        [Fact]
        public async Task ThrowErrorWhenDeleteEntity()
        {
            _fakeRepositoryConfiguration.ErrorWhen((o, e) => o == CUDOperation.DELETE && e is AProjection);
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();

            AnEntity entity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            Func<Task> action = async () =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    entity = await repository.LoadAsync<AnEntity>(entity.Id);
                    await repository.DeleteAsync(entity);
                    await uow.FinishAsync();
                }
            };

            var assertion = await action.Should().ThrowAsync<DerivationException>();
            var exception = assertion.Which;
            exception.Message.Should().Contain("Error while executing the following derivation rule")
                .And.Contain(nameof(AnEntity))
                .And.Contain(nameof(AProjection));
            exception.InnerException.Message.Should().Be("Fake delete error.");
        }

        [Fact]
        public async Task ThrowUpdateErrorWhenUpdateDependencyEntity()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();
            _derivationBuilder.ForDependency<DependencyEntityA>().Update<AProjection>((e, p) => e.Id == p.DependencyAId).UsingMapping();

            AnEntity entity = null;
            DependencyEntityA dependency = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependency = new DependencyEntityA().WithId();
                dependency.Version = 1;
                entity = new AnEntity().WithId();
                entity.DependencyAId = dependency.Id;
                await repository.InsertManyAsync(entity, dependency);
                await uow.FinishAsync();
            }

            _fakeRepositoryConfiguration.ErrorWhen((o, e) => o == CUDOperation.UPDATE && e is AProjection);

            Func<Task> action = async () =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    dependency = await repository.LoadAsync<DependencyEntityA>(dependency.Id);
                    dependency.Version = 2;
                    await repository.UpdateAsync(dependency);
                    await uow.FinishAsync();
                }
            };

            var assertion = await action.Should().ThrowAsync<DerivationException>();
            var exception = assertion.Which;
            exception.Message.Should().Contain("Error while executing the following derivation rule")
                .And.Contain(nameof(DependencyEntityA))
                .And.Contain(nameof(AProjection));
            exception.InnerException.Message.Should().Be("Fake update error.");
        }

        [Fact]
        public async Task ThrowSynchronizerErrorWhenUpdateDependencyEntityAsync()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();
            _derivationBuilder.ForDependency<DependencyEntityA>().Update<AProjection>((e, p) => e.Id == p.DependencyAId).UsingMapping("error");

            AnEntity entity = null;
            DependencyEntityA dependency = null;
            Func<Task> action = async () =>
             {
                 using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                 {
                     var repository = uow.Resolve<IRepository>();
                     dependency = new DependencyEntityA().WithId();
                     entity = new AnEntity().WithId();
                     entity.DependencyAId = dependency.Id;
                     await repository.InsertManyAsync(entity, dependency);
                     await uow.FinishAsync();
                 }
             };

            var assertion = await action.Should().ThrowAsync<DerivationException>();
            var exception = assertion.Which;
            exception.Message.Should().Contain("Error while executing the following derivation rule")
                .And.Contain(nameof(AnEntity))
                .And.Contain(nameof(AProjection));
            exception.InnerException.Message.Should().Be("Derivation mapping with discriminator 'error' from "
                + $"{typeof(DependencyEntityA)} to projection {typeof(AProjection)} throw an exception, see inner for details.");
            exception.InnerException.InnerException.Message.Should().Be("Fake synchronizer error.");
            _fakeRepositoryConfiguration.TotalNbUpdateCall.Should().Be(0);
        }

        [Fact]
        public async Task ThrowQueryErrorWhenUpdateDependencyEntity()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();
            _derivationBuilder.ForDependency<DependencyEntityA>().Update<AProjection>((e, p) => e.Id == p.DependencyAId).UsingMapping();

            AnEntity entity = null;
            DependencyEntityA dependency = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependency = new DependencyEntityA().WithId();
                dependency.Version = 1;
                entity = new AnEntity().WithId();
                entity.DependencyAId = dependency.Id;
                await repository.InsertManyAsync(entity, dependency);
                await uow.FinishAsync();
            }

            _fakeRepositoryConfiguration.ErrorWhenQuery(t => typeof(AProjection).IsAssignableFrom(t));

            Func<Task> action = async () =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    dependency = await repository.LoadAsync<DependencyEntityA>(dependency.Id);
                    dependency.Name = "toto";
                    await repository.UpdateAsync(dependency);
                    await uow.FinishAsync();
                }
            };

            var assertion = await action.Should().ThrowAsync<DerivationException>();
            var exception = assertion.Which;
            exception.Message.Should().Contain("Error while executing the following derivation rule")
                .And.Contain(nameof(DependencyEntityA))
                .And.Contain(nameof(AProjection));

            exception.InnerException.Message.Should().Be("Fake query error.");
            var datas = exception.InnerException.Data;
            var linqQuery = (string)datas["Linq"];
            linqQuery.Should().Contain(typeof(AProjection).ToString())
                .And.Contain("Contains")
                .And.Contain(nameof(AProjection.DependencyAId));
        }

        [Fact]
        public void ConfigurationExceptionIfRuleUsingMappingButNoMappingDefined()
        {
            _derivationBuilder.MappingActions = new MappingAction[0];
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();

            DerivationBusOptions options = null;
            Action action = () => options = _derivationBuilder.Build();

            action.Should().Throw<ConfigurationException>()
                .Which.Message.Should().Be($"No mapping defined from entity {typeof(AnEntity)} to projection {typeof(AProjection)} with discriminator ''.\n"
                + $"Ensure you have configured mappings using {nameof(DerivationBusMappingExtension.ConfigureMappings)}() "
                + $"and defined a mapping action for this entity, projection and discriminator within an implementation of {typeof(IMappingConfiguration)}.");
        }

        [Fact]
        public void ConfigurationExceptionIfIncompleteRule()
        {
            _derivationBuilder.ForDependency<AnEntity>().Update<AProjection>((e, p) => e.Id == p.DependencyAId);

            DerivationBusOptions options = null;
            Action action = () => options = _derivationBuilder.Build();

            action.Should().Throw<ConfigurationException>()
                .Which.Message.Should().Be($"Rule for entity {typeof(AnEntity)} is not complete.\n"
                + "Ensure you have define a projection type and a synchronizer type.");
        }


        [Fact]
        public void ConfigurationExceptionIfTwoRulesForSameDependencyProjectionAndDiscriminator()
        {
            var action1 = new MappingAction(typeof(AnEntity));
            action1.ProjectionType = typeof(AProjection);
            action1.Discriminator = "toto";
            var action2 = new MappingAction(typeof(AnEntity));
            action2.ProjectionType = typeof(AProjection);
            action2.Discriminator = "toto";
            _derivationBuilder.MappingActions = new[] { action1, action2 };

            DerivationBusOptions options = null;
            Action action = () => options = _derivationBuilder.Build();

            action.Should().Throw<ConfigurationException>()
                .Which.Message.Should().Be($"Duplicate mapping defined for entity {typeof(AnEntity)}, projection {typeof(AProjection)} and discriminator 'toto'.");
        }


        [Fact]
        public void ConfigurationExceptionIfDependencyWithDiscriminorButNoMappingDefinedWithSameDiscriminator()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();
            _derivationBuilder.ForDependency<DependencyEntityA>().Update<AProjection>((e, p) => e.Id == p.DependencyAId).UsingMapping("toto");

            DerivationBusOptions options = null;
            Action action = () => options = _derivationBuilder.Build();

            action.Should().Throw<ConfigurationException>()
                .Which.Message.Should().Be($"No mapping defined from entity {typeof(DependencyEntityA)} to projection {typeof(AProjection)} with discriminator 'toto'.\n"
                + $"Ensure you have configured mappings using {nameof(DerivationBusMappingExtension.ConfigureMappings)}() "
                + $"and defined a mapping action for this entity, projection and discriminator within an implementation of {typeof(IMappingConfiguration)}.");
        }

        public class MyRepository : DecorableRepository, IDerivationRepository, IRepository
        {
            public MyRepository(IBaseRepository baseRepository, IUnitOfWork unitOfWork, IEnumerable<Type> decoratorTypes) 
                : base(baseRepository, unitOfWork, decoratorTypes)
            {
            }
        }
    }
}

