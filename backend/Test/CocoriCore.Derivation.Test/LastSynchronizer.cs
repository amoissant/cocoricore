﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocoriCore.Derivation.Test
{
    public class LastSynchronizer : SynchronizerBase<ALastEntity, ALastProjection>,
        IAutoRegister
    {
        public LastSynchronizer(DependencyCoupler coupler)
            : base(coupler)
        {
        }

        public override Task ResetAsync(DependencyInfo info, IEnumerable<ALastEntity> dependencies, IEnumerable<ALastProjection> projections)
        {
            foreach (var projection in projections)
            {
                projection.IsLast = false;
            }
            return Task.CompletedTask;
        }

        public override Task SynchonizeAsync(DependencyInfo info, IEnumerable<ALastEntity> dependencies, IEnumerable<ALastProjection> projections)
        {
            var groups = projections.GroupBy(x => x.ParentId).ToArray();
            foreach (var group in groups)
            {
                foreach(var projection in group)
                {
                    projection.IsLast = projection.CreatedAt == group.Max(x => x.CreatedAt);
                }
            }
            return Task.CompletedTask;
        }
    }
}