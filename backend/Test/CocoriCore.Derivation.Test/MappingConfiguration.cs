using System;
using CocoriCore.Test;

namespace CocoriCore.Derivation.Test
{
    public class MappingConfiguration : MappingConfigurationBase
    {
        public MappingConfiguration()
        {
            Map<AnEntity>().To<AProjection>().Using((e, p) =>
            {
                AutoMap(e, p);
                p.EntityName = e.Name;
                p.Duration = e.End - e.Start;
            });
            Map<AnotherEntity>().To<AProjection>().Using((e, p) =>
            {
                AutoMap(e, p);
                p.EntityName = e.Name;
            });
            Map<DependencyEntityA>().To<AProjection>().Using((e, p) =>
            {
                p.DependencyAName = e.Name;
                p.Town = e.Town;
                p.DependencyAVersion = e.Version;
                p.DependencyBId = e.DependencyBId;
                p.DependencyCId = e.DependencyCId;
            });
            Map<INamedEntity>().To<AProjection>().Using((e, p) =>
            {
                p.DependencyAName = e.Name;
            });
            Map<DependencyEntityA>("error").To<AProjection>().Using((e, p) =>
            {
                throw new Exception("Fake synchronizer error.");
            });
            Map<DependencyEntityB>().To<AProjection>().Using((e, p) =>
            {
                p.DependencyBName = e.Name;
            });
            Map<DependencyEntityC>().To<AProjection>().Using((e, p) =>
            {
                p.DependencyCName = e.Name;
            });
            Map<DependencyEntityD>().To<AProjection>().Using((e, p) =>
            {
                p.DependencyDName = e.Name;
            });
            Map<AnEntity>().To<AGroupProjection>().Using((e, p) =>
            {
                AutoMap(e, p);
            });
            Map<AOneEntity>("one").To<AOneManyProjection>().Using((e, p) =>
            {
                p.OneId = e.Id;
                p.ParentId = e.ParentId;
                p.OneName = e.Name;
            });
            Map<AManyEntity>().To<AOneManyProjection>().Using((e, p) =>
            {
                p.ManyId = e.Id;
                p.ManyName = e.Name;
                p.AnotherEntityId = e.AnotherEntityId;
            });
            Map<AnotherEntity>().To<AOneManyProjection>().Using((e, p) =>
            {
                p.AnotherName = e.Name;
            });
            Map<AOneEntity>().To<AOneManyProjection>().Using((e, p) =>
            {
                p.OneId = e.Id;
                p.ParentId = e.ParentId;
                p.OneName = e.Name;
            });
            Map<AManyEntity>("many").To<AOneManyProjection>().Using((e, p) =>
            {
                p.ManyId = e.Id;
                p.ManyName = e.Name?.ToUpper();
            });
            Map<AOneEntity>("many").To<AOneManyProjection>().Using((e, p) =>
            {
                p.OneId = e.Id;
                p.ParentId = e.ParentId;
                p.OneName = e.Name?.ToUpper();
            });
            Map<AOneEntity>().To<AOneProjection>().Using((e, p) =>
            {
                AutoMap(e, p);
            });
            Map<AManyEntity>().To<AManyProjection>().Using((e, p) =>
            {
                AutoMap(e, p);
            });
            Map<AOneEntity>("erase").To<AOneManyProjection>().Using((e, p) =>
            {
                p.Id = e.Id;
                p.OneId = e.Id;
                p.OneName = e.Name;
            });
            Map<AManyEntity>("erase").To<AOneManyProjection>().Using((e, p) =>
            {
                p.Id = e.Id;
                p.ManyId = Guid.Empty;
                p.ManyName = e.Name;
            });
            Map<AnAssociationEntity>().To<AManyProjection>().Using((e, p) =>
            {
                p.OneId = e.OneId;
            });
            Map<AOneEntity>().To<AManyProjection>().Using((e, p) =>
            {
                p.OneName = e.Name;
            });
            Map<AParentChildEntity>().To<AParentChildProjection>().Using((e, p) =>
            {
                AutoMap(e, p);
            });
            Map<AParentChildEntity>("parent").To<AParentChildProjection>().Using((e, p) =>
            {
                p.ParentDepth = e.Depth;
                p.ParentName = e.Name;
            })
            .ResetUsing(p =>
            {
                p.ParentDepth = null;
                p.ParentName = null;
            });
            Map<AParentChildEntity>().To<AnotherParentChildProjection>().Using((e, p) =>
            {
                AutoMap(e, p);
                if (e.Depth == 0)
                {
                    p.RootId = e.Id;
                }
            });
            Map<AParentChildEntity>("parent").To<AnotherParentChildProjection>().Using((e, p) =>
            {
                p.ParentName = e.Name;
                p.ParentDepth = e.Depth;
                if (e.Depth == 0)
                {
                    p.RootId = e.Id;
                }
            })
            .ResetUsing(p =>
            {
                p.ParentDepth = null;
                p.ParentName = null;
            });
            Map<AParentChildEntity>("root").To<AnotherParentChildProjection>().Using((e, p) =>
            {
                p.RootName = e.Name;
            });

            Map<AnEntity>().To<AProjectionWithCustomConstruction>().Using((e, p) =>
            {
                p.EntityName = e.Name;
                p.DepAId = e.DependencyAId;
            });
            Map<DependencyEntityA>().To<AProjectionWithCustomConstruction>().Using((e, p) =>
            {
                p.DepAName = e.Name;
            });

            Map<AnEntityWithPolymorphicDependency>().To<AProjectionWithPolymorphicDependency>().Using((e, p) =>
            {
                p.Name = e.Name;
                p.DependencyId = e.DependencyId;
            });
            Map<DependencyEntityA>().To<AProjectionWithPolymorphicDependency>().Using((e, p) =>
            {
                p.DependencyName = e.Name;
                p.Town = e.Town;
            });
            Map<DependencyEntityB>().To<AProjectionWithPolymorphicDependency>().Using((e, p) =>
            {
                p.DependencyName = e.Name;
            });

            Map<AnEntityWithParents>().To<AProjectionWithParents>().Using((e, p) =>
            {
                AutoMap(e, p);
            });
            Map<AParent>().To<AProjectionWithParents>().Using((e, p) =>
            {
                p.GreatestParentName = e.Name;
            });

            Map<EntityType>().To<ATypedProjection>().Using((e, p) =>
            {
                p.TypeId = e.Id;
            });
            Map<EntitySubType>().To<ATypedProjection>().Using((e, p) =>
            {
                p.SubTypeId = e.Id;
            });
            Map<EntityType>("type").To<ATypedProjection>().Using((e, p) =>
            {
                p.TypeName = e.Name;
            });
            Map<EntitySubType>("subType").To<ATypedProjection>().Using((e, p) =>
            {
                p.SubTypeName = e.Name;
                p.TypeId = e.TypeId;
            });
            Map<AProjection>().To<AnotherProjectionWithDependency>().Using((e, p) =>
            {
                p.DependencyId = e.DependencyAId;
                p.DependencyName = e.DependencyAName;
            });
            Map<ADependencyEntity>().To<AProjection>().Using((e, p) => 
            {
                p.DependencyAName = e.Name;
            });
            Map<EntityA>().To<EntityAProjection>().Using((e, p) =>
            {
                AutoMap(e, p);
            });
            Map<AssocEntity>().To<AssocEntityProjection>().Using((e, p) =>
            {
                AutoMap(e, p);
            });
            Map<EntityA>().To<AssocEntityProjection>().Using((e, p) =>
            {
                p.AName = e.Name;
            });
            Map<EntityB>().To<AssocEntityProjection>().Using((e, p) =>
            {
                p.BName = e.Name;
            });
            Map<EntityC>().To<EntityCProjection>().Using((e, p) =>
            {
                AutoMap(e, p);
            });
            Map<AssocEntityProjection>().To<EntityCProjection>().Using((e, p) =>
            {
                p.AName = e.AName;
                p.BName = e.BName;
            });
            Map<ALastEntity>().To<ALastProjection>().Using((e, p) =>
            {
                AutoMap(e, p);
            });
        }
    }
}