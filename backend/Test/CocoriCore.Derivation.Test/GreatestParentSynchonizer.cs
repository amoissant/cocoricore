﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocoriCore.Derivation.Test
{
    public class GreatestParentSynchonizer : SynchronizerBase<AParent, AProjectionWithParents>,
        IAutoRegister
    {
        public GreatestParentSynchonizer(DependencyCoupler coupler)
            : base(coupler)
        {
        }

        public override Task SynchonizeAsync(DependencyInfo info, IEnumerable<AParent> dependencies, IEnumerable<AProjectionWithParents> projections)
        {
            var groups = Coupler.GroupDependenciesByProjection(info, dependencies, projections);
            foreach (var group in groups)
            {
                group.projection.GreatestParentId = group.dependencies.OrderByDescending(x => x.Height).FirstOrDefault()?.Id;
            }
            return Task.CompletedTask;
        }
    }
}