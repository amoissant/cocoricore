using CocoriCore.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CocoriCore.Derivation.Test
{
    public class ACustomSynchronizer : ProjectionHandlerBase, IProjectionCreator, IAutoRegister
    {
        public ACustomSynchronizer(IDerivationRepository repository, ISynchronizerProcessor processor,
            IFactory factory, JoinLoader joinLoader) 
            : base(repository, processor, factory, joinLoader)
        {
        }

        public override async Task<IEnumerable<IEntity>> InitProjectionsAsync(DerivationRule rule, IEnumerable<IEntity> dependencies)
        {
            var projections = await base.InitProjectionsAsync(rule, dependencies);
            foreach(var projection in projections)
            {
                ((AProjectionWithCustomConstruction)projection).EntityId = projection.Id;
            }
            return projections;
        }
    }
}