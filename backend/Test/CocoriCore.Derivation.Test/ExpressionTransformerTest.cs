﻿using CocoriCore.Common;
using CocoriCore.TestUtils;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Xunit;

namespace CocoriCore.Derivation.Test
{
    public class ExpressionTransformerTest
    {
        [Fact]
        public void TransformTrue()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => true;
            var dependencies = new Dependency[0];
            var projections = new[]
            {
                new Projection { Id = Tuid.Guid("1") },
                new Projection { Id = Tuid.Guid("2") },
                new Projection { Id = Tuid.Guid("3") }
            };

            var loadParameter = joinExpression.Parameters[1];
            var predicate = new ExpressionTransformer().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(projections, predicate);
            result.Should().HaveCount(3);
            result.Should().Contain(projections[0])
                .And.Contain(projections[1])
                .And.Contain(projections[2]);
        }

        [Fact]
        public void TransformFalse()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => false;
            var dependencies = new Dependency[0];
            var projections = new[]
            {
                new Projection { Id = Tuid.Guid("1") },
                new Projection { Id = Tuid.Guid("2") },
                new Projection { Id = Tuid.Guid("3") }
            };

            var loadParameter = joinExpression.Parameters[1];
            var predicate = new ExpressionTransformer().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(projections, predicate);
            result.Should().HaveCount(0);
            result.Should().NotContain(projections[0])
                .And.NotContain(projections[1])
                .And.NotContain(projections[2]);
        }

        [Fact]
        public void TransformSingleEquality_left()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => e.Id == p.Id;
            var dependencies = new[]
            {
                new Dependency { Id = Tuid.Guid("1") },
                new Dependency { Id = Tuid.Guid("2") }
            };
            var projections = new[]
            {
                new Projection { Id = Tuid.Guid("1") },
                new Projection { Id = Tuid.Guid("2") },
                new Projection { Id = Tuid.Guid("3") }
            };

            var loadParameter = joinExpression.Parameters[1];
            var predicate = new ExpressionTransformer().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(projections, predicate);
            result.Should().HaveCount(2);
            result.Should().Contain(projections[0])
                .And.Contain(projections[1])
                .And.NotContain(projections[2]);
        }

        [Fact]
        public void TransformSingleEquality_right()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => p.Id == e.Id;
            var dependencies = new[] 
            { 
                new Dependency { Id = Tuid.Guid("1") }, 
                new Dependency { Id = Tuid.Guid("2") } 
            };
            var projections = new[]
            {
                new Projection { Id = Tuid.Guid("1") },
                new Projection { Id = Tuid.Guid("2") },
                new Projection { Id = Tuid.Guid("3") }
            };

            var loadParameter = joinExpression.Parameters[1];
            var predicate = new ExpressionTransformer().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(projections, predicate);
            result.Should().HaveCount(2);
            result.Should().Contain(projections[0])
                .And.Contain(projections[1])
                .And.NotContain(projections[2]);
        }

        [Fact]
        public void TransformSingleNotEquality()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => e.Id != p.Id;
            var dependencies = new[]
            {
                new Dependency { Id = Tuid.Guid("1") },
                new Dependency { Id = Tuid.Guid("2") }
            };
            var projections = new[]
            {
                new Projection { Id = Tuid.Guid("1") },
                new Projection { Id = Tuid.Guid("2") },
                new Projection { Id = Tuid.Guid("3") }
            };

            var loadParameter = joinExpression.Parameters[1];
            var predicate = new ExpressionTransformer().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(projections, predicate);
            result.Should().HaveCount(1);
            result.Should().NotContain(projections[0])
                .And.NotContain(projections[1])
                .And.Contain(projections[2]);

        }
        [Fact]
        public void TransformDoubleEquality_and()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => e.Id == p.Id && p.Id2 == e.Id2;
            var dependencies = new[]
            {
                new Dependency { Id = Tuid.Guid("1"), Id2 = Tuid.Guid("a") },
                new Dependency { Id = Tuid.Guid("2"), Id2 = Tuid.Guid("b") }
            };
            var projections = new[]
            {
                new Projection { Id = Tuid.Guid("1"), Id2 = Tuid.Guid("a") },
                new Projection { Id = Tuid.Guid("2"), Id2 = Tuid.Guid("c") },
                new Projection { Id = Tuid.Guid("3"), Id2 = Tuid.Guid("d") }
            };

            var loadParameter = joinExpression.Parameters[1];
            var predicate = new ExpressionTransformer().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(projections, predicate);
            result.Should().HaveCount(1);
            result.Should().Contain(projections[0])
                .And.NotContain(projections[1])
                .And.NotContain(projections[2]);
        }

        [Fact]
        public void TransformDoubleEquality_or()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => e.Id == p.Id || p.Id2 == e.Id2;
            var dependencies = new[]
            {
                new Dependency { Id = Tuid.Guid("1"), Id2 = Tuid.Guid("a") },
                new Dependency { Id = Tuid.Guid("2"), Id2 = Tuid.Guid("b") }
            };
            var projections = new[]
            {
                new Projection { Id = Tuid.Guid("1"), Id2 = Tuid.Guid("a") },
                new Projection { Id = Tuid.Guid("2"), Id2 = Tuid.Guid("c") },
                new Projection { Id = Tuid.Guid("3"), Id2 = Tuid.Guid("d") }
            };

            var loadParameter = joinExpression.Parameters[1];
            var predicate = new ExpressionTransformer().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(projections, predicate);
            result.Should().HaveCount(2);
            result.Should().Contain(projections[0])
                .And.Contain(projections[1])
                .And.NotContain(projections[2]);
        }

        [Fact]
        public void TransformSingleEquality_projectionConstant()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => p.Id == Tuid.Guid("1");
            var dependencies = new Dependency[0];
            var projections = new[]
            {
                new Projection { Id = Tuid.Guid("1") },
                new Projection { Id = Tuid.Guid("2") },
                new Projection { Id = Tuid.Guid("3") }
            };

            var loadParameter = joinExpression.Parameters[1];
            var predicate = new ExpressionTransformer().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(projections, predicate);
            result.Should().HaveCount(1);
            result.Should().Contain(projections[0])
                .And.NotContain(projections[1])
                .And.NotContain(projections[2]);
        }

        [Fact]
        public void TransformSingleEquality_dependencyConstantEq()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => e.Id2 == Tuid.Guid("a");
            var dependencies = new Dependency[0];
            var projections = new[]
            {
                new Projection { Id = Tuid.Guid("1") },
                new Projection { Id = Tuid.Guid("2") },
                new Projection { Id = Tuid.Guid("3") }
            };

            var loadParameter = joinExpression.Parameters[1];
            var predicate = new ExpressionTransformer().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(projections, predicate);
            result.Should().HaveCount(3);
            result.Should().Contain(projections[0])
                .And.Contain(projections[1])
                .And.Contain(projections[2]);
        }

        [Fact]
        public void TransformSingleEquality_dependencyConstantNotEq()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => e.Id2 != Tuid.Guid("a");
            var dependencies = new Dependency[0];
            var projections = new[]
            {
                new Projection { Id = Tuid.Guid("1") },
                new Projection { Id = Tuid.Guid("2") },
                new Projection { Id = Tuid.Guid("3") }
            };

            var loadParameter = joinExpression.Parameters[1];
            var predicate = new ExpressionTransformer().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(projections, predicate);
            result.Should().HaveCount(3);
            result.Should().Contain(projections[0])
                .And.Contain(projections[1])
                .And.Contain(projections[2]);
        }

        [Fact]
        public void TransformDoubleEquality_projectionConstantEq()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => e.Id == p.Id && p.Id2 == Tuid.Guid("a");
            var dependencies = new[]
            {
                new Dependency { Id = Tuid.Guid("1") },
                new Dependency { Id = Tuid.Guid("2") }
            };
            var projections = new[]
            {
                new Projection { Id = Tuid.Guid("1"), Id2 = Tuid.Guid("a") },
                new Projection { Id = Tuid.Guid("2"), Id2 = Tuid.Guid("b") },
                new Projection { Id = Tuid.Guid("3"), Id2 = Tuid.Guid("c") }
            };

            var loadParameter = joinExpression.Parameters[1];
            var predicate = new ExpressionTransformer().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(projections, predicate);
            result.Should().HaveCount(1);
            result.Should().Contain(projections[0])
                .And.NotContain(projections[1])
                .And.NotContain(projections[2]);
        }

        [Fact]
        public void TransformDoubleEquality_projectionConstantNotEq()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => e.Id == p.Id && p.Id2 != Tuid.Guid("a");
            var dependencies = new[]
            {
                new Dependency { Id = Tuid.Guid("1") },
                new Dependency { Id = Tuid.Guid("2") }
            };
            var projections = new[]
            {
                new Projection { Id = Tuid.Guid("1"), Id2 = Tuid.Guid("a") },
                new Projection { Id = Tuid.Guid("2"), Id2 = Tuid.Guid("b") },
                new Projection { Id = Tuid.Guid("3"), Id2 = Tuid.Guid("c") }
            };

            var loadParameter = joinExpression.Parameters[1];
            var predicate = new ExpressionTransformer().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(projections, predicate);
            result.Should().HaveCount(1);
            result.Should().NotContain(projections[0])
                .And.Contain(projections[1])
                .And.NotContain(projections[2]);
        }

        [Fact]
        public void TransformDoubleEquality_dependencyConstantAnd()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => e.Id == p.Id && e.Id2 == Tuid.Guid("a");
            var dependencies = new[]
            {
                new Dependency { Id = Tuid.Guid("1") },
                new Dependency { Id = Tuid.Guid("2") }
            };
            var projections = new[]
            {
                new Projection { Id = Tuid.Guid("1"), Id2 = Tuid.Guid("a") },
                new Projection { Id = Tuid.Guid("2"), Id2 = Tuid.Guid("b") },
                new Projection { Id = Tuid.Guid("3"), Id2 = Tuid.Guid("c") }
            };

            var loadParameter = joinExpression.Parameters[1];
            var predicate = new ExpressionTransformer().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(projections, predicate);
            result.Should().HaveCount(2);
            result.Should().Contain(projections[0])
                .And.Contain(projections[1])
                .And.NotContain(projections[2]);
        }

        [Fact]
        public void TransformDoubleEquality_dependencyConstantOr()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => e.Id == p.Id || e.Id2 == Tuid.Guid("a");
            var dependencies = new[]
            {
                new Dependency { Id = Tuid.Guid("1") },
                new Dependency { Id = Tuid.Guid("2") }
            };
            var projections = new[]
            {
                new Projection { Id = Tuid.Guid("1"), Id2 = Tuid.Guid("a") },
                new Projection { Id = Tuid.Guid("2"), Id2 = Tuid.Guid("b") },
                new Projection { Id = Tuid.Guid("3"), Id2 = Tuid.Guid("c") }
            };

            var loadParameter = joinExpression.Parameters[1];
            var predicate = new ExpressionTransformer().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(projections, predicate);
            result.Should().HaveCount(2);
            result.Should().Contain(projections[0])
                .And.Contain(projections[1])
                .And.NotContain(projections[2]);
        }

        [Fact]
        public void TransformDoubleEquality_dependencyConstantOrAnd()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => e.Id == p.Id || (e.Id2 == Tuid.Guid("a") && p.Id2 == Tuid.Guid("b"));
            var dependencies = new[]
            {
                new Dependency { Id = Tuid.Guid("1") },
                new Dependency { Id = Tuid.Guid("2") }
            };
            var projections = new[]
            {
                new Projection { Id = Tuid.Guid("1"), Id2 = Tuid.Guid("a") },
                new Projection { Id = Tuid.Guid("3"), Id2 = Tuid.Guid("b") },
                new Projection { Id = Tuid.Guid("3"), Id2 = Tuid.Guid("c") }
            };

            var loadParameter = joinExpression.Parameters[1];
            var predicate = new ExpressionTransformer().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(projections, predicate);
            result.Should().HaveCount(2);
            result.Should().Contain(projections[0])
                .And.Contain(projections[1])
                .And.NotContain(projections[2]);
        }

        [Fact]
        public void TransformSingleEquality_dependencyBoolEq()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => e.IsOk;
            var dependencies = new Dependency[0];
            var projections = new[]
            {
                new Projection { Id = Tuid.Guid("1") },
                new Projection { Id = Tuid.Guid("2") },
                new Projection { Id = Tuid.Guid("3") }
            };

            var loadParameter = joinExpression.Parameters[1];
            var predicate = new ExpressionTransformer().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(projections, predicate);
            result.Should().HaveCount(3);
            result.Should().Contain(projections[0])
                .And.Contain(projections[1])
                .And.Contain(projections[2]);
        }

        [Fact]
        public void TransformSingleEquality_dependencyBoolNotEq()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => !e.IsOk;
            var dependencies = new Dependency[0];
            var projections = new[]
            {
                new Projection { Id = Tuid.Guid("1") },
                new Projection { Id = Tuid.Guid("2") },
                new Projection { Id = Tuid.Guid("3") }
            };

            var loadParameter = joinExpression.Parameters[1];
            var predicate = new ExpressionTransformer().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(projections, predicate);
            result.Should().HaveCount(3);
            result.Should().Contain(projections[0])
                .And.Contain(projections[1])
                .And.Contain(projections[2]);
        }

        [Fact]
        public void TransformSingleEquality_dependencyBoolEqAnd()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => e.IsOk && p.Id == e.Id;
            var dependencies = new[]
            {
                new Dependency { Id = Tuid.Guid("1") },
                new Dependency { Id = Tuid.Guid("2") }
            };
            var projections = new[]
            {
                new Projection { Id = Tuid.Guid("1") },
                new Projection { Id = Tuid.Guid("2") },
                new Projection { Id = Tuid.Guid("3") }
            };

            var loadParameter = joinExpression.Parameters[1];
            var predicate = new ExpressionTransformer().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(projections, predicate);
            result.Should().HaveCount(2);
            result.Should().Contain(projections[0])
                .And.Contain(projections[1])
                .And.NotContain(projections[2]);
        }

        [Fact]
        public void TransformSingleEquality_dependencyBoolNotEqAnd()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => !e.IsOk && p.Id == e.Id;
            var dependencies = new[]
            {
                new Dependency { Id = Tuid.Guid("1") },
                new Dependency { Id = Tuid.Guid("2") }
            };
            var projections = new[]
            {
                new Projection { Id = Tuid.Guid("1") },
                new Projection { Id = Tuid.Guid("2") },
                new Projection { Id = Tuid.Guid("3") }
            };

            var loadParameter = joinExpression.Parameters[1];
            var predicate = new ExpressionTransformer().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(projections, predicate);
            result.Should().HaveCount(2);
            result.Should().Contain(projections[0])
                .And.Contain(projections[1])
                .And.NotContain(projections[2]);
        }

        [Fact]
        public void TransformSingleEquality_dependencyAndProjectionSameType()
        {
            Expression<Func<Projection, Projection, bool>> joinExpression = (e, p) => e.Id == p.Id2;
            var dependencies = new[]
            {
                new Projection { Id = Tuid.Guid("1") },
                new Projection { Id = Tuid.Guid("2") }
            };
            var projections = new[]
            {
                new Projection { Id2 = Tuid.Guid("1") },
                new Projection { Id2 = Tuid.Guid("2") },
                new Projection { Id2 = Tuid.Guid("3") }
            };

            var loadParameter = joinExpression.Parameters[1];
            var predicate = new ExpressionTransformer().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(projections, predicate);
            result.Should().HaveCount(2);
            result.Should().Contain(projections[0])
                .And.Contain(projections[1])
                .And.NotContain(projections[2]);
        }

        public IEnumerable<T> ApplyPredicate<T>(IEnumerable<T> source, LambdaExpression predicate)
        {
            return ConstructQueryableWhere(source.AsQueryable(), typeof(Projection), predicate).Cast<T>().ToArray();
        }

        protected virtual IQueryable ConstructQueryableWhere(IQueryable queryable, Type typeToLoad, LambdaExpression predicate)
        {
            var whereMethod = QueryableMethods.Where.MakeGenericMethod(typeToLoad);
            var whereExp = Expression.Call(whereMethod, new Expression[] { queryable.Expression, Expression.Quote(predicate) });
            return queryable.Provider.CreateQuery(whereExp);
        }

        public class Dependency : IEntity
        {
            public Guid Id { get; set; }
            public Guid Id2 { get; set; }
            public bool IsOk { get; set; }
        }

        public class Projection : IEntity
        {
            public Guid Id { get; set; }
            public Guid? Id2 { get; set; }
        }

        public class ValueVisitor : ExpressionVisitor
        {
            protected override Expression VisitConstant(ConstantExpression node)
            {
                return base.VisitConstant(node);
            }
        }
    }
}
