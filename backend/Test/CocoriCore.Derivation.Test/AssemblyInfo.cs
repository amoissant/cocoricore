﻿using System.Reflection;

namespace CocoriCore.Derivation.Test
{
    public static class AssemblyInfo
    {
        public static Assembly Assembly => typeof(AssemblyInfo).Assembly;
    }
}
