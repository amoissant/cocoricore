using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocoriCore.Derivation.Test
{
    public class AOneManyProjectionSynchronizer : SynchronizerBase<AOneEntity, AOneManyProjection>,
        IAutoRegister
    {
        public AOneManyProjectionSynchronizer(DependencyCoupler coupler) 
            : base(coupler)
        {
        }

        public override Task ResetAsync(DependencyInfo info, IEnumerable<AOneEntity> dependencies, IEnumerable<AOneManyProjection> projections)
        {
            foreach (var projection in projections)
            {
                projection.OneCount = 0;
            }
            return Task.CompletedTask;
        }

        public override Task SynchonizeAsync(DependencyInfo info, IEnumerable<AOneEntity> dependencies, IEnumerable<AOneManyProjection> projections)
        {
            var groups = Coupler.GroupDependenciesAndProjections(info, dependencies, projections);
            foreach (var group in groups)
            {
                foreach (var projection in projections)
                {
                    projection.OneCount = group.dependencies.Count();
                }
            }
            return Task.CompletedTask;
        }
    }
}