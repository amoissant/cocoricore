﻿using System;
using Xunit;
using FluentAssertions;
using System.Threading.Tasks;
using Autofac;
using System.Linq;
using CocoriCore.Test;
using Microsoft.Extensions.Caching.Memory;
using CocoriCore.Autofac;
using Xunit.Abstractions;
using CocoriCore.TestUtils;
using CocoriCore.Repository;
using CocoriCore.Common;
using System.Collections.Generic;
using CocoriCore.Linq.Async;
using CocoriCore.JsonNet;
using System.Reflection;

namespace CocoriCore.Derivation.Test
{
    public class DerivationBusTest : BusTestBase
    {
        private DerivationBusOptionsBuilder _derivationBuilder;
        private RepositoryFailureOptions _fakeRepositoryConfiguration;
        private IRepository Repository => RootScope.Resolve<IRepository>();
        private IMemoryCache _cache;

        public DerivationBusTest(ITestOutputHelper output)
        {
            _cache = new MemoryCache(new MemoryCacheOptions());

            _derivationBuilder = new DerivationBusOptionsBuilder();
            _derivationBuilder.ConfigureMappings(this.GetType().Assembly);
            //TODO vérifier qu'on a au moins une assembly de configurée
            _derivationBuilder.EntityAssemblies = new Assembly[] { CocoriCore.Test.AssemblyInfo.Assembly, CocoriCore.Derivation.Test.AssemblyInfo.Assembly };
            _derivationBuilder.DifferDependencyRules = true;
            _derivationBuilder.DependencyRulesBatchSize = 1;
            _derivationBuilder.WaitDependencyRules = true;
            _unitOfWorkBuilder
                .Call<IRepository>(r => r.FlushAsync())
                .Call<IDerivationBus>(b => b.ApplyRulesAsync())
                .Call<ITransactionHolder>(b => b.CommitAsync())
                .Call<IDerivationBus>(b => b.ApplyDifferedRulesAsync());
            _fakeRepositoryConfiguration = new RepositoryFailureOptions();

            ContainerBuilder.RegisterInstance(_cache).As<IMemoryCache>();
            ContainerBuilder.RegisterInstance(_derivationBuilder).AsSelf();
            ContainerBuilder.RegisterInstance(_fakeRepositoryConfiguration).AsSelf();
            ContainerBuilder.RegisterType<Factory>().As<IFactory>().SingleInstance();
            ContainerBuilder.RegisterType<InMemoryEntityStore>().As<IInMemoryEntityStore>().SingleInstance();

            ContainerBuilder.RegisterType<TransactionalInMemoryBaseRepository>()
                .AsSelf()
                .As<IBaseRepository>()
                .As<ITransactionHolder>()
                .InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<MyRepository>()
               .As<IDerivationRepository, IRepository>()
               .WithParameter(new TypedParameter<IEnumerable<Type>>(new[] { typeof(EntityEventDecorator), typeof(StateStoreDecorator), typeof(FailureDecorator) }))
               .InstancePerLifetimeScope();

            ContainerBuilder.Register(c => _derivationBuilder.Build()).As<DerivationBusOptions>().SingleInstance();
            ContainerBuilder.RegisterType<DerivationBus>().AsSelf().As<IDerivationBus>().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<StateStore>().As<IStateStore>().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<EntityEventStore>().AsImplementedInterfaces().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<StateStoreDecorator>().AsSelf().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<EntityEventDecorator>().AsSelf().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<FailureDecorator>().AsSelf().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<Utf8JsonCopier>().As<ICopier>().InstancePerLifetimeScope();

            ContainerBuilder.RegisterType<DependencyManager>().AsSelf().SingleInstance();
            ContainerBuilder.RegisterType<ObjectSerializer>().AsImplementedInterfaces().SingleInstance();

            ContainerBuilder.RegisterAssemblyTypes(CocoriCore.Derivation.AssemblyInfo.Assembly, this.GetType().Assembly)
                .AssignableTo<IAutoRegister>()
                .AsImplementedInterfaces()
                .AsSelf();
            ContainerBuilder.RegisterAssemblyTypes(this.GetType().Assembly)
                .AssignableTo<IEntity>()
                .AsSelf();

            ContainerBuilder.Register(c => new DefaultErrorBusOptionsBuilder().Options).SingleInstance();
            ContainerBuilder.RegisterType<ErrorBus>().AsImplementedInterfaces().InstancePerLifetimeScope();

            ContainerBuilder.ConfigureXunitLogger();
        }

        #region projection

        [Fact]
        public async Task WhenInsertEntityInsertProjection()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();

            AnEntity entity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                entity.Name = "toto";
                entity.Start = DateTime.Now;
                entity.End = entity.Start.AddHours(1);
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }
            var projection = await Repository.LoadAsync<AProjection>(entity.Id);
            projection.Should().NotBeNull();
            projection.EntityName.Should().Be("toto");
            projection.Start.Should().Be(entity.Start);
            projection.End.Should().Be(entity.End);
            projection.Duration.TotalHours.Should().Be(1);
        }

        [Fact]
        public async Task WhenUpdateEntityUpdateProjection()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();

            AnEntity entity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                entity.Start = DateTime.Now;
                entity.End = entity.Start.AddHours(1);
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = await repository.LoadAsync<AnEntity>(entity.Id);
                entity.End = entity.End.AddHours(1);
                await repository.UpdateAsync(entity);
                await uow.FinishAsync();
            }

            var projection = await Repository.LoadAsync<AProjection>(entity.Id);
            projection.Should().NotBeNull();
            projection.Start.Should().Be(entity.Start);
            projection.End.Should().Be(entity.End);
            projection.Duration.TotalHours.Should().Be(2);
        }

        [Fact]
        public async Task WhenDeleteEntityDeleteProjection()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();

            AnEntity entity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            Repository.ExistsAsync<AProjection>(entity.Id).Result.Should().BeTrue();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = await repository.LoadAsync<AnEntity>(entity.Id);
                await repository.DeleteAsync(entity);
                await uow.FinishAsync();
            }

            Repository.ExistsAsync<AProjection>(entity.Id).Result.Should().BeFalse();
        }

        [Theory]
        [InlineData(false)]
        [InlineData(true)]
        public async Task CanCreateProjectionLikeLeftJoin(bool reverse)
        {
            _derivationBuilder.Project<AOneEntity, AManyEntity>(e => e.ManyIds)
                .IntoMany<AOneManyProjection>(p => p.OneId, p => p.ManyId)
                .UsingMapping();
            _derivationBuilder.Project<AOneEntity>().Into<AOneManyProjection>().UsingMapping();
            _derivationBuilder
                .ForDependency<AnotherEntity>()
                .Update<AOneManyProjection>((e, p) => e.Id == p.AnotherEntityId)
                .UsingMapping();

            AOneEntity oneEntity = null;
            AManyEntity manyEntity1 = null;
            AManyEntity manyEntity2 = null;
            AnotherEntity anotherEntity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                anotherEntity = new AnotherEntity().WithId();
                anotherEntity.Name = "another entity";
                await repository.InsertAsync(anotherEntity);
                await uow.FinishAsync();
            }
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                manyEntity1 = new AManyEntity().WithId();
                manyEntity1.Name = "titi";
                manyEntity1.AnotherEntityId = anotherEntity.Id;
                manyEntity2 = new AManyEntity().WithId();
                manyEntity2.Name = "tutu";
                oneEntity = new AOneEntity().WithId();
                oneEntity.Name = "toto";
                oneEntity.ManyIds = new Guid[] { manyEntity1.Id, manyEntity2.Id };

                var entitiesToInsert = new IEntity[] { oneEntity, manyEntity1, manyEntity2 };
                await repository.InsertManyAsync(entitiesToInsert.Reverse(reverse));
                await uow.FinishAsync();
            }

            var projections = await Repository
                .Query<AOneManyProjection>()
                .ToArrayAsync();

            projections.Should().HaveCount(3);
            var projection1 = projections.Single(x => x.Id == oneEntity.Id);
            projection1.Id.Should().Be(oneEntity.Id);
            projection1.ManyId.Should().BeNull();
            projection1.ManyName.Should().BeNull();
            projection1.OneId.Should().Be(oneEntity.Id);
            projection1.OneName.Should().Be("toto");
            projection1.AnotherEntityId.Should().Be(Guid.Empty);
            projection1.AnotherName.Should().BeNull();
            var projection2 = projections.Single(x => x.ManyId == manyEntity1.Id);
            projection2.Id.Should().NotBe(manyEntity1.Id)
                .And.NotBe(manyEntity2.Id)
                .And.NotBe(oneEntity.Id);
            projection2.ManyId.Should().Be(manyEntity1.Id);
            projection2.ManyName.Should().Be("titi");
            projection2.OneId.Should().Be(oneEntity.Id);
            projection2.OneName.Should().Be("toto");
            projection2.AnotherEntityId.Should().Be(anotherEntity.Id);
            projection2.AnotherName.Should().Be("another entity");
            var projection3 = projections.Single(x => x.ManyId == manyEntity2.Id);
            projection3.Id.Should().NotBe(manyEntity1.Id)
                .And.NotBe(manyEntity2.Id)
                .And.NotBe(oneEntity.Id);
            projection3.ManyId.Should().Be(manyEntity2.Id);
            projection3.ManyName.Should().Be("tutu");
            projection3.OneId.Should().Be(oneEntity.Id);
            projection3.OneName.Should().Be("toto");
            projection3.AnotherEntityId.Should().Be(Guid.Empty);
            projection3.AnotherName.Should().BeNull();
        }

        [Fact]
        public async Task CanCreateProjectionLikeLeftJoin_MultiStepInserts()
        {
            _derivationBuilder.Project<AOneEntity, AManyEntity>(e => e.ManyIds)
                .IntoMany<AOneManyProjection>(p => p.OneId, p => p.ManyId)
                .UsingMapping();
            _derivationBuilder.Project<AOneEntity>().Into<AOneManyProjection>().UsingMapping();
            _derivationBuilder
                .ForDependency<AnotherEntity>()
                .Update<AOneManyProjection>((e, p) => e.Id == p.AnotherEntityId)
                .UsingMapping();

            AOneEntity oneEntity = null;
            AManyEntity manyEntity1 = null;
            AManyEntity manyEntity2 = null;
            AnotherEntity anotherEntity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                anotherEntity = new AnotherEntity().WithId();
                anotherEntity.Name = "another entity";
                await repository.InsertAsync(anotherEntity);
                await uow.FinishAsync();
            }
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                manyEntity1 = new AManyEntity().WithId();
                manyEntity1.Name = "many entity 1";
                manyEntity1.AnotherEntityId = anotherEntity.Id;
                manyEntity2 = new AManyEntity().WithId();
                manyEntity2.Name = "many entity 2";
                await repository.InsertManyAsync(manyEntity1, manyEntity2);
                await uow.FinishAsync();
            }
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                oneEntity = new AOneEntity().WithId();
                oneEntity.Name = "one entity";
                oneEntity.ManyIds = new Guid[] { manyEntity1.Id, manyEntity2.Id };
                await repository.InsertAsync(oneEntity);
                await uow.FinishAsync();
            }

            var projections = await Repository
                .Query<AOneManyProjection>()
                .ToArrayAsync();

            projections.Should().HaveCount(3);
            var projection1 = projections.Single(x => x.Id == oneEntity.Id);
            projection1.Id.Should().Be(oneEntity.Id);
            projection1.ManyId.Should().BeNull();
            projection1.ManyName.Should().BeNull();
            projection1.OneId.Should().Be(oneEntity.Id);
            projection1.OneName.Should().Be("one entity");
            projection1.AnotherEntityId.Should().Be(Guid.Empty);
            projection1.AnotherName.Should().BeNull();
            var projection2 = projections.Single(x => x.ManyId == manyEntity1.Id);
            projection2.Id.Should().NotBe(manyEntity1.Id)
                .And.NotBe(manyEntity2.Id)
                .And.NotBe(oneEntity.Id);
            projection2.ManyId.Should().Be(manyEntity1.Id);
            projection2.ManyName.Should().Be("many entity 1");
            projection2.OneId.Should().Be(oneEntity.Id);
            projection2.OneName.Should().Be("one entity");
            projection2.AnotherEntityId.Should().Be(anotherEntity.Id);
            projection2.AnotherName.Should().Be("another entity");
            var projection3 = projections.Single(x => x.ManyId == manyEntity2.Id);
            projection3.Id.Should().NotBe(manyEntity1.Id)
                .And.NotBe(manyEntity2.Id)
                .And.NotBe(oneEntity.Id);
            projection3.ManyId.Should().Be(manyEntity2.Id);
            projection3.ManyName.Should().Be("many entity 2");
            projection3.OneId.Should().Be(oneEntity.Id);
            projection3.OneName.Should().Be("one entity");
            projection3.AnotherEntityId.Should().Be(Guid.Empty);
            projection3.AnotherName.Should().BeNull();
        }

        [Fact]
        public async Task CanUpdateProjectionLikeLeftJoin()
        {
            _derivationBuilder.Project<AOneEntity, AManyEntity>(e => e.ManyIds)
                .IntoMany<AOneManyProjection>(p => p.OneId, p => p.ManyId)
                .UsingMapping();

            AOneEntity oneEntity1 = null;
            AManyEntity manyEntity1 = null;
            AManyEntity manyEntity2 = null;
            AnotherEntity anotherEntity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                anotherEntity = new AnotherEntity().WithId();
                manyEntity1 = new AManyEntity().WithId();
                manyEntity1.AnotherEntityId = anotherEntity.Id;
                manyEntity2 = new AManyEntity().WithId();
                oneEntity1 = new AOneEntity().WithId();
                oneEntity1.ManyIds = new Guid[] { manyEntity1.Id, manyEntity2.Id };
                await repository.InsertManyAsync(manyEntity1, manyEntity2, oneEntity1);
                await uow.FinishAsync();
            }

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                manyEntity1 = await repository.LoadAsync<AManyEntity>(manyEntity1.Id);
                manyEntity1.Name = "titi";
                manyEntity2 = await repository.LoadAsync<AManyEntity>(manyEntity2.Id);
                manyEntity2.Name = "tutu";
                oneEntity1 = await repository.LoadAsync<AOneEntity>(oneEntity1.Id);
                oneEntity1.Name = "toto";
                oneEntity1.ManyIds = new Guid[] { manyEntity1.Id, manyEntity2.Id };
                await repository.UpdateManyAsync(manyEntity1, manyEntity2, oneEntity1);
                await uow.FinishAsync();
            }

            var projections = await Repository
                .Query<AOneManyProjection>()
                .Where(x => x.OneId == oneEntity1.Id)
                .ToArrayAsync();

            projections.Should().HaveCount(2);
            var projection1 = projections.Single(x => x.ManyId == manyEntity1.Id);
            projection1.Id.Should().NotBe(manyEntity1.Id)
                .And.NotBe(manyEntity2.Id)
                .And.NotBe(oneEntity1.Id);
            projection1.ManyId.Should().Be(manyEntity1.Id);
            projection1.ManyName.Should().Be("titi");
            projection1.OneId.Should().Be(oneEntity1.Id);
            projection1.OneName.Should().Be("toto");
            var projection2 = projections.Single(x => x.ManyId == manyEntity2.Id);
            projection2.Id.Should().NotBe(manyEntity1.Id)
                .And.NotBe(manyEntity2.Id)
                .And.NotBe(oneEntity1.Id);
            projection2.ManyId.Should().Be(manyEntity2.Id);
            projection2.ManyName.Should().Be("tutu");
            projection2.OneId.Should().Be(oneEntity1.Id);
            projection2.OneName.Should().Be("toto");
        }

        [Fact]
        public async Task CanUseDiscriminatorForOneManyProjection()
        {
            _derivationBuilder.Project<AOneEntity, AManyEntity>(e => e.ManyIds)
                .IntoMany<AOneManyProjection>(p => p.OneId, p => p.ManyId)
                .UsingMapping("many");

            AOneEntity oneEntity1 = null;
            AManyEntity manyEntity1 = null;
            AManyEntity manyEntity2 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                manyEntity1 = new AManyEntity().WithId();
                manyEntity1.Name = "titi";
                manyEntity2 = new AManyEntity().WithId();
                manyEntity2.Name = "tutu";
                oneEntity1 = new AOneEntity().WithId();
                oneEntity1.Name = "toto";
                oneEntity1.ManyIds = new Guid[] { manyEntity1.Id, manyEntity2.Id };
                await repository.InsertManyAsync(manyEntity1, manyEntity2, oneEntity1);
                await uow.FinishAsync();
            }

            var projections = await Repository
                .Query<AOneManyProjection>()
                .ToArrayAsync();

            projections.Should().HaveCount(2);
            var projection1 = projections.Single(x => x.ManyId == manyEntity1.Id);
            projection1.Id.Should().NotBe(manyEntity1.Id)
                .And.NotBe(manyEntity2.Id)
                .And.NotBe(oneEntity1.Id);
            projection1.ManyId.Should().Be(manyEntity1.Id);
            projection1.ManyName.Should().Be("TITI");
            projection1.OneId.Should().Be(oneEntity1.Id);
            projection1.OneName.Should().Be("TOTO");
            var projection2 = projections.Single(x => x.ManyId == manyEntity2.Id);
            projection2.Id.Should().NotBe(manyEntity1.Id)
                .And.NotBe(manyEntity2.Id)
                .And.NotBe(oneEntity1.Id);
            projection2.ManyId.Should().Be(manyEntity2.Id);
            projection2.ManyName.Should().Be("TUTU");
            projection2.OneId.Should().Be(oneEntity1.Id);
            projection2.OneName.Should().Be("TOTO");
        }

        [Fact]
        public async Task CanUseOtherIdPropertyForGroupUpdateJoin()
        {
            _derivationBuilder.Project<AOneEntity, AManyEntity>(e => e.ManyIds)
                .IntoMany<AOneManyProjection>(p => p.OneId, p => p.ManyId)
                .UsingMapping();
            _derivationBuilder.Project<AOneEntity>().Into<AOneManyProjection>().UsingMapping();
            _derivationBuilder
                .ForDependency<AOneEntity>()
                .Update<AOneManyProjection>((e, p) => e.ParentId == p.ParentId)
                .Using<AOneManyProjectionSynchronizer>();

            AOneEntity oneEntity1 = null;
            AOneEntity oneEntity2 = null;
            AManyEntity manyEntity1 = null;
            AManyEntity manyEntity2 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var parentId = Guid.NewGuid();
                var repository = uow.Resolve<IRepository>();
                manyEntity1 = new AManyEntity().WithId();
                manyEntity2 = new AManyEntity().WithId();
                oneEntity1 = new AOneEntity().WithId();
                oneEntity1.ParentId = parentId;
                oneEntity1.ManyIds = new Guid[] { manyEntity1.Id, manyEntity2.Id };
                oneEntity2 = new AOneEntity().WithId();
                oneEntity2.ParentId = parentId;
                oneEntity2.ManyIds = new Guid[] { manyEntity1.Id };
                await repository.InsertManyAsync(manyEntity1, manyEntity2, oneEntity1, oneEntity2);
                await uow.FinishAsync();
            }

            var projections = await Repository
                .Query<AOneManyProjection>()
                .Where(x => x.OneId == oneEntity1.Id)
                .ToArrayAsync();

            projections.Should().HaveCount(3);
            var projection1 = projections.Single(x => x.Id == oneEntity1.Id);
            projection1.OneCount.Should().Be(2);

            var projection2 = projections.Single(x => x.ManyId == manyEntity1.Id);
            projection2.Id.Should().NotBe(manyEntity1.Id)
                .And.NotBe(manyEntity2.Id)
                .And.NotBe(oneEntity1.Id);
            projection2.OneCount.Should().Be(2);

            var projection3 = projections.Single(x => x.ManyId == manyEntity2.Id);
            projection3.Id.Should().NotBe(manyEntity1.Id)
                .And.NotBe(manyEntity2.Id)
                .And.NotBe(oneEntity1.Id);
            projection2.OneCount.Should().Be(2);
        }

        [Fact]
        public async Task CantEraseDependencyIdsWithMapping_projectMany()
        {
            _derivationBuilder.Project<AOneEntity, AManyEntity>(e => e.ManyIds)
                .IntoMany<AOneManyProjection>(p => p.OneId, p => p.ManyId)
                .UsingMapping("erase");
            _derivationBuilder.Project<AOneEntity>().Into<AOneManyProjection>().UsingMapping("erase");

            AOneEntity oneEntity = null;
            AManyEntity manyEntity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var parentId = Guid.NewGuid();
                var repository = uow.Resolve<IRepository>();
                manyEntity = new AManyEntity().WithId();
                manyEntity.Name = "many";
                oneEntity = new AOneEntity().WithId();
                oneEntity.Name = "one";
                oneEntity.ManyIds = new Guid[] { manyEntity.Id };
                await repository.InsertManyAsync(oneEntity, manyEntity);
                await uow.FinishAsync();
            }

            var projections = await Repository
                .Query<AOneManyProjection>()
                .OrderBy(x => x.ManyName)
                .ToArrayAsync();

            projections.Should().HaveCount(2);
            projections[0].Id.Should().Be(oneEntity.Id);
            projections[0].ManyId.Should().BeNull();
            projections[0].ManyName.Should().BeNull();
            projections[0].OneId.Should().Be(oneEntity.Id);
            projections[0].OneName.Should().Be("one");
            projections[1].Id.Should().NotBe(oneEntity.Id);
            projections[1].ManyId.Should().Be(manyEntity.Id);
            projections[1].ManyName.Should().Be("many");
            projections[1].OneId.Should().Be(oneEntity.Id);
            projections[1].OneName.Should().Be("one");
        }

        #endregion

        #region dependency

        [Fact]
        public async Task WhenUpdateEntitysPropertySynchronizeProjection()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();
            _derivationBuilder
                .ForDependency<DependencyEntityA>(x => x.Name, x => x.Town)
                .Update<AProjection>((e, p) => e.Id == p.DependencyAId)
                .UsingMapping();

            DependencyEntityA dependency = null;
            AnEntity entity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependency = new DependencyEntityA().WithId();
                dependency.Name = "luc";
                dependency.Town = "dax";
                dependency.Bool = false;
                entity = new AnEntity().WithId();
                entity.DependencyAId = dependency.Id;
                await repository.InsertManyAsync(entity, dependency);
                await uow.FinishAsync();
            }

            _fakeRepositoryConfiguration.TotalNbQueryCall.Should().Be(6);
            var projection = await Repository.LoadAsync<AProjection>(entity.Id);
            projection.DependencyAName.Should().Be("luc");
            projection.Town.Should().Be("dax");

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependency = await repository.LoadAsync<DependencyEntityA>(dependency.Id);
                dependency.Bool = true;
                await repository.UpdateAsync(dependency);
                await uow.FinishAsync();
            }

            _fakeRepositoryConfiguration.TotalNbQueryCall.Should().Be(7);

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependency = await repository.LoadAsync<DependencyEntityA>(dependency.Id);
                dependency.Town = "bruges";
                await repository.UpdateAsync(dependency);
                await uow.FinishAsync();
            }

            _fakeRepositoryConfiguration.TotalNbQueryCall.Should().Be(11);
            projection = await Repository.LoadAsync<AProjection>(entity.Id);
            projection.DependencyAName.Should().Be("luc");
            projection.Town.Should().Be("bruges");

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependency = await repository.LoadAsync<DependencyEntityA>(dependency.Id);
                dependency.Name = "toto";
                await repository.UpdateAsync(dependency);
                await uow.FinishAsync();
            }

            _fakeRepositoryConfiguration.TotalNbQueryCall.Should().Be(15);
            projection = await Repository.LoadAsync<AProjection>(entity.Id);
            projection.DependencyAName.Should().Be("toto");
            projection.Town.Should().Be("bruges");
        }

        [Fact]
        public async Task WhenUpdateEntitySynchronizeProjection_WithBatchSizeEqualOne()
        {
            _derivationBuilder.DependencyRulesBatchSize = 1;
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();
            _derivationBuilder
                .ForDependency<DependencyEntityA>(x => x.Name, x => x.Town)
                .Update<AProjection>((e, p) => e.Id == p.DependencyAId)
                .UsingMapping();

            DependencyEntityA dependency = null;
            AnEntity entity1 = null;
            AnEntity entity2 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependency = new DependencyEntityA().WithId();
                dependency.Name = "dependency";
                entity1 = new AnEntity().WithId();
                entity1.DependencyAId = dependency.Id;
                entity2 = new AnEntity().WithId();
                entity2.DependencyAId = dependency.Id;
                await repository.InsertManyAsync(entity1, entity2, dependency);
                await uow.FinishAsync();
            }

            var projection1 = await Repository.LoadAsync<AProjection>(entity1.Id);
            var projection2 = await Repository.LoadAsync<AProjection>(entity2.Id);
            projection1.DependencyAName.Should().Be("dependency");
            projection2.DependencyAName.Should().Be("dependency");

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependency = await repository.LoadAsync<DependencyEntityA>(dependency.Id);
                dependency.Name = "new name";
                await repository.UpdateAsync(dependency);
                await uow.FinishAsync();
            }

            projection1 = await Repository.LoadAsync<AProjection>(projection1.Id);
            projection2 = await Repository.LoadAsync<AProjection>(projection2.Id);
            projection1.DependencyAName.Should().Be("new name");
            projection2.DependencyAName.Should().Be("new name");
        }

        [Fact]
        public async Task CanResetWithInterfaceDependency()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();
            _derivationBuilder
                .ForDependency<INamedEntity>()
                .Update<AProjection>((e, p) => e.Id == p.DependencyAId)
                .UsingMapping();

            DependencyEntityA dependency = null;
            AnEntity entity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependency = new DependencyEntityA().WithId();
                dependency.Name = "luc";
                entity = new AnEntity().WithId();
                entity.DependencyAId = dependency.Id;
                await repository.InsertManyAsync(entity, dependency);
                await uow.FinishAsync();
            }

            var projection = await Repository.LoadAsync<AProjection>(entity.Id);
            projection.DependencyAName.Should().Be("luc");

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependency = await repository.LoadAsync<DependencyEntityA>(dependency.Id);
                await repository.DeleteAsync(dependency);
                await uow.FinishAsync();
            }

            projection = await Repository.LoadAsync<AProjection>(entity.Id);
            projection.DependencyAName.Should().BeNull();
        }

        [Fact]
        public async Task CanUseJoinExpressionWithNotOnlyFKEquality()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();
            _derivationBuilder
                .ForDependency<DependencyEntityA>(x => x.Name, x => x.Town)
                .Update<AProjection>((e, p) => e.Name != null && e.Id == p.DependencyAId)
                .UsingMapping();

            AnEntity entity = null;
            DependencyEntityA dependencyA = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependencyA = new DependencyEntityA().WithId();
                dependencyA.Name = "depA";
                entity = new AnEntity().WithId();
                entity.Name = "entity";
                entity.DependencyAId = dependencyA.Id;
                await repository.InsertManyAsync(entity, dependencyA);
                await uow.FinishAsync();
            }

            var projection = await Repository.LoadAsync<AProjection>(entity.Id);
            projection.Id.Should().Be(entity.Id);
            projection.EntityName.Should().Be("entity");
            projection.DependencyAId.Should().Be(dependencyA.Id);
            projection.DependencyAName.Should().Be("depA");
            projection.DependencyBId.Should().BeNull();
        }

        #endregion

        #region mapper

        [Fact]
        public async Task WhenInsertEntityWithDependencyCallDependenciesMappers()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();
            _derivationBuilder.ForDependency<DependencyEntityA>().Update<AProjection>((e, p) => e.Id == p.DependencyAId).UsingMapping();
            _derivationBuilder.ForDependency<DependencyEntityB>().Update<AProjection>((e, p) => e.Id == p.DependencyBId).UsingMapping();

            AnEntity entity = null;
            DependencyEntityA dependencyA = null;
            DependencyEntityB dependencyB = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependencyB = new DependencyEntityB().WithId();
                dependencyB.Name = "toto";
                dependencyA = new DependencyEntityA().WithId();
                dependencyA.Name = "luc";
                dependencyA.DependencyBId = dependencyB.Id;
                entity = new AnEntity().WithId();
                entity.DependencyAId = dependencyA.Id;
                await repository.InsertManyAsync(entity, dependencyA, dependencyB);
                await uow.FinishAsync();
            }

            var projection = await Repository.LoadAsync<AProjection>(entity.Id);
            projection.DependencyAId.Should().Be(dependencyA.Id);
            projection.DependencyAName.Should().Be("luc");
            projection.DependencyBId.Should().Be(dependencyB.Id);
            projection.DependencyBName.Should().Be("toto");
        }

        [Fact]
        public async Task WhenRemoveDependencyForEntityWithDependencyCallDependenciesMappers()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();
            _derivationBuilder.ForDependency<DependencyEntityA>().Update<AProjection>((e, p) => e.Id == p.DependencyAId).UsingMapping();
            _derivationBuilder.ForDependency<DependencyEntityB>().Update<AProjection>((e, p) => e.Id == p.DependencyBId).UsingMapping();

            AnEntity entity = null;
            DependencyEntityA dependencyA = null;
            DependencyEntityB dependencyB = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependencyB = new DependencyEntityB().WithId();
                dependencyB.Name = "toto";
                dependencyA = new DependencyEntityA().WithId();
                dependencyA.Name = "luc";
                dependencyA.DependencyBId = dependencyB.Id;
                entity = new AnEntity().WithId();
                entity.DependencyAId = dependencyA.Id;
                await repository.InsertManyAsync(entity, dependencyA, dependencyB);
                await uow.FinishAsync();
            }

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = await repository.LoadAsync<AnEntity>(entity.Id);
                entity.DependencyAId = Guid.Empty;
                await repository.UpdateAsync(entity);
                await uow.FinishAsync();
            }

            var projection = await Repository.LoadAsync<AProjection>(entity.Id);
            projection.DependencyAId.Should().BeEmpty();
            projection.DependencyAName.Should().BeNull();
            projection.DependencyBId.Should().BeNull();
            projection.DependencyBName.Should().BeNull();
        }

        [Fact]
        public async Task WhenUpdateEntityWithDependencyCallOnlyNeededDependenciesMappers()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();
            _derivationBuilder.ForDependency<DependencyEntityA>().Update<AProjection>((e, p) => e.Id == p.DependencyAId).UsingMapping();
            _derivationBuilder.ForDependency<DependencyEntityB>().Update<AProjection>((e, p) => e.Id == p.DependencyBId).UsingMapping();

            AnEntity entity = null;
            DependencyEntityA dependencyA1 = null;
            DependencyEntityA dependencyA2 = null;
            DependencyEntityB dependencyB = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependencyB = new DependencyEntityB().WithId();
                dependencyB.Name = "toto";
                dependencyA1 = new DependencyEntityA().WithId();
                dependencyA1.Name = "luc";
                dependencyA1.DependencyBId = dependencyB.Id;
                dependencyA2 = new DependencyEntityA().WithId();
                dependencyA2.Name = "etienne";
                dependencyA2.DependencyBId = dependencyB.Id;
                entity = new AnEntity().WithId();
                entity.DependencyAId = dependencyA1.Id;
                await repository.InsertManyAsync(entity, dependencyA1, dependencyA2, dependencyB);
                await uow.FinishAsync();
            }

            _fakeRepositoryConfiguration.Reset();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = await repository.LoadAsync<AnEntity>(entity.Id);
                entity.DependencyAId = dependencyA2.Id;
                await repository.UpdateAsync(entity);
                await uow.FinishAsync();
            }

            _fakeRepositoryConfiguration.LoadedEntities.Contains((typeof(DependencyEntityB), dependencyB.Id)).Should().BeFalse();
            var projection = await Repository.LoadAsync<AProjection>(entity.Id);
            projection.DependencyAId.Should().Be(dependencyA2.Id);
            projection.DependencyAName.Should().Be("etienne");
            projection.DependencyBId.Should().Be(dependencyB.Id);
            projection.DependencyBName.Should().Be("toto");
        }

        [Fact]
        public async Task WhenUpdateDependencyCallNeededDependenciesMappers()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();
            _derivationBuilder.ForDependency<DependencyEntityA>().Update<AProjection>((e, p) => e.Id == p.DependencyAId).UsingMapping();
            _derivationBuilder.ForDependency<DependencyEntityB>().Update<AProjection>((e, p) => e.Id == p.DependencyBId).UsingMapping();

            AnEntity entity = null;
            DependencyEntityA dependencyA = null;
            DependencyEntityB dependencyB2 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var dependencyB1 = new DependencyEntityB().WithId();
                dependencyB1.Name = "toto";
                dependencyB2 = new DependencyEntityB().WithId();
                dependencyB2.Name = "titi";
                dependencyA = new DependencyEntityA().WithId();
                dependencyA.Name = "luc";
                dependencyA.DependencyBId = dependencyB1.Id;
                entity = new AnEntity().WithId();
                entity.DependencyAId = dependencyA.Id;
                await repository.InsertManyAsync(dependencyB1, dependencyB2, dependencyA, entity);
                await uow.FinishAsync();
            }

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependencyA = await repository.LoadAsync<DependencyEntityA>(dependencyA.Id);
                dependencyA.Name = "amandine";
                dependencyA.DependencyBId = dependencyB2.Id;
                await repository.UpdateAsync(dependencyA);
                await uow.FinishAsync();
            }

            var projection = await Repository.LoadAsync<AProjection>(entity.Id);
            projection.DependencyAId.Should().Be(dependencyA.Id);
            projection.DependencyAName.Should().Be("amandine");
            projection.DependencyBId.Should().Be(dependencyB2.Id);
            projection.DependencyBName.Should().Be("titi");
        }

        [Fact]
        public async Task CallMappersForAllEntityDependencies()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();
            _derivationBuilder.ForDependency<DependencyEntityA>().Update<AProjection>((e, p) => e.Id == p.DependencyAId).UsingMapping();
            _derivationBuilder.ForDependency<DependencyEntityB>().Update<AProjection>((e, p) => e.Id == p.DependencyBId).UsingMapping();
            _derivationBuilder.ForDependency<DependencyEntityC>().Update<AProjection>((e, p) => e.Id == p.DependencyCId).UsingMapping();
            _derivationBuilder.ForDependency<DependencyEntityD>().Update<AProjection>((e, p) => e.Id == p.DependencyDId).UsingMapping();

            AnEntity entity = null;
            DependencyEntityA dependencyA = null;
            DependencyEntityB dependencyB = null;
            DependencyEntityC dependencyC = null;
            DependencyEntityD dependencyD = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependencyA = new DependencyEntityA().WithId();
                dependencyA.Name = "A";
                dependencyB = new DependencyEntityB().WithId();
                dependencyB.Name = "B";
                dependencyC = new DependencyEntityC().WithId();
                dependencyC.Name = "C";
                dependencyD = new DependencyEntityD().WithId();
                dependencyD.Name = "D";
                dependencyA.DependencyBId = dependencyB.Id;
                dependencyA.DependencyCId = dependencyC.Id;
                entity = new AnEntity().WithId();
                entity.DependencyAId = dependencyA.Id;
                entity.DependencyDId = dependencyD.Id;
                await repository.InsertManyAsync(dependencyD, dependencyC, dependencyB, dependencyA, entity);
                await uow.FinishAsync();
            }

            var projection = await Repository.LoadAsync<AProjection>(entity.Id);
            projection.DependencyAId.Should().Be(dependencyA.Id);
            projection.DependencyAName.Should().Be("A");
            projection.DependencyBId.Should().Be(dependencyB.Id);
            projection.DependencyBName.Should().Be("B");
            projection.DependencyCId.Should().Be(dependencyC.Id);
            projection.DependencyCName.Should().Be("C");
            projection.DependencyDId.Should().Be(dependencyD.Id);
            projection.DependencyDName.Should().Be("D");
        }

        [Fact]
        public async Task CanNormalizeNullableIds()
        {
            _derivationBuilder.HandleGuidToNullableGuid = true;
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();
            _derivationBuilder.ForDependency<DependencyEntityA>().Update<AProjection>((e, p) => e.Id == p.DependencyAId).UsingMapping();
            _derivationBuilder.ForDependency<DependencyEntityB>().Update<AProjection>((e, p) => e.Id == p.DependencyBId).UsingMapping();
            _derivationBuilder.ForDependency<DependencyEntityC>().Update<AProjection>((e, p) => e.Id == p.DependencyCId).UsingMapping();
            _derivationBuilder.ForDependency<DependencyEntityD>().Update<AProjection>((e, p) => e.Id == p.DependencyDId).UsingMapping();

            AnEntity entity = null;
            DependencyEntityA dependencyA = null;
            DependencyEntityB dependencyB = null;
            DependencyEntityC dependencyC = null;
            DependencyEntityD dependencyD = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependencyA = new DependencyEntityA().WithId();
                dependencyA.Name = "A";
                dependencyB = new DependencyEntityB().WithId();
                dependencyB.Name = "B";
                dependencyC = new DependencyEntityC().WithId();
                dependencyC.Name = "C";
                dependencyD = new DependencyEntityD().WithId();
                dependencyD.Name = "D";
                dependencyA.DependencyBId = dependencyB.Id;
                dependencyA.DependencyCId = dependencyC.Id;
                entity = new AnEntity().WithId();
                entity.DependencyAId = dependencyA.Id;
                entity.DependencyDId = dependencyD.Id;
                await repository.InsertManyAsync(dependencyD, dependencyC, dependencyB, dependencyA, entity);
                await uow.FinishAsync();
            }

            var projection = await Repository.LoadAsync<AProjection>(entity.Id);
            projection.DependencyAId.Should().Be(dependencyA.Id);
            projection.DependencyAName.Should().Be("A");
            projection.DependencyBId.Should().Be(dependencyB.Id);
            projection.DependencyBName.Should().Be("B");
            projection.DependencyCId.Should().Be(dependencyC.Id);
            projection.DependencyCName.Should().Be("C");

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = await repository.LoadAsync<AnEntity>(entity.Id);
                entity.DependencyAId = Guid.Empty;
                entity.DependencyDId = Guid.Empty;
                await repository.UpdateAsync(entity);
                await uow.FinishAsync();
            }

            projection = await Repository.LoadAsync<AProjection>(entity.Id);
            projection.DependencyAId.Should().BeEmpty();
            projection.DependencyAName.Should().BeNull();
            projection.DependencyBId.Should().BeNull();
            projection.DependencyBName.Should().BeNull();
            projection.DependencyCId.Should().BeEmpty();
            projection.DependencyCName.Should().BeNull();
            projection.DependencyDId.Should().BeNull();
            projection.DependencyDName.Should().BeNull();
        }

        [Fact]
        public async Task GroupUpdateAfterInsert()
        {
            _derivationBuilder.Project<AnEntity>().Into<AGroupProjection>().UsingMapping();
            _derivationBuilder
                .ForDependency<DependencyEntityD>()
                .Update<AGroupProjection>((e, p) => e.AnEntityId == p.Id)
                .Using<GroupProjectionSynchronizer>();

            AnEntity entity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                entity.Name = "toto";
                var dependencyD1 = new DependencyEntityD().WithId();
                dependencyD1.AnEntityId = entity.Id;
                var dependencyD2 = new DependencyEntityD().WithId();
                dependencyD2.AnEntityId = entity.Id;
                await repository.InsertManyAsync(dependencyD1, dependencyD2, entity);
                await uow.FinishAsync();
            }

            var projection = await Repository.LoadAsync<AGroupProjection>(entity.Id);
            projection.Name.Should().Be("toto");
            projection.DependencyDCount.Should().Be(2);
        }

        [Fact]
        public async Task GroupUpdateAfterUpdate()
        {
            _derivationBuilder.Project<AnEntity>().Into<AGroupProjection>().UsingMapping();
            _derivationBuilder
                .ForDependency<DependencyEntityD>()
                .Update<AGroupProjection>((e, p) => e.AnEntityId == p.Id)
                .Using<GroupProjectionSynchronizer>();

            AnEntity entity1 = null;
            AnEntity entity2 = null;
            DependencyEntityD dependencyD1 = null;
            DependencyEntityD dependencyD2 = null;
            DependencyEntityD dependencyD3 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity1 = new AnEntity().WithId();
                entity2 = new AnEntity().WithId();
                dependencyD1 = new DependencyEntityD().WithId();
                dependencyD1.AnEntityId = entity1.Id;
                dependencyD2 = new DependencyEntityD().WithId();
                dependencyD2.AnEntityId = entity1.Id;
                dependencyD3 = new DependencyEntityD().WithId();
                dependencyD3.AnEntityId = entity2.Id;
                await repository.InsertManyAsync(entity1, entity2, dependencyD1, dependencyD2, dependencyD3);
                await uow.FinishAsync();
            }

            var projection1 = await Repository.LoadAsync<AGroupProjection>(entity1.Id);
            projection1.DependencyDCount.Should().Be(2);
            var projection2 = await Repository.LoadAsync<AGroupProjection>(entity2.Id);
            projection2.DependencyDCount.Should().Be(1);

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependencyD1 = await repository.LoadAsync<DependencyEntityD>(dependencyD1.Id);
                dependencyD1.AnEntityId = entity2.Id;
                await repository.UpdateAsync(dependencyD1);
                await uow.FinishAsync();
            }

            projection1 = await Repository.LoadAsync<AGroupProjection>(entity1.Id);
            projection2 = await Repository.LoadAsync<AGroupProjection>(entity2.Id);
            projection1.DependencyDCount.Should().Be(1);
            projection2.DependencyDCount.Should().Be(2);
        }

        [Fact]
        public async Task GroupUpdateAfterDelete()
        {
            _derivationBuilder.Project<AnEntity>().Into<AGroupProjection>().UsingMapping();
            _derivationBuilder
                .ForDependency<DependencyEntityD>()
                .Update<AGroupProjection>((e, p) => e.AnEntityId == p.Id)
                .Using<GroupProjectionSynchronizer>();

            AnEntity entity = null;
            DependencyEntityD dependencyD1 = null;
            DependencyEntityD dependencyD2 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                entity.Name = "toto";
                dependencyD1 = new DependencyEntityD().WithId();
                dependencyD1.AnEntityId = entity.Id;
                dependencyD2 = new DependencyEntityD().WithId();
                dependencyD2.AnEntityId = entity.Id;
                await repository.InsertManyAsync(entity, dependencyD1, dependencyD2);
                await uow.FinishAsync();
            }

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependencyD1 = await repository.LoadAsync<DependencyEntityD>(dependencyD1.Id);
                await repository.DeleteAsync(dependencyD1);
                await uow.FinishAsync();
            }

            var projection = await Repository.LoadAsync<AGroupProjection>(entity.Id);
            projection.Name.Should().Be("toto");
            projection.DependencyDCount.Should().Be(1);
        }

        [Theory]
        [InlineData(false)]
        [InlineData(true)]
        public async Task ProjectEntityIntoManyProjectionsWhenInsertEntity(bool reverse)
        {
            _derivationBuilder.Project<AOneEntity, AManyEntity>(e => e.ManyIds)
                .IntoMany<AOneManyProjection>(p => p.OneId, p => p.ManyId)
                .UsingMapping();

            AOneEntity oneEntity = null;
            AManyEntity manyEntity1 = null;
            AManyEntity manyEntity2 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                manyEntity1 = new AManyEntity().WithId();
                manyEntity1.Name = "titi";
                manyEntity2 = new AManyEntity().WithId();
                manyEntity2.Name = "tutu";
                oneEntity = new AOneEntity().WithId();
                oneEntity.Name = "toto";
                oneEntity.ManyIds = new Guid[] { manyEntity1.Id, manyEntity2.Id };

                var entitiesToInsert = new IEntity[] { oneEntity, manyEntity1, manyEntity2 };
                await repository.InsertManyAsync(entitiesToInsert.Reverse(reverse));
                await uow.FinishAsync();
            }

            var projections = await Repository
                .Query<AOneManyProjection>()
                .Where(x => x.OneId == oneEntity.Id)
                .ToListAsync();
            projections.Should().HaveCount(2);
            var projection1 = projections.Single(x => x.ManyId == manyEntity1.Id);
            projection1.Id.Should().NotBe(oneEntity.Id).And.NotBe(manyEntity1.Id);
            projection1.OneId.Should().Be(oneEntity.Id);
            projection1.OneName.Should().Be("toto");
            projection1.ManyId.Should().Be(manyEntity1.Id);
            projection1.ManyName.Should().Be("titi");
            var projection2 = projections.Single(x => x.ManyId == manyEntity2.Id);
            projection2.Id.Should().NotBe(oneEntity.Id).And.NotBe(manyEntity2.Id);
            projection2.OneId.Should().Be(oneEntity.Id);
            projection2.OneName.Should().Be("toto");
            projection2.ManyId.Should().Be(manyEntity2.Id);
            projection2.ManyName.Should().Be("tutu");
        }

        [Fact]
        public async Task MixSimpleAndManyProjections1()
        {
            _derivationBuilder.Project<AOneEntity, AManyEntity>(e => e.ManyIds)
                .IntoMany<AOneManyProjection>(p => p.OneId, p => p.ManyId)
                .UsingMapping();
            _derivationBuilder.Project<AOneEntity>().Into<AOneManyProjection>().UsingMapping();

            AOneEntity oneEntity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                oneEntity = new AOneEntity().WithId();
                oneEntity.Name = "one name";
                oneEntity.ManyIds = new Guid[] { };
                await repository.InsertManyAsync(oneEntity);
                await uow.FinishAsync();
            }

            var projections = await Repository
                .Query<AOneManyProjection>()
                .ToListAsync();
            projections.Should().HaveCount(1);
            projections[0].Id.Should().Be(oneEntity.Id);
            projections[0].OneId.Should().Be(oneEntity.Id);
            projections[0].OneName.Should().Be("one name");
            projections[0].ManyId.Should().BeNull();
            projections[0].ManyName.Should().BeNull();

            AManyEntity manyEntity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                manyEntity = new AManyEntity().WithId();
                manyEntity.Name = "many name";
                oneEntity = await repository.LoadAsync<AOneEntity>(oneEntity.Id);
                oneEntity.ManyIds = new Guid[] { manyEntity.Id };
                await repository.InsertAsync(manyEntity);
                await repository.UpdateAsync(oneEntity);
                await uow.FinishAsync();
            }

            projections = await Repository
               .Query<AOneManyProjection>()
               .ToListAsync();
            projections.Should().HaveCount(2);
            var projection1 = projections.Single(x => x.Id == oneEntity.Id);
            projection1.Id.Should().Be(oneEntity.Id);
            projection1.OneId.Should().Be(oneEntity.Id);
            projection1.OneName.Should().Be("one name");
            projection1.ManyId.Should().BeNull();
            projection1.ManyName.Should().BeNull();
            var projection2 = projections.Single(x => x.ManyId == manyEntity.Id);
            projection2.Id.Should().NotBe(oneEntity.Id);
            projection2.OneId.Should().Be(oneEntity.Id);
            projection2.OneName.Should().Be("one name");
            projection2.ManyId.Should().Be(manyEntity.Id);
            projection2.ManyName.Should().Be("many name");
        }

        [Fact]
        public async Task MixSimpleAndManyProjections2()
        {
            _derivationBuilder.Project<AOneEntity, AManyEntity>(e => e.ManyIds)
                 .IntoMany<AOneManyProjection>(p => p.OneId, p => p.ManyId2)
                 .UsingMapping();
            _derivationBuilder.Project<AOneEntity>().Into<AOneManyProjection>().UsingMapping();

            AOneEntity oneEntity = null;
            AManyEntity manyEntity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                manyEntity = new AManyEntity().WithId();
                manyEntity.Name = "many name";
                oneEntity = new AOneEntity().WithId();
                oneEntity.Name = "one name";
                oneEntity.ManyIds = new Guid[] { };
                await repository.InsertManyAsync(oneEntity, manyEntity);
                await uow.FinishAsync();
            }

            var projections = await Repository
                .Query<AOneManyProjection>()
                .ToListAsync();
            projections.Should().HaveCount(1);
            projections[0].Id.Should().Be(oneEntity.Id);
            projections[0].OneId.Should().Be(oneEntity.Id);
            projections[0].OneName.Should().Be("one name");
            projections[0].ManyId2.Should().BeEmpty();
            projections[0].ManyName.Should().BeNull();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                oneEntity = await repository.LoadAsync<AOneEntity>(oneEntity.Id);
                oneEntity.Name = "one name 2";
                oneEntity.ManyIds = new Guid[] { manyEntity.Id };
                await repository.UpdateAsync(oneEntity);
                await uow.FinishAsync();
            }

            projections = await Repository
               .Query<AOneManyProjection>()
               .ToListAsync();
            projections.Should().HaveCount(2);
            var projection1 = projections.Single(x => x.Id == oneEntity.Id);
            projection1.Id.Should().Be(oneEntity.Id);
            projection1.OneId.Should().Be(oneEntity.Id);
            projection1.OneName.Should().Be("one name 2");
            projection1.ManyId2.Should().BeEmpty();
            projection1.ManyName.Should().BeNull();
            var projection2 = projections.Single(x => x.ManyId == manyEntity.Id);
            projection2.Id.Should().NotBe(oneEntity.Id);
            projection2.OneId.Should().Be(oneEntity.Id);
            projection2.OneName.Should().Be("one name 2");
            projection2.ManyId2.Should().Be(manyEntity.Id);
            projection2.ManyName.Should().Be("many name");
        }

        [Fact]
        public async Task MixSimpleAndManyProjections3()
        {
            _derivationBuilder.Project<AOneEntity, AManyEntity>(e => e.ManyIds)
                .IntoMany<AOneManyProjection>(p => p.OneId, p => p.ManyId)
                .UsingMapping();
            _derivationBuilder.Project<AOneEntity>().Into<AOneManyProjection>().UsingMapping();

            AOneEntity oneEntity = null;
            AManyEntity manyEntity1 = null;
            AManyEntity manyEntity2 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                manyEntity1 = new AManyEntity().WithId();
                manyEntity1.Name = "many name 1";
                manyEntity2 = new AManyEntity().WithId();
                manyEntity2.Name = "many name 2";
                oneEntity = new AOneEntity().WithId();
                oneEntity.Name = "one name";
                oneEntity.ManyIds = new Guid[] { manyEntity1.Id, manyEntity2.Id };
                await repository.InsertManyAsync(oneEntity, manyEntity1, manyEntity2);
                await uow.FinishAsync();
            }

            var projections = await Repository
               .Query<AOneManyProjection>()
               .ToListAsync();
            projections.Should().HaveCount(3);
            var projection1 = projections.Single(x => x.Id == oneEntity.Id);
            projection1.Id.Should().Be(oneEntity.Id);
            projection1.OneId.Should().Be(oneEntity.Id);
            projection1.OneName.Should().Be("one name");
            projection1.ManyId.Should().BeNull();
            projection1.ManyName.Should().BeNull();
            var projection2 = projections.Single(x => x.ManyId == manyEntity1.Id);
            projection2.Id.Should().NotBe(oneEntity.Id);
            projection2.OneId.Should().Be(oneEntity.Id);
            projection2.OneName.Should().Be("one name");
            projection2.ManyId.Should().Be(manyEntity1.Id);
            projection2.ManyName.Should().Be("many name 1");
            var projection3 = projections.Single(x => x.ManyId == manyEntity2.Id);
            projection3.Id.Should().NotBe(oneEntity.Id);
            projection3.OneId.Should().Be(oneEntity.Id);
            projection3.OneName.Should().Be("one name");
            projection3.ManyId.Should().Be(manyEntity2.Id);
            projection3.ManyName.Should().Be("many name 2");
        }

        [Fact]
        public async Task ProjectEntityIntoManyProjectionsWhenAddEntity()
        {
            _derivationBuilder.Project<AOneEntity, AManyEntity>(e => e.ManyIds)
                .IntoMany<AOneManyProjection>(p => p.OneId, p => p.ManyId)
                .UsingMapping();

            AOneEntity oneEntity = null;
            AManyEntity manyEntity1 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                manyEntity1 = new AManyEntity().WithId();
                manyEntity1.Name = "titi";
                oneEntity = new AOneEntity().WithId();
                oneEntity.Name = "toto";
                oneEntity.ManyIds = new Guid[] { manyEntity1.Id };
                await repository.InsertManyAsync(manyEntity1, oneEntity);
                await uow.FinishAsync();
            }

            var previousProjection = await Repository
                .Query<AOneManyProjection>()
                .SingleAsync();

            AManyEntity manyEntity2 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                manyEntity2 = new AManyEntity().WithId();
                manyEntity2.Name = "tutu";
                oneEntity = await repository.LoadAsync<AOneEntity>(oneEntity.Id);
                oneEntity.ManyIds = oneEntity.ManyIds.Append(manyEntity2.Id);
                await repository.InsertAsync(manyEntity2);
                await repository.UpdateAsync(oneEntity);
                await uow.FinishAsync();
            }

            var projections = await Repository
                .Query<AOneManyProjection>()
                .ToListAsync();
            projections.Should().HaveCount(2);
            var projection1 = projections.Single(x => x.ManyId == manyEntity1.Id);
            projection1.Id.Should().Be(previousProjection.Id)
                .And.NotBe(oneEntity.Id)
                .And.NotBe(manyEntity1.Id);
            projection1.OneId.Should().Be(oneEntity.Id);
            projection1.OneName.Should().Be("toto");
            projection1.ManyId.Should().Be(manyEntity1.Id);
            projection1.ManyName.Should().Be("titi");
            var projection2 = projections.Single(x => x.ManyId == manyEntity2.Id);
            projection2.Id.Should().NotBe(oneEntity.Id).And.NotBe(manyEntity2.Id);
            projection2.OneId.Should().Be(oneEntity.Id);
            projection2.OneName.Should().Be("toto");
            projection2.ManyId.Should().Be(manyEntity2.Id);
            projection2.ManyName.Should().Be("tutu");
        }

        [Fact]
        public async Task ProjectEntityIntoManyProjectionsWhenRemoveEntity()
        {
            _derivationBuilder.Project<AOneEntity, AManyEntity>(e => e.ManyIds)
                .IntoMany<AOneManyProjection>(p => p.OneId, p => p.ManyId2)
                .UsingMapping();

            AOneEntity oneEntity = null;
            AManyEntity manyEntity1 = null;
            AManyEntity manyEntity2 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                manyEntity1 = new AManyEntity().WithId();
                manyEntity1.Name = "titi";
                manyEntity2 = new AManyEntity().WithId();
                manyEntity2.Name = "tutu";
                oneEntity = new AOneEntity().WithId();
                oneEntity.Name = "toto";
                oneEntity.ManyIds = new Guid[] { manyEntity1.Id, manyEntity2.Id };
                await repository.InsertManyAsync(manyEntity1, manyEntity2, oneEntity);
                await uow.FinishAsync();
            }

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                oneEntity = await repository.LoadAsync<AOneEntity>(oneEntity.Id);
                oneEntity.ManyIds = new Guid[] { manyEntity2.Id };
                await repository.UpdateAsync(oneEntity);
                await uow.FinishAsync();
            }

            var projections = await Repository
                .Query<AOneManyProjection>()
                .ToListAsync();
            var projection = projections.Should().ContainSingle().Which;
            projection.Id.Should().NotBe(oneEntity.Id).And.NotBe(manyEntity1.Id);
            projection.OneId.Should().Be(oneEntity.Id);
            projection.OneName.Should().Be("toto");
            projection.ManyId.Should().Be(manyEntity2.Id);
            projection.ManyName.Should().Be("tutu");
        }

        [Fact]
        public async Task DeleteManyProjectionsWhenDeleteOneEntity()
        {
            _derivationBuilder.Project<AOneEntity, AManyEntity>(e => e.ManyIds)
                .IntoMany<AOneManyProjection>(p => p.OneId, p => p.ManyId)
                .UsingMapping();

            AOneEntity oneEntity = null;
            AManyEntity manyEntity1 = null;
            AManyEntity manyEntity2 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                manyEntity1 = new AManyEntity().WithId();
                manyEntity1.Name = "titi";
                manyEntity2 = new AManyEntity().WithId();
                manyEntity2.Name = "tutu";
                oneEntity = new AOneEntity().WithId();
                oneEntity.Name = "toto";
                oneEntity.ManyIds = new Guid[] { manyEntity1.Id, manyEntity2.Id };
                await repository.InsertManyAsync(manyEntity1, manyEntity2, oneEntity);
                await uow.FinishAsync();
            }

            var projections = await Repository
                .Query<AOneManyProjection>()
                .ToListAsync();
            projections.Should().HaveCount(2);

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                oneEntity = await repository.LoadAsync<AOneEntity>(oneEntity.Id);
                await repository.DeleteAsync(oneEntity);
                await uow.FinishAsync();
            }

            projections = await Repository
                .Query<AOneManyProjection>()
                .Where(x => x.OneId == oneEntity.Id)
                .ToListAsync();
            projections.Should().BeEmpty();
        }

        [Fact]
        public async Task CascadeDerivationOneMany()
        {
            _derivationBuilder.Project<AOneEntity>().Into<AOneProjection>().UsingMapping();
            _derivationBuilder.Project<AOneEntity, AManyEntity>(e => e.ManyIds)
               .IntoMany<AOneManyProjection>(p => p.OneId, p => p.ManyId)
               .UsingMapping("many");
            _derivationBuilder
                .ForDependency<AOneManyProjection>()
                .Update<AOneProjection>((e, p) => e.OneId == p.Id)
                .Using<AOneProjectionSynchronizer>();

            AOneEntity oneEntity = null;
            AManyEntity manyEntity1 = null;
            AManyEntity manyEntity2 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                manyEntity1 = new AManyEntity().WithId();
                manyEntity1.Name = "titi";
                manyEntity2 = new AManyEntity().WithId();
                manyEntity2.Name = "tutu";
                oneEntity = new AOneEntity().WithId();
                oneEntity.Name = "toto";
                oneEntity.ManyIds = new Guid[] { manyEntity1.Id, manyEntity2.Id };
                await repository.InsertManyAsync(manyEntity1, manyEntity2, oneEntity);
                await uow.FinishAsync();
            }

            var projection = await Repository.LoadAsync<AOneProjection>(oneEntity.Id);
            projection.Name.Should().Be(oneEntity.Name);
            projection.ManyCount.Should().Be(2);
            _cache.Get<int>(AOneProjectionSynchronizer.CallCount).Should().Be(3);
        }

        [Fact]
        public async Task CascadeDerivationManyMany()
        {
            _derivationBuilder.Project<AManyEntity>().Into<AManyProjection>().UsingMapping();
            _derivationBuilder.Project<AOneEntity, AManyEntity>(e => e.ManyIds)
               .IntoMany<AOneManyProjection>(p => p.OneId, p => p.ManyId)
               .UsingMapping("many");
            _derivationBuilder
                .ForDependency<AOneManyProjection>()
                .Update<AManyProjection>((e, p) => e.ManyId == p.Id)
                .Using<AManyProjectionSynchronizer>();

            AOneEntity oneEntity1 = null;
            AManyEntity manyEntity1 = null;
            AManyEntity manyEntity2 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                manyEntity1 = new AManyEntity().WithId();
                manyEntity2 = new AManyEntity().WithId();
                oneEntity1 = new AOneEntity().WithId();
                oneEntity1.ManyIds = new Guid[] { manyEntity1.Id, manyEntity2.Id };
                await repository.InsertManyAsync(manyEntity1, manyEntity2, oneEntity1);
                await uow.FinishAsync();
            }

            Repository.LoadAsync<AManyProjection>(manyEntity1.Id).Result.OneCount.Should().Be(1);
            Repository.LoadAsync<AManyProjection>(manyEntity2.Id).Result.OneCount.Should().Be(1);

            AOneEntity oneEntity2 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                oneEntity2 = new AOneEntity().WithId();
                oneEntity2.ManyIds = new Guid[] { manyEntity1.Id };
                await repository.InsertManyAsync(oneEntity2);
                await uow.FinishAsync();
            }

            Repository.LoadAsync<AManyProjection>(manyEntity1.Id).Result.OneCount.Should().Be(2);
            Repository.LoadAsync<AManyProjection>(manyEntity2.Id).Result.OneCount.Should().Be(1);
        }

        [Fact]
        public async Task CascadeDerivationProjectIntoManyWhenUpdate()
        {
            _derivationBuilder.Project<AManyEntity>().Into<AManyProjection>().UsingMapping();
            _derivationBuilder.Project<AOneEntity, AManyEntity>(e => e.ManyIds)
              .IntoMany<AOneManyProjection>(p => p.OneId, p => p.ManyId)
              .UsingMapping();

            _derivationBuilder
                .ForDependency<AOneManyProjection>()
                .Update<AManyProjection>((e, p) => e.ManyId == p.Id)
                .Using<AManyProjectionSynchronizer>();

            AOneEntity oneEntity1 = null;
            AOneEntity oneEntity2 = null;
            AManyEntity manyEntity1 = null;
            AManyEntity manyEntity2 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                manyEntity1 = new AManyEntity().WithId();
                manyEntity2 = new AManyEntity().WithId();
                oneEntity1 = new AOneEntity().WithId();
                oneEntity2 = new AOneEntity().WithId();
                oneEntity1.ManyIds = new Guid[] { manyEntity1.Id, manyEntity2.Id };
                oneEntity2.ManyIds = new Guid[] { manyEntity1.Id };
                await repository.InsertManyAsync(manyEntity1, manyEntity2, oneEntity1, oneEntity2);
                await uow.FinishAsync();
            }

            Repository.LoadAsync<AManyProjection>(manyEntity1.Id).Result.OneCount.Should().Be(2);
            Repository.LoadAsync<AManyProjection>(manyEntity2.Id).Result.OneCount.Should().Be(1);

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                oneEntity1 = await repository.LoadAsync<AOneEntity>(oneEntity1.Id);
                oneEntity2 = await repository.LoadAsync<AOneEntity>(oneEntity2.Id);
                oneEntity1.ManyIds = new Guid[] { manyEntity1.Id };
                oneEntity2.ManyIds = new Guid[] { manyEntity2.Id };
                await repository.UpdateManyAsync(oneEntity1, oneEntity2);
                await uow.FinishAsync();
            }

            Repository.LoadAsync<AManyProjection>(manyEntity1.Id).Result.OneCount.Should().Be(1);
            Repository.LoadAsync<AManyProjection>(manyEntity2.Id).Result.OneCount.Should().Be(1);
        }

        [Fact]
        public async Task CascadeDerivationWhenUpdateAssociation()
        {
            _derivationBuilder.Project<AManyEntity>().Into<AManyProjection>().UsingMapping();
            _derivationBuilder
                .ForDependency<AnAssociationEntity>()
                .Update<AManyProjection>((e, p) => e.ManyId == p.Id)
                .Using<AnAssociationSynchronizer>();

            AManyEntity manyEntity1 = null;
            AManyEntity manyEntity2 = null;
            AnAssociationEntity associationEntity1 = null;
            AnAssociationEntity associationEntity2 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var oneEntity1 = new AOneEntity().WithId();
                var oneEntity2 = new AOneEntity().WithId();
                manyEntity1 = new AManyEntity().WithId();
                manyEntity2 = new AManyEntity().WithId();
                associationEntity1 = new AnAssociationEntity().WithId();
                associationEntity2 = new AnAssociationEntity().WithId();
                associationEntity1.OneId = oneEntity1.Id;
                associationEntity1.ManyId = manyEntity1.Id;
                associationEntity2.OneId = oneEntity2.Id;
                associationEntity2.ManyId = manyEntity2.Id;
                await repository.InsertManyAsync(oneEntity1, oneEntity2, manyEntity1, manyEntity2, associationEntity1, associationEntity2);
                await uow.FinishAsync();
            }

            Repository.LoadAsync<AManyProjection>(manyEntity1.Id).Result.OneCount.Should().Be(1);
            Repository.LoadAsync<AManyProjection>(manyEntity2.Id).Result.OneCount.Should().Be(1);

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                associationEntity1 = await repository.LoadAsync<AnAssociationEntity>(associationEntity1.Id);
                associationEntity1.ManyId = manyEntity2.Id;
                await repository.UpdateAsync(associationEntity1);
                await uow.FinishAsync();
            }

            Repository.LoadAsync<AManyProjection>(manyEntity1.Id).Result.OneCount.Should().Be(0);
            Repository.LoadAsync<AManyProjection>(manyEntity2.Id).Result.OneCount.Should().Be(2);
        }

        [Fact]
        public async Task CascadeDerivationWhenDeleteAssociation()
        {
            _derivationBuilder.Project<AManyEntity>().Into<AManyProjection>().UsingMapping();
            _derivationBuilder
                .ForDependency<AnAssociationEntity>()
                .Update<AManyProjection>((e, p) => e.ManyId == p.Id)
                .Using<AnAssociationSynchronizer>();

            AManyEntity manyEntity = null;
            AnAssociationEntity associationEntity1 = null;
            AnAssociationEntity associationEntity2 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var oneEntity1 = new AOneEntity().WithId();
                var oneEntity2 = new AOneEntity().WithId();
                manyEntity = new AManyEntity().WithId();
                associationEntity1 = new AnAssociationEntity().WithId();
                associationEntity2 = new AnAssociationEntity().WithId();
                associationEntity1.OneId = oneEntity1.Id;
                associationEntity1.ManyId = manyEntity.Id;
                associationEntity2.OneId = oneEntity2.Id;
                associationEntity2.ManyId = manyEntity.Id;
                await repository.InsertManyAsync(oneEntity1, oneEntity2, manyEntity, associationEntity1, associationEntity2);
                await uow.FinishAsync();
            }

            Repository.LoadAsync<AManyProjection>(manyEntity.Id).Result.OneCount.Should().Be(2);

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                associationEntity1 = await repository.LoadAsync<AnAssociationEntity>(associationEntity1.Id);
                await repository.DeleteAsync(associationEntity1);
                await uow.FinishAsync();
            }

            Repository.LoadAsync<AManyProjection>(manyEntity.Id).Result.OneCount.Should().Be(1);
        }

        [Fact]
        public async Task CreateProjectionWithAssociationEntityAsDependency()
        {
            _derivationBuilder.Project<AManyEntity>().Into<AManyProjection>().UsingMapping();
            _derivationBuilder
                .ForDependency<AnAssociationEntity>()
                .Update<AManyProjection>((e, p) => e.ManyId == p.Id)
                .UsingMapping();
            _derivationBuilder
                .ForDependency<AOneEntity>()
                .Update<AManyProjection>((e, p) => e.Id == p.OneId)
                .UsingMapping();

            AManyEntity manyEntity = null;
            AOneEntity oneEntity = null;
            AnAssociationEntity associationEntity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                oneEntity = new AOneEntity().WithId();
                manyEntity = new AManyEntity().WithId();
                associationEntity = new AnAssociationEntity().WithId();
                associationEntity.OneId = oneEntity.Id;
                associationEntity.ManyId = manyEntity.Id;
                await repository.InsertManyAsync(oneEntity, manyEntity, associationEntity);
                await uow.FinishAsync();
            }

            var projection = await Repository.LoadAsync<AManyProjection>(manyEntity.Id);
            projection.OneId.Should().Be(oneEntity.Id);
            projection.OneName.Should().Be(oneEntity.Name);
        }

        [Fact]
        public async Task UpdateProjectionWithAssociationEntityAsDependencyWhenInsertDependency()
        {
            _derivationBuilder.Project<AManyEntity>().Into<AManyProjection>().UsingMapping();
            _derivationBuilder
                .ForDependency<AnAssociationEntity>()
                .Update<AManyProjection>((e, p) => e.ManyId == p.Id)
                .UsingMapping();
            _derivationBuilder
                .ForDependency<AOneEntity>()
                .Update<AManyProjection>((e, p) => e.Id == p.OneId)
                .UsingMapping();

            AManyEntity manyEntity = null;
            AOneEntity oneEntity = null;

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                oneEntity = new AOneEntity().WithId();
                manyEntity = new AManyEntity().WithId();
                await repository.InsertManyAsync(oneEntity, manyEntity);
                await uow.FinishAsync();
            }

            var projection = await Repository.LoadAsync<AManyProjection>(manyEntity.Id);
            projection.OneId.Should().BeNull();
            projection.OneName.Should().BeNull();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var associationEntity = new AnAssociationEntity().WithId();
                associationEntity.OneId = oneEntity.Id;
                associationEntity.ManyId = manyEntity.Id;
                await repository.InsertManyAsync(associationEntity);
                await uow.FinishAsync();
            }

            projection = await Repository.LoadAsync<AManyProjection>(manyEntity.Id);
            projection.OneId.Should().Be(oneEntity.Id);
            projection.OneName.Should().Be(oneEntity.Name);
        }

        [Fact]
        public async Task UpdateProjectionWithAssociationEntityAsDependencyWhenDeleteDependency()
        {
            _derivationBuilder.Project<AManyEntity>().Into<AManyProjection>().UsingMapping();
            _derivationBuilder
                .ForDependency<AnAssociationEntity>()
                .Update<AManyProjection>((e, p) => e.ManyId == p.Id)
                .UsingMapping();
            _derivationBuilder
                .ForDependency<AOneEntity>()
                .Update<AManyProjection>((e, p) => e.Id == p.OneId)
                .UsingMapping();

            AManyEntity manyEntity = null;
            AOneEntity oneEntity = null;
            AnAssociationEntity associationEntity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                oneEntity = new AOneEntity().WithId();
                manyEntity = new AManyEntity().WithId();
                associationEntity = new AnAssociationEntity().WithId();
                associationEntity.OneId = oneEntity.Id;
                associationEntity.ManyId = manyEntity.Id;
                await repository.InsertManyAsync(oneEntity, manyEntity, associationEntity);
                await uow.FinishAsync();
            }

            var projection = await Repository.LoadAsync<AManyProjection>(manyEntity.Id);
            projection.OneId.Should().Be(oneEntity.Id);
            projection.OneName.Should().Be(oneEntity.Name);

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                associationEntity = await repository.LoadAsync<AnAssociationEntity>(associationEntity.Id);
                await repository.DeleteAsync(associationEntity);
                await uow.FinishAsync();
            }

            projection = await Repository.LoadAsync<AManyProjection>(manyEntity.Id);
            projection.OneId.Should().BeNull();
            projection.OneName.Should().BeNull();
        }

        [Fact]
        public async Task UpdateProjectionWithAssociationEntityAsDependencyWhenUpdateDependency()
        {
            _derivationBuilder.Project<AManyEntity>().Into<AManyProjection>().UsingMapping();
            _derivationBuilder
                .ForDependency<AnAssociationEntity>()
                .Update<AManyProjection>((e, p) => e.ManyId == p.Id)
                .UsingMapping();
            _derivationBuilder
                .ForDependency<AOneEntity>()
                .Update<AManyProjection>((e, p) => e.Id == p.OneId)
                .UsingMapping();

            AManyEntity manyEntity1 = null;
            AManyEntity manyEntity2 = null;
            AOneEntity oneEntity = null;
            AnAssociationEntity associationEntity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                oneEntity = new AOneEntity().WithId();
                manyEntity1 = new AManyEntity().WithId();
                manyEntity2 = new AManyEntity().WithId();
                associationEntity = new AnAssociationEntity().WithId();
                associationEntity.OneId = oneEntity.Id;
                associationEntity.ManyId = manyEntity1.Id;
                await repository.InsertManyAsync(oneEntity, manyEntity1, manyEntity2, associationEntity);
                await uow.FinishAsync();
            }

            var projection1 = await Repository.LoadAsync<AManyProjection>(manyEntity1.Id);
            projection1.OneId.Should().Be(oneEntity.Id);
            projection1.OneName.Should().Be(oneEntity.Name);
            var projection2 = await Repository.LoadAsync<AManyProjection>(manyEntity2.Id);
            projection2.OneId.Should().BeNull();
            projection2.OneName.Should().BeNull();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                associationEntity = await repository.LoadAsync<AnAssociationEntity>(associationEntity.Id);
                associationEntity.ManyId = manyEntity2.Id;
                await repository.UpdateAsync(associationEntity);
                await uow.FinishAsync();
            }

            projection1 = await Repository.LoadAsync<AManyProjection>(manyEntity1.Id);
            projection1.OneId.Should().BeNull();
            projection1.OneName.Should().BeNull();
            projection2 = await Repository.LoadAsync<AManyProjection>(manyEntity2.Id);
            projection2.OneId.Should().Be(oneEntity.Id);
            projection2.OneName.Should().Be(oneEntity.Name);

        }

        [Fact]
        public async Task CanUseResetActionForDependency()
        {
            _derivationBuilder.Project<AParentChildEntity>().Into<AParentChildProjection>().UsingMapping();
            _derivationBuilder
                .ForDependency<AParentChildEntity>()
                .Update<AParentChildProjection>((e, p) => e.Id == p.ParentId)
                .UsingMapping("parent");

            AParentChildEntity childEntity = null;
            AParentChildEntity parentEntity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                childEntity = new AParentChildEntity().WithId();
                parentEntity = new AParentChildEntity().WithId();
                childEntity.Depth = 1;
                childEntity.Name = "toto";
                childEntity.ParentId = parentEntity.Id;
                parentEntity.Depth = 0;
                parentEntity.Name = "titi";
                await repository.InsertManyAsync(childEntity, parentEntity);
                await uow.FinishAsync();
            }

            var projection1 = await Repository.LoadAsync<AParentChildProjection>(childEntity.Id);
            projection1.Depth.Should().Be(1);
            projection1.Name.Should().Be("toto");
            projection1.ParentId.Should().Be(parentEntity.Id);
            projection1.ParentDepth.Should().Be(0);
            projection1.ParentName.Should().Be("titi");
            var projection2 = await Repository.LoadAsync<AParentChildProjection>(parentEntity.Id);
            projection2.Depth.Should().Be(0);
            projection2.Name.Should().Be("titi");
            projection2.ParentId.Should().BeNull();
            projection2.ParentDepth.Should().BeNull();
            projection2.ParentName.Should().BeNull();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                childEntity = await repository.LoadAsync<AParentChildEntity>(childEntity.Id);
                childEntity.ParentId = null;
                await repository.UpdateAsync(childEntity);
                await uow.FinishAsync();
            }

            projection1 = await Repository.LoadAsync<AParentChildProjection>(childEntity.Id);
            projection1.Depth.Should().Be(1);
            projection1.Name.Should().Be("toto");
            projection1.ParentId.Should().BeNull();
            projection1.ParentDepth.Should().BeNull();
            projection1.ParentName.Should().BeNull();
        }

        [Fact]
        public async Task CanHaveRecursiveDependency()
        {
            _derivationBuilder.Project<AParentChildEntity>().Into<AnotherParentChildProjection>().UsingMapping();
            _derivationBuilder
                .ForDependency<AParentChildEntity>()
                .Update<AnotherParentChildProjection>((e, p) => e.Id == p.ParentId)
                .UsingMapping("parent");
            _derivationBuilder
                .ForDependency<AParentChildEntity>()
                .Update<AnotherParentChildProjection>((e, p) => e.Id == p.RootId)
                .UsingMapping("root");

            AParentChildEntity childEntity1 = null;
            AParentChildEntity childEntity2 = null;
            AParentChildEntity rootEntity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                childEntity1 = new AParentChildEntity().WithId();
                childEntity2 = new AParentChildEntity().WithId();
                rootEntity = new AParentChildEntity().WithId();
                childEntity2.Depth = 2;
                childEntity2.Name = "tutu";
                childEntity2.ParentId = childEntity1.Id;
                childEntity1.Depth = 1;
                childEntity1.Name = "toto";
                childEntity1.ParentId = rootEntity.Id;
                rootEntity.Depth = 0;
                rootEntity.Name = "titi";
                await repository.InsertManyAsync(rootEntity, childEntity1, childEntity2);
                await uow.FinishAsync();
            }

            var rootProjection = await Repository.LoadAsync<AnotherParentChildProjection>(rootEntity.Id);
            rootProjection.Depth.Should().Be(0);
            rootProjection.Name.Should().Be("titi");
            rootProjection.ParentId.Should().BeNull();
            rootProjection.ParentDepth.Should().BeNull();
            rootProjection.ParentName.Should().BeNull();
            var projection1 = await Repository.LoadAsync<AnotherParentChildProjection>(childEntity1.Id);
            projection1.Depth.Should().Be(1);
            projection1.Name.Should().Be("toto");
            projection1.ParentId.Should().Be(rootEntity.Id);
            projection1.ParentDepth.Should().Be(0);
            projection1.ParentName.Should().Be("titi");
            var projection2 = await Repository.LoadAsync<AnotherParentChildProjection>(childEntity2.Id);
            projection2.Depth.Should().Be(2);
            projection2.Name.Should().Be("tutu");
            projection2.ParentId.Should().Be(childEntity1.Id);
            projection2.ParentDepth.Should().Be(1);
            projection2.ParentName.Should().Be("toto");
        }

        [Fact]
        public async Task CallDependencyMappingsEvenIfProjectionCreatedWithCustomSynchronizer()
        {
            _derivationBuilder.WhenModify<AnEntity>().ForProjection<AProjectionWithCustomConstruction>((e, p) => e.Id == p.Id).Use<ACustomSynchronizer>();
            _derivationBuilder
                .ForDependency<AnEntity>()
                .Update<AProjectionWithCustomConstruction>((e, p) => e.Id == p.EntityId)
                .UsingMapping();
            _derivationBuilder
               .ForDependency<DependencyEntityA>()
               .Update<AProjectionWithCustomConstruction>((e, p) => e.Id == p.DepAId)
               .UsingMapping();

            AnEntity entity = null;
            DependencyEntityA dependencyEntity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependencyEntity = new DependencyEntityA().WithId();
                dependencyEntity.Name = "depA";
                await repository.InsertAsync(dependencyEntity);
                await uow.FinishAsync();
            }
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                entity.Name = "toto";
                entity.DependencyAId = dependencyEntity.Id;
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            var projection = await Repository
                .Query<AProjectionWithCustomConstruction>()
                .Where(x => x.EntityId == entity.Id)
                .SingleAsync();

            projection.EntityId.Should().Be(entity.Id);
            projection.EntityName.Should().Be("toto");
            projection.DepAId.Should().Be(dependencyEntity.Id);
            projection.DepAName.Should().Be("depA");
        }

        [Fact]
        public async Task CanHaveDiffrentDependencyTypesForSameProjectionProperty()
        {
            _derivationBuilder.Project<AnEntityWithPolymorphicDependency>().Into<AProjectionWithPolymorphicDependency>().UsingMapping();
            _derivationBuilder
                .ForDependency<DependencyEntityA>()
                .Update<AProjectionWithPolymorphicDependency>((e, p) => e.Id == p.DependencyId)
                .UsingMapping();
            _derivationBuilder
               .ForDependency<DependencyEntityB>()
               .Update<AProjectionWithPolymorphicDependency>((e, p) => e.Id == p.DependencyId)
               .UsingMapping();

            DependencyEntityA dependencyEntityA = null;
            DependencyEntityB dependencyEntityB = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependencyEntityA = new DependencyEntityA().WithId();
                dependencyEntityA.Name = "depA";
                dependencyEntityA.Town = "Bordeaux";
                dependencyEntityB = new DependencyEntityB().WithId();
                dependencyEntityB.Name = "depB";
                await repository.InsertManyAsync(dependencyEntityA, dependencyEntityB);
                await uow.FinishAsync();
            }

            AnEntityWithPolymorphicDependency entity1 = null;
            AnEntityWithPolymorphicDependency entity2 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity1 = new AnEntityWithPolymorphicDependency().WithId();
                entity1.Name = "toto1";
                entity1.DependencyId = dependencyEntityA.Id;
                entity2 = new AnEntityWithPolymorphicDependency().WithId();
                entity2.Name = "toto2";
                entity2.DependencyId = dependencyEntityB.Id;
                await repository.InsertManyAsync(entity1, entity2);
                await uow.FinishAsync();
            }

            var projection1 = await Repository.LoadAsync<AProjectionWithPolymorphicDependency>(entity1.Id);
            projection1.Name.Should().Be("toto1");
            projection1.Town.Should().Be("Bordeaux");
            projection1.DependencyId.Should().Be(dependencyEntityA.Id);
            projection1.DependencyName.Should().Be("depA");
            var projection2 = await Repository.LoadAsync<AProjectionWithPolymorphicDependency>(entity2.Id);
            projection2.Name.Should().Be("toto2");
            projection2.Town.Should().BeNull();
            projection2.DependencyId.Should().Be(dependencyEntityB.Id);
            projection2.DependencyName.Should().Be("depB");

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var entity = await repository.LoadAsync<AnEntityWithPolymorphicDependency>(entity1.Id);
                entity.DependencyId = dependencyEntityB.Id;
                await repository.UpdateAsync(entity);
                await uow.FinishAsync();
            }

            projection1 = await Repository.LoadAsync<AProjectionWithPolymorphicDependency>(entity1.Id);
            projection1.Name.Should().Be("toto1");
            projection1.Town.Should().BeNull();
            projection1.DependencyId.Should().Be(dependencyEntityB.Id);
            projection1.DependencyName.Should().Be("depB");

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var entity = await repository.LoadAsync<AnEntityWithPolymorphicDependency>(entity1.Id);
                entity.Name = "titi";
                entity.DependencyId = null;
                await repository.UpdateAsync(entity);
                await uow.FinishAsync();
            }

            projection1 = await Repository.LoadAsync<AProjectionWithPolymorphicDependency>(entity1.Id);
            projection1.Name.Should().Be("titi");
            projection1.Town.Should().BeNull();
            projection1.DependencyId.Should().BeNull();
            projection1.DependencyName.Should().BeNull();
        }

        [Fact]
        public async Task ProjectDifferentEntitiesIntoSameProjection()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();
            _derivationBuilder.Project<AnotherEntity>().Into<AProjection>().UsingMapping();

            AnEntity entity1 = new AnEntity();
            AnotherEntity entity2 = new AnotherEntity();
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity1 = new AnEntity().WithId();
                entity1.Name = "entity1";
                entity2 = new AnotherEntity().WithId();
                entity2.Name = "entity2";
                await repository.InsertManyAsync(entity1, entity2);
                await uow.FinishAsync();
            }

            var projections = await Repository
                .Query<AProjection>()
                .OrderBy(x => x.EntityName)
                .ToArrayAsync();
            projections.Should().HaveCount(2);
            projections[0].Id.Should().Be(entity1.Id);
            projections[0].EntityName.Should().Be("entity1");
            projections[1].Id.Should().Be(entity2.Id);
            projections[1].EntityName.Should().Be("entity2");
        }


        [Fact]
        public async Task CanMixMappingAndSynchronizer()
        {
            _derivationBuilder.Project<AnEntityWithParents>().Into<AProjectionWithParents>().UsingMapping();
            _derivationBuilder
                .ForDependency<AParent>()
                .Update<AProjectionWithParents>((e, p) => e.Id == p.GreatestParentId)
                .UsingMapping();
            _derivationBuilder
               .ForDependency<AParent>()
               .Update<AProjectionWithParents>((e, p) => e.FamilyId == p.FamilyId)
               .Using<GreatestParentSynchonizer>();

            AnEntityWithParents child = new AnEntityWithParents();
            AParent parent1 = new AParent();
            AParent parent2 = new AParent();
            Guid familyId = Guid.NewGuid();
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                parent1 = new AParent().WithId();
                parent1.Height = 165;
                parent1.Name = "Roberta";
                parent1.FamilyId = familyId;
                parent2 = new AParent().WithId();
                parent2.Height = 183;
                parent2.Name = "Patrick";
                parent2.FamilyId = familyId;
                child = new AnEntityWithParents().WithId();
                child.Name = "Toto";
                child.FamilyId = familyId;
                await repository.InsertManyAsync(child, parent1, parent2);
                await uow.FinishAsync();
            }

            var projections = await Repository
                .Query<AProjectionWithParents>()
                .ToArrayAsync();
            projections.Should().HaveCount(1);
            projections[0].Id.Should().Be(child.Id);
            projections[0].FamilyId.Should().Be(familyId);
            projections[0].GreatestParentId.Should().Be(parent2.Id);
            projections[0].GreatestParentName.Should().Be("Patrick");
            projections[0].Name.Should().Be("Toto");
        }

        [Fact]
        public async Task CanFlattenHierarchy()
        {
            _derivationBuilder.Project<EntityType>().Into<ATypedProjection>().UsingMapping();
            _derivationBuilder.Project<EntitySubType>().Into<ATypedProjection>().UsingMapping();
            _derivationBuilder.ForDependency<EntityType>().Update<ATypedProjection>((e, p) => e.Id == p.TypeId).UsingMapping("type");
            _derivationBuilder.ForDependency<EntitySubType>().Update<ATypedProjection>((e, p) => e.Id == p.SubTypeId).UsingMapping("subType");

            EntityType entityType = null;
            EntitySubType entitySubType = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entityType = new EntityType().WithId();
                entityType.Name = "myType";
                entitySubType = new EntitySubType().WithId();
                entitySubType.TypeId = entityType.Id;
                entitySubType.Name = "mySubType";
                await repository.InsertManyAsync(entityType, entitySubType);
                await uow.FinishAsync();
            }

            var projections = await Repository
                .Query<ATypedProjection>()
                .OrderBy(x => x.SubTypeName)
                .ToArrayAsync();

            projections.Should().HaveCount(2);
            projections[0].Id.Should().Be(entityType.Id);
            projections[0].SubTypeId.Should().BeNull();
            projections[0].SubTypeName.Should().BeNull();
            projections[0].TypeId.Should().Be(entityType.Id);
            projections[0].TypeName.Should().Be("myType");
            projections[1].Id.Should().Be(entitySubType.Id);
            projections[1].SubTypeId.Should().Be(entitySubType.Id);
            projections[1].SubTypeName.Should().Be("mySubType");
            projections[1].TypeId.Should().Be(entityType.Id);
            projections[1].TypeName.Should().Be("myType");
        }

        [Fact]
        public async Task CascadeUpdateWhenUpdateEntity()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();
            _derivationBuilder.ForDependency<ADependencyEntity>().Update<AProjection>((e, p) => e.Id == p.DependencyAId).UsingMapping();
            _derivationBuilder.Project<AProjection>().Into<AnotherProjectionWithDependency>().UsingMapping();

            AnEntity entity = null;
            ADependencyEntity dependency = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependency = new ADependencyEntity().WithId();
                dependency.Name = "";
                entity = new AnEntity().WithId();
                entity.DependencyAId = dependency.Id;
                await repository.InsertManyAsync(entity, dependency);
                await uow.FinishAsync();
            }

            var projection = await Repository
                .Query<AProjection>()
                .Where(x => x.Id == entity.Id)
                .SingleAsync();
            var anotherProjection = await Repository
                .Query<AnotherProjectionWithDependency>()
                .Where(x => x.Id == entity.Id)
                .SingleAsync();

            projection.DependencyAId.Should().Be(dependency.Id);
            projection.DependencyAName.Should().BeEmpty();
            anotherProjection.DependencyId.Should().Be(dependency.Id);
            anotherProjection.DependencyName.Should().BeEmpty();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependency = await repository.LoadAsync<ADependencyEntity>(dependency.Id);
                dependency.Name = "dependency";
                await repository.UpdateAsync(dependency);
                await uow.FinishAsync();
            }

            projection = await Repository.LoadAsync<AProjection>(projection.Id);
            anotherProjection = await Repository.LoadAsync<AnotherProjectionWithDependency>(anotherProjection.Id);

            projection.DependencyAId.Should().Be(dependency.Id);
            projection.DependencyAName.Should().Be("dependency");
            anotherProjection.DependencyId.Should().Be(dependency.Id);
            anotherProjection.DependencyName.Should().Be("dependency");
        }

        [Fact]
        public async Task CascadeUpdateWhenDeleteEntity()
        {
            _derivationBuilder.Project<EntityA>().Into<EntityAProjection>().UsingMapping();
            _derivationBuilder.ForDependency<AssocEntityProjection>().Update<EntityAProjection>((d, p) => d.IdA == p.Id).Using<AggregateSynchronizer>();
            
            _derivationBuilder.Project<AssocEntity>().Into<AssocEntityProjection>().UsingMapping();
            _derivationBuilder.ForDependency<EntityA>().Update<AssocEntityProjection>((d, p) => d.Id == p.IdA).UsingMapping();
            _derivationBuilder.ForDependency<EntityB>().Update<AssocEntityProjection>((d, p) => d.Id == p.IdB).UsingMapping();

            EntityA entityA1;
            EntityA entityA2;
            EntityB entityB1;
            EntityB entityB2;
            AssocEntity assoc1;
            AssocEntity assoc2;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entityA1 = new EntityA().WithId();
                entityA1.Name = "A1";
                entityA2 = new EntityA().WithId();
                entityA2.Name = "A2";
                entityB1 = new EntityB().WithId();
                entityB1.Name = "B1";
                entityB2 = new EntityB().WithId();
                entityB2.Name = "B2";
                assoc1 = new AssocEntity().WithId();
                assoc1.IdA = entityA1.Id;
                assoc1.IdB = entityB1.Id;
                assoc2 = new AssocEntity().WithId();
                assoc2.IdA = entityA2.Id;
                assoc2.IdB = entityB2.Id;
                await repository.InsertManyAsync(entityA1, entityA2, entityB1, entityB2, assoc1, assoc2);
                await uow.FinishAsync();
            }

            var projection1 = await Repository.LoadAsync<EntityAProjection>(entityA1.Id);
            var projection2 = await Repository.LoadAsync<EntityAProjection>(entityA2.Id);

            projection1.Id.Should().Be(entityA1.Id);
            projection1.Name.Should().Be("A1");
            projection1.BNames.Should().BeEquivalentTo("B1");
            projection2.Id.Should().Be(entityA2.Id);
            projection2.Name.Should().Be("A2");
            projection2.BNames.Should().BeEquivalentTo("B2");

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entityB1 = await repository.LoadAsync<EntityB>(entityB1.Id);
                entityB1.Name = "b1";
                await repository.UpdateAsync(entityB1);
                await uow.FinishAsync();
            }

            projection1 = await Repository.LoadAsync<EntityAProjection>(projection1.Id);
            projection2 = await Repository.LoadAsync<EntityAProjection>(projection2.Id);

            projection1.Id.Should().Be(entityA1.Id);
            projection1.Name.Should().Be("A1");
            projection1.BNames.Should().BeEquivalentTo("b1");
            projection2.Id.Should().Be(entityA2.Id);
            projection2.Name.Should().Be("A2");
            projection2.BNames.Should().BeEquivalentTo("B2");
        }

        [Fact]
        public async Task UpdateDependenciesInCascadeWithAssociation_twoDependencyLevels()
        {
            _derivationBuilder.Project<EntityC>().Into<EntityCProjection>().UsingMapping();
            _derivationBuilder.ForDependency<AssocEntityProjection>().Update<EntityCProjection>((d, p) => d.Id == p.IdAssoc).UsingMapping();

            _derivationBuilder.Project<AssocEntity>().Into<AssocEntityProjection>().UsingMapping();
            _derivationBuilder.ForDependency<EntityA>().Update<AssocEntityProjection>((d, p) => d.Id == p.IdA).UsingMapping();
            _derivationBuilder.ForDependency<EntityB>().Update<AssocEntityProjection>((d, p) => d.Id == p.IdB).UsingMapping();

            EntityA entityA1;
            EntityA entityA2;
            EntityB entityB1;
            EntityB entityB2;
            AssocEntity assoc1;
            AssocEntity assoc2;
            EntityC entityC1;
            EntityC entityC2;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entityA1 = new EntityA().WithId();
                entityA1.Name = "A1";
                entityA2 = new EntityA().WithId();
                entityA2.Name = "A2";
                entityB1 = new EntityB().WithId();
                entityB1.Name = "B1";
                entityB2 = new EntityB().WithId();
                entityB2.Name = "B2";
                assoc1 = new AssocEntity().WithId();
                assoc1.IdA = entityA1.Id;
                assoc1.IdB = entityB1.Id;
                assoc2 = new AssocEntity().WithId();
                assoc2.IdA = entityA2.Id;
                assoc2.IdB = entityB2.Id;
                entityC1 = new EntityC().WithId();
                entityC1.Name = "C1";
                entityC1.IdAssoc = assoc1.Id;
                entityC2 = new EntityC().WithId();
                entityC2.Name = "C2";
                entityC2.IdAssoc = assoc2.Id;
                await repository.InsertManyAsync(entityA1, entityA2, entityB1, entityB2, entityC1, entityC2, assoc1, assoc2);
                await uow.FinishAsync();
            }

            var projection1 = await Repository.LoadAsync<EntityCProjection>(entityC1.Id);
            var projection2 = await Repository.LoadAsync<EntityCProjection>(entityC2.Id);

            projection1.Id.Should().Be(entityC1.Id);
            projection1.Name.Should().Be("C1");
            projection1.AName.Should().Be("A1");
            projection1.BName.Should().Be("B1");
            projection2.Id.Should().Be(entityC2.Id);
            projection2.Name.Should().Be("C2");
            projection2.AName.Should().Be("A2");
            projection2.BName.Should().Be("B2");

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entityB1 = await repository.LoadAsync<EntityB>(entityB1.Id);
                await repository.DeleteAsync(entityB1);
                await uow.FinishAsync();
            }

            projection1 = await Repository.LoadAsync<EntityCProjection>(projection1.Id);
            projection2 = await Repository.LoadAsync<EntityCProjection>(projection2.Id);

            projection1.Id.Should().Be(entityC1.Id);
            projection1.Name.Should().Be("C1");
            projection1.AName.Should().Be("A1");
            projection1.BName.Should().BeNull();
            projection2.Id.Should().Be(entityC2.Id);
            projection2.Name.Should().Be("C2");
            projection2.AName.Should().Be("A2");
            projection2.BName.Should().Be("B2");
        }

        [Fact]
        public async Task CanComputeIfIsLastCreated()
        {
            _derivationBuilder.Project<ALastEntity>().Into<ALastProjection>().UsingMapping();
            _derivationBuilder.ForDependency<ALastEntity>().Update<ALastProjection>((d, p) => d.ParentId == p.ParentId).Using<LastSynchronizer>();

            ALastEntity entity1;
            ALastEntity entity2;
            ALastEntity entity3;
            var parentId = Guid.NewGuid();
            var creationDate = DateTime.Now;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity1 = new ALastEntity().WithId();
                entity1.ParentId = parentId;
                entity1.CreatedAt = creationDate;
                entity2 = new ALastEntity().WithId();
                entity2.ParentId = parentId;
                entity2.CreatedAt = creationDate.AddSeconds(1);
                await repository.InsertManyAsync(entity1, entity2);
                await uow.FinishAsync();
            }

            var projection1 = await Repository.LoadAsync<ALastProjection>(entity1.Id);
            var projection2 = await Repository.LoadAsync<ALastProjection>(entity2.Id);
            projection1.IsLast.Should().BeFalse();
            projection2.IsLast.Should().BeTrue();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity3 = new ALastEntity().WithId();
                entity3.ParentId = parentId;
                entity3.CreatedAt = creationDate.AddSeconds(2);
                await repository.InsertAsync(entity3);
                await uow.FinishAsync();
            }

            projection1 = await Repository.LoadAsync<ALastProjection>(projection1.Id);
            projection2 = await Repository.LoadAsync<ALastProjection>(projection2.Id);
            var projection3 = await Repository.LoadAsync<ALastProjection>(entity3.Id);
            projection1.IsLast.Should().BeFalse();
            projection2.IsLast.Should().BeFalse();
            projection3.IsLast.Should().BeTrue();
        }

        #endregion

        public class MyRepository : DecorableRepository, IDerivationRepository, IRepository
        {
            public MyRepository(IBaseRepository baseRepository, IUnitOfWork unitOfWork, IEnumerable<Type> decoratorTypes)
                : base(baseRepository, unitOfWork, decoratorTypes)
            {
            }
        }
    }
}

