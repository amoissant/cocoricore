using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocoriCore.Derivation.Test
{
    public class AManyProjectionSynchronizer : SynchronizerBase<AOneManyProjection, AManyProjection>, IAutoRegister
    {

        public AManyProjectionSynchronizer(DependencyCoupler coupler) 
            : base(coupler)
        {
        }

        public override Task SynchonizeAsync(DependencyInfo info, IEnumerable<AOneManyProjection> dependencies, IEnumerable<AManyProjection> projections)
        {
            var groups = Coupler.GroupDependenciesByProjection(info, dependencies, projections);
            foreach (var group in groups)
            {
                group.projection.OneCount = group.dependencies.Count();
            }
            return Task.CompletedTask;
        }
    }
}