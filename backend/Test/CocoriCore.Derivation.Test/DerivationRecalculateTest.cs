﻿using System;
using Xunit;
using FluentAssertions;
using System.Threading.Tasks;
using Autofac;
using System.Linq;
using CocoriCore.Test;
using Microsoft.Extensions.Caching.Memory;
using CocoriCore.Autofac;
using CocoriCore.TestUtils;
using CocoriCore.Common;
using CocoriCore.Repository;
using System.Collections.Generic;
using CocoriCore.Linq.Async;
using CocoriCore.JsonNet;
using CocoriCore.Collection.Extensions;

namespace CocoriCore.Derivation.Test
{
    public class DerivationRecalculateTest : BusTestBase
    {
        private DerivationBusOptionsBuilder _derivationBuilder;
        private RepositoryFailureOptions _fakeRepositoryConfiguration;
        private IBaseRepository BaseRepository => RootScope.Resolve<IBaseRepository>();

        private SpyFnFExceptionHandler FnFErrorHandler => RootScope.Resolve<SpyFnFExceptionHandler>();

        public DerivationRecalculateTest()
        {
            _derivationBuilder = new DerivationBusOptionsBuilder();
            _derivationBuilder.ConfigureMappings(this.GetType().Assembly);
            _derivationBuilder.EntityAssemblies = new[] { CocoriCore.Test.AssemblyInfo.Assembly, CocoriCore.Derivation.Test.AssemblyInfo.Assembly };
            _unitOfWorkBuilder
                .Call<IRepository>(r => r.FlushAsync())
                .Call<IDerivationBus>(b => b.ApplyRulesAsync())
                .Call<ITransactionHolder>(b => b.CommitAsync())
                .Call<IDerivationBus>(b => b.ApplyDifferedRulesAsync());
            _fakeRepositoryConfiguration = new RepositoryFailureOptions();

            ContainerBuilder.RegisterInstance(new MemoryCache(new MemoryCacheOptions())).As<IMemoryCache>();
            ContainerBuilder.RegisterInstance(_derivationBuilder).AsSelf();
            ContainerBuilder.RegisterInstance(_fakeRepositoryConfiguration).AsSelf();
            ContainerBuilder.RegisterType<Factory>().As<IFactory>().SingleInstance();
            ContainerBuilder.RegisterType<InMemoryEntityStore>().As<IInMemoryEntityStore>().SingleInstance();

            ContainerBuilder.RegisterType<TransactionalInMemoryBaseRepository>()
                .AsSelf()
                .As<IBaseRepository>()
                .As<ITransactionHolder>()
                .InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<MyRepository>()
               .As<IDerivationRepository, IRepository>()
               .WithParameter(new TypedParameter<IEnumerable<Type>>(new[] { typeof(EntityEventDecorator), typeof(StateStoreDecorator), typeof(FailureDecorator) }))
               .InstancePerLifetimeScope();

            ContainerBuilder.Register(c => _derivationBuilder.Build()).As<DerivationBusOptions>().SingleInstance();
            ContainerBuilder.RegisterType<DerivationBus>().AsSelf().As<IDerivationBus>().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<StateStore>().As<IStateStore>().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<EntityEventStore>().AsImplementedInterfaces().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<StateStoreDecorator>().AsSelf().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<EntityEventDecorator>().AsSelf().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<FailureDecorator>().AsSelf().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<Utf8JsonCopier>().As<ICopier>().InstancePerLifetimeScope();

            ContainerBuilder.RegisterType<ObjectSerializer>().AsImplementedInterfaces().SingleInstance();

            ContainerBuilder.RegisterAssemblyTypes(CocoriCore.Derivation.AssemblyInfo.Assembly, this.GetType().Assembly)
                .AssignableTo<IAutoRegister>()
                .AsImplementedInterfaces()
                .AsSelf();
            ContainerBuilder.RegisterAssemblyTypes(this.GetType().Assembly)
                .AssignableTo<IEntity>()
                .AsSelf();

            ContainerBuilder.Register(c => new DefaultErrorBusOptionsBuilder().Options).SingleInstance();
            ContainerBuilder.RegisterType<ErrorBus>().AsImplementedInterfaces().InstancePerLifetimeScope();

            ContainerBuilder.ConfigureXunitLogger();
        }

        [Fact]
        public async Task RecalculateSimpleProjectionWhenNotUpToDate()
        {
            _derivationBuilder.Project<AOneEntity>().Into<AOneManyProjection>().UsingMapping();

            AOneEntity entity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AOneEntity().WithId();
                entity.Name = "toto";
                await repository.InsertManyAsync(entity);
                await uow.FinishAsync();
            }

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var projection = await repository.LoadAsync<AOneManyProjection>(entity.Id);
                projection.OneName = "titi";
                await repository.UpdateAsync(projection);
                await uow.FinishAsync();
            }

            var projections = await BaseRepository
                .Query<AOneManyProjection>()
                .ToArrayAsync();

            projections.Should().ContainSingle().Which.OneName.Should().Be("titi");

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var derivationBus = uow.Resolve<IDerivationBus>();
                await derivationBus.RecalculateAsync<AOneManyProjection>();
            }

            projections = await BaseRepository
                .Query<AOneManyProjection>()
                .ToArrayAsync();

            projections.Should().ContainSingle().Which.OneName.Should().Be("toto");
        }

        [Fact]
        public async Task RecalculateManyProjectionWhenNotUpToDate()
        {
            _derivationBuilder.Project<AOneEntity, AManyEntity>(e => e.ManyIds)
                .IntoMany<AOneManyProjection>(p => p.OneId, p => p.ManyId)
                .UsingMapping();

            AOneEntity oneEntity = null;
            AManyEntity manyEntity1 = null;
            AManyEntity manyEntity2 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                manyEntity1 = new AManyEntity().WithId();
                manyEntity1.Name = "many1";
                manyEntity2 = new AManyEntity().WithId();
                manyEntity2.Name = "many2";
                oneEntity = new AOneEntity().WithId();
                oneEntity.Name = "one";
                oneEntity.ManyIds = new[] { manyEntity1.Id, manyEntity2.Id };
                await repository.InsertManyAsync(manyEntity1, manyEntity2, oneEntity);
                await uow.FinishAsync();
            }

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var projections = await repository.Query<AOneManyProjection>().ToArrayAsync();
                projections.ForEach(x => x.OneName = null);
                projections.ForEach(x => x.ManyName = null);
                await repository.UpdateManyAsync(projections);
                await uow.FinishAsync();
            }

            var outOfDateProjections = await BaseRepository
                .Query<AOneManyProjection>()
                .ToArrayAsync();
            outOfDateProjections.Should().HaveCount(2);
            outOfDateProjections.All(x => x.OneName == null && x.ManyName == null).Should().BeTrue();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var derivationBus = uow.Resolve<IDerivationBus>();
                await derivationBus.RecalculateAsync<AOneManyProjection>();
            }

            var recalculatedProjections = await BaseRepository
                .Query<AOneManyProjection>()
                .OrderBy(x => x.ManyName)
                .ToArrayAsync();

            recalculatedProjections.Should().HaveCount(2);
            recalculatedProjections[0].OneName.Should().Be("one");
            recalculatedProjections[0].ManyName.Should().Be("many1");
            recalculatedProjections[1].OneName.Should().Be("one");
            recalculatedProjections[1].ManyName.Should().Be("many2");
        }

        [Fact]
        public async Task RecalculateMixSimpleAndManyProjectionWhenNotUpToDate()
        {
            _derivationBuilder.Project<AOneEntity, AManyEntity>(e => e.ManyIds)
                .IntoMany<AOneManyProjection>(p => p.OneId, p => p.ManyId)
                //.PlusOne()
                .UsingMapping();
            _derivationBuilder.Project<AOneEntity>().Into<AOneManyProjection>().UsingMapping();

            AOneEntity oneEntity = null;
            AManyEntity manyEntity1 = null;
            AManyEntity manyEntity2 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                manyEntity1 = new AManyEntity().WithId();
                manyEntity1.Name = "many1";
                manyEntity2 = new AManyEntity().WithId();
                manyEntity2.Name = "many2";
                oneEntity = new AOneEntity().WithId();
                oneEntity.Name = "one";
                oneEntity.ManyIds = new[] { manyEntity1.Id, manyEntity2.Id };
                await repository.InsertManyAsync(manyEntity1, manyEntity2, oneEntity);
                await uow.FinishAsync();
            }

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var projections = await repository.Query<AOneManyProjection>().ToArrayAsync();
                projections.ForEach(x => x.OneName = null);
                projections.ForEach(x => x.ManyName = null);
                await repository.UpdateManyAsync(projections);
                await uow.FinishAsync();
            }

            var outOfDateProjections = await BaseRepository
                .Query<AOneManyProjection>()
                .ToArrayAsync();
            outOfDateProjections.Should().HaveCount(3);
            outOfDateProjections.All(x => x.OneName == null && x.ManyName == null).Should().BeTrue();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var derivationBus = uow.Resolve<IDerivationBus>();
                await derivationBus.RecalculateAsync<AOneManyProjection>();
            }

            var recalculatedProjections = await BaseRepository
                .Query<AOneManyProjection>()
                .OrderBy(x => x.ManyName)
                .ToArrayAsync();

            recalculatedProjections.Should().HaveCount(3);
            recalculatedProjections[0].OneName.Should().Be("one");
            recalculatedProjections[0].ManyName.Should().BeNull();
            recalculatedProjections[1].OneName.Should().Be("one");
            recalculatedProjections[1].ManyName.Should().Be("many1");
            recalculatedProjections[2].OneName.Should().Be("one");
            recalculatedProjections[2].ManyName.Should().Be("many2");
        }

        [Fact]
        public async Task RecalculateWhenProjectDifferentEntitiesIntoSameProjectionNotUpToDate()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();
            _derivationBuilder.Project<AnotherEntity>().Into<AProjection>().UsingMapping();

            AnEntity entity1 = new AnEntity();
            AnotherEntity entity2 = new AnotherEntity();
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity1 = new AnEntity().WithId();
                entity1.Name = "entity1";
                entity2 = new AnotherEntity().WithId();
                entity2.Name = "entity2";
                await repository.InsertManyAsync(entity1, entity2);
                await uow.FinishAsync();
            }

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var projections = await repository.Query<AProjection>().ToArrayAsync();
                projections.ForEach(x => x.EntityName = null);
                await repository.UpdateManyAsync(projections);
                await uow.FinishAsync();
            }

            var outOfDateProjections = await BaseRepository
                .Query<AProjection>()
                .ToArrayAsync();
            outOfDateProjections.Should().HaveCount(2);
            outOfDateProjections.All(x => x.EntityName == null).Should().BeTrue();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var derivationBus = uow.Resolve<IDerivationBus>();
                await derivationBus.RecalculateAsync<AProjection>();
            }

            var recalculatedProjections = await BaseRepository
                .Query<AProjection>()
                .OrderBy(x => x.EntityName)
                .ToArrayAsync();
            recalculatedProjections.Should().HaveCount(2);
            recalculatedProjections[0].Id.Should().Be(entity1.Id);
            recalculatedProjections[0].EntityName.Should().Be("entity1");
            recalculatedProjections[1].Id.Should().Be(entity2.Id);
            recalculatedProjections[1].EntityName.Should().Be("entity2");
        }

        [Fact]
        public async Task RecalculateWhenSomeProjectionsAreMissing()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();
            _derivationBuilder.Project<AnotherEntity>().Into<AProjection>().UsingMapping();

            AnEntity entity1 = new AnEntity();
            AnotherEntity entity2 = new AnotherEntity();
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity1 = new AnEntity().WithId();
                entity1.Name = "entity1";
                entity2 = new AnotherEntity().WithId();
                entity2.Name = "entity2";
                await repository.InsertManyAsync(entity1, entity2);
                await uow.FinishAsync();
            }

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var projections = await repository.Query<AProjection>().ToArrayAsync();
                await repository.DeleteAsync(projections[0]);
                projections[1].EntityName = null;
                await repository.UpdateAsync(projections[1]);
                await uow.FinishAsync();
            }

            var outOfDateProjections = await BaseRepository
                .Query<AProjection>()
                .ToArrayAsync();
            outOfDateProjections.Should().HaveCount(1);
            outOfDateProjections[0].EntityName.Should().BeNull();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var derivationBus = uow.Resolve<IDerivationBus>();
                await derivationBus.RecalculateAsync<AProjection>();
            }

            var recalculatedProjections = await BaseRepository
                .Query<AProjection>()
                .OrderBy(x => x.EntityName)
                .ToArrayAsync();
            recalculatedProjections.Should().HaveCount(2);
            recalculatedProjections[0].Id.Should().Be(entity1.Id);
            recalculatedProjections[0].EntityName.Should().Be("entity1");
            recalculatedProjections[1].Id.Should().Be(entity2.Id);
            recalculatedProjections[1].EntityName.Should().Be("entity2");
        }

        [Fact]
        public async Task RecalculateMixSimpleAndManyProjectionWhenMissing()
        {
            _derivationBuilder.Project<AOneEntity, AManyEntity>(e => e.ManyIds)
                .IntoMany<AOneManyProjection>(p => p.OneId, p => p.ManyId)
                .UsingMapping();
            _derivationBuilder.Project<AOneEntity>().Into<AOneManyProjection>().UsingMapping();

            AOneEntity oneEntity = null;
            AManyEntity manyEntity1 = null;
            AManyEntity manyEntity2 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                manyEntity1 = new AManyEntity().WithId();
                manyEntity1.Name = "many1";
                manyEntity2 = new AManyEntity().WithId();
                manyEntity2.Name = "many2";
                oneEntity = new AOneEntity().WithId();
                oneEntity.Name = "one";
                oneEntity.ManyIds = new[] { manyEntity1.Id, manyEntity2.Id };
                await repository.InsertManyAsync(manyEntity1, manyEntity2, oneEntity);
                await uow.FinishAsync();
            }

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var projections = await repository.Query<AOneManyProjection>().ToArrayAsync();
                await repository.DeleteManyAsync(projections);
                await uow.FinishAsync();
            }

            var outOfDateProjections = await BaseRepository
                .Query<AOneManyProjection>()
                .ToArrayAsync();
            outOfDateProjections.Should().BeEmpty();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var derivationBus = uow.Resolve<IDerivationBus>();
                await derivationBus.RecalculateAsync<AOneManyProjection>();
            }

            var recalculatedProjections = await BaseRepository
                .Query<AOneManyProjection>()
                .OrderBy(x => x.ManyName)
                .ToArrayAsync();

            recalculatedProjections.Should().HaveCount(3);
            recalculatedProjections[0].OneName.Should().Be("one");
            recalculatedProjections[0].ManyName.Should().BeNull();
            recalculatedProjections[1].OneName.Should().Be("one");
            recalculatedProjections[1].ManyName.Should().Be("many1");
            recalculatedProjections[2].OneName.Should().Be("one");
            recalculatedProjections[2].ManyName.Should().Be("many2");
        }

        [Fact]
        public async Task UseOneTransactionPerBatch()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();

            AnEntity entity1 = new AnEntity();
            AnEntity entity2 = new AnEntity();
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity1 = new AnEntity().WithId();
                entity1.Name = "entity1";
                entity2 = new AnEntity().WithId();
                entity2.Name = "entity2";
                await repository.InsertManyAsync(entity1, entity2);
                await uow.FinishAsync();
            }

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var projections = await repository.Query<AProjection>().ToArrayAsync();
                await repository.DeleteManyAsync(projections);
                await uow.FinishAsync();
            }

            _fakeRepositoryConfiguration.ErrorWhen((o, e) => o == CUDOperation.CREATE && e.Id == entity1.Id);

            Func<Task> action = async () =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var derivationBus = uow.Resolve<IDerivationBus>();
                    var batchSize = 1;
                    await derivationBus.RecalculateAsync<AProjection>(batchSize, new BatchOrderBy<AnEntity>(x => x.Name, OrderDirection.Desc));
                }
            };

            await action.Should().ThrowAsync<Exception>();

            var recalculatedProjections = await BaseRepository
                .Query<AProjection>()
                .ToArrayAsync();

            recalculatedProjections.Should().ContainSingle().Which.EntityName.Should().Be("entity2");
        }

        [Fact]
        public async Task NoExceptionIfNoBatchOrderByProviderAsync()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();

            Func<Task> action = async () =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var derivationBus = uow.Resolve<IDerivationBus>();
                    await derivationBus.RecalculateAsync<AProjection>();
                }
            };

            await action.Should().NotThrowAsync<Exception>();
        }

        [Fact]
        public async Task RecalculateWhenUseSynchonizer()
        {
            _derivationBuilder.Project<AnEntity>().Into<AGroupProjection>().UsingMapping();
            _derivationBuilder
                .ForDependency<DependencyEntityD>()
                .Update<AGroupProjection>((e, p) => e.AnEntityId == p.Id)
                .Using<GroupProjectionSynchronizer>();

            AnEntity entity1 = null;
            DependencyEntityD dependency1 = null;
            DependencyEntityD dependency2 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity1 = new AnEntity().WithId();
                entity1.Name = "entity1";
                dependency1 = new DependencyEntityD().WithId();
                dependency1.AnEntityId = entity1.Id;
                dependency2 = new DependencyEntityD().WithId();
                dependency2.AnEntityId = entity1.Id;
                await repository.InsertManyAsync(dependency1, dependency2, entity1);
                await uow.FinishAsync();
            }

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var projections = await repository.Query<AGroupProjection>().ToArrayAsync();
                await repository.DeleteManyAsync(projections);
                await uow.FinishAsync();
            }

            var outOfDateProjections = await BaseRepository
                .Query<AGroupProjection>()
                .ToArrayAsync();
            outOfDateProjections.Should().BeEmpty();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var derivationBus = uow.Resolve<IDerivationBus>();
                await derivationBus.RecalculateAsync<AGroupProjection>();
            }

            var recalculatedProjections = await BaseRepository
               .Query<AGroupProjection>()
               .ToArrayAsync();
            recalculatedProjections.Should().HaveCount(1);
            recalculatedProjections[0].Name.Should().Be("entity1");
            recalculatedProjections[0].DependencyDCount.Should().Be(2);
        }

        public class MyRepository : DecorableRepository, IDerivationRepository, IRepository
        {
            public MyRepository(IBaseRepository baseRepository, IUnitOfWork unitOfWork, IEnumerable<Type> decoratorTypes)
                : base(baseRepository, unitOfWork, decoratorTypes)
            {
            }
        }
    }
}

