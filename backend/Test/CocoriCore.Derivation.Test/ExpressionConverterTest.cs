﻿using CocoriCore.Common;
using CocoriCore.TestUtils;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Xunit;

namespace CocoriCore.Derivation.Test
{
    public class ExpressionConverterTest
    {
        [Fact]
        public void ConvertTrue()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => true;
            var dependencies = new Dependency[0];
            var allDependencies = new[]
            {
                new Dependency { Id2 = Tuid.Guid("1") },
                new Dependency { Id2 = Tuid.Guid("2") },
                new Dependency { Id2 = Tuid.Guid("3") }
            };

            var loadParameter = joinExpression.Parameters[0];
            var predicate = new ExpressionConverter().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(allDependencies, predicate);
            result.Should().HaveCount(3);
            result.Should().Contain(allDependencies[0])
                .And.Contain(allDependencies[1])
                .And.Contain(allDependencies[2]);
        }

        [Fact]
        public void ConvertFalse()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => false;
            var dependencies = new Dependency[0];
            var allDependencies = new[]
            {
                new Dependency { Id2 = Tuid.Guid("1") },
                new Dependency { Id2 = Tuid.Guid("2") },
                new Dependency { Id2 = Tuid.Guid("3") }
            };

            var loadParameter = joinExpression.Parameters[0];
            var predicate = new ExpressionConverter().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(allDependencies, predicate);
            result.Should().HaveCount(0);
            result.Should().NotContain(allDependencies[0])
                .And.NotContain(allDependencies[1])
                .And.NotContain(allDependencies[2]);
        }

        [Fact]
        public void ConvertSingleEquality_left()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => e.Id2 == p.Id2;
            var dependencies = new[]
            {
                new Dependency { Id2 = Tuid.Guid("1") },
                new Dependency { Id2 = Tuid.Guid("2") }
            };
            var allDependencies = new[]
            {
                new Dependency { Id2 = Tuid.Guid("1") },
                new Dependency { Id2 = Tuid.Guid("2") },
                new Dependency { Id2 = Tuid.Guid("2") },
                new Dependency { Id2 = Tuid.Guid("3") }
            };

            var loadParameter = joinExpression.Parameters[0];
            var predicate = new ExpressionConverter().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(allDependencies, predicate);
            result.Should().HaveCount(3);
            result.Should().Contain(allDependencies[0])
                .And.Contain(allDependencies[1])
                .And.Contain(allDependencies[2])
                .And.NotContain(allDependencies[3]);
        }

        [Fact]
        public void ConvertSingleEquality_right()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => p.Id2 == e.Id2;
            var dependencies = new[] 
            { 
                new Dependency { Id2 = Tuid.Guid("1") }, 
                new Dependency { Id2 = Tuid.Guid("2") } 
            };
            var allDependencies = new[]
            {
                new Dependency { Id2 = Tuid.Guid("1") },
                new Dependency { Id2 = Tuid.Guid("2") },
                new Dependency { Id2 = Tuid.Guid("2") },
                new Dependency { Id2 = Tuid.Guid("3") }
            };

            var loadParameter = joinExpression.Parameters[0];
            var predicate = new ExpressionConverter().GetPredicate(loadParameter, joinExpression, dependencies);

           var result = ApplyPredicate(allDependencies, predicate);
            result.Should().HaveCount(3);
            result.Should().Contain(allDependencies[0])
                .And.Contain(allDependencies[1])
                .And.Contain(allDependencies[2])
                .And.NotContain(allDependencies[3]);
        }

        [Fact]
        public void ConvertSingleNotEquality()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => e.Id != p.Id;
            var dependencies = new[]
            {
                new Dependency { Id = Tuid.Guid("1") },
                new Dependency { Id = Tuid.Guid("2") }
            };
            var allDependencies = new[]
            {
                new Dependency { Id = Tuid.Guid("1") },
                new Dependency { Id = Tuid.Guid("2") },
                new Dependency { Id = Tuid.Guid("3") }
            };

            var loadParameter = joinExpression.Parameters[0];
            var predicate = new ExpressionConverter().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(allDependencies, predicate);
            result.Should().HaveCount(1);
            result.Should().NotContain(allDependencies[0])
                .And.NotContain(allDependencies[1])
                .And.Contain(allDependencies[2]);

        }

        [Fact]
        public void ConvertDoubleEquality_and()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => e.Id == p.Id && p.Id2 == e.Id2;
            var dependencies = new[]
            {
                new Dependency { Id = Tuid.Guid("1"), Id2 = Tuid.Guid("a") },
                new Dependency { Id = Tuid.Guid("2"), Id2 = Tuid.Guid("b") }
            };
            var allDependencies = new[]
            {
                new Dependency { Id = Tuid.Guid("1"), Id2 = Tuid.Guid("a") },
                new Dependency { Id = Tuid.Guid("2"), Id2 = Tuid.Guid("a") },
                new Dependency { Id = Tuid.Guid("2"), Id2 = Tuid.Guid("b") },
                new Dependency { Id = Tuid.Guid("2"), Id2 = Tuid.Guid("c") },
                new Dependency { Id = Tuid.Guid("3"), Id2 = Tuid.Guid("d") }
            };

            var loadParameter = joinExpression.Parameters[0];
            var predicate = new ExpressionConverter().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(allDependencies, predicate);
            result.Should().HaveCount(3);
            result.Should().Contain(allDependencies[0])
                .And.Contain(allDependencies[1])
                .And.Contain(allDependencies[2])
                .And.NotContain(allDependencies[3])
                .And.NotContain(allDependencies[4]);
        }

        [Fact]
        public void ConvertDoubleEquality_or()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => e.Id == p.Id || p.Id2 == e.Id2;
            var dependencies = new[]
            {
                new Dependency { Id = Tuid.Guid("1"), Id2 = Tuid.Guid("a") },
                new Dependency { Id = Tuid.Guid("2"), Id2 = Tuid.Guid("b") }
            };
            var allDependencies = new[]
            {
                new Dependency { Id = Tuid.Guid("1"), Id2 = Tuid.Guid("a") },
                new Dependency { Id = Tuid.Guid("1"), Id2 = Tuid.Guid("b") },
                new Dependency { Id = Tuid.Guid("2"), Id2 = Tuid.Guid("c") },
                new Dependency { Id = Tuid.Guid("3"), Id2 = Tuid.Guid("b") },
                new Dependency { Id = Tuid.Guid("3"), Id2 = Tuid.Guid("d") },
            };

            var loadParameter = joinExpression.Parameters[0];
            var predicate = new ExpressionConverter().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(allDependencies, predicate);
            result.Should().HaveCount(4);
            result.Should().Contain(allDependencies[0])
                .And.Contain(allDependencies[1])
                .And.Contain(allDependencies[2])
                .And.Contain(allDependencies[3])
                .And.NotContain(allDependencies[4]);
        }

        [Fact]
        public void ConvertSingleEquality_projectionConstant()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => p.Id == Tuid.Guid("1");
            var dependencies = new Dependency[0];
            var allDependencies = new[]
            {
                new Dependency { Id = Tuid.Guid("1") },
                new Dependency { Id = Tuid.Guid("2") },
                new Dependency { Id = Tuid.Guid("3") }
            };

            var loadParameter = joinExpression.Parameters[0];
            var predicate = new ExpressionConverter().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(allDependencies, predicate);
            result.Should().HaveCount(3);
            result.Should().Contain(allDependencies[0])
                .And.Contain(allDependencies[1])
                .And.Contain(allDependencies[2]);
        }

        [Fact]
        public void ConvertSingleEquality_dependencyConstantEq()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => e.Id == Tuid.Guid("1");
            var dependencies = new Dependency[0];
            var allDependencies = new[]
            {
                new Dependency { Id = Tuid.Guid("1") },
                new Dependency { Id = Tuid.Guid("2") },
                new Dependency { Id = Tuid.Guid("3") }
            };

            var loadParameter = joinExpression.Parameters[0];
            var predicate = new ExpressionConverter().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(allDependencies, predicate);
            result.Should().HaveCount(1);
            result.Should().Contain(allDependencies[0])
                .And.NotContain(allDependencies[1])
                .And.NotContain(allDependencies[2]);
        }

        [Fact]
        public void ConvertSingleEquality_dependencyConstantNotEq()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => e.Id != Tuid.Guid("1");
            var dependencies = new Dependency[0];
            var allDependencies = new[]
            {
                new Dependency { Id = Tuid.Guid("1") },
                new Dependency { Id = Tuid.Guid("2") },
                new Dependency { Id = Tuid.Guid("3") }
            };

            var loadParameter = joinExpression.Parameters[0];
            var predicate = new ExpressionConverter().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(allDependencies, predicate);
            result.Should().HaveCount(2);
            result.Should().NotContain(allDependencies[0])
                .And.Contain(allDependencies[1])
                .And.Contain(allDependencies[2]);
        }

        [Fact]
        public void ConvertDoubleEquality_projectionConstantEq()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => e.Id == p.Id && p.Id2 == Tuid.Guid("a");
            var dependencies = new[]
            {
                new Dependency { Id = Tuid.Guid("1") },
                new Dependency { Id = Tuid.Guid("2") }
            };
            var allDependencies = new[]
            {
                new Dependency { Id = Tuid.Guid("1"), Id2 = Tuid.Guid("a") },
                new Dependency { Id = Tuid.Guid("2"), Id2 = Tuid.Guid("b") },
                new Dependency { Id = Tuid.Guid("3"), Id2 = Tuid.Guid("c") }
            };

            var loadParameter = joinExpression.Parameters[0];
            var predicate = new ExpressionConverter().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(allDependencies, predicate);
            result.Should().HaveCount(2);
            result.Should().Contain(allDependencies[0])
                .And.Contain(allDependencies[1])
                .And.NotContain(allDependencies[2]);
        }

        [Fact]
        public void ConvertDoubleEquality_projectionConstantNotEq()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => e.Id == p.Id && p.Id2 != Tuid.Guid("a");
            var dependencies = new[]
            {
                new Dependency { Id = Tuid.Guid("1") },
                new Dependency { Id = Tuid.Guid("2") }
            };
            var allDependencies = new[]
            {
                new Dependency { Id = Tuid.Guid("1"), Id2 = Tuid.Guid("a") },
                new Dependency { Id = Tuid.Guid("2"), Id2 = Tuid.Guid("b") },
                new Dependency { Id = Tuid.Guid("3"), Id2 = Tuid.Guid("c") }
            };

            var loadParameter = joinExpression.Parameters[0];
            var predicate = new ExpressionConverter().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(allDependencies, predicate);
            result.Should().HaveCount(2);
            result.Should().Contain(allDependencies[0])
                .And.Contain(allDependencies[1])
                .And.NotContain(allDependencies[2]);
        }

        [Fact]
        public void ConvertDoubleEquality_dependencyConstantAnd()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => e.Id == p.Id && e.Id2 == Tuid.Guid("a");
            var dependencies = new[]
            {
                new Dependency { Id = Tuid.Guid("1") },
                new Dependency { Id = Tuid.Guid("2") }
            };
            var allDependencies = new[]
            {
                new Dependency { Id = Tuid.Guid("1"), Id2 = Tuid.Guid("a") },
                new Dependency { Id = Tuid.Guid("2"), Id2 = Tuid.Guid("b") },
                new Dependency { Id = Tuid.Guid("3"), Id2 = Tuid.Guid("c") }
            };

            var loadParameter = joinExpression.Parameters[0];
            var predicate = new ExpressionConverter().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(allDependencies, predicate);
            result.Should().HaveCount(1);
            result.Should().Contain(allDependencies[0])
                .And.NotContain(allDependencies[1])
                .And.NotContain(allDependencies[2]);
        }

        [Fact]
        public void ConvertDoubleEquality_dependencyConstantOr()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => e.Id == p.Id || e.Id2 == Tuid.Guid("a");
            var dependencies = new[]
            {
                new Dependency { Id = Tuid.Guid("1") },
                new Dependency { Id = Tuid.Guid("2") }
            };
            var allDependencies = new[]
            {
                new Dependency { Id = Tuid.Guid("1"), Id2 = Tuid.Guid("a") },
                new Dependency { Id = Tuid.Guid("2"), Id2 = Tuid.Guid("b") },
                new Dependency { Id = Tuid.Guid("3"), Id2 = Tuid.Guid("a") },
                new Dependency { Id = Tuid.Guid("3"), Id2 = Tuid.Guid("c") }
            };

            var loadParameter = joinExpression.Parameters[0];
            var predicate = new ExpressionConverter().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(allDependencies, predicate);
            result.Should().HaveCount(3);
            result.Should().Contain(allDependencies[0])
                .And.Contain(allDependencies[1])
                .And.Contain(allDependencies[2])
                .And.NotContain(allDependencies[3]);
        }

        [Fact]
        public void ConvertDoubleEquality_dependencyConstantOrAnd()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => e.Id == p.Id || (e.Id2 == Tuid.Guid("a") && p.Id2 == Tuid.Guid("b"));
            var dependencies = new[]
            {
                new Dependency { Id = Tuid.Guid("1") },
                new Dependency { Id = Tuid.Guid("2") }
            };
            var allDependencies = new[]
            {
                new Dependency { Id = Tuid.Guid("1"), Id2 = Tuid.Guid("a") },
                new Dependency { Id = Tuid.Guid("2"), Id2 = Tuid.Guid("b") },
                new Dependency { Id = Tuid.Guid("3"), Id2 = Tuid.Guid("a") },
                new Dependency { Id = Tuid.Guid("3"), Id2 = Tuid.Guid("c") }
            };

            var loadParameter = joinExpression.Parameters[0];
            var predicate = new ExpressionConverter().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(allDependencies, predicate);
            result.Should().HaveCount(3);
            result.Should().Contain(allDependencies[0])
                .And.Contain(allDependencies[1])
                .And.Contain(allDependencies[2])
                .And.NotContain(allDependencies[3]);
        }

        [Fact]
        public void ConvertSingleEquality_dependencyBoolEq()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => e.IsOk;
            var dependencies = new Dependency[0];
            var allDependencies = new[]
            {
                new Dependency { IsOk = true },
                new Dependency { IsOk = true },
                new Dependency { IsOk = false }
            };

            var loadParameter = joinExpression.Parameters[0];
            var predicate = new ExpressionConverter().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(allDependencies, predicate);
            result.Should().HaveCount(2);
            result.Should().Contain(allDependencies[0])
                .And.Contain(allDependencies[1])
                .And.NotContain(allDependencies[2]);
        }

        [Fact]
        public void ConvertSingleEquality_dependencyBoolNotEq()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => !e.IsOk;
            var dependencies = new Dependency[0];
            var allDependencies = new[]
            {
                new Dependency { IsOk = false },
                new Dependency { IsOk = false },
                new Dependency { IsOk = true }
            };

            var loadParameter = joinExpression.Parameters[0];
            var predicate = new ExpressionConverter().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(allDependencies, predicate);
            result.Should().HaveCount(2);
            result.Should().Contain(allDependencies[0])
                .And.Contain(allDependencies[1])
                .And.NotContain(allDependencies[2]);
        }

        [Fact]
        public void ConvertSingleEquality_dependencyBoolEqAnd()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => e.IsOk && p.Id == e.Id;
            var dependencies = new[]
            {
                new Dependency { Id = Tuid.Guid("1") },
                new Dependency { Id = Tuid.Guid("2") }
            };
            var allDependencies = new[]
            {
                new Dependency { Id = Tuid.Guid("1"), IsOk = true },
                new Dependency { Id = Tuid.Guid("2"), IsOk = false },
                new Dependency { Id = Tuid.Guid("3"), IsOk = true }
            };

            var loadParameter = joinExpression.Parameters[0];
            var predicate = new ExpressionConverter().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(allDependencies, predicate);
            result.Should().HaveCount(1);
            result.Should().Contain(allDependencies[0])
                .And.NotContain(allDependencies[1])
                .And.NotContain(allDependencies[2]);
        }

        [Fact]
        public void ConvertSingleEquality_dependencyBoolNotEqAnd()
        {
            Expression<Func<Dependency, Projection, bool>> joinExpression = (e, p) => !e.IsOk && p.Id == e.Id;
            var dependencies = new[]
            {
                new Dependency { Id = Tuid.Guid("1") },
                new Dependency { Id = Tuid.Guid("2") }
            };
            var allDependencies = new[]
            {
                new Dependency { Id = Tuid.Guid("1"), IsOk = false },
                new Dependency { Id = Tuid.Guid("2"), IsOk = true },
                new Dependency { Id = Tuid.Guid("3"), IsOk = true }
            };

            var loadParameter = joinExpression.Parameters[0];
            var predicate = new ExpressionConverter().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(allDependencies, predicate);
            result.Should().HaveCount(1);
            result.Should().Contain(allDependencies[0])
                .And.NotContain(allDependencies[1])
                .And.NotContain(allDependencies[2]);
        }

        [Fact]
        public void ConvertSingleEquality_dependencyAndProjectionSameType()
        {
            Expression<Func<Dependency, Dependency, bool>> joinExpression = (e, p) => e.Id2 == p.Id;
            var dependencies = new[]
            {
                new Dependency { Id2 = Tuid.Guid("1") },
                new Dependency { Id2 = Tuid.Guid("2") }
            };
            var allDependencies = new[]
            {
                new Dependency { Id2 = Tuid.Guid("1") },
                new Dependency { Id2 = Tuid.Guid("2") },
                new Dependency { Id2 = Tuid.Guid("3") }
            };

            var loadParameter = joinExpression.Parameters[0];
            var predicate = new ExpressionConverter().GetPredicate(loadParameter, joinExpression, dependencies);

            var result = ApplyPredicate(allDependencies, predicate);
            result.Should().HaveCount(2);
            result.Should().Contain(allDependencies[0])
                .And.Contain(allDependencies[1])
                .And.NotContain(allDependencies[2]);
        }

        public IEnumerable<T> ApplyPredicate<T>(IEnumerable<T> source, LambdaExpression predicate)
        {
            return ConstructQueryableWhere(source.AsQueryable(), typeof(Dependency), predicate).Cast<T>().ToArray();
        }

        protected virtual IQueryable ConstructQueryableWhere(IQueryable queryable, Type typeToLoad, LambdaExpression predicate)
        {
            var whereMethod = QueryableMethods.Where.MakeGenericMethod(typeToLoad);
            var whereExp = Expression.Call(whereMethod, new Expression[] { queryable.Expression, Expression.Quote(predicate) });
            return queryable.Provider.CreateQuery(whereExp);
        }

        public class Dependency : IEntity
        {
            public Guid Id { get; set; }
            public Guid Id2 { get; set; }
            public bool IsOk { get; set; }
        }

        public class Projection : IEntity
        {
            public Guid Id { get; set; }
            public Guid? Id2 { get; set; }
        }

        public class ValueVisitor : ExpressionVisitor
        {
            protected override Expression VisitConstant(ConstantExpression node)
            {
                return base.VisitConstant(node);
            }
        }
    }
}
