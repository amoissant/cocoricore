using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocoriCore.Derivation.Test
{
    public class GroupProjectionSynchronizer : SynchronizerBase<DependencyEntityD, AGroupProjection>,
        IAutoRegister
    {
        public GroupProjectionSynchronizer(DependencyCoupler coupler) 
            : base(coupler)
        {
        }

        public override Task SynchonizeAsync(DependencyInfo info, IEnumerable<DependencyEntityD> dependencies, IEnumerable<AGroupProjection> projections)
        {
            var groups = Coupler.GroupDependenciesByProjection(info, dependencies, projections);
            foreach (var group in groups)
            {
                group.projection.DependencyDCount = group.dependencies.Count();
            }
            return Task.CompletedTask;
        }
    }
}