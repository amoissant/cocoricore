using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocoriCore.Derivation.Test
{
    public class AnAssociationSynchronizer : SynchronizerBase<AnAssociationEntity, AManyProjection>,
        IAutoRegister
    {
        public AnAssociationSynchronizer(DependencyCoupler coupler) 
            : base(coupler)
        {
        }

        public override Task ResetAsync(DependencyInfo info, IEnumerable<AnAssociationEntity> dependencies, IEnumerable<AManyProjection> projections)
        {
            foreach (var projection in projections)
            {
                projection.OneCount = 0;
            }
            return Task.CompletedTask;
        }

        public override Task SynchonizeAsync(DependencyInfo info, IEnumerable<AnAssociationEntity> dependencies, IEnumerable<AManyProjection> projections)
        {
            var groups = Coupler.GroupDependenciesByProjection(info, dependencies, projections);
            foreach (var group in groups)
            {
                group.projection.OneCount = group.dependencies.Count();
            }
            return Task.CompletedTask;
        }
    }
}