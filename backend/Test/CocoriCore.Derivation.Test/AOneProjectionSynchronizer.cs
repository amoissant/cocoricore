using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;

namespace CocoriCore.Derivation.Test
{
    public class AOneProjectionSynchronizer : SynchronizerBase<AOneManyProjection, AOneProjection>,
        IAutoRegister
    {
        public static string CallCount = $"{nameof(AOneProjectionSynchronizer)}_{nameof(CallCount)}";
        private IMemoryCache _cache;

        public AOneProjectionSynchronizer(DependencyCoupler coupler, IMemoryCache cache) 
            : base(coupler)
        {
            _cache = cache;
        }

        public override Task ResetAsync(DependencyInfo info, IEnumerable<AOneManyProjection> dependencies, IEnumerable<AOneProjection> projections)
        {
            foreach(var projection in projections)
            {
                projection.ManyCount = 0;
            }
            return Task.CompletedTask;
        }

        public override Task SynchonizeAsync(DependencyInfo info, IEnumerable<AOneManyProjection> dependencies, IEnumerable<AOneProjection> projections)
        {
            _cache.Set(CallCount, _cache.Get<int>(CallCount) + 1);
            var groups = Coupler.GroupDependenciesByProjection(info, dependencies, projections);
            foreach (var group in groups)
            {
                group.projection.ManyCount = group.dependencies.Count();
            }
            return Task.CompletedTask;
        }
    }
}