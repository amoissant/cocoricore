using System;
using System.Threading.Tasks;
using System.Threading;

namespace CocoriCore.Derivation.Test
{
    public class SpyFnFExceptionHandler : IAutoRegister
    {
        public int NbCall = 0;

        public Task HandleError(Exception exception)
        {
            Interlocked.Increment(ref NbCall);
            return Task.CompletedTask;
        }
    }
}

