﻿using System;
using Xunit;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Autofac;
using CocoriCore.Autofac;
using Autofac.Features.ResolveAnything;
using CocoriCore.TestUtils;

namespace CocoriCore.HttpError.Test
{
    public class AutofacLoggerExtensionTest
    {
        [Fact]
        public void LoggerNameMatchInjectedService()
        {
            var builder = new ContainerBuilder();
            var logEntries = new LogEntryCollection();
            builder.RegisterModule<LoggerModule>();
            builder.RegisterInstance(LoggerFactory.Create(x => x.AddSpy(logEntries)));
            builder.RegisterType<AService>().AsSelf();

            var service = builder.Build().Resolve<AService>();
            service.DoSomethinfWithLog();

            var logLine = logEntries.Should().ContainSingle().Which;
            logLine.Category.Should().Be(typeof(AService).FullName);
            logLine.Message.Should().Be("something");
        }

        [Fact]
        public void LoggerNameIdDefautWhenResolveMannualyLogger()
        {
            var builder = new ContainerBuilder();
            var logEntries = new LogEntryCollection();
            builder.RegisterModule<LoggerModule>();
            builder.RegisterInstance(LoggerFactory.Create(x => x.AddSpy(logEntries)));

            var logger = builder.Build().Resolve<ILogger>();
            logger.LogInformation("test default logger");

            var logEntry = logEntries.Should().ContainSingle().Which;
            logEntry.Category.Should().Be("testhost.DefaultLogger");
            logEntry.Message.Should().Be("test default logger");
        }

        [Fact]
        public void ResolveUnregisteredConcreteClass()
        {
            var builder = new ContainerBuilder();
            builder.RegisterSource(new AnyConcreteTypeNotAlreadyRegisteredSource());

            AConcreteClass service = null;
            Action action = () => service = builder.Build().Resolve<AConcreteClass>();

            action.Should().NotThrow<Exception>();
        }
    }

    public class AService
    {
        public ILogger Logger { get; }

        public AService(ILogger logger)
        {
            Logger = logger;
        }

        public void DoSomethinfWithLog()
        {
            Logger.LogInformation("something");
        }
    }

    public class AConcreteClass
    {
    }
}
