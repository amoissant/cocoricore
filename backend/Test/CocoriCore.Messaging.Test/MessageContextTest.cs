﻿using Xunit;
using FluentAssertions;
using CocoriCore.TestUtils;

namespace CocoriCore.Messaging.Test
{
    public class MessageContextTest
    {
        [Fact]
        public void HasKeyWithValue_existingKeyWithValue_returnTrue()
        {
            var context = new MessageContext(null);
            context["key"] = "a";

            var result = context.HasKeyWithValue("key", "a");

            result.Should().BeTrue();
        }

        [Fact]
        public void HasKeyWithValue_nonExistingKeyWithValue_returnFalse()
        {
            var context = new MessageContext(null);
            context["zKey"] = "a";

            var result = context.HasKeyWithValue("key", "a");

            result.Should().BeFalse();
        }

        [Fact]
        public void HasKeyWithValue_existingKeyWithOtherValue_returnFalse()
        {
            var context = new MessageContext(null);
            context["key"] = "a";

            var result = context.HasKeyWithValue("key", "b");

            result.Should().BeFalse();
        }
    }
}
