﻿using System;
using Xunit;
using FluentAssertions;
using System.Threading.Tasks;
using CocoriCore.TestUtils;
using CocoriCore.Common;
using System.Linq.Expressions;

namespace CocoriCore.Messaging.Test
{
    public class MessageBusOptionsBuilderTest
    {
        private readonly MessageBusOptionsBuilder _messageBusBuilder;

        public MessageBusOptionsBuilderTest()
        {
            _messageBusBuilder = new MessageBusOptionsBuilder();
        }

        [Fact]
        public void AddRule_withName_createEmptyRule()
        {
            _messageBusBuilder.AddRule("myRule");

            var options = _messageBusBuilder.Build();

            var rule = options.Rules.Should().ContainSingle().Which;
            rule.Name.Should().Be("myRule");
            rule.Condition.Should().BeNull();
            rule.CallActions.Should().BeEmpty();
            rule.ErrorActions.Should().BeEmpty();
            rule.FinallyActions.Should().BeEmpty();
        }
        [Fact]
        public void When_func_createRuleWithCondition()
        {
            Func<MessageContext, bool> condition = (c) => true;
            _messageBusBuilder
                .AddRule("myRule")
                .When(condition);

            var options = _messageBusBuilder.Build();

            options.Rules.Should().ContainSingle().Which.Condition.Should().WrapCall(condition);
        }

        [Fact]
        public void When_funcTask_createRuleWithCondition()
        {
            Func<MessageContext, Task<bool>> condition = (c) => Task.FromResult(true);
            _messageBusBuilder
                .AddRule("myRule")
                .When(condition);

            var options = _messageBusBuilder.Build();

            options.Rules.Should().ContainSingle().Which.Condition.Should().WrapCall(condition);
        }

        [Fact]
        public void When_funcService_createRuleWithCondition()
        {
            Func<IConditionService, MessageContext, bool> condition = (x, c) => x.IsConditionTrue(c);
            _messageBusBuilder
                .AddRule("myRule")
                .When(condition);

            var options = _messageBusBuilder.Build();

            options.Rules.Should().ContainSingle().Which.Condition.Should().WrapCall(condition);
        }

        [Fact]
        public void When_funcTaskService_createRuleWithCondition()
        {
            Func<IConditionService, MessageContext, Task<bool>> condition = (x, c) => x.IsConditionTrueAsync(c);
            _messageBusBuilder
                .AddRule("myRule")
                .When(condition);

            var options = _messageBusBuilder.Build();

            options.Rules.Should().ContainSingle().Which.Condition.Should().WrapCall(condition);
        }

        [Fact]
        public void Call_func_addActionToRule()
        {
            Action<MessageContext> action = (c) => { };
            _messageBusBuilder
                .AddRule("myRule")
                .Call(action);

            var options = _messageBusBuilder.Build();

            options.Rules.Should().ContainSingle()
                .Which.CallActions.Should().ContainSingle()
                .Which.Should().WrapCall(action);
        }

        [Fact]
        public void Call_funcTask_addActionToRule()
        {
            Func<MessageContext, Task> action = (c) => Task.CompletedTask;
            _messageBusBuilder
                .AddRule("myRule")
                .Call(action);

            var options = _messageBusBuilder.Build();

            options.Rules.Should().ContainSingle()
                .Which.CallActions.Should().ContainSingle()
                .Which.Should().WrapCall(action);
        }

        [Fact]
        public void Call_funcService_addActionToRule()
        {
            Action<IMessageHandler, MessageContext> action = (x, c) => x.HandleMessage(c);
            _messageBusBuilder
                .AddRule("myRule")
                .Call(action);

            var options = _messageBusBuilder.Build();

            options.Rules.Should().ContainSingle()
                .Which.CallActions.Should().ContainSingle()
                .Which.Should().WrapCall(action);
        }

        [Fact]
        public void Call_funcTaskService_addActionToRule()
        {
            Func<IMessageHandler, MessageContext, Task> action = (x, c) => x.HandleMessageAsync(c);
            _messageBusBuilder
                .AddRule("myRule")
                .Call(action);

            var options = _messageBusBuilder.Build();

            options.Rules.Should().ContainSingle()
                .Which.CallActions.Should().ContainSingle()
                .Which.Should().WrapCall(action);
        }

        [Fact]
        public void Call_chained_appendActionIntoRule()
        {
            Func<IMessageHandler, MessageContext, Task> action1 = (x, c) => x.HandleMessageAsync(c);
            Action<IOtherService, MessageContext> action2 = (x, c) => x.DoSomething(c);
            _messageBusBuilder
                .AddRule("myRule")
                .Call(action1)
                .Call(action2);

            var options = _messageBusBuilder.Build();

            var rule = options.Rules.Should().ContainSingle().Which;
            rule.CallActions.Should().HaveCount(2);
            rule.CallActions[0].Should().WrapCall(action1);
            rule.CallActions[1].Should().WrapCall(action2);
        }

        [Fact]
        public void OnError_func_addActionToRule()
        {
            Action<MessageContext> action = (c) => { };
            _messageBusBuilder
                .AddRule("myRule")
                .OnError(action);

            var options = _messageBusBuilder.Build();

            options.Rules.Should().ContainSingle()
                .Which.ErrorActions.Should().ContainSingle()
                .Which.Should().WrapCall(action);
        }

        [Fact]
        public void OnError_funcTask_addActionToRule()
        {
            Func<MessageContext, Task> action = (c) => Task.CompletedTask;
            _messageBusBuilder
                .AddRule("myRule")
                .OnError(action);

            var options = _messageBusBuilder.Build();

            options.Rules.Should().ContainSingle()
                .Which.ErrorActions.Should().ContainSingle()
                .Which.Should().WrapCall(action);
        }

        [Fact]
        public void OnError_funcService_addActionToRule()
        {
            Action<IErrorService, MessageContext> action = (x, c) => x.HandleError(c);
            _messageBusBuilder
                .AddRule("myRule")
                .OnError(action);

            var options = _messageBusBuilder.Build();

            options.Rules.Should().ContainSingle()
                .Which.ErrorActions.Should().ContainSingle()
                .Which.Should().WrapCall(action);
        }

        [Fact]
        public void OnError_funcTaskService_addActionToRule()
        {
            Func<IErrorService, MessageContext, Task> action = (x, c) => x.HandleErrorAsync(c);
            _messageBusBuilder
                .AddRule("myRule")
                .OnError(action);

            var options = _messageBusBuilder.Build();

            options.Rules.Should().ContainSingle()
                .Which.ErrorActions.Should().ContainSingle()
                .Which.Should().WrapCall(action);
        }

        [Fact]
        public void OnError_chained_appendErrorActionIntoRule()
        {
            Func<IErrorService, MessageContext, Task> action1 = (x, c) => x.HandleErrorAsync(c);
            Action<IOtherService, MessageContext> action2 = (x, c) => x.DoSomething(c);
            _messageBusBuilder
                .AddRule("myRule")
                .OnError(action1)
                .OnError(action2);

            var options = _messageBusBuilder.Build();

            var rule = options.Rules.Should().ContainSingle().Which;
            rule.ErrorActions.Should().HaveCount(2);
            rule.ErrorActions[0].Should().WrapCall(action1);
            rule.ErrorActions[1].Should().WrapCall(action2);
        }

        [Fact]
        public void Finally_func_addActionToRule()
        {
            Action<MessageContext> action = (c) => { };
            _messageBusBuilder
                .AddRule("myRule")
                .Finally(action);

            var options = _messageBusBuilder.Build();

            options.Rules.Should().ContainSingle()
                .Which.FinallyActions.Should().ContainSingle()
                .Which.Should().WrapCall(action);
        }

        [Fact]
        public void Finally_funcTask_addActionToRule()
        {
            Func<MessageContext, Task> action = (c) => Task.CompletedTask;
            _messageBusBuilder
                .AddRule("myRule")
                .Finally(action);

            var options = _messageBusBuilder.Build();

            options.Rules.Should().ContainSingle()
                .Which.FinallyActions.Should().ContainSingle()
                .Which.Should().WrapCall(action);
        }

        [Fact]
        public void Finally_funcService_addActionToRule()
        {
            Action<IFinallyService, MessageContext> action = (x, c) => x.Finally(c);
            _messageBusBuilder
                .AddRule("myRule")
                .Finally(action);

            var options = _messageBusBuilder.Build();

            options.Rules.Should().ContainSingle()
                .Which.FinallyActions.Should().ContainSingle()
                .Which.Should().WrapCall(action);
        }

        [Fact]
        public void Finally_funcTaskService_addActionToRule()
        {
            Func<IFinallyService, MessageContext, Task> action = (x, c) => x.FinallyAsync(c);
            _messageBusBuilder
                .AddRule("myRule")
                .Finally(action);

            var options = _messageBusBuilder.Build();

            options.Rules.Should().ContainSingle()
                .Which.FinallyActions.Should().ContainSingle()
                .Which.Should().WrapCall(action);
        }

        [Fact]
        public void Finally_chained_appendFinallyActionIntoRule()
        {
            Func<IFinallyService, MessageContext, Task> action1 = (x, c) => x.FinallyAsync(c);
            Action<IOtherService, MessageContext> action2 = (x, c) => x.DoSomething(c);
            _messageBusBuilder
                .AddRule("myRule")
                .Finally(action1)
                .Finally(action2);

            var options = _messageBusBuilder.Build();

            var rule = options.Rules.Should().ContainSingle().Which;
            rule.FinallyActions.Should().HaveCount(2);
            rule.FinallyActions[0].Should().WrapCall(action1);
            rule.FinallyActions[1].Should().WrapCall(action2);
        }

        [Fact]
        public void BeforeAll_func_addBeforeAction()
        {
            Action<MessageContext> action = (c) => { };
            _messageBusBuilder.BeforeAll(action);

            var options = _messageBusBuilder.Build();

            options.BeforeAllActions.Should().ContainSingle()
                .Which.Should().WrapCall(action);
        }

        [Fact]
        public void BeforeAll_funcTask_addBeforeAction()
        {
            Func<MessageContext, Task> action = (c) => Task.CompletedTask;
            _messageBusBuilder.BeforeAll(action);

            var options = _messageBusBuilder.Build();

            options.BeforeAllActions.Should().ContainSingle()
                .Which.Should().WrapCall(action);
        }

        [Fact]
        public void BeforeAll_funcService_addBeforeAction()
        {
            Action<IBeforeService, MessageContext> action = (x, c) => x.Before(c);
            _messageBusBuilder.BeforeAll(action);

            var options = _messageBusBuilder.Build();

            options.BeforeAllActions.Should().ContainSingle()
                .Which.Should().WrapCall(action);
        }

        [Fact]
        public void BeforeAll_funcTaskService_addBeforeAllAction()
        {
            Func<IBeforeService, MessageContext, Task> action = (x, c) => x.BeforeAsync(c);
            _messageBusBuilder.BeforeAll(action);

            var options = _messageBusBuilder.Build();

            options.BeforeAllActions.Should().ContainSingle()
                .Which.Should().WrapCall(action);
        }

        [Fact]
        public void BeforeAll_chained_appendBeforeAllAction()
        {
            Func<IBeforeService, MessageContext, Task> action1 = (x, c) => x.BeforeAsync(c);
            Action<IOtherService, MessageContext> action2 = (x, c) => x.DoSomething(c);
            _messageBusBuilder
                .BeforeAll(action1)
                .BeforeAll(action2);

            var options = _messageBusBuilder.Build();

            options.BeforeAllActions.Should().HaveCount(2);
            options.BeforeAllActions[0].Should().WrapCall(action1);
            options.BeforeAllActions[1].Should().WrapCall(action2);
        }

        [Fact]
        public void Build_twoRulesSameName_throwException()
        {
            _messageBusBuilder.AddRule("myRule");
            _messageBusBuilder.AddRule("myRule");

            Action action = () => _messageBusBuilder.Build();

            action.Should().Throw<ConfigurationException>().WithMessage("Duplicate*");
        }

        [Fact]
        public void CallForEach_emptyConfiguration_addActionXithEmptyBatchRule()
        {
            BatchRule batchRule = null;
            _messageBusBuilder
                .AddRule("bacth")
                .When(c => c is IBatch)
                .CallForEach(b => b.ExposeRule(r => batchRule = r));

            var options = _messageBusBuilder.Build();

            options.Rules.Should().ContainSingle().Which
                .CallActions.Should().ContainSingle().Which
                .Expression.Should().BeEquivalentTo<IBatchHandler, MessageContext, Task>((x, c) => x.ExecuteAsync(batchRule, c));
            batchRule.CallActions.Should().BeEmpty();
            batchRule.ErrorActions.Should().BeEmpty();
            batchRule.FinallyActions.Should().BeEmpty();
        }

        [Fact]
        public void CallForEach_callFunc_addActionToBatchRule()
        {
            Action<BatchContext> action = c => { };
            BatchRule batchRule = null;
            _messageBusBuilder
                .AddRule("bacth")
                .When(c => c is IBatch)
                .CallForEach(b => b
                    .Call(action)
                    .ExposeRule(r => batchRule = r));

            var options = _messageBusBuilder.Build();

            batchRule.CallActions.Should().ContainSingle().Which
                .Should().WrapCall(action);
        }

        [Fact]
        public void CallForEach_callFuncTask_addActionToBatchRule()
        {
            Func<BatchContext, Task> action = c => Task.CompletedTask;
            BatchRule batchRule = null;
            _messageBusBuilder
                .AddRule("bacth")
                .When(c => c is IBatch)
                .CallForEach(b => b
                    .Call(action)
                    .ExposeRule(r => batchRule = r));

            var options = _messageBusBuilder.Build();

            batchRule.CallActions.Should().ContainSingle().Which
                .Should().WrapCall(action);
        }

        [Fact]
        public void CallForEach_callFuncService_addActionToBatchRule()
        {
            Action<IMessageHandler, BatchContext> action = (x, c) => x.HandleMessage(c);
            BatchRule batchRule = null;
            _messageBusBuilder
                .AddRule("bacth")
                .When(c => c is IBatch)
                .CallForEach(b => b
                    .Call(action)
                    .ExposeRule(r => batchRule = r));

            var options = _messageBusBuilder.Build();

            batchRule.CallActions.Should().ContainSingle().Which
                .Should().WrapCall(action);
        }

        [Fact]
        public void CallForEach_callFuncTaskService_addActionToBatchRule()
        {
            Func<IMessageHandler, BatchContext, Task> action = (x, c) => x.HandleMessageAsync(c);
            BatchRule batchRule = null;
            _messageBusBuilder
                .AddRule("bacth")
                .When(c => c is IBatch)
                .CallForEach(b => b
                    .Call(action)
                    .ExposeRule(r => batchRule = r));

            var options = _messageBusBuilder.Build();

            batchRule.CallActions.Should().ContainSingle().Which
                .Should().WrapCall(action);
        }

        [Fact]
        public void CallForEach_onErrorFunc_addActionToBatchRule()
        {
            Action<BatchContext> action = c => { };
            BatchRule batchRule = null;
            _messageBusBuilder
                .AddRule("bacth")
                .When(c => c is IBatch)
                .CallForEach(b => b
                    .OnError(action)
                    .ExposeRule(r => batchRule = r));

            var options = _messageBusBuilder.Build();

            batchRule.ErrorActions.Should().ContainSingle().Which
                .Should().WrapCall(action);
        }

        [Fact]
        public void CallForEach_onErrorFuncTask_addActionToBatchRule()
        {
            Func<BatchContext, Task> action = c => Task.CompletedTask;
            BatchRule batchRule = null;
            _messageBusBuilder
                .AddRule("bacth")
                .When(c => c is IBatch)
                .CallForEach(b => b
                    .OnError(action)
                    .ExposeRule(r => batchRule = r));

            var options = _messageBusBuilder.Build();

            batchRule.ErrorActions.Should().ContainSingle().Which
                .Should().WrapCall(action);
        }

        [Fact]
        public void CallForEach_onErrorFuncService_addActionToBatchRule()
        {
            Action<IErrorService, BatchContext> action = (x, c) => x.HandleError(c);
            BatchRule batchRule = null;
            _messageBusBuilder
                .AddRule("bacth")
                .When(c => c is IBatch)
                .CallForEach(b => b
                    .OnError(action)
                    .ExposeRule(r => batchRule = r));

            var options = _messageBusBuilder.Build();

            batchRule.ErrorActions.Should().ContainSingle().Which
                .Should().WrapCall(action);
        }

        [Fact]
        public void CallForEach_onErrorFuncTaskService_addActionToBatchRule()
        {
            Func<IErrorService, BatchContext, Task> action = (x, c) => x.HandleErrorAsync(c);
            BatchRule batchRule = null;
            _messageBusBuilder
                .AddRule("bacth")
                .When(c => c is IBatch)
                .CallForEach(b => b
                    .OnError(action)
                    .ExposeRule(r => batchRule = r));

            var options = _messageBusBuilder.Build();

            batchRule.ErrorActions.Should().ContainSingle().Which
                .Should().WrapCall(action);
        }

        [Fact]
        public void CallForEach_finallyFunc_addActionToBatchRule()
        {
            Action<BatchContext> action = c => { };
            BatchRule batchRule = null;
            _messageBusBuilder
                .AddRule("bacth")
                .When(c => c is IBatch)
                .CallForEach(b => b
                    .Finally(action)
                    .ExposeRule(r => batchRule = r));

            var options = _messageBusBuilder.Build();

            batchRule.FinallyActions.Should().ContainSingle().Which
                .Should().WrapCall(action);
        }

        [Fact]
        public void CallForEach_finallyFuncTask_addActionToBatchRule()
        {
            Func<BatchContext, Task> action = c => Task.CompletedTask;
            BatchRule batchRule = null;
            _messageBusBuilder
                .AddRule("bacth")
                .When(c => c is IBatch)
                .CallForEach(b => b
                    .Finally(action)
                    .ExposeRule(r => batchRule = r));

            var options = _messageBusBuilder.Build();

            batchRule.FinallyActions.Should().ContainSingle().Which
                .Should().WrapCall(action);
        }

        [Fact]
        public void CallForEach_finallyFuncService_addActionToBatchRule()
        {
            Action<IFinallyService, BatchContext> action = (x, c) => x.Finally(c);
            BatchRule batchRule = null;
            _messageBusBuilder
                .AddRule("bacth")
                .When(c => c is IBatch)
                .CallForEach(b => b
                    .Finally(action)
                    .ExposeRule(r => batchRule = r));

            var options = _messageBusBuilder.Build();

            batchRule.FinallyActions.Should().ContainSingle().Which
                .Should().WrapCall(action);
        }

        [Fact]
        public void CallForEach_finallyFuncTaskService_addActionToBatchRule()
        {
            Func<IFinallyService, BatchContext, Task> action = (x, c) => x.FinallyAsync(c);
            BatchRule batchRule = null;
            _messageBusBuilder
                .AddRule("bacth")
                .When(c => c is IBatch)
                .CallForEach(b => b
                    .Finally(action)
                    .ExposeRule(r => batchRule = r));

            var options = _messageBusBuilder.Build();

            batchRule.FinallyActions.Should().ContainSingle().Which
                .Should().WrapCall(action);
        }

        public interface IBeforeService
        {
            void Before(MessageContext context);
            Task BeforeAsync(MessageContext context);
        }

        public interface IConditionService
        {
            bool IsConditionTrue(MessageContext context);

            Task<bool> IsConditionTrueAsync(MessageContext context);
        }

        public interface IMessageHandler
        {
            void HandleMessage(MessageContext context);

            Task HandleMessageAsync(MessageContext context);
        }

        public interface IOtherService
        {
            void DoSomething(MessageContext context);
        }

        public interface IErrorService
        {
            void HandleError(MessageContext context);
            Task HandleErrorAsync(MessageContext context);
        }

        public interface IFinallyService
        {
            void Finally(MessageContext context);
            Task FinallyAsync(MessageContext context);
        }

        public class MyMessage
        {
        }
    }
}
