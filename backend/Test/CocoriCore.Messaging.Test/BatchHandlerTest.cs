﻿using Xunit;
using FluentAssertions;
using CocoriCore.TestUtils;
using System;
using Autofac;
using CocoriCore.Common;
using CocoriCore.Autofac;
using Moq;
using System.Threading.Tasks;
using System.Collections.Generic;
using CocoriCore.Types.Extension;
using CocoriCore.Common.Diagnostic;
using System.Linq.Expressions;
using Moq.Language.Flow;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace CocoriCore.Messaging.Test
{
    public class BatchHandlerTest
    {
        private readonly Lazy<ILifetimeScope> _rootScope;
        private readonly ContainerBuilder _builder;
        private readonly MessageBusOptionsBuilder _messageBusBuilder;
        private readonly Mock<IService> _serviceMock;
        private readonly Mock<IErrorService> _errorServiceMock;
        private readonly Mock<IFinallyService> _finallyServiceMock;
        private readonly Mock<IOtherService> _otherServiceMock;
        private readonly LogEntryCollection _logEntries;
        private readonly List<(string method, BatchContext context)> _calls;

        public BatchHandlerTest()
        {
            _builder = new ContainerBuilder();
            _rootScope = new Lazy<ILifetimeScope>(() => _builder.Build());
            _messageBusBuilder = new MessageBusOptionsBuilder();
            _logEntries = new LogEntryCollection();
            _calls = new List<(string, BatchContext)>();

            _builder.Register(c => _messageBusBuilder.Build()).SingleInstance();
            _builder.RegisterType<BatchHandler>().AsSelf().As<IBatchHandler>().InstancePerLifetimeScope();

            _builder.Register(c => new UnitOfWorkOptionsBuilder().Options).SingleInstance();
            _builder.RegisterType<AutofacUnitOfWorkFactory>().As<IUnitOfWorkFactory>().SingleInstance();
            _builder.RegisterType<AutofacUnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();

            _serviceMock = new Mock<IService>();
            _errorServiceMock = new Mock<IErrorService>();
            _finallyServiceMock = new Mock<IFinallyService>();
            _otherServiceMock = new Mock<IOtherService>();
            
            _serviceMock.Setup(x => x.Handle(It.IsAny<BatchContext>())).Callback<BatchContext>(c => _calls.Add((nameof(IService.Handle), c)));
            _otherServiceMock.Setup(x => x.Other(It.IsAny<BatchContext>())).Callback<BatchContext>(c => _calls.Add((nameof(IOtherService.Other), c)));
            _errorServiceMock.Setup(x => x.OnError(It.IsAny<BatchContext>())).Callback<BatchContext>(c => _calls.Add((nameof(IErrorService.OnError), c)));
            _finallyServiceMock.Setup(x => x.Finally(It.IsAny<BatchContext>())).Callback<BatchContext>(c => _calls.Add((nameof(IFinallyService.Finally), c)));

            _builder.RegisterInstance(_serviceMock.Object);
            _builder.RegisterInstance(_errorServiceMock.Object);
            _builder.RegisterInstance(_finallyServiceMock.Object);
            _builder.RegisterInstance(_otherServiceMock.Object);

            _builder.ConfigureXunitLogger(logEntries: _logEntries);
        }

        private ILifetimeScope RootScope => _rootScope.Value;
        private IBatchHandler BatchHandler => RootScope.Resolve<IBatchHandler>();

        [Fact]
        public async Task ExecuteAsync_nullContext_throwExceptionAsync()
        {
            var rule = new BatchRule();

            Func<Task> action = () => BatchHandler.ExecuteAsync(rule, null);

            await action.Should().ThrowAsync<ArgumentNullException>().WithMessage($"*context*");
        }

        [Fact]
        public async Task ExecuteAsync_nullRule_throwExceptionAsync()
        {
            var context = new MessageContext(new Batch<MyMessage>());

            Func<Task> action = () => BatchHandler.ExecuteAsync(null, context);

            await action.Should().ThrowAsync<ArgumentNullException>().WithMessage($"*rule*");
        }

        [Theory]
        [MemberData(nameof(Messages))]
        public async Task ExecuteAsync_nonBatchMessageOrNullMessage_throwExceptionAsync(object message)
        {
            var context = new MessageContext(message);
            var rule = new BatchRule();

            Func<Task> action = () => BatchHandler.ExecuteAsync(rule, context);

            await action.Should().ThrowAsync<MessageBusException>().WithMessage($"*{message?.GetPrettyType(full : true)}*must implements {typeof(IBatch)}*");
        }

        [Fact]
        public async Task ExecuteAsync_emptyRuleAndEmptyBatch_runWithoutErrorAsync()
        {
            var context = new MessageContext(new Batch<MyMessage>());
            var rule = new BatchRule();

            Func<Task> action = () => BatchHandler.ExecuteAsync(rule, context);

            await action.Should().NotThrowAsync<Exception>();
        }

        [Fact]
        public async Task ExecuteAsync_oneMessageSeveralCall_runActionsInRegistrationOrderForEachMessage()
        {
            var batch = new Batch<MyMessage>(new MyMessage(), new MyMessage());
            var context = new MessageContext(batch);
            var rule = new BatchRuleBuilder()
                .Call<IService>((x, c) => x.Handle(c))
                .Call<IOtherService>((x, c) => x.Other(c))
                .Rule;

            await BatchHandler.ExecuteAsync(rule, context);

            _calls.Should().HaveCount(4);
            _calls.Select(x => x.method).Should().ContainInOrder(new[] 
            { 
                nameof(IService.Handle), 
                nameof(IOtherService.Other),
                nameof(IService.Handle),
                nameof(IOtherService.Other)
            });
        }

        [Fact]
        public async Task ExecuteAsync_oneMessageSeveralOnError_runActionsInRegistrationOrderForEachMessage()
        {
            var batch = new Batch<MyMessage>(new MyMessage(), new MyMessage());
            var context = new MessageContext(batch);
            var rule = new BatchRuleBuilder()
                .Call(x => throw new Exception())
                .OnError<IErrorService>((x, c) => x.OnError(c))
                .OnError<IOtherService>((x, c) => x.Other(c))
                .Rule;

            await BatchHandler.ExecuteAsync(rule, context);

            _calls.Should().HaveCount(4);
            _calls.Select(x => x.method).Should().ContainInOrder(new[]
            {
                nameof(IErrorService.OnError),
                nameof(IOtherService.Other),
                nameof(IErrorService.OnError),
                nameof(IOtherService.Other)
            });
        }

        [Fact]
        public async Task ExecuteAsync_oneMessageSeveralFinally_runActionsInRegistrationOrderForEachMessage()
        {
            var batch = new Batch<MyMessage>(new MyMessage(), new MyMessage());
            var context = new MessageContext(batch);
            var rule = new BatchRuleBuilder()
                .Finally<IFinallyService>((x, c) => x.Finally(c))
                .Finally<IOtherService>((x, c) => x.Other(c))
                .Rule;

            await BatchHandler.ExecuteAsync(rule, context);

            _calls.Should().HaveCount(4);
            _calls.Select(x => x.method).Should().ContainInOrder(new[]
            {
                nameof(IFinallyService.Finally),
                nameof(IOtherService.Other),
                nameof(IFinallyService.Finally),
                nameof(IOtherService.Other)
            });
        }

        [Fact]
        public async Task ExecuteAsync_ruleWithCallOnErrorFinallyNoException_runCallThenFinallyForEachMessage()
        {
            var batch = new Batch<MyMessage>(new MyMessage(), new MyMessage());
            var context = new MessageContext(batch);
            var rule = new BatchRuleBuilder()
                .Call<IService>((x, c) => x.Handle(c))
                .OnError<IErrorService>((x, c) => x.OnError(c))
                .Finally<IFinallyService>((x, c) => x.Finally(c))
                .Rule;

            await BatchHandler.ExecuteAsync(rule, context);

            _errorServiceMock.Verify(x => x.OnError(It.IsAny<BatchContext>()), Times.Never);
            var batchContexts = _calls.Select(x => x.context).Distinct().ToArray();
            batchContexts[0].Message.Should().BeSameAs(batch.Messages[0]);
            batchContexts[0].ParentContext.Should().BeSameAs(context);
            batchContexts[0].Batch.Should().BeSameAs(batch);
            batchContexts[0].Response.Should().BeNull();
            batchContexts[0].Exception.Should().BeNull();
            batchContexts[0].DebugDatas.Should().BeNull();
            batchContexts[1].Message.Should().BeSameAs(batch.Messages[1]);
            batchContexts[1].ParentContext.Should().BeSameAs(context);
            batchContexts[1].Batch.Should().BeSameAs(batch);
            batchContexts[1].Response.Should().BeNull();
            batchContexts[1].Exception.Should().BeNull();
            batchContexts[1].DebugDatas.Should().BeNull();
            _calls.Should().HaveCount(4);
            _calls[0].method.Should().Be(nameof(IService.Handle));
            _calls[0].context.Should().BeSameAs(batchContexts[0]);
            _calls[1].method.Should().Be(nameof(IFinallyService.Finally));
            _calls[1].context.Should().BeSameAs(batchContexts[0]);
            _calls[2].method.Should().Be(nameof(IService.Handle));
            _calls[2].context.Should().BeSameAs(batchContexts[1]);
            _calls[3].method.Should().Be(nameof(IFinallyService.Finally));
            _calls[3].context.Should().BeSameAs(batchContexts[1]);
        }

        [Fact]
        public async Task ExecuteAsync_ruleWithCallOnErrorFinallyThrowingException_runCallThenOnErrorThenFinallyForEachMessageAsync()
        {
            var batch = new Batch<MyMessage>(new MyMessage(), new MyMessage());
            var context = new MessageContext(batch);
            var rule = new BatchRuleBuilder()
                .Call<IService>((x, c) => { x.Handle(c); throw new Exception($"Exception n°{c.Index}"); })
                .OnError<IErrorService>((x, c) => x.OnError(c))
                .Finally<IFinallyService>((x, c) => x.Finally(c))
                .Rule;

            Func<Task> action = () => BatchHandler.ExecuteAsync(rule, context);

            await action.Should().NotThrowAsync<Exception>();
            var batchContexts = _calls.Select(x => x.context).Distinct().ToArray();
            batchContexts[0].Message.Should().BeSameAs(batch.Messages[0]);
            batchContexts[0].Index.Should().Be(0);
            batchContexts[0].ParentContext.Should().BeSameAs(context);
            batchContexts[0].Response.Should().Be(batchContexts[0].Exception);
            batchContexts[0].Exception.Message.Should().Be("Exception n°0");
            batchContexts[0].Exception.Data[nameof(MessageBus)].Should().BeSameAs(batchContexts[0].DebugDatas);
            batchContexts[0].DebugDatas["Index"].Should().Be(0);
            batchContexts[1].Message.Should().BeSameAs(batch.Messages[1]);
            batchContexts[1].Index.Should().Be(1);
            batchContexts[1].ParentContext.Should().BeSameAs(context);
            batchContexts[1].Response.Should().Be(batchContexts[1].Exception);
            batchContexts[1].Exception.Message.Should().Be("Exception n°1");
            batchContexts[1].Exception.Data[nameof(MessageBus)].Should().BeSameAs(batchContexts[1].DebugDatas);
            batchContexts[1].DebugDatas["Index"].Should().Be(1);
            _calls.Should().HaveCount(6);
            _calls[0].method.Should().Be(nameof(IService.Handle));
            _calls[0].context.Should().BeSameAs(batchContexts[0]);
            _calls[1].method.Should().Be(nameof(IErrorService.OnError));
            _calls[1].context.Should().BeSameAs(batchContexts[0]);
            _calls[2].method.Should().Be(nameof(IFinallyService.Finally));
            _calls[2].context.Should().BeSameAs(batchContexts[0]);
            _calls[3].method.Should().Be(nameof(IService.Handle));
            _calls[3].context.Should().BeSameAs(batchContexts[1]);
            _calls[4].method.Should().Be(nameof(IErrorService.OnError));
            _calls[4].context.Should().BeSameAs(batchContexts[1]);
            _calls[5].method.Should().Be(nameof(IFinallyService.Finally));
            _calls[5].context.Should().BeSameAs(batchContexts[1]);
        }

        [Fact]
        public async Task ExecuteAsync_severalMessagesOneThrowingException_responseContainsResultOrException()
        {
            var batch = new Batch<MyMessage>(new MyMessage(), new MyMessage(), new MyMessage());
            var context = new MessageContext(batch);
            var cpt = 0;
            var rule = new BatchRuleBuilder()
                .Call(c => {
                    if (cpt++ == 1) throw new Exception("Something went wrong.");
                    else c.Response = 42;
                })
                .Rule;

            await BatchHandler.ExecuteAsync(rule, context);

            var responses = context.Response.Should().BeOfType<object[]>().Subject;
            responses[0].Should().Be(42);
            responses[1].Should().BeOfType<Exception>().Which.Message.Should().Be("Something went wrong.");
            responses[2].Should().Be(42);
        }

        [Fact]
        public async Task ExecuteAsync_actionThrowingException_logExceptions()
        {
            var batch = new Batch<MyMessage>(new MyMessage(), new MyMessage(), new MyMessage());
            var context = new MessageContext(batch);
            var cpt = 0;
            var rule = new BatchRuleBuilder()
                .Call(c => { if (cpt++ != 1) throw new Exception("Something went wrong."); })
                .Rule;

            await BatchHandler.ExecuteAsync(rule, context);

            _logEntries.Should().HaveCount(2);
            _logEntries[0].Message.Should().Contain("Something went wrong.");
            _logEntries[0].Level.Should().Be(LogLevel.Error);
            _logEntries[1].Message.Should().Contain("Something went wrong.");
            _logEntries[1].Level.Should().Be(LogLevel.Error);
        }

        [Fact]
        public async Task ExecuteAsync_onErrorSettingResponse_returnThisResponseInsteadOfException()
        {
            var batch = new Batch<MyMessage>(new MyMessage(), new MyMessage());
            var context = new MessageContext(batch);
            var cpt = 0;
            var rule = new BatchRuleBuilder()
                .Call(c => {
                    if (cpt++ == 0) throw new Exception("Something went wrong.");
                    else c.Response = 42;
                })
                .OnError(c => { c.Response = 0; })
                .Rule;

            await BatchHandler.ExecuteAsync(rule, context);

            var responses = context.Response.Should().BeOfType<object[]>().Subject;
            responses[0].Should().Be(0);
            responses[1].Should().Be(42);
        }

        //TODO tester qu'on stocke les réponses et les exceptions dans un tableau dans Response du ParentContext

        public static IEnumerable<object[]> Messages => new[]
        {
            new object[] { null },
            new object[] { new MyMessage() }
        };

        public interface IService
        {
            object Handle(BatchContext context);
        }

        public interface IErrorService
        {
            object OnError(BatchContext context);
        }

        public interface IFinallyService
        {
            object Finally(BatchContext context);
        }

        public interface IOtherService
        {
            void Other(BatchContext context);
        }

        public class MyMessage
        {
        }
    }
}
