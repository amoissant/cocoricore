using System;
using Xunit;
using FluentAssertions;
using System.Threading.Tasks;
using Autofac;
using CocoriCore.TestUtils;
using CocoriCore.Common;
using System.Collections.Generic;
using Microsoft.Extensions.Caching.Memory;
using Moq;
using Microsoft.Extensions.Logging;
using CocoriCore.Autofac;
using CocoriCore.Types.Extension;
using CocoriCore.Common.Diagnostic;

namespace CocoriCore.Messaging.Test
{
    public class MessageBusTest
    {
        private readonly Lazy<ILifetimeScope> _rootScope;
        private readonly ContainerBuilder _builder;
        private readonly MessageBusOptionsBuilder _messageBusBuilder;
        private readonly Mock<IConditionService> _conditionMock;
        private readonly Mock<IMessageHandler> _messageHanlderMock;
        private readonly Mock<IOtherService> _otherServiceMock;
        private readonly Mock<IErrorService> _errorServiceMock;
        private readonly Mock<IFinallyService> _finallyServiceMock;
        private readonly LogEntryCollection _logEntries;

        public MessageBusTest()
        {
            _builder = new ContainerBuilder();
            _rootScope = new Lazy<ILifetimeScope>(() => _builder.Build());
            _messageBusBuilder = new MessageBusOptionsBuilder();
            _logEntries = new LogEntryCollection();

            _builder.Register(c => _messageBusBuilder.Build()).SingleInstance();
            _builder.RegisterInstance(new MemoryCache(new MemoryCacheOptions())).As<IMemoryCache>();
            _builder.RegisterType<MessageBus>().AsSelf().As<IMessageBus>().InstancePerLifetimeScope();

            _builder.Register(c => new UnitOfWorkOptionsBuilder().Options).SingleInstance();
            _builder.RegisterType<AutofacUnitOfWorkFactory>().As<IUnitOfWorkFactory>().SingleInstance();
            _builder.RegisterType<AutofacUnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();

            _conditionMock = new Mock<IConditionService>();
            _messageHanlderMock = new Mock<IMessageHandler>();
            _otherServiceMock = new Mock<IOtherService>();
            _errorServiceMock = new Mock<IErrorService>();
            _finallyServiceMock = new Mock<IFinallyService>();

            _builder.RegisterInstance(_conditionMock.Object);
            _builder.RegisterInstance(_messageHanlderMock.Object);
            _builder.RegisterInstance(_errorServiceMock.Object);
            _builder.RegisterInstance(_otherServiceMock.Object);
            _builder.RegisterInstance(_finallyServiceMock.Object);

            _builder.ConfigureXunitLogger(logEntries: _logEntries);
        }

        private ILifetimeScope RootScope => _rootScope.Value;
        private IMessageBus MessageBus => RootScope.Resolve<IMessageBus>();

        [Theory]
        [MemberData(nameof(Messages))]
        public async Task ExecuteAsync_noRuleAndThrowExceptionWhenNoMatchingRule_throwsExceptionAsync(object message)
        {
            var context = new MessageContext(message);
            _messageBusBuilder.ThrowExceptionWhenNoRuleMatch = true;

            Func<Task> action = () => MessageBus.ExecuteAsync(context);

            await action.Should().ThrowAsync<MessageBusException>().WithMessage($"*matches condition*{message?.GetPrettyType(full: true)}*");
        }

        [Fact]
        public async Task ExecuteAsync_noRuleAndNotThrowExceptionWhenNoMatchingRule_runWithoutExceptionAsync()
        {
            var context = new MessageContext(new MyMessage());
            _messageBusBuilder.ThrowExceptionWhenNoRuleMatch = false;

            Func<Task> action = () => MessageBus.ExecuteAsync(context);

            await action.Should().NotThrowAsync<Exception>();
        }

        [Theory]
        [MemberData(nameof(Messages))]
        public async Task ExecuteAsync_noMatchingRuleAndNotThrowExceptionWhenNoMatchingRule_throwExceptionAsync(object message)
        {
            var context = new MessageContext(message);
            _messageBusBuilder
                .AddRule("rule")
                .When(c => false)
                .Call<IMessageHandler>((x, c) => x.Handle(c));

            Func<Task> action = () => MessageBus.ExecuteAsync(context);

            await action.Should().ThrowAsync<MessageBusException>().WithMessage($"*matches condition*{message?.GetPrettyType(full: true)}*");
        }

        [Fact]
        public async Task ExecuteAsync_noConditionNoExplicitRuleNameAndDisbaleErrorWhenNoRuleMatch_dontExecuteRuleButNoException()
        {
            var context = new MessageContext(null);
            _messageBusBuilder.ThrowExceptionWhenNoRuleMatch = false;
            _messageBusBuilder
                .AddRule("myRule")
                .Call<IMessageHandler>((x, c) => x.Handle(c));

            await MessageBus.ExecuteAsync(context);

            _messageHanlderMock.Verify(x => x.Handle(It.IsAny<MessageContext>()), Times.Never);
        }

        [Fact]
        public async Task ExecuteAsync_ruleWithoutConditionAndExecuteWithExplicitRuleName_executeRule()
        {
            var context = new MessageContext(null);
            _messageBusBuilder
                .AddRule("myRule")
                .Call<IMessageHandler>((x, c) => x.Handle(c));

            await MessageBus.ExecuteAsync(context, "myRule");

            _messageHanlderMock.Verify(x => x.Handle(It.IsAny<MessageContext>()), Times.Once);
        }

        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public async Task ExecuteAsync_oneRule_callActionWhenConditionIsTrue(bool condition)
        {
            var context = new MessageContext(null);
            _messageBusBuilder.ThrowExceptionWhenNoRuleMatch = false;
            _messageBusBuilder
                .AddRule("myRule")
                .When(c => condition)
                .Call<IMessageHandler>((x, c) => x.Handle(c));

            await MessageBus.ExecuteAsync(context);

            _messageHanlderMock.Verify(x => x.Handle(It.IsAny<MessageContext>()), condition ? Times.Once : Times.Never);
        }

        [Fact]
        public async Task ExecuteAsync_twoRulesWithTrueCondition_executeFirstDeclaredOne()
        {
            var context = new MessageContext(null);
            _messageBusBuilder
                .AddRule("myRule1")
                .When(c => true)
                .Call<IMessageHandler>((x, c) => x.Handle(c));
            _messageBusBuilder
                .AddRule("myRule2")
                .When(c => true)
                .Call<IOtherService>((x, c) => x.Other(c));

            await MessageBus.ExecuteAsync(context);

            _messageHanlderMock.Verify(x => x.Handle(context), Times.Once);
            _otherServiceMock.Verify(x => x.Other(It.IsAny<MessageContext>()), Times.Never);
        }

        [Fact]
        public async Task ExecuteAsync_oneRuleSeveralCalls_runctionsInRegistrationOrder()
        {
            var context = new MessageContext(null);
            var calls = new List<string>();
            _messageHanlderMock.Setup(x => x.Handle(It.IsAny<MessageContext>())).Callback(() => calls.Add(nameof(IMessageHandler.Handle)));
            _otherServiceMock.Setup(x => x.Other(It.IsAny<MessageContext>())).Callback(() => calls.Add(nameof(IOtherService.Other)));
            _messageBusBuilder
                .AddRule("myRule")
                .When(c => true)
                .Call<IMessageHandler>((x, c) => x.Handle(c))
                .Call<IOtherService>((x, c) => x.Other(c));

            await MessageBus.ExecuteAsync(context);

            _messageHanlderMock.Verify(x => x.Handle(It.IsAny<MessageContext>()), Times.Once);
            _otherServiceMock.Verify(x => x.Other(It.IsAny<MessageContext>()), Times.Once);
            calls.Should().ContainInOrder(new[] { nameof(IMessageHandler.Handle), nameof(IOtherService.Other) });
        }

        [Fact]
        public async Task ExecuteAsync_oneRuleSeveralOnError_runctionsInRegistrationOrderAsync()
        {
            var context = new MessageContext(null);
            var calls = new List<string>();
            _errorServiceMock.Setup(x => x.OnError(It.IsAny<MessageContext>())).Callback(() => calls.Add(nameof(IErrorService.OnError)));
            _otherServiceMock.Setup(x => x.Other(It.IsAny<MessageContext>())).Callback(() => calls.Add(nameof(IOtherService.Other)));
            _messageBusBuilder
                .AddRule("myRule")
                .When(c => true)
                .Call(c => throw new Exception("Something went wrong"))
                .OnError<IErrorService>((x, c) => x.OnError(c))
                .OnError<IOtherService>((x, c) => x.Other(c));

            Func<Task> action = () => MessageBus.ExecuteAsync(context);

            await action.Should().ThrowAsync<Exception>().WithMessage("Something went wrong");
            _errorServiceMock.Verify(x => x.OnError(It.IsAny<MessageContext>()), Times.Once);
            _otherServiceMock.Verify(x => x.Other(It.IsAny<MessageContext>()), Times.Once);
            calls.Should().ContainInOrder(new[] { nameof(IErrorService.OnError), nameof(IOtherService.Other) });
        }

        [Fact]
        public async Task ExecuteAsync_oneRuleSeveralFinally_runctionsInRegistrationOrder()
        {
            var context = new MessageContext(null);
            var calls = new List<string>();
            _finallyServiceMock.Setup(x => x.Finally(It.IsAny<MessageContext>())).Callback(() => calls.Add(nameof(IFinallyService.Finally)));
            _otherServiceMock.Setup(x => x.Other(It.IsAny<MessageContext>())).Callback(() => calls.Add(nameof(IOtherService.Other)));
            _messageBusBuilder
                .AddRule("myRule")
                .When(c => true)
                .Finally<IFinallyService>((x, c) => x.Finally(c))
                .Finally<IOtherService>((x, c) => x.Other(c));

            await MessageBus.ExecuteAsync(context);

            _finallyServiceMock.Verify(x => x.Finally(It.IsAny<MessageContext>()), Times.Once);
            _otherServiceMock.Verify(x => x.Other(It.IsAny<MessageContext>()), Times.Once);
            calls.Should().ContainInOrder(new[] { nameof(IFinallyService.Finally), nameof(IOtherService.Other) });
        }

        [Fact]
        public async Task ExecuteAsync_ruleWithCallOnErrorFinallyNoException_executeCallThenFinally()
        {
            var context = new MessageContext(null);
            var calls = new List<string>();
            _messageHanlderMock.Setup(x => x.Handle(It.IsAny<MessageContext>())).Callback(() => calls.Add(nameof(IMessageHandler.Handle)));
            _errorServiceMock.Setup(x => x.OnError(It.IsAny<MessageContext>())).Callback(() => calls.Add(nameof(IErrorService.OnError)));
            _finallyServiceMock.Setup(x => x.Finally(It.IsAny<MessageContext>())).Callback(() => calls.Add(nameof(IFinallyService.Finally)));
            _messageBusBuilder
                .AddRule("myRule")
                .When(c => true)
                .Call<IMessageHandler>((x, c) => x.Handle(c))
                .OnError<IErrorService>((x, c) => x.OnError(c))
                .Finally<IFinallyService>((x, c) => x.Finally(c));

            await MessageBus.ExecuteAsync(context);

            calls.Should().ContainInOrder(new[]
            {
                nameof(IMessageHandler.Handle),
                nameof(IFinallyService.Finally)
            })
            .And.NotContain(nameof(IErrorService.OnError));
        }

        [Fact]
        public async Task ExecuteAsync_ruleWithCallOnErrorFinallyThrowingException_runCallThenOnErrorThenFinallyAsync()
        {
            var context = new MessageContext(null);
            var calls = new List<string>();
            _messageHanlderMock.Setup(x => x.Handle(It.IsAny<MessageContext>())).Callback(() => calls.Add(nameof(IMessageHandler.Handle)));
            _errorServiceMock.Setup(x => x.OnError(It.IsAny<MessageContext>())).Callback(() => calls.Add(nameof(IErrorService.OnError)));
            _finallyServiceMock.Setup(x => x.Finally(It.IsAny<MessageContext>())).Callback(() => calls.Add(nameof(IFinallyService.Finally)));
            _messageBusBuilder
                .AddRule("myRule")
                .When(c => true)
                .Call<IMessageHandler>((x, c) => { x.Handle(c); throw new Exception("Something went wrong"); })
                .OnError<IErrorService>((x, c) => x.OnError(c))
                .Finally<IFinallyService>((x, c) => x.Finally(c));

            Func<Task> action = () => MessageBus.ExecuteAsync(context);

            await action.Should().ThrowAsync<Exception>().WithMessage("Something went wrong");
            calls.Should().ContainInOrder(new[] 
            { 
                nameof(IMessageHandler.Handle), 
                nameof(IErrorService.OnError), 
                nameof(IFinallyService.Finally) 
            });
        }

        [Theory]
        [MemberData(nameof(Messages))]
        public async Task ExecuteAsync_ruleSucessfullyExecuted_logMessageInformations(object message)
        {
            var context = new MessageContext(message);
            _messageBusBuilder
                .AddRule("myRule")
                .When(c => true)
                .Call<IMessageHandler>((x, c) => x.Handle(c));

            await MessageBus.ExecuteAsync(context);

            _logEntries.Should().HaveCount(1);
            _logEntries.Should().Contain(LogLevel.Information, $"myRule*{message?.GetPrettyType()}*success");
        }

        public static IEnumerable<object[]> Messages => new[] 
        {
            new object[] { null },
            new object[] { new MyMessage() }
        };

        [Theory]
        [MemberData(nameof(Messages))]
        public async Task ExecuteAsync_ruleExecutedWithError_logErrorAndAddExceptionIntoContextAsync(object message)
        {
            var context = new MessageContext(message);
            _messageHanlderMock.Setup(x => x.Handle(It.IsAny<MessageContext>())).Throws(new Exception("something went wrong"));
            _messageBusBuilder
                .AddRule("myRule")
                .When(c => true)
                .Call<IMessageHandler>((x, c) => x.Handle(c));

            Func<Task> action = () => MessageBus.ExecuteAsync(context);

            var assertion = await action.Should().ThrowAsync<Exception>();
            var exception = assertion.Which;
            exception.Message.Should().Be("something went wrong");
            exception.Data[nameof(MessageBus)].As<IDictionary<string, object>>()
                .Should().HaveCount(1)
                .And.Contain("MessageType", message?.GetPrettyType(full: true));
            exception.Data[nameof(MessageBus)].Should().Be(context.DebugDatas);
            context.Exception.Should().Be(exception);
            _logEntries.Should().HaveCount(1);
            _logEntries.Should().Contain(LogLevel.Error, $"myRule*{message?.GetPrettyType()}*error");
        }

        [Theory]
        [InlineData("rule2")]
        [InlineData("RULE2")]
        public async Task ExecuteAsync_withName_executeRuleHavingThisName(string ruleName)
        {
            var context = new MessageContext(null);
            _messageBusBuilder
                .AddRule("rule1")
                .When(c => true)
                .When<IConditionService>((x, c)=> x.IsConditionTrue(c))
                .Call<IMessageHandler>((x, c) => x.Handle(c));
            _messageBusBuilder
                .AddRule("rule2")
                .When(c => true)
                .Call<IOtherService>((x, c) => x.Other(c));

            await MessageBus.ExecuteAsync(context, ruleName);

            _otherServiceMock.Verify(x => x.Other(It.IsAny<MessageContext>()), Times.Once);
            _conditionMock.Verify(x => x.IsConditionTrue(It.IsAny<MessageContext>()), Times.Never);
            _messageHanlderMock.Verify(x => x.Handle(It.IsAny<MessageContext>()), Times.Never);
        }

        [Fact]
        public async Task ExecuteAsync_withInvalidRuleName_throwExceptionAsync()
        {
            var context = new MessageContext(null);
            _messageBusBuilder
                .AddRule("rule1")
                .When(c => true)
                .Call<IMessageHandler>((x, c) => x.Handle(c));
            _messageBusBuilder
                .AddRule("rule2")
                .When(c => true)
                .Call<IOtherService>((x, c) => x.Other(c));

            Func<Task> action = () => MessageBus.ExecuteAsync(context, "rule2zzzz");

            await action.Should().ThrowAsync<MessageBusException>().WithMessage("*rule2zzzz*rule1*rule2*");
        }

        [Fact]
        public async Task ExecuteAsync_directMessageNoRuleName_executeMatchingRuleAndReturnResponse()
        {
            var message = new MyMessage();
            _messageHanlderMock.Setup(x => x.Handle(It.IsAny<MessageContext>())).Callback<MessageContext>(c => c.Response = 42);
            _messageBusBuilder
                .AddRule("myRule")
                .When(c => true)
                .Call<IMessageHandler>((x, c) => x.Handle(c));

            var response = await MessageBus.ExecuteAsync(message);

            response.Should().Be(42);
            _messageHanlderMock.Verify(x => x.Handle(It.IsAny<MessageContext>()), Times.Once);
        }

        [Fact]
        public async Task ExecuteAsync_directMessageWithRuleName_executeNamedRuleAndReturnResponse()
        {
            var message = new MyMessage();
            _messageHanlderMock.Setup(x => x.Handle(It.IsAny<MessageContext>())).Callback<MessageContext>(c => c.Response = 42);
            _messageBusBuilder
                .AddRule("otherRule")
                .When(x => true)
                .Call<IOtherService>((x, c) => x.Other(c));
            _messageBusBuilder
                .AddRule("myRule")
                .Call<IMessageHandler>((x, c) => x.Handle(c));

            var response = await MessageBus.ExecuteAsync(message, "myRule");

            response.Should().Be(42);
            _messageHanlderMock.Verify(x => x.Handle(It.IsAny<MessageContext>()), Times.Once);
            _otherServiceMock.Verify(x => x.Other(It.IsAny<MessageContext>()), Times.Never);
        }

        public interface IConditionService
        {
            bool IsConditionTrue(MessageContext context);

            Task<bool> IsConditionTrueAsync(MessageContext context);
        }

        public interface IMessageHandler
        {
            object Handle(MessageContext context);

            Task<object> HandleAsync(MessageContext context);
        }

        public interface IOtherService
        {
            void Other(MessageContext context);
        }

        public interface IErrorService
        {
            void OnError(MessageContext context);
        }

        public interface IFinallyService
        {
            void Finally(MessageContext context);
        }

        public class MyMessage
        {
        }
    }
}
