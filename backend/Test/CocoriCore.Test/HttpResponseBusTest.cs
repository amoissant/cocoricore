using System;
using Xunit;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using FluentAssertions;
using System.Text;
using Newtonsoft.Json;
using Autofac;
using CocoriCore.TestUtils;
using CocoriCore.Common;

namespace CocoriCore.Test
{
    public class HttpResponseBusTest : BusTestBase
    {
        //TODO séparer les tests avec le defaultOptionBuilder de l'autre pour plus de lisibilité
        public HttpResponseBusTest()
        {
            ContainerBuilder.RegisterType<JsonSerializer>().AsSelf();
        }

        [Fact]
        public async Task UseDefaultResponseBus()
        {
            ContainerBuilder.RegisterType<HttpResponseWriterDefaultHandler>().AsSelf();
            var optionsBuilder = new DefaultHttpResponseWriterOptionsBuilder();
            var responseBus = new HttpResponseWriter(UnitOfWorkFactory, UnitOfWork, optionsBuilder.Options);
            var httpResponse = new FakeHttpResponse();

            await responseBus.WriteResponseAsync(new AResponse { name = "toto" }, httpResponse);

            httpResponse.StatusCode.Should().Be(200);
            httpResponse.ContentType.Should().Be("application/json; charset=utf-8");
            httpResponse.GetTextBody().Should().Be("{\"name\":\"toto\"}");
        }

        [Fact]
        public async Task UseCustomHandlerToWriteAResponse()
        {
            var responseWriter = new AResponseWriter();
            ContainerBuilder.RegisterInstance(responseWriter).AsSelf();
            var optionsBuilder = new HttpResponseWriterOptionsBuilder();
            optionsBuilder.For<AResponse>().Call<AResponseWriter>();
            var responseBus = new HttpResponseWriter(UnitOfWorkFactory, UnitOfWork, optionsBuilder.Options);

            await responseBus.WriteResponseAsync(new AResponse(), new FakeHttpResponse());

            responseWriter.MethodCall.Should().BeTrue();
        }

        [Fact]
        public async Task UseCustomHandlerToWriteIResponse()
        {
            var responseWriter = new AnIResponseWriter();
            ContainerBuilder.RegisterInstance(responseWriter).AsSelf();
            var optionsBuilder = new HttpResponseWriterOptionsBuilder();
            optionsBuilder.For<IResponse>().Call<AnIResponseWriter>();
            var responseBus = new HttpResponseWriter(UnitOfWorkFactory, UnitOfWork, optionsBuilder.Options);

            await responseBus.WriteResponseAsync(new AResponse(), new FakeHttpResponse());

            responseWriter.MethodCall.Should().BeTrue();
        }

        [Fact]
        public void ExceptionIfSuperTypeRuleBeforeSpecificRule()
        {
            var optionsBuilder = new HttpResponseWriterOptionsBuilder();
            optionsBuilder.For<IResponse>().Call<AnIResponseWriter>();
            optionsBuilder.For<AResponse>().Call<AResponseWriter>();

            HttpResponseWriterOptions option = null;
            Action action = () => option = optionsBuilder.Options;

            var exception = action.Should().Throw<ConfigurationException>().Which;
            exception.Message.Should().Be($"Rule for {typeof(IResponse).FullName} is less specific than "
                + $"rule for {typeof(AResponse).FullName} and should be defined after.");
        }

        [Fact]
        public void ExceptionIfTwoRulesForSameType()
        {
            var optionsBuilder = new DefaultHttpResponseWriterOptionsBuilder();
            optionsBuilder.For<IResponse>().Call<AnIResponseWriter>();
            Action action = () => optionsBuilder.For<IResponse>().Call<AResponseWriter>();

            var exception = action.Should().Throw<ConfigurationException>().Which;
            exception.Message.Should().Be($"Can't add a rule for type {typeof(IResponse).FullName}"
                        + $" because a previous one is already defined and this rule doesn't allow multiple handlers.");
        }

        [Fact]
        public void ExceptionIfTwoHandlersForASameRule()
        {
            var optionsBuilder = new HttpResponseWriterOptionsBuilder();
            optionsBuilder.For<object>().Call<HttpResponseWriterDefaultHandler>();
            Action action = () => optionsBuilder.For<IResponse>()
                .Call<AResponseWriter>()
                .Call<AnIResponseWriter>();

            var exception = action.Should().Throw<ConfigurationException>().Which;
            exception.Message.Should().Be($"Can't add handler [{typeof(AnIResponseWriter).FullName}]"
                        + $" because a previous one is already defined and this rule doesn't allow multiple handlers.");
        }

        [Fact]
        public async Task ExceptionIfNoRuleCallAsync()
        {
            var optionsBuilder = new HttpResponseWriterOptionsBuilder();
            var responseBus = new HttpResponseWriter(UnitOfWorkFactory, UnitOfWork, optionsBuilder.Options);
            var httpResponse = new FakeHttpResponse();

            Func<Task> action = async () => await responseBus.WriteResponseAsync(new AResponse { name = "toto" }, httpResponse);

            await action.Should().ThrowAsync<ConfigurationException>()
                .WithMessage($"No rule found for {typeof(AResponse).FullName}");
        }

        [Fact]
        public async Task RunFirstMatchingRuleAsync()
        {
            var aResponseWriter = new AResponseWriter();
            var iResponseWriter = new AnIResponseWriter();
            ContainerBuilder.RegisterInstance(aResponseWriter).AsSelf();
            ContainerBuilder.RegisterInstance(iResponseWriter).AsSelf();
            var optionsBuilder = new HttpResponseWriterOptionsBuilder();
            optionsBuilder.For<AResponse>().Call<AResponseWriter>();
            optionsBuilder.For<IResponse>().Call<AnIResponseWriter>();

            var responseBus = new HttpResponseWriter(UnitOfWorkFactory, UnitOfWork, optionsBuilder.Options);
            var httpResponse = new FakeHttpResponse();

            await responseBus.WriteResponseAsync(new AResponse { name = "toto" }, httpResponse);

            aResponseWriter.MethodCall.Should().BeTrue();
            iResponseWriter.MethodCall.Should().BeFalse();
        }

        [Fact]
        public void ExceptionIfTwoHandlerForAType()
        {
            var optionsBuilder = new DefaultHttpResponseWriterOptionsBuilder();
            Action action = () => optionsBuilder.For<AResponse>().Call<AnIResponseWriter>().Call<AResponseWriter>();

            var exception = action.Should().Throw<ConfigurationException>().Which;
            exception.Message.Should().Be($"Can't add handler [{typeof(AResponseWriter).FullName}] "
                + $"because a previous one is already defined and this rule doesn't allow multiple handlers.");
        }

        [Fact]
        public async Task UseResponseBusToWriteFileResponse()
        {
            ContainerBuilder.RegisterType<HttpResponseWriterFileHandler>().AsSelf();
            var optionsBuilder = new DefaultHttpResponseWriterOptionsBuilder();
            var responseBus = new HttpResponseWriter(UnitOfWorkFactory, UnitOfWork, optionsBuilder.Options);
            var fileResponse = new FileResponse()
            {
                FileName = "tôto.txt",
                MimeType = "text/plain",
                Content = Encoding.UTF8.GetBytes("ceci est un test")
            };
            var httpResponse = new FakeHttpResponse();

            await responseBus.WriteResponseAsync(fileResponse, httpResponse);

            httpResponse.ContentType.Should().Be("text/plain");
            httpResponse.Headers.Should().ContainKey("Content-Disposition");
            httpResponse.Headers["Content-Disposition"].Should().Contain("attachment; filename=\"toto.txt\"");
            httpResponse.GetTextBody().Should().Contain("ceci est un test");
        }

        [Fact]
        public async Task UseResponseBusToWriteVoidResponse()
        {
            ContainerBuilder.RegisterType<HttpResponseWriterDefaultHandler>().AsSelf();
            var optionsBuilder = new DefaultHttpResponseWriterOptionsBuilder();
            var responseBus = new HttpResponseWriter(UnitOfWorkFactory, UnitOfWork, optionsBuilder.Options);

            var httpResponse = new FakeHttpResponse();

            await responseBus.WriteResponseAsync(null, httpResponse);

            httpResponse.StatusCode.Should().Be(200);
            httpResponse.ContentType.Should().Be("application/json; charset=utf-8");
            httpResponse.GetTextBody().Should().Be("");
        }

        [Fact]
        public async Task ThrowExceptionIfExceptionDuringHandlerProcessAsync()
        {
            ContainerBuilder.RegisterType<AResponseWriter>().AsSelf();
            var optionsBuilder = new HttpResponseWriterOptionsBuilder();
            var exception = new Exception("something bad happened");
            optionsBuilder.For<AResponse>().Call<AResponseWriter>(c => throw exception);
            var responseBus = new HttpResponseWriter(UnitOfWorkFactory, UnitOfWork, optionsBuilder.Options);
            var httpResponse = new FakeHttpResponse();

            Func<Task> action = async () => await responseBus.WriteResponseAsync(new AResponse(), httpResponse);

            var assertion = await action.Should().ThrowAsync<Exception>();
            assertion.Which.Should().Be(exception);
        }

        public class AResponse : IResponse
        {
            public string name;
        }

        public class AResponseWriter : IHttpReponseWriterHandler
        {
            public bool MethodCall { get; set; }

            public AResponseWriter()
            {
                MethodCall = false;
            }

            public Task WriteResponseAsync(HttpResponseWriterContext context)
            {
                MethodCall = true;
                context.Response.Should().BeOfType<AResponse>();
                context.HttpResponse.Should().BeAssignableTo<HttpResponse>();
                return Task.CompletedTask;
            }
        }

        public class AnIResponseWriter : IHttpReponseWriterHandler
        {
            public bool MethodCall { get; set; }

            public AnIResponseWriter()
            {
                MethodCall = false;
            }

            public Task WriteResponseAsync(HttpResponseWriterContext context)
            {
                MethodCall = true;
                context.Response.Should().BeAssignableTo<IResponse>();
                context.HttpResponse.Should().BeAssignableTo<HttpResponse>();
                return Task.CompletedTask;
            }
        }

        public interface IResponse
        {
        }
    }
}