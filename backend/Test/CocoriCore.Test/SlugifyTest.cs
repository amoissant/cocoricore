﻿using CocoriCore.Common;
using FluentAssertions;
using Xunit;

namespace CocoriCore.Test
{
    public class SlugifyTest
    {
        [Theory]
        [InlineData(null, null)]
        [InlineData("Landes", "landes")]
        [InlineData("pyrénèês", "pyrenees")]
        [InlineData("pyrenees    atlantique", "pyrenees-atlantique")]
        [InlineData(" gers ", "gers")]
        [InlineData("côte d'armor", "cote-d-armor")]
        [InlineData("-landes-", "landes")]
        [InlineData("Landes 01", "landes-01")]
        public void SlugifyDefault(string str, string slug)
        {
            str.Slugify().Should().Be(slug);
        }

        [Theory]
        [InlineData(null, null)]
        [InlineData("Landes", "landes")]
        [InlineData("pyrénèês", "pyrenees")]
        [InlineData("pyrenees    atlantique", "pyrenees atlantique")]
        [InlineData(" gers ", "gers")]
        [InlineData("côte d'armor", "cote d armor")]
        [InlineData("-landes-", "landes")]
        [InlineData("Landes 01", "landes 01")]
        public void SlugifySpace(string str, string slug)
        {
            str.Slugify(" ").Should().Be(slug);
        }
    }
}
