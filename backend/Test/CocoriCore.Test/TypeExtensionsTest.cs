﻿using FluentAssertions;
using System;
using Xunit;
using CocoriCore.Types.Extension;

namespace CocoriCore.Test
{
    public class TypeExtensionsTest
    {
        [Theory]
        [InlineData(typeof(object))]
        [InlineData(typeof(AClass))]
        [InlineData(typeof(int?))]
        [InlineData(typeof(AnEnum?))]
        public void GetDefaultNullable(Type type)
        {
            var defaultValue = type.GetDefault();
            defaultValue.Should().BeNull();
        }

        [Fact]
        public void GetDefaultPrimitive()
        {
            var defaultValue = typeof(int).GetDefault();
            defaultValue.Should().NotBeNull().And.Be(0);
        }

        [Fact]
        public void GetDefaultStruct()
        {
            var defaultValue = typeof(DateTime).GetDefault();
            defaultValue.Should().NotBeNull().And.Be(default(DateTime));
        }

        [Fact]
        public void GetDefaultEnum()
        {
            var defaultValue = typeof(AnEnum).GetDefault();
            defaultValue.Should().NotBeNull().And.Be(AnEnum.None);
        }

        [Fact]
        public void GetPrettyType_null_returnFormattedNull()
        {
            ((Type)null).GetPrettyName().Should().Be("(null)");
        }

        [Fact]
        public void GetPrettyType_simpleClass_returnItsClassName()
        {
            typeof(AClass).GetPrettyName().Should().Be("AClass");
        }

        [Fact]
        public void GetPrettyType_genericClass_returnItsClassName()
        {
            typeof(AGeneric<AClass>).GetPrettyName().Should().Be("AGeneric<AClass>");
        }

        [Fact]
        public void GetPrettyType_genericClassOfGenericClass_returnItsClassName()
        {
            typeof(AGeneric<AGeneric<AClass>>).GetPrettyName().Should().Be("AGeneric<AGeneric<AClass>>");
        }

        [Fact]
        public void GetPrettyType_simpleClassFull_returnItsClassNameWithNameSpace()
        {
            typeof(AClass).GetPrettyName(full: true).Should().Be("CocoriCore.Test.TypeExtensionsTest+AClass");
        }

        [Fact]
        public void GetPrettyType_genericClassFull_returnItsClassNameWithNameSpace()
        {
            typeof(AGeneric<AClass>).GetPrettyName(full: true).Should().Be("CocoriCore.Test.TypeExtensionsTest+AGeneric<CocoriCore.Test.TypeExtensionsTest+AClass>");
        }

        [Fact]
        public void GetPrettyType_genericClassOfGenericClassFull_returnItsClassNameWithNameSpace()
        {
            typeof(AGeneric<AGeneric<AClass>>).GetPrettyName(full: true).Should().Be("CocoriCore.Test.TypeExtensionsTest+AGeneric<CocoriCore.Test.TypeExtensionsTest+AGeneric<CocoriCore.Test.TypeExtensionsTest+AClass>>");
        }

        class AClass { }

        class AGeneric<T> { }
        enum AnEnum { None = 0, A =1 }
    }
}
