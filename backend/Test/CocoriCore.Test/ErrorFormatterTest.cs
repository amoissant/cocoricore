﻿using System;
using Xunit;
using FluentAssertions;
using System.Threading.Tasks;
using CocoriCore.TestUtils;
using CocoriCore.Common;

namespace CocoriCore.Test
{
    public class ErrorFormatterTest
    {
        [Fact]
        public void ConstructErrorTraceFromException()
        {
            Action action = () => AMethodWithExceptionAndInnerException("fatal error", "low level error");
            var exception = action.CatchException();

            var formattedError = new ErrorFormatter().Format(exception);

            formattedError.Should().StartWith(
                $"CocoriCore.Test.ErrorFormatterTest+AnException: fatal error{Environment.NewLine}" +
                $"--- Inner message: low level error{Environment.NewLine}" +
                $"--- Datas:{Environment.NewLine}" +
                $"   {{{Environment.NewLine}" +
                $"     \"infos\": {{{Environment.NewLine}" +
                $"       \"id\": 1,{Environment.NewLine}" +
                $"       \"context\": \"a context message\"{Environment.NewLine}" +
                $"     }},{Environment.NewLine}" +
                $"     \"InnerDatas\": {{{Environment.NewLine}" +
                $"       \"errorCode\": 12{Environment.NewLine}" +
                $"     }}{Environment.NewLine}" +
                $"   }}");
            formattedError.Should().EndWith($"{exception.InnerException.StackTrace}{Environment.NewLine}" + 
                $"--- End of stack trace ---{Environment.NewLine}{exception.StackTrace}{Environment.NewLine}");
        }

        [Fact]
        public void ConstructErrorTraceFromExceptionWithoutStackNeitherDatas()
        {
            var formattedError = new ErrorFormatter().Format(new Exception("test"));

            formattedError.Should().Be($"System.Exception: test{Environment.NewLine}");                
        }

        [Fact]
        public void MessageInDataWhenErrorDuringSerialization()
        {
            var exception = new Exception("test");
            exception.Data["infos"] = 3; 
            var formattedError = new ErrorFormatter(x => throw new Exception("argh")).Format(exception);

            formattedError.Should().Be(
                $"System.Exception: test{Environment.NewLine}" +
                $"--- Datas:{Environment.NewLine}" + 
                $"   Unable to serialize exception's datas -> argh{Environment.NewLine}");
        }

        [Fact]
        public void ConstructErrorTraceFromAggregateException()
        {
            Action action = () => AMethodWithAggregateException("error 1", "error 2");
            var exception = action.CatchException();

            var formattedError = new ErrorFormatter().Format(exception);

            formattedError.Should().StartWith(
                $"System.AggregateException: One or more errors occurred. (error 1) (error 2){Environment.NewLine}" +
                $"--- Inner message: error 1{Environment.NewLine}" +
                $"--- Datas:{Environment.NewLine}" +
                $"   {{{Environment.NewLine}" +
                $"     \"InnerDatas\": {{{Environment.NewLine}" +
                $"       \"errorCode\": 12{Environment.NewLine}" +
                $"     }}{Environment.NewLine}" +
                $"   }}");
            formattedError.Should().EndWith($"{exception.InnerException.StackTrace}{Environment.NewLine}" +
                $"--- End of stack trace ---{Environment.NewLine}{exception.StackTrace}{Environment.NewLine}");
        }

        private void AMethodWithAggregateException(string message1, string message2)
        {
            Task action1 = Task.Run(() => AMethodWithInnerException(message1));
            Task action2 = Task.Run(() => AMethodWithInnerException(message2));
            Task.WaitAll(action1, action2);
        }

        private void AMethodWithExceptionAndInnerException(string message, string innerMessage)
        {
            try
            {
                AMethodWithInnerException(innerMessage);
            }
            catch (Exception e)
            {
                var exception = new AnException(message, e);
                exception.Data["infos"] = new { id = 1, context = "a context message" };
                throw exception;
            }
        }

        private void AMethodWithInnerException(string message)
        {
            var exception = new AnInnerException(message);
            exception.Data["errorCode"] = 12;
            throw exception;
        }

        class AnException : Exception
        {
            public AnException(string message) : base(message)
            {
            }

            public AnException(string message, Exception innerException)
                : base(message, innerException)
            {
            }
        }

        class AnInnerException : Exception
        {
            public AnInnerException(string message) : base(message)
            {
            }
        }
    }
}
