﻿using System;
using Xunit;
using FluentAssertions;
using System.Dynamic;
using CocoriCore.Dynamic.Extension;
using CocoriCore.TestUtils;
using System.Collections.Generic;
using CocoriCore.Common.Diagnostic;

namespace CocoriCore.Test
{
    public class DynamicUtilsTest
    {
        [Fact]
        public void ConvertExpandoToDynamicRecursively()
        {
            dynamic childObj = new ExpandoObject();
            childObj.BoolField = true;
            dynamic rootObj = new ExpandoObject();
            rootObj.IntField = 1;
            rootObj.StringField = "toto";
            rootObj.NullField = null;
            rootObj.CollectionField = new object[] { 1, new { Name = "2" } };
            rootObj.Child1 = childObj;
            rootObj.Child2 = new { DateTimeField = new DateTime(2020, 9, 6) };
            rootObj.EnumField = AnEnum.A;

            dynamic result = ((object)rootObj).ToDynamic();
            ((object)result.IntField).Should().Be(1);
            ((object)result.StringField).Should().Be("toto");
            ((object)result.NullField).Should().Be(null);
            ((object)result.CollectionField[0]).Should().Be(1);
            ((object)result.CollectionField[1]).Should().BeOfType<ExpandoObject>();
            ((object)result.CollectionField[1].Name).Should().Be("2");
            ((object)result.Child1.BoolField).Should().Be(true);
            ((object)result.Child2).Should().BeOfType<ExpandoObject>();
            ((object)result.Child2.DateTimeField).Should().Be(new DateTime(2020, 9, 6));
            ((object)result.EnumField).Should().Be(AnEnum.A);
        }

        [Fact]
        public void ConvertDictionaryToDynamicRecursively()
        {
            var rootObj = new Dictionary<string, object>();
            rootObj["IntField"] = 1;
            rootObj["StringField"] = "toto";
            rootObj["NullField"] = null;
            rootObj["CollectionField"] = new object[] { 1, new { Name = "2" } };
            rootObj["Child1"] = new { BoolField = true };
            rootObj["Child2"] = new { DateTimeField = new DateTime(2020, 9, 6) };
            rootObj["EnumField"] = AnEnum.A;

            dynamic result = ((object)rootObj).ToDynamic();
            ((object)result.IntField).Should().Be(1);
            ((object)result.StringField).Should().Be("toto");
            ((object)result.NullField).Should().Be(null);
            ((object)result.CollectionField[0]).Should().Be(1);
            ((object)result.CollectionField[1]).Should().BeOfType<ExpandoObject>();
            ((object)result.CollectionField[1].Name).Should().Be("2");
            ((object)result.Child1.BoolField).Should().Be(true);
            ((object)result.Child2).Should().BeOfType<ExpandoObject>();
            ((object)result.Child2.DateTimeField).Should().Be(new DateTime(2020, 9, 6));
            ((object)result.EnumField).Should().Be(AnEnum.A);
        }

        [Fact]
        public void ConvertUntypedDictionaryToDynamicRecursively()
        {
            var rootObj = new Exception().Data;
            rootObj["IntField"] = 1;
            rootObj["StringField"] = "toto";
            rootObj["NullField"] = null;
            rootObj["CollectionField"] = new object[] { 1, new { Name = "2" } };
            rootObj["Child1"] = new { BoolField = true };
            rootObj["Child2"] = new { DateTimeField = new DateTime(2020, 9, 6) };
            rootObj["EnumField"] = AnEnum.A;

            dynamic result = ((object)rootObj).ToDynamic();
            ((object)result.IntField).Should().Be(1);
            ((object)result.StringField).Should().Be("toto");
            ((object)result.NullField).Should().Be(null);
            ((object)result.CollectionField[0]).Should().Be(1);
            ((object)result.CollectionField[1]).Should().BeOfType<ExpandoObject>();
            ((object)result.CollectionField[1].Name).Should().Be("2");
            ((object)result.Child1.BoolField).Should().Be(true);
            ((object)result.Child2).Should().BeOfType<ExpandoObject>();
            ((object)result.Child2.DateTimeField).Should().Be(new DateTime(2020, 9, 6));
            ((object)result.EnumField).Should().Be(AnEnum.A);
        }

        [Fact]
        public void ConvertAnonymousClassToDynamicRecursively()
        {
            object rootObj = new
            {
                IntField = 1,
                StringField = "toto",
                NullField = (object)null,
                CollectionField = new object[] { 1, new { Name = "2" } },
                Child1 = new { BoolField = true },
                Child2 = new { DateTimeField = new DateTime(2020, 9, 6) },
                EnumField = AnEnum.B
            };

            dynamic result = rootObj.ToDynamic();
            ((object)result.IntField).Should().Be(1);
            ((object)result.StringField).Should().Be("toto");
            ((object)result.NullField).Should().Be(null);
            ((object)result.CollectionField[0]).Should().Be(1);
            ((object)result.CollectionField[1]).Should().BeOfType<ExpandoObject>();
            ((object)result.CollectionField[1].Name).Should().Be("2");
            ((object)result.Child1.BoolField).Should().Be(true);
            ((object)result.Child2).Should().BeOfType<ExpandoObject>();
            ((object)result.Child2.DateTimeField).Should().Be(new DateTime(2020, 9, 6));
            ((object)result.EnumField).Should().Be(AnEnum.B);
        }

        [Fact]
        public void ConvertStaticTypeToDynamicRecursively()
        {
            AClass rootObj = new AClass
            {
                IntField = 1,
                StringField = "toto",
                NullField = (object)null,
                CollectionField = new object[] { 1, new { Name = "2" } },
                Child1 = new AClass.AChild1 { BoolField = true },
                Child2 = new AClass.AChild2 { DateTimeField = new DateTime(2020, 9, 6) },
                EnumField = AnEnum.C
            };

            dynamic result = rootObj.ToDynamic();
            ((object)result.IntField).Should().Be(1);
            ((object)result.StringField).Should().Be("toto");
            ((object)result.NullField).Should().Be(null);
            ((object)result.CollectionField[0]).Should().Be(1);
            ((object)result.CollectionField[1]).Should().BeOfType<ExpandoObject>();
            ((object)result.CollectionField[1].Name).Should().Be("2");
            ((object)result.Child1.BoolField).Should().Be(true);
            ((object)result.Child2).Should().BeOfType<ExpandoObject>();
            ((object)result.Child2.DateTimeField).Should().Be(new DateTime(2020, 9, 6));
            ((object)result.EnumField).Should().Be(AnEnum.C);
        }

        [Fact]
        public void CheckStructureForExpandoObject()
        {
            dynamic childObj = new ExpandoObject();
            childObj.BoolField = true;
            dynamic rootObj = new ExpandoObject();
            rootObj.IntField = 1;
            rootObj.StringField = "toto";
            rootObj.NullField = null;
            rootObj.CollectionField = new object[] { 1, new { Name = "2" } };
            rootObj.Child1 = childObj;
            rootObj.Child2 = new { DateTimeField = new DateTime(2020, 9, 6) };

            ((object)rootObj).Should().MatchStructure(new
            {
                IntField = 1,
                StringField = "toto",
                NullField = (object)null,
                CollectionField = new object[] { 1, new { Name = "2" } },
                Child1 = new { BoolField = true },
                Child2 = new { DateTimeField = new DateTime(2020, 9, 6) },
            });

            Action action = () => ((object)rootObj).Should().MatchStructure(new
            {
                IntField = "1",
                StringField = "toto",
                NullField = (object)null,
                CollectionField = new object[] { 1, new { Name = "2" } },
                Child1 = new { BoolField = true },
                Child2 = new { DateTimeField = new DateTime(2020, 9, 6) },
            });
            action.Should().Throw<Exception>().Which.Message.Should().Contain("IntField");
        }

        [Fact]
        public void CheckStructureForAnonymousObject()
        {
            object rootObj = new
            {
                IntField = 1,
                StringField = "toto",
                NullField = (object)null,
                CollectionField = new object[] { 1, new { Name = "2" } },
                Child1 = new { BoolField = true },
                Child2 = new { DateTimeField = new DateTime(2020, 9, 6) },
            };

            rootObj.Should().MatchStructure(new
            {
                IntField = 1,
                StringField = "toto",
                NullField = (object)null,
                CollectionField = new object[] { 1, new { Name = "2" } },
                Child1 = new { BoolField = true },
                Child2 = new { DateTimeField = new DateTime(2020, 9, 6) },
            });

            Action action = () => rootObj.Should().MatchStructure(new
            {
                IntField = 1
            });
            action.Should().Throw<Exception>().Which.Message
                .Should().Contain("StringField")
                .And.Contain(rootObj.ToJson(indent: true));
        }

        [Fact]
        public void CheckStructureForStaticObject()
        {
            AClass rootObj = new AClass
            {
                IntField = 1,
                StringField = "toto",
                NullField = (object)null,
                CollectionField = new object[] { 1, new { Name = "2" } },
                Child1 = new AClass.AChild1 { BoolField = true },
                Child2 = new AClass.AChild2 { DateTimeField = new DateTime(2020, 9, 6) },
                EnumField = AnEnum.B
            };

            rootObj.Should().MatchStructure(new
            {
                IntField = 1,
                StringField = "toto",
                NullField = (object)null,
                CollectionField = new object[] { 1, new { Name = "2" } },
                Child1 = new { BoolField = true },
                Child2 = new { DateTimeField = new DateTime(2020, 9, 6) },
                EnumField = AnEnum.B
            });

            Action action = () => rootObj.Should().MatchStructure(new
            {
                IntField = 1,
                StringField = "toto",
                NullField = (object)null,
                CollectionField = new object[] { 1, new { Name = 2 } },//Name here is int instead of string
                Child1 = new { BoolField = true },
                Child2 = new { DateTimeField = new DateTime(2020, 9, 6) },
                EnumField = AnEnum.B
            });
            action.Should().Throw<Exception>().Which.Message.Should().Contain("Name");
        }

        [Fact]
        public void CheckStructureIgnoringMissingMembers()
        {
            object rootObj = new
            {
                IntField = 1,
                StringField = "toto",
                NullField = (object)null,
                CollectionField = new object[] { 1, new { Name = "2" } },
                Child1 = new { BoolField = true },
                Child2 = new { DateTimeField = new DateTime(2020, 9, 6) },
            };

            rootObj.Should().MatchStructure(new
            {
                IntField = 1,
                StringField = "toto",
            }, ignoreMissingMembers: true);

            Action action = () => rootObj.Should().MatchStructure(new
            {
                IntFieldzz = 1,
                StringField = "toto",
            }, ignoreMissingMembers: true);

            action.Should().Throw<Exception>().Which.Message.Should().Contain("IntFieldzz");
        }

        [Fact]
        public void CheckStructureIgnoringChildMissingMembers()
        {
            object rootObj = new
            {
                IntField = 1,
                StringField = "toto",
                NullField = (object)null,
                CollectionField = new object[] { 1, new { Name = "2", Age = 25 } },
                Child1 = new { BoolField = true, GuidField = Guid.Empty },
                Child2 = new { DateTimeField = new DateTime(2020, 9, 6) },
            };

            rootObj.Should().MatchStructure(new
            {
                CollectionField = new object[] { 1, new { Name = "2" } },
            }, ignoreMissingMembers: true);
            
            rootObj.Should().MatchStructure(new
            {
                Child1 = new { BoolField = true },
            }, ignoreMissingMembers: true);

            Action action1 = () => rootObj.Should().MatchStructure(new
            {
                CollectionField = new object[] { 1, new { Namezz = "2" } },
            }, ignoreMissingMembers: true);
            action1.Should().Throw<Exception>().Which.Message.Should().Contain("Namezz");

            Action action2 = () => rootObj.Should().MatchStructure(new
            {
                Child1 = new { GuidField = "1234" },
            }, ignoreMissingMembers: true);
            action2.Should().Throw<Exception>().Which.Message.Should().Contain("GuidField");
        }

        class AClass
        {
            public int IntField;
            public string StringField;
            public object NullField;
            public object[] CollectionField;
            public AChild1 Child1;
            public AChild2 Child2;
            public AnEnum EnumField;

            public class AChild1
            {
                public bool BoolField;
            }

            public class AChild2
            {
                public DateTime DateTimeField;
            }
        }

        enum AnEnum
        {
            A,
            B,
            C
        }
    }
}
