using CocoriCore.Common;
using CocoriCore.TestUtils;

namespace CocoriCore.Test
{
    public static class EntityWithDataSetsExtention
    {
        public static DataSets GetDataSets(this IEntity entity)
        {
            return (DataSets)((entity as IWithDataSet).DataSet);
        }
    }
}
