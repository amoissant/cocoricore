﻿using Autofac;
using CocoriCore.Common;
using CocoriCore.Logging;
using CocoriCore.Messaging;
using CocoriCore.Repository;
using CocoriCore.TestUtils;
using FluentAssertions;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace CocoriCore.Test
{
    public class HandlerExecutorTest : BusTestBase
    {
        private MessageBusOptionsBuilder _optionsBuilder;
        private IMessageBus _messageBus;
        private IMemoryCache _cache;

        public HandlerExecutorTest()
        {
            _cache = new MemoryCache(new MemoryCacheOptions());
            _optionsBuilder = new MessageBusOptionsBuilder();
            var assembly = this.GetType().Assembly;
            _optionsBuilder
                .AddRule("")
                .When(c => true)
                .Call<IHandlerExecutor>(async (x, c) => c.Response = await x.ExecuteAsync(c.Message));

            ContainerBuilder.Register(c => _optionsBuilder.Build()).AsSelf();
            ContainerBuilder.RegisterInstance(_cache).As<IMemoryCache>();
            ContainerBuilder.RegisterType<MessageBus>().As<IMessageBus>();
            ContainerBuilder.RegisterType<Anonymizer>().As<IAnonymizer>().SingleInstance();
            ContainerBuilder.RegisterType<HandlerExecutor>().As<IHandlerExecutor>()
                .WithParameter(new NamedParameter("assemblies", new[] { assembly }));
            ContainerBuilder.RegisterType<InMemoryEntityStore>().As<IInMemoryEntityStore>().SingleInstance();
            ContainerBuilder.RegisterType<InMemoryBaseRepository>().AsImplementedInterfaces().SingleInstance();
            ContainerBuilder.RegisterType<AppRepository>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
            ContainerBuilder.ConfigureXunitLogger();
        }

        public IMessageBus MessageBus
        {
            get
            {
                if (_messageBus == null)
                    _messageBus = RootScope.Resolve<IMessageBus>();
                return _messageBus;
            }
        }

        [Fact]
        public async Task ExecuteHandlerForQuery()
        {
            var nbCall = 0;
            Action action = () => nbCall++;
            ContainerBuilder.RegisterInstance(new AQueryHandler(() => nbCall++)).AsSelf();

            var response = await MessageBus.ExecuteAsync(new AQuery());

            nbCall.Should().Be(1);
            response.Should().Be(1234);
        }

        [Fact]
        public async Task ExecuteHandlerForVoidCommand()
        {
            var nbCall = 0;
            Action action = () => nbCall++;
            ContainerBuilder.RegisterInstance(new AVoidCommandHandler(() => nbCall++)).AsSelf();

            var response = await MessageBus.ExecuteAsync(new AVoidCommand());

            nbCall.Should().Be(1);
            response.Should().BeNull();
        }

        [Fact]
        public async Task ExecuteHandlerForCreateCommand()
        {
            var nbCall = 0;
            Action action = () => nbCall++;
            ContainerBuilder.RegisterInstance(new ACreateCommandHandler(() => nbCall++)).AsSelf();

            var id = await MessageBus.ExecuteAsync(new ACreateCommand());

            nbCall.Should().Be(1);
            id.Should().NotBe(default(Guid));
        }

        [Fact]
        public async Task ExceptionIfNoHandlerDefinedForMessageAsync()
        {
            Func<Task> action = async () => await MessageBus.ExecuteAsync(new AMessageWithoutHandler());

            await action.Should()
                .ThrowAsync<ConfigurationException>()
                .WithMessage($"No handler found for message type '{typeof(AMessageWithoutHandler).FullName}' in scanned assemblies :\n"
                    + $" - {typeof(AMessageWithoutHandler).Assembly.GetName().Name}\n");
        }

        [Fact]
        public async Task ThrowExceptionIfExceptionDuringHandlerProcessAsync()
        {
            var exception = new Exception("something bad happened");
            ContainerBuilder.RegisterInstance(new AVoidCommandHandler(() => throw exception)).AsSelf();

            Func<Task> action = async () => await MessageBus.ExecuteAsync(new AVoidCommand());

            var assertion = await action.Should().ThrowAsync<Exception>();
            assertion.Which.Should().Be(exception);
        }

        [Fact]
        public async Task CanHandleGenericCommandWithGenericCommandAndEntityParameter()
        {
            ContainerBuilder.RegisterGeneric(typeof(GenericCommandHandler1<,>)).AsSelf();

            var anEntityId = (Guid)await MessageBus.ExecuteAsync(new AGenericCommand1<AnEntity>());
            var anotherEntityId = (Guid)await MessageBus.ExecuteAsync(new AGenericCommand1<AnotherEntity>());

            var repository = _rootScope.Resolve<IRepository>();
            repository.ExistsAsync<AnEntity>(anEntityId).Result.Should().BeTrue();
            repository.ExistsAsync<AnotherEntity>(anotherEntityId).Result.Should().BeTrue();
        }

        [Fact]
        public async Task CanHandleGenericCommandWithGenericEntityParameter()
        {
            ContainerBuilder.RegisterGeneric(typeof(GenericCommandHandler2<>)).AsSelf();

            var anEntityId = (Guid)await MessageBus.ExecuteAsync(new AGenericCommand2<AnEntity>());
            var anotherEntityId = (Guid)await MessageBus.ExecuteAsync(new AGenericCommand2<AnotherEntity>());

            var repository = _rootScope.Resolve<IRepository>();
            repository.ExistsAsync<AnEntity>(anEntityId).Result.Should().BeTrue();
            repository.ExistsAsync<AnotherEntity>(anotherEntityId).Result.Should().BeTrue();
        }

        [Fact]
        public async Task ExceptionIfCantFindMatchingTypeFromMessageAsync()
        {
            ContainerBuilder.RegisterGeneric(typeof(GenericCommandHandler3<,>)).AsSelf();

            Func<Task> action = () => MessageBus.ExecuteAsync(new AGenericCommand3<AnEntity>());

            await action.Should().ThrowAsync<InvalidOperationException>()
                .WithMessage($"Unable to find matching type for parameter T of "
                + $"generic handler {typeof(GenericCommandHandler3<,>)} among [{typeof(AnEntity)}, {typeof(AGenericCommand3<AnEntity>)}]");
        }

       public class AppRepository : DecorableRepository, IRepository
        {
            public AppRepository(IBaseRepository baseRepository, IUnitOfWork unitOfWork, IEnumerable<Type> decoratorTypes)
                : base(baseRepository, unitOfWork, decoratorTypes)
            {
            }
        }
    }
}
