using System;
using Xunit;
using FluentAssertions;
using System.Linq.Expressions;
using CocoriCore.Reflection.Extension;
using CocoriCore.Expressions.Extension;

namespace CocoriCore.Test
{
    public class ReflectionTest
    {
        [Fact]
        public void GetMemberInfosForStructFieldExpression()
        {
            var o = new AClass();
            Expression<Func<AClass, object>> expr = x => x.anIntField;

            var memberInfos = expr.GetMemberInfo();
            memberInfos.Name.Should().Be("anIntField");
            memberInfos.GetMemberType().Should().Be(typeof(int));
        }

        [Fact]
        public void GetMemberInfosForObjectFieldExpression()
        {
            var o = new AClass();
            Expression<Func<AClass, object>> expr = x => x.aStringField;

            var memberInfos = expr.GetMemberInfo();
            memberInfos.Name.Should().Be("aStringField");
            memberInfos.GetMemberType().Should().Be(typeof(string));
        }

        [Fact]
        public void GetMemberInfosForStructPropertyExpression()
        {
            var o = new AClass();
            Expression<Func<AClass, object>> expr = x => x.anIntProperty;

            var memberInfos = expr.GetMemberInfo();
            memberInfos.Name.Should().Be("anIntProperty");
            memberInfos.GetMemberType().Should().Be(typeof(int));
        }

        [Fact]
        public void GetMemberInfosForObjectPropertyExpression()
        {
            var o = new AClass();
            Expression<Func<AClass, object>> expr = x => x.aStringProperty;

            var memberInfos = expr.GetMemberInfo();
            memberInfos.Name.Should().Be("aStringProperty");
            memberInfos.GetMemberType().Should().Be(typeof(string));
        }

        public class AClass
        {
            public int anIntField;
            public string aStringField;
            public int anIntProperty { get; set; }
            public string aStringProperty { get; set; }

            public AClass()
            {
            }
        }
    }
}
