using System.Linq;
using FluentAssertions;
using Xunit;
using CocoriCore.Common;

namespace CocoriCore.Test
{
    public class QueryableExtensionsTest
    {
        [Fact]
        public void SearchOverOneFieldOneKeyWordStartWith()
        {
            var collection = new[]
            {
                new SearchEntity { Text1 = "toto" },
                new SearchEntity { Text1 = "titi" }
            }
            .AsQueryable();

            var result = collection.Search("to", x => x.Text1);

            result.Should().ContainSingle().Which.Text1.Should().Be("toto");
        }

        [Fact]
        public void SearchOverOneFieldOneKeyWordEndsWith()
        {
            var collection = new[]
            {
                new SearchEntity { Text1 = "tito" },
                new SearchEntity { Text1 = "titi" }
            }
            .AsQueryable();

            var result = collection.Search("to", x => x.Text1);

            result.Should().ContainSingle().Which.Text1.Should().Be("tito");
        }

        [Fact]
        public void SearchOverOneFieldOneKeyWordContains()
        {
            var collection = new[]
            {
                new SearchEntity { Text1 = "tuto" },
                new SearchEntity { Text1 = "titi" }
            }
            .AsQueryable();

            var result = collection.Search("ut", x => x.Text1);

            result.Should().ContainSingle().Which.Text1.Should().Be("tuto");
        }

        [Theory]
        [InlineData("o", "toto", "titi")]
        [InlineData("O", "toto", "titi")]
        [InlineData("o", "TOTO", "TITI")]
        [InlineData("O", "TOTO", "TITI")]
        public void SearchOverOneFieldOneKeyWord(string keyword, string text1, string text2)
        {
            var collection = new[]
            {
                new SearchEntity { Text1 = text1 },
                new SearchEntity { Text1 = text2 },
            }
            .AsQueryable();

            var result = collection.Search(keyword, x => x.Text1);

            result.Select(x => x.Text1).Should().Contain(text1)
                .And.NotContain(text2);
        }

        [Theory]
        [InlineData("o u", "toto", "titi", "tutu")]
        [InlineData("O U", "toto", "titi", "tutu")]
        [InlineData("o u", "TOTO", "TITI", "TUTU")]
        [InlineData("O U", "TOTO", "TITI", "TUTU")]
        public void SearchOverOneFieldManyKeyWords(string keyword, string text1, string text2, string text3)
        {
            var collection = new[]
            {
                new SearchEntity { Text1 = text1 },
                new SearchEntity { Text1 = text2 },
                new SearchEntity { Text1 = text3 },
            }
            .AsQueryable();

            var result = collection.Search(keyword, x => x.Text1);

            result.Select(x => x.Text1).Should().BeEquivalentTo(text1, text3)
                .And.NotContain(text2);
        }

        [Fact]
        public void SearchOverManyFieldsOneKeyWord()
        {
            var collection = new[]
            {
                new SearchEntity { Text1 = "toto", Text2 = "tata" },
                new SearchEntity { Text1 = "titi", Text2 = "tuto" },
                new SearchEntity { Text1 = "tutu", Text2 = "tata" },
            }
            .AsQueryable();

            var result = collection.Search("to", x => x.Text1, x => x.Text2);

            result.Select(x => x.Text1).Should().BeEquivalentTo("toto", "titi")
                .And.NotContain("tutu");
        }

        [Fact]
        public void SearchOverManyFieldsManyKeyWords()
        {
            var collection = new[]
            {
                new SearchEntity { Text1 = "toto", Text2 = "a" },
                new SearchEntity { Text1 = "titi", Text2 = "b" },
                new SearchEntity { Text1 = "tutu", Text2 = "c" },
            }
            .AsQueryable();

            var result = collection.Search("o b", x => x.Text1, x => x.Text2);

            result.Select(x => x.Text1).Should().BeEquivalentTo("toto", "titi")
                .And.NotContain("tutu");
        }

        [Fact]
        public void SearchOverManyFieldsManyKeyWordsBehaviorAnd()
        {
            var collection = new[]
            {
                new SearchEntity { Text1 = "toto", Text2 = "a" },
                new SearchEntity { Text1 = "moto", Text2 = "b" },
                new SearchEntity { Text1 = "titi", Text2 = "b" },
                new SearchEntity { Text1 = "tutu", Text2 = "c" },
            }
            .AsQueryable();

            var result = collection.Search("o b", SearchBehavior.MultiKeywordsAnd, x => x.Text1, x => x.Text2);

            result.Select(x => x.Text1).Should().ContainSingle().Which.Should().Be("moto")
                .And.NotContain("toto")
                .And.NotContain("titi")
                .And.NotContain("tutu");
        }
    }

    public class SearchEntity
    {
        public string Text1;
        public string Text2;
    }
}