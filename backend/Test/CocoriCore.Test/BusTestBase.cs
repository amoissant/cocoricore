using System;
using Autofac;
using CocoriCore.Autofac;
using CocoriCore.Common;

namespace CocoriCore.Test
{
    public class BusTestBase
    {
        protected ContainerBuilder ContainerBuilder { get; }
        protected UnitOfWorkOptionsBuilder _unitOfWorkBuilder;
        protected ILifetimeScope _rootScope;

        public BusTestBase()
        {
            ContainerBuilder = new ContainerBuilder();
            _unitOfWorkBuilder = new UnitOfWorkOptionsBuilder();

            ContainerBuilder.Register<Func<ILifetimeScope>>(c => RootScope.BeginLifetimeScope).AsSelf();
            ContainerBuilder.Register(c => _unitOfWorkBuilder.Options).As<UnitOfWorkOptions>().SingleInstance();
            ContainerBuilder.RegisterType<AutofacUnitOfWorkFactory>().As<IUnitOfWorkFactory>().SingleInstance();
            ContainerBuilder.RegisterType<AutofacUnitOfWork>()
                .As<IUnitOfWork>()
                .InstancePerLifetimeScope();
        }

        protected ILifetimeScope RootScope
        {
            get
            {
                if (_rootScope == null)
                {
                    _rootScope = ContainerBuilder.Build();
                }
                return _rootScope;
            }
        }

        protected IUnitOfWork UnitOfWork => RootScope.Resolve<IUnitOfWork>();

        protected IUnitOfWorkFactory UnitOfWorkFactory => RootScope.Resolve<IUnitOfWorkFactory>();
    }
}