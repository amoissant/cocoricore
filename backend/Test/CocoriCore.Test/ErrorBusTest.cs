using System;
using CocoriCore.Common;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using FluentAssertions;
using Xunit;

namespace CocoriCore.Test
{
    public class ErrorBusTest : BusTestBase
    {
        private Exception _unexpectedException;
        private ErrorBusOptionsBuilder _optionsBuilder;

        public ErrorBusTest()
        {
            ContainerBuilder.RegisterType<ErrorAction>().AsSelf().SingleInstance();
            _optionsBuilder = new ErrorBusOptionsBuilder()
                .SetFireAndForgetExceptionHandler<ErrorAction>((h, e) =>
                {
                    _unexpectedException = e;
                    return Task.CompletedTask;
                });
            _optionsBuilder.SetUnexpectedExceptionHandler((ue, e) => Task.CompletedTask);//just for test purpose, never do that
        }

        [Fact]
        public async Task CallHandlerFirstSyntaxForAnException()
        {
            var handlerCalled = false;
            Exception passedException = null;
            var errorAction = new ErrorAction(e => { handlerCalled = true; passedException = e; });
            ContainerBuilder.RegisterInstance(errorAction);
            var exception = new AnException();
            _optionsBuilder.For<AnException>().Call<ErrorAction>(a => a.Handle);
            var errorBus = new ErrorBus(UnitOfWorkFactory, UnitOfWork, _optionsBuilder.Options);

            await errorBus.HandleAsync(exception);

            _unexpectedException.Should().BeNull();
            handlerCalled.Should().BeTrue();
            passedException.Should().Be(exception);
        }

        [Fact]
        public async Task CallHandlerSecondSyntaxForAnException()
        {
            var handlerCalled = false;
            Exception passedException = null;
            var exception = new AnException();
            _optionsBuilder.For<AnException>().Call<ErrorAction>((a, e) => { handlerCalled = true; passedException = e; return Task.CompletedTask; });
            var errorBus = new ErrorBus(UnitOfWorkFactory, UnitOfWork, _optionsBuilder.Options);

            await errorBus.HandleAsync(exception);

            _unexpectedException.Should().BeNull();
            handlerCalled.Should().BeTrue();
            passedException.Should().Be(exception);
        }

        [Fact]
        public async Task CanHaveTwoHandlersForAnException()
        {
            var handler1Called = false;
            var handler2Called = false;
            var exception = new AnException();
            _optionsBuilder
                .For<AnException>()
                .Call<ErrorAction>((a, e) => { handler1Called = true; return Task.CompletedTask; });
            _optionsBuilder
                .For<AnException>()
                .Call<ErrorAction>((a, e) => { handler2Called = true; return Task.CompletedTask; });
            var errorBus = new ErrorBus(UnitOfWorkFactory, UnitOfWork, _optionsBuilder.Options);

            await errorBus.HandleAsync(exception);

            _unexpectedException.Should().BeNull();
            handler1Called.Should().BeTrue();
            handler2Called.Should().BeTrue();
        }

        [Fact]
        public async Task StopPropagationAfterFirstHandler()
        {
            var exception = new AnException();
            var handler1Called = false;
            var handler2Called = false;
            _optionsBuilder
                .For<AnException>()
                .Call<ErrorAction>((a, e) => { handler1Called = true; return Task.CompletedTask; })
                .StopPropagation();
            _optionsBuilder
                .For<AnException>()
                .Call<ErrorAction>((a, e) => { handler2Called = true; return Task.CompletedTask; });
            var errorBus = new ErrorBus(UnitOfWorkFactory, UnitOfWork, _optionsBuilder.Options);

            await errorBus.HandleAsync(exception);

            _unexpectedException.Should().BeNull();
            handler1Called.Should().BeTrue();
            handler2Called.Should().BeFalse();
        }

        [Fact]
        public async Task UseHandlerDeclarationOrderToStopPropagation()
        {
            var exception = new AnException();
            var handler1Called = false;
            var handler2Called = false;
            var handler3Called = false;
            _optionsBuilder
                .For<AnException>()
                .Call<ErrorAction>((a, e) => { handler1Called = true; return Task.CompletedTask; });
            _optionsBuilder
                .For<AnException>()
                .Call<ErrorAction>((a, e) => { handler2Called = true; return Task.CompletedTask; })
                .StopPropagation();
            _optionsBuilder
                .For<AnException>()
                .Call<ErrorAction>((a, e) => { handler3Called = true; return Task.CompletedTask; });
            var errorBus = new ErrorBus(UnitOfWorkFactory, UnitOfWork, _optionsBuilder.Options);

            await errorBus.HandleAsync(exception);

            _unexpectedException.Should().BeNull();
            handler1Called.Should().BeTrue();
            handler2Called.Should().BeTrue();
            handler3Called.Should().BeFalse();
        }


        [Fact]
        public async Task StopPropagationAfterAllChainedHandlers()
        {
            var exception = new AnException();
            DateTime? handler1CallTime = null;
            DateTime? handler2CallTime = null;
            DateTime? handler3CallTime = null;
            DateTime? handler4CallTime = null;
            DateTime? handler5CallTime = null;
            _optionsBuilder
                .For<AnException>()
                .Call<ErrorAction>((a, e) => { handler1CallTime = DateTime.Now; return Task.CompletedTask; });
            _optionsBuilder
                .For<AnException>()
                .Call<ErrorAction>((a, e) => { handler2CallTime = DateTime.Now; return Task.CompletedTask; })
                .Call<ErrorAction>((a, e) => { handler3CallTime = DateTime.Now; return Task.CompletedTask; })
                .StopPropagation()
                .Call<ErrorAction>((a, e) => { handler4CallTime = DateTime.Now; return Task.CompletedTask; });
            _optionsBuilder
                .For<AnException>()
                .Call<ErrorAction>((a, e) => { handler5CallTime = DateTime.Now; return Task.CompletedTask; });

            var errorBus = new ErrorBus(UnitOfWorkFactory, UnitOfWork, _optionsBuilder.Options);

            await errorBus.HandleAsync(exception);

            _unexpectedException.Should().BeNull();
            handler1CallTime.Should().NotBeNull();
            handler2CallTime.Should().NotBeNull();
            handler3CallTime.Should().NotBeNull();
            handler4CallTime.Should().NotBeNull();
            handler5CallTime.Should().BeNull();

            handler1CallTime.Should().BeBefore(handler2CallTime.Value);
            handler2CallTime.Should().BeBefore(handler3CallTime.Value);
            handler3CallTime.Should().BeBefore(handler4CallTime.Value);
        }

        [Fact]
        public async Task StopPropagationAfterConcreteType()
        {
            var exception = new AnException();
            var handler1Called = false;
            var handler2Called = false;
            _optionsBuilder
                .For<AnException>()
                .Call<ErrorAction>((a, e) => { handler1Called = true; return Task.CompletedTask; })
                .StopPropagation();
            _optionsBuilder
                .For<Exception>()
                .Call<ErrorAction>((a, e) => { handler2Called = true; return Task.CompletedTask; });
            var errorBus = new ErrorBus(UnitOfWorkFactory, UnitOfWork, _optionsBuilder.Options);

            await errorBus.HandleAsync(exception);

            _unexpectedException.Should().BeNull();
            handler1Called.Should().BeTrue();
            handler2Called.Should().BeFalse();
        }

        [Fact]
        public async Task FireAndForgetApplyOnDeclarationChain()
        {
            var exception = new AnException();
            var handler1Called = false;
            var handler2Called = false;
            var handler3Called = false;
            _optionsBuilder
                .For<AnException>()
                .Call<ErrorAction>(async (a, e) => { await Task.Delay(50); handler1Called = true; })
                .FireAndForget()
                .Call<ErrorAction>(async (a, e) => { await Task.Delay(50); handler2Called = true; });
            _optionsBuilder
                .For<Exception>()
                .Call<ErrorAction>((a, e) => { handler3Called = true; return Task.CompletedTask; });
            var errorBus = new ErrorBus(UnitOfWorkFactory, UnitOfWork, _optionsBuilder.Options);

            await errorBus.HandleAsync(exception);

            _unexpectedException.Should().BeNull();
            handler1Called.Should().BeFalse();
            handler2Called.Should().BeFalse();
            handler3Called.Should().BeTrue();

            await Task.WhenAll(errorBus.FireAndForgetTasks.ToArray());

            _unexpectedException.Should().BeNull();
            handler1Called.Should().BeTrue();
            handler2Called.Should().BeTrue();
        }

        [Fact(Skip="Remettre une fois clarifi� les donn�es � mettre dans les datas de l'exception")]
        public async Task CallUnexpectedExceptionHandlerWhenExceptionInFireAndForget()
        {
            var exception = new Exception("my exception");
            var unexpectedHandlerCalled = false;
            Exception unexpectedException = new Exception("unexpected");
            Exception passedException = null;
            Exception passedContext = null;
            _optionsBuilder.SetFireAndForgetExceptionHandler<ErrorAction>((h, e) =>
            {
                unexpectedHandlerCalled = true;
                passedException = e;
                //passedContext = (Exception)e.GetBusContext();
                return Task.CompletedTask;
            });
            _optionsBuilder
                .For<Exception>()
                .Call<ErrorAction>(async (a, e) => { await Task.Delay(50); throw unexpectedException; })
                .FireAndForget();
            var errorBus = new ErrorBus(UnitOfWorkFactory, UnitOfWork, _optionsBuilder.Options);

            await errorBus.HandleAsync(exception);

            unexpectedHandlerCalled.Should().BeFalse();

            await Task.WhenAll(errorBus.FireAndForgetTasks.ToArray());

            unexpectedHandlerCalled.Should().BeTrue();
            passedException.Should().Be(unexpectedException);
            var exceptionId = passedContext.GetId();
            exceptionId.Should().NotBeEmpty();
            passedContext.Should().Be(exception);
        }

        [Fact]
        public async Task CanChainTwoHandlersForAnException()
        {
            var exception = new AnException();
            var handler1Called = false;
            var handler2Called = false;
            _optionsBuilder.For<AnException>()
            .Call<ErrorAction>((a, e) => { handler1Called = true; return Task.CompletedTask; })
            .Call<ErrorAction>((a, e) => { handler2Called = true; return Task.CompletedTask; });
            var errorBus = new ErrorBus(UnitOfWorkFactory, UnitOfWork, _optionsBuilder.Options);

            await errorBus.HandleAsync(exception);

            _unexpectedException.Should().BeNull();
            handler1Called.Should().BeTrue();
            handler2Called.Should().BeTrue();
        }

        [Fact]
        public async Task CallDefaultHandlerIfNoRuleDefinedForException()
        {
            var exception = new AnException();
            _optionsBuilder.For<Exception>().Call<ErrorAction>(a => a.Handle);
            var errorBus = new ErrorBus(UnitOfWorkFactory, UnitOfWork, _optionsBuilder.Options);

            await errorBus.HandleAsync(exception);

            _unexpectedException.Should().BeNull();
            RootScope.Resolve<ErrorAction>().CallCount.Should().Be(1);
        }

        [Fact]
        public void ThrowExceptionIfNoFireAndForgetExceptiontHandlerDefinedAndFireAndForgetRule()
        {
            _optionsBuilder.For<Exception>().Call<ErrorAction>().FireAndForget();
            _optionsBuilder.Options.FireAndForgetExceptionHandler = null;
            Action action = () => new ErrorBus(UnitOfWorkFactory, UnitOfWork, _optionsBuilder.Options);

            action.Should().Throw<ConfigurationException>()
            .Which.Message.Should().Be("No fire and forget exception handler defined and at least one rule"
                + $" is configured with .{nameof(ErrorBusRule.FireAndForget)}()."
                + $" Please use {nameof(ErrorBusOptionsBuilder.SetFireAndForgetExceptionHandler)}() to define one.");
        }

        [Fact]
        public async Task CallHandlersDefinedForConcreteTypeAbstractTypeBaseTypeOrInterface()
        {
            var exception = new AnException();
            var handler1Called = false;
            var handler2Called = false;
            var handler3Called = false;
            var handler4Called = false;
            _optionsBuilder
                .For<AnException>()
                .Call<ErrorAction>((a, e) => { handler1Called = true; return Task.CompletedTask; });
            _optionsBuilder
                .For<AnAbstractException>()
                .Call<ErrorAction>((a, e) => { handler3Called = true; return Task.CompletedTask; });
            _optionsBuilder
                .For<Exception>()
                .Call<ErrorAction>((a, e) => { handler2Called = true; return Task.CompletedTask; });
            _optionsBuilder
                .For<IException>()
                .Call<ErrorAction>((a, e) => { handler4Called = true; return Task.CompletedTask; });

            var errorBus = new ErrorBus(UnitOfWorkFactory, UnitOfWork, _optionsBuilder.Options);

            await errorBus.HandleAsync(exception);

            _unexpectedException.Should().BeNull();
            handler1Called.Should().BeTrue();
            handler2Called.Should().BeTrue();
            handler3Called.Should().BeTrue();
            handler4Called.Should().BeTrue();
        }

        [Fact]
        public async Task CallUnexpectedExceptionHandlerIfExceptionDuringHandler()
        {
            var exception = new Exception("something bad happened");
            var unexpectedException = new Exception("fatal error");
            var unexpectedExceptionHandlerCalled = false;
            _optionsBuilder.SetUnexpectedExceptionHandler((ue, e) =>
            {
                unexpectedExceptionHandlerCalled = true;
                ue.Should().Be(unexpectedException);
                e.Should().Be(exception);
                return Task.CompletedTask;
            });
            _optionsBuilder.For<Exception>().Call<ErrorAction>(a => throw unexpectedException);

            var errorBus = new ErrorBus(UnitOfWorkFactory, UnitOfWork, _optionsBuilder.Options);

            await errorBus.HandleAsync(exception);

            unexpectedExceptionHandlerCalled.Should().BeTrue();
        }

        [Fact]
        public void ThrowExceptionIfNoUnexpectedExceptiontHandlerDefined()
        {
            _optionsBuilder.Options.UnexceptedExceptionHandler = null;
            Action action = () => new ErrorBus(UnitOfWorkFactory, UnitOfWork, _optionsBuilder.Options);

            action.Should().Throw<ConfigurationException>()
            .Which.Message.Should().Be("No action defined in case of exception during error handling."
                + $" Please use {nameof(ErrorBusOptionsBuilder.SetUnexpectedExceptionHandler)}() to define one.");
        }

        class AnException : AnAbstractException, IException
        {
        }

        abstract class AnAbstractException : Exception
        {
        }

        interface IException { }

        class ErrorAction : IErrorHandler<Exception>
        {
            private Func<Exception, Task> _action;

            private int _callCount;
            private Exception _exception;

            public ErrorAction(Action<Exception> action)
            {
                _action = e => { action(e); return Task.CompletedTask; };
            }

            public ErrorAction()
            {
                _action = e => Task.CompletedTask;
            }

            public Func<Exception, Task> Action
            {
                get => new NotConfiguredException().IfNull(_action);
                set => _action = value;
            }

            public int CallCount => _callCount;
            public Exception Exception => _exception;

            public async Task Handle(Exception exception)
            {
                await Action(exception);
                _exception = exception;
                _callCount++;
            }

            public Task HandlerErrorAsync(Exception exception)
            {
                _exception = exception;
                _callCount++;
                return Task.CompletedTask;
            }
        }
    }
}