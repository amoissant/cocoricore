using System;
using CocoriCore.Common;
using FluentAssertions;
using Xunit;

namespace CocoriCore.Test
{
    public class FeatureOptionsTest
    {
        [Fact]
        public void DefineAndRetrieveFeaturesFromMessageType()
        {
            var options = new FeatureOptions();

            options.DefineFeature(Feature.Feature1)
                .Add<Command1>()
                .Add<Query1>();
            options.DefineFeature(Feature.Feature2)
                .Add<Command3>()
                .Add<Query2>();

            options.GetFeatures(typeof(Command1)).Should().ContainSingle().Which.Should().Be(Feature.Feature1);
            options.GetFeatures(typeof(Query2)).Should().ContainSingle().Which.Should().Be(Feature.Feature2);
        }

        [Fact]
        public void RetrieveFeaturesFromMessageSubType()
        {
            var options = new FeatureOptions();

            options.DefineFeature(Feature.Feature1)
                .Add<Command1>();

            options.GetFeatures(typeof(Command1SubType)).Should().ContainSingle().Which.Should().Be(Feature.Feature1);
        }

        [Fact]
        public void ExceptionIfNoFeatureForMessageTypeByDefault()
        {
            var options = new FeatureOptions();

            Action action = () => options.GetFeatures(typeof(Command1));

            action.Should().Throw<ConfigurationException>()
                .WithMessage($"No feature defined for message of type {typeof(Command1)} neither it's super types."
                + $"\\r\\nYou can disable this behavior using {nameof(FeatureOptions)}.{nameof(FeatureOptions.ErrorIfNoFeatureForMessage)}().");
        }

        [Fact]
        public void NoExceptionIfDisableErrorWhenFeatureForMessageType()
        {
            var options = new FeatureOptions().ErrorIfNoFeatureForMessage(false);

            var features = options.GetFeatures(typeof(Command1));

            features.Should().BeEmpty();
        }

        [Fact]
        public void MessageTypeCanBelongSeveralFeatures()
        {
            var options = new FeatureOptions();
            options
                .DefineFeature(Feature.Feature1)
                .Add<Command1>();

            options
                .DefineFeature(Feature.Feature2)
                .Add<Command1>();

            options.GetFeatures(typeof(Command1)).Should().HaveCount(2)
                .And.BeEquivalentTo(new[] { Feature.Feature1, Feature.Feature2 });
        }

        [Fact]
        public void ExceptionIfTwoFeaturesWithSameKey()
        {
            var options = new FeatureOptions();
            options
                .DefineFeature(Feature.Feature1)
                .Add<Command1>();

            Action action = () => options.DefineFeature(Feature.Feature1);

            action.Should().Throw<ConfigurationException>()
                .WithMessage($"A feature with the same key {Feature.Feature1} already exists.");
        }

        [Fact]
        public void CanDefineFeatureIncludingMessagesFromAnotherFeature()
        {
            var options = new FeatureOptions();
            options
                .DefineFeature(Feature.Feature1)
                .Add<Command1>()
                .Add<Command2>();

            options
                .DefineFeature(Feature.Feature2)
                .Include(Feature.Feature1)
                .Add<Command3>();

            options.GetFeatures(typeof(Command1)).Should().HaveCount(2)
                .And.BeEquivalentTo(new[] { Feature.Feature1, Feature.Feature2 });
        }
    }

    public enum Feature
    {
        Feature1 = 1,
        Feature2 = 2,
    }

    public class Command1SubType : Command1
    {
    }

    public class Command1
    {
    }

    public class Command2
    {
    }

    public class Command3
    {
    }


    public class Query1
    {
    }

    public class Query2
    { 
    }
}