using CocoriCore.Common;
using CocoriCore.TestUtils;

namespace CocoriCore.Test
{
    public class DataSets : DataSetBase
    {
        public DataSets(IUnitOfWorkFactory unitOfWorkFactory)
            : base(unitOfWorkFactory)
        {
        }
    }
}