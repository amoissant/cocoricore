﻿using System.Reflection;

namespace CocoriCore.Test
{
    public static class AssemblyInfo
    {
        public static Assembly Assembly => typeof(AssemblyInfo).Assembly;
    }
}
