using System;

namespace CocoriCore.Test
{
    public class NotConfiguredException : Exception
    {
        public T IfNull<T> (T value)
        {
            if(value == null)
                throw this;
            else
                return value;
        }
    }

    
}