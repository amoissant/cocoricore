using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;

namespace CocoriCore.Test
{
    public class ApplicationBuilderFake : IApplicationBuilder
    {
        private List<Func<RequestDelegate, RequestDelegate>> _middlewareCollection;

        public ApplicationBuilderFake() 
        {
            _middlewareCollection = new List<Func<RequestDelegate, RequestDelegate>>();
        }

        public IServiceProvider ApplicationServices { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public IFeatureCollection ServerFeatures => throw new NotImplementedException();

        public IDictionary<string, object> Properties => throw new NotImplementedException();

        public IApplicationBuilder New()
        {
            throw new NotImplementedException();
        }

        public IApplicationBuilder Use(Func<RequestDelegate, RequestDelegate> middleware)
        {
            _middlewareCollection.Add(middleware);
            return this;
        }

        public RequestDelegate Build()
        {
            //as in https://github.com/aspnet/HttpAbstraforeachctions/blob/a78b194a84cfbc560a56d6d951eb71c8367d17bb/src/Microsoft.AspNetCore.Http/Internal/ApplicationBuilder.cs
            RequestDelegate app = context =>
            {
                context.Response.StatusCode = 404;
                return Task.CompletedTask;
            };
            _middlewareCollection.Reverse();
            foreach (var component in _middlewareCollection)
                app = component(app);
            return app;
        } 

        public async Task ExecuteAsync(RequestDelegate processRequest)
        {
            _middlewareCollection.Add(next => { return context => processRequest(context); });
            var app = Build();
            await app(new DefaultHttpContext());
        }

        public async Task ExecuteAsync(HttpContext httpContext)
        {
            var app = Build();
            await app(httpContext);
        }
    }
}