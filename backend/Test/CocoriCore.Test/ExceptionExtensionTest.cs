using System;
using FluentAssertions;
using Xunit;
using CocoriCore.Common;

namespace CocoriCore.Test
{
    public class ExceptionExtensionTest
    {
        [Fact]
        public void ExceptionIdIsNullIfExceptionIsNull()
        {
            Exception exception = null;
            exception.GetId().Should().BeNull();
        }

        [Fact]
        public void ExceptionIdIsNullIfExceptionDataDoenstContainsExceptionId()
        {
            Exception exception = new Exception();
            exception.GetId().Should().BeNull();
        }

        [Fact]
        public void ExceptionIdIsNotEmptyIfExceptionDataContainsExceptionId()
        {
            Exception exception = new Exception();
            var id = exception.GetOrCreateId();
            exception.GetId().Should().NotBeEmpty().And.Be(id);
        }
    }
}