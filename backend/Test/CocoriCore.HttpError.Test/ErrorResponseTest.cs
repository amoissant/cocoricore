﻿using System;
using Xunit;
using FluentAssertions;
using System.Collections.Generic;
using CocoriCore.Common;

namespace CocoriCore.HttpError.Test
{
    public class ErrorResponseTest
    {
        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public void FillDebugInformationsWithMessageDataAndStackTrace(bool fillDebug)
        {
            var errorId = Guid.NewGuid();
            try
            {
                ThrowException(errorId, "an exception message", ("debugKey", "debugValue"));
            }
            catch (Exception e)
            {
                var errorResponse = new ErrorResponse(e, 500, fillDebug);
                errorResponse.Id.Should().Be(errorId);
                errorResponse.Message.Should().Be("an exception message");
                if (fillDebug)
                {
                    errorResponse.Debug.Type.Should().Be(typeof(InvalidOperationException).FullName);
                    errorResponse.Debug.Messages.Should().BeEquivalentTo(new[] { "an exception message" });
                    errorResponse.Debug.StackTraces.Should().HaveCount(2);
                    errorResponse.Debug.StackTraces[0].Should().Contain(nameof(ThrowException));
                    errorResponse.Debug.StackTraces[1].Should().Contain(nameof(FillDebugInformationsWithMessageDataAndStackTrace));
                    ((IDictionary<string, object>)(errorResponse.Debug.Datas)).Should().HaveCount(1)
                        .And.Contain("debugKey", "debugValue");
                }
                else
                {
                    errorResponse.Debug.Should().BeNull();
                }
                return;
            }
            true.Should().BeFalse("Test failed");
        }

        [Fact]
        public void FillDebugInformationsWithInnerMessagesDatasAndStackTraces()
        {
            var errorId = Guid.NewGuid();
            try
            {
                ThrowExceptionWithInner(errorId, "an exception message", ("debugKey", "debugValue"), "an inner exception message", ("code", "INVALID_VALUE"));
            }
            catch (Exception e)
            {
                var errorResponse = new ErrorResponse(e, 500, true);
                errorResponse.Id.Should().Be(errorId);
                errorResponse.Message.Should().Be("an exception message");
                errorResponse.Debug.Type.Should().Be(typeof(InvalidOperationException).FullName);
                errorResponse.Debug.Messages.Should().BeEquivalentTo(
                    new[]
                    {
                        "an exception message",
                        "--- Inner message :",
                        "an inner exception message"
                    });
                errorResponse.Debug.StackTraces.Should().HaveCount(5);
                errorResponse.Debug.StackTraces[0].Should().Contain(nameof(ThrowInnerException));
                errorResponse.Debug.StackTraces[1].Should().Contain(nameof(ThrowExceptionWithInner));
                errorResponse.Debug.StackTraces[2].Should().Be("--- End of the stack trace.");
                errorResponse.Debug.StackTraces[3].Should().Contain(nameof(ThrowExceptionWithInner));
                errorResponse.Debug.StackTraces[4].Should().Contain(nameof(FillDebugInformationsWithInnerMessagesDatasAndStackTraces));
                var datas = (IDictionary<string, object>)errorResponse.Debug.Datas;
                var innerDatas = (IDictionary<string, object>)datas["InnerDatas"];
                datas.Should().HaveCount(2)
                    .And.Contain("debugKey", "debugValue")
                    .And.ContainKey("InnerDatas");
                innerDatas.Should().HaveCount(1)
                    .And.Contain("code", "INVALID_VALUE");
                return;
            }
            true.Should().BeFalse("Test failed");
        }

        [Fact]
        public void FillResponseEvenWithNullStackTracesAndNullMessages()
        {
            var inner = new ExceptionWithNullMessage();
            var exception = new ExceptionWithNullMessage(inner);

            var errorResponse = new ErrorResponse(exception, 500, true);

            errorResponse.Id.Should().BeNull();
            errorResponse.Message.Should().BeNull();
            errorResponse.Debug.Type.Should().Be(typeof(ExceptionWithNullMessage).FullName);
            errorResponse.Debug.Messages.Should().BeEquivalentTo(
                new[]
                {
                    "",
                    "--- Inner message :",
                    ""
                });
            errorResponse.Debug.StackTraces.Should().BeEquivalentTo(
                new[]
                {
                    "",
                    "--- End of the stack trace.",
                    ""
                });
            var datas = (IDictionary<string, object>)errorResponse.Debug.Datas;
            datas.Should().BeEmpty();
            return;
        }

        [Fact]
        public void FillResponseEvenWithNullException()
        {
            var errorResponse = new ErrorResponse(null, 500, true);

            errorResponse.Id.Should().BeNull();
            errorResponse.Message.Should().BeNull();
            errorResponse.Debug.Type.Should().BeNull();
            errorResponse.Debug.Messages.Should().BeEquivalentTo(new[] { "" });
            errorResponse.Debug.StackTraces.Should().BeEquivalentTo(new[] { "" });
            var datas = (IDictionary<string, object>)errorResponse.Debug.Datas;
            datas.Should().BeEmpty();
            return;
        }

        private void ThrowException(Guid errorId, string errorMessage, (string, string) errorDatas)
        {
            var exception = new InvalidOperationException(errorMessage);
            exception.SetId(errorId);
            exception.Data[errorDatas.Item1] = errorDatas.Item2;
            throw exception;
        }

        private void ThrowExceptionWithInner(Guid errorId, string errorMessage, (string, string) errorDatas, string innerErrorMessage, (string, string) innerErrorDatas)
        {
            try
            {
                ThrowInnerException(innerErrorMessage, innerErrorDatas);
            }
            catch (Exception inner)
            {
                var exception = new InvalidOperationException(errorMessage, inner);
                exception.SetId(errorId);
                exception.Data[errorDatas.Item1] = errorDatas.Item2;
                throw exception;
            }
        }

        private void ThrowInnerException(string errorMessage, (string, string) errorDatas)
        {
            var exception = new InvalidOperationException(errorMessage);
            exception.Data[errorDatas.Item1] = errorDatas.Item2;
            throw exception;
        }

        public class ExceptionWithNullMessage : Exception
        {
            public ExceptionWithNullMessage(ExceptionWithNullMessage inner) : base(null, inner)
            {
            }

            public ExceptionWithNullMessage()
            {
            }

            public override string Message => null;
        }
    }
}
