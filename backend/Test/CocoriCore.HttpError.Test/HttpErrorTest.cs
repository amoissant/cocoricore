using System;
using Xunit;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Autofac;
using Newtonsoft.Json;
using CocoriCore.HttpError;
using FluentAssertions;
using CocoriCore.TestUtils;
using CocoriCore.Common;
using CocoriCore.Autofac;
using CocoriCore.AspNetCore;
using Microsoft.Extensions.Logging;

namespace CocoriCore.Router.Test
{
    public class HttpErrorTest
    {
        private readonly Lazy<ILifetimeScope> _rootScope;
        private readonly ContainerBuilder _builder;
        private readonly HttpErrorOptionsBuilder _httpErrorBuilder;

        public HttpErrorTest()
        {
            _builder = new ContainerBuilder();
            _rootScope = new Lazy<ILifetimeScope>(() => _builder.Build());
            _httpErrorBuilder = new HttpErrorOptionsBuilder();

            _builder.Register(c => _httpErrorBuilder.Options).SingleInstance();
            _builder.RegisterType<Router>().AsSelf().SingleInstance();
            _builder.RegisterInstance(new MemoryCache(new MemoryCacheOptions())).As<IMemoryCache>();
            _builder.RegisterType<JsonSerializer>().As<JsonSerializer>().SingleInstance();
            _builder.RegisterType<HttpErrorHandler>().As<IHttpErrorHandler>().SingleInstance();

            _builder.Register(c => new UnitOfWorkOptionsBuilder().Options).SingleInstance();
            _builder.RegisterType<AutofacUnitOfWorkFactory>().As<IUnitOfWorkFactory>().SingleInstance();
            _builder.RegisterType<AutofacUnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();

            _builder.RegisterType<Renderer>().AsImplementedInterfaces();
            _builder.ConfigureXunitLogger();
        }

        private ILifetimeScope RootScope => _rootScope.Value;
        private IHttpErrorHandler HttpErrorHandler => RootScope.Resolve<IHttpErrorHandler>();
        private LogEntryCollection LogEntries => RootScope.Resolve<LogEntryCollection>();

        [Fact]
        public async Task ErrorResponseCover()
        {
            _httpErrorBuilder.SetIsDebug(true);
            _httpErrorBuilder.For<Exception>().Use<ErrorResponse>().WithStatusCode(500.1f);
            _httpErrorBuilder.AddMapper<Exception, ErrorResponse>((e, r) => new ErrorResponse(e, r.StatusCode, r.IsDebug));

            var exception = new Exception("An error occured.");
            exception.EnsureHasId();
            exception.Data["value1"] = "toto";
            exception.Data["value2"] = 12;
            exception = MakeThrownException(exception);
            
            var response = await BuildResponseAsync(exception);

            var errorResponse = response.Should().BeOfType<ErrorResponse>().Which;
            errorResponse.Id.Should().NotBeNull().And.Be(exception.GetId());
            errorResponse.Message.Should().Be("An error occured.");
            errorResponse.StatusCode.Should().Be(500.1f);
            errorResponse.Datas.Should().Be(null);
            errorResponse.Debug.Datas.Should().MatchStructure(new { value1 = "toto", value2 = 12 });
            errorResponse.Debug.Messages.Should().BeEquivalentTo(new[] { "An error occured." });
            errorResponse.Debug.StackTraces.Should().HaveCount(2);
            errorResponse.Debug.StackTraces[0].Should().Contain(nameof(ThrowException));
            errorResponse.Debug.StackTraces[1].Should().Contain(nameof(MakeThrownException));
            errorResponse.Debug.Type.Should().Be(typeof(Exception).FullName);
        }

        [Fact]
        public void ExceptionWhenTwoRuleForSameExceptionType()
        {
            _httpErrorBuilder.For<Exception>().Use<ErrorResponse>();
            _httpErrorBuilder.For<Exception>().Use<object>();

            HttpErrorOptions options;
            Action action = () => options = _httpErrorBuilder.Options;

            action.Should().Throw<ConfigurationException>().Which
                .Message.Should().Contain("Duplicate rule")
                .And.Contain(typeof(Exception).FullName);
        }

        [Fact]
        public void ExceptionWhenTwoMappersForSameExceptionType()
        {
            _httpErrorBuilder.AddMapper<Exception, ErrorResponse>((e, r) => new ErrorResponse(e, r.StatusCode, r.IsDebug));
            _httpErrorBuilder.AddMapper<Exception, object>((e, r) => new { });

            HttpErrorOptions options;
            Action action = () => options = _httpErrorBuilder.Options;

            action.Should().Throw<ConfigurationException>().Which
                .Message.Should().Contain("Duplicate mapper")
                .And.Contain(typeof(Exception).FullName);
        }

        [Fact]
        public async Task CanAddMappersFromProvider()
        {
            _httpErrorBuilder.AddMappersFromProvider<MyHttpErrorMapperProvider>();

            _httpErrorBuilder.For<Exception>().Use<ErrorResponse>();
            _httpErrorBuilder.For<ArgumentNullException>().Use<string>();

            var response1 = await BuildResponseAsync(new Exception("An error occured."));
            var response2 = await BuildResponseAsync(new ArgumentNullException("Toto"));

            response1.Should().BeOfType<ErrorResponse>().Which.Message.Should().Be("An error occured.");
            response2.Should().BeOfType<string>().Which.Should().Be("Toto");
        }

        [Fact]
        public async Task UseDefaultRuleAndMapperByDefault()
        {
            var response1 = (ErrorResponse)await BuildResponseAsync(new Exception("An error occured."));
            var response2 = (ErrorResponse)await BuildResponseAsync(new ArgumentNullException("Toto"));

            response1.Message.Should().Be("An error occured.");
            response1.StatusCode.Should().Be(500f);
            response2.Message.Should().Be("Value cannot be null. (Parameter 'Toto')");
            response2.StatusCode.Should().Be(500f);
        }

        [Fact]
        public async Task UseExceptionTypeHierarchyToChooseRule()
        {
            _httpErrorBuilder.For<Exception>().Use<TestResponse>().WithStatusCode(500.1f);
            _httpErrorBuilder.For<ArgumentException>().Use<TestResponse>().WithStatusCode(500.2f);
            _httpErrorBuilder.For<IMyException>().Use<TestResponse>().WithStatusCode(500.4f);

            _httpErrorBuilder.AddMapper<Exception, TestResponse>((e, r) => 
                new TestResponse(e.Message, r.StatusCode, "Exception mapper"));
            _httpErrorBuilder.AddMapper<ArgumentNullException, TestResponse>((e, r) => 
                new TestResponse(e.Message, r.StatusCode, "ArgumentNullException mapper"));
            _httpErrorBuilder.AddMapper<ArgumentOutOfRangeException, TestResponse>((e, r) => 
                new TestResponse(e.Message, r.StatusCode, "ArgumentOutOfRangeException mapper"));
            _httpErrorBuilder.AddMapper<IMyException, TestResponse>((e, r) =>
                new TestResponse(e.Message, r.StatusCode, "IMyException mapper"));

            var response1 = (TestResponse)await BuildResponseAsync(new Exception("An error occured."));
            var response2 = (TestResponse)await BuildResponseAsync(new ArgumentNullException("Toto"));
            var response3 = (TestResponse)await BuildResponseAsync(new ArgumentOutOfRangeException("Tutu"));
            var response4 = (TestResponse)await BuildResponseAsync(new MyException("My exception."));

            response1.Message.Should().Be("An error occured.");
            response1.StatusCode.Should().Be(500.1f);
            response1.Mapper.Should().Be("Exception mapper");
            response2.Message.Should().Be("Value cannot be null. (Parameter 'Toto')");
            response2.StatusCode.Should().Be(500.2f);
            response2.Mapper.Should().Be("ArgumentNullException mapper");
            response3.Message.Should().Be("Specified argument was out of the range of valid values. (Parameter 'Tutu')");
            response3.StatusCode.Should().Be(500.2f);
            response3.Mapper.Should().Be("ArgumentOutOfRangeException mapper");
            response4.Message.Should().Be("My exception.");
            response4.StatusCode.Should().Be(500.4f);
            response4.Mapper.Should().Be("IMyException mapper");
        }

        [Fact]
        public async Task UseExceptionTypeHierachyToChooseMapper()
        {
            _httpErrorBuilder.For<Exception>().Use<TestResponse>().WithStatusCode(500.1f);
            _httpErrorBuilder.For<ArgumentNullException>().Use<TestResponse>().WithStatusCode(500.2f);
            _httpErrorBuilder.For<ArgumentOutOfRangeException>().Use<TestResponse>().WithStatusCode(500.3f);

            _httpErrorBuilder.AddMapper<Exception, TestResponse>((e, r) =>
                new TestResponse(e.Message, r.StatusCode, "Exception mapper"));
            _httpErrorBuilder.AddMapper<ArgumentException, TestResponse>((e, r) =>
                new TestResponse(e.Message, r.StatusCode, "ArgumentException mapper"));
            _httpErrorBuilder.AddMapper<IMyException, TestResponse>((e, r) =>
                new TestResponse(e.Message, r.StatusCode, "IMyException mapper"));

            var response1 = (TestResponse)await BuildResponseAsync(new Exception("An error occured."));
            var response2 = (TestResponse)await BuildResponseAsync(new ArgumentNullException("Toto"));
            var response3 = (TestResponse)await BuildResponseAsync(new ArgumentOutOfRangeException("Tutu"));
            var response4 = (TestResponse)await BuildResponseAsync(new MyException("My exception."));

            response1.Message.Should().Be("An error occured.");
            response1.StatusCode.Should().Be(500.1f);
            response1.Mapper.Should().Be("Exception mapper");
            response2.Message.Should().Be("Value cannot be null. (Parameter 'Toto')");
            response2.StatusCode.Should().Be(500.2f);
            response2.Mapper.Should().Be("ArgumentException mapper");
            response3.Message.Should().Be("Specified argument was out of the range of valid values. (Parameter 'Tutu')");
            response3.StatusCode.Should().Be(500.3f);
            response3.Mapper.Should().Be("ArgumentException mapper");
            response4.Message.Should().Be("My exception.");
            response4.StatusCode.Should().Be(500.1f);
            response4.Mapper.Should().Be("IMyException mapper");
        }

        [Fact]
        public void ExceptionWhenNoMapperForRuleExceptionType()
        {
            _httpErrorBuilder.For<Exception>().Use<TestResponse>().WithStatusCode(500);

            HttpErrorOptions options;
            Action action = () => options = _httpErrorBuilder.Options;

            action.Should().Throw<ConfigurationException>().Which
                .Message.Should().Contain("No mapper")
                .And.Contain("For<Exception>().Use<TestResponse>().WithStatusCode(500)")
                .And.Contain(typeof(Exception).FullName)
                .And.Contain(typeof(TestResponse).FullName);
        }

        [Fact]
        public void ExceptionWhenNoMapperForRuleResponseType()
        {
            _httpErrorBuilder.For<IMyException>().Use<TestResponse>().WithStatusCode(500);
            _httpErrorBuilder.AddMapper<IMyException, object>((e, r) => new { });

            HttpErrorOptions options;
            Action action = () => options = _httpErrorBuilder.Options;

            action.Should().Throw<ConfigurationException>().Which
                .Message.Should().Contain("No mapper")
                .And.Contain("For<IMyException>().Use<TestResponse>().WithStatusCode(500)")
                .And.Contain(typeof(IMyException).FullName)
                .And.Contain(typeof(TestResponse).FullName);
        }

        [Fact]
        public async Task WriteErrorResponseToHttpResponseAsync()
        {
            _httpErrorBuilder.For<Exception>().Use<ErrorResponse>().WithStatusCode(599.1f);

            var context = new FakeHttpContext();
            var exception = new Exception("Test");
            exception.SetId(new Guid("64e227ed-a052-45cd-b497-572420616ac0"));
            await HttpErrorHandler.HandleAsync(exception, context);

            exception.GetId().Should().NotBeNull();
            context.GetErrorId().Should().Be(exception.GetId().ToString());
            context.Response.StatusCode.Should().Be(599);
            context.Response.ContentType.Should().Be("application/json; charset=utf-8");
            context.FakeResponse.GetTextBody().Should().Be("{\"Id\":\"64e227ed-a052-45cd-b497-572420616ac0\",\"StatusCode\":599.1,\"Message\":\"Test\",\"Datas\":null,\"Debug\":null}");
        }

        [Fact]
        public async Task ReturnODataErroResponseForODataRequest()
        {
            var context = new FakeHttpContext();
            context.Request.Headers["Accept"] = "odata=verbose";
            var exception = new Exception("Test");
            exception.SetId(new Guid("64e227ed-a052-45cd-b497-572420616ac0"));
            await HttpErrorHandler.HandleAsync(exception, context);

            context.Response.StatusCode.Should().Be(500);
            context.Response.ContentType.Should().Be("application/json; charset=utf-8");
            context.FakeResponse.GetTextBody().Should().Be("{\"error\":{\"code\":500.0,\"message\":\"Test\",\"innererror\":{\"Id\":\"64e227ed-a052-45cd-b497-572420616ac0\",\"StatusCode\":500.0,\"Message\":\"Test\",\"Datas\":null,\"Debug\":null}}}");
        }

        [Fact]
        public async Task ErrorResponseCoverWithUnitOfWork()
        {
            _httpErrorBuilder.For<Exception>().Use<ErrorResponse>().WithStatusCode(500);
            _httpErrorBuilder.AddMapper<Exception, ErrorResponse>(async (e, r, u) =>
            {
                var response = new ErrorResponse(e, r.StatusCode, r.IsDebug);
                response.Message = await u.Resolve<IRenderer>().RenderAsync(e.GetId());
                return response;
            });

            var exception = new Exception("An error occured.");
            exception.EnsureHasId();
            var response = await BuildResponseAsync(exception);

            var errorResponse = response.Should().BeOfType<ErrorResponse>().Which;
            errorResponse.Id.Should().NotBeNull();
            errorResponse.Message.Should().Be($"An error occured, please contact administrator with this code : {errorResponse.Id}.");
        }

        [Fact]
        public async Task LogExceptionIfResponseHasStarted()
        {
            _httpErrorBuilder.For<Exception>().Use<ErrorResponse>().WithStatusCode(500);
            var context = new FakeHttpContext();
            context.FakeResponse.SetHasStarted(true);
            context.FakeResponse.SetTextBody("already started response");
            var exception = new Exception("An error occured.");

            await HttpErrorHandler.HandleAsync(exception, context);

            context.FakeResponse.GetTextBody().Should().Be("already started response");
            LogEntries.Should().Contain(LogLevel.Error, "An error occured.");
        }

        [Fact]
        public async Task UseConditionToApplyRule()
        {
            _httpErrorBuilder.For<Exception>().Use<ErrorResponse>().WithStatusCode(500);
            _httpErrorBuilder.For<Exception>().When(x => x is InvalidOperationException).Use<ErrorResponse>().WithStatusCode(503);

            var response1 = await BuildResponseAsync(new Exception());

            response1.Should().BeOfType<ErrorResponse>().Which.StatusCode.Should().Be(500);

            var response2 = await BuildResponseAsync(new InvalidOperationException());

            response2.Should().BeOfType<ErrorResponse>().Which.StatusCode.Should().Be(503);
        }

        [Fact]
        public async Task ErrorWhenSeveralRulesMatchExceptionAsync()
        {
            _httpErrorBuilder.For<Exception>().When(x => true).Use<ErrorResponse>().WithStatusCode(500);
            _httpErrorBuilder.For<Exception>().When(x => true).Use<ErrorResponse>().WithStatusCode(503);

            Func<Task> action = () => BuildResponseAsync(new Exception());

            var assertion = await action.Should().ThrowAsync<ConfigurationException>();
            assertion.Which.Message.Should().StartWith("Several rules");
        }

        [Fact]
        public async Task ErrorWhenNoRuleMatchExceptionAsync()
        {
            _httpErrorBuilder.For<Exception>().When(x => x is InvalidOperationException).Use<ErrorResponse>().WithStatusCode(503);
            _httpErrorBuilder.For<Exception>().When(x => x is InvalidOperationException).Use<ErrorResponse>().WithStatusCode(504);

            Func<Task> action = () => BuildResponseAsync(new Exception());

            var assertion = await action.Should().ThrowAsync<ConfigurationException>();
            assertion.Which.Message.Should().StartWith("No rule");
        }

        private TException MakeThrownException<TException>(TException exception) where TException : Exception
        {
            try
            {
                return ThrowException(exception);
            }
            catch (Exception e)
            {
                return (TException)e;
            }
        }

        private TException ThrowException<TException>(TException exception) where TException : Exception
        {
            throw exception;
        }

        private async Task<object> BuildResponseAsync(Exception exception)
        {
            var rule = HttpErrorHandler.GetRule(exception);
            var context = new FakeHttpContext();
            return await HttpErrorHandler.BuildResponseAsync(exception, context, rule);
        }

        class MyHttpErrorMapperProvider : HttpErrorMapperProviderBase
        {
            public MyHttpErrorMapperProvider()
            {
                AddMapper<Exception, ErrorResponse>((e, r) => new ErrorResponse(e, r.StatusCode, r.IsDebug));
                AddMapper<ArgumentNullException, string>((e, r) => e.ParamName);
            }
        }

        class TestResponse
        {
            public string Mapper;
            public float StatusCode;
            public string Message;

            public TestResponse(string message, float statusCode, string mapper)
            {
                Message = message;
                StatusCode = statusCode;
                Mapper = mapper;
            }
        }

        interface IMyException
        {
            string Message { get; }
        }

        class MyException : Exception, IMyException
        {
            public MyException(string message) : base(message)
            {
            }
        }

        interface IRenderer
        {
            Task<string> RenderAsync(Guid? exceptionId);
        }

        class Renderer : IRenderer
        {
            public Task<string> RenderAsync(Guid? exceptionId)
            {
                return Task.FromResult($"An error occured, please contact administrator with this code : {exceptionId}.");
            }
        }
    }
}
