using System;
using System.Linq;
using System.Threading.Tasks;
using CocoriCore.Common;
using CocoriCore.Repository;
using FluentAssertions;
using Mongo2Go;
using MongoDB.Driver;

namespace CocoriCore.MongoDb.Test
{
    public class MongoDbRepositoryTest : IDisposable
    {
        private IMongoDatabase _database;
        private MongoDbRunner _runner;
        private IMongoClient _client;

        private async Task<MongoDBRepository> GetRepositoryAsync()
        {
            _runner = MongoDbRunner.Start(singleNodeReplSet: true);
            _client = new MongoClient(_runner.ConnectionString);
            _database = _client.GetDatabase("test");

            var collectiondefinition = new MongoCollectionsDefinition()
                .DefineCollections<IEntity>(this.GetType().Assembly);
            await collectiondefinition.EnsureAllCollectionsExistAsync(_database);

            return new MongoDBRepository(_database);
        }

        //[Fact(Skip = "upgrade to .netcore3.0")]
        public async Task InsertAsync()
        {
            var repository = await GetRepositoryAsync();
            var entity = new Entity();
            entity.Id = Guid.NewGuid();
            entity.Name = "toto";

            await repository.InsertAsync(entity);
            await repository.CommitAsync();

            var persitedEntity = _database.GetCollection<Entity>(nameof(Entity)).Find(x => x.Id == entity.Id);
            persitedEntity.Single().Should().BeEquivalentTo(entity);
        }

        //[Fact(Skip = "upgrade to .netcore3.0")]
        public async Task UpdateAsync()
        {
            var repository = await GetRepositoryAsync();
            var entity = new Entity();
            entity.Id = Guid.NewGuid();
            await repository.InsertAsync(entity);
            await repository.CommitAsync();

            entity.Name = "titi";
            await repository.UpdateAsync(entity);
            await repository.CommitAsync();

            var persitedEntity = _database.GetCollection<Entity>(nameof(Entity)).Find(x => x.Id == entity.Id);
            persitedEntity.Single().Should().BeEquivalentTo(entity);
        }

        //[Fact(Skip = "upgrade to .netcore3.0")]
        public async Task DeleteAsync()
        {
            var repository = await GetRepositoryAsync();
            var entity = new Entity();
            entity.Id = Guid.NewGuid();
            await repository.InsertAsync(entity);
            await repository.CommitAsync();

            await repository.DeleteAsync(entity);
            await repository.CommitAsync();

            _database.GetCollection<Entity>(nameof(Entity)).CountDocuments(x => true).Should().Be(0);
        }

        //[Fact(Skip = "upgrade to .netcore3.0")]
        public async Task ExistsAsync()
        {
            var repository = await GetRepositoryAsync();
            var entity = new Entity();
            entity.Id = Guid.NewGuid();
            await repository.InsertAsync(entity);
            await repository.CommitAsync();

            var entityExists = await repository.ExistsAsync<Entity>(entity.Id);
            var invalidEntityExists = await repository.ExistsAsync<Entity>(Guid.NewGuid());

            entityExists.Should().BeTrue();
            invalidEntityExists.Should().BeFalse();
        }

        //[Fact(Skip = "upgrade to .netcore3.0")]
        public async Task ExistsForTypeAsync()
        {
            var repository = await GetRepositoryAsync();
            var entity = new Entity();
            entity.Id = Guid.NewGuid();
            await repository.InsertAsync(entity);
            await repository.CommitAsync();

            var entityExists = await repository.ExistsAsync(typeof(Entity), entity.Id);
            var invalidEntiyExists = await repository.ExistsAsync(typeof(Entity), Guid.NewGuid());

            entityExists.Should().BeTrue();
            invalidEntiyExists.Should().BeFalse();
        }

        //[Fact(Skip = "upgrade to .netcore3.0")]
        public async Task ExistsForUniqueMemberAsync()
        {
            var repository = await GetRepositoryAsync();
            var entity = new Entity();
            entity.Id = Guid.NewGuid();
            entity.Name = "toto";
            await repository.InsertAsync(entity);
            await repository.CommitAsync();

            var entityExists = await repository.ExistsAsync(typeof(Entity), typeof(Entity).GetProperty(nameof(Entity.Name)), "toto");
            var invalidEntityExists = await repository.ExistsAsync(typeof(Entity), typeof(Entity).GetProperty(nameof(Entity.Name)), "titi");

            entityExists.Should().BeTrue();
            invalidEntityExists.Should().BeFalse();
        }

        //[Fact(Skip = "upgrade to .netcore3.0")]
        public async Task ExistsCollectionAsync()
        {
            var repository = await GetRepositoryAsync();
            var entity1 = new Entity();
            entity1.Id = Guid.NewGuid();
            entity1.Name = "toto";
            var entity2 = new Entity();
            entity2.Id = Guid.NewGuid();
            entity2.Name = "tit";
            await repository.InsertManyAsync(entity1, entity2);
            await repository.CommitAsync();

            var entityExists = await repository.ExistsAsync(typeof(Entity), new Guid[] { entity1.Id, entity2.Id });
            var invalidEntityExists = await repository.ExistsAsync(typeof(Entity), new Guid[] { Guid.NewGuid(), Guid.NewGuid() });

            entityExists.Should().BeTrue();
            invalidEntityExists.Should().BeFalse();
        }

        //[Fact(Skip = "upgrade to .netcore3.0")]
        public async Task LoadAsync()
        {
            var repository = await GetRepositoryAsync();
            var entity = new Entity();
            entity.Id = Guid.NewGuid();
            entity.Name = "toto";
            await repository.InsertAsync(entity);
            await repository.CommitAsync();

            var loadedEntity = await repository.LoadAsync<Entity>(entity.Id);

            loadedEntity.Should().NotBeSameAs(entity);
            loadedEntity.Should().BeEquivalentTo(entity);
        }

        //[Fact(Skip = "upgrade to .netcore3.0")]
        public async Task LoadForTypeAsync()
        {
            var repository = await GetRepositoryAsync();
            var entity = new Entity();
            entity.Id = Guid.NewGuid();
            entity.Name = "toto";
            await repository.InsertAsync(entity);
            await repository.CommitAsync();

            var loadedEntity = await repository.LoadAsync(typeof(Entity), entity.Id);

            loadedEntity.Should().NotBeSameAs(entity);
            loadedEntity.Should().BeEquivalentTo(entity);
        }

        //[Fact(Skip = "upgrade to .netcore3.0")]
        public async Task LoadForUniqueMemberAsync()
        {
            var repository = await GetRepositoryAsync();
            var entity = new Entity();
            entity.Id = Guid.NewGuid();
            entity.Name = "toto";
            await repository.InsertAsync(entity);
            await repository.CommitAsync();

            var loadedEntity = await repository.LoadAsync<Entity>(x => x.Name, "toto");

            loadedEntity.Should().NotBeSameAs(entity);
            loadedEntity.Should().BeEquivalentTo(entity);
        }

        //[Fact(Skip = "upgrade to .netcore3.0")]
        public async Task Query()
        {
            var repository = await GetRepositoryAsync();
            var entity = new Entity();
            entity.Id = Guid.NewGuid();
            entity.Name = "toto";
            await repository.InsertAsync(entity);
            await repository.CommitAsync();

            var entities = repository.Query<Entity>().ToList();

            entities.Should().ContainSingle().Which.Should().BeEquivalentTo(entity);
        }

        //[Fact(Skip = "upgrade to .netcore3.0")]
        public async Task CommitAsync()
        {
            var repository = await GetRepositoryAsync();
            var entity = new Entity();
            entity.Id = Guid.NewGuid();
            entity.Name = "toto";
            await repository.InsertAsync(entity);

            var entityExists = await repository.ExistsAsync<Entity>(entity.Id);
            entityExists.Should().BeFalse();

            await repository.CommitAsync();

            entityExists = await repository.ExistsAsync<Entity>(entity.Id);
            entityExists.Should().BeTrue();
        }

        //[Fact(Skip = "upgrade to .netcore3.0")]
        public async Task RollbackAsync()
        {
            var repository = await GetRepositoryAsync();
            var entity = new Entity();
            entity.Id = Guid.NewGuid();
            entity.Name = "toto";
            await repository.InsertAsync(entity);

            await repository.RollbackAsync();

            var entityExists = await repository.ExistsAsync<Entity>(entity.Id);
            entityExists.Should().BeFalse();
        }

        //[Fact(Skip = "upgrade to .netcore3.0")]
        public async Task ReloadAsync()
        {
            var repository = await GetRepositoryAsync();
            var entity = new Entity();
            entity.Id = Guid.NewGuid();
            entity.Name = "toto";
            await repository.InsertAsync(entity);
            await repository.CommitAsync();

            var reloadedEntity = await repository.ReloadAsync(entity);

            reloadedEntity.Should().NotBeSameAs(entity);
            reloadedEntity.Should().BeEquivalentTo(entity);
        }

        //[Fact(Skip = "upgrade to .netcore3.0")]
        public async Task ReloadForTypeAsync()
        {
            var repository = await GetRepositoryAsync();
            var entity = new Entity();
            entity.Id = Guid.NewGuid();
            entity.Name = "toto";
            await repository.InsertAsync(entity);
            await repository.CommitAsync();

            var reloadedEntity = await repository.ReloadAsync<Entity>(entity.Id);

            reloadedEntity.Should().NotBeSameAs(entity);
            reloadedEntity.Should().BeEquivalentTo(entity);
        }

        public void Dispose()
        {
            _runner.Dispose();
        }
    }

    public class Entity : IEntity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
