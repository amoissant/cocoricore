﻿using Autofac;
using CocoriCore.Autofac;
using CocoriCore.Common;
using FluentAssertions;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Linq;
using Xunit;
using Xunit.Abstractions;

namespace CocoriCore.Authentication.Test
{
    public class PrivateMessageTest
    {
        private readonly ContainerBuilder _builder;
        private readonly ITestOutputHelper _output;
        private ILifetimeScope _scope;

        public PrivateMessageTest(ITestOutputHelper output)
        {
            _output = output;
            _builder = new ContainerBuilder();
            _builder.RegisterType<PrivateMessageController>().AsSelf();
            _builder.RegisterType<AutofacUnitOfWork>().As<IUnitOfWork>();
            _builder.RegisterType<UnitOfWorkOptions>().AsSelf();

            _builder.RegisterInstance(new MemoryCache(new MemoryCacheOptions())).As<IMemoryCache>();
            _builder.RegisterType<MyFeatureOptions>().AsSelf();
        }

        protected ILifetimeScope Scope
        {
            get
            {
                if (_scope == null)
                {
                    _scope = _builder.Build();
                }
                return _scope;
            }
        }

        [Fact]
        public void ConfigureAuthenticationDefaultPrivateFromType()
        {
            var builder = new PrivateMessageOptionsBuilder()
                .DefaultPrivate()
                .Public<PublicCommand>()
                .Public<PublicQuery>();
            _builder.RegisterInstance(builder.Options);
            var authenticationController = Scope.Resolve<PrivateMessageController>();

            authenticationController.IsPrivate(typeof(PublicCommand)).Should().BeFalse();
            authenticationController.IsPrivate(typeof(PublicQuery)).Should().BeFalse();
            authenticationController.IsPrivate(typeof(PrivateCommand)).Should().BeTrue();
            authenticationController.IsPrivate(typeof(PrivateQuery)).Should().BeTrue();
        }

        [Fact]
        public void ConfigureAuthenticationDefaultPublicFromType()
        {
            var builder = new PrivateMessageOptionsBuilder()
                .DefaultPublic()
                .Private<PrivateCommand>()
                .Private<PrivateQuery>();
            _builder.RegisterInstance(builder.Options);
            var authenticationController = Scope.Resolve<PrivateMessageController>();

            authenticationController.IsPrivate(typeof(PublicCommand)).Should().BeFalse();
            authenticationController.IsPrivate(typeof(PublicQuery)).Should().BeFalse();
            authenticationController.IsPrivate(typeof(PrivateCommand)).Should().BeTrue();
            authenticationController.IsPrivate(typeof(PrivateQuery)).Should().BeTrue();
        }

        [Fact]
        public void ConfigureAuthenticationDefaultPrivateFromInterface()
        {
            var builder = new PrivateMessageOptionsBuilder()
                .DefaultPrivate()
                .Public<IPublic>();
            _builder.RegisterInstance(builder.Options);
            var authenticationController = Scope.Resolve<PrivateMessageController>();

            authenticationController.IsPrivate(typeof(PublicCommand)).Should().BeFalse();
            authenticationController.IsPrivate(typeof(PublicQuery)).Should().BeFalse();
            authenticationController.IsPrivate(typeof(PrivateCommand)).Should().BeTrue();
            authenticationController.IsPrivate(typeof(PrivateQuery)).Should().BeTrue();
        }

        [Fact]
        public void ConfigureAuthenticationDefaultPublicFromInterface()
        {
            var builder = new PrivateMessageOptionsBuilder()
                .DefaultPublic()
                .Private<IPrivate>();
            _builder.RegisterInstance(builder.Options);
            var authenticationController = Scope.Resolve<PrivateMessageController>();

            authenticationController.IsPrivate(typeof(PublicCommand)).Should().BeFalse();
            authenticationController.IsPrivate(typeof(PublicQuery)).Should().BeFalse();
            authenticationController.IsPrivate(typeof(PrivateCommand)).Should().BeTrue();
            authenticationController.IsPrivate(typeof(PrivateQuery)).Should().BeTrue();
        }

        [Fact]
        public void ConfigureAuthenticationDefaultPrivateFromExpression()
        {
            var builder = new PrivateMessageOptionsBuilder()
                .DefaultPrivate()
                .Public(t => t.Name.Contains("Public"));
            _builder.RegisterInstance(builder.Options);
            var authenticationController = Scope.Resolve<PrivateMessageController>();

            authenticationController.IsPrivate(typeof(PublicCommand)).Should().BeFalse();
            authenticationController.IsPrivate(typeof(PublicQuery)).Should().BeFalse();
            authenticationController.IsPrivate(typeof(PrivateCommand)).Should().BeTrue();
            authenticationController.IsPrivate(typeof(PrivateQuery)).Should().BeTrue();
        }

        [Fact]
        public void ConfigureAuthenticationDefaultPublicFromExpression()
        {
            var builder = new PrivateMessageOptionsBuilder()
                .DefaultPublic()
                .Private(t => t.Name.Contains("Private"));
            _builder.RegisterInstance(builder.Options);
            var authenticationController = Scope.Resolve<PrivateMessageController>();

            authenticationController.IsPrivate(typeof(PublicCommand)).Should().BeFalse();
            authenticationController.IsPrivate(typeof(PublicQuery)).Should().BeFalse();
            authenticationController.IsPrivate(typeof(PrivateCommand)).Should().BeTrue();
            authenticationController.IsPrivate(typeof(PrivateQuery)).Should().BeTrue();
        }

        [Fact]
        public void ConfigureAuthenticationDefaultPrivateFromExpressionWithType()
        {
            var builder = new PrivateMessageOptionsBuilder()
                .DefaultPrivate()
                .Public<MyFeatureOptions>((s, t) => s.IsPublic(t));
            _builder.RegisterInstance(builder.Options);
            var authenticationController = Scope.Resolve<PrivateMessageController>();

            authenticationController.IsPrivate(typeof(PublicCommand)).Should().BeFalse();
            authenticationController.IsPrivate(typeof(PublicQuery)).Should().BeFalse();
            authenticationController.IsPrivate(typeof(PrivateCommand)).Should().BeTrue();
            authenticationController.IsPrivate(typeof(PrivateQuery)).Should().BeTrue();
        }

        [Fact]
        public void ConfigureAuthenticationDefaultPublicFromExpressionWithType()
        {
            var builder = new PrivateMessageOptionsBuilder()
                .DefaultPublic()
                .Private<MyFeatureOptions>((s, t) => s.IsPrivate(t));
            _builder.RegisterInstance(builder.Options);
            var authenticationController = Scope.Resolve<PrivateMessageController>();

            authenticationController.IsPrivate(typeof(PublicCommand)).Should().BeFalse();
            authenticationController.IsPrivate(typeof(PublicQuery)).Should().BeFalse();
            authenticationController.IsPrivate(typeof(PrivateCommand)).Should().BeTrue();
            authenticationController.IsPrivate(typeof(PrivateQuery)).Should().BeTrue();
        }
    }

    public enum MyFeatures { Public, Private };

    public class MyFeatureOptions : FeatureOptions
    {
        public MyFeatureOptions()
        {
            DefineFeature(MyFeatures.Public)
                .Add<PublicCommand>()
                .Add<PublicQuery>();

            DefineFeature(MyFeatures.Private)
                .Add<PrivateCommand>()
                .Add<PrivateQuery>();
        }

        public bool IsPublic(Type messageType)
        {
            return GetFeatures(messageType).Contains(MyFeatures.Public);
        }

        public bool IsPrivate(Type messageType)
        {
            return GetFeatures(messageType).Contains(MyFeatures.Private);
        }
    }

    public interface IPublic
    {
    }

    public interface IPrivate
    {
    }

    public class PublicCommand : IPublic
    {
    }

    public class PublicQuery : IPublic
    {
    }

    public class PrivateCommand : IPrivate
    {
    }

    public class PrivateQuery : IPrivate
    {
    }
}
