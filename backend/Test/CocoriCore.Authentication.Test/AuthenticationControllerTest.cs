﻿using Autofac;
using CocoriCore.AspNetCore;
using CocoriCore.Autofac;
using CocoriCore.Common;
using CocoriCore.TestUtils;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace CocoriCore.Authentication.Test
{
    public class AuthenticationControllerTest
    {
        private readonly Lazy<ILifetimeScope> _rootScope;
        private readonly ContainerBuilder _builder;
        private readonly AuthenticationOptionsBuilder _authenticationBuilder;
        private readonly Mock<IConditionService> _conditionMock;
        private readonly Mock<IIpFilterService> _ipFiterMock;
        private readonly Mock<IAuthenticationService> _authenticateMock;
        private readonly Mock<IClaimsService> _claimsServiceMock;

        public AuthenticationControllerTest()
        {
            _builder = new ContainerBuilder();
            _rootScope = new Lazy<ILifetimeScope>(() => _builder.Build());
            _authenticationBuilder = new AuthenticationOptionsBuilder();

            _builder.Register(c => _authenticationBuilder.Options).SingleInstance();
            _builder.RegisterInstance(new MemoryCache(new MemoryCacheOptions())).As<IMemoryCache>();
            _builder.RegisterType<AuthenticationController>().AsSelf().As<IAuthenticationController>().InstancePerLifetimeScope();

            _builder.Register(c => new UnitOfWorkOptionsBuilder().Options).SingleInstance();
            _builder.RegisterType<AutofacUnitOfWorkFactory>().As<IUnitOfWorkFactory>().SingleInstance();
            _builder.RegisterType<AutofacUnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();

            _builder.RegisterType<HttpContextProvider>().As<IHttpContextProvider>().InstancePerLifetimeScope();
            _builder.Register(c => c.Resolve<IHttpContextProvider>().HttpContextOrDefault ?? new FakeHttpContext()).As<HttpContext>().InstancePerLifetimeScope();

            _conditionMock = new Mock<IConditionService>();
            _ipFiterMock = new Mock<IIpFilterService>();
            _authenticateMock = new Mock<IAuthenticationService>();
            _claimsServiceMock = new Mock<IClaimsService>();

            _builder.RegisterInstance(_conditionMock.Object);
            _builder.RegisterInstance(_ipFiterMock.Object);
            _builder.RegisterInstance(_authenticateMock.Object);
            _builder.RegisterInstance(_claimsServiceMock.Object);

            _builder.ConfigureXunitLogger();
        }

        private ILifetimeScope RootScope => _rootScope.Value;
        private IAuthenticationController AuthenticationController => RootScope.Resolve<IAuthenticationController>();
        private IHttpContextProvider HttpContextProvider => RootScope.Resolve<IHttpContextProvider>();

        [Fact]
        public async Task RunAuthenticationStepsUsingSynchronousMethods()
        {
            var message = new MyMessage();
            var claims = new MyClaims();
            _conditionMock.Setup(x => x.IsConditionTrue(It.IsAny<AuthenticationContext>())).Returns(true);
            _claimsServiceMock.Setup(x => x.GetClaims(It.IsAny<Type>())).Returns(claims);
            _authenticationBuilder
                .When<IConditionService>((x, c) => x.IsConditionTrue(c))
                .Call<IIpFilterService>((x, c) => x.CheckIp(c))
                .Call<IAuthenticationService>((x, c) => x.Authenticate(c))
                .GetClaimsUsing<IClaimsService>((x, t) => x.GetClaims(t));

            await AuthenticationController.AuthenticateAsync(message);

            _conditionMock.Verify(x => 
                x.IsConditionTrue(It.Is<AuthenticationContext>(x => x.Message == message)), 
                Times.Once);
            _ipFiterMock.Verify(x => 
                x.CheckIp(It.Is<AuthenticationContext>(x => x.Message == message)), 
                Times.Once);
            _authenticateMock.Verify(x => 
                x.Authenticate(It.Is<AuthenticationContext>(x => x.Message == message)), 
                Times.Once);
            _claimsServiceMock.Verify(x => x.GetClaims(It.IsAny<Type>()), Times.Never);

            AuthenticationController.IsAuthenticated.Should().Be(true);
            AuthenticationController.GetClaims<MyClaims>().Should().Be(claims);
            AuthenticationController.GetClaimsOrDefault<MyClaims>().Should().Be(claims);

            _claimsServiceMock.Verify(x => x.GetClaims(typeof(MyClaims)), Times.Exactly(2));
        }

        [Fact]
        public async Task RunAuthenticationStepsUsingAynchronousMethods()
        {
            var fakeHttpContext = new FakeHttpContext();
            var message = new MyMessage();
            var claims = new MyClaims();
            _conditionMock.Setup(x => x.IsConditionTrueAsync(It.IsAny<AuthenticationContext>())).Returns(Task.FromResult(true));
            _claimsServiceMock.Setup(x => x.GetClaims(It.IsAny<Type>())).Returns(claims);
            _authenticationBuilder
                .When<IConditionService>((x, c) => x.IsConditionTrueAsync(c))
                .Call<IIpFilterService>((x, c) => x.CheckIpAsync(c))
                .Call<IAuthenticationService>((x, c) => x.AuthenticateAsync(c))
                .GetClaimsUsing<IClaimsService>((x, t) => x.GetClaims(t));

            await AuthenticationController.AuthenticateAsync(message);

            _conditionMock.Verify(x =>
               x.IsConditionTrueAsync(It.Is<AuthenticationContext>(x => x.Message == message)),
               Times.Once);
            _ipFiterMock.Verify(x =>
                x.CheckIpAsync(It.Is<AuthenticationContext>(x => x.Message == message)),
                Times.Once);
            _authenticateMock.Verify(x =>
                x.AuthenticateAsync(It.Is<AuthenticationContext>(x => x.Message == message)),
                Times.Once);
            _claimsServiceMock.Verify(x => x.GetClaims(It.IsAny<Type>()), Times.Never);

            AuthenticationController.IsAuthenticated.Should().Be(true);
            AuthenticationController.GetClaims<MyClaims>().Should().Be(claims);
            AuthenticationController.GetClaimsOrDefault<MyClaims>().Should().Be(claims);

            _claimsServiceMock.Verify(x => x.GetClaims(It.IsAny<Type>()), Times.Exactly(2));
        }

        [Fact]
        public async Task RunAuthenticationStepsWithoutServices()
        {
            var fakeHttpContext = new FakeHttpContext();
            var message = new MyMessage();
            var claims = new MyClaims();
            _conditionMock.Setup(x => x.IsConditionTrue(It.IsAny<AuthenticationContext>())).Returns(true);
            _claimsServiceMock.Setup(x => x.GetClaims(It.IsAny<Type>())).Returns(claims);

            _authenticationBuilder
                .When(c => _conditionMock.Object.IsConditionTrue(c))
                .Call(c => _ipFiterMock.Object.CheckIp(c))
                .Call(c => _authenticateMock.Object.Authenticate(c))
                .GetClaimsUsing(t => _claimsServiceMock.Object.GetClaims(t));

            HttpContextProvider.HttpContext = fakeHttpContext;
            await AuthenticationController.AuthenticateAsync(message);

            _conditionMock.Verify(x =>
               x.IsConditionTrue(It.Is<AuthenticationContext>(x => x.Message == message)),
               Times.Once);
            _ipFiterMock.Verify(x =>
                x.CheckIp(It.Is<AuthenticationContext>(x => x.Message == message)),
                Times.Once);
            _authenticateMock.Verify(x =>
                x.Authenticate(It.Is<AuthenticationContext>(x => x.Message == message)),
                Times.Once);
            _claimsServiceMock.Verify(x => x.GetClaims(It.IsAny<Type>()), Times.Never);

            AuthenticationController.IsAuthenticated.Should().Be(true);
            AuthenticationController.GetClaims<MyClaims>().Should().Be(claims);
            AuthenticationController.GetClaimsOrDefault<MyClaims>().Should().Be(claims);

            _claimsServiceMock.Verify(x => x.GetClaims(It.IsAny<Type>()), Times.Exactly(2));
        }

        [Fact]
        public void ExceptionWhenGetClaimsWithoutAuthenticateFirst()
        {
            _conditionMock.Setup(x => x.IsConditionTrueAsync(It.IsAny<AuthenticationContext>())).Returns(Task.FromResult(true));
            
            _authenticationBuilder
                .When<IConditionService>((x, c) => x.IsConditionTrue(c))
                .Call<IIpFilterService>((x, c) => x.CheckIp(c))
                .Call<IAuthenticationService>((x, c) => x.Authenticate(c))
                .GetClaimsUsing<IClaimsService>((x, t) => x.GetClaims(t));

            Action action1 = () => AuthenticationController.GetClaims<MyClaims>();
            Action action2 = () => AuthenticationController.GetClaimsOrDefault<MyClaims>();

            action1.Should().Throw<InvalidOperationException>().Which
                .Message.Should().StartWith($"Must be authenticated to call");
            action2.Should().NotThrow<Exception>();
        }

        [Fact]
        public async Task GetClaimsThrowExceptionWhenNotAuthenticated()
        {
            _authenticationBuilder
                .When(c => false)
                .Call<IAuthenticationService>((x, c) => x.Authenticate(c))
                .GetClaimsUsing<IClaimsService>((x, t) => x.GetClaims(t));

            await AuthenticationController.AuthenticateAsync(new MyMessage());

            AuthenticationController.IsAuthenticated.Should().Be(false);
            AuthenticationController.GetClaimsOrDefault<MyClaims>().Should().Be(null);
            Action action = () => AuthenticationController.GetClaims<MyClaims>();
            action.Should().Throw<InvalidOperationException>().Which
                .Message.Should().StartWith("Must be authenticated to call GetClaims()");
        }

        [Fact]
        public async Task IsNotAuthenticatedWhenNoRulesMatch()
        {
            _authenticationBuilder
                .When(c => false)
                .Call<IAuthenticationService>((x, c) => { })
                .GetClaimsUsing<IClaimsService>((x, t) => x.GetClaims(t));

            HttpContextProvider.HttpContext = new FakeHttpContext();
            await AuthenticationController.AuthenticateAsync(new MyMessage());

            AuthenticationController.IsAuthenticated.Should().Be(false);
            AuthenticationController.GetClaimsOrDefault<MyClaims>().Should().Be(null);
        }

        [Fact]
        public async Task IsAuthenticatedWhenOneRuleMatchendAuthenticateMethodDontThrowException()
        {
            _authenticationBuilder
                .When(c => true)
                .Call<IAuthenticationService>((x, c) => { })
                .GetClaimsUsing<IClaimsService>((x, t) => x.GetClaims(t));

            HttpContextProvider.HttpContext = new FakeHttpContext().Default(x => x.Request.Path = "/admin/stuff");
            await AuthenticationController.AuthenticateAsync(new MyMessage());

            AuthenticationController.IsAuthenticated.Should().Be(true);
        }

        [Fact]
        public async Task ExceptionWhenAuthenticationMethodReturnFalseAsync()
        {
            _authenticateMock.Setup(x => x.Authenticate(It.IsAny<AuthenticationContext>())).Throws(new AuthenticationException("auth error"));
            _authenticationBuilder
                .When(c => true)
                .Call<IAuthenticationService>((x, c) => x.Authenticate(c))
                .GetClaimsUsing<IClaimsService>((x, t) => x.GetClaims(t));

            Func<Task> action = () => AuthenticationController.AuthenticateAsync(new MyMessage());

            await action.Should().ThrowAsync<AuthenticationException>().WithMessage("auth error");
            _authenticateMock.Verify(x => x.Authenticate(It.IsAny<AuthenticationContext>()), Times.Once);
            AuthenticationController.IsAuthenticated.Should().Be(false);
        }

        [Fact]
        public void ExceptionWhenRuleWithoutGetClaimsMethod()
        {
            _authenticationBuilder
                .When(c => true)
                .Call<IAuthenticationService>((x, c) => x.Authenticate(c));

            AuthenticationOptions options;
            Action action = () => options = _authenticationBuilder.Options;

            action.Should().Throw<ConfigurationException>().Which
                .Message.Should().Contain($"must provide claims method with {nameof(AuthenticationBuilder.GetClaimsUsing)}");
        }

        [Fact]
        public async Task ManuallySetClaims()
        {
            var claims = new MyClaims();
            AuthenticationController.IsAuthenticated.Should().Be(false);
            
            AuthenticationController.UseClaims(claims);

            AuthenticationController.IsAuthenticated.Should().Be(true);
            AuthenticationController.GetClaims<MyClaims>().Should().Be(claims);

            await AuthenticationController.AuthenticateAsync(new MyMessage());

            AuthenticationController.IsAuthenticated.Should().Be(true);
            AuthenticationController.GetClaims<MyClaims>().Should().Be(claims);
        }

        [Fact]
        public async Task DontCallAuthenticationMethodsWhenAlreadyAuthenticated()
        {
            _authenticationBuilder
                .When(c => true)
                .Call<IAuthenticationService>((x, c) => x.Authenticate(c))
                .GetClaimsUsing<IClaimsService>((x, t) => x.GetClaims(t));

            var claims = new MyClaims();
            AuthenticationController.UseClaims(claims);
            AuthenticationController.IsAuthenticated.Should().Be(true);

            await AuthenticationController.AuthenticateAsync(new MyMessage());

            _authenticateMock.Verify(x => x.Authenticate(It.IsAny<AuthenticationContext>()), Times.Never);
        }

        [Fact]
        public async Task DefineFuntionToCallBeforeEachRule()
        {
            var httpContext = new FakeHttpContext();
            _authenticateMock.Setup(x => x.Authenticate(It.IsAny<AuthenticationContext>()));

            _authenticationBuilder
                .BeforeAuthenticate<IHttpContextProvider>((x, c) => c["httpContext"] = x.HttpContextOrDefault);
            _authenticationBuilder
                .When(c => true)
                .Call<IAuthenticationService>((x, c) => x.Authenticate(c))
                .GetClaimsUsing<IClaimsService>((x, t) => x.GetClaims(t));

            HttpContextProvider.HttpContext = httpContext;
            await AuthenticationController.AuthenticateAsync(new MyMessage());

            _authenticateMock.Verify(x =>
                x.Authenticate(It.Is<AuthenticationContext>(x => x["httpContext"] == httpContext)),
                Times.Once);
        }

        [Fact]
        public async Task GetClaimsOrDefaultWhileAuthenticatingAsync()
        {
            var httpContext = new FakeHttpContext();
            _claimsServiceMock.Setup(x => x.GetClaims(typeof(MyClaims))).Returns(new MyClaims());

            _authenticationBuilder
                .When(c => true)
                .Call<IAuthenticationController>((x, c) => { if (x.GetClaimsOrDefault<MyClaims>() == null) throw new Exception("null claims."); })
                .GetClaimsUsing<IClaimsService>((x, t) => x.GetClaims(t));

            Func<Task> action = () => AuthenticationController.AuthenticateAsync(new MyMessage());

            await action.Should().NotThrowAsync<Exception>();
        }

        [Fact]
        public async Task GetClaimsWhileAuthenticatingAsync()
        {
            var httpContext = new FakeHttpContext();
            _claimsServiceMock.Setup(x => x.GetClaims(typeof(MyClaims))).Returns(new MyClaims());

            _authenticationBuilder
                .When(c => true)
                .Call<IAuthenticationController>((x, c) => x.GetClaims<MyClaims>())
                .GetClaimsUsing<IClaimsService>((x, t) => x.GetClaims(t));

            Func<Task> action = () => AuthenticationController.AuthenticateAsync(new MyMessage());

            await action.Should().NotThrowAsync<Exception>();
        }

        [Fact]
        public async Task EnsureClaimsProviderRetureDefaultWhenCalledAfterExceptionDuringAuthentication()
        {
            var httpContext = new FakeHttpContext();
            _authenticateMock.Setup(x => x.Authenticate(It.IsAny<AuthenticationContext>())).Throws(new AuthenticationException("invalid token"));

            _authenticationBuilder
                .When(c => true)
                .Call<IAuthenticationService>((x, c) => x.Authenticate(c))
                .GetClaimsUsing<IClaimsService>((x, t) => x.GetClaims(t));

            try
            {
                await AuthenticationController.AuthenticateAsync(new MyMessage());
            }
            catch(Exception)
            {
                AuthenticationController.GetClaimsOrDefault<object>().Should().Be(null);
                _claimsServiceMock.Verify(x => x.GetClaims(It.IsAny<Type>()), Times.Never());
                return;
            }

            true.Should().Be(false, "must not go here");
        }

        public interface IConditionService
        {
            bool IsConditionTrue(AuthenticationContext context);

            Task<bool> IsConditionTrueAsync(AuthenticationContext context);
        }

        public interface IIpFilterService
        {
            void CheckIp(AuthenticationContext context);

            Task CheckIpAsync(AuthenticationContext context);
        }

        public interface IAuthenticationService
        {
            void Authenticate(AuthenticationContext context);

            Task AuthenticateAsync(AuthenticationContext context);
        }

        public interface IClaimsService
        {
            object GetClaims(Type claimsType);
        }

        public class MyMessage
        {
        }

        public class MyClaims
        {
            public DateTime ExpireAt { get; set; }
        }
    }
}
