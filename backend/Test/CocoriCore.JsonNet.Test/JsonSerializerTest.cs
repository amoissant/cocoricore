﻿using FluentAssertions;
using System;
using Xunit;

namespace CocoriCore.JsonNet.Test
{
    public class JsonSerializerTest
    {
        private readonly ObjectSerializer _serializer;

        public JsonSerializerTest()
        {
            _serializer = new ObjectSerializer();
        }

        public class AClass
        {
            public object Value;
        }

        [Fact]
        public void SerializeGuidValue()
        {
            var obj = new AClass();
            obj.Value = new Guid("0326cebc-7c57-4719-a206-5d5ed5070319");

            var json = _serializer.Serialize(obj);
            json.Should().Be("{\"value\":{\"_t\":\"System.Guid\",\"_v\":\"0326cebc-7c57-4719-a206-5d5ed5070319\"}}");
        }

        [Fact]
        public void SerializeIntValue()
        {
            var obj = new AClass();
            obj.Value = 12;

            var json = _serializer.Serialize(obj);
            json.Should().Be("{\"value\":{\"_t\":\"System.Int32\",\"_v\":12}}");
        }

        [Fact]
        public void SerializeInstanceValue()
        {
            var obj = new AClass();
            obj.Value = new AValueClass { Id = new Guid("0326cebc-7c57-4719-a206-5d5ed5070319"), Name = "toto" };

            var json = _serializer.Serialize(obj);
            json.Should().Be("{\"value\":{\"_t\":\"CocoriCore.JsonNet.Test.AValueClass\",\"_v\":{\"id\":\"0326cebc-7c57-4719-a206-5d5ed5070319\",\"name\":\"toto\"}}}");
        }

        [Fact]
        public void DeserializeGuidValue()
        {
            var obj = new AClass();
            obj.Value = new Guid("0326cebc-7c57-4719-a206-5d5ed5070319");

            var json = _serializer.Serialize(obj);
            var obj2 = _serializer.Deserialize<AClass>(json);

            obj2.Should().BeOfType<AClass>()
                .Subject.Value.Should().Be(new Guid("0326cebc-7c57-4719-a206-5d5ed5070319"));
        }

        [Fact]
        public void DeserializeIntValue()
        {
            var obj = new AClass();
            obj.Value = 12;

            var json = _serializer.Serialize(obj);
            var obj2 = _serializer.Deserialize<AClass>(json);

            obj2.Should().BeOfType<AClass>()
                .Subject.Value.Should().Be(12);
        }

        [Fact]
        public void DeserializeInstanceValue()
        {
            var obj = new AClass();
            obj.Value = new AValueClass { Id = new Guid("0326cebc-7c57-4719-a206-5d5ed5070319"), Name = "toto" };

            var json = _serializer.Serialize(obj);
            var obj2 = _serializer.Deserialize<AClass>(json);

            var value = obj2.Should().BeOfType<AClass>()
                .Subject.Value.Should().BeOfType<AValueClass>().Subject;
            value.Id.Should().Be(new Guid("0326cebc-7c57-4719-a206-5d5ed5070319"));
            value.Name.Should().Be("toto");
        }

        [Fact]
        public void SerializeNullAsObject()
        {
            string json = _serializer.Serialize<object>(null);

            json.Should().Be("null");
        }

        [Fact]
        public void SerializeGuidAsObject()
        {
            var obj = new Guid("0326cebc-7c57-4719-a206-5d5ed5070319");

            string json = _serializer.Serialize<object>(obj);

            json.Should().Be("{\"_t\":\"System.Guid\",\"_v\":\"0326cebc-7c57-4719-a206-5d5ed5070319\"}");
        }

        [Fact]
        public void SerializeIntAsObject()
        {
            var obj = 12;

            string json = _serializer.Serialize<object>(obj);

            json.Should().Be("{\"_t\":\"System.Int32\",\"_v\":12}");
        }

        [Fact]
        public void SerializeInstanceAsObject()
        {
            var obj = new AValueClass { Id = new Guid("0326cebc-7c57-4719-a206-5d5ed5070319"), Name = "toto" };

            string json = _serializer.Serialize<object>(obj);

            json.Should().Be("{\"_t\":\"CocoriCore.JsonNet.Test.AValueClass\",\"_v\":{\"id\":\"0326cebc-7c57-4719-a206-5d5ed5070319\",\"name\":\"toto\"}}");
        }

        [Fact]
        public void DeserializeGuidAsObject()
        {
            var obj = new Guid("0326cebc-7c57-4719-a206-5d5ed5070319");

            var json = _serializer.Serialize((object)obj);
            var obj2 = _serializer.Deserialize<object>(json);

            obj2.Should().Be(new Guid("0326cebc-7c57-4719-a206-5d5ed5070319"));
        }

        [Fact]
        public void DeserializeIntAsObject()
        {
            var obj = 12;

            var json = _serializer.Serialize((object)obj);
            var obj2 = _serializer.Deserialize<object>(json);

            obj2.Should().Be(12);
        }

        [Fact]
        public void DeserializeInstanceAsObject()
        {
            var obj = new AValueClass { Id = new Guid("0326cebc-7c57-4719-a206-5d5ed5070319"), Name = "toto" };

            var json = _serializer.Serialize((object)obj);
            var obj2 = _serializer.Deserialize<object>(json);

            var value = obj2.Should().BeOfType<AValueClass>().Subject;
            value.Id.Should().Be(new Guid("0326cebc-7c57-4719-a206-5d5ed5070319"));
            value.Name.Should().Be("toto");
        }

        [Fact]
        public void DeserializeObjectContainingNullObject()
        {
            var obj = new AClassWithAnotherObject { Name = "toto", Object = null };

            var json = _serializer.Serialize((object)obj);
            var obj2 = _serializer.Deserialize<object>(json);

            var value = obj2.Should().BeOfType<AClassWithAnotherObject>().Subject;
            value.Name.Should().Be("toto");
            value.Object.Should().BeNull();
        }

        [Fact]
        public void DeserializeObjectContainingAnotherObject()
        {
            var obj = new AClassWithAnotherObject 
            { 
                Name = "toto", 
                Object = new AValueClass { Id = new Guid("0326cebc-7c57-4719-a206-5d5ed5070319") } 
            };

            var json = _serializer.Serialize((object)obj);
            var obj2 = _serializer.Deserialize<object>(json);

            var value = obj2.Should().BeOfType<AClassWithAnotherObject>().Subject;
            value.Name.Should().Be("toto");
            var innerObject = value.Object.Should().BeOfType<AValueClass>().Which;
            innerObject.Id.Should().Be(new Guid("0326cebc-7c57-4719-a206-5d5ed5070319"));
            innerObject.Name.Should().BeNull();
        }
    }
    
    public class AValueClass
    {
        public Guid Id;
        public string Name;
    }

    public class AClassWithAnotherObject
    {
        public string Name;
        public object Object;
    }
}
