using System;
using Xunit;
using FluentAssertions;
using Newtonsoft.Json;

namespace CocoriCore.JsonNet.Test
{
    public class DynamicObjectConverterTest
    {
        [Fact]
        public void CanDeserializeRecursivelyDynamicField()
        {
            var json = @"{""stringField"": ""toto"", 
                          ""dynamicField"": {""intField"": 2, 
                                             ""dateTimeField"": ""2018-05-23T21:51:45.508Z"", 
                                             ""arrayField"": [{""aField"": 3}, ""stringValue""]}}";
            var serializer = new JsonSerializer();
            serializer.Converters.Add(new DynamicObjectConverter());
            var anObject = serializer.Deserialize<AnObject>(json);

            anObject.stringField.Should().Be("toto");
            var dynamicValue = anObject.dynamicField;
            ((object)dynamicValue).Should().NotBeNull();
            ((object)dynamicValue.intField).Should().Be(2L);
            ((object)dynamicValue.dateTimeField).Should().Be(DateTime.Parse("2018-05-23T21:51:45.508Z").ToUniversalTime());
            ((object)dynamicValue.arrayField[0].aField).Should().Be(3L);
            ((object)dynamicValue.arrayField[1]).Should().Be("stringValue");
        }

        class AnObject
        {
            public string stringField = null;
            public dynamic dynamicField = null;
        }
    }
}
