﻿using FluentAssertions;
using Newtonsoft.Json;
using System.Collections.Generic;
using Xunit;

namespace CocoriCore.JsonNet.Test
{
    public class NoNullEnumerableResolverTest
    {
        private JsonSerializer _serializer;

        public NoNullEnumerableResolverTest()
        {
            _serializer = new JsonSerializer();
            _serializer.ContractResolver = new NoNullEnumerableResolver();
        }

        [Fact]
        public void DeserializeEmptyObject()
        {
            var deserializedObject = _serializer.Deserialize<WithCollection>(@"{}");

            deserializedObject.fArray.Should().NotBeNull();
            deserializedObject.fList.Should().NotBeNull();
            deserializedObject.fHashSet.Should().NotBeNull();
            deserializedObject.fIList.Should().NotBeNull();
            deserializedObject.fIEnumerable.Should().NotBeNull();
            deserializedObject.fICollection.Should().NotBeNull();
            deserializedObject.fInt.Should().Be(0);

            deserializedObject.pArray.Should().NotBeNull();
            deserializedObject.pList.Should().NotBeNull();
            deserializedObject.pHashSet.Should().NotBeNull();
            deserializedObject.pIList.Should().NotBeNull();
            deserializedObject.pIEnumerable.Should().NotBeNull();
            deserializedObject.pICollection.Should().NotBeNull();
            deserializedObject.pInt.Should().Be(0);
        }

        [Fact]
        public void DeserializeWithNullAndValues()
        {
            var deserializedObject = _serializer.Deserialize<WithCollection>(@"{""fArray"": null, ""fList"": [1, 2, 3], ""pArray"": [1, 2, 3], ""pList"": null}");

            deserializedObject.fArray.Should().NotBeNull();
            deserializedObject.fList.Should().BeEquivalentTo(new[] { 1, 2, 3 });
            deserializedObject.fHashSet.Should().NotBeNull();
            deserializedObject.fIList.Should().NotBeNull();
            deserializedObject.fIEnumerable.Should().NotBeNull();
            deserializedObject.fICollection.Should().NotBeNull();

            deserializedObject.pArray.Should().BeEquivalentTo(new[] { 1, 2, 3 });
            deserializedObject.pList.Should().NotBeNull();
            deserializedObject.pHashSet.Should().NotBeNull();
            deserializedObject.pIList.Should().NotBeNull();
            deserializedObject.pIEnumerable.Should().NotBeNull();
            deserializedObject.pICollection.Should().NotBeNull();
        }

        [Fact]
        public void DeserializeToEnumerable()
        {
            var result = _serializer.Deserialize<IEnumerable<string>>(@"[ 'a', 'b', 'c' ]");

            result.Should().HaveCount(3).And.BeEquivalentTo(new[] { "a", "b", "c" });
        }

        [Fact]
        public void NoExceptionWhenIndexOrReadonlyProps()
        {
            var result = _serializer.Deserialize<AClassWithSpecialProperties>(@"{ ""ROProp"": [ ""a"", ""b"" ]}");

            result.ROProp.Should().BeNull();
        }

        class AClassWithSpecialProperties
        {
            private string[] _array;

            public IEnumerable<string> ROProp { get; }

            public string this[int index]
            {
                get => _array[index];
                set => _array[index] = value;
            }            
        }

        class WithCollection
        {
            public object[] fArray;
            public List<object> fList;
            public HashSet<object> fHashSet;
            public IList<object> fIList;
            public IEnumerable<object> fIEnumerable;
            public ICollection<object> fICollection;
            public int fInt;

            public object[] pArray { get; set; }
            public List<object> pList { get; set; }
            public HashSet<object> pHashSet { get; set; }
            public IList<object> pIList { get; set; }
            public IEnumerable<object> pIEnumerable { get; set; }
            public ICollection<object> pICollection { get; set; }
            public int pInt { get; set; }
    }
    }
}
