using System;
using Xunit;
using Microsoft.AspNetCore.Http;
using FluentAssertions;
using Microsoft.AspNetCore.Http.Internal;
using System.Threading.Tasks;
using System.Collections.Generic;
using CocoriCore.OData.Converter;
using Microsoft.AspNetCore.Http.Extensions;
using CocoriCore.TestUtils;
using CocoriCore.OData.Linq;
using Microsoft.Extensions.Caching.Memory;
using System.Linq;
using Autofac;
using Newtonsoft.Json;
using System.Reflection;
using CocoriCore.Autofac;
using CocoriCore.Messaging;

namespace CocoriCore.Router.Test
{
    public class RouterTest
    {
        private readonly Lazy<ILifetimeScope> _rootScope;
        private readonly ContainerBuilder _builder;
        private readonly RouterOptionsBuilder _routerBuilder;
        private readonly ClientCodeGeneratorOptionsBuilder _codeGeneratorBuilder;

        public RouterTest()
        {
            _builder = new ContainerBuilder();
            _rootScope = new Lazy<ILifetimeScope>(() => _builder.Build());
            _routerBuilder = new RouterOptionsBuilder();
            _codeGeneratorBuilder = new ClientCodeGeneratorOptionsBuilder();

            _builder.Register(c => _routerBuilder.Options).SingleInstance();
            _builder.Register(c => _codeGeneratorBuilder.Options).SingleInstance();
            _builder.RegisterType<Router>().AsSelf().SingleInstance();
            _builder.RegisterType<ResponseTypeProvider>()
                .AsImplementedInterfaces()
                .WithParameter(new TypedParameter<Assembly[]>(new[] { this.GetType().Assembly }))
                .SingleInstance();
            _builder.RegisterInstance(new MemoryCache(new MemoryCacheOptions())).As<IMemoryCache>();
            _builder.RegisterType<MyJsonSerializer>().As<JsonSerializer>().SingleInstance();
            _builder.RegisterType<MessageDeserializer>().AsImplementedInterfaces().SingleInstance();
        }

        private ILifetimeScope RootScope => _rootScope.Value;
        private Router Router => RootScope.Resolve<Router>();

        private async Task<object> FindMessageAsync(HttpRequest request)
        {
            var route = Router.TryFindMessageType(request);
            if (route != null)
            {
                return await Router.DeserializeMessageAsync<object>(request);
            }
            return null;
        }

        [Fact]
        public async Task CreateMessageForRouteWithParametersFromUrl()
        {
            _routerBuilder.Get<AQuery>()
                 .AddPath("user")
                 .AddPathParameter(m => m.idReferent)
                 .AddPath("edit")
                 .AddPathParameter(m => m.id)
                 .AddPathParameter(m => m.nb);

            Guid guid2 = Guid.NewGuid();
            Guid guid1 = Guid.NewGuid();
            int nb = 666;

            var httpRequest = new FakeHttpRequest()
                .Default(x => x.Path = "/user/" + guid1 + "/edit/" + guid2 + "/" + nb);

            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<AQuery>();

            AQuery query = (AQuery)message;
            query.idReferent.Should().Be(guid1);
            query.id.Should().Be(guid2);
            query.nb.Should().Be(nb);
        }

        [Fact]
        public async Task CreateMessageForRouteWithParametersFromUrlAndQuery()
        {
            _routerBuilder.Get<AQuery>()
                 .AddPath("user")
                 .AddPathParameter(m => m.idReferent)
                 .AddPath("edit")
                 .AddPathParameter(m => m.id)
                 .UseQuery();

            Guid guid2 = Guid.NewGuid();
            Guid guid1 = Guid.NewGuid();

            var httpRequest = new FakeHttpRequest().Default(
                x => x.Path = "/user/" + guid1 + "/edit/" + guid2,
                x => x.QueryString = new QueryString("?nb=666&lat=44.825&p=unused"));

            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<AQuery>();

            AQuery query = (AQuery)message;
            query.idReferent.Should().Be(guid1);
            query.id.Should().Be(guid2);
            query.nb.Should().Be(666);
            query.lat.Should().Be(44.825);
        }

        [Fact]
        public async Task CreateMessageForRouteWithParametersFromUrlAndBody()
        {
            _routerBuilder.Get<AQuery>()
                 .AddPath("user")
                 .AddPathParameter(m => m.idReferent)
                 .AddPath("edit")
                 .AddPathParameter(m => m.id)
                 .UseBody();

            Guid guid2 = Guid.NewGuid();
            Guid guid1 = Guid.NewGuid();

            var httpRequest = new FakeHttpRequest().Default(
                x => x.Path = "/user/" + guid1 + "/edit/" + guid2,
                x => x.SetTextBody("{\"lat\": 44.825, \"nb\": 666}"));

            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<AQuery>();

            AQuery query = (AQuery)message;
            query.idReferent.Should().Be(guid1);
            query.id.Should().Be(guid2);
            query.nb.Should().Be(666);
            query.lat.Should().Be(44.825);
        }

        [Fact]
        public async Task KeepPathParameterWhenEmptyQueryString()
        {
            _routerBuilder.Get<AQuery>()
                 .AddPath("user")
                 .AddPathParameter(m => m.idReferent)
                 .UseQuery();

            Guid guid = Guid.NewGuid();

            var httpRequest = new FakeHttpRequest()
                .Default(x => x.Path = "/user/" + guid);

            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<AQuery>();

            AQuery query = (AQuery)message;
            query.idReferent.Should().Be(guid);
        }

        [Fact]
        public async Task CanSetPathWithString()
        {
            _routerBuilder.Get<AnotherCommand>().SetPath("test/path");
            var httpRequest = new FakeHttpRequest().Default(x => x.Path = "/test/path");
            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
        }

        [Fact]
        public async Task CanSetPathWithSimpleStringFunc()
        {
            _routerBuilder.Get<AnotherCommand>().SetPath(x => "test/path");
            var httpRequest = new FakeHttpRequest().Default(x => x.Path = "/test/path");
            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
        }

        [Fact]
        public async Task CanSetPathWithInterpolatedString()
        {
            _routerBuilder.Get<AnotherCommand>().SetPath(x => $"test/{x.Data}");
            var httpRequest = new FakeHttpRequest().Default(x => x.Path = "/test/12");
            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<AnotherCommand>();
            ((AnotherCommand)message).Data.Should().Be("12");
        }

        [Fact]
        public void CannotSetPathWithSomethingElseThanInterpolatedString()
        {
            Action action = () => _routerBuilder.Get<AnotherCommand>().SetPath(x => x == null ? "/a" : "/b");
            action.Should().Throw<InvalidOperationException>();

            action = () => _routerBuilder.Post<AnotherCommand>().SetPath(x => "test".Substring(1, 2));
            action.Should().Throw<InvalidOperationException>();
        }

        [Fact]
        public void AllowSameRoutesWhenMethodIsDifferent()
        {
            _routerBuilder.Get<AQuery>()
                 .AddPath("user");

            _routerBuilder.Post<ACommand>()
                 .AddPath("user");

            _routerBuilder.Put<ACommand>()
                 .AddPath("user");

            _routerBuilder.Delete<ACommand>()
                 .AddPath("user");

            Action action = () => Router.CheckDuplicatesRoutes();

            action.Should().NotThrow<Exception>();
        }

        [Fact]
        public void ExceptionIfDuplicateRoutes()
        {
            _routerBuilder.Get<AQuery>()
                 .AddPath("user")
                 .AddPathParameter(m => m.idReferent)
                 .AddPath("edit")
                 .AddPathParameter(m => m.id);

            _routerBuilder.Get<AQuery>()
                 .AddPath("user")
                 .AddPathParameter(m => m.id)
                 .AddPath("edit")
                 .AddPathParameter(m => m.idReferent);

            Action action = () => Router.CheckDuplicatesRoutes();

            var exception = action.Should().Throw<DuplicateRoutesException>().Which;
            exception.Message.Should().Contain("edit");
            exception.Message.Should().Contain("user");
        }


        [Theory]
        [InlineData(null)]
        [InlineData("truc")]
        [InlineData("POST")]
        public async Task NoErrorIfMethodIsNotSupported(string method)
        {
            var httpRequest = new FakeHttpRequest().Default(x => x.Method = method);
            var message = await FindMessageAsync(httpRequest);
            message.Should().BeNull();
        }

        [Theory]
        [InlineData("{\"nb\": NaN}")]
        [InlineData("    ")]
        public async Task ThrowExceptionIfCantDeserializeMessageAsync(string body)
        {
            _routerBuilder.Post<ACommand>()
                 .UseBody();

            var httpRequest = new FakeHttpRequest().Default(
                x => x.Method = "POST",
                x => x.SetTextBody(body));

            Func<Task> action = async () => await FindMessageAsync(httpRequest);

            await action.Should().ThrowAsync<DeserializeMessageException>();
        }

        [Fact]
        public async Task CreateMessageForRouteWithEmptyBody()
        {
            _routerBuilder.Get<AQuery>()
                 .UseBody();

            var httpRequest = new FakeHttpRequest().Default(
                x => x.SetTextBody(string.Empty));

            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<AQuery>();

            AQuery query = (AQuery)message;
            query.idReferent.Should().Be(Guid.Empty);
            query.id.Should().Be(Guid.Empty);
            query.nb.Should().Be(0);
            query.lat.Should().Be(0);
        }

        [Fact]
        public void ThrowExceptionIfMessageExpressionWithdepth()
        {
            Action action = () => _routerBuilder.Get<AQuery>()
                 .AddPathParameter(m => m.stringObject.Value);

            var exception = action.Should().Throw<InvalidOperationException>().Which;
            exception.Message.Should().Contain("stringObject.Value");
            exception.Message.Should().Contain("depth");
        }

        [Fact]
        public async Task CanAddRegexForCustomType()
        {
            _routerBuilder.SetPatternForType("[a-z]+", typeof(CustomType));

            _routerBuilder.Get<AQuery>()
                 .AddPathParameter(m => m.customTypeValue);

            var httpRequest = new FakeHttpRequest()
                .Default(x => x.Path = "/toto");

            AMessage message = (AMessage)await FindMessageAsync(httpRequest);
            message.customTypeValue.ToString().Should().Be("toto");
        }

        [Fact]
        public async Task CantFindRouteIfRegexDoesntMatchForCustomType()
        {
            _routerBuilder.SetPatternForType("[a-z]+", typeof(CustomType));

            _routerBuilder.Get<AQuery>()
                 .AddPathParameter(m => m.customTypeValue);

            var httpRequest = new FakeHttpRequest()
                .Default(x => x.Path = "/toto123");

            (await FindMessageAsync(httpRequest)).Should().BeNull();
        }

        [Fact]
        public void ExceptionIfNoPatternForCustomType()
        {
            Action action = () => _routerBuilder.Get<AQuery>().AddPathParameter(m => m.customTypeValue);

            action.Should().Throw<NotSupportedException>().Which
                .Message.Should().Contain($"No pattern provided for type {typeof(CustomType)}")
                .And.Contain($"{nameof(RouterOptionsBuilder.SetPatternForType)}");
        }

        [Fact]
        public async Task CanPassArrayIntoQueryStringFirstWay()
        {
            _routerBuilder.Get<AQuery>()
                 .AddPath("testArray")
                 .UseQuery();

            var httpRequest = new FakeHttpRequest().Default(
                    x => x.Path = "/testArray",
                    x => x.QueryString = new QueryString("?tab[]=t&tab[]=o&tab[]=t&tab[]=o"));

            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<AQuery>();

            AQuery query = (AQuery)message;
            query.tab.Should().HaveCount(4);
            string.Join("", query.tab).Should().Be("toto");
        }

        [Fact]
        public async Task CanPassArrayIntoQueryStringSecondWay()
        {
            _routerBuilder.Get<AQuery>()
                 .AddPath("testArray")
                 .UseQuery();

            var httpRequest = new FakeHttpRequest().Default(
                    x => x.Path = "/testArray",
                    x => x.QueryString = new QueryString("?tab=[\"t\",\"o\",\"t\",\"o\"]"));

            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<AQuery>();

            AQuery query = (AQuery)message;
            query.tab.Should().HaveCount(4);
            string.Join("", query.tab).Should().Be("toto");
        }

        [Fact]
        public async Task CanPassArrayIntoQueryStringThirdWay()
        {
            _routerBuilder.Get<AQuery>()
                 .AddPath("testArray")
                 .UseQuery();

            var httpRequest = new FakeHttpRequest().Default(
                    x => x.Path = "/testArray",
                    x => x.QueryString = new QueryString("?tab=['t','o','t','o']"));

            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<AQuery>();

            AQuery query = (AQuery)message;
            query.tab.Should().HaveCount(4);
            string.Join("", query.tab).Should().Be("toto");
        }

        [Fact]
        public async Task CanPassHeader()
        {
            _routerBuilder.Get<AQuery>()
                 .AddPath("testHeader")
                 .AddHeader("test-header", x => x.name);

            var httpRequest = new FakeHttpRequest().Default(
                    x => x.Path = "/testHeader",
                    x => x.Headers.Add("test-header", "test value"));

            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<AQuery>();

            AQuery query = (AQuery)message;
            query.name.Should().Be("test value");
        }

        [Fact]
        public async Task CanUseCookie()
        {
            _routerBuilder.Get<ATokenQuery>()
                 .AddPath("testCookie")
                 .UseCookies();

            var httpRequest = new FakeHttpRequest().Default(
                    x => x.Path = "/testCookie",
                    x => x.Cookies = RequestCookieCollection.Parse(new List<string>
                    {
                        "refreshToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9; Secure; HttpOnly;"
                    }));

            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<ATokenQuery>();

            ATokenQuery query = (ATokenQuery)message;
            query.refreshToken.Should().Be("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9");
        }

        [Fact]
        public async Task UrlWithoutAndWithUseQueryAreDifferent()
        {
            _routerBuilder.Get<GetAllQuery>().AddPath("toto");
            _routerBuilder.Get<ASearchQuery>().AddPath("toto").UseQuery();

            Action action = () => Router.CheckDuplicatesRoutes();
            action.Should().NotThrow<DuplicateRoutesException>();

            var httpRequest1 = new FakeHttpRequest().Default(
                    x => x.Path = "/toto");
            var message1 = await FindMessageAsync(httpRequest1);
            message1.Should().NotBeNull();
            message1.Should().BeOfType<GetAllQuery>();

            var httpRequest2 = new FakeHttpRequest().Default(
                   x => x.Path = "/toto",
                   x => x.QueryString = new QueryString("?keywords=truc"));
            var message2 = await FindMessageAsync(httpRequest2);
            message2.Should().NotBeNull();
            message2.Should().BeOfType<ASearchQuery>();
        }

        [Fact]
        public async Task RouteUsingQueryCanMatchIfNoOtherRouteWhenNoQueryString()
        {
            _routerBuilder.Get<ASearchQuery>().AddPath("toto").UseQuery();

            var httpRequest1 = new FakeHttpRequest().Default(
                    x => x.Path = "/toto");
            var message1 = await FindMessageAsync(httpRequest1);
            message1.Should().NotBeNull();
            message1.Should().BeOfType<ASearchQuery>();

            var httpRequest2 = new FakeHttpRequest().Default(
                   x => x.Path = "/toto",
                   x => x.QueryString = new QueryString("?keywords=truc"));
            var message2 = await FindMessageAsync(httpRequest2);
            message2.Should().NotBeNull();
            message2.Should().BeOfType<ASearchQuery>();
        }

        [Fact]
        public async Task ThrowExceptionWhenSeveralRoutesMathUrlAsync()
        {
            _routerBuilder.Get<ASearchQuery>().AddPath("toto").UseQuery();
            _routerBuilder.Get<AQuery>().AddPath("toto").UseQuery();

            var httpRequest = new FakeHttpRequest().Default(
                    x => x.Path = "/toto");

            Func<Task<object>> action = () => FindMessageAsync(httpRequest);

            var assertion = await action.Should().ThrowAsync<DuplicateRoutesException>();
            assertion.Which.Message.Should().Contain(httpRequest.GetDisplayUrl())
                .And.Contain("/toto")
                .And.Contain(nameof(ASearchQuery))
                .And.Contain(nameof(AQuery))
                .And.Contain(nameof(Router.CheckDuplicatesRoutes));
        }

        [Theory]
        [InlineData("truc+machin")]
        [InlineData("truc machin")]
        [InlineData("truc%20machin")]
        public async Task ReplacePlusBySpaceForQueryStringParameters(string keywords)
        {
            _routerBuilder.Get<ASearchQuery>().AddPath("toto").UseQuery();

            var httpRequest = new FakeHttpRequest().Default(
                   x => x.Path = "/toto",
                   x => x.QueryString = new QueryString($"?keywords={keywords}"));
            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<ASearchQuery>().Subject.keywords.Should().Be("truc machin");
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public async Task NoErrorIfEmptyBodyAndUseBody(string body)
        {
            _routerBuilder.Post<AnotherCommand>()
                 .AddPath("toto")
                 .UseBody();

            var httpRequest = new FakeHttpRequest()
                .Default(x => x.Path = "/toto",
                         x => x.Method = HttpMethods.Post,
                         x => x.SetTextBody(body));

            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<AnotherCommand>();

            AnotherCommand command = (AnotherCommand)message;
            command.Data.Should().BeNull();
        }

        [Theory]
        [InlineData("toto")]
        [InlineData("TOTO")]
        [InlineData("tOTo")]
        public async Task RoutesAreNotCaseSensitive(string path)
        {
            _routerBuilder.Get<AQuery>().AddPath("toto");

            Action action = () => Router.CheckDuplicatesRoutes();
            action.Should().NotThrow<DuplicateRoutesException>();

            var httpRequest = new FakeHttpRequest().Default(
                    x => x.Path = "/" + path);
            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<AQuery>();
        }

        [Fact]
        public async Task TestDollarTopAndSkip()
        {
            _routerBuilder.Get<AnODataQuery>().AddPath("odata").UseQuery();

            var httpRequest = new FakeHttpRequest().Default(
                x => x.Path = "/odata",
                x => x.QueryString = new QueryString("?$top=5&$skip=10"));

            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<AnODataQuery>();

            AnODataQuery query = (AnODataQuery)message;
            query.Top.Should().Be(5);
            query.Skip.Should().Be(10);
        }

        [Theory]
        [InlineData("?$orderBy=myField", OrderDirection.Asc)]
        [InlineData("?$orderBy=myField asc", OrderDirection.Asc)]
        [InlineData("?$orderBy=myField Asc", OrderDirection.Asc)]
        [InlineData("?$orderBy=myField ASC", OrderDirection.Asc)]
        [InlineData("?$orderBy=myField desc", OrderDirection.Desc)]
        [InlineData("?$orderBy=myField Desc", OrderDirection.Desc)]
        [InlineData("?$orderBy=myField DESC", OrderDirection.Desc)]
        public async Task TestODataOrderBy(string queryString, OrderDirection direction)
        {
            _routerBuilder.Get<AnODataQuery>().AddPath("odata").UseQuery();

            var httpRequest = new FakeHttpRequest().Default(
                x => x.Path = "/odata",
                x => x.QueryString = new QueryString(queryString));

            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<AnODataQuery>();

            AnODataQuery query = (AnODataQuery)message;
            query.OrderBy.Direction.Should().Be(direction);
            query.OrderBy.Member.Should().Be("myField");
        }

        [Fact]
        public async Task CanDeserializeMessageWithLeadingOrTrailingWhiteSpace()
        {
            _routerBuilder.Get<ACommand>().AddPath("test").UseBody();

            var httpRequest = new FakeHttpRequest().Default(
                x => x.Path = "/test",
                x => x.SetTextBody(" {\"name\":\"toto\"} "));

            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<ACommand>();

            ACommand command = (ACommand)message;
            command.name.Should().Be("toto");
        }

        [Fact]
        public async Task CanDeserializeArrayMessage()
        {
            _routerBuilder.Get<Batch<ACommand>>().AddPath("test").UseBody();

            var httpRequest = new FakeHttpRequest().Default(
                x => x.Path = "/test",
                x => x.SetTextBody("{Messages: [{\"name\":\"toto\"}, {\"name\":\"titi\"}]}"));

            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            var batch = message.Should().BeOfType<Batch<ACommand>>().Which;
            batch.Messages[0].name.Should().Be("toto");
            batch.Messages[1].name.Should().Be("titi");
        }

        [Fact]
        public async Task CanPassUrlEncodedValuesInPath()
        {
            _routerBuilder.Get<AQuery>().AddPathParameter(x => x.name);

            var httpRequest1 = new FakeHttpRequest().Default(
                    x => x.Path = "/t%C3%B4to");
            var message1 = await FindMessageAsync(httpRequest1);
            message1.Should().NotBeNull();
            message1.Should().BeOfType<AQuery>().Which.name.Should().Be("tôto");
        }

        [Fact]
        public void CanGetPathBeforeVariable()
        {
            _routerBuilder.Get<AQuery>().AddPath("toto").AddPath("test").AddPathParameter(x => x.name).AddPath("truc"); ;
            var route = Router.GetRouteDescriptors().Single(x => x.MessageType == typeof(AQuery));
            
            route.GetUrlBeforeFirstParameter().Should().Be("/toto/test");
        }

        [Fact]
        public async Task CreateMessageForPatchRoute()
        {
            _routerBuilder.Patch<State1Command>().AddPath("test").UseBody();
            _routerBuilder.Patch<State2Command>().AddPath("test").UseBody();

            var httpRequest1 = new FakeHttpRequest().Default(
                x => x.Method = HttpMethods.Patch,
                x => x.Path = "/test",
                x => x.SetTextBody("{\"name\":\"toto\"}"));

            var message1 = await FindMessageAsync(httpRequest1);
            message1.Should().NotBeNull();
            message1.Should().BeOfType<State1Command>();

            var httpRequest2 = new FakeHttpRequest().Default(
                x => x.Method = HttpMethods.Patch,
                x => x.Path = "/test",
                x => x.SetTextBody("{\"IsArchived\": true}"));

            var message2 = await FindMessageAsync(httpRequest2);
            message2.Should().NotBeNull();
            message2.Should().BeOfType<State2Command>();
        }

        [Fact]
        public async Task ExceptionIfCanDetermineAUniqueMatchingPatchRouteAsync()
        {
            _routerBuilder.Patch<State1Command>().AddPath("test").UseBody();
            _routerBuilder.Patch<State3Command>().AddPath("test").UseBody();

            var httpRequest = new FakeHttpRequest().Default(
                x => x.Method = HttpMethods.Patch,
                x => x.Path = "/test",
                x => x.SetTextBody("{\"name\":\"toto\"}"));

            Func<Task> action = () => FindMessageAsync(httpRequest);

            var assertion = await action.Should().ThrowAsync<DuplicateRoutesException>();
            assertion.Which
                .Message
                .Should().Contain(nameof(State1Command))
                .And.Contain(nameof(State3Command));
        }

        [Fact]
        public async Task HasBuildInUrlParameterPatterns()
        {
            _routerBuilder.Get<PatternQuery>().AddPath("String").AddPathParameter(x => x.String);
            _routerBuilder.Get<PatternQuery>().AddPath("Guid").AddPathParameter(x => x.Guid);
            _routerBuilder.Get<PatternQuery>().AddPath("Decimal").AddPathParameter(x => x.Decimal);
            _routerBuilder.Get<PatternQuery>().AddPath("Double").AddPathParameter(x => x.Double);
            _routerBuilder.Get<PatternQuery>().AddPath("Float").AddPathParameter(x => x.Float);
            _routerBuilder.Get<PatternQuery>().AddPath("Int").AddPathParameter(x => x.Int);
            _routerBuilder.Get<PatternQuery>().AddPath("Uint").AddPathParameter(x => x.Uint);
            _routerBuilder.Get<PatternQuery>().AddPath("Long").AddPathParameter(x => x.Long);
            _routerBuilder.Get<PatternQuery>().AddPath("Ulong").AddPathParameter(x => x.Ulong);
            _routerBuilder.Get<PatternQuery>().AddPath("Short").AddPathParameter(x => x.Short);
            _routerBuilder.Get<PatternQuery>().AddPath("Ushort").AddPathParameter(x => x.Ushort);
            _routerBuilder.Get<PatternQuery>().AddPath("MyEnum").AddPathParameter(x => x.MyEnum);

            var httpRequest = new FakeHttpRequest().Default(x => x.Method = HttpMethods.Get);
            PatternQuery message;

            httpRequest.Path = "/String/toto";
            message = (PatternQuery)await FindMessageAsync(httpRequest);
            message.String.Should().Be("toto");

            httpRequest.Path = "/Guid/00000000-0000-0000-0000-000000000000";
            message = (PatternQuery)await FindMessageAsync(httpRequest);
            message.Guid.Should().Be(Guid.Empty);

            httpRequest.Path = "/Decimal/7.3";
            message = (PatternQuery)await FindMessageAsync(httpRequest);
            message.Decimal.Should().Be(7.3m);

            httpRequest.Path = "/Double/7.2";
            message = (PatternQuery)await FindMessageAsync(httpRequest);
            message.Double.Should().Be(7.2);

            httpRequest.Path = "/Float/7.1";
            message = (PatternQuery)await FindMessageAsync(httpRequest);
            message.Float.Should().Be(7.1f);

            httpRequest.Path = "/Int/6";
            message = (PatternQuery)await FindMessageAsync(httpRequest);
            message.Int.Should().Be(6);

            httpRequest.Path = "/Uint/5";
            message = (PatternQuery)await FindMessageAsync(httpRequest);
            message.Uint.Should().Be(5);

            httpRequest.Path = "/Long/-4";
            message = (PatternQuery)await FindMessageAsync(httpRequest);
            message.Long.Should().Be(-4);

            httpRequest.Path = "/Ulong/3";
            message = (PatternQuery)await FindMessageAsync(httpRequest);
            message.Ulong.Should().Be(3);

            httpRequest.Path = "/Short/-2";
            message = (PatternQuery)await FindMessageAsync(httpRequest);
            message.Short.Should().Be(-2);

            httpRequest.Path = "/Ushort/1";
            message = (PatternQuery)await FindMessageAsync(httpRequest);
            message.Ushort.Should().Be(1);

            httpRequest.Path = "/MyEnum/value1";
            message = (PatternQuery)await FindMessageAsync(httpRequest);
            message.MyEnum.Should().Be(MyEnum.Value1);

            httpRequest.Path = "/MyEnum/1";
            message = (PatternQuery)await FindMessageAsync(httpRequest);
            message.MyEnum.Should().Be(MyEnum.Value1);
        }

        [Theory]
        [InlineData("/test/truc", typeof(AQuery))]
        [InlineData("/test/TRUC", typeof(AQuery))]
        [InlineData("/test/stuff", typeof(ASearchQuery))]
        public async Task SelectRouteWithExactPathMatch(string path, Type expectedType)
        {
            _routerBuilder.Get<AQuery>().SetPath($"test/truc");
            _routerBuilder.Get<ASearchQuery>().SetPath(x => $"test/{x.keywords}");

            var httpRequest = new FakeHttpRequest().Default(
                x => x.Method = HttpMethods.Get,
                x => x.Path = path);

            var message = await FindMessageAsync(httpRequest);

            message.Should().BeOfType(expectedType);
        }

        public class PatternQuery : ICommand
        {
            public string String;
            public Guid Guid;
            public decimal Decimal;
            public double Double;
            public float Float;
            public int Int;
            public uint Uint;
            public long Long;
            public ulong Ulong;
            public short Short;
            public ushort Ushort;
            public MyEnum MyEnum;
        }

        public enum MyEnum
        {
            None = 0,
            Value1 = 1,
            Value2 = 2
        }

        public class State1Command : ICommand
        {
            public string Name;
        }

        public class State2Command : ICommand
        {
            public bool IsArchived;
        }

        public class State3Command : ICommand
        {
            public string Name;
        }

        public class AnotherCommand : ICommand
        {
            public string Data;
        }

        public class ACommand : AMessage, ICommand
        {
        }

        public class AQuery : AMessage, IQuery
        {
        }

        public class GetAllQuery : IQuery
        {
        }

        public class ASearchQuery : IQuery
        {
            public string keywords;
        }

        public class ATokenQuery : IQuery
        {
            public string refreshToken;
        }

        public abstract class AMessage
        {
            public Guid id;
            public Guid idReferent;
            public string name;
            public int nb;
            public double lat;
            public AStringObject stringObject;
            public CustomType customTypeValue;
            public string[] tab;

            public AMessage()
            {
            }
        }

        public class AStringObject
        {
            public string Value;
        }

        public class CustomType
        {
            private string _value;
            public CustomType(string value)
            {
                _value = value;
            }

            public override string ToString()
            {
                return _value;
            }

            public static implicit operator CustomType(string value)
            {
                return new CustomType(value);
            }
        }

        public class AnODataQuery : IQuery
        {
            public int Top;
            public int Skip;
            public ODataOrderBy OrderBy;
        }

        class AHandler : QueryHandler<AQuery, object>
        {
            public override Task<object> ExecuteAsync(AQuery query)
            {
                throw new NotImplementedException();
            }
        }

        class ResponseTypeProvider : HandlerResponseTypeProvider, IResponseTypeProvider
        {
            public ResponseTypeProvider(params Assembly[] assemblies) : base(assemblies)
            {
            }
        }

        class MyJsonSerializer : JsonSerializer
        {
            public MyJsonSerializer()
            {
                Converters.Add(new ODataOrderByConverter());
            }
        }
    }
}
