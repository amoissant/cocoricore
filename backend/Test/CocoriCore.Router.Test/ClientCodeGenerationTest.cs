using System;
using Xunit;
using FluentAssertions;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Soltys.ChangeCase;
using CocoriCore.Common;
using CocoriCore.Types.Extension;
using System.Reflection;
using Autofac;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using CocoriCore.Autofac;

namespace CocoriCore.Router.Test
{
    public class ClientCodeGenerationTest
    {
        private readonly Lazy<ILifetimeScope> _rootScope;
        private readonly ContainerBuilder _builder;
        private readonly RouterOptionsBuilder _routerBuilder;
        private readonly ClientCodeGeneratorOptionsBuilder _codeGeneratorBuilder;

        public ClientCodeGenerationTest()
        {
            _builder = new ContainerBuilder();
            _rootScope = new Lazy<ILifetimeScope>(() => _builder.Build());
            _routerBuilder = new RouterOptionsBuilder();
            _codeGeneratorBuilder = new ClientCodeGeneratorOptionsBuilder();

            _builder.Register(c => _routerBuilder.Options).SingleInstance();
            _builder.Register(c => _codeGeneratorBuilder.Options).SingleInstance();
            _builder.RegisterType<Router>().AsImplementedInterfaces().SingleInstance();
            _builder.RegisterType<ClientCodeGenerator>().AsImplementedInterfaces().SingleInstance();
            _builder.RegisterType<ResponseTypeProvider>()
                .AsImplementedInterfaces()
                .WithParameter(new TypedParameter<Assembly[]>(new[] { this.GetType().Assembly }))
                .SingleInstance();
            _builder.RegisterInstance(new MemoryCache(new MemoryCacheOptions())).As<IMemoryCache>();
            _builder.RegisterType<JsonSerializer>().AsSelf().SingleInstance();
            _builder.RegisterType<MessageDeserializer>().AsImplementedInterfaces().SingleInstance();
        }

        private ILifetimeScope RootScope => _rootScope.Value;
        private IRouter Router => RootScope.Resolve<IRouter>();
        private IClientCodeGenerator ClientCodeGenerator => RootScope.Resolve<IClientCodeGenerator>();

        //TODO presets de correspondance de type
        //TODO ajouter fichier index.ts

        [Fact]
        public void GenerateFlatTypeWithDefaultOptions()
        {
            _routerBuilder.Get<AQuery>()
                 .AddPath("admin")
                 .AddPath("users")
                 .AddPathParameter(x => x.Id);

            var routeDescriptors = Router.GetRouteDescriptors();
            var clientDescriptors = ClientCodeGenerator.GenerateClientDescriptors(routeDescriptors);

            var clientDescriptor = clientDescriptors.Should().ContainSingle().Which;
            clientDescriptor.MessageType.ParameterizedUrl.Should().Be("/admin/users/id:guid");
            clientDescriptor.MessageType.RoutePath.Should().Be(new Path("admin/users"));
            clientDescriptor.MessageType.ClrType.Should().Be(typeof(AQuery));
            clientDescriptor.MessageType.Name.Should().Be(nameof(AQuery));
            clientDescriptor.MessageType.IsArray.Should().BeFalse();
            clientDescriptor.MessageType.Fields.Should().BeEquivalentTo(new[]
            {
                new Field("Id", "Guid"),
                new Field("OptionalId", "Guid", true, false)
            });
            clientDescriptor.MessageType.Imports.Should().BeEmpty();
            clientDescriptor.MessageType.IsEnum.Should().BeFalse();
            clientDescriptor.MessageType.Values.Should().BeEmpty();
            clientDescriptor.MessageType.InnerTypes.Should().BeNull();
            clientDescriptor.MessageType.RoutePath.Should().Be(new Path("admin/users"));

            clientDescriptor.ResponseType.IsArray.Should().BeFalse();
            clientDescriptor.ResponseType.ClrType.Should().Be(typeof(AResponse));
            clientDescriptor.ResponseType.Name.Should().Be(nameof(AResponse));
            clientDescriptor.ResponseType.Fields.Should().BeEquivalentTo(new[]
            {
                new Field("Id", "Guid"),
                new Field("Text", "String"),
                new Field("Date", "DateTime"),
                new Field("Count", "Int32"),
                new Field("Average", "Single"),
                new Field("Time", "TimeSpan", false, true),
                new Field("OptionalId", "Guid", true, false)
            });
            clientDescriptor.ResponseType.Imports.Should().BeEmpty();
            clientDescriptor.ResponseType.IsEnum.Should().BeFalse();
            clientDescriptor.ResponseType.Values.Should().BeEmpty();
            clientDescriptor.ResponseType.InnerTypes.Should().BeNull();
            clientDescriptor.ResponseType.ParameterizedUrl.Should().Be("/admin/users/id:guid");
            clientDescriptor.ResponseType.RoutePath.Should().Be(new Path("admin/users"));
        }

        [Fact]
        public void CanSetFieldNameTransformation()
        {
            _routerBuilder.Get<AQuery>()
                 .AddPath("admin")
                 .AddPath("users")
                 .AddPathParameter(x => x.Id);

            _codeGeneratorBuilder.TransformFieldNames(f => f.CamelCase());

            var routeDescriptors = Router.GetRouteDescriptors();
            var clientDescriptors = ClientCodeGenerator.GenerateClientDescriptors(routeDescriptors);

            var clientDescriptor = clientDescriptors.Should().ContainSingle().Which;
            clientDescriptor.MessageType.Fields.Should().BeEquivalentTo(new[]
            {
                new Field("id", "Guid"),
                new Field("optionalId", "Guid", true, false)
            });
            clientDescriptor.ResponseType.Fields.Should().BeEquivalentTo(new[]
            {
                new Field("id", "Guid"),
                new Field("text", "String"),
                new Field("date", "DateTime"),
                new Field("count", "Int32"),
                new Field("average", "Single"),
                new Field("time", "TimeSpan", false, true),
                new Field("optionalId", "Guid", true, false)
            });
        }

        [Fact]
        public void CanSetFieldTypeTransformation()
        {
            _routerBuilder.Get<AQuery>()
                 .AddPath("admin")
                 .AddPath("users")
                 .AddPathParameter(x => x.Id);

            _codeGeneratorBuilder.TransformFieldTypes(t => t.CamelCase());

            var routeDescriptors = Router.GetRouteDescriptors();
            var clientDescriptors = ClientCodeGenerator.GenerateClientDescriptors(routeDescriptors);

            var clientDescriptor = clientDescriptors.Should().ContainSingle().Which;
            clientDescriptor.MessageType.Fields.Should().BeEquivalentTo(new[]
            {
                new Field("Id", "guid"),
                new Field("OptionalId", "guid", true, false)
            });
            clientDescriptor.ResponseType.Fields.Should().BeEquivalentTo(new[]
            {
                new Field("Id", "guid"),
                new Field("Text", "string"),
                new Field("Date", "dateTime"),
                new Field("Count", "int32"),
                new Field("Average", "single"),
                new Field("Time", "timeSpan", false, true),
                new Field("OptionalId", "guid", true, false)
            });
        }

        [Fact]
        public void CanDefineTypeCorrespondences()
        {
            _routerBuilder.Get<AQuery>()
                 .AddPath("admin")
                 .AddPath("users")
                 .AddPathParameter(x => x.Id);

            _codeGeneratorBuilder
                .AddFieldTypeCorrespondence(typeof(Guid), "Guid")
                .AddFieldTypeCorrespondence(typeof(DateTime), "Date")
                .AddFieldTypeCorrespondence(typeof(float), "Number")
                .AddFieldTypeCorrespondence(typeof(TimeSpan), "String");

            var routeDescriptors = Router.GetRouteDescriptors();
            var clientDescriptors = ClientCodeGenerator.GenerateClientDescriptors(routeDescriptors);

            var clientDescriptor = clientDescriptors.Should().ContainSingle().Which;
            clientDescriptor.MessageType.RoutePath.Should().Be(new Path("admin/users"));
            clientDescriptor.MessageType.Name.Should().Be(nameof(AQuery));
            clientDescriptor.MessageType.Fields.Should().BeEquivalentTo(new[]
            {
                new Field("Id", "Guid"),
                new Field("OptionalId", "Guid", true, false)
            });
            clientDescriptor.ResponseType.Fields.Should().BeEquivalentTo(new[]
            {
                new Field("Id", "Guid"),
                new Field("Text", "String"),
                new Field("Date", "Date"),
                new Field("Count", "Int32"),
                new Field("Average", "Number"),
                new Field("Time", "String", false, true),
                new Field("OptionalId", "Guid", true, false)
            });
            clientDescriptor.ResponseType.Values.Should().BeEmpty();
        }

        [Fact]
        public void CanDefineImports()
        {
            _routerBuilder.Get<AQuery>()
                 .AddPath("admin")
                 .AddPath("users")
                 .AddPathParameter(x => x.Id);

            _codeGeneratorBuilder
                .AddConditionalImport((r, t) => t.HasMemberType<Guid>(), (r, t) => new
                {
                    name = "{ Guid }",
                    from = "guid-typescript"
                })
                .AddConditionalImport((r, t) => t.HasMemberType<TimeSpan>(), (r, t) => new
                {
                    name = "TimeSpan",
                    from = "time-import"
                });

            var routeDescriptors = Router.GetRouteDescriptors();
            var clientDescriptors = ClientCodeGenerator.GenerateClientDescriptors(routeDescriptors);

            var clientDescriptor = clientDescriptors.Should().ContainSingle().Which;
            clientDescriptor.MessageType.Imports.Should().BeEquivalentTo(new object[] {
                new {
                    name = "{ Guid }",
                    from = "guid-typescript"
                }
            });
            clientDescriptor.ResponseType.Imports.Should().BeEquivalentTo(new object[] {
                new {
                    name = "{ Guid }",
                    from = "guid-typescript"
                },
                new {
                    name = "TimeSpan",
                    from = "time-import"
                }
            });
        }

        [Fact]
        public void GenerateEnumerableTypeWithDefaultOptions()
        {
            _routerBuilder.Get<AEnumerableQuery>()
                 .AddPath("admin")
                 .AddPath("users");

            var routeDescriptors = Router.GetRouteDescriptors();
            var clientDescriptors = ClientCodeGenerator.GenerateClientDescriptors(routeDescriptors);

            var clientDescriptor = clientDescriptors.Should().ContainSingle().Which;
            clientDescriptor.MessageType.Name.Should().Be(nameof(AEnumerableQuery));
            clientDescriptor.MessageType.IsArray.Should().BeFalse();
            clientDescriptor.MessageType.Fields.Should().BeEmpty();
            clientDescriptor.ResponseType.Name.Should().Be(nameof(AResponse));
            clientDescriptor.ResponseType.IsArray.Should().BeTrue();
            clientDescriptor.ResponseType.Fields.Should().BeEquivalentTo(new[]
            {
                new Field("Id", "Guid"),
                new Field("Text", "String"),
                new Field("Date", "DateTime"),
                new Field("Count", "Int32"),
                new Field("Average", "Single"),
                new Field("Time", "TimeSpan", false, true),
                new Field("OptionalId", "Guid", true, false)
            });
        }

        [Fact]
        public void GenerateArrayTypeWithDefaultOptions()
        {
            _routerBuilder.Get<AnArrayQuery>()
                 .AddPath("admin")
                 .AddPath("users");

            var routeDescriptors = Router.GetRouteDescriptors();
            var clientDescriptors = ClientCodeGenerator.GenerateClientDescriptors(routeDescriptors);

            var clientDescriptor = clientDescriptors.Should().ContainSingle().Which;
            clientDescriptor.MessageType.Name.Should().Be(nameof(AnArrayQuery));
            clientDescriptor.MessageType.IsArray.Should().BeFalse();
            clientDescriptor.MessageType.Fields.Should().BeEmpty();
            clientDescriptor.ResponseType.Name.Should().Be(nameof(AResponse));
            clientDescriptor.ResponseType.IsArray.Should().BeTrue();
            clientDescriptor.ResponseType.Fields.Should().BeEquivalentTo(new[]
            {
                new Field("Id", "Guid"),
                new Field("Text", "String"),
                new Field("Date", "DateTime"),
                new Field("Count", "Int32"),
                new Field("Average", "Single"),
                new Field("Time", "TimeSpan", false, true),
                new Field("OptionalId", "Guid", true, false)
            });
        }

        [Fact]
        public void RoutePathStopAtFirstParameterizedSegment()
        {
            _routerBuilder.Get<AQuery>()
                 .AddPath("users")
                 .AddPathParameter(x => x.Id);

            _routerBuilder.Get<AnArrayQuery>()
                 .AddPath("admin")
                 .AddPath("users");

            var routeDescriptors = Router.GetRouteDescriptors();
            var clientDescriptors = ClientCodeGenerator.GenerateClientDescriptors(routeDescriptors);

            clientDescriptors.Single(x => x.MessageType.Name == nameof(AQuery))
                .MessageType.RoutePath.Should().Be(new Path("users"));
            clientDescriptors.Single(x => x.MessageType.Name == nameof(AnArrayQuery))
                .MessageType.RoutePath.Should().Be(new Path("admin", "users"));
        }

        [Fact]
        public void CanExcludeRoute()
        {
            _routerBuilder.Get<AQuery>()
                 .AddPath("users")
                 .AddPathParameter(x => x.Id);

            _routerBuilder.Get<AnArrayQuery>()
                 .AddPath("admin")
                 .AddPath("users");

            _codeGeneratorBuilder.ExcludeRouteWhen(d => d.ParameterizedUrl.Contains("admin"));

            var routeDescriptors = Router.GetRouteDescriptors();
            var clientDescriptors = ClientCodeGenerator.GenerateClientDescriptors(routeDescriptors);

            clientDescriptors.Should().ContainSingle().Which.MessageType.RoutePath.Should().Be(new Path("users"));
        }

        [Fact]
        public void GenerateClassesRecursively()
        {
            _routerBuilder.Get<AQueryWithDeepResponse>();

            _codeGeneratorBuilder.AddConditionalImport((r, t) => t.HasMemberTypeRecursively<Guid>(), (r, t) => new
            {
                name = "{ Guid }",
                from = "guid-typescript"
            });

            var routeDescriptors = Router.GetRouteDescriptors();
            var clientDescriptors = ClientCodeGenerator.GenerateClientDescriptors(routeDescriptors);

            var clientDescriptor = clientDescriptors.Should().ContainSingle().Which;
            clientDescriptor.MessageType.Name.Should().Be(nameof(AQueryWithDeepResponse));
            clientDescriptor.MessageType.Fields.Should().BeEmpty();
            clientDescriptor.ResponseType.Name.Should().Be(nameof(RootResponse));
            clientDescriptor.ResponseType.Fields.Should().BeEquivalentTo(new[]
            {
                new Field("Name", "String"),
                new Field("Data", "SubResponse"),
                new Field("Childrens", "SubResponse", false, true)
            });
            var deepResponse = clientDescriptor.ResponseType.InnerTypes.Should().ContainSingle().Which;
            deepResponse.Fields.Should().BeEquivalentTo(new[] 
            {
                new Field("Id", "Guid"),
                new Field("Childrens", "SubResponse", false, true)
            });
            clientDescriptor.ResponseType.Imports.Should().ContainSingle().Which
                .Should().BeEquivalentTo(new { name = "{ Guid }", from = "guid-typescript" });
        }

        [Fact]
        public void GenerateTypesForVoidCommand()
        {
            _routerBuilder.Post<AVoidCommand>();

            var routeDescriptors = Router.GetRouteDescriptors();
            var clientDescriptors = ClientCodeGenerator.GenerateClientDescriptors(routeDescriptors);

            var clientDescriptor = clientDescriptors.Should().ContainSingle().Which;
            clientDescriptor.MessageType.Name.Should().Be(nameof(AVoidCommand));
            clientDescriptor.ResponseType.Should().BeNull();
        }

        [Fact]
        public void GenerateTypesForCreateCommand()
        {
            _routerBuilder.Post<ACreateCommand>();

            var routeDescriptors = Router.GetRouteDescriptors();
            var clientDescriptors = ClientCodeGenerator.GenerateClientDescriptors(routeDescriptors);

            var clientDescriptor = clientDescriptors.Should().ContainSingle().Which;
            clientDescriptor.MessageType.Name.Should().Be(nameof(ACreateCommand));
            clientDescriptor.ResponseType.Should().BeNull();
        }

        [Fact]
        public void GenerateEnums()
        {
            _routerBuilder.Post<AEnumCommand>()
                .AddPath("users");

            var routeDescriptors = Router.GetRouteDescriptors();
            var clientDescriptors = ClientCodeGenerator.GenerateClientDescriptors(routeDescriptors);

            var clientDescriptor = clientDescriptors.Should().ContainSingle().Which;
            clientDescriptor.MessageType.Name.Should().Be(nameof(AEnumCommand));
            var enumType = clientDescriptor.MessageType.InnerTypes.Should().ContainSingle().Which;
            enumType.Name.Should().Be(nameof(AnEnum));
            enumType.IsEnum.Should().BeTrue();
            enumType.Fields.Should().BeEmpty();
            enumType.Values.Should().BeEquivalentTo(new[] 
            {
                new EnumValue("A", 0),
                new EnumValue("B", 1),
                new EnumValue("C", 2)
            });
            enumType.Imports.Should().BeEmpty();
            clientDescriptor.ResponseType.Should().BeNull();
        }

        [Fact]
        public void GenerateGenericTypes()
        {
            _routerBuilder.Post<AGenericCommand<DateTime, string>>()
                .AddPath("users");

            _codeGeneratorBuilder.AddFieldTypeCorrespondence(typeof(DateTime), "Date");

            var routeDescriptors = Router.GetRouteDescriptors();
            var clientDescriptors = ClientCodeGenerator.GenerateClientDescriptors(routeDescriptors);

            var clientDescriptor = clientDescriptors.Should().ContainSingle().Which;
            clientDescriptor.MessageType.Name.Should().Be("AGenericCommand<Date, String>");
        }

        [Fact]
        public void GenerateGenericTypesCustom()
        {
            _routerBuilder.Post<AGenericCommand<DateTime, string>>()
                .AddPath("users");

            _codeGeneratorBuilder.AddClientTypeCorrespondence(
                typeof(AGenericCommand<DateTime, string>),
                "AGenericDateStringCommand");

            var routeDescriptors = Router.GetRouteDescriptors();
            var clientDescriptors = ClientCodeGenerator.GenerateClientDescriptors(routeDescriptors);

            var clientDescriptor = clientDescriptors.Should().ContainSingle().Which;
            clientDescriptor.MessageType.Name.Should().Be("AGenericDateStringCommand");
        }

        [Fact]
        public void GenerateGenericTypesCustomTransform()
        {
            _routerBuilder.Post<AGenericCommand<DateTime, string>>()
                .AddPath("users");

            _codeGeneratorBuilder
                .AddFieldTypeCorrespondence(typeof(DateTime), "Date")
                .TransformClientTypes(str =>
                {
                    return str.Replace("<", "").Replace(">", "").Replace(", ", "");
                });

            var routeDescriptors = Router.GetRouteDescriptors();
            var clientDescriptors = ClientCodeGenerator.GenerateClientDescriptors(routeDescriptors);

            var clientDescriptor = clientDescriptors.Should().ContainSingle().Which;
            clientDescriptor.MessageType.Name.Should().Be("AGenericCommandDateString");
        }

        [Fact]
        public void GenerateMethodUrlAndQueryStringForQuery()
        {
            _routerBuilder.Get<AQuery>()
                .AddPath("admin")
                .AddPath("users")
                .AddPathParameter(x => x.Id)
                .AddPath("test")
                .UseQuery();

            _codeGeneratorBuilder.TransformFieldNames(n => n.ToUpper());
            _codeGeneratorBuilder.RenderPathParameter((t, n) => $"${{this.{n}}}");

            var routeDescriptors = Router.GetRouteDescriptors();
            var clientDescriptors = ClientCodeGenerator.GenerateClientDescriptors(routeDescriptors);

            var clientDescriptor = clientDescriptors.Should().ContainSingle().Which;
            clientDescriptor.MessageType.Method.Should().Be("GET");
            clientDescriptor.MessageType.Url.Should().Be("/admin/users/${this.ID}/test");
            clientDescriptor.MessageType.RoutePath.Should().Be(new Path("admin/users/test"));
            clientDescriptor.MessageType.QueryStringFields
                .Should().ContainSingle()
                .Which.Should().Be(new Field("OPTIONALID", "Guid", true, false));
            clientDescriptor.MessageType.BodyFields.Should().BeNull();
        }

        [Fact]
        public void GenerateMethodUrlAndQueryStringForCommand()
        {
            _routerBuilder.Post<AVoidCommand>()
                .AddPath("admin")
                .AddPath("users")
                .AddPathParameter(x => x.Id)
                .UseBody();

            _codeGeneratorBuilder.TransformFieldNames(n => n.CamelCase());
            _codeGeneratorBuilder.RenderPathParameter((t, n) => $"${{this.{n}:{t}}}");

            var routeDescriptors = Router.GetRouteDescriptors();
            var clientDescriptors = ClientCodeGenerator.GenerateClientDescriptors(routeDescriptors);

            var clientDescriptor = clientDescriptors.Should().ContainSingle().Which;
            clientDescriptor.MessageType.Method.Should().Be("POST");
            clientDescriptor.MessageType.Url.Should().Be("/admin/users/${this.id:Guid}");
            clientDescriptor.MessageType.QueryStringFields.Should().BeNull();
            clientDescriptor.MessageType.BodyFields
                .Should().ContainSingle()
                .Which.Should().Be(new Field("name", "String", false, false));
        }

        [Fact]
        public void GenerateFlatTypeWithCustomTemplates()
        {
            _routerBuilder.Get<AQuery>()
                 .AddPath("admin")
                 .AddPath("users")
                 .AddPathParameter(x => x.Id);

            _codeGeneratorBuilder.UseMessageTemplate(clrType =>
            {
                if (typeof(IQuery).IsAssignableFrom(clrType))
                {
                    return "CustomMessageTemplate.tpl";
                }
                return null;
            });

            _codeGeneratorBuilder.UseResponseTemplate(clrType =>
            {
                if (typeof(AResponse).IsAssignableFrom(clrType))
                {
                    return "CustomResponseTemplate.tpl";
                }
                return null;
            });

            var routeDescriptors = Router.GetRouteDescriptors();
            var clientDescriptors = ClientCodeGenerator.GenerateClientDescriptors(routeDescriptors);

            var clientDescriptor = clientDescriptors.Should().ContainSingle().Which;
            clientDescriptor.MessageType.Template.Should().Be("CustomMessageTemplate.tpl");
            clientDescriptor.ResponseType.Template.Should().Be("CustomResponseTemplate.tpl");
        }

        public class AQuery : IQuery
        {
            public Guid Id;
            public Guid? OptionalId;
        }

        public class AQueryHandler : QueryHandler<AQuery, AResponse>
        {
            public override Task<AResponse> ExecuteAsync(AQuery query)
            {
                throw new NotImplementedException();
            }
        }

        public class AEnumerableQuery : IQuery
        {
        }

        public class AEnumerableQueryHandler : QueryHandler<AEnumerableQuery, IEnumerable<AResponse>>
        {
            public override Task<IEnumerable<AResponse>> ExecuteAsync(AEnumerableQuery query)
            {
                throw new NotImplementedException();
            }
        }

        public class AnArrayQuery : IQuery
        {
        }

        public class AnArrayQueryHandler : QueryHandler<AnArrayQuery, AResponse[]>
        {
            public override Task<AResponse[]> ExecuteAsync(AnArrayQuery query)
            {
                throw new NotImplementedException();
            }
        }

        public class AQueryWithDeepResponse : IQuery
        {
        }

        public class ADeepResponseQueryHandler : QueryHandler<AQueryWithDeepResponse, RootResponse>
        {
            public override Task<RootResponse> ExecuteAsync(AQueryWithDeepResponse query)
            {
                throw new NotImplementedException();
            }
        }

        public class AResponse
        {
            public Guid Id;
            public string Text;
            public DateTime Date;
            public int Count;
            public float Average;
            public TimeSpan?[] Time;
            public Guid? OptionalId;
        }

        public class RootResponse
        {
            public string Name;
            public SubResponse Data;
            public SubResponse[] Childrens;

            public class SubResponse
            {
                public Guid Id;
                public SubResponse[] Childrens;
            }
        }

        public class ACreateCommand : ICommand
        {
        }

        public class ACreateCommandHandler : CreateCommandHandler<ACreateCommand>
        {
            public override Task<Guid> ExecuteAsync(ACreateCommand command)
            {
                throw new NotImplementedException();
            }
        }

        public class AVoidCommand : ICommand
        {
            public Guid Id;
            public string Name;
        }

        public class AVoidCommandHandler : VoidCommandHandler<AVoidCommand>
        {
            public override Task ExecuteAsync(AVoidCommand command)
            {
                throw new NotImplementedException();
            }
        }

        public class AEnumCommand : ICommand
        {
            public AnEnum Enum;
        }

        public class AEnumCommandHandler : VoidCommandHandler<AEnumCommand>
        {
            public override Task ExecuteAsync(AEnumCommand command)
            {
                throw new NotImplementedException();
            }
        }

        public enum AnEnum
        {
            A, B, C
        }

        public class AGenericCommand<T1, T2> : ICommand
        {
        }

        public class AGenericCommandHandler<T1, T2> : VoidCommandHandler<AGenericCommand<T1, T2>>
        {
            public override Task ExecuteAsync(AGenericCommand<T1, T2> command)
            {
                throw new NotImplementedException();
            }
        }

        class ResponseTypeProvider : HandlerResponseTypeProvider, IResponseTypeProvider
        {
            public ResponseTypeProvider(params Assembly[] assemblies) 
                : base(assemblies)
            {
            }
        }
    }
}
