using System;
using Xunit;
using FluentAssertions;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using CocoriCore.Common;
using Microsoft.Extensions.Caching.Memory;
using System.Reflection;
using Autofac;
using Newtonsoft.Json;
using CocoriCore.Autofac;
using CocoriCore.TestUtils;
using CocoriCore.Messaging;

namespace CocoriCore.Router.Test
{
    public class ApiFootprintTest
    {
        private readonly Lazy<ILifetimeScope> _rootScope;
        private readonly ContainerBuilder _builder;
        private readonly RouterOptionsBuilder _routerBuilder;
        private readonly ClientCodeGeneratorOptionsBuilder _codeGeneratorBuilder;
        private Dictionary<Type, string> _finalTypes;

        public ApiFootprintTest()
        {
            _builder = new ContainerBuilder();
            _rootScope = new Lazy<ILifetimeScope>(() => _builder.Build());
            _routerBuilder = new RouterOptionsBuilder();
            _codeGeneratorBuilder = new ClientCodeGeneratorOptionsBuilder();

            _builder.Register(c => _routerBuilder.Options).SingleInstance();
            _builder.Register(c => _codeGeneratorBuilder.Options).SingleInstance();
            _builder.RegisterType<Router>().AsImplementedInterfaces().SingleInstance();
            _builder.RegisterType<ClientCodeGenerator>().AsImplementedInterfaces().SingleInstance();
            _builder.RegisterType<ResponseTypeProvider>()
                .AsImplementedInterfaces()
                .WithParameter(new TypedParameter<Assembly[]>(new[] { GetType().Assembly }))
                .SingleInstance();
            _builder.RegisterInstance(new MemoryCache(new MemoryCacheOptions())).As<IMemoryCache>();
            _builder.RegisterType<JsonSerializer>().AsSelf().SingleInstance();
            _builder.RegisterType<MessageDeserializer>().AsImplementedInterfaces().SingleInstance();
            _builder.RegisterType<FootprintGenerator>()
                .AsImplementedInterfaces()
                .WithParameter(new TypedParameter<Dictionary<Type, string>>(c => _finalTypes))
                .SingleInstance();
        }

        private ILifetimeScope RootScope => _rootScope.Value;

        private IFootprintGenerator FootprintGenerator => RootScope.Resolve<IFootprintGenerator>();

        private IRouter Router => RootScope.Resolve<IRouter>();

        [Fact]
        public void RouteWithoutQueryString_Get()
        {
            var route = _routerBuilder.Get<GetSomethingQuery>()
                 .AddPath("somethings")
                 .AddPathParameter(x => x.Id);

            var descriptors = Router.GetRouteDescriptors();
            var footPrint = FootprintGenerator.Generate(descriptors);

            var routeFootprint = footPrint.Routes.Should().ContainSingle().Which;
            routeFootprint.Url.Should().Be("/somethings/id:guid");
            routeFootprint.Method.Should().Be("GET");
            routeFootprint.QueryString.Should().BeNull();
            routeFootprint.Response.Should().MatchStructure(new
            {
                Id = "guid",
                Label = "string",
                RelatedId = "guid?",
                Gender = "gender",
                SubResponse = "subResponse"
            });

            var gender = footPrint.Enums["Gender"];
            gender["None"].Should().Be(0);
            gender["Male"].Should().Be(1);
            gender["Female"].Should().Be(2);
        }

        [Fact]
        public void RouteWithNullableParamaterInUrl()
        {
            var route = _routerBuilder.Get<GetSomethingNullableQuery>()
                 .AddPath("somethings")
                 .AddPathParameter(x => x.Id);

            var descriptors = Router.GetRouteDescriptors();
            var footPrint = FootprintGenerator.Generate(descriptors);

            var routeFootprint = footPrint.Routes.Should().ContainSingle().Which;
            routeFootprint.Url.Should().Be("/somethings/id:guid?");
            routeFootprint.Method.Should().Be("GET");
            routeFootprint.QueryString.Should().BeNull();
        }

        [Fact]
        public void RouteWithQueryString_Deep0()
        {
            var route = _routerBuilder.Get<SearchSomethingQuery>()
                 .AddPath("somethings")
                 .AddPath("search")
                 .UseQuery();

            var descriptors = Router.GetRouteDescriptors();
            var footPrint = FootprintGenerator.Generate(descriptors, 0);

            var routeFootprint = footPrint.Routes.Should().ContainSingle().Which;
            routeFootprint.Url.Should().Be("/somethings/search");
            routeFootprint.Method.Should().Be("GET");
            routeFootprint.QueryString.Should().MatchStructure(new
            {
                Keywords = "string"
            });
            routeFootprint.Response.Should().Be("aresponse[]");

            footPrint.Types.Should().HaveCount(2);
            footPrint.Types["AResponse"].Should().MatchStructure(new
            {
                Id = "guid",
                Label = "string",
                RelatedId = "guid?",
                Gender = "gender",
                SubResponse = "subResponse",
            });
            footPrint.Types["SubResponse"].Should().MatchStructure(new
            {
                Name = "string",
            });
        }

        [Fact]
        public void RouteWithQueryString_Deep1()
        {
            var route = _routerBuilder.Get<SearchSomethingQuery>()
                 .AddPath("somethings")
                 .AddPath("search")
                 .UseQuery();

            var descriptors = Router.GetRouteDescriptors();
            var footPrint = FootprintGenerator.Generate(descriptors, 1);

            var routeFootprint = footPrint.Routes.Should().ContainSingle().Which;
            routeFootprint.Url.Should().Be("/somethings/search");
            routeFootprint.Method.Should().Be("GET");
            routeFootprint.QueryString.Should().MatchStructure(new
            {
                Keywords = "string"
            });
            routeFootprint.Response.Should().MatchStructure(new []
            {
                new 
                {
                    Id = "guid",
                    Label = "string",
                    RelatedId = "guid?",
                    Gender = "gender",
                    SubResponse = "subResponse"
                }
            });

            footPrint.Types.Should().HaveCount(1);
            footPrint.Types["SubResponse"].Should().MatchStructure(new
            {
                Name = "string",
            });
        }

        [Fact]
        public void RouteWithQueryString_Deep2()
        {
            var route = _routerBuilder.Get<SearchSomethingQuery>()
                 .AddPath("somethings")
                 .AddPath("search")
                 .UseQuery();

            var descriptors = Router.GetRouteDescriptors();
            var footPrint = FootprintGenerator.Generate(descriptors, 2);

            var routeFootprint = footPrint.Routes.Should().ContainSingle().Which;
            routeFootprint.Url.Should().Be("/somethings/search");
            routeFootprint.Method.Should().Be("GET");
            routeFootprint.QueryString.Should().MatchStructure(new
            {
                Keywords = "string"
            });
            routeFootprint.Response.Should().MatchStructure(new[]
            {
                new
                {
                    Id = "guid",
                    Label = "string",
                    RelatedId = "guid?",
                    Gender = "gender",
                    SubResponse = new { Name = "string" }
                }
            });

            footPrint.Types.Should().HaveCount(0);
        }

        [Fact]
        public void RouteWithRecursiveResponse()
        {
            var route = _routerBuilder.Get<RecursiveQuery>()
                 .AddPath("recursive");

            var descriptors = Router.GetRouteDescriptors();
            var footPrint = FootprintGenerator.Generate(descriptors);

            var routeFootprint = footPrint.Routes.Should().ContainSingle().Which;
            routeFootprint.Url.Should().Be("/recursive");
            routeFootprint.Method.Should().Be("GET");
            routeFootprint.QueryString.Should().BeNull();
            routeFootprint.Response.Should().MatchStructure(new
            {
                Response = "recursiveResponse",
            });
        }

        [Fact]
        public void RouteWithBody_Put()
        {
            var route = _routerBuilder.Put<CreateSomethingCommand>()
                 .AddPath("somethings")
                 .UseBody();

            var descriptors = Router.GetRouteDescriptors();
            var footPrint = FootprintGenerator.Generate(descriptors);

            var routeFootprint = footPrint.Routes.Should().ContainSingle().Which;
            routeFootprint.Url.Should().Be("/somethings");
            routeFootprint.Method.Should().Be("PUT");
            routeFootprint.QueryString.Should().BeNull();
            routeFootprint.Body.Should().MatchStructure(new
            {
                Name = "string",
                BirthDate = "dateTime"
            });
            routeFootprint.Response.Should().Be("guid");
        }

        [Fact]
        public void RouteWithBody_Post()
        {
            var route = _routerBuilder.Post<UpdateSomethingCommand>()
                 .AddPath("somethings")
                 .AddPathParameter(x => x.Id)
                 .UseBody();

            var descriptors = Router.GetRouteDescriptors();
            var footPrint = FootprintGenerator.Generate(descriptors);

            var routeFootprint = footPrint.Routes.Should().ContainSingle().Which;
            routeFootprint.Url.Should().Be("/somethings/id:guid");
            routeFootprint.Method.Should().Be("POST");
            routeFootprint.QueryString.Should().BeNull();
            routeFootprint.Body.Should().MatchStructure(new
            {
                Name = "string"
            });
            routeFootprint.Response.Should().BeNull();
        }

        [Fact]
        public void RouteWithGenericCommand_Post()
        {
            var route = _routerBuilder.Post<UpdateSomethingGenericCommand<AnEntity>>()
                 .AddPath("somethings")
                 .AddPathParameter(x => x.Id)
                 .UseBody();

            var descriptors = Router.GetRouteDescriptors();
            var footPrint = FootprintGenerator.Generate(descriptors);

            var routeFootprint = footPrint.Routes.Should().ContainSingle().Which;
            routeFootprint.Url.Should().Be("/somethings/id:guid");
            routeFootprint.Method.Should().Be("POST");
            routeFootprint.QueryString.Should().BeNull();
            routeFootprint.Body.Should().BeNull();
            routeFootprint.Response.Should().BeNull();
        }

        [Fact]
        public void RouteWithGenericAndConstraintCommand_Post()
        {
            var route = _routerBuilder.Post<DoSomethingGenericCommand<AnEntity>>()
                 .AddPath("somethings")
                 .AddPathParameter(x => x.Id)
                 .UseBody();

            var descriptors = Router.GetRouteDescriptors();
            var footPrint = FootprintGenerator.Generate(descriptors);

            var routeFootprint = footPrint.Routes.Should().ContainSingle().Which;
            routeFootprint.Url.Should().Be("/somethings/id:guid");
            routeFootprint.Method.Should().Be("POST");
            routeFootprint.QueryString.Should().BeNull();
            routeFootprint.Body.Should().BeNull();
            routeFootprint.Response.Should().BeNull();
        }

        [Fact]
        public void RouteWithResponseContainingEnumerable_Get()
        {
            var route = _routerBuilder.Get<GetSomethingWithEnumerableQuery>()
                 .AddPath("somethings")
                 .AddPathParameter(x => x.Id);

            var descriptors = Router.GetRouteDescriptors();
            var footPrint = FootprintGenerator.Generate(descriptors);

            var routeFootprint = footPrint.Routes.Should().ContainSingle().Which;
            routeFootprint.Url.Should().Be("/somethings/id:guid");
            routeFootprint.Method.Should().Be("GET");
            routeFootprint.QueryString.Should().BeNull();
            routeFootprint.Response.Should().MatchStructure(new
            {
                Labels = "string[]",
                Items = "itemResponse[]"
            });
        }

        [Fact]
        public void RouteWithResponseContainingEnumerable_Deep2()
        {
            var route = _routerBuilder.Get<GetSomethingWithEnumerableQuery>()
                 .AddPath("somethings")
                 .AddPathParameter(x => x.Id);

            var descriptors = Router.GetRouteDescriptors();
            var footPrint = FootprintGenerator.Generate(descriptors, 2);

            var routeFootprint = footPrint.Routes.Should().ContainSingle().Which;
            routeFootprint.Url.Should().Be("/somethings/id:guid");
            routeFootprint.Method.Should().Be("GET");
            routeFootprint.QueryString.Should().BeNull();
            routeFootprint.Response.Should().MatchStructure(new
            {
                Labels = "string[]",
                Items = new[] { new { Age = "int32", Name = "string" } }
            });
        }

        [Fact]
        public void ExceptionIfDuplicateFieldIntoDataStructure()
        {
            var route = _routerBuilder.Get<ADuplicateResponseFieldQuery>().AddPath("stuff");

            var descriptors = Router.GetRouteDescriptors();

            Action action = () => FootprintGenerator.Generate(descriptors);

            action.Should().Throw<Exception>()
                .WithMessage("Error during footprint generation for route : GET /stuff")
                .WithInnerException<InvalidOperationException>()
                .WithMessage($"Duplicate field or property name for type {typeof(AResponseWithDuplicateField).FullName} : \nId\nId");
        }

        [Fact]
        public void ExceptionIfUseQueryWithEmtptyQuery()
        {
            var route = _routerBuilder.Get<EmtptyQuery>().AddPath("empty").UseQuery();

            var descriptors = Router.GetRouteDescriptors();

            Action action = () => FootprintGenerator.Generate(descriptors);

            action.Should().Throw<Exception>()
                .WithMessage("Error during footprint generation for route : GET /empty")
                .WithInnerException<ConfigurationException>()
                .WithMessage("Query string is empty for route GET /empty, remove call to .UseQuery().");
        }

        [Fact]
        public void CanDefineFinalTypesWithDescription()
        {
            var route = _routerBuilder.Get<WithPrimitiveTypesQuery>()
                 .AddPath("test")
                 .UseQuery();

            _finalTypes = CocoriCore.Router.FootprintGenerator.DEFAULT_FINAL_TYPES;
            _finalTypes.Add(typeof(AClassConsideredAsFinalType), "object handled by deserializer");
            _finalTypes.Add(typeof(Uri), null);

            var descriptors = Router.GetRouteDescriptors();
            var footPrint = FootprintGenerator.Generate(descriptors, 2);

            footPrint.Types.Should().HaveCount(1);
            var firstType = footPrint.Types.ElementAt(0);
            firstType.Key.Should().Be("AClassConsideredAsFinalType");
            firstType.Value.Should().Be("object handled by deserializer");
            var routeFootprint = footPrint.Routes.Should().ContainSingle().Which;
            routeFootprint.Url.Should().Be("/test");
            routeFootprint.Method.Should().Be("GET");
            routeFootprint.QueryString.Should().MatchStructure(new
            {
                MyFinalObject = "aclassConsideredAsFinalType",
                Url = "uri"
            });
        }

        [Fact]
        public void CanDefineRouteDescription()
        {
            var route = _routerBuilder.Get<EmtptyQuery>()
                 .AddPath("test")
                 .SetDescritpion("a short description for this route");

            var descriptors = Router.GetRouteDescriptors();
            var footPrint = FootprintGenerator.Generate(descriptors);

            var routeFootprint = footPrint.Routes.Should().ContainSingle().Which;
            routeFootprint.Url.Should().Be("/test");
            routeFootprint.Method.Should().Be("GET");
            routeFootprint.Description.Should().Be("a short description for this route");
        }

        [Fact]
        public void BatchRouteCreateCommand()
        {
            var route = _routerBuilder.Post<Batch<CreateSomethingCommand>>()
                 .AddPath("test")
                 .UseBody();

            var descriptors = Router.GetRouteDescriptors();
            var footPrint = FootprintGenerator.Generate(descriptors, 2);

            var routeFootprint = footPrint.Routes.Should().ContainSingle().Which;
            routeFootprint.Url.Should().Be("/test");
            routeFootprint.Method.Should().Be("POST");
            routeFootprint.Body.Should().MatchStructure(new
            {
                Messages = new []
                {
                    new { BirthDate = "dateTime", Name = "string" }
                }
            });
            routeFootprint.Response.Should().Be("guid[]");
        }

        [Fact]
        public void BatchRouteUpdateCommand()
        {
            var route = _routerBuilder.Put<Batch<UpdateSomethingCommand>>()
                 .AddPath("test")
                 .UseBody();

            var descriptors = Router.GetRouteDescriptors();
            var footPrint = FootprintGenerator.Generate(descriptors, 2);

            var routeFootprint = footPrint.Routes.Should().ContainSingle().Which;
            routeFootprint.Url.Should().Be("/test");
            routeFootprint.Method.Should().Be("PUT");
            routeFootprint.Body.Should().MatchStructure(new 
            {
                Messages = new[] 
                {
                    new { Id = "guid", Name = "string" }
                }
            });
            routeFootprint.Response.Should().Be("object[]");
        }
    }

#pragma warning disable 0649

    class AnEntity : IEntity
    {
        public Guid Id { get; set; }
    }

    class GetSomethingQuery : IQuery
    {
        public Guid Id;
    }

    class GetSomethingNullableQuery : IQuery
    {
        public Guid? Id;
    }

    class SearchSomethingQuery : IQuery
    {
        public string Keywords;
    }

    class GetSomethingWithEnumerableQuery : IQuery
    {
        public Guid Id;
    }

    class RecursiveQuery : IQuery
    {
    }

    class AResponse : IQuery
    {
        public Guid Id;
        public string Label;
        public Guid? RelatedId;
        public Gender Gender;
        public SubResponse SubResponse;
    }

    class AResponseWithDuplicateField : AResponseWithDuplicateFieldBase
    {
        public new Guid Id;
    }

    class AResponseWithDuplicateFieldBase : IQuery
    {
        public Guid Id;
    }

    class AResponseWithEnumerable : IQuery
    {
        public string[] Labels;
        public List<ItemResponse> Items;
    }

    class ItemResponse
    {
        public int Age;
        public string Name;
    }

    class RecursiveResponse
    {
        public RecursiveResponse Response;
    }

    class SubResponse
    {
        public string Name;
    }

    public enum Gender
    {
        None = 0,
        Male = 1,
        Female = 2
    }

    class ADuplicateResponseFieldQuery : IQuery
    {
    }

    class EmtptyQuery : IQuery
    {
    }

    class CreateSomethingCommand : ICommand
    {
        public string Name;
        public DateTime BirthDate;
    }

    class UpdateSomethingCommand : ICommand
    {
        public Guid Id;
        public string Name;
    }

    class UpdateSomethingGenericCommand<T> : ICommand
        where T : IEntity
    {
        public Guid Id;
    }

    class DoSomethingGenericCommand<T> : ICommand
        where T : IEntity
    {
        public Guid Id;
    }

    class WithPrimitiveTypesQuery : IQuery
    {
        public AClassConsideredAsFinalType MyFinalObject;
        public Uri Url;
    }

    public class AClassConsideredAsFinalType
    {
        public object AField;
    }

#pragma warning restore 0649

    class AFootprintQueryHandler : QueryHandler<GetSomethingQuery, AResponse>
    {
        public override Task<AResponse> ExecuteAsync(GetSomethingQuery query)
        {
            throw new NotImplementedException();
        }
    }

    class AFootprintNullableQueryHandler : QueryHandler<GetSomethingNullableQuery, AResponse>
    {
        public override Task<AResponse> ExecuteAsync(GetSomethingNullableQuery query)
        {
            throw new NotImplementedException();
        }
    }

    class AFootprintWithEnumerableQueryHandler : QueryHandler<GetSomethingWithEnumerableQuery, AResponseWithEnumerable>
    {
        public override Task<AResponseWithEnumerable> ExecuteAsync(GetSomethingWithEnumerableQuery query)
        {
            throw new NotImplementedException();
        }
    }

    class AFootprintQueryIEnumerableHandler : QueryHandler<SearchSomethingQuery, IEnumerable<AResponse>>
    {
        public override Task<IEnumerable<AResponse>> ExecuteAsync(SearchSomethingQuery query)
        {
            throw new NotImplementedException();
        }
    }

    class AFootprintCreateCommandHandler : CreateCommandHandler<CreateSomethingCommand>
    {
        public override Task<Guid> ExecuteAsync(CreateSomethingCommand command)
        {
            throw new NotImplementedException();
        }
    }

    class AFootprintVoidCommandHandler : VoidCommandHandler<UpdateSomethingCommand>
    {
        public override Task ExecuteAsync(UpdateSomethingCommand command)
        {
            throw new NotImplementedException();
        }
    }

    class AFootprintGenericVoidCommandHandler<T> : VoidCommandHandler<UpdateSomethingGenericCommand<T>>
        where T : IEntity
    {
        public override Task ExecuteAsync(UpdateSomethingGenericCommand<T> command)
        {
            throw new NotImplementedException();
        }
    }

    class AFootprintGenericWithConstraintVoidCommandHandler<TCommand, T> : VoidCommandHandler<TCommand>
        where TCommand : DoSomethingGenericCommand<T>
        where T : IEntity
    {
        public override Task ExecuteAsync(TCommand command)
        {
            throw new NotImplementedException();
        }
    }

    class AFootprintQueryWithDuplicateHandler : QueryHandler<ADuplicateResponseFieldQuery, AResponseWithDuplicateField>
    {
        public override Task<AResponseWithDuplicateField> ExecuteAsync(ADuplicateResponseFieldQuery query)
        {
            throw new NotImplementedException();
        }
    }

    class AFootprintEmptyQueryHandler : QueryHandler<EmtptyQuery, object>
    {
        public override Task<object> ExecuteAsync(EmtptyQuery query)
        {
            throw new NotImplementedException();
        }
    }

    class AFootprintWithPrimitiveTypeQueryHandler : QueryHandler<WithPrimitiveTypesQuery, object>
    {
        public override Task<object> ExecuteAsync(WithPrimitiveTypesQuery query)
        {
            throw new NotImplementedException();
        }
    }

    class ARecursiveQueryHandler : QueryHandler<RecursiveQuery, RecursiveResponse>
    {
        public override Task<RecursiveResponse> ExecuteAsync(RecursiveQuery query)
        {
            throw new NotImplementedException();
        }
    }

    class ResponseTypeProvider : HandlerResponseTypeProvider, IResponseTypeProvider
    {
        public ResponseTypeProvider(params Assembly[] assemblies) 
            : base(assemblies)
        {
        }
    }
}
