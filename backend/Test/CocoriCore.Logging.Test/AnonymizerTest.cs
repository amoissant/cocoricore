﻿using CocoriCore.Expressions.Extension;
using CocoriCore.Messaging;
using CocoriCore.TestUtils;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace CocoriCore.Logging.Test
{
    public class AnonymizerTest
    {

        [Fact]
        public void AnonymizeSensitiveDatasInDepth()
        {
            Expression<Func<ACommandWithAnonymizedDatas, object>> member1 = x => x.Password;
            Expression<Func<ACommandWithAnonymizedDatas.InnerData, object>> member2 = x => x.CreditCardNumber;
            Expression<Func<ACommandWithAnonymizedDatas.AdressData, object>> member3 = x => x.Adress;
            var membersToAnonymize = new HashSet<MemberInfo>
            {
                member1.GetMemberInfo(), 
                member2.GetMemberInfo(),
                member3.GetMemberInfo()
            };

            var anonymizer = new Anonymizer(new Mock<ILogger>().Object);

            var command = new ACommandWithAnonymizedDatas
            {
                Name = "Toto",
                Password = "1234",
                Inner = new ACommandWithAnonymizedDatas.InnerData
                {
                    CreditCardNumber = "1234567890",
                    Adresses = new[]
                    {
                        new ACommandWithAnonymizedDatas.AdressData
                        {
                            Adress = "12 avenur Gilbert Montagné",
                            Age = 23
                        },
                        new ACommandWithAnonymizedDatas.AdressData
                        {
                            Adress = "impasse Claude Monet",
                            Age = 56
                        }
                    }
                }
            };

            var anonymizedCopy = anonymizer.GetAnonymizedCopy(command, membersToAnonymize);

            anonymizedCopy.Should().MatchStructure(new
            {
                Name = "Toto",
                Password = "****",
                Inner = new
                {
                    CreditCardNumber = "****",
                    Adresses = new[]
                    {
                        new { Adress = "****", Age = 23 },
                        new { Adress = "****", Age = 56 }
                    }
                }
            });
        }

        public class ACommandWithAnonymizedDatas
        {
            public string Password;
            public string Name;

            public InnerData Inner;

            public class InnerData
            {
                public string CreditCardNumber;

                public AdressData[] Adresses;
            }

            public class AdressData
            {
                public int Age;
                public string Adress;
            }
        }
    }
}
