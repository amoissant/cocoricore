﻿using CocoriCore.Clock;
using CocoriCore.Common;
using CocoriCore.FileSystem;
using FluentAssertions;
using System;
using System.Security.Cryptography;
using Xunit;

namespace CocoriCore.Security.Test
{
    public class AESCryptoServiceTest
    {
        private IFileSystem _fileSystem;

        public AESCryptoServiceTest()
        {
            var clock = new SystemClock();
            _fileSystem = new InMemoryFileSystem(clock);
        }

        public AESCryptoService CreateCryptoService()
        {
            return new AESCryptoService("R8PbMs5Nc+gsZMPnFmmJpv3ZNNyF2z9mXjVMd+4OF58=");
        }

        [Theory]
        [InlineData("")]
        [InlineData("t")]
        [InlineData("to")]
        [InlineData("toto")]
        [InlineData("toto2")]
        [InlineData("veryveryveryveryveryveryveryveryveryveryveryverylongsecret")]
        public void EncryptAndDecryptText(string text)
        {
            var crypto = CreateCryptoService();

            var encryptedText = crypto.Encrypt(text);

            encryptedText.Should().NotBeEmpty();

            var decryptedText = crypto.Decrypt(encryptedText).ToUtf8();

            decryptedText.Should().Be(text);
        }

        [Fact]
        public void EncryptedTextIsDifferentAtEachEncryption()
        {
            var crypto = CreateCryptoService();

            var encryptedText1 = crypto.Encrypt("toto");
            var encryptedText2 = crypto.Encrypt("toto");

            var decryptedText2 = crypto.Decrypt(encryptedText2).ToUtf8();
            var decryptedText1 = crypto.Decrypt(encryptedText1).ToUtf8();

            decryptedText1.Should().Be("toto");
            decryptedText2.Should().Be("toto");
            encryptedText1.Should().NotBeEquivalentTo(encryptedText2);
        }

        [Fact]
        public void ThrowExplicitMessageWhenDecryptWhithWrongKey()
        {
            var crypto1 = new AESCryptoService("kwfVo2iE2hWMN6XA9wA9xRj9nEcCtemwVUDhMbG0YoU=");
            var crypto2 = CreateCryptoService();

            var encryptedText = crypto1.Encrypt("toto");

            Action action = () => crypto2.Decrypt(encryptedText).ToUtf8();

            action.Should().Throw<CryptographicException>()
                .WithMessage("Unable to decrypt datas.\nEnsure data is encrypted and to use the same key for encryption and decryption.\nSee inner exception for more informations.");
        }
    }
}
