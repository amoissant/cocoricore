﻿using FluentAssertions;
using System.Text.RegularExpressions;
using Xunit;

namespace CocoriCore.Security.Test
{
    public class RandomServiceTest
    {
        [Fact]
        public void GenerateRandomString()
        {
            var random = new RandomService();

            var result = random.GenerateRandomString(4);

            result.Length.Should().Be(4);
            new Regex("^[a-z]+$").IsMatch(result).Should().Be(true);
        }

        [Fact]
        public void GenerateRandomStringFromAllowedChars()
        {
            var random = new RandomService();

            var result = random.GenerateRandomString(5, "abcd");

            result.Length.Should().Be(5);
            new Regex("^[abcd]+$").IsMatch(result).Should().Be(true);
        }
    }
}
