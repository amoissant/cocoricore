﻿using Autofac;
using FluentAssertions;
using System;
using System.Text.RegularExpressions;
using Xunit;

namespace CocoriCore.Security.Test
{
    public class PasswordServiceTest
    {
        private readonly ContainerBuilder _builder;
        private readonly Lazy<IContainer> _container;

        public PasswordServiceTest()
        {
            _builder = new ContainerBuilder();
            _builder.RegisterType<PasswordService>().As<IPasswordService>();
            _builder.RegisterType<HashService>().As<IHashService>();
            _container = new Lazy<IContainer>(() => _builder.Build());
        }

        public IPasswordService PasswordService => _container.Value.Resolve<IPasswordService>();

        [Fact]
        public void GeneratePasswordWithDefaultRequirements()
        {
            var password = PasswordService.GeneratePassword();

            password.Length.Should().Be(8);
            new Regex("[a-z]").IsMatch(password).Should().Be(true);
            new Regex("[A-Z]").IsMatch(password).Should().Be(true);
            new Regex(@"[ !""#$%&'()*+,\-.\/\\:;<=>?@\[\]^_`{|}~]").IsMatch(password).Should().Be(true);
            new Regex("\\d").IsMatch(password).Should().Be(true);
            PasswordService.PasswordIsWeak(password).Should().Be(false);
        }

        [Fact]
        public void VerifyIfPasswordMatchCorrectHash()
        {
            var password = PasswordService.GeneratePassword();
            
            var correctHash = PasswordService.HashPassword(password);

            correctHash.Should().NotBe(password);

            PasswordService.PasswordIsValid(password, correctHash).Should().BeTrue();
        }

        [Fact]
        public void GeneratePasswordWithCustomRequirements()
        {
            var passwordOptions = new PasswordOptions
            {
                MinimumLength = 5,
                RequireSpecialCharacters = false,
                RequireDigits = false,
                RequireUppercase = false,
            };
            _builder.RegisterInstance(passwordOptions);

            var password = PasswordService.GeneratePassword();

            password.Length.Should().Be(5);
            new Regex("^[a-z]+$").IsMatch(password).Should().Be(true);
            PasswordService.PasswordIsWeak(password).Should().Be(false);
        }
    }
}
