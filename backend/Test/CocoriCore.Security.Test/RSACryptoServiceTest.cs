﻿using FluentAssertions;
using System;
using CocoriCore.Common;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace CocoriCore.Security.Test
{
    public class RSACryptoServiceTest
    {
        public RSACryptoService CreateRSA(string keyContainerName)
        {
            var crypto = new RSACryptoService(keyContainerName);
            var aKeyIsInstalled = crypto.AKeyIsAlreadyInstalledAsync().GetAwaiter().GetResult();
            if (aKeyIsInstalled)
            {
                crypto.UninstallKeyAsync().GetAwaiter().GetResult();
            }
            return crypto;
        }

        //[Fact(Skip="use RSA.Create() to run under linux")]
        public void GenerateKey()
        {
            var crypto = CreateRSA(nameof(GenerateKey));

            var key = crypto.GenerateNewKey();

            key.Should().HaveCount(1172);
            key.All(x => x == 0x00).Should().BeFalse();
        }

        //[Fact(Skip="use RSA.Create() to run under linux")]
        public async Task InstallNewKey()
        {
            var crypto = CreateRSA(nameof(InstallNewKey));

            var aKeyIsInstalled = await crypto.AKeyIsAlreadyInstalledAsync();
            aKeyIsInstalled.Should().BeFalse();

            var key = await crypto.InstallNewKeyAsync();

            aKeyIsInstalled = await crypto.AKeyIsAlreadyInstalledAsync();
            aKeyIsInstalled.Should().BeTrue();
            var installedKey = await crypto.GetInstalledKeyAsync();
            installedKey.Should().BeEquivalentTo(key);
        }

        //[Fact(Skip="use RSA.Create() to run under linux")]
        public async Task InstallKey()
        {
            var crypto = CreateRSA(nameof(InstallKey));
            var key = crypto.GenerateNewKey();

            var aKeyIsInstalled = await crypto.AKeyIsAlreadyInstalledAsync();
            aKeyIsInstalled.Should().BeFalse();

            await crypto.InstallKeyAsync(key);

            aKeyIsInstalled = await crypto.AKeyIsAlreadyInstalledAsync();
            aKeyIsInstalled.Should().BeTrue();
            var installedKey = await crypto.GetInstalledKeyAsync();
            installedKey.Should().BeEquivalentTo(key);
        }

        //[Fact(Skip="use RSA.Create() to run under linux")]
        public async Task ExceptionIfTryInstallKeyWhenExisting()
        {
            var crypto = CreateRSA(nameof(ExceptionIfTryInstallKeyWhenExisting));
            var key = await crypto.InstallNewKeyAsync();

            Func<Task> action = async () => await crypto.InstallKeyAsync(key);

            await action.Should().ThrowAsync<ConfigurationException>()
                .WithMessage($"A key is already installed in container '{nameof(ExceptionIfTryInstallKeyWhenExisting)}'.\n" +
                             $"Clear container using {nameof(RSACryptoService)}.{nameof(RSACryptoService.UninstallKeyAsync)}() first if you want to install another key.");
        }

        //[Fact(Skip="use RSA.Create() to run under linux")]
        public async Task UninstallKey()
        {
            var crypto = CreateRSA(nameof(UninstallKey));
            await crypto.InstallNewKeyAsync();

            var aKeyIsInstalled = await crypto.AKeyIsAlreadyInstalledAsync();
            aKeyIsInstalled.Should().BeTrue();

            await crypto.UninstallKeyAsync();

            aKeyIsInstalled = await crypto.AKeyIsAlreadyInstalledAsync();
            aKeyIsInstalled.Should().BeFalse();
        }

        //[Theory(Skip="use RSA.Create() to run under linux")]
        //[InlineData("")]
        //[InlineData("t")]
        //[InlineData("to")]
        //[InlineData("toto")]
        //[InlineData("toto2")]
        public async Task EncryptAndDecryptText(string text)
        {
            var crypto = CreateRSA(nameof(EncryptAndDecryptText) + text);
            await crypto.InstallNewKeyAsync();

            var encryptedText = crypto.Encrypt(text.GetUtf8Bytes());

            encryptedText.Should().NotBeEmpty();

            var decryptedText = crypto.Decrypt(encryptedText).ToUtf8();

            decryptedText.Should().Be(text);
        }

        //[Fact(Skip="use RSA.Create() to run under linux")]
        public void ExceptionIfEncryptWithoutaKeyIsInstalled()
        {
            var crypto = CreateRSA(nameof(ExceptionIfEncryptWithoutaKeyIsInstalled));

            Action action = () => crypto.Encrypt("toto".GetUtf8Bytes());

            action.Should().Throw<ConfigurationException>()
                .WithMessage($"Key container '{nameof(ExceptionIfEncryptWithoutaKeyIsInstalled)}' is empty, you must install a key into this container before encrypt/decrypt datas.\n" +
                        $"You can install an existing key using {nameof(RSACryptoService)}.{nameof(RSACryptoService.InstallKeyAsync)}() " +
                        $"or you can generate and install a new one using {nameof(RSACryptoService)}.{nameof(RSACryptoService.InstallNewKeyAsync)}().");
        }

        //[Fact(Skip="use RSA.Create() to run under linux")]
        public void ExceptionIfDecryptWithoutaKeyIsInstalled()
        {
            var crypto = CreateRSA(nameof(ExceptionIfDecryptWithoutaKeyIsInstalled));

            Action action = () => crypto.Decrypt(new byte[] { 0x00 });

            action.Should().Throw<ConfigurationException>()
                .WithMessage($"Key container '{nameof(ExceptionIfDecryptWithoutaKeyIsInstalled)}' is empty, you must install a key into this container before encrypt/decrypt datas.\n" +
                        $"You can install an existing key using {nameof(RSACryptoService)}.{nameof(RSACryptoService.InstallKeyAsync)}() " +
                        $"or you can generate and install a new one using {nameof(RSACryptoService)}.{nameof(RSACryptoService.InstallNewKeyAsync)}().");
        }

        //[Fact(Skip="use RSA.Create() to run under linux")]
        public async Task EncryptedTextIsDifferentAtEachEncryption()
        {
            var crypto = CreateRSA(nameof(EncryptedTextIsDifferentAtEachEncryption));
            await crypto.InstallNewKeyAsync();

            var encryptedText1 = crypto.Encrypt("toto".GetUtf8Bytes());
            var encryptedText2 = crypto.Encrypt("toto".GetUtf8Bytes());

            var decryptedText2 = crypto.Decrypt(encryptedText2).ToUtf8();
            var decryptedText1 = crypto.Decrypt(encryptedText1).ToUtf8();

            decryptedText1.Should().Be("toto");
            decryptedText2.Should().Be("toto");
            encryptedText1.Should().NotBeEquivalentTo(encryptedText2);
        }
    }
}
