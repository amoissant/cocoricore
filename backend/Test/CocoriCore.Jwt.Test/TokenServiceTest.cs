﻿using Autofac;
using CocoriCore.Clock;
using CocoriCore.TestUtils;
using FluentAssertions;
using Jose;
using System;
using Xunit;
using CocoriCore.Common;

namespace CocoriCore.Jwt.Test
{
    public class TokenServiceTest
    {
        private readonly Lazy<ILifetimeScope> _rootScope;
        private readonly ContainerBuilder _builder;

        public TokenServiceTest()
        {
            _builder = new ContainerBuilder();
            _rootScope = new Lazy<ILifetimeScope>(() => _builder.Build());

            var tokenConfiguration = new TokenConfiguration()
                .UseSecret("secret")
                .UseAlgorithm(JwsAlgorithm.HS256);

            _builder.RegisterType<SystemClock>().As<IClock>();
            _builder.RegisterType<TokenService>().AsSelf();
            _builder.RegisterInstance(tokenConfiguration);
            _builder.RegisterType<JwtSerializer>()
                .WithParameter(new NamedParameter("expirationDateMember", "ExpireAt"))
                .As<IJwtSerializer>();
        }

        private ILifetimeScope RootScope => _rootScope.Value;
        private IClock Clock => RootScope.Resolve<IClock>();
        private TokenService TokenService => RootScope.Resolve<TokenService>();

        [Fact]
        public void GenerateTokenFromPayloadAndSalt()
        {
            var expirationDateTime = Clock.Now.AddMinutes(10);
            var token = TokenService.GenerateToken(new { UserId = "abcd", ExpireAt = expirationDateTime });

            var payload = TokenService.GetPayload<object>(token);
            payload.Should().MatchStructure(new
            {
                exp = expirationDateTime.ToUnixTimeSeconds(),
                userId = "abcd"
            });
        }

        [Fact]
        public void CheckTokenAuthenticityWhitoutSalt()
        {
            var expirationDateTime = Clock.Now.AddMinutes(10);
            var token = TokenService.GenerateToken(new { UserId = "abcd", ExpireAt = expirationDateTime });

            Action action1 = () => TokenService.CheckAuthenticity(token);
            Action action2 = () => TokenService.CheckAuthenticity(token, "salt");
            Action action3 = () => TokenService.CheckAuthenticity($"{token}zz");

            action1.Should().NotThrow<Exception>();
            action2.Should().Throw<InvalidTokenException>();
            action3.Should().Throw<InvalidTokenException>();

            TokenService.IsAuthentic(token).Should().Be(true);
            TokenService.IsAuthentic(token, "salt").Should().Be(false);
            TokenService.IsAuthentic($"{token}zz").Should().Be(false);
        }

        [Fact]
        public void CheckTokenAuthenticityWhitSalt()
        {
            var expirationDateTime = Clock.Now.AddMinutes(10);
            var token = TokenService.GenerateToken(new { UserId = "abcd", ExpireAt = expirationDateTime }, "salt");

            Action action1 = () => TokenService.CheckAuthenticity(token, "salt");
            Action action2 = () => TokenService.CheckAuthenticity(token, null);
            Action action3 = () => TokenService.CheckAuthenticity(token, "wrong salt");
            Action action4 = () => TokenService.CheckAuthenticity($"{token}zz");

            action1.Should().NotThrow<Exception>();
            action2.Should().Throw<InvalidTokenException>();
            action3.Should().Throw<InvalidTokenException>();
            action4.Should().Throw<InvalidTokenException>();

            TokenService.IsAuthentic(token, "salt").Should().Be(true);
            TokenService.IsAuthentic(token, null).Should().Be(false);
            TokenService.IsAuthentic(token, "wrong salt").Should().Be(false);
            TokenService.IsAuthentic($"{token}zz").Should().Be(false);
        }

        [Fact]
        public void CompareExpirationDateLocal()
        {
            var fakeClock = new FakeClock();
            _builder.RegisterInstance(fakeClock).As<IClock>();

            var expirationDateTime = Clock.Now.AddMinutes(10);
            var token = TokenService.GenerateToken(new { ExpireAt = expirationDateTime });

            TokenService.IsExpired(token).Should().Be(false);
            Action action1 = () => TokenService.CheckExpiration(token);
            action1.Should().NotThrow<Exception>();

            fakeClock.Now = expirationDateTime;

            TokenService.IsExpired(token).Should().Be(true);
            Action action2 = () => TokenService.CheckExpiration(token);
            action2.Should().Throw<InvalidTokenException>().WithMessage("Token is expired.");
        }

        [Fact]
        public void CompareExpirationDateUtc()
        {
            var fakeClock = new FakeClock();
            _builder.RegisterInstance(fakeClock).As<IClock>();

            var expirationDateTime = Clock.UtcNow.AddMinutes(10);
            var token = TokenService.GenerateToken(new { ExpireAt = expirationDateTime });

            TokenService.IsExpired(token).Should().Be(false);
            Action action1 = () => TokenService.CheckExpiration(token);
            action1.Should().NotThrow<Exception>();

            fakeClock.Now = expirationDateTime;

            TokenService.IsExpired(token).Should().Be(true);
            Action action2 = () => TokenService.CheckExpiration(token);
            action2.Should().Throw<InvalidTokenException>().WithMessage("Token is expired.");
        }

        [Fact]
        public void CheckTokenAuthenticityAndExpiration()
        {
            var fakeClock = new FakeClock();
            _builder.RegisterInstance(fakeClock).As<IClock>();

            var expirationDateTime = Clock.Now.AddMinutes(10);
            var token = TokenService.GenerateToken(new { UserId = "abcd", ExpireAt = expirationDateTime }, "salt");

            Action action1 = () => TokenService.CheckAuthenticityAndExpiration(token, "salt");
            Action action2 = () => TokenService.CheckAuthenticityAndExpiration(token, null);
            Action action3 = () => TokenService.CheckAuthenticityAndExpiration(token, "wrong salt");
            Action action4 = () => TokenService.CheckAuthenticityAndExpiration($"{token}zz");

            action1.Should().NotThrow<Exception>();
            action2.Should().Throw<InvalidTokenException>();
            action3.Should().Throw<InvalidTokenException>();
            action4.Should().Throw<InvalidTokenException>();

            TokenService.IsAuthentic(token, "salt").Should().Be(true);
            TokenService.IsAuthentic(token, null).Should().Be(false);
            TokenService.IsAuthentic(token, "wrong salt").Should().Be(false);
            TokenService.IsAuthentic($"{token}zz").Should().Be(false);

            fakeClock.Now = expirationDateTime;

            action1.Should().Throw<InvalidTokenException>();
            action2.Should().Throw<InvalidTokenException>();
            action3.Should().Throw<InvalidTokenException>();
            action4.Should().Throw<InvalidTokenException>();

            TokenService.IsAuthenticAndNotExpired(token, "salt").Should().Be(false);
            TokenService.IsAuthenticAndNotExpired(token, null).Should().Be(false);
            TokenService.IsAuthenticAndNotExpired(token, "wrong salt").Should().Be(false);
            TokenService.IsAuthenticAndNotExpired($"{token}zz").Should().Be(false);
        }
    }
}