﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Diagnostics;
using System.Data.Common;
using System.Threading;
using System.Collections.Generic;

namespace CocoriCore.EFCore.Test
{
    public class SpyCommandInterceptor : DbCommandInterceptor
    {
        public List<DbCommand> Commands { get; }

        public SpyCommandInterceptor()
        {
            Commands = new List<DbCommand>();
        }

        public override ValueTask<InterceptionResult<DbDataReader>> ReaderExecutingAsync(DbCommand command,
            CommandEventData eventData, InterceptionResult<DbDataReader> result, CancellationToken cancellationToken = default)
        {
            Commands.Add(command);
            return base.ReaderExecutingAsync(command, eventData, result, cancellationToken);
        }
    }
}
