﻿using System;
using System.Threading.Tasks;
using CocoriCore.TestUtils;
using FluentAssertions;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Xunit;
using CocoriCore.Repository;
using Autofac;
using CocoriCore.Common;
using CocoriCore.Autofac;
using Xunit.Abstractions;
using System.Collections.Generic;

namespace CocoriCore.EFCore.Test
{
    public class EFCoreAndStateStoreTest
    {
        private readonly Lazy<ILifetimeScope> _rootScope;
        private readonly ContainerBuilder _builder;
        private readonly DbContextOptionsBuilder _dbContextBuilder;
        private readonly SpyCommandInterceptor _interceptor;
        private bool _detectChangesUsingStateStore;

        public EFCoreAndStateStoreTest(ITestOutputHelper output)
        {
            _builder = new ContainerBuilder();
            _rootScope = new Lazy<ILifetimeScope>(() =>
            {
                var rootScope = _builder.Build();
                rootScope.Resolve<MyDbContext>().Database.EnsureCreated();
                return rootScope;
            });

            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var loggerFactory = _builder.ConfigureXunitLogger(output);
            _interceptor = new SpyCommandInterceptor();
            _dbContextBuilder = new DbContextOptionsBuilder()
                .UseSqlite(connection)
                .UseLoggerFactory(loggerFactory)
                .AddInterceptors(_interceptor);

            _builder.RegisterType<MyDbContext>()
                .AsSelf()
                .WithParameter(new TypedParameter<DbContextOptions>(_dbContextBuilder.Options));
            _builder.RegisterType<EFCoreBaseRepository>()
                .AsImplementedInterfaces()
                .WithParameter(new TypedParameter<DbContext>(c => c.Resolve<MyDbContext>()))
                .WithParameter(new TypedParameter<bool>(c => _detectChangesUsingStateStore))
                .InstancePerLifetimeScope();
            _builder.RegisterType<MyRepository>()
                .As<IRepository>()
                .WithParameter(new TypedParameter<IEnumerable<Type>>(new[] { typeof(StateStoreDecorator) }))
                .InstancePerLifetimeScope();

            _builder.RegisterType<StateStoreDecorator>().AsSelf().InstancePerLifetimeScope();
            _builder.RegisterType<StateStore>().As<IStateStore>().InstancePerLifetimeScope();
            _builder.RegisterType<Utf8JsonCopier>().As<ICopier>().InstancePerLifetimeScope();
            _builder.RegisterInstance(new MemoryCache(new MemoryCacheOptions())).As<IMemoryCache>();

            _builder.Register(c => new UnitOfWorkOptionsBuilder().Options).As<UnitOfWorkOptions>().SingleInstance();
            _builder.RegisterType<AutofacUnitOfWorkFactory>().As<IUnitOfWorkFactory>().SingleInstance();
            _builder.RegisterType<AutofacUnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();
        }

        private ILifetimeScope RootScope => _rootScope.Value;

        private IUnitOfWorkFactory UnitOfWorkFactory => RootScope.Resolve<IUnitOfWorkFactory>();

        [Fact]
        public async Task UpdateAllFieldByDefault()
        {
            var id = Guid.NewGuid();
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var entity = new AnEntity();
                entity.Id = id;
                entity.Name = "toto";
                await repository.InsertAsync(entity);
                await uow.Resolve<ITransactionHolder>().CommitAsync();
            }

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var entity = await repository.LoadAsync<AnEntity>(id);
                entity.Name = "titi";
                await repository.UpdateAsync(entity);
                await uow.Resolve<ITransactionHolder>().CommitAsync();
            }

            _interceptor.Commands[2].CommandText.Should()
                .StartWith("UPDATE \"AnEntity\" SET")
                .And.Contain("\"Name\" = @p0")
                .And.Contain("\"NullableId\" = @p1");
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var entity = await repository.LoadAsync<AnEntity>(id);
                entity.Name.Should().Be("titi");
            }
        }

        [Fact]
        public async Task UpdateOnlyModifiedFieldWhenUseStateStore()
        {
            _detectChangesUsingStateStore = true;

            var id = Guid.NewGuid();
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var entity = new AnEntity();
                entity.Id = id;
                entity.Name = "toto";
                await repository.InsertAsync(entity);
                await uow.Resolve<ITransactionHolder>().CommitAsync();
            }

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var entity = await repository.LoadAsync<AnEntity>(id);
                entity.Name = "titi";
                await repository.UpdateAsync(entity);
                await uow.Resolve<ITransactionHolder>().CommitAsync();
            }

            _interceptor.Commands[2].CommandText.Should()
               .StartWith("UPDATE \"AnEntity\" SET")
               .And.Contain("\"Name\" = @p0")
               .And.NotContain("\"NullableId\" = @p1");

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var entity = await repository.LoadAsync<AnEntity>(id);
                entity.Name.Should().Be("titi");
            }
        }

        public class MyDbContext : DbContext
        {
            public MyDbContext(DbContextOptions options) : base(options)
            {
            }

            protected override void OnModelCreating(ModelBuilder modelBuilder)
            {
                modelBuilder.Entity<AnEntity>();
            }
        }

        public class MyRepository : DecorableRepository, IRepository
        {
            public MyRepository(IBaseRepository baseRepository, IUnitOfWork unitOfWork, IEnumerable<Type> decoratorTypes) 
                : base(baseRepository, unitOfWork, decoratorTypes)
            {
            }
        }

        public class AnEntity : IEntity
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public Guid? NullableId { get; set; }
        }
    }
}