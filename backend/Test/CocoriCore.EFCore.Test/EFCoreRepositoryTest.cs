using System;
using System.Data;
using System.Threading.Tasks;
using CocoriCore.TestUtils;
using FluentAssertions;
using Microsoft.Data.Sqlite;
using Microsoft.Extensions.Caching.Memory;
using Xunit;
using System.Linq;
using Microsoft.Extensions.Logging;
using Autofac;
using CocoriCore.Common;
using CocoriCore.Autofac;
using Xunit.Abstractions;
using CocoriCore.Linq.Async;

namespace CocoriCore.EFCore.Test
{
    public class EFCoreRepositoryTest
    {
        private readonly Lazy<ILifetimeScope> _rootScope;
        private readonly ContainerBuilder _builder;
        private readonly Microsoft.EntityFrameworkCore.DbContextOptionsBuilder _dbContextBuilder;

        public EFCoreRepositoryTest(ITestOutputHelper output)
        {
            _builder = new ContainerBuilder();
            _rootScope = new Lazy<ILifetimeScope>(() =>
            {
                var rootScope = _builder.Build();
                rootScope.Resolve<MyDbContext>().Database.EnsureCreated();
                return rootScope;
            });

            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var loggerFactory = _builder.ConfigureXunitLogger(output);
            _dbContextBuilder = new Microsoft.EntityFrameworkCore.DbContextOptionsBuilder()
                .UseLoggerFactory(loggerFactory);
            _dbContextBuilder = Microsoft.EntityFrameworkCore.SqliteDbContextOptionsBuilderExtensions.UseSqlite(_dbContextBuilder, connection);

            _builder.RegisterType<MyDbContext>()
                .AsSelf()
                .WithParameter(new TypedParameter<Microsoft.EntityFrameworkCore.DbContextOptions>(_dbContextBuilder.Options));
            _builder.RegisterType<MyEFCoreRepository>()
                .AsSelf()
                .AsImplementedInterfaces()
                .WithParameter(new TypedParameter<Microsoft.EntityFrameworkCore.DbContext>(c => c.Resolve<MyDbContext>()));                

            _builder.RegisterInstance(new MemoryCache(new MemoryCacheOptions())).As<IMemoryCache>();

            _builder.Register(c => new UnitOfWorkOptionsBuilder().Options).As<UnitOfWorkOptions>().SingleInstance();
            _builder.RegisterType<AutofacUnitOfWorkFactory>().As<IUnitOfWorkFactory>().SingleInstance();
            _builder.RegisterType<AutofacUnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();
        }

        private ILifetimeScope RootScope => _rootScope.Value;

        private MyEFCoreRepository CreateRepository()
        {
            return RootScope.Resolve<MyEFCoreRepository>();
        }

        [Fact]
        public async Task CanInsertEntity()
        {
            var id = Guid.NewGuid();
            using (var repository = CreateRepository())
            {
                var entity = new AnEntity();
                entity.Id = id;
                entity.Name = "toto";
                await repository.InsertAsync(entity);

                var entity2 = await repository.LoadAsync<AnEntity>(id);
                entity2.Name.Should().Be("toto");
                entity2.Should().BeSameAs(entity);

                var entity3 = await repository.LoadAsync<AnEntity>(id);
                entity3.Name.Should().Be("toto");
                entity3.Should().BeSameAs(entity);

                await repository.CommitAsync();
            }

            using (var repository = CreateRepository())
            {
                var entity = await repository.LoadAsync<AnEntity>(id);
                entity.Name.Should().Be("toto");
            }
        }

        [Fact]
        public async Task CanUpdateEntity()
        {
            var id = Guid.NewGuid();
            using (var repository = CreateRepository())
            {
                var entity = new AnEntity();
                entity.Id = id;
                entity.Name = "toto";
                await repository.InsertAsync(entity);
                await repository.CommitAsync();
            }

            using (var repository = CreateRepository())
            {
                var entity = await repository.LoadAsync<AnEntity>(id);
                entity.Name = "titi";
                await repository.UpdateAsync(entity);
                await repository.CommitAsync();
            }

            using (var repository = CreateRepository())
            {
                var entity = await repository.LoadAsync<AnEntity>(id);
                entity.Name.Should().Be("titi");
            }
        }

        [Fact]
        public async Task CanDeleteEntity()
        {
            var id = Guid.NewGuid();
            using (var repository = CreateRepository())
            {
                var entity = new AnEntity();
                entity.Id = id;
                entity.Name = "toto";
                await repository.InsertAsync(entity);
                await repository.CommitAsync();
            }

            using (var repository = CreateRepository())
            {
                var entity = await repository.LoadAsync<AnEntity>(id);
                entity.Name = "titi";
                await repository.DeleteAsync(entity);
                await repository.CommitAsync();
            }

            using (var repository = CreateRepository())
            {
                var entity = await repository.LoadAsync<AnEntity>(id);
                entity.Should().BeNull();
            }
        }

        [Fact]
        public async Task CanRollbackTransaction()
        {
            var id = Guid.NewGuid();
            using (var repository = CreateRepository())
            {
                var entity = new AnEntity();
                entity.Id = id;
                entity.Name = "toto";
                await repository.InsertAsync(entity);
                await repository.CommitAsync();
            }

            using (var repository = CreateRepository())
            {
                var entity = await repository.LoadAsync<AnEntity>(id);
                entity.Name = "titi";
                await repository.UpdateAsync(entity);
                await repository.RollbackAsync();
            }

            using (var repository = CreateRepository())
            {
                var entity = await repository.LoadAsync<AnEntity>(id);
                entity.Name.Should().Be("toto");
            }
        }

        [Fact]
        public async Task FlushAndCommitAfterRollbackHasNoEffect()
        {
            var id = Guid.NewGuid();
            using (var repository = CreateRepository())
            {
                var entity = new AnEntity();
                entity.Id = id;
                entity.Name = "toto";
                await repository.InsertAsync(entity);
                await repository.RollbackAsync();
                await repository.FlushAsync();
                await repository.CommitAsync();
            }

            using (var repository = CreateRepository())
            {
                var entity = await repository.LoadAsync<AnEntity>(id);
                entity.Should().BeNull();
            }
        }

        [Fact]
        public async Task CanLoadByIdCollection()
        {
            AnEntity entity;
            using (var repository = CreateRepository())
            {
                entity = new AnEntity().WithId();
                await repository.InsertAsync(entity);
                await repository.CommitAsync();
            }

            using (var repository = CreateRepository())
            {
                var entities1 = await repository.LoadAsync(typeof(AnEntity), new Guid[] { entity.Id });
                entities1.Should().ContainSingle().Which.Id.Should().Be(entity.Id);
                var entities2 = await repository.LoadAsync(typeof(IInterface), new Guid[] { entity.Id });
                entities2.Should().ContainSingle().Which.Id.Should().Be(entity.Id);
            }
        }

        [Fact]
        public async Task CanLoadByUniqueMember()
        {
            using (var repository = CreateRepository())
            {
                var entity = new AnEntity();
                entity.Id = Tuid.Guid("1");
                entity.Name = "toto";
                entity.NullableId = Tuid.Guid("2");
                await repository.InsertAsync(entity);
                await repository.CommitAsync();
            }

            using (var repository = CreateRepository())
            {
                var entities1 = await repository.LoadAsync(typeof(AnEntity), typeof(AnEntity).GetProperty(nameof(AnEntity.Name)), "toto");
                entities1.Should().ContainSingle().Which.Id.Should().Be(Tuid.Guid("1"));
                var entities2 = await repository.LoadAsync(typeof(AnEntity), typeof(AnEntity).GetProperty(nameof(AnEntity.NullableId)), Tuid.Guid("2"));
                entities2.Should().ContainSingle().Which.Id.Should().Be(Tuid.Guid("1"));
            }
        }

        [Fact]
        public async Task NoExceptionIfMoreThanOneResultForloadByUniqueMember()
        {
            using (var repository = CreateRepository())
            {
                var entity1 = new AnEntity();
                entity1.Id = Tuid.Guid("1");
                entity1.Name = "toto";
                var entity2 = new AnEntity();
                entity2.Id = Tuid.Guid("2");
                entity2.Name = "toto";
                await repository.InsertAsync(entity1);
                await repository.InsertAsync(entity2);
                await repository.CommitAsync();
            }

            using (var repository = CreateRepository())
            {
                var entities = await repository.LoadAsync(typeof(AnEntity), typeof(AnEntity).GetProperty(nameof(AnEntity.Name)), "toto");
                entities.Should().HaveCount(2);
                entities.Select(x => x.Id).Should().Contain(Tuid.Guid("1"))
                    .And.Contain(Tuid.Guid("2"));
            }
        }

        [Fact]
        public async Task CanQueryEntity()
        {
            using (var repository = CreateRepository())
            {
                var entity1 = new AnEntity().WithId();
                entity1.Name = "toto";
                await repository.InsertAsync(entity1);
                var entity2 = new AnEntity().WithId();
                entity2.Name = "titi";
                await repository.InsertAsync(entity2);
                await repository.CommitAsync();
            }

            using (var repository = CreateRepository())
            {
                var request = repository.Query<AnEntity>().Where(x => x.Name != null).OrderBy(x => x.Name).Take(10);
                var entities = await request.ToArrayAsync();
                entities.Select(x => x.Name).Should().BeEquivalentTo("toto", "titi");
            }
        }

        [Fact]
        public async Task CanQueryAbstractClass()
        {
            using (var repository = CreateRepository())
            {
                var entity1 = new AConcreteEntity1().WithId();
                entity1.Name = "toto";
                await repository.InsertAsync(entity1);
                var entity2 = new AConcreteEntity2().WithId();
                entity2.Name = "titi";
                await repository.InsertAsync(entity2);
                await repository.CommitAsync();
            }

            using (var repository = CreateRepository())
            {
                var request = repository.Query<AnAbstractEntity>().Where(x => x.Name != null).OrderBy(x => x.Name);
                var entities = await request.ToArrayAsync();
                entities.Select(x => x.Name).Should().BeEquivalentTo("titi", "toto");
            }
        }

        [Fact]
        public async Task CanQueryInterface()
        {
            var id1 = Guid.NewGuid();
            var id2 = Guid.NewGuid();
            var id3 = Guid.NewGuid();
            using (var repository = CreateRepository())
            {
                var entity1 = new AConcreteEntity1();
                entity1.Id = id1;
                entity1.Name = "toto";
                await repository.InsertAsync(entity1);
                var entity2 = new AConcreteEntity2();
                entity2.Id = id2;
                entity2.Name = "titi";
                await repository.InsertAsync(entity2);
                var entity3 = new AnEntity();
                entity3.Id = id3;
                entity3.Name = "tutu";
                await repository.InsertAsync(entity3);
                await repository.CommitAsync();
            }

            using (var repository = CreateRepository())
            {
                var request = repository.Query<IEntity>().Where(x => x.Id != default);
                var entities = await request.ToArrayAsync();
                entities.Select(x => x.Id).Should().BeEquivalentTo(new[] { id1, id2, id3 });
                
                var entity = await repository.Query<IEntity>().FirstOrDefaultAsync();
                entity.Id.Should().Be(id1);
            }
        }

        [Fact]
        public async Task CanLoadAbstractClass()
        {
            var id1 = Guid.NewGuid();
            var id2 = Guid.NewGuid();
            using (var repository = CreateRepository())
            {
                var entity1 = new AConcreteEntity1();
                entity1.Id = id1;
                entity1.Name = "toto";
                await repository.InsertAsync(entity1);
                var entity2 = new AConcreteEntity2();
                entity2.Id = id2;
                entity2.Name = "titi";
                await repository.InsertAsync(entity2);
                await repository.CommitAsync();
            }

            using (var repository = CreateRepository())
            {
                var entity1 = await repository.LoadAsync<AnAbstractEntity>(id1);
                entity1.Should().NotBeNull();
                var entity2 = await repository.LoadAsync<AnAbstractEntity>(id2);
                entity2.Should().NotBeNull();
                var entity3 = await repository.LoadAsync<AnAbstractEntity>(Guid.NewGuid());
                entity3.Should().BeNull();
            }
        }

        public class MyEFCoreRepository : EFCoreBaseRepository
        {
            public MyEFCoreRepository(Microsoft.EntityFrameworkCore.DbContext dbContext, ILogger logger, 
                IUnitOfWork unitOfWork, IMemoryCache memoryCache, bool detectChangesUsingStateStore = false) 
                : base(dbContext, logger, unitOfWork, memoryCache, detectChangesUsingStateStore)
            {
            }

            public async Task<TEntity> LoadAsync<TEntity>(Guid id)
            {
                var entities = await LoadAsync(typeof(TEntity), id);
                return (TEntity)entities.SingleOrDefault();
            }
        }

        //juste l� pour �viter la mise en cache du mappings par EFCore qui doit se baser sur le type du repo et de mani�re statique.
        //TODO utiliser IModelCacheKeyFactory
        public class MyDbContext : Microsoft.EntityFrameworkCore.DbContext
        {
            public MyDbContext(Microsoft.EntityFrameworkCore.DbContextOptions options) : base(options)
            {
            }

            protected override void OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder modelBuilder)
            {
                modelBuilder.Entity<AnEntity>();
                modelBuilder.Entity<AnotherEntity>();
                modelBuilder.Entity<AnAbstractEntity>()
                    .HasDiscriminator<int>("type")
                    .HasValue<AConcreteEntity1>(1)
                    .HasValue<AConcreteEntity2>(2);
                modelBuilder.Entity<AConcreteEntity1>();
                modelBuilder.Entity<AConcreteEntity2>();
            }
        }

        public interface IInterface : IEntity
        {
        }

        public class AnEntity : IInterface
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public Guid? NullableId { get; set; }
        }

        public abstract class AnAbstractEntity : IEntity
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
        }

        public class AConcreteEntity1 : AnAbstractEntity
        {
            public DateTime Date { get; set; }
        }

        public class AConcreteEntity2 : AnAbstractEntity
        {
            public int Age { get; set; }
        }

        public class AnotherEntity : IEntity
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
        }
    }
}
