using System;
using System.Data;
using System.Threading.Tasks;
using CocoriCore.TestUtils;
using FluentAssertions;
using Microsoft.Data.Sqlite;
using Xunit;
using System.Linq;
using CocoriCore.Common;
using Xunit.Abstractions;
using CocoriCore.Linq.Async;

namespace CocoriCore.EFCore.Test
{
    public class EFCoreAsyncQueryableTest
    {
        private readonly Microsoft.EntityFrameworkCore.DbContextOptionsBuilder _dbContextBuilder;

        public EFCoreAsyncQueryableTest(ITestOutputHelper output)
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            _dbContextBuilder = new Microsoft.EntityFrameworkCore.DbContextOptionsBuilder();
            _dbContextBuilder = Microsoft.EntityFrameworkCore.SqliteDbContextOptionsBuilderExtensions
                .UseSqlite(_dbContextBuilder, connection);
        }

        [Fact]
        public async Task QueryEntitiesWithFilterAndOrder()
        {
            var dbContext = new MyDbContext(_dbContextBuilder.Options);
            await dbContext.Database.EnsureCreatedAsync();

            var entity1 = new AnEntity().WithId();
            entity1.Name = "toto";
            var entity2 = new AnEntity().WithId();
            entity2.Name = "titi";
            var entity3 = new AnEntity().WithId();
            entity3.Name = null;
            dbContext.Add(entity1);
            dbContext.Add(entity2);
            dbContext.Add(entity3);

            await dbContext.SaveChangesAsync();

            var set = dbContext.Set<AnEntity>();
            IQueryable<AnEntity> queryable = new EFCoreAsyncQueryProvider<AnEntity>(set);

            var entities = await queryable
                .Where(x => x.Name != null)
                .OrderBy(x => x.Name)
                .ToArrayAsync();

            entities.Select(x => x.Name).Should().ContainInOrder("titi", "toto");
        }

        [Fact]
        public async Task QueryAllEntities()
        {
            var dbContext = new MyDbContext(_dbContextBuilder.Options);
            await dbContext.Database.EnsureCreatedAsync();

            var entity1 = new AnEntity().WithId();
            entity1.Name = "toto";
            var entity2 = new AnEntity().WithId();
            entity2.Name = "titi";
            var entity3 = new AnEntity().WithId();
            entity3.Name = null;
            dbContext.Add(entity1);
            dbContext.Add(entity2);
            dbContext.Add(entity3);

            await dbContext.SaveChangesAsync();

            var set = dbContext.Set<AnEntity>();
            IQueryable<AnEntity> queryable = new EFCoreAsyncQueryProvider<AnEntity>(set);

            var entities = await queryable.ToArrayAsync();

            entities.Select(x => x.Name).Should().BeEquivalentTo("titi", "toto", null);
        }

        [Fact]
        public async Task Select()
        {
            var dbContext = new MyDbContext(_dbContextBuilder.Options);
            await dbContext.Database.EnsureCreatedAsync();

            var entity1 = new AnEntity().WithId();
            entity1.Name = "toto";
            var entity2 = new AnEntity().WithId();
            entity2.Name = "titi";
            var entity3 = new AnEntity().WithId();
            entity3.Name = null;
            dbContext.Add(entity1);
            dbContext.Add(entity2);
            dbContext.Add(entity3);

            await dbContext.SaveChangesAsync();

            var set = dbContext.Set<AnEntity>();
            IQueryable<AnEntity> queryable = new EFCoreAsyncQueryProvider<AnEntity>(set);

            var entities = await queryable
                .Where(x => x.Name != null)
                .Select(x => x.Name)
                .ToArrayAsync();
            entities.Should().BeEquivalentTo("titi", "toto");
        }

        [Fact]
        public async Task GetFirst()
        {
            var dbContext = new MyDbContext(_dbContextBuilder.Options);
            await dbContext.Database.EnsureCreatedAsync();

            var entity1 = new AnEntity().WithId();
            entity1.Name = "toto";
            var entity2 = new AnEntity().WithId();
            entity2.Name = "titi";
            var entity3 = new AnEntity().WithId();
            entity3.Name = null;
            dbContext.Add(entity1);
            dbContext.Add(entity2);
            dbContext.Add(entity3);

            await dbContext.SaveChangesAsync();

            var set = dbContext.Set<AnEntity>();
            IQueryable<AnEntity> queryable = new EFCoreAsyncQueryProvider<AnEntity>(set);

            var entity = await queryable.Where(x => x.Name == null).FirstAsync();
            entity.Id.Should().Be(entity3.Id);
        }

        [Fact]
        public async Task GetSingle()
        {
            var dbContext = new MyDbContext(_dbContextBuilder.Options);
            await dbContext.Database.EnsureCreatedAsync();

            var entity1 = new AnEntity().WithId();
            entity1.Name = "toto";
            var entity2 = new AnEntity().WithId();
            entity2.Name = "titi";
            var entity3 = new AnEntity().WithId();
            entity3.Name = null;
            dbContext.Add(entity1);
            dbContext.Add(entity2);
            dbContext.Add(entity3);

            await dbContext.SaveChangesAsync();

            var set = dbContext.Set<AnEntity>();
            IQueryable<AnEntity> queryable = new EFCoreAsyncQueryProvider<AnEntity>(set);

            var entity = await queryable.Where(x => x.Name == null).SingleAsync();
            entity.Id.Should().Be(entity3.Id);
        }

        public class MyDbContext : Microsoft.EntityFrameworkCore.DbContext
        {
            public MyDbContext(Microsoft.EntityFrameworkCore.DbContextOptions options) : base(options)
            {
            }

            protected override void OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder modelBuilder)
            {
                modelBuilder.Entity<AnEntity>();
            }
        }

        public class AnEntity : IEntity
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
        }
    }
}
