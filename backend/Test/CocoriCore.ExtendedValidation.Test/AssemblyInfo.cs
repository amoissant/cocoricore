﻿using System.Reflection;

namespace CocoriCore.ExtendedValidation.Test
{
    public static class AssemblyInfo
    {
        public static Assembly Assembly => typeof(AssemblyInfo).Assembly;
    }
}
