using System;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using CocoriCore.Autofac;
using CocoriCore.Common;
using CocoriCore.Messaging;
using CocoriCore.TestUtils;
using FluentAssertions;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.Extensions.Caching.Memory;
using Xunit;

namespace CocoriCore.ExtendedValidation.Test
{
    public class HandlerWithValidatorTest
    {
        private ILifetimeScope _scope;
        private ContainerBuilder _builder;
        private UnitOfWorkOptionsBuilder _unitOfWorkBuilder;
        private MessageBusOptionsBuilder _optionBuilder;
        private IMessageBus _messageBus;

        public HandlerWithValidatorTest()
        {
            _builder = new ContainerBuilder();
            _optionBuilder = new MessageBusOptionsBuilder();
            _unitOfWorkBuilder = new UnitOfWorkOptionsBuilder();
            var assembly = this.GetType().Assembly;
            _optionBuilder
                .AddRule("default")
                .When(c => true)
                .Call<IValidatorExecutor>((x, c) => x.ValidateAsync(c.Message))
                .Call<IHandlerExecutor>(async (x, c) => c.Response = await x.ExecuteAsync(c.Message));

            _builder.Register(c => _optionBuilder.Build()).AsSelf();
            _builder.RegisterType<MessageBus>().As<IMessageBus>();
            _builder.RegisterType<HandlerExecutor>().As<IHandlerExecutor>()
                .WithParameter(new NamedParameter("assemblies", new[] { assembly }));
            _builder.RegisterType<ValidatorExecutor>().As<IValidatorExecutor>()
                .WithParameter(new NamedParameter("assemblies", new [] { assembly }));
            _builder.RegisterGeneric(typeof(EmbeddedValidator<>)).AsSelf();

            _builder.RegisterAssemblyTypes(assembly)
                .AsClosedTypesOf(typeof(IValidate<>)).AsSelf()
                .SingleInstance();
            _builder.RegisterAssemblyTypes(assembly)
                .AsClosedTypesOf(typeof(IValidator<>)).AsSelf()
                .SingleInstance();

            _builder.RegisterInstance(new MemoryCache(new MemoryCacheOptions())).As<IMemoryCache>();
            _builder.Register<Func<ILifetimeScope>>(c => Scope.BeginLifetimeScope).AsSelf();
            _builder.Register(c => _unitOfWorkBuilder.Options).As<UnitOfWorkOptions>().SingleInstance();
            _builder.RegisterType<AutofacUnitOfWorkFactory>().As<IUnitOfWorkFactory>().SingleInstance();
            _builder.RegisterType<AutofacUnitOfWork>()
                .As<IUnitOfWork>()
                .InstancePerLifetimeScope();
            _builder.ConfigureXunitLogger();
        }

        public ILifetimeScope Scope
        {
            get
            {
                if (_scope == null)
                    _scope = _builder.Build();
                return _scope;
            }
        }

        public IMessageBus MessageBus
        {
            get
            {
                if (_messageBus == null)
                    _messageBus = Scope.Resolve<IMessageBus>();
                return _messageBus;
            }
        }

        [Fact]
        public async Task ThrowValidationExceptionWhenInvalidCommandAndIntegratedValidator()
        {
            var handler = Scope.Resolve<AHandlerWithValidator>();

            Func<Task> action = () => MessageBus.ExecuteAsync(new ACommand() { Name = null });

            await action.Should().ThrowAsync<ValidationException>();
            handler.IsValidatorCalled.Should().BeTrue();
            handler.IsExecuteCalled.Should().BeFalse();
        }

        [Fact]
        public async Task NoValidationExceptionWhenValidCommandAndIntegratedValidator()
        {
            var handler = Scope.Resolve<AHandlerWithValidator>();

            Func<Task> action = () => MessageBus.ExecuteAsync(new ACommand() { Name = "toto" });

            await action.Should().NotThrowAsync<Exception>();
            handler.IsValidatorCalled.Should().BeTrue();
            handler.IsExecuteCalled.Should().BeTrue();
        }

        [Fact]
        public async Task CallAllValidatorAssociatedToMessage()
        {
            var extendedValidator = Scope.Resolve<AnotherCommandExtendedValidator>();
            var validator = Scope.Resolve<AnotherCommandValidator>();
            var handler = Scope.Resolve<AnotherCommandHandlerWithValidator>();

            Func<Task> action = () => MessageBus.ExecuteAsync(new AnotherCommand() { Name = "toto" });

            await action.Should().NotThrowAsync<Exception>();
            extendedValidator.IsValidatorCalled.Should().Be(true);
            validator.IsValidatorCalled.Should().Be(true);
            handler.IsValidatorCalled.Should().Be(true);
            handler.IsExecuteCalled.Should().Be(true);
        }

        [Fact]
        public async Task CallAutonomousValidatorsAssociatedToMessage()
        {
            _optionBuilder.Rules.Clear();
            _optionBuilder
                .AddRule("")
                .When(c => true)
                .Call<IValidatorExecutor>((x, c) => x.CallAutonomousValidatorsAsync(c.Message))
                .Call<IHandlerExecutor>((x, c) => x.ExecuteAsync(c.Message));

            var extendedValidator = Scope.Resolve<AnotherCommandExtendedValidator>();
            var validator = Scope.Resolve<AnotherCommandValidator>();
            var handler = Scope.Resolve<AnotherCommandHandlerWithValidator>();

            Func<Task> action = () => MessageBus.ExecuteAsync(new AnotherCommand() { Name = "toto" });

            await action.Should().NotThrowAsync<Exception>();
            extendedValidator.IsValidatorCalled.Should().Be(true);
            validator.IsValidatorCalled.Should().Be(true);
            handler.IsValidatorCalled.Should().Be(false);
            handler.IsExecuteCalled.Should().Be(true);
        }

        [Fact]
        public async Task CallEmbedValidatorsAssociatedToMessage()
        {
            _optionBuilder.Rules.Clear();
            _optionBuilder
               .AddRule("")
               .When(c => true)
               .Call<IValidatorExecutor>((x, c) => x.CallEmbedValidatorsAsync(c.Message))
               .Call<IHandlerExecutor>((x, c) => x.ExecuteAsync(c.Message));

            var extendedValidator = Scope.Resolve<AnotherCommandExtendedValidator>();
            var validator = Scope.Resolve<AnotherCommandValidator>();
            var handler = Scope.Resolve<AnotherCommandHandlerWithValidator>();

            Func<Task> action = () => MessageBus.ExecuteAsync(new AnotherCommand() { Name = "toto" });

            await action.Should().NotThrowAsync<Exception>();
            extendedValidator.IsValidatorCalled.Should().Be(false);
            validator.IsValidatorCalled.Should().Be(false);
            handler.IsValidatorCalled.Should().Be(true);
            handler.IsExecuteCalled.Should().Be(true);
        }

        [Fact]
        public async Task StopsWhenFirstValidatorThrowException()
        {
            var extendedValidator = Scope.Resolve<AnotherCommandExtendedValidator>();
            var validator = Scope.Resolve<AnotherCommandValidator>();
            var handler = Scope.Resolve<AnotherCommandHandlerWithValidator>();

            Func<Task> action = () => MessageBus.ExecuteAsync(new AnotherCommand() { Name = string.Empty });

            await action.Should().ThrowAsync<ValidationException>();
            extendedValidator.IsValidatorCalled.Should().Be(true);
            validator.IsValidatorCalled.Should().Be(false);
            handler.IsValidatorCalled.Should().Be(false);
            handler.IsExecuteCalled.Should().Be(false);
        }

        class ACommand : ICommand
        {
            public string Name;
        }

        class AHandlerWithValidator : VoidCommandHandler<ACommand>, IValidate<ACommand>
        {
            public bool IsExecuteCalled = false;
            public bool IsValidatorCalled = false;

            public void SetupValidator(ExtendedValidator<ACommand> validator)
            {
                validator.RuleFor(x => x.Name).NotEmpty();
                IsValidatorCalled = true;
            }

            public override Task ExecuteAsync(ACommand command)
            {
                IsExecuteCalled = true;
                return Task.CompletedTask;
            }
        }


        class AnotherCommand : ICommand
        {
            public string Name;
        }

        class AnotherCommandExtendedValidator : ExtendedValidator<AnotherCommand>
        {
            public bool IsValidatorCalled = false;
            public AnotherCommandExtendedValidator(IUnitOfWork unitOfWork) : base(unitOfWork)
            {
                RuleFor(x => x.Name).NotEmpty();
            }

            public override Task<ValidationResult> ValidateAsync(ValidationContext<AnotherCommand> context, CancellationToken cancellation = default)
            {
                IsValidatorCalled = true;
                return base.ValidateAsync(context, cancellation);
            }
        }

        class AnotherCommandValidator : AbstractValidator<AnotherCommand>
        {
            public bool IsValidatorCalled = false;
            public AnotherCommandValidator()
            {
                RuleFor(x => x.Name).NotEmpty();
            }

            public override Task<ValidationResult> ValidateAsync(ValidationContext<AnotherCommand> context, CancellationToken cancellation = default)
            {
                IsValidatorCalled = true;
                return base.ValidateAsync(context, cancellation);
            }
        }

        class AnotherCommandHandlerWithValidator : VoidCommandHandler<AnotherCommand>, IValidate<AnotherCommand>
        {
            public bool IsExecuteCalled = false;
            public bool IsValidatorCalled = false;

            public void SetupValidator(ExtendedValidator<AnotherCommand> validator)
            {
                validator.RuleFor(x => x.Name).NotEmpty();
                IsValidatorCalled = true;
            }

            public override Task ExecuteAsync(AnotherCommand command)
            {
                IsExecuteCalled = true;
                return Task.CompletedTask;
            }
        }
    }
}