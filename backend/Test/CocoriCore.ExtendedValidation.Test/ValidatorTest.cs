using System;
using CocoriCore.Common;
using System.Globalization;
using System.Threading.Tasks;
using Autofac;
using FluentAssertions;
using Xunit;
using CocoriCore.Repository;
using CocoriCore.Autofac;
using System.Collections.Generic;
using CocoriCore.Resource;

namespace CocoriCore.ExtendedValidation.Test
{
    public class ValidatorTest
    {
        private readonly IFormats _formats;
        private readonly ILifetimeScope _scope;
        private readonly ResourceDictionary _resourceDictionary;

        public ValidatorTest()
        {
            _formats = new Formats();
            var builder = new ContainerBuilder();
            builder.RegisterType<InMemoryEntityStore>().As<IInMemoryEntityStore>().SingleInstance();

            builder.RegisterType<InMemoryBaseRepository>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<AppRepository>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.Register(c => new UnitOfWorkOptionsBuilder().Options).As<UnitOfWorkOptions>().SingleInstance();
            builder.RegisterType<AutofacUnitOfWork>().As<IUnitOfWork>().SingleInstance();
            builder.RegisterType<AutofacUnitOfWorkFactory>().As<IUnitOfWorkFactory>().SingleInstance();

            var resourceProviderOptionBuilder = new ResourceProviderOptionsBuilder();
            _resourceDictionary = new ResourceDictionary();
            resourceProviderOptionBuilder
                .For<string>()
                .UseDictionaryResourceProvider(_resourceDictionary)
                .GetPathUsing(k => new Path(k));
            builder.RegisterInstance(resourceProviderOptionBuilder.Options);
            builder.RegisterType<ResourceProvider>().As<IResourceProvider>().InstancePerLifetimeScope();
            builder.RegisterType<DictionaryResourceProvider>().AsSelf().InstancePerLifetimeScope();
            builder.RegisterInstance(_formats).As<IFormats>();

            _scope = builder.Build();
        }

        [Fact]
        public void ValidateStringMatchingDateTime()
        {
            var message = new AMessage();
            message.datetime = "31-12-1986";
            _formats.Set<DateTime>("dd-MM-yyyy");
            var validator = new AMessageValidator(_scope.Resolve<IUnitOfWork>());
            var result = validator.Validate(message);

            result.Errors.Should().BeEmpty();
        }

        [Fact]
        public void UnvalidateStringMatchingDateTime()
        {
            var message = new AMessage();
            message.datetime = "31-12-1986";
            _formats.Set<DateTime>("MM-dd-yyyy");
            var validator = new AMessageValidator(_scope.Resolve<IUnitOfWork>());
            var result = validator.Validate(message);

            result.Errors.Should().ContainSingle()
                .Which.ErrorMessage.Should().Be("datetime must be a string corresponding to DateTime format.");
            //TODO peut-on avoir le type du message et la profondeurs des champs ex : (AMessage.?)adresseLivraison.codePostal
        }

        [Fact]
        public void ValidateStringMatchingTimeSpan()
        {
            var message = new AMessage();
            message.timespan = "1.02:03:04.0050000";
            var validator = new AMessageValidator(_scope.Resolve<IUnitOfWork>());
            var result = validator.Validate(message);

            result.Errors.Should().BeEmpty();
        }

        [Fact]
        public void UnvalidateStringMatchingTimeSpan()
        {
            var message = new AMessage();
            message.timespan = "truc";
            var validator = new AMessageValidator(_scope.Resolve<IUnitOfWork>());
            var result = validator.Validate(message);

            result.Errors.Should().ContainSingle()
                .Which.ErrorMessage.Should().Be("timespan must be a string corresponding to TimeSpan format.");
        }


        [Fact]
        public async Task ValidateEntityId()
        {
            var entity = new AnEntity();
            var repository = _scope.Resolve<IRepository>();
            await repository.InsertAsync(entity);

            var message = new AGuidMessage();
            message.id = entity.Id;

            var validator = new AGuidMessageValidator(_scope.Resolve<IUnitOfWork>());
            var result = validator.Validate(message);

            result.Errors.Should().BeEmpty();
        }

        [Fact]
        public async Task UnvalidateEntityId()
        {
            var entity = new AnEntity();
            var repository = _scope.Resolve<IRepository>();
            await repository.InsertAsync(entity);

            var message = new AGuidMessage();
            message.id = Guid.NewGuid();

            var validator = new AGuidMessageValidator(_scope.Resolve<IUnitOfWork>());
            var result = validator.Validate(message);

            result.Errors.Should().ContainSingle()
                .Which.ErrorMessage.Should().Be($"id with value {message.id} must correspond to an entity of type {typeof(AnEntity)}.");
        }

        [Fact]
        public void UseCodeAndMessageFromResouceProvider()
        {
            var message = new AnotherMessage();
            message.Name = null;
            _resourceDictionary.Add("MY_CODE", "My error message");

            var validator = new AMessageValidatorWithResouce(_scope.Resolve<IUnitOfWork>());
            var result = validator.Validate(message);

            var error = result.Errors.Should().ContainSingle().Which;
            error.ErrorMessage.Should().Be("My error message");
            error.ErrorCode.Should().Be("MY_CODE");
        }

        public class AppRepository : DecorableRepository, IRepository, IExists
        {
            public AppRepository(IBaseRepository baseRepository, IUnitOfWork unitOfWork, IEnumerable<Type> decoratorTypes)
                : base(baseRepository, unitOfWork, decoratorTypes)
            {
            }
        }
    }
}