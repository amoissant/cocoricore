using System;
using System.Globalization;
using CocoriCore.Common;
using CocoriCore.Resource;
using FluentValidation;

namespace CocoriCore.ExtendedValidation
{
    public class AMessageValidator : ExtendedValidator<AMessage>
    {
        public AMessageValidator(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            RuleFor(x => x.datetime).MatchDateTime().When(x => x.datetime != null);
            RuleFor(x => x.timespan).MatchTimeSpan().When(x => x.timespan != null);
        }
    }

    public class NotProperlyInheritedValidator : AbstractValidator<AMessage>
    {
        public NotProperlyInheritedValidator()
        {
            RuleFor(x => x.datetime).MatchDateTime().When(x => x.datetime != null);
            RuleFor(x => x.timespan).MatchTimeSpan().When(x => x.timespan != null);
        }
    }

    public class AMessage
    {
        public string datetime;
        public string timespan;
    }

    public class AGuidMessageValidator : ExtendedValidator<AGuidMessage>
    {
        public AGuidMessageValidator(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            RuleFor(x => x.id).Identifier(typeof(AnEntity));
        }
    }

    public class AMessageValidatorWithResouce : ExtendedValidator<AnotherMessage>
    {
        public AMessageValidatorWithResouce(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            RuleFor(x => x.Name).NotEmpty().WithResourceMessage("MY_CODE");
        }
    }

    public class AnEntity : IEntity
    {
        public Guid Id { get; set; }
    }

    public class AGuidMessage : ICommand
    {
        public Guid id;
    }

    public class AnotherMessage : ICommand
    {
        public string Name;
    }
}