﻿using CocoriCore.EntityAccess;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Autofac;
using CocoriCore.TestUtils;
using FluentAssertions;
using Microsoft.Extensions.Caching.Memory;
using CocoriCore.Common;
using CocoriCore.Repository;
using CocoriCore.Autofac;
using System.Collections.Generic;
using Xunit.Abstractions;
using CocoriCore.Linq.Async;
using CocoriCore.Collection.Extensions;

namespace CocoriCore.Test
{
    public class EntityQueryFilterControllerTest
    {
        private Lazy<ILifetimeScope> _rootScope;
        private ContainerBuilder _builder;
        private EntityAccessOptionsBuilder _accessOptionsbuilder;
        private IndexedSets<Enum, object> _userAuthorizations;

        public EntityQueryFilterControllerTest(ITestOutputHelper output)
        {
            _builder = new ContainerBuilder();
            _rootScope = new Lazy<ILifetimeScope>(() => _builder.Build());

            _userAuthorizations = new IndexedSets<Enum, object>();
            _builder.RegisterInstance(_userAuthorizations);

            _accessOptionsbuilder = new EntityAccessOptionsBuilder();
            _builder.Register(c => _accessOptionsbuilder.Options).SingleInstance();
            _builder.RegisterType<MyEntityAccessController>()
                .AsSelf()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
            _builder.RegisterType<EntityQueryFilterController>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
            _builder.RegisterType<EntityReadWriteAccessController>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            _builder.RegisterType<InMemoryEntityStore>().As<IInMemoryEntityStore>().SingleInstance();
            _builder.RegisterType<InMemoryBaseRepository>().As<IBaseRepository>().InstancePerLifetimeScope();
            _builder.RegisterType<EntityAccessDecorator>().AsSelf().InstancePerLifetimeScope();//TODO autoregister ces décorators
            _builder.RegisterType<StateStoreDecorator>().AsSelf().InstancePerLifetimeScope();//TODO autoregister ces décorators

            _builder.RegisterType<MyRepository>().As<IEntityAccessRepository>().InstancePerLifetimeScope();
            _builder.RegisterType<MyRepository>().As<IRepository>().InstancePerLifetimeScope()
                .WithParameter(new TypedParameter<IEnumerable<Type>>(new[] { typeof(EntityAccessDecorator), typeof(StateStoreDecorator) }));

            _builder.RegisterType<StateStore>().As<IStateStore>().InstancePerLifetimeScope();
            _builder.RegisterType<Utf8JsonCopier>().As<ICopier>().InstancePerLifetimeScope();
            _builder.RegisterInstance(new MemoryCache(new MemoryCacheOptions())).As<IMemoryCache>();

            _builder.Register(c => new UnitOfWorkOptionsBuilder().Options).As<UnitOfWorkOptions>().SingleInstance();
            _builder.RegisterType<AutofacUnitOfWorkFactory>().As<IUnitOfWorkFactory>().SingleInstance();
            _builder.RegisterType<AutofacUnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();

            _builder.RegisterInstance(new MemoryCache(new MemoryCacheOptions())).As<IMemoryCache>();

            _builder.ConfigureXunitLogger(output);
        }

        private ILifetimeScope RootScope => _rootScope.Value;

        private IRepository PersitentStore => RootScope.Resolve<IRepository>();//TODO retourner le IPersistentStore et faire des méthodes d'extension spéciales pour les tests (et synchornes) pour aider aux assertions

        private IUnitOfWorkFactory UnitOfWorkFactory => RootScope.Resolve<IUnitOfWorkFactory>();

        [Fact]
        public async Task DontFilterWhenNoRule()
        {
            await PersitentStore.InsertManyAsync(new[]
            {
                new AnEntity { Id = Tuid.Guid("1"), Type1Id = Tuid.Guid("a") },
                new AnEntity { Id = Tuid.Guid("2"), Type1Id = Tuid.Guid("b") }
            });
            _userAuthorizations[AuthorizationType.Type1].Add(Tuid.Guid("a"));

            IEnumerable<AnEntity> results;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                results = await repository.Query<AnEntity>().ToListAsync();
            }

            results.Select(x => x.Id).Should()
                .Contain(Tuid.Guid("1"))
                .And.Contain(Tuid.Guid("2"));
        }

        [Fact]
        public async Task FilterAllIfNoAuthorizationForEntityType()
        {
            await PersitentStore.InsertManyAsync(new[]
            {
                new AnEntity { Id = Tuid.Guid("1"), Type1Id = Tuid.Guid("a") },
                new AnEntity { Id = Tuid.Guid("2"), Type1Id = Tuid.Guid("b") }
            });
            _accessOptionsbuilder.RestrictAccess<AnEntity>().By(AuthorizationType.Type1, x => x.Type1Id);

            IEnumerable<AnEntity> results;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                results = await repository.Query<AnEntity>().ToListAsync();
            }

            results.Select(x => x.Id).Should()
                .NotContain(Tuid.Guid("1"))
                .And.NotContain(Tuid.Guid("2"));
        } 
       
        [Fact]
        public async Task FilterUse_And_BetweenRules()
        {
            await PersitentStore.InsertManyAsync(new[]
            {
                new AnEntity { Id = Tuid.Guid("1"), Type1Id = Tuid.Guid("a"), Type2Id = Tuid.Guid("x") },
                new AnEntity { Id = Tuid.Guid("2"), Type1Id = Tuid.Guid("a"), Type2Id = Tuid.Guid("y") },
                new AnEntity { Id = Tuid.Guid("3"), Type1Id = Tuid.Guid("b"), Type2Id = Tuid.Guid("x") }
            });
            _accessOptionsbuilder.RestrictAccess<AnEntity>()
                .By(AuthorizationType.Type1, x => x.Type1Id)
                .By(AuthorizationType.Type2, x => x.Type2Id);
            _userAuthorizations[AuthorizationType.Type1].Add(Tuid.Guid("a"));
            _userAuthorizations[AuthorizationType.Type2].Add(Tuid.Guid("x"));

            IEnumerable<AnEntity> results;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                results = await repository.Query<AnEntity>().ToListAsync();
            }
            
            results.Select(x => x.Id).Should()
                .Contain(Tuid.Guid("1"))
                .And.NotContain(Tuid.Guid("2"))
                .And.NotContain(Tuid.Guid("3"));
        }

        [Fact]
        public async Task FilterWithFilterMember_guid()
        {
            await PersitentStore.InsertManyAsync(new[]
            {
                new AnEntity { Id = Tuid.Guid("1"), Type1Id = Tuid.Guid("a") },
                new AnEntity { Id = Tuid.Guid("2"), Type1Id = Tuid.Guid("b") },
                new AnEntity { Id = Tuid.Guid("3"), Type1Id = Tuid.Guid("c") },
                new AnEntity { Id = Tuid.Guid("4"), Type1Id = Guid.Empty }
            });
            _accessOptionsbuilder.RestrictAccess<AnEntity>()
                .By(AuthorizationType.Type1, x => x.Type1Id);
            _userAuthorizations[AuthorizationType.Type1].AddRange(Tuid.Guid("a"), Tuid.Guid("b"));

            IEnumerable<AnEntity> results;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                results = await repository.Query<AnEntity>().ToListAsync();
            }

            results.Select(x => x.Id).Should()
                .Contain(Tuid.Guid("1"))
                .And.Contain(Tuid.Guid("2"))
                .And.NotContain(Tuid.Guid("3"))
                .And.NotContain(Tuid.Guid("4"));
        }

        [Fact]
        public async Task FilterWithFilterMember_guid_allowNullOrEmpty()
        {
            await PersitentStore.InsertManyAsync(new[]
            {
                new AnEntity { Id = Tuid.Guid("1"), Type1Id = Tuid.Guid("a") },
                new AnEntity { Id = Tuid.Guid("2"), Type1Id = Tuid.Guid("b") },
                new AnEntity { Id = Tuid.Guid("3"), Type1Id = Tuid.Guid("c") },
                new AnEntity { Id = Tuid.Guid("4"), Type1Id = Guid.Empty }
            });
            _accessOptionsbuilder.RestrictAccess<AnEntity>()
                .By(AuthorizationType.Type1, x => x.Type1Id, allowNullOrEmpty: true);
            _userAuthorizations[AuthorizationType.Type1].AddRange(Tuid.Guid("a"), Tuid.Guid("b"));

            IEnumerable<AnEntity> results;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                results = await repository.Query<AnEntity>().ToListAsync();
            }

            results.Select(x => x.Id).Should()
                .Contain(Tuid.Guid("1"))
                .And.Contain(Tuid.Guid("2"))
                .And.NotContain(Tuid.Guid("3"))
                .And.Contain(Tuid.Guid("4"));
        }

        [Fact]
        public async Task FilterWithFilterMember_nullableGuid()
        {
            await PersitentStore.InsertManyAsync(new[]
            {
                new AnEntity { Id = Tuid.Guid("1"), NullableType1Id = Tuid.Guid("a") },
                new AnEntity { Id = Tuid.Guid("2"), NullableType1Id = Tuid.Guid("b") },
                new AnEntity { Id = Tuid.Guid("3"), NullableType1Id = Tuid.Guid("c") },
                new AnEntity { Id = Tuid.Guid("4"), NullableType1Id = Guid.Empty },
                new AnEntity { Id = Tuid.Guid("5"), NullableType1Id = null }
            });
            _accessOptionsbuilder.RestrictAccess<AnEntity>()
                .By(AuthorizationType.Type1, x => x.NullableType1Id);
            _userAuthorizations[AuthorizationType.Type1].AddRange(Tuid.Guid("a"), Tuid.Guid("b"));

            IEnumerable<AnEntity> results;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                results = await repository.Query<AnEntity>().ToListAsync();
            }

            results.Select(x => x.Id).Should()
                .Contain(Tuid.Guid("1"))
                .And.Contain(Tuid.Guid("2"))
                .And.NotContain(Tuid.Guid("3"))
                .And.NotContain(Tuid.Guid("4"))
                .And.NotContain(Tuid.Guid("5"));
        }

        [Fact]
        public async Task FilterWithFilterMember_nullableGuid_allowNullOrEmpty()
        {
            await PersitentStore.InsertManyAsync(new[]
            {
                new AnEntity { Id = Tuid.Guid("1"), NullableType1Id = Tuid.Guid("a") },
                new AnEntity { Id = Tuid.Guid("2"), NullableType1Id = Tuid.Guid("b") },
                new AnEntity { Id = Tuid.Guid("3"), NullableType1Id = Tuid.Guid("c") },
                new AnEntity { Id = Tuid.Guid("4"), NullableType1Id = Guid.Empty },
                new AnEntity { Id = Tuid.Guid("5"), NullableType1Id = null }
            });
            _accessOptionsbuilder.RestrictAccess<AnEntity>()
                .By(AuthorizationType.Type1, x => x.NullableType1Id, allowNullOrEmpty: true);
            _userAuthorizations[AuthorizationType.Type1].AddRange(Tuid.Guid("a"), Tuid.Guid("b"));

            IEnumerable<AnEntity> results;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                results = await repository.Query<AnEntity>().ToListAsync();
            }

            results.Select(x => x.Id).Should()
                .Contain(Tuid.Guid("1"))
                .And.Contain(Tuid.Guid("2"))
                .And.NotContain(Tuid.Guid("3"))
                .And.Contain(Tuid.Guid("4"))
                .And.Contain(Tuid.Guid("5"));
        }

        [Fact]
        public async Task FilterWithFilterMember_string()
        {
            await PersitentStore.InsertManyAsync(new[]
            {
                new AnEntity { Id = Tuid.Guid("1"), ConcatenedIds = $"{Tuid.Guid("a")}-{Tuid.Guid("z")}" },
                new AnEntity { Id = Tuid.Guid("2"), ConcatenedIds = $"{Tuid.Guid("b")}-{Tuid.Guid("z")}" },
                new AnEntity { Id = Tuid.Guid("3"), ConcatenedIds = $"{Tuid.Guid("c")}-{Tuid.Guid("z")}" },
                new AnEntity { Id = Tuid.Guid("4"), ConcatenedIds = string.Empty },
                new AnEntity { Id = Tuid.Guid("5"), ConcatenedIds = null }
            });
            _accessOptionsbuilder.RestrictAccess<AnEntity>()
                .By(AuthorizationType.Type1, x => x.ConcatenedIds);
            _userAuthorizations[AuthorizationType.Type1].AddRange(Tuid.Guid("a"), Tuid.Guid("b"));

            IEnumerable<AnEntity> results;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                results = await repository.Query<AnEntity>().ToListAsync();
            }

            results.Select(x => x.Id).Should()
                .Contain(Tuid.Guid("1"))
                .And.Contain(Tuid.Guid("2"))
                .And.NotContain(Tuid.Guid("3"))
                .And.NotContain(Tuid.Guid("4"))
                .And.NotContain(Tuid.Guid("5"));
        }

        [Fact]
        public async Task FilterWithFilterMember_string_allowNullOrEmpty()
        {
            await PersitentStore.InsertManyAsync(new[]
            {
                new AnEntity { Id = Tuid.Guid("1"), ConcatenedIds = $"{Tuid.Guid("a")}-{Tuid.Guid("z")}" },
                new AnEntity { Id = Tuid.Guid("2"), ConcatenedIds = $"{Tuid.Guid("b")}-{Tuid.Guid("z")}" },
                new AnEntity { Id = Tuid.Guid("3"), ConcatenedIds = $"{Tuid.Guid("c")}-{Tuid.Guid("z")}" },
                new AnEntity { Id = Tuid.Guid("4"), ConcatenedIds = string.Empty },
                new AnEntity { Id = Tuid.Guid("5"), ConcatenedIds = null }
            });
            _accessOptionsbuilder.RestrictAccess<AnEntity>()
                .By(AuthorizationType.Type1, x => x.ConcatenedIds, allowNullOrEmpty: true);
            _userAuthorizations[AuthorizationType.Type1].AddRange(Tuid.Guid("a"), Tuid.Guid("b"));

            IEnumerable<AnEntity> results;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                results = await repository.Query<AnEntity>().ToListAsync();
            }

            results.Select(x => x.Id).Should()
                .Contain(Tuid.Guid("1"))
                .And.Contain(Tuid.Guid("2"))
                .And.NotContain(Tuid.Guid("3"))
                .And.Contain(Tuid.Guid("4"))
                .And.Contain(Tuid.Guid("5"));
        }

        [Fact]
        public async Task ExceptionIfFilterMemberTypeIsNotSupportedAsync()
        {
            _accessOptionsbuilder.RestrictAccess<AnEntity>()
                .By(AuthorizationType.Type1, x => x.UnsupportedMemberType);
            _userAuthorizations[AuthorizationType.Type1].AddRange(new { }, new { });

            Func<Task> action = async () =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    await repository.Query<AnEntity>().ToListAsync();
                }
            };

            var assertion = await action.Should().ThrowAsync<NotSupportedException>();
            assertion.Which
                .Message.Should().Contain(nameof(AnEntity))
                .And.Contain(nameof(AnEntity.UnsupportedMemberType))
                .And.Contain("System.Object");
        }

        public class MyRepository : DecorableRepository, IEntityAccessRepository, IRepository
        {
            public MyRepository(IBaseRepository baseRepository, IUnitOfWork unitOfWork, IEnumerable<Type> decoratorTypes)
                : base(baseRepository, unitOfWork, decoratorTypes)
            {
            }
        }

        public class MyEntityAccessController : EntityAccessControllerBase, IEntityAccessController
        {
            private IndexedSets<Enum, object> _userAuthorizations;

            public MyEntityAccessController(IEntityQueryFilterController queryFilterController,
                IEntityReadWriteAccessController readWriteAccessController, IStateStore stateStore, IndexedSets<Enum, object> userAuthorizations)
                : base(queryFilterController, readWriteAccessController, stateStore)
            {
                _userAuthorizations = userAuthorizations;
            }

            protected override IndexedSets<Enum, object> GetUserAuthorizations()
            {
                return _userAuthorizations;
            }
        }

        public enum AuthorizationType
        {
            Type1,
            Type2
        }

        public class AnEntity : IEntity
        {
            public Guid Id { get; set; }
            public Guid Type1Id { get; set; }
            public Guid Type2Id { get; set; }
            public Guid? NullableType1Id { get; set; }
            public string ConcatenedIds { get; set; }
            public object UnsupportedMemberType { get; set; }
        }
    }
}
