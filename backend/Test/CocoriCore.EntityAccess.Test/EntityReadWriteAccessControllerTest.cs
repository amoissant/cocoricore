﻿using CocoriCore.EntityAccess;
using System;
using Autofac;
using Xunit;
using System.Threading.Tasks;
using CocoriCore.TestUtils;
using System.Linq;
using FluentAssertions;
using CocoriCore.Autofac;
using Microsoft.Extensions.Caching.Memory;
using CocoriCore.Common;
using CocoriCore.Repository;
using System.Collections.Generic;
using CocoriCore.Linq.Async;
using CocoriCore.Collection.Extensions;

namespace CocoriCore.Test
{
    public class EntityReadWriteAccessControllerTest
    {
        private Lazy<ILifetimeScope> _rootScope;
        private ContainerBuilder _builder;
        private EntityAccessOptionsBuilder _accessOptionsbuilder;
        private IndexedSets<Enum, object> _userAuthorizations;

        public EntityReadWriteAccessControllerTest()
        {
            _builder = new ContainerBuilder();
            _rootScope = new Lazy<ILifetimeScope>(() => _builder.Build());

            _userAuthorizations = new IndexedSets<Enum, object>();
            _builder.RegisterInstance(_userAuthorizations);
            
            _accessOptionsbuilder = new EntityAccessOptionsBuilder();
            _builder.Register(c => _accessOptionsbuilder.Options).SingleInstance();
            _builder.RegisterType<MyEntityAccessController>()
                .AsSelf()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
            _builder.RegisterType<EntityQueryFilterController>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
            _builder.RegisterType<EntityReadWriteAccessController>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
            
            _builder.RegisterType<InMemoryEntityStore>().As<IInMemoryEntityStore>().SingleInstance();
            _builder.RegisterType<InMemoryBaseRepository>().As<IBaseRepository>().InstancePerLifetimeScope();
            _builder.RegisterType<EntityAccessDecorator>().AsSelf().InstancePerLifetimeScope();//TODO autoregister ces décorators
            _builder.RegisterType<StateStoreDecorator>().AsSelf().InstancePerLifetimeScope();//TODO autoregister ces décorators

            _builder.RegisterType<MyRepository>().As<IEntityAccessRepository>().InstancePerLifetimeScope();
            _builder.RegisterType<MyRepository>().As<IRepository>().InstancePerLifetimeScope()
                .WithParameter(new TypedParameter<IEnumerable<Type>>(new [] { typeof(EntityAccessDecorator), typeof(StateStoreDecorator) }));

            _builder.RegisterType<StateStore>().As<IStateStore>().InstancePerLifetimeScope();
            _builder.RegisterType<Utf8JsonCopier>().As<ICopier>().InstancePerLifetimeScope();
            _builder.RegisterInstance(new MemoryCache(new MemoryCacheOptions())).As<IMemoryCache>();

            _builder.Register(c => new UnitOfWorkOptionsBuilder().Options).As<UnitOfWorkOptions>().SingleInstance();
            _builder.RegisterType<AutofacUnitOfWorkFactory>().As<IUnitOfWorkFactory>().SingleInstance();
            _builder.RegisterType<AutofacUnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();

            _builder.ConfigureXunitLogger();
        }

        private ILifetimeScope RootScope => _rootScope.Value;

        private IRepository PersitentStore => RootScope.Resolve<IRepository>();//TODO retourner le IPersistentStore et faire des méthodes d'extension spéciales pour les tests (et synchornes) pour aider aux assertions

        private IUnitOfWorkFactory UnitOfWorkFactory => RootScope.Resolve<IUnitOfWorkFactory>();

        [Fact]
        public async Task CheckUserAuthorizationsWhenLoadEntity()
        {
            await PersitentStore.InsertManyAsync(new[]
            {
                new AnEntity { Id = Tuid.Guid("1"), Type1Id = Tuid.Guid("a") },
                new AnEntity { Id = Tuid.Guid("2"), Type1Id = Tuid.Guid("b") }
            });
            _accessOptionsbuilder.RestrictAccess<AnEntity>().By(AuthorizationType.Type1, x => x.Type1Id);
            _userAuthorizations[AuthorizationType.Type1].Add(Tuid.Guid("a"));

            Func<Guid, Task> loadEntity = async (id) =>
            {
                using(var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    var controller = uow.Resolve<IEntityAccessController>();
                    await repository.LoadAsync<AnEntity>(id);
                    await controller.CkeckEntitiesAccessForUserAsync();
                }
            };
            Func<Task> action1 = () => loadEntity(Tuid.Guid("1"));
            Func<Task> action2 = () => loadEntity(Tuid.Guid("2"));

            await action1.Should().NotThrowAsync<Exception>();
            await action2.Should().ThrowAsync<ForbiddenAccessException>();
        }

        [Fact]
        public async Task CheckUserAuthorizationsWhenLoadManyEntities()
        {
            await PersitentStore.InsertManyAsync(new[]
            {
                new AnEntity { Id = Tuid.Guid("1"), Type1Id = Tuid.Guid("a") },
                new AnEntity { Id = Tuid.Guid("2"), Type1Id = Tuid.Guid("b") }
            });
            _accessOptionsbuilder.RestrictAccess<AnEntity>().By(AuthorizationType.Type1, x => x.Type1Id);
            _userAuthorizations[AuthorizationType.Type1].Add(Tuid.Guid("a"));

            Func<Guid, Task> loadEntity = async (id) =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    var controller = uow.Resolve<IEntityAccessController>();
                    await repository.LoadAsync<AnEntity>(new[] { id });
                    await controller.CkeckEntitiesAccessForUserAsync();
                }
            };
            Func<Task> action1 = () => loadEntity(Tuid.Guid("1"));
            Func<Task> action2 = () => loadEntity(Tuid.Guid("2"));

            await action1.Should().NotThrowAsync<Exception>();
            await action2.Should().ThrowAsync<ForbiddenAccessException>();
        }

        [Fact]
        public async Task CheckUserAuthorizationsWhenLoadMByUniqueMember()
        {
            await PersitentStore.InsertManyAsync(new[]
            {
                new AnEntity { Id = Tuid.Guid("1"), Type1Id = Tuid.Guid("a") },
                new AnEntity { Id = Tuid.Guid("2"), Type1Id = Tuid.Guid("b") }
            });
            _accessOptionsbuilder.RestrictAccess<AnEntity>().By(AuthorizationType.Type1, x => x.Type1Id);
            _userAuthorizations[AuthorizationType.Type1].Add(Tuid.Guid("a"));

            Func<Guid, Task> loadEntity = async (id) =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    var controller = uow.Resolve<IEntityAccessController>();
                    await repository.LoadAsync<AnEntity>(x => x.Id, id);
                    await controller.CkeckEntitiesAccessForUserAsync();
                }
            };
            Func<Task> action1 = () => loadEntity(Tuid.Guid("1"));
            Func<Task> action2 = () => loadEntity(Tuid.Guid("2"));

            await action1.Should().NotThrowAsync<Exception>();
            await action2.Should().ThrowAsync<ForbiddenAccessException>();
        }

        [Fact]
        public async Task CheckUserAuthorizationsWhenCreateEntity()
        {
            _accessOptionsbuilder.RestrictAccess<AnEntity>().By(AuthorizationType.Type1, x => x.Type1Id);
            _userAuthorizations[AuthorizationType.Type1].Add(Tuid.Guid("a"));

            Func<Guid, Task> createEntity = async (typeId) =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    var controller = uow.Resolve<IEntityAccessController>();
                    var entity = new AnEntity { Id = Guid.NewGuid(), Type1Id = typeId };
                    await repository.InsertAsync(entity);
                    await controller.CkeckEntitiesAccessForUserAsync();
                }
            };
            Func<Task> action1 = () => createEntity(Tuid.Guid("a"));
            Func<Task> action2 = () => createEntity(Tuid.Guid("b"));

            await action1.Should().NotThrowAsync<Exception>();
            await action2.Should().ThrowAsync<ForbiddenAccessException>();
        }

        [Fact]
        public async Task CheckUserAuthorizationsWhenUpdateEntity_load()
        {
            await PersitentStore.InsertManyAsync(new[]
            {
                new AnEntity { Id = Tuid.Guid("1"), Type1Id = Tuid.Guid("a") },
                new AnEntity { Id = Tuid.Guid("2"), Type1Id = Tuid.Guid("c") }
            });
            _accessOptionsbuilder.RestrictAccess<AnEntity>().By(AuthorizationType.Type1, x => x.Type1Id);
            _userAuthorizations[AuthorizationType.Type1].AddRange(Tuid.Guid("a"), Tuid.Guid("b"));

            Func<Guid, Guid, Task> updateEntity = async (id, typeId) =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    var controller = uow.Resolve<IEntityAccessController>();
                    var entity = await repository.LoadAsync<AnEntity>(id);
                    entity.Type1Id = typeId;
                    await repository.UpdateAsync(entity);
                    await controller.CkeckEntitiesAccessForUserAsync();
                }
            };
            Func<Task> action1 = () => updateEntity(Tuid.Guid("1"), Tuid.Guid("b"));
            Func<Task> action2 = () => updateEntity(Tuid.Guid("1"), Tuid.Guid("c"));
            Func<Task> action3 = () => updateEntity(Tuid.Guid("2"), Tuid.Guid("a"));

            await action1.Should().NotThrowAsync<Exception>();
            await action2.Should().ThrowAsync<ForbiddenAccessException>();
            await action3.Should().ThrowAsync<ForbiddenAccessException>();
        }

        [Fact]
        public async Task CheckUserAuthorizationsWhenUpdateEntity_loadMany()
        {
            await PersitentStore.InsertManyAsync(new[]
            {
                new AnEntity { Id = Tuid.Guid("1"), Type1Id = Tuid.Guid("a") },
                new AnEntity { Id = Tuid.Guid("2"), Type1Id = Tuid.Guid("c") }
            });
            _accessOptionsbuilder.RestrictAccess<AnEntity>().By(AuthorizationType.Type1, x => x.Type1Id);
            _userAuthorizations[AuthorizationType.Type1].AddRange(Tuid.Guid("a"), Tuid.Guid("b"));

            Func<Guid, Guid, Task> updateEntity = async (id, typeId) =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    var controller = uow.Resolve<IEntityAccessController>();
                    var entities = await repository.LoadAsync<AnEntity>(new[] { id });
                    var entity = entities.First();
                    entity.Type1Id = typeId;
                    await repository.UpdateAsync(entity);
                    await controller.CkeckEntitiesAccessForUserAsync();
                }
            };
            Func<Task> action1 = () => updateEntity(Tuid.Guid("1"), Tuid.Guid("b"));
            Func<Task> action2 = () => updateEntity(Tuid.Guid("1"), Tuid.Guid("c"));
            Func<Task> action3 = () => updateEntity(Tuid.Guid("2"), Tuid.Guid("a"));

            await action1.Should().NotThrowAsync<Exception>();
            await action2.Should().ThrowAsync<ForbiddenAccessException>();
            await action3.Should().ThrowAsync<ForbiddenAccessException>();
        }

        [Fact]
        public async Task CheckUserAuthorizationsWhenUpdateEntity_loadUnique()
        {
            await PersitentStore.InsertManyAsync(new[]
            {
                new AnEntity { Id = Tuid.Guid("1"), Type1Id = Tuid.Guid("a") },
                new AnEntity { Id = Tuid.Guid("2"), Type1Id = Tuid.Guid("c") }
            });
            _accessOptionsbuilder.RestrictAccess<AnEntity>().By(AuthorizationType.Type1, x => x.Type1Id);
            _userAuthorizations[AuthorizationType.Type1].AddRange(Tuid.Guid("a"), Tuid.Guid("b"));

            Func<Guid, Guid, Task> updateEntity = async (id, typeId) =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    var controller = uow.Resolve<IEntityAccessController>();
                    var entity = await repository.LoadAsync<AnEntity>(x => x.Id, id);
                    entity.Type1Id = typeId;
                    await repository.UpdateAsync(entity);
                    await controller.CkeckEntitiesAccessForUserAsync();
                }
            };
            Func<Task> action1 = () => updateEntity(Tuid.Guid("1"), Tuid.Guid("b"));
            Func<Task> action2 = () => updateEntity(Tuid.Guid("1"), Tuid.Guid("c"));
            Func<Task> action3 = () => updateEntity(Tuid.Guid("2"), Tuid.Guid("a"));

            await action1.Should().NotThrowAsync<Exception>();
            await action2.Should().ThrowAsync<ForbiddenAccessException>();
            await action3.Should().ThrowAsync<ForbiddenAccessException>();
        }

        [Fact]
        public async Task CheckUserAuthorizationsWhenUpdateEntity_query()
        {
            await PersitentStore.InsertManyAsync(new[]
            {
                new AnEntity { Id = Tuid.Guid("1"), Type1Id = Tuid.Guid("a") },
                new AnEntity { Id = Tuid.Guid("2"), Type1Id = Tuid.Guid("c") }
            });
            _accessOptionsbuilder.RestrictAccess<AnEntity>().By(AuthorizationType.Type1, x => x.Type1Id);
            _userAuthorizations[AuthorizationType.Type1].AddRange(Tuid.Guid("a"), Tuid.Guid("b"));

            Func<Guid, Guid, Task> updateEntity = async (id, typeId) =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    var controller = uow.Resolve<IEntityAccessController>();
                    var entity = await repository.Query<AnEntity>().Where(x => x.Id == id).SingleAsync();
                    entity.Type1Id = typeId;
                    await repository.UpdateAsync(entity);
                    await controller.CkeckEntitiesAccessForUserAsync();
                }
            };
            Func<Task> action1 = () => updateEntity(Tuid.Guid("1"), Tuid.Guid("b"));
            Func<Task> action2 = () => updateEntity(Tuid.Guid("1"), Tuid.Guid("c"));

            await action1.Should().NotThrowAsync<Exception>();
            await action2.Should().ThrowAsync<ForbiddenAccessException>();
        }

        [Fact]
        public async Task CheckUserAuthorizationsOneStep()
        {
            await PersitentStore.InsertManyAsync(new[]
            {
                new Step1Entity { Id = Tuid.Guid("1"), Type1Id = Tuid.Guid("a") },
                new Step1Entity { Id = Tuid.Guid("2"), Type1Id = Tuid.Guid("b") }
            });
            _accessOptionsbuilder.RestrictAccess<Step0Entity>()
                .Through<Step1Entity>(AuthorizationType.Type1, (x, y) => x.Step1Id == y.Id);
            _accessOptionsbuilder.RestrictAccess<Step1Entity>()
                .By(AuthorizationType.Type1, x => x.Type1Id);

            _userAuthorizations[AuthorizationType.Type1].Add(Tuid.Guid("a"));

            Func<Guid?, Task> createEntity = async (step1Id) =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    var controller = uow.Resolve<IEntityAccessController>();
                    var step0Entity = new Step0Entity { Id = Guid.NewGuid(), Step1Id = step1Id };
                    await repository.InsertManyAsync(step0Entity);
                    await controller.CkeckEntitiesAccessForUserAsync();
                }
            };
            Func<Task> action1 = () => createEntity(Tuid.Guid("1"));
            Func<Task> action2 = () => createEntity(Tuid.Guid("2"));
            Func<Task> action3 = () => createEntity(null);

            await action1.Should().NotThrowAsync<Exception>();
            await action2.Should().ThrowAsync<ForbiddenAccessException>();
            await action3.Should().NotThrowAsync<Exception>();
        }

        [Fact]
        public async Task CheckUserAuthorizationTwoSteps()
        {
            await PersitentStore.InsertManyAsync(new IEntity[]
            {
                new Step2Entity { Id = Tuid.Guid("2a"), Type2Id = Tuid.Guid("a") },
                new Step2Entity { Id = Tuid.Guid("2b"), Type2Id = Tuid.Guid("b") },
                new Step1Entity { Id = Tuid.Guid("1a"), Step2Id = Tuid.Guid("2a") },
                new Step1Entity { Id = Tuid.Guid("1b"), Step2Id = Tuid.Guid("2b") },
            });
            _accessOptionsbuilder.RestrictAccess<Step0Entity>()
                .Through<Step1Entity>(AuthorizationType.Type2, (x, y) => x.Step1Id == y.Id);
            _accessOptionsbuilder.RestrictAccess<Step1Entity>()
                .Through<Step2Entity>(AuthorizationType.Type2, (x, y) => x.Step2Id == y.Id);
            _accessOptionsbuilder.RestrictAccess<Step2Entity>()
                .By(AuthorizationType.Type2, x => x.Type2Id);

            _userAuthorizations[AuthorizationType.Type2].Add(Tuid.Guid("a"));

            Func<Guid, Task> createEntity = async (step1Id) =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    var controller = uow.Resolve<IEntityAccessController>();
                    var step0Entity = new Step0Entity { Id = Guid.NewGuid(), Step1Id = step1Id };
                    await repository.InsertManyAsync(step0Entity);
                    await controller.CkeckEntitiesAccessForUserAsync();
                }
            };
            Func<Task> action1 = () => createEntity(Tuid.Guid("1a"));
            Func<Task> action2 = () => createEntity(Tuid.Guid("1b"));

            await action1.Should().NotThrowAsync<Exception>();
            await action2.Should().ThrowAsync<ForbiddenAccessException>();
        }

        [Fact]
        public async Task AllRulesMustBeRespectedToAuthorizeAccess()
        {
            await PersitentStore.InsertManyAsync(new[]
            {
                new Step1Entity { Id = Tuid.Guid("1"), Type1Id = Tuid.Guid("a") },
                new Step1Entity { Id = Tuid.Guid("2"), Type1Id = Tuid.Guid("b") }
            });
            _accessOptionsbuilder.RestrictAccess<Step0Entity>()
                .Through<Step1Entity>(AuthorizationType.Type1, (x, y) => x.Step1Id == y.Id)
                .By(AuthorizationType.Type2, x => x.Type2Id);
            _accessOptionsbuilder.RestrictAccess<Step1Entity>()
                .By(AuthorizationType.Type1, x => x.Type1Id);

            _userAuthorizations[AuthorizationType.Type1].Add(Tuid.Guid("a"));
            _userAuthorizations[AuthorizationType.Type2].Add(Tuid.Guid("x"));

            Func<Guid?, Guid, Task> createEntity = async (step1Id, type2Id) =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    var controller = uow.Resolve<IEntityAccessController>();
                    var step0Entity = new Step0Entity { Id = Guid.NewGuid(), Step1Id = step1Id, Type2Id = type2Id };
                    await repository.InsertManyAsync(step0Entity);
                    await controller.CkeckEntitiesAccessForUserAsync();
                }
            };
            Func<Task> action1 = () => createEntity(Tuid.Guid("1"), Tuid.Guid("x"));
            Func<Task> action2 = () => createEntity(Tuid.Guid("2"), Tuid.Guid("x"));
            Func<Task> action3 = () => createEntity(Tuid.Guid("1"), Tuid.Guid("y"));
            Func<Task> action4 = () => createEntity(Tuid.Guid("2"), Tuid.Guid("y"));

            await action1.Should().NotThrowAsync<Exception>();
            await action2.Should().ThrowAsync<ForbiddenAccessException>();
            await action3.Should().ThrowAsync<ForbiddenAccessException>();
            await action4.Should().ThrowAsync<ForbiddenAccessException>();
        }

        [Fact]
        public async Task CheckAuthorizationsWithStepAndAtLeastOne()
        {
            await PersitentStore.InsertManyAsync(new IEntity[]
            {
                new AnEntity { Id = Tuid.Guid("1") },
                new AnAssociationEntity { Type1Id = Tuid.Guid("a"), EntityId = Tuid.Guid("1") },
                new AnAssociationEntity { Type1Id = Tuid.Guid("b"), EntityId = Tuid.Guid("1") },
                new AnEntity { Id = Tuid.Guid("2") },
                new AnAssociationEntity { Type1Id = Tuid.Guid("b"), EntityId = Tuid.Guid("2") },
                new AnEntity { Id = Tuid.Guid("3") }
            });
            _accessOptionsbuilder.RestrictAccess<AnEntity>()
                .Through<AnAssociationEntity>(AuthorizationType.Type1, (x, y) => x.Id == y.EntityId, AuthorizationCardinality.AlLeastOne);
            _accessOptionsbuilder.RestrictAccess<AnAssociationEntity>()
                .By(AuthorizationType.Type1, x => x.Type1Id);

            _userAuthorizations[AuthorizationType.Type1].AddRange(Tuid.Guid("a"));

            Func<Guid, Task> loadEntity = async (id) =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    var controller = uow.Resolve<IEntityAccessController>();
                    await repository.LoadAsync<AnEntity>(id);
                    await controller.CkeckEntitiesAccessForUserAsync();
                }
            };
            Func<Task> action1 = () => loadEntity(Tuid.Guid("1"));
            Func<Task> action2 = () => loadEntity(Tuid.Guid("2"));
            Func<Task> action3 = () => loadEntity(Tuid.Guid("3"));

            await action1.Should().NotThrowAsync<Exception>();
            await action2.Should().ThrowAsync<ForbiddenAccessException>();
            await action3.Should().NotThrowAsync<Exception>();
        }

        [Fact]
        public void ConfigurationExceptionIfThroughRuleWithoutFinalRule()
        {
            _accessOptionsbuilder.RestrictAccess<Step0Entity>()
                .Through<Step1Entity>(AuthorizationType.Type2, (x, y) => x.Step1Id == y.Id);
            _accessOptionsbuilder.RestrictAccess<Step1Entity>()
                .Through<Step2Entity>(AuthorizationType.Type2, (x, y) => x.Step2Id == y.Id);
            
            EntityAccessOptions options;
            Action action = () => options = _accessOptionsbuilder.Options;

            action.Should().Throw<ConfigurationException>().Which.Message
                .Should().Contain("is missing")
                .And.Contain(nameof(Step1Entity))
                .And.Contain(nameof(Step2Entity))
                .And.Contain(nameof(AuthorizationType.Type2));
        }

        [Fact]
        public void ConfigurationExceptionIfTwoRulesForSameEntityTypeAndAuthorizationType()
        {
            _accessOptionsbuilder.RestrictAccess<Step0Entity>()
                .Through<Step1Entity>(AuthorizationType.Type2, (x, y) => x.Step1Id == y.Id)
                .By(AuthorizationType.Type2, x => x.Type2Id);

            EntityAccessOptions options;
            Action action = () => options = _accessOptionsbuilder.Options;

            action.Should().Throw<ConfigurationException>().Which.Message
                .Should().Contain("Duplicate")
                .And.Contain(nameof(Step0Entity))
                .And.Contain(nameof(AuthorizationType.Type2));
        }

        [Fact]
        public async Task CanDefinePolymorphicSteps()
        {
            await PersitentStore.InsertManyAsync(new IEntity[]
            {
                new PolymorphicEntity1 { Id = Tuid.Guid("1"), Type1Id = Tuid.Guid("a") },
                new PolymorphicEntity1 { Id = Tuid.Guid("2"), Type1Id = Tuid.Guid("b") },
                new PolymorphicEntity2 { Id = Tuid.Guid("3") },
            });
            _accessOptionsbuilder.RestrictAccess<AnotherEntity>()
                .Through<IPolymorphic>(AuthorizationType.Type1, (x, y) => x.PolymorphicId == y.Id);
            _accessOptionsbuilder.RestrictAccess<PolymorphicEntity1>()
                .By(AuthorizationType.Type1, x => x.Type1Id);

            _userAuthorizations[AuthorizationType.Type1].AddRange(Tuid.Guid("a"));

            Func<Guid, Task> createEntity = async (polymorphicId) =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    var controller = uow.Resolve<IEntityAccessController>();
                    var entity = new AnotherEntity { Id = Guid.NewGuid(), PolymorphicId = polymorphicId };
                    await repository.InsertAsync(entity);
                    await controller.CkeckEntitiesAccessForUserAsync();
                }
            };
            Func<Task> action1 = () => createEntity(Tuid.Guid("1"));
            Func<Task> action2 = () => createEntity(Tuid.Guid("2"));
            Func<Task> action3 = () => createEntity(Tuid.Guid("3"));

            await action1.Should().NotThrowAsync<Exception>();
            await action2.Should().ThrowAsync<ForbiddenAccessException>();
            await action3.Should().NotThrowAsync<Exception>();
        }

        [Fact]
        public async Task CanDefinePolymorphicStepsAndAtLeastOneCardinality()
        {
            await PersitentStore.InsertManyAsync(new IEntity[]
            {
                new AnEntity { Id = Tuid.Guid("1") },
                new PolymorphicAssociationEntity1 { EntityId = Tuid.Guid("1"), Type1Id = Tuid.Guid("a") },
                new PolymorphicAssociationEntity1 { EntityId = Tuid.Guid("1"), Type1Id = Tuid.Guid("b") },
                new PolymorphicAssociationEntity2 { EntityId = Tuid.Guid("1"), },
                new AnEntity { Id = Tuid.Guid("2") },
                new PolymorphicAssociationEntity1 { EntityId = Tuid.Guid("2"), Type1Id = Tuid.Guid("b") },
                new AnEntity { Id = Tuid.Guid("3") },
                new PolymorphicAssociationEntity2 { EntityId = Tuid.Guid("3"), },
                new AnEntity { Id = Tuid.Guid("4") },
            });
            _accessOptionsbuilder.RestrictAccess<AnEntity>()
                .Through<IPolymorphicAssociation>(AuthorizationType.Type1, (x, y) => x.Id == y.EntityId, AuthorizationCardinality.AlLeastOne);
            _accessOptionsbuilder.RestrictAccess<PolymorphicAssociationEntity1>()
                .By(AuthorizationType.Type1, x => x.Type1Id);

            _userAuthorizations[AuthorizationType.Type1].AddRange(Tuid.Guid("a"));

            Func<Guid, Task> loadEntity = async (id) =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    var controller = uow.Resolve<IEntityAccessController>();
                    await repository.LoadAsync<AnEntity>(id);
                    await controller.CkeckEntitiesAccessForUserAsync();
                }
            };
            Func<Task> action1 = () => loadEntity(Tuid.Guid("1"));
            Func<Task> action2 = () => loadEntity(Tuid.Guid("2"));
            Func<Task> action3 = () => loadEntity(Tuid.Guid("3"));
            Func<Task> action4 = () => loadEntity(Tuid.Guid("4"));

            await action1.Should().NotThrowAsync<Exception>();
            await action2.Should().ThrowAsync<ForbiddenAccessException>();
            await action3.Should().NotThrowAsync<Exception>();
            await action4.Should().NotThrowAsync<Exception>();
        }

        [Fact]
        public async Task CanDefineAuthorizationUsingInterface()
        {
            await PersitentStore.InsertManyAsync(new IEntity[]
            {
                new EntityImpl1 { Id = Tuid.Guid("1"), Type1Id = Tuid.Guid("a") },
                new EntityImpl1 { Id = Tuid.Guid("2"), Type1Id = Tuid.Guid("b") },
                new EntityImpl1 { Id = Tuid.Guid("3"), Type1Id = null },
                new EntityImpl2 { Id = Tuid.Guid("4"), Type1Id = Tuid.Guid("a") },
                new EntityImpl2 { Id = Tuid.Guid("5"), Type1Id = Tuid.Guid("b") },
                new EntityImpl2 { Id = Tuid.Guid("6"), Type1Id = null },
            });
            //TODO erreur de conf si règle définie pour interface et une des implémentation ?
            _accessOptionsbuilder.RestrictAccess<IAnInterface>()
                .By(AuthorizationType.Type1, x => x.Type1Id);

            _userAuthorizations[AuthorizationType.Type1].AddRange(Tuid.Guid("a"));

            Func<Guid, Task> loadEntity = async (id) =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    var controller = uow.Resolve<IEntityAccessController>();
                    await repository.LoadAsync<IAnInterface>(id);
                    await controller.CkeckEntitiesAccessForUserAsync();
                }
            };
            Func<Task> action1 = () => loadEntity(Tuid.Guid("1"));
            Func<Task> action2 = () => loadEntity(Tuid.Guid("2"));
            Func<Task> action3 = () => loadEntity(Tuid.Guid("3"));
            Func<Task> action4 = () => loadEntity(Tuid.Guid("4"));
            Func<Task> action5 = () => loadEntity(Tuid.Guid("5"));
            Func<Task> action6 = () => loadEntity(Tuid.Guid("6"));

            await action1.Should().NotThrowAsync<Exception>();
            await action2.Should().ThrowAsync<ForbiddenAccessException>();
            await action3.Should().ThrowAsync<ForbiddenAccessException>();
            await action4.Should().NotThrowAsync<Exception>();
            await action5.Should().ThrowAsync<ForbiddenAccessException>();
            await action6.Should().ThrowAsync<ForbiddenAccessException>();
        }

        [Fact]
        public async Task CanAllowNullOrEmptyValues()
        {
            await PersitentStore.InsertManyAsync(new IEntity[]
            {
                new EntityImpl1 { Id = Tuid.Guid("1"), Type1Id = Tuid.Guid("a") },
                new EntityImpl1 { Id = Tuid.Guid("2"), Type1Id = Tuid.Guid("b") },
                new EntityImpl1 { Id = Tuid.Guid("3"), Type1Id = Guid.Empty },
                new EntityImpl1 { Id = Tuid.Guid("4"), Type1Id = null },
            });
            _accessOptionsbuilder.RestrictAccess<EntityImpl1>()
                .By(AuthorizationType.Type1, x => x.Type1Id, allowNullOrEmpty: true);

            _userAuthorizations[AuthorizationType.Type1].AddRange(Tuid.Guid("a"));

            Func<Guid, Task> loadEntity = async (id) =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    var controller = uow.Resolve<IEntityAccessController>();
                    await repository.LoadAsync<IAnInterface>(id);
                    await controller.CkeckEntitiesAccessForUserAsync();
                }
            };
            Func<Task> action1 = () => loadEntity(Tuid.Guid("1"));
            Func<Task> action2 = () => loadEntity(Tuid.Guid("2"));
            Func<Task> action3 = () => loadEntity(Tuid.Guid("3"));
            Func<Task> action4 = () => loadEntity(Tuid.Guid("4"));

            await action1.Should().NotThrowAsync<Exception>();
            await action2.Should().ThrowAsync<ForbiddenAccessException>();
            await action3.Should().NotThrowAsync<Exception>();
            await action4.Should().NotThrowAsync<Exception>();
        }

        [Fact]
        public void ConfigurationExceptionIfAllowNullOrEmptyAndNoEmptyValueConfigured()
        {
            _accessOptionsbuilder.Options.EmptyValues.Remove(typeof(Guid));

            _accessOptionsbuilder.RestrictAccess<AnEntity>()
                .By(AuthorizationType.Type1, x => x.Type1Id, allowNullOrEmpty: true);

            EntityAccessOptions options;
            Action action = () => options = _accessOptionsbuilder.Options;

            action.Should().Throw<ConfigurationException>().Which.Message
                .Should().Contain("empty")
                .And.Contain(nameof(Guid))
                .And.Contain(nameof(AnEntity))
                .And.Contain(nameof(AuthorizationType.Type1));
        }

        [Fact]
        public async Task UseTypeHierarchyToFindMatchingRule()
        {
            await PersitentStore.InsertManyAsync(new IEntity[]
            {
                new EntityImpl1 { Id = Tuid.Guid("1"), Type1Id = Tuid.Guid("a") },
                new EntityImpl1 { Id = Tuid.Guid("2"), Type1Id = Tuid.Guid("b") },
            });
            _accessOptionsbuilder.RestrictAccess<AnAbstractClass>()
                .By(AuthorizationType.Type1, x => x.Type1Id);

            _userAuthorizations[AuthorizationType.Type1].AddRange(Tuid.Guid("a"));

            Func<Guid, Task> loadEntity = async (id) =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    var controller = uow.Resolve<IEntityAccessController>();
                    await repository.LoadAsync<IAnInterface>(id);
                    await controller.CkeckEntitiesAccessForUserAsync();
                }
            };
            Func<Task> action1 = () => loadEntity(Tuid.Guid("1"));
            Func<Task> action2 = () => loadEntity(Tuid.Guid("2"));

            await action1.Should().NotThrowAsync<Exception>();
            await action2.Should().ThrowAsync<ForbiddenAccessException>();
        }

        public enum AuthorizationType
        {
            Type1,
            Type2
        }

        public class AnEntity : IEntity
        {
            public Guid Id { get; set; }
            public Guid Type1Id { get; set; }
            public Guid Type2Id { get; set; }
            public Guid? NullableType1Id { get; set; }
            public string ConcatenedIds { get; set; }
            public object UnsupportedMemberType { get; set; }
            public string Name { get; set; }
        }

        public class AnAssociationEntity : IEntity
        {
            public Guid Id { get; set; }
            public Guid EntityId { get; set; }
            public Guid Type1Id { get; set; }
        }

        public class Step0Entity : IEntity
        {
            public Guid Id { get; set; }
            public Guid? Step1Id { get; set; }
            public Guid Type2Id { get; set; }
        }

        public class Step1Entity : IEntity
        {
            public Guid Id { get; set; }
            public Guid Type1Id { get; set; }
            public Guid Step2Id { get; set; }
        }

        public class Step2Entity : IEntity
        {
            public Guid Id { get; set; }
            public Guid Type2Id { get; set; }
        }

        public class AnotherEntity : IEntity
        {
            public Guid Id { get; set; }
            public Guid PolymorphicId { get; set; }
            public Guid EntityId { get; set; }
        }

        public interface IPolymorphic : IEntity
        {
        }

        public class PolymorphicEntity1 : IPolymorphic
        {
            public Guid Id { get; set; }
            public Guid Type1Id { get; set; }
        }

        public class PolymorphicEntity2 : IPolymorphic
        {
            public Guid Id { get; set; }
        }

        public interface IPolymorphicAssociation : IEntity
        {
            public Guid EntityId { get; set; }
        }

        public class PolymorphicAssociationEntity1 : IPolymorphicAssociation
        {
            public Guid Id { get; set; }
            public Guid EntityId { get; set; }
            public Guid Type1Id { get; set; }
        }

        public class PolymorphicAssociationEntity2 : IPolymorphicAssociation
        {
            public Guid Id { get; set; }
            public Guid EntityId { get; set; }
        }

        public interface IAnInterface : IEntity
        {
            public Guid? Type1Id { get; set; }
        }

        public abstract class AnAbstractClass : IAnInterface
        {
            public Guid Id { get; set; }
            public abstract Guid? Type1Id { get; set; }
        }

        public class EntityImpl1 : AnAbstractClass
        {
            public override Guid? Type1Id { get; set; }
        }

        public class EntityImpl2 : IAnInterface
        {
            public Guid Id { get; set; }
            public Guid? Type1Id { get; set; }
        }

        public class MyRepository : DecorableRepository, IEntityAccessRepository, IRepository
        {
            public MyRepository(IBaseRepository baseRepository, IUnitOfWork unitOfWork, IEnumerable<Type> decoratorTypes)
                : base(baseRepository, unitOfWork, decoratorTypes)
            {
            }
        }

        public class MyEntityAccessController : EntityAccessControllerBase, IEntityAccessController
        {
            private IndexedSets<Enum, object> _userAuthorizations;

            public MyEntityAccessController(IEntityQueryFilterController queryFilterController, 
                IEntityReadWriteAccessController readWriteAccessController, IStateStore stateStore, IndexedSets<Enum, object> userAuthorizations) 
                : base(queryFilterController, readWriteAccessController, stateStore)
            {
                _userAuthorizations = userAuthorizations;
            }

            protected override IndexedSets<Enum, object> GetUserAuthorizations()
            {
                return _userAuthorizations;
            }
        }
    }
}
