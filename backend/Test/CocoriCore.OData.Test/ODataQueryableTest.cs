using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CocoriCore.OData.Linq;
using FluentAssertions;
using Xunit;
using CocoriCore.Common;
using CocoriCore.Repository;

namespace CocoriCore.OData.Test
{
    public class ODataQueryableTest
    {
        [Theory]
        [InlineData("Name")]
        [InlineData("name")]
        public void OrderByPropertyAsc(string propertyName)
        {
            var collection = new ODataEntity[]
            {
                new ODataEntity { Name = "b" },
                new ODataEntity { Name = "a" },
                new ODataEntity { Name = "c" },
            };
            var queryable = collection.AsQueryable();

            var order = new ODataOrderBy();
            order.Direction = OrderDirection.Asc;
            order.Member = propertyName;

            queryable.OrderBy(order).Should().BeInAscendingOrder(x => x.Name);
        }

        [Theory]
        [InlineData("Name")]
        [InlineData("name")]
        public void OrderByPropertyDesc(string propertyName)
        {
            var collection = new ODataEntity[]
            {
                new ODataEntity { Name = "b" },
                new ODataEntity { Name = "a" },
                new ODataEntity { Name = "c" },
            };
            var queryable = collection.AsQueryable();

            var order = new ODataOrderBy();
            order.Direction = OrderDirection.Desc;
            order.Member = propertyName;

            queryable.OrderBy(order).Should().BeInDescendingOrder(x => x.Name);
        }

        [Fact]
        public void OrderByIntField()
        {
            var collection = new ODataEntity[]
            {
                new ODataEntity { Price = 45 },
                new ODataEntity { Price = 23 },
                new ODataEntity { Price = 64 },
            };
            var queryable = collection.AsQueryable();

            var order = new ODataOrderBy();
            order.Direction = OrderDirection.Asc;
            order.Member = "price";

            queryable.OrderBy(order).Should().BeInAscendingOrder(x => x.Price);
        }

        [Fact]
        public void OrderByBoolField()
        {
            var collection = new ODataEntity[]
            {
                new ODataEntity { Archived = true },
                new ODataEntity { Archived = false },
                new ODataEntity { Archived = true },
            };
            var queryable = collection.AsQueryable();

            var order = new ODataOrderBy();
            order.Direction = OrderDirection.Asc;
            order.Member = "archived";

            queryable.OrderBy(order).Should().BeInAscendingOrder(x => x.Archived);
        }

        [Fact]
        public void OrderByDateTimeField()
        {
            var collection = new ODataEntity[]
            {
                new ODataEntity { Date = new DateTime(2018, 12, 1) },
                new ODataEntity { Date = new DateTime(2018, 11, 30) },
                new ODataEntity { Date = new DateTime(2018, 12, 2) },
            };
            var queryable = collection.AsQueryable();

            var order = new ODataOrderBy();
            order.Direction = OrderDirection.Asc;
            order.Member = "date";

            queryable.OrderBy(order).Should().BeInAscendingOrder(x => x.Date);
        }

        [Fact]
        public void ThrowExceptionIfNoMemberFound()
        {
            var collection = new ODataEntity[0];
            var queryable = collection.AsQueryable();

            var order = new ODataOrderBy();
            order.Direction = OrderDirection.Desc;
            order.Member = "toto";
            Action action = () => queryable.OrderBy(order).ToList();

            action.Should().Throw<MissingMemberException>()
                .WithMessage($"No property neither field named 'toto' found for type {typeof(ODataEntity)}");
        }

        [Fact]
        public void DoNothingIfOrderIsNull()
        {
            var collection = new ODataEntity[]
            {
                new ODataEntity { Name = "b" },
                new ODataEntity { Name = "a" },
                new ODataEntity { Name = "c" },
            };
            var queryable = collection.AsQueryable();

            queryable.OrderBy((ODataOrderBy)null).Select(x => x.Name).Should().ContainInOrder("b", "a", "c");
        }

        [Fact]
        public async Task FilterEqOnOneProperty()
        {
            var collection = new ODataEntity[]
            {
                new ODataEntity { Name = "bertrand" },
                new ODataEntity { Name = "antho" },
                new ODataEntity { Name = "luc" },
            };
            var queryable = new CollectionAsyncQueryProvider<ODataEntity>(collection);

            var query = new ODataQuery
            {
                Filter = $"{nameof(ODataEntity.Name)} eq 'antho' or "
                    + $"{nameof(ODataEntity.Name)} eq 'bertrand' or " 
                    + $"{nameof(ODataEntity.Name)} eq 'c'"
            };

            var results = await queryable
                .ToODataRequest(query)
                .GetResultAsync();
            results.Results.Select(x => x.Name).Should().BeEquivalentTo("antho", "bertrand");
        }

        [Theory]
        [InlineData("begaar")]
        [InlineData("BégaAr")]
        public async Task FilterEqReplaceField(string search)
        {
            var collection = new ODataEntity[]
            {
                new ODataEntity { Name = "Bégaar", _Name = "begaar"},
                new ODataEntity { Name = "Babar", _Name = "babar"},
            };
            var queryable = new CollectionAsyncQueryProvider<ODataEntity>(collection);

            var query = new ODataQuery
            {
                Filter = $"{nameof(ODataEntity.Name)} eq '{search}'"
            };

            var results = await queryable
                .ToODataRequest(query)
                .Configure(x => x.Filter
                    .TransformValues(v => v.RemoveDiacritics().ToLower())
                    .ReplaceField(c => c.Name, c => c._Name))
                .GetResultAsync();
            results.Results.Select(x => x.Name).Should().ContainSingle().Which.Should().Be("Bégaar");
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public async Task FilterEqReplaceFieldWhenEmptySearch(string search)
        {
            var collection = new ODataEntity[]
            {
                new ODataEntity { Name = "Bégaar", _Name = "begaar"},
                new ODataEntity { Name = "Babar", _Name = "babar"},
            };
            var queryable = new CollectionAsyncQueryProvider<ODataEntity>(collection);

            var query = new ODataQuery
            {
                Filter = search
            };

            var results = await queryable
                .ToODataRequest(query)
                .Configure(x => x.Filter
                    .TransformValues(v => v.RemoveDiacritics().ToLower())
                    .ReplaceField(c => c.Name, c => c._Name))
                .GetResultAsync();
            results.Results.Select(x => x.Name).Should().BeEquivalentTo("Bégaar", "Babar");
        }

        [Fact]
        public async Task FilterEqOnOneIntField()
        {
            var collection = new ODataEntity[]
            {
                new ODataEntity { Price = 12 },
                new ODataEntity { Price = 19 },
                new ODataEntity { Price = 25 },
            };
            var queryable = new CollectionAsyncQueryProvider<ODataEntity>(collection);

            var query = new ODataQuery
            {
                Filter = $"{nameof(ODataEntity.Price)} eq 12 or {nameof(ODataEntity.Price)} eq 25"
            };

            var results = await queryable
                .ToODataRequest(query)
                .GetResultAsync();

            results.Results.Select(x => x.Price).Should().BeEquivalentTo(new[] { 12, 25 });
        }

        [Fact]
        public async Task FilterEqOnOneBoolField()
        {
            var collection = new ODataEntity[]
            {
                new ODataEntity { Name = "a", Archived = true },
                new ODataEntity { Name = "b", Archived = false },
                new ODataEntity { Name = "c", Archived = true },
            };
            var queryable = new CollectionAsyncQueryProvider<ODataEntity>(collection);

            var query = new ODataQuery
            {
                Filter = $"{nameof(ODataEntity.Archived)} eq true"
            };

            var results = await queryable
                .ToODataRequest(query)
                .GetResultAsync();

            results.Results.Select(x => x.Name).Should().BeEquivalentTo("a", "c");
        }

        [Fact]
        public async Task FilterEqOnOneGuidField()
        {
            var id1 = Guid.NewGuid();
            var id2 = Guid.NewGuid();
            var collection = new ODataEntity[]
            {
                new ODataEntity { Name = "a", AnId = id1 },
                new ODataEntity { Name = "b", AnId = id2 },
                new ODataEntity { Name = "c", AnId = id1 },
            };
            var queryable = new CollectionAsyncQueryProvider<ODataEntity>(collection);

            var query = new ODataQuery
            {
                Filter = $"{nameof(ODataEntity.AnId)} eq {id1.ToString()}"
            };

            var results = await queryable
                .ToODataRequest(query)
                .GetResultAsync();

            results.Results.Select(x => x.Name).Should().BeEquivalentTo("a", "c");
        }

        [Fact]
        public async Task FilterEqOnManyField()
        {
            var collection = new ODataEntity[]
            {
                new ODataEntity { Name = "bertrand", Code = "33520"},
                new ODataEntity { Name = "antho"   , Code = "16600"},
                new ODataEntity { Name = "luc"     , Code = "40250"},
            };
            var queryable = new CollectionAsyncQueryProvider<ODataEntity>(collection);

            var query = new ODataQuery
            {
                Filter = $"({nameof(ODataEntity.Name)} eq 'antho' or "
                    + $"{nameof(ODataEntity.Name)} eq 'luc') and "
                    + $"({nameof(ODataEntity.Code)} eq '40250' or "
                    + $"{nameof(ODataEntity.Code)} eq '33520')"
            };

            var results = await queryable
                .ToODataRequest(query)
                .GetResultAsync();
            results.Results.Should().ContainSingle().Which.Name.Should().Be("luc");
        }

        [Fact]
        public async Task FilterContainsOnManyField()
        {
            var collection = new ODataEntity[]
            {
                new ODataEntity { Name = "bertrand", Code = "33520"},
                new ODataEntity { Name = "antho"   , Code = "16600"},
                new ODataEntity { Name = "luc"     , Code = "40250"},
            };
            var queryable = new CollectionAsyncQueryProvider<ODataEntity>(collection);

            var query = new ODataQuery
            {
                Filter = $"contains({nameof(ODataEntity.Name)}, 'an') eq true and "
                    + $"(contains({nameof(ODataEntity.Code)},'5') eq true)"
            };

            var results = await queryable
                .ToODataRequest(query)
                .GetResultAsync();
            results.Results.Should().ContainSingle().Which.Name.Should().Be("bertrand");
        }

        [Theory]
        [InlineData("beg")]
        [InlineData("Bég")]
        public async Task FilterContainsReplaceField(string search)
        {
            var collection = new ODataEntity[]
            {
                new ODataEntity { Name = "Bégaar", _Name = "begaar"},
                new ODataEntity { Name = "Babar", _Name = "babar"},
            };
            var queryable = new CollectionAsyncQueryProvider<ODataEntity>(collection);

            var query = new ODataQuery
            {
                Filter = $"contains({nameof(ODataEntity.Name)},'{search}') eq true"
            };
            var results = await queryable
                .ToODataRequest(query)
                .Configure(x => x.Filter
                    .TransformValues(v => v.RemoveDiacritics().ToLower())
                    .ReplaceField(c => c.Name, c => c._Name))
                .GetResultAsync();

            results.Results.Select(x => x.Name).Should().ContainSingle().Which.Should().Be("Bégaar");
        }

        [Fact]
        public void SelectAllPropertiesIfNullFieldResponse()
        {
            var collection = new ODataEntity[]
            {
                new ODataEntity { Name = "bertrand", Code = "A", Other = "o"},
                new ODataEntity { Name = "antho"   , Code = "B", Other = "o"},
            };
            var queryable = collection.AsQueryable();

            var result = queryable.Select<ODataEntity, FieldResponse>();

            result.ElementAt(0).Name.Should().Be("bertrand");
            result.ElementAt(0).Code.Should().Be("A");
            result.ElementAt(0).AnId.Should().BeEmpty();
            result.ElementAt(1).Name.Should().Be("antho");
            result.ElementAt(1).Code.Should().Be("B");
            result.ElementAt(1).AnId.Should().BeEmpty();
        }

        [Fact]
        public void SelectAllPropertiesIfNullPropertyResponse()
        {
            var collection = new ODataEntity[]
            {
                new ODataEntity { Name = "bertrand", Code = "A", Other = "o"},
                new ODataEntity { Name = "antho"   , Code = "B", Other = "o"},
            };
            var queryable = collection.AsQueryable();

            var result = queryable.Select<ODataEntity, PropertyResponse>();

            result.ElementAt(0).Name.Should().Be("bertrand");
            result.ElementAt(0).Code.Should().Be("A");
            result.ElementAt(0).AnId.Should().BeEmpty();
            result.ElementAt(1).Name.Should().Be("antho");
            result.ElementAt(1).Code.Should().Be("B");
            result.ElementAt(1).AnId.Should().BeEmpty();
        }

        [Fact]
        public void SelectFieldPropertyResponse()
        {
            var collection = new ODataEntity[]
            {
                new ODataEntity { Name = "bertrand", Code = "A", Other = "o" },
                new ODataEntity { Name = "antho"   , Code = "B", Other = "o" },
            };
            var queryable = collection.AsQueryable();

            var select = new ODataSelect("name");
            var result = queryable.Select<ODataEntity, PropertyResponse>(select);

            result.ElementAt(0).Name.Should().Be("bertrand");
            result.ElementAt(0).Code.Should().BeNull();
            result.ElementAt(1).Name.Should().Be("antho");
            result.ElementAt(1).Code.Should().BeNull();
        }

        [Fact]
        public void SelectPropertiesFieldResponse()
        {
            var collection = new ODataEntity[]
            {
                new ODataEntity { Name = "bertrand", Code = "A", Other = "o"},
                new ODataEntity { Name = "antho"   , Code = "B", Other = "o"},
            };
            var queryable = collection.AsQueryable();

            var select = new ODataSelect("name");
            var result = queryable.Select<ODataEntity, FieldResponse>(select);

            result.ElementAt(0).Name.Should().Be("bertrand");
            result.ElementAt(0).Code.Should().BeNull();
            result.ElementAt(1).Name.Should().Be("antho");
            result.ElementAt(1).Code.Should().BeNull();
        }

        [Fact]
        public void SelectPropertyWithEntityTypeAsResponse()
        {
            var collection = new ODataEntity[]
            {
                new ODataEntity { Name = "bertrand", Code = "A", Other = "c"},
                new ODataEntity { Name = "antho"   , Code = "B", Other = "d"},
            };
            var queryable = collection.AsQueryable();

            var select = new ODataSelect("name");
            var result = queryable.Select(select);

            result.ElementAt(0).Name.Should().Be("bertrand");
            result.ElementAt(0).Code.Should().BeNull();
            result.ElementAt(0).Other.Should().BeNull();
            result.ElementAt(1).Name.Should().Be("antho");
            result.ElementAt(1).Code.Should().BeNull();
            result.ElementAt(1).Other.Should().BeNull();
        }

        [Fact]
        public void SelectAllPropertiesWithEntityTypeAsResponse()
        {
            var collection = new ODataEntity[]
            {
                new ODataEntity { Name = "bertrand", Code = "A", Other = "c"},
                new ODataEntity { Name = "antho"   , Code = "B", Other = "d"},
            };
            var queryable = collection.AsQueryable();

            ODataSelect select = null;
            var result = queryable.Select(select);

            result.ElementAt(0).Name.Should().Be("bertrand");
            result.ElementAt(0).Code.Should().Be("A");
            result.ElementAt(0).Other.Should().Be("c");
            result.ElementAt(1).Name.Should().Be("antho");
            result.ElementAt(1).Code.Should().Be("B");
            result.ElementAt(1).Other.Should().Be("d");
        }

        [Fact]
        public void SelectAllPropertiesWhenEmptySelect()
        {
            var collection = new ODataEntity[]
            {
                new ODataEntity { Name = "bertrand", Code = "A", Other = "c"},
                new ODataEntity { Name = "antho"   , Code = "B", Other = "d"},
            };
            var queryable = collection.AsQueryable();

            var select = new ODataSelect();
            var result = queryable.Select(select);

            result.ElementAt(0).Name.Should().Be("bertrand");
            result.ElementAt(0).Code.Should().Be("A");
            result.ElementAt(0).Other.Should().Be("c");
            result.ElementAt(1).Name.Should().Be("antho");
            result.ElementAt(1).Code.Should().Be("B");
            result.ElementAt(1).Other.Should().Be("d");
        }

        [Fact]
        public void SelectNotNullableToNullable()
        {
            Guid id1 = Guid.NewGuid();
            Guid id2 = Guid.NewGuid();
            var collection = new ODataEntity[]
            {
                new ODataEntity { Name = "bertrand", Code = "A", AnId = id1 },
                new ODataEntity { Name = "antho"   , Code = "B", AnId = id2 },
            };
            var queryable = collection.AsQueryable();

            var select = new ODataSelect("name", "anid");
            var result = queryable.Select<ODataEntity, PropertyResponse>(select);

            result.ElementAt(0).Name.Should().Be("bertrand");
            result.ElementAt(0).Code.Should().BeNull();
            result.ElementAt(0).AnId.Should().Be(id1);
            result.ElementAt(1).Name.Should().Be("antho");
            result.ElementAt(1).Code.Should().BeNull();
            result.ElementAt(1).AnId.Should().Be(id2);
        }

        [Fact]
        public void ApplyDistinctOnSelectedFields()
        {
            var collection = new ODataEntity[]
            {
                new ODataEntity { Name = "bertrand", AnId = Guid.NewGuid() },
                new ODataEntity { Name = "antho"   , AnId = Guid.NewGuid() },
                new ODataEntity { Name = "antho"   , AnId = Guid.NewGuid() },
            };
            var queryable = new DistinctEnumerable<ODataEntity>(collection.AsQueryable());

            var select = new ODataSelect("name");
            var result = queryable.Select<ODataEntity, FieldResponse>(select).Distinct(true);

            result.Select(x => x.Name).Should().BeEquivalentTo("bertrand", "antho");
        }

        [Fact]
        public async Task CanForceDistinctWhenSelectOneField()
        {
            var collection = new ODataEntity[]
            {
                new ODataEntity { Name = "bertrand", AnId = Guid.NewGuid() },
                new ODataEntity { Name = "antho"   , AnId = Guid.NewGuid() },
                new ODataEntity { Name = "antho"   , AnId = Guid.NewGuid() },
            };
            var queryable = new CollectionAsyncQueryProvider<ODataEntity>(collection);

            var query = new ODataQuery
            {
                Select = new ODataSelect("name")
            };
            var options = new ODataOptions
            {
                ForceDistinctWhenSelectOneField = true
            };
            var result = await queryable.ToODataRequest(query, options).GetResultAsync();

            result.Count.Should().Be(2);
            result.Results.Select(x => x.Name).Should().BeEquivalentTo("bertrand", "antho");
        }

        [Fact]
        //On SQLServer, if top equals 0 then we get this error :
        //The number of rows provided for a FETCH clause must be greater then zero.
        public async Task Top100WhenZero()
        {
            var collection = new List<ODataEntity>();
            for(var i=0 ; i<110; i++)
            {
                collection.Add(new ODataEntity());
            }

            var result = await new CollectionAsyncQueryProvider<ODataEntity>(collection)
                .ToODataRequest(new ODataQuery { Top = 0 })
                .GetResultAsync();

            result.Count.Should().Be(110);
            result.Results.Length.Should().Be(100);
        }

        [Fact]
        public async Task ConfigureValueTransformationBeforeExecuteFilter()
        {
            var collection = new ODataEntity[]
            {
                new ODataEntity { AnId = Guid.NewGuid(), Name = "bertrand" },
                new ODataEntity { AnId = Guid.NewGuid(), Name = "antho"    },
                new ODataEntity { AnId = Guid.NewGuid(), Name = "antho"    },
            };
            var queryable = new CollectionAsyncQueryProvider<ODataEntity>(collection);

            var query = new ODataQuery
            {
                Filter = $"{nameof(ODataEntity.Name)} eq 'anthô'"
            };
            var result = await queryable
                .ToODataRequest(query)
                .Configure(x => x.Filter.TransformValues(v => v.RemoveDiacritics()))
                .GetResultAsync();

            result.Results.Select(x => x.AnId).Should().BeEquivalentTo(new[] { collection[1].AnId, collection[2].AnId });
        }
    }

    public class ODataEntity
    {
        public string Name { get; set; }
        public string _Name { get; set; }
        public string Code { get; set; }
        public string Other { get; set; }
        public Guid AnId { get; set; }
        public int Price { get; set; }
        public bool Archived { get; set; }
        public DateTime Date { get; set; }
    }

    public class PropertyResponse
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public Guid? AnId { get; set; }
    }

    public class FieldResponse
    {
        public string Name;
        public string Code;
        public Guid? AnId;
    }
}