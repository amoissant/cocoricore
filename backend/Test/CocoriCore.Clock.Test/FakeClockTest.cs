﻿using CocoriCore.Clock;
using CocoriCore.TestUtils;
using FluentAssertions;
using System;
using Xunit;
using CocoriCore.Common;
using System.Threading.Tasks;

namespace CocoriCore.Jwt.Test
{
    public class FakeClockTest
    {
        [Fact]
        public void NowIsSystemNowAndRoundedToMillisecond()
        {
            var clock = new FakeClock();

            clock.Now.Should().BeCloseTo(DateTime.Now, TimeSpan.FromMilliseconds(500));
            var now = clock.Now;
            now.Should().Be(now.RoundMilliseconds());
        }

        [Fact]
        public void TodayIsSystemNowDate()
        {
            var clock = new FakeClock();

            clock.Today.Should().Be(DateTime.Now.Date);
        }

        [Fact]
        public void UtcNowIsSystemUtcNowAndRoundedToMillisecond()
        {
            var clock = new FakeClock();

            clock.UtcNow.Should().BeCloseTo(DateTime.UtcNow, TimeSpan.FromMilliseconds(500));
            var utcNow = clock.UtcNow;
            utcNow.Should().Be(utcNow.RoundMilliseconds());
        }

        [Fact]
        public void TodayIsSystemUtcNowDate()
        {
            var clock = new FakeClock();

            clock.UtcToday.Should().Be(DateTime.UtcNow.Date);
        }

        [Fact]
        public async Task StopTimeThenContinue()
        {
            var clock = new FakeClock();

            clock.StopTime();

            var now1 = clock.Now;
            await Task.Delay(1000);
            var now2 = clock.Now;
            now1.Should().Be(now2);

            clock.ContinueTime();
            await Task.Delay(1000);

            var now3 = clock.Now;
            now3.Should().BeAfter(now2);
        }

        [Fact]
        public async Task StopTimeAtThenContinue()
        {
            var clock = new FakeClock();

            clock.StopTimeAt(new DateTime(2021, 6, 1, 17, 49, 34));

            clock.Now.Should().Be(new DateTime(2021, 6, 1, 17, 49, 34));
            await Task.Delay(1000);
            clock.Now.Should().Be(new DateTime(2021, 6, 1, 17, 49, 34));

            clock.ContinueTime();
            await Task.Delay(1000);

            clock.Now.Should().BeAfter(new DateTime(2021, 6, 1, 17, 49, 35));
        }

        [Fact]
        public void SetNow()
        {
            var clock = new FakeClock();

            clock.Now = new DateTime(2021, 6, 1, 17, 49, 34);

            clock.Now.Should().BeCloseTo(new DateTime(2021, 6, 1, 17, 49, 34), TimeSpan.FromMilliseconds(500));
        }

        [Fact]
        public void SetUtcNow()
        {
            var clock = new FakeClock();

            clock.UtcNow = new DateTime(2021, 6, 1, 17, 49, 34);

            clock.UtcNow.Should().BeCloseTo(new DateTime(2021, 6, 1, 17, 49, 34).ToUniversalTime(), TimeSpan.FromMilliseconds(500));
        }

        [Fact]
        public void SuccessiveCallToNowReturnDifferentValues()
        {
            var clock = new FakeClock();

            clock.Now.Should().BeBefore(clock.Now);
            clock.Now.RoundMilliseconds().Should().BeBefore(clock.Now.RoundMilliseconds());

            clock.UtcNow.Should().BeBefore(clock.UtcNow);
            clock.UtcNow.RoundMilliseconds().Should().BeBefore(clock.UtcNow.RoundMilliseconds());
        }
    }
}