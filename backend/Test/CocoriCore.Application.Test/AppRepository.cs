﻿using CocoriCore.Common;
using CocoriCore.Repository;
using System;
using System.Collections.Generic;

namespace CocoriCore.Application.Test
{
    public class AppRepository : DecorableRepository, IRepository
    {
        public AppRepository(IBaseRepository baseRepository, IUnitOfWork unitOfWork, IEnumerable<Type> decoratorTypes) 
            : base(baseRepository, unitOfWork, decoratorTypes)
        {
        }
    }
}
