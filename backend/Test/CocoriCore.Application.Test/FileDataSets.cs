using System;
using System.Threading.Tasks;

namespace CocoriCore.Application.Test
{
    public static class FileDataSets
    {
        public static async Task<File> CreateFileAsync(this DataSets _,
            params Action<CreateFileCommand>[] modifications)
        {
            var command = new CreateFileCommand().Default(modifications);
            return await _.CreateFileAsync(command);
        }

        public static async Task<File> CreateFileAsync(this DataSets _, CreateFileCommand command)
        {
            var id = (Guid)await _.ExecuteAsync(command);
            return await _.Repository.LoadAsync<File>(id);
        }
    }
}
