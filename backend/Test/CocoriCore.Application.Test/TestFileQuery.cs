﻿using System.Threading.Tasks;
using Xunit;
using CocoriCore.TestUtils;
using CocoriCore.Common;

namespace CocoriCore.Application.Test
{
    public class TestFileQuery : TestWithDataSets<DataSets, DependencyInjection>
    {
        [Fact]
        public async Task GetFile()
        {
            var file = await _.CreateFileAsync();

            var query = new GetFileQuery() { Id = file.Id };
            var response = (FileResponse)await _.ExecuteAsync(query);

            await file.ShouldBeEquivalentToAsync(response);
        }
    }
}
