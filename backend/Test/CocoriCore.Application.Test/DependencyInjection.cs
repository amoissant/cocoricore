using Autofac;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.Loader;
using CocoriCore.TestUtils;
using CocoriCore.Autofac;
using CocoriCore.FileSystem;
using CocoriCore.Common;
using CocoriCore.Repository;
using Microsoft.Extensions.Caching.Memory;
using CocoriCore.Clock;
using CocoriCore.Messaging;
using AutoMapper;

namespace CocoriCore.Application.Test
{
    //TODO : factoriser code pour l'injection de dépendances
    public class DependencyInjection : IDependencyInjection
    {
        protected ILifetimeScope _rootScope;
        protected ContainerBuilder _builder;
        private UnitOfWorkOptionsBuilder _unitOfWorkBuilder;

        public ILifetimeScope RootScope
        {
            get
            {
                if (_rootScope == null)
                    _rootScope = _builder.Build();
                return _rootScope;
            }
        }

        public ContainerBuilder Builder => _builder;

        public DependencyInjection()//TODO faire du ménage ici pour ne garder que ce qui est necessaire
        {
            _builder = new ContainerBuilder();
            _unitOfWorkBuilder = new UnitOfWorkOptionsBuilder();

            var AssemblyContext = AssemblyLoadContext.Default;
            var applicationAssembly = AssemblyContext.LoadFromAssemblyName(typeof(CocoriCore.Application.AssemblyInfo).Assembly.GetName());
            var domainAssembly = AssemblyContext.LoadFromAssemblyName(typeof(CocoriCore.Application.AssemblyInfo).Assembly.GetName());

            _builder.RegisterAssemblyTypes(applicationAssembly)
                    .AssignableTo<IHandler>()
                    .AsSelf();

            _builder.RegisterAssemblyTypes(domainAssembly)
                    .AssignableTo<IEntity>()
                    .AsSelf();

            _builder.RegisterType<InMemoryEntityStore>().As<IInMemoryEntityStore>().SingleInstance();
            _builder.RegisterType<TransactionalInMemoryBaseRepository>()
                .AsSelf()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
            _builder.RegisterType<StateStore>().As<IStateStore>().InstancePerLifetimeScope();
            _builder.RegisterType<Utf8JsonCopier>().As<ICopier>().InstancePerLifetimeScope();

            _builder.RegisterType<AppRepository>()
                .AsImplementedInterfaces()
                .WithParameter(new TypedParameter<IEnumerable<Type>>(new[] { typeof(DataSetDecorator) }))
                .InstancePerLifetimeScope();
            _builder.RegisterType<DataSets>().As<TestUtils.IDataSet>().SingleInstance();
            _builder.RegisterType<DataSetDecorator>().AsSelf().SingleInstance();
            _builder.RegisterType<Factory>().As<IFactory>().SingleInstance();

            var formats = new Dictionary<Type, string>();
            _builder.RegisterType<JsonSerializer>().As<JsonSerializer>().SingleInstance()
                .WithParameter(new TypedParameter(typeof(Dictionary<Type, string>), formats));
            _builder.RegisterType<Formats>().AsSelf().SingleInstance()
                .WithParameter(new TypedParameter(typeof(Dictionary<Type, string>), formats));

            var autoMapperConfiguration = new MapperConfiguration(x => x.AddMaps(applicationAssembly));
            _builder.RegisterAssemblyTypes(applicationAssembly)
               .AssignableTo<Profile>()
               .AsSelf();

            _builder.Register(c => autoMapperConfiguration.CreateMapper(c.Resolve<IComponentContext>().Resolve))
                .As<IMapper>()
                .InstancePerLifetimeScope();

            _builder.RegisterType<InMemoryFileSystem>().As<IFileSystem>().SingleInstance();

            _builder.Register<Func<ILifetimeScope>>(c => _rootScope.BeginLifetimeScope).AsSelf();
            _builder.Register(c => _unitOfWorkBuilder.Options).As<UnitOfWorkOptions>().SingleInstance();
            _builder.RegisterType<AutofacUnitOfWorkFactory>().As<IUnitOfWorkFactory>().SingleInstance();
            _builder.RegisterType<AutofacUnitOfWork>()
                .As<IUnitOfWork>()
                .InstancePerLifetimeScope();

            _builder.Register<Func<Type, object>>(c => _rootScope.Resolve).AsSelf().SingleInstance();

            var messageBusOptionsBuilder = new MessageBusOptionsBuilder();
            messageBusOptionsBuilder
                .AddRule("")
                .When(c => true)
                .Call<IHandlerExecutor>(async (x, c) => c.Response = await x.ExecuteAsync(c.Message))
                .Call<ITransactionHolder>((t, c) => t.CommitAsync());

            _builder.Register(c => messageBusOptionsBuilder.Build());
            _builder.RegisterType<MessageBus>().As<IMessageBus>().SingleInstance();
            _builder.RegisterType<HandlerExecutor>().As<IHandlerExecutor>()
                .WithParameter(new NamedParameter("assemblies", new[] { CocoriCore.Application.AssemblyInfo.Assembly }));

            _builder.RegisterType<SystemClock>().As<IClock>().SingleInstance();
            _builder.RegisterInstance(new MemoryCache(new MemoryCacheOptions())).As<IMemoryCache>();

            _builder.ConfigureXunitLogger();
        }

        public void Dispose()
        {
        }
    }
}
