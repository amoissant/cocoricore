﻿using CocoriCore.Common;
using FluentAssertions;
using System.Threading.Tasks;

namespace CocoriCore.Application.Test
{
    public static class FileAssertions
    {
        public static async Task ShouldBeEquivalentToAsync(this File file, CreateFileCommand command)
        {
            var _ = file.GetDataSets();
            var base64FileContent = await _.FileSystem.ReadAsBase64Async(file.GetPath());
            file.FileName.ToString().Should().Be(command.FileName);
            base64FileContent.Should().Be(command.Base64Content);
            file.MimeType.Should().Be(command.MimeType);
            file.FolderPath.Should().Be(command.FolderPath);
        }

        public static async Task ShouldBeEquivalentToAsync(this File file, FileResponse response)
        {
            var _ = file.GetDataSets();
            var binaryContent = await _.FileSystem.ReadAsBinaryAsync(file.GetPath());
            file.FileName.ToString().Should().Be(response.FileName);
            binaryContent.Should().BeEquivalentTo(response.Content);
            file.MimeType.ToString().Should().Be(response.MimeType);
        }
    }
}
