﻿using CocoriCore.Common;
using CocoriCore.FileSystem;
using CocoriCore.Repository;
using CocoriCore.TestUtils;

namespace CocoriCore.Application.Test
{
    public class DataSets : DataSetBase
    {
        public DataSets(IUnitOfWorkFactory unitOfWorkFactory)
            : base(unitOfWorkFactory)
        {
        }

        public IRepository Repository => Get<IRepository>();

        public IFileSystem FileSystem => Get<IFileSystem>();
    }
}
