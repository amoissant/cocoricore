﻿using System.Reflection;

namespace CocoriCore.Application.Test
{
    public static class AssemblyInfo
    {
        public static Assembly Assembly => typeof(AssemblyInfo).Assembly;
    }
}
