﻿using CocoriCore.Common;
using System.Text;
using System.Threading.Tasks;

namespace CocoriCore.Resource
{
    public class DictionaryResourceProvider
    {
        public Task<System.IO.Stream> GetStreamAsync(ResourceDictionary resourceDictionary, Path path)
        {
            string resource = null;
            if (TryFind(resourceDictionary, path, out resource))
            {
                var textContent = resource;
                System.IO.Stream stream = new System.IO.MemoryStream(Encoding.UTF8.GetBytes(textContent));
                return Task.FromResult(stream);
            }
            else
            {
                throw new ConfigurationException($"No resource found for path '{path}'.");
            }
        }

        public bool TryFind(ResourceDictionary dictionary, Path path, out string resource)
        {
            if (!path.IsEmpty() && dictionary.Contains(path.Segments[0]))
            {
                var found = dictionary[path.Segments[0]];
                if (path.Segments.Length == 1)
                {
                    resource = (string)found;
                    return true;
                }
                else if (found is ResourceDictionary subDictionary)
                {
                    return TryFind(subDictionary, path.RemoveFirstSegment(), out resource);
                }
            }
            resource = null;
            return false;
        }
    }
}
