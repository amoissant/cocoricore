﻿using System;
using System.Threading.Tasks;

namespace CocoriCore.Resource
{
    public interface IExceptionFactory
    {
        Task<Exception> CreateAsync(object errorCode, dynamic variables = null, string fieldName = null);
        Task<Exception> CreateAsync(object errorCode, Exception inner, dynamic variables = null, string fieldName = null);
    }
}