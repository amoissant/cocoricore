using CocoriCore.Common;
using System;
using System.Threading.Tasks;

namespace CocoriCore.Resource
{
    public class DictionaryResourceProviderRuleBuilder<TKey>
    {
        private ResourceProviderRule _rule;
        private ResourceDictionary _resourceDictionary;

        public DictionaryResourceProviderRuleBuilder(ResourceProviderRule rule, ResourceDictionary resourceDictionary)
        {
            _rule = rule;
            _resourceDictionary = resourceDictionary;
        }

        public void GetPathUsing(Func<TKey, Path> resolvePath)
        {
            _rule.GetResourceAsync = async (u, k) =>
            {
                var provider = u.Resolve<DictionaryResourceProvider>();
                var path = resolvePath((TKey)k);
                return await provider.GetStreamAsync(_resourceDictionary, path);
            };
        }

        public void GetPathUsing<TService>(Func<TService, TKey, Task<Path>> resolvePathAsync)
        {
            _rule.GetResourceAsync = async (u, k) =>
            {
                var provider = u.Resolve<DictionaryResourceProvider>();
                var pathService = u.Resolve<TService>();
                var path = await resolvePathAsync(pathService, (TKey)k);
                return await provider.GetStreamAsync(_resourceDictionary, path);
            };
        }
    }
}