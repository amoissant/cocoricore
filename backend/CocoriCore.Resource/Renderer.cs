﻿using CocoriCore.Dynamic.Extension;
using System.Threading.Tasks;

namespace CocoriCore.Resource
{
    public class Renderer : IRenderer
    {
        private IResourceProvider _resourceProvider;

        public Renderer(IResourceProvider resourceProvider)
        {
            _resourceProvider = resourceProvider;
        }

        public async Task<string> RenderAsync(object templateKey, object variables = null)
        {
            var template = await _resourceProvider.ReadAsTextAsync(templateKey);
            return ReplaceVariablesIntoTemplate(variables, template);
        }

        private string ReplaceVariablesIntoTemplate(object variables, string template)
        {
            if (variables == null) return template;

            foreach (var kvp in variables.ToExpando())
            {
                var variableKey = kvp.Key;
                var variableValue = kvp.Value;
                template = template.Replace($"{{{{{variableKey}}}}}", $"{variableValue}");
            }
            return template;
        }
    }
}
