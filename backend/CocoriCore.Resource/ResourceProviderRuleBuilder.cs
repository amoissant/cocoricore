using System.Reflection;

namespace CocoriCore.Resource
{
    public class ResourceProviderRuleBuilder<TKey>
    {
        private ResourceProviderRule _rule;

        public ResourceProviderRuleBuilder(ResourceProviderRule rule)
        {
            _rule = rule;
            _rule.ResourceKeyType = typeof(TKey);
        }

        public DictionaryResourceProviderRuleBuilder<TKey> UseDictionaryResourceProvider(ResourceDictionary resourceDictionary)
        {
            return new DictionaryResourceProviderRuleBuilder<TKey>(_rule, resourceDictionary);
        }

        public EmbeddedResourceProviderRuleBuilder<TKey> UseEmbeddedResourceProvider(Assembly resourceAssembly)
        {
            return new EmbeddedResourceProviderRuleBuilder<TKey>(_rule, resourceAssembly);
        }
    }
}