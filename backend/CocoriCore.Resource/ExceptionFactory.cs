﻿using CocoriCore.Common;
using System;
using System.Threading.Tasks;

namespace CocoriCore.Resource
{
    public class ExceptionFactory : IExceptionFactory
    {
        private IResourceProvider _resourceProvider;
        private IRenderer _renderer;

        public ExceptionFactory(IResourceProvider resourceProvider, IRenderer renderer)
        {
            _resourceProvider = resourceProvider;
            _renderer = renderer;
        }

        public async Task<Exception> CreateAsync(object errorCode, dynamic variables = null, string fieldName = null)
        {
            var message = await _renderer.RenderAsync(errorCode, variables);
            return new ExceptionWithCode(errorCode, message, fieldName);
        }

        public async Task<Exception> CreateAsync(object errorCode, Exception inner, dynamic variables = null, string fieldName = null)
        {
            var message = await _renderer.RenderAsync(errorCode, variables);
            return new ExceptionWithCode(errorCode, message, inner, fieldName);
        }
    }
}