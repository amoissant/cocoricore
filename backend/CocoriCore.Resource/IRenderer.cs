﻿using System.Threading.Tasks;

namespace CocoriCore.Resource
{
    public interface IRenderer
    {
        Task<string> RenderAsync(object templateKey, dynamic variables = null);
    }

}
