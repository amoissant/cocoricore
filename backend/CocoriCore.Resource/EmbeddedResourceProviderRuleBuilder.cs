using CocoriCore.Common;
using System;
using System.Reflection;

namespace CocoriCore.Resource
{
    public class EmbeddedResourceProviderRuleBuilder<TKey>
    {
        private ResourceProviderRule _rule;
        private Assembly _resourceAssembly;

        public EmbeddedResourceProviderRuleBuilder(ResourceProviderRule rule, Assembly resourceAssembly)
        {
            _rule = rule;
            _resourceAssembly = resourceAssembly;
        }

        public void GetPathUsing(Func<TKey, Path> resolvePath)
        {
            _rule.GetResourceAsync = (u, k) =>
            {
                var provider = u.Resolve<EmbeddedResourceProvider>();
                var path = resolvePath((TKey)k);
                return provider.GetStreamAsync(_resourceAssembly, path);
            };
        }

    }
}