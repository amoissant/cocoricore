﻿using CocoriCore.Common;
using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace CocoriCore.Resource
{
    public class EmbeddedResourceProvider
    {
        public Task<System.IO.Stream> GetStreamAsync(Assembly resourceAssembly, Path path)
        {
            var embeddedResourcePath = $"{resourceAssembly.GetName().Name}.{string.Join(".", path.Segments)}";
            return GetStreamAsync(resourceAssembly, embeddedResourcePath);
        }

        public Task<System.IO.Stream> GetStreamAsync(Assembly resourceAssembly, string path)
        {
            if (resourceAssembly.GetManifestResourceNames().Any(x => x == path))
            {
                var stream = resourceAssembly.GetManifestResourceStream(path);
                return Task.FromResult(stream);
            }
            else
            {
                throw new InvalidOperationException($"Embedded resource '{path}' not found."
                    + "Ensure the path is correct and file is configured as embebded resource.");
            }
        }
    }
}
