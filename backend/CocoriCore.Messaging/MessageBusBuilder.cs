﻿using CocoriCore.Common;
using System;
using System.Threading.Tasks;

namespace CocoriCore.Messaging
{
    public class MessageBusBuilder
    {
        public MessageBusOptions Options { get; }
        public MessageRule Rule { get; }

        public MessageBusBuilder(MessageBusOptions options, MessageRule rule)
        {
            Options = options;
            Rule = rule;
        }

        /// <summary>
        /// Add an action to execute when rule is applied. 
        /// </summary>
        /// <remarks>
        /// <para>Actions are executed in order of their definition</para>
        /// <para>A rule can have zero or many actions</para>
        /// </remarks>
        /// <param name="action">Action to execute</param>
        /// <returns><see cref="MessageBusBuilder"/></returns>
        public MessageBusBuilder Call(Action<MessageContext> action)
        {
            Rule.CallActions.Add(MethodCall.FromFunction(action));
            Rule.StringRepresentation += $".{nameof(Call)}()";
            return this;
        }

        /// <summary>
        /// Add an action to execute when rule is applied.
        /// </summary>
        /// <remarks>
        /// <para>Actions are executed in order of their definition</para>
        /// <para>A rule can have zero or many actions</para>
        /// </remarks>
        /// <param name="action">Action to execute</param>
        /// <returns><see cref="MessageBusBuilder"/></returns>
        public MessageBusBuilder Call(Func<MessageContext, Task> action)
        {
            Rule.CallActions.Add(MethodCall.FromFunction(action));
            Rule.StringRepresentation += $".{nameof(Call)}()";
            return this;
        }

        /// <summary>
        /// Add an action to execute when rule is applied
        /// </summary>
        /// <remarks>
        /// <para>Actions are executed in order of their definition</para>
        /// <para>A rule can have zero or many actions</para>
        /// </remarks>
        /// <param name="action">Action to execute on the <typeparamref name="TService"/></param>
        /// <typeparam name="TService">Type of the service to resolve to execute action</typeparam>
        /// <returns><see cref="MessageBusBuilder"/></returns>
        public MessageBusBuilder Call<TService>(Action<TService, MessageContext> action)
        {
            Rule.CallActions.Add(MethodCall.FromFunction(action));
            Rule.StringRepresentation += $".{nameof(Call)}<{typeof(TService).Name}>()";
            return this;
        }

        /// <summary>
        /// Add an action to execute when rule is applied.
        /// </summary>
        /// <remarks>
        /// <para>Actions are executed in order of their definition</para>
        /// <para>A rule can have zero or many actions</para>
        /// </remarks>
        /// <param name="action">Action to execute on the <typeparamref name="TService"/></param>
        /// <typeparam name="TService">Type of the service to resolve to execute action</typeparam>
        /// <returns><see cref="MessageBusBuilder"/></returns>
        public MessageBusBuilder Call<TService>(Func<TService, MessageContext, Task> action)
        {
            Rule.CallActions.Add(MethodCall.FromFunction(action));
            Rule.StringRepresentation += $".{nameof(Call)}<{typeof(TService).Name}>()";
            return this;
        }
    }
}
  