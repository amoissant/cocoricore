using CocoriCore.Common;
using System.Collections.Generic;
using System.Linq;

namespace CocoriCore.Messaging
{
    /// <summary>
    /// Options class for <see cref="MessageBus"/>.
    /// Use <see cref="MessageBusOptionsBuilder"/> to build this object.
    /// </summary>
    public class MessageBusOptions : Dictionary<string, object>
    {
        public IReadOnlyList<MessageRule> Rules { get; }
        public IReadOnlyList<IMethodCall<MessageContext, object>> BeforeAllActions { get; }
        public bool ThrowExceptionWhenNoRuleMatch { get; }

        public MessageBusOptions(IEnumerable<IMethodCall<MessageContext, object>> beforeActions, 
            IEnumerable<MessageRule> rules, 
            bool throwExceptionWhenNoRuleMatch)
        {
            BeforeAllActions = beforeActions.ToList().AsReadOnly();
            Rules = rules.ToList().AsReadOnly();
            ThrowExceptionWhenNoRuleMatch = throwExceptionWhenNoRuleMatch;
        }
    }
}