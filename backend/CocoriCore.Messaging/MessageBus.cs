﻿using System;
using System.Linq;
using System.Threading.Tasks;
using CocoriCore.Common;
using Microsoft.Extensions.Logging;
using CocoriCore.Types.Extension;
using System.Diagnostics;
using CocoriCore.Common.Diagnostic;

namespace CocoriCore.Messaging
{
    public class MessageBus : IMessageBus
    {
        private readonly MessageBusOptions _options;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger _logger;

        public MessageBus(MessageBusOptions options, IUnitOfWork unitOfWork, ILogger logger)
        {
            _options = options;
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        public async Task<object> ExecuteAsync(object message, string ruleName = null)
        {
            var context = new MessageContext(message);
            await ExecuteAsync(context, ruleName);
            return context.Response;
        }
        
        public async Task<MessageContext> ExecuteAsync(MessageContext context, string ruleName = null)
        {
            context = context ?? throw new ArgumentNullException(nameof(context));//TODO TU
            var sw = Stopwatch.StartNew();
            await RunBeforeActionsAsync(context);
            var rule = await GetMatchingRuleOrDefaultAsync(context, ruleName);
            if (rule != null)
            {
                try
                {
                    await RunCallActionsAsync(rule, context);
                }
                catch (Exception exception)
                {
                    AddExceptionToContext(context, exception);
                    await RunErrorActionsAsync(rule, context);
                    throw;
                }
                finally
                {
                    await RunFinallyActionsAsync(rule, context);
                    sw.Stop();
                    LogMessageProcessingInfos(context, rule.Name, sw);
                }
            }
            return context;
        }

        private async Task RunBeforeActionsAsync(MessageContext context)
        {
            foreach (var action in _options.BeforeAllActions)
            {
                await action.ExecuteAsync(_unitOfWork, context);
            }
        }

        private async Task<MessageRule> GetMatchingRuleOrDefaultAsync(MessageContext context, string ruleName)
        {
            if (ruleName != null)
            {
                var rule = GetFirstRuleMatchingNameOrDefault(ruleName);
                if (rule == null)
                {
                    throw BuildRuleNameNotFoundException(ruleName);
                }
                return rule;
            }
            else
            {
                var rule = await GetFirstRuleMatchingConditionOrDefaultAsync(context);
                if (rule == null && _options.ThrowExceptionWhenNoRuleMatch)
                {
                    throw BuildNoMatchinfRuleException(context);
                }
                return rule;
            }
        }

        private MessageRule GetFirstRuleMatchingNameOrDefault(string ruleName)
        {
            return _options.Rules.FirstOrDefault(x => string.Equals(x.Name, ruleName, StringComparison.InvariantCultureIgnoreCase));
        }


        private Exception BuildRuleNameNotFoundException(string ruleName)
        {
            var ruleNames = string.Join(", ", _options.Rules.Select(x => x.Name));
            return new MessageBusException(
                $"No rule found for name '{ruleName}', ensure this name match a rule into your configuration.\n" +
                $"Available rules names : [{ruleNames}]");
        }

        private async Task<MessageRule> GetFirstRuleMatchingConditionOrDefaultAsync(MessageContext context)
        {
            foreach (var rule in _options.Rules)
            {
                if (rule.Condition != null && await rule.Condition.ExecuteAsync(_unitOfWork, context) == true)
                {
                    return rule;
                }
            }
            return null;
        }

        private Exception BuildNoMatchinfRuleException(MessageContext context)
        {
            Func<MessageRule, string> formatRule = r => $"{r.Name}{(r.Condition == null ? " (only by explicit call)" : string.Empty)}";
            var ruleNames = string.Join(", ", _options.Rules.Select(x => formatRule(x)));
            var messageType = context.Message?.GetPrettyType(full: true);
            return new MessageBusException(
                $"No rule matches condition for message of type {messageType}, ensure that one rule with condition will match.\n" +
                $"Rules without condition must be called explicitly using .{nameof(ExecuteAsync)}(message, <ruleName>).\n" +
                $"You can disable error when not rule match using builder.{nameof(MessageBusOptionsBuilder.ThrowExceptionWhenNoRuleMatch)} = false.\n" +
                $"Available rules: \n{ruleNames}\n");
        }

        private async Task RunCallActionsAsync(MessageRule rule, MessageContext context)
        {
            foreach (var action in rule.CallActions)
            {
                await action.ExecuteAsync(_unitOfWork, context);
            }
        }

        private void AddExceptionToContext(MessageContext context, Exception exception)
        {
            var messageType = context.Message?.GetPrettyType(full: true);
            context.Exception = exception;
            context.DebugDatas = new DebugDatas { { "MessageType", messageType } };
            exception.Data[nameof(MessageBus)] = context.DebugDatas;
        }

        private async Task RunErrorActionsAsync(MessageRule rule, MessageContext context)
        {
            foreach (var action in rule.ErrorActions)
            {
                await action.ExecuteAsync(_unitOfWork, context);
            }
        }

        private async Task RunFinallyActionsAsync(MessageRule rule, MessageContext context)
        {
            foreach (var action in rule.FinallyActions)
            {
                await action.ExecuteAsync(_unitOfWork, context);
            }
        }

        private void LogMessageProcessingInfos(MessageContext context, string ruleName, Stopwatch sw)
        {
            var messageType = context.Message?.GetPrettyType();
            if (context.Exception == null)
            {
                _logger.LogInformation("[{0}] {1} processed with success in {2}ms", ruleName, messageType, sw.ElapsedMilliseconds);
            }
            else
            {
                _logger.LogError("[{0}] {1} processed with error in {2}ms", ruleName, messageType, sw.ElapsedMilliseconds);
            }
        }
    }
}