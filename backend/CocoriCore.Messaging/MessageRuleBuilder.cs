﻿using CocoriCore.Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CocoriCore.Messaging
{
    public class MessageRuleBuilder
    {
        public MessageRule Rule { get; }

        public MessageRuleBuilder(MessageRule rule)
        {
            Rule = rule;
        }

        /// <summary>
        /// Define rule's condition, if condition return <c>true</c> then rule is applied otherwise it won't.
        /// </summary>
        /// <param name="condition">Lambda returning <c>true</c> when rule must apply</param>
        /// <returns><see cref="MessageRuleBuilder"/></returns>
        public MessageRuleBuilder When(Func<MessageContext, bool> condition)
        {
            Rule.Condition = MethodCall.FromFunction(condition);
            Rule.StringRepresentation += $".{nameof(When)}()";
            return new MessageRuleBuilder(Rule);
        }

        /// <summary>
        /// Define rule's condition, if condition return <c>true</c> then rule is applied otherwise it won't.
        /// </summary>
        /// <param name="condition">Async lambda returning <c>true</c> when rule must apply</param>
        /// <returns><see cref="MessageRuleBuilder"/></returns>
        public MessageRuleBuilder When(Func<MessageContext, Task<bool>> condition)
        {
            Rule.Condition = MethodCall.FromFunction(condition);
            Rule.StringRepresentation += $".{nameof(When)}()";
            return new MessageRuleBuilder(Rule);
        }

        /// <summary>
        /// Define rule's condition, if condition return <c>true</c> then rule is applied otherwise it won't.
        /// </summary>
        /// <param name="condition">Lambda using <typeparamref name="TService"/> and returning <c>true</c> when rule must apply</param>
        /// <typeparam name="TService">Service to resolve in order to execute <paramref name="condition"/></typeparam>
        /// <returns><see cref="MessageRuleBuilder"/></returns>
        public MessageRuleBuilder When<TService>(Func<TService, MessageContext, bool> condition) where TService : class
        {
            Rule.Condition = MethodCall.FromFunction(condition);
            Rule.StringRepresentation += $".{nameof(When)}<{typeof(TService).Name}>()";
            return new MessageRuleBuilder(Rule);
        }

        /// <summary>
        /// Define rule's condition, if condition return <c>true</c> then rule is applied otherwise it won't.
        /// </summary>
        /// <param name="condition">Async lambda using <typeparamref name="TService"/> and returning <c>true</c> when rule must apply</param>
        /// <typeparam name="TService">Service to resolve in order to execute <paramref name="condition"/></typeparam>
        /// <returns><see cref="MessageRuleBuilder"/></returns>
        public MessageRuleBuilder When<TService>(Func<TService, MessageContext, Task<bool>> condition) where TService : class
        {
            Rule.Condition = MethodCall.FromFunction(condition);
            Rule.StringRepresentation += $".{nameof(When)}<{typeof(TService).Name}>()";
            return new MessageRuleBuilder(Rule);
        }

        /// <summary>
        /// Add an action to execute when rule is applied. 
        /// </summary>
        /// <remarks>
        /// <para>Actions are executed in order of their definition</para>
        /// <para>A rule can have zero or many actions</para>
        /// </remarks>
        /// <param name="action">Action to execute</param>
        /// <returns><see cref="MessageRuleBuilder"/></returns>
        public MessageRuleBuilder Call(Action<MessageContext> action)
        {
            Rule.CallActions.Add(MethodCall.FromFunction(action));
            Rule.StringRepresentation += $".{nameof(Call)}()";
            return this;
        }

        /// <summary>
        /// Add an action to execute when rule is applied.
        /// </summary>
        /// <remarks>
        /// <para>Actions are executed in order of their definition</para>
        /// <para>A rule can have zero or many actions</para>
        /// </remarks>
        /// <param name="action">Action to execute</param>
        /// <returns><see cref="MessageRuleBuilder"/></returns>
        public MessageRuleBuilder Call(Func<MessageContext, Task> action)
        {
            Rule.CallActions.Add(MethodCall.FromFunction(action));
            Rule.StringRepresentation += $".{nameof(Call)}()";
            return this;
        }

        /// <summary>
        /// Add an action to execute when rule is applied
        /// </summary>
        /// <remarks>
        /// <para>Actions are executed in order of their definition</para>
        /// <para>A rule can have zero or many actions</para>
        /// </remarks>
        /// <param name="action">Action to execute on the <typeparamref name="TService"/></param>
        /// <typeparam name="TService">Type of the service to resolve to execute action</typeparam>
        /// <returns><see cref="MessageRuleBuilder"/></returns>
        public MessageRuleBuilder Call<TService>(Action<TService, MessageContext> action)
        {
            Rule.CallActions.Add(MethodCall.FromFunction(action));
            Rule.StringRepresentation += $".{nameof(Call)}<{typeof(TService).Name}>()";
            return this;
        }

        /// <summary>
        /// Add an action to execute when rule is applied.
        /// </summary>
        /// <remarks>
        /// <para>Actions are executed in order of their definition</para>
        /// <para>A rule can have zero or many actions</para>
        /// </remarks>
        /// <param name="action">Action to execute on the <typeparamref name="TService"/></param>
        /// <typeparam name="TService">Type of the service to resolve to execute action</typeparam>
        /// <returns><see cref="MessageRuleBuilder"/></returns>
        public MessageRuleBuilder Call<TService>(Func<TService, MessageContext, Task> action)
        {
            Rule.CallActions.Add(MethodCall.FromFunction(action));
            Rule.StringRepresentation += $".{nameof(Call)}<{typeof(TService).Name}>()";
            return this;
        }

        /// <summary>
        /// Add an action to execute on exception when rule is applied. 
        /// </summary>
        /// <remarks>
        /// <para>Actions are executed in order of their definition</para>
        /// <para>A rule can have zero or many error actions</para>
        /// </remarks>
        /// <param name="action">Action to execute on exception</param>
        /// <returns><see cref="MessageRuleBuilder"/></returns>
        public MessageRuleBuilder OnError(Action<MessageContext> action)
        {
            Rule.ErrorActions.Add(MethodCall.FromFunction(action));
            Rule.StringRepresentation += $".{nameof(OnError)}()";
            return this;
        }

        /// <summary>
        /// Add an action to execute on exception when rule is applied.
        /// </summary>
        /// <remarks>
        /// <para>Actions are executed in order of their definition</para>
        /// <para>A rule can have zero or many error actions</para>
        /// </remarks>
        /// <param name="action">Action to execute on exception</param>
        /// <returns><see cref="MessageRuleBuilder"/></returns>
        public MessageRuleBuilder OnError(Func<MessageContext, Task> action)
        {
            Rule.ErrorActions.Add(MethodCall.FromFunction(action));
            Rule.StringRepresentation += $".{nameof(OnError)}()";
            return this;
        }

        /// <summary>
        /// Add an action to execute on exception when rule is applied
        /// </summary>
        /// <remarks>
        /// <para>Actions are executed in order of their definition</para>
        /// <para>A rule can have zero or many error actions</para>
        /// </remarks>
        /// <param name="action">Action to execute on exception using <typeparamref name="TService"/></param>
        /// <typeparam name="TService">Type of the service to resolve to execute action</typeparam>
        /// <returns><see cref="MessageRuleBuilder"/></returns>
        public MessageRuleBuilder OnError<TService>(Action<TService, MessageContext> action)
        {
            Rule.ErrorActions.Add(MethodCall.FromFunction(action));
            Rule.StringRepresentation += $".{nameof(OnError)}<{typeof(TService).Name}>()";
            return this;
        }

        /// <summary>
        /// Add an action to execute on exception when rule is applied.
        /// </summary>
        /// <remarks>
        /// <para>Actions are executed in order of their definition</para>
        /// <para>A rule can have zero or many error actions</para>
        /// </remarks>
        /// <param name="action">Action to execute on exception using <typeparamref name="TService"/></param>
        /// <typeparam name="TService">Type of the service to resolve to execute action</typeparam>
        /// <returns><see cref="MessageRuleBuilder"/></returns>
        public MessageRuleBuilder OnError<TService>(Func<TService, MessageContext, Task> action)
        {
            Rule.ErrorActions.Add(MethodCall.FromFunction(action));
            Rule.StringRepresentation += $".{nameof(OnError)}<{typeof(TService).Name}>()";
            return this;
        }

        /// <summary>
        /// Add an action to execute after rule is applied whatever an exception occured or not. 
        /// </summary>
        /// <remarks>
        /// <para>Actions are executed in order of their definition</para>
        /// <para>A rule can have zero or many finally actions</para>
        /// </remarks>
        /// <param name="action">Action to execute whatever an exception occured or not</param>
        /// <returns><see cref="MessageRuleBuilder"/></returns>
        public MessageRuleBuilder Finally(Action<MessageContext> action)
        {
            Rule.FinallyActions.Add(MethodCall.FromFunction(action));
            Rule.StringRepresentation += $".{nameof(Finally)}()";
            return this;
        }

        /// <summary>
        /// Add an action to execute after rule is applied whatever an exception occured or not. 
        /// </summary>
        /// <remarks>
        /// <para>Actions are executed in order of their definition</para>
        /// <para>A rule can have zero or many finally actions</para>
        /// </remarks>
        /// <param name="action">Action to execute whatever an exception occured or not</param>
        /// <returns><see cref="MessageRuleBuilder"/></returns>
        public MessageRuleBuilder Finally(Func<MessageContext, Task> action)
        {
            Rule.FinallyActions.Add(MethodCall.FromFunction(action));
            Rule.StringRepresentation += $".{nameof(Finally)}()";
            return this;
        }

        /// <summary>
        /// Add an action to execute after rule is applied whatever an exception occured or not. 
        /// </summary>
        /// <remarks>
        /// <para>Actions are executed in order of their definition</para>
        /// <para>A rule can have zero or many finally actions</para>
        /// </remarks>
        /// <param name="action">Action to execute whatever an exception occured or not using <typeparamref name="TService"/></param>
        /// <typeparam name="TService">Type of the service to resolve to execute action</typeparam>
        /// <returns><see cref="MessageRuleBuilder"/></returns>
        public MessageRuleBuilder Finally<TService>(Action<TService, MessageContext> action)
        {
            Rule.FinallyActions.Add(MethodCall.FromFunction(action));
            Rule.StringRepresentation += $".{nameof(Finally)}<{typeof(TService).Name}>()";
            return this;
        }

        /// <summary>
        /// Add an action to execute after rule is applied whatever an exception occured or not. 
        /// </summary>
        /// <remarks>
        /// <para>Actions are executed in order of their definition</para>
        /// <para>A rule can have zero or many finally actions</para>
        /// </remarks>
        /// <param name="action">Action to execute whatever an exception occured or not using <typeparamref name="TService"/></param>
        /// <typeparam name="TService">Type of the service to resolve to execute action</typeparam>
        /// <returns><see cref="MessageRuleBuilder"/></returns>
        public MessageRuleBuilder Finally<TService>(Func<TService, MessageContext, Task> action)
        {
            Rule.FinallyActions.Add(MethodCall.FromFunction(action));
            Rule.StringRepresentation += $".{nameof(Finally)}<{typeof(TService).Name}>()";
            return this;
        }

        public MessageRuleBuilder CallForEach(Func<BatchRuleBuilder, BatchRuleBuilder> batchSteps)
        {
            var batchRule = new BatchRule();
            var batchRuleBuilder = new BatchRuleBuilder(batchRule);
            batchSteps(batchRuleBuilder);
            var methodCall = MethodCall.FromExpression<IBatchHandler, MessageContext>((x, c) => x.ExecuteAsync(batchRule, c));
            Rule.CallActions.Add(methodCall);
            return this;
        }
    }
}
  