using CocoriCore.Collection.Extensions;
using CocoriCore.Common;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CocoriCore.Messaging
{
    /// <summary>
    /// Fluent builder for <see cref="MessageBusOptions"/> used by <see cref="MessageBus"/>.
    /// </summary>
    public class MessageBusOptionsBuilder
    {
        public string StringRepresentation { get; set; }
        public IList<MessageRule> Rules { get; set; }
        public IList<IMethodCall<MessageContext, object>> BeforeActions { get; set; }
        //TODO doc
        public bool ThrowExceptionWhenNoRuleMatch { get; set; }

        public MessageBusOptionsBuilder()
        {
            Rules = new List<MessageRule>();
            BeforeActions = new List<IMethodCall<MessageContext, object>>();
            ThrowExceptionWhenNoRuleMatch = true;
        }

        /// <summary>
        /// Build an <see cref="MessageBusOptions"/> using defined rules.
        /// </summary>
        public MessageBusOptions Build()
        {
            if(!Rules.IsUnique(x => x.Name, out var ruleName))
            {
                throw new ConfigurationException($"Duplicate rule name '{ruleName}' found in your configuration.\n" + 
                    "Please review your configuration to give unique name for each rule.");
            }
            return new MessageBusOptions(
                BeforeActions,
                Rules,
                ThrowExceptionWhenNoRuleMatch);
        }

        /// <summary>
        /// Add global action to call immediatly after creating <see cref="MessageContext"/> and before executing rules.
        /// </summary>
        /// <remarks>
        /// <para>Actions are executed in order of their definition</para>
        /// <para>Global configuration can have zero or many actions</para>
        /// </remarks>
        /// <param name="action">The action to execute</param>
        /// <returns><see cref="MessageBusOptionsBuilder"/></returns>
        public MessageBusOptionsBuilder BeforeAll(Action<MessageContext> action)
        {
            BeforeActions.Add(MethodCall.FromFunction(action));
            return this;
        }

        /// <summary>
        /// Add global action to call immediatly after creating <see cref="MessageContext"/> and before executing rules.
        /// </summary>
        /// <remarks>
        /// <para>Actions are executed in order of their definition</para>
        /// <para>Global configuration can have zero or many actions</para>
        /// </remarks>
        /// <param name="action">The async action to execute</param>
        /// <returns><see cref="MessageBusOptionsBuilder"/></returns>
        public MessageBusOptionsBuilder BeforeAll(Func<MessageContext, Task> action)
        {
            BeforeActions.Add(MethodCall.FromFunction(action));
            return this;
        }

        /// <summary>
        /// Add global action to call immediatly after creating <see cref="MessageContext"/> and before executing rules.
        /// </summary>
        /// <remarks>
        /// <para>Actions are executed in order of their definition</para>
        /// <para>Global configuration can have zero or many actions</para>
        /// </remarks>
        /// <typeparam name="TService">The extra service to resolve to execute action</typeparam>
        /// <param name="action">The action to execute</param>
        /// <returns><see cref="MessageBusOptionsBuilder"/></returns>
        public MessageBusOptionsBuilder BeforeAll<TService>(Action<TService, MessageContext> action)
        {
            BeforeActions.Add(MethodCall.FromFunction(action));
            return this;
        }

        /// <summary>
        /// Add global action to call immediatly after creating <see cref="MessageContext"/> and before executing rules.
        /// </summary>
        /// <remarks>
        /// <para>Actions are executed in order of their definition</para>
        /// <para>Global configuration can have zero or many actions</para>
        /// </remarks>
        /// <typeparam name="TService">The extra service to resolve to execute action</typeparam>
        /// <param name="action">The async action to execute</param>
        /// <returns><see cref="MessageBusOptionsBuilder"/></returns>
        public MessageBusOptionsBuilder BeforeAll<TService>(Func<TService, MessageContext, Task> action)
        {
            BeforeActions.Add(MethodCall.FromFunction(action));
            return this;
        }

        /// <summary>
        /// Add a new empty <see cref="MessageRule"/> and define when it will be applied by <see cref="MessageBus"/>.
        /// </summary>
        /// <param name="name">The name of the rule</param>
        /// <returns><see cref="MessageRuleBuilder"/></returns>
        public MessageRuleBuilder AddRule(string name)
        {
            var rule = new MessageRule(name);
            rule.StringRepresentation += $"{nameof(AddRule)}(\"{name}\")";
            Rules.Add(rule);
            return new MessageRuleBuilder(rule);
        }
    }
}