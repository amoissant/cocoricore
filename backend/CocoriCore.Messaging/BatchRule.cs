﻿using CocoriCore.Common;
using System.Collections.Generic;
using System.Diagnostics;

namespace CocoriCore.Messaging
{
    [DebuggerDisplay("{ToString()}")]
    public class BatchRule : Dictionary<string, object>
    {
        public string StringRepresentation { get; set; }
        public List<IMethodCall<BatchContext, object>> CallActions { get; set; }
        public List<IMethodCall<BatchContext, object>> ErrorActions { get; set; }
        public List<IMethodCall<BatchContext, object>> FinallyActions { get; set; }

        public BatchRule()
        {
            CallActions = new List<IMethodCall<BatchContext, object>>();
            ErrorActions = new List<IMethodCall<BatchContext, object>>();
            FinallyActions = new List<IMethodCall<BatchContext, object>>();
        }

        public override string ToString()
        {
            return StringRepresentation;
        }
    }
}
  