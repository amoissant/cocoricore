namespace CocoriCore.Messaging
{
    public interface IBatch
    {
        object[] Messages { get; }
    }
}