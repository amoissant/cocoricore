﻿namespace CocoriCore.Messaging
{
    public class BatchContext : MessageContext
    {
        public BatchContext(MessageContext context, object message, int index) : base(message)
        {
            Index = index;
            ParentContext = context;
            Batch = (IBatch)ParentContext.Message;
        }

        public int Index { get; }
        public MessageContext ParentContext { get; }
        public IBatch Batch { get; }
    }
}