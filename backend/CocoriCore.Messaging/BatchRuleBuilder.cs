﻿using CocoriCore.Common;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CocoriCore.Messaging
{
    public class BatchRuleBuilder
    {
        public BatchRule Rule { get; }

        public BatchRuleBuilder() : this (new BatchRule())
        {
        }

        public BatchRuleBuilder(BatchRule rule)
        {
            Rule = rule;
        }

        public BatchRuleBuilder ExposeRule(Action<BatchRule> action)//TODO ExposeRule aussi pour MessageRule ?
        {
            action(Rule);
            return this;
        }

        public BatchRuleBuilder Call(Action<BatchContext> action)
        {
            Rule.CallActions.Add(MethodCall.FromFunction(action));
            Rule.StringRepresentation += $".{nameof(Call)}()";
            return this;
        }
        public BatchRuleBuilder Call(Func<BatchContext, Task> action)
        {
            Rule.CallActions.Add(MethodCall.FromFunction(action));
            Rule.StringRepresentation += $".{nameof(Call)}()";
            return this;
        }

        public BatchRuleBuilder Call<TService>(Action<TService, BatchContext> action)
        {
            Rule.CallActions.Add(MethodCall.FromFunction(action));
            Rule.StringRepresentation += $".{nameof(Call)}<{typeof(TService).Name}>()";
            return this;
        }

        public BatchRuleBuilder Call<TService>(Func<TService, BatchContext, Task> action)
        {
            Rule.CallActions.Add(MethodCall.FromFunction(action));
            Rule.StringRepresentation += $".{nameof(Call)}<{typeof(TService).Name}>()";
            return this;
        }

        //TODO si on défini un OnError alors ca veut dire qu'on continue donc ne pas throw l'erreur ensuite
        //lever les exception dans les finally, ce n'est pas sensé se produire
        public BatchRuleBuilder OnError(Action<BatchContext> action)
        {
            Rule.ErrorActions.Add(MethodCall.FromFunction(action));
            Rule.StringRepresentation += $".{nameof(Call)}()";
            return this;
        }
        public BatchRuleBuilder OnError(Func<BatchContext, Task> action)
        {
            Rule.ErrorActions.Add(MethodCall.FromFunction(action));
            Rule.StringRepresentation += $".{nameof(Call)}()";
            return this;
        }

        public BatchRuleBuilder OnError<TService>(Action<TService, BatchContext> action)
        {
            Rule.ErrorActions.Add(MethodCall.FromFunction(action));
            Rule.StringRepresentation += $".{nameof(Call)}<{typeof(TService).Name}>()";
            return this;
        }

        public BatchRuleBuilder OnError<TService>(Func<TService, BatchContext, Task> action)
        {
            Rule.ErrorActions.Add(MethodCall.FromFunction(action));
            Rule.StringRepresentation += $".{nameof(Call)}<{typeof(TService).Name}>()";
            return this;
        }

        //TODO lever les exception dans les finally, ce n'est pas sensé se produire
        public BatchRuleBuilder Finally(Action<BatchContext> action)
        {
            Rule.FinallyActions.Add(MethodCall.FromFunction(action));
            Rule.StringRepresentation += $".{nameof(Finally)}()";
            return this;
        }
        public BatchRuleBuilder Finally(Func<BatchContext, Task> action)
        {
            Rule.FinallyActions.Add(MethodCall.FromFunction(action));
            Rule.StringRepresentation += $".{nameof(Finally)}()";
            return this;
        }

        public BatchRuleBuilder Finally<TService>(Action<TService, BatchContext> action)
        {
            Rule.FinallyActions.Add(MethodCall.FromFunction(action));
            Rule.StringRepresentation += $".{nameof(Finally)}<{typeof(TService).Name}>()";
            return this;
        }

        public BatchRuleBuilder Finally<TService>(Func<TService, BatchContext, Task> action)
        {
            Rule.FinallyActions.Add(MethodCall.FromFunction(action));
            Rule.StringRepresentation += $".{nameof(Finally)}<{typeof(TService).Name}>()";
            return this;
        }
    }
}
  