﻿using System.Threading.Tasks;

namespace CocoriCore.Messaging
{
    public interface IBatchHandler
    {
        Task ExecuteAsync(BatchRule rule, MessageContext context);
    }
}