﻿using System.Linq;

namespace CocoriCore.Messaging
{
    public class Batch<TMessage> : IBatch
    {
        public TMessage[] Messages;

        public Batch()
        {
            Messages = new TMessage[0];
        }

        public Batch(params TMessage[] messages)
        {
            Messages = messages;
        }

        object[] IBatch.Messages => Messages.Cast<object>().ToArray();
    }
}
