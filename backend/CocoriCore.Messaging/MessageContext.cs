﻿using CocoriCore.Collection.Extensions;
using System;
using System.Collections.Generic;

namespace CocoriCore.Messaging
{
    public class MessageContext : Dictionary<string, object>
    {
        public MessageContext(object message)
        {
            Message = message;
        }

        public object Message { get; }
        public object Response { get; set; }
        public Exception Exception { get; set; }
        public DebugDatas DebugDatas { get; set; }

        public bool HasKeyWithValue(string key, object value)
        {
            return Equals(this.GetValueOrDefault(key), value);
        }
    }
}