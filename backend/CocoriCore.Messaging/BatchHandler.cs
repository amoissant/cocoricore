﻿using CocoriCore.Common;
using CocoriCore.Common.Diagnostic;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CocoriCore.Messaging
{
    public class BatchHandler : IBatchHandler
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger _logger;

        public BatchHandler(IUnitOfWork unitOfWork, ILogger logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        public async Task ExecuteAsync(BatchRule rule, MessageContext context)
        {
            rule = rule ?? throw new ArgumentNullException(nameof(rule));
            context = context ?? throw new ArgumentNullException(nameof(context));

            var messages = GetBatchMessages(context);
            var responses = new object[messages.Length];
            for (var i = 0; i < messages.Length; i++)
            {
                responses[i] = await RunActionForMessageAsync(rule, context, messages[i], i);
            }
            context.Response = responses;
        }

        private object[] GetBatchMessages(MessageContext context)
        {
            if (context.Message is null || !(context.Message is IBatch))
            {
                throw new MessageBusException($"Received message of type {context.Message?.GetPrettyType(full: true)} must implements {typeof(IBatch)}.");
            }
            var messages = ((IBatch)context.Message).Messages;
            return messages;
        }

        private async Task<object> RunActionForMessageAsync(BatchRule rule, MessageContext parentContext, object message, int index)
        {
            var context = new BatchContext(parentContext, message, index);
            try
            {
                await RunCallActionsAsync(rule, context);
                return context.Response;
            }
            catch (Exception exception)
            {
                _logger.LogError(exception);
                AddExceptionToContext(context, exception);
                context.Response = exception;
                await RunErrorActionsAsync(rule, context);
                return context.Response;
            }
            finally
            {
                await RunFinallyActionsAsync(rule, context);
            }
        }

        private async Task RunCallActionsAsync(BatchRule rule, BatchContext context)
        {
            foreach (var action in rule.CallActions)
            {
                await action.ExecuteAsync(_unitOfWork, context);
            }
        }

        private void AddExceptionToContext(BatchContext context, Exception exception)
        {
            var messageType = context.Message?.GetType()?.FullName ?? "(null)";
            context.Exception = exception;
            context.DebugDatas = new DebugDatas { { "Index", context.Index } };
            exception.Data[nameof(MessageBus)] = context.DebugDatas;
        }

        private async Task RunErrorActionsAsync(BatchRule rule, BatchContext context)
        {
            foreach (var action in rule.ErrorActions)
            {
                await action.ExecuteAsync(_unitOfWork, context);
            }
        }

        private async Task RunFinallyActionsAsync(BatchRule rule, BatchContext context)
        {
            foreach (var action in rule.FinallyActions)
            {
                await action.ExecuteAsync(_unitOfWork, context);
            }
        }
    }
}
  