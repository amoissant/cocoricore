﻿using System;

namespace CocoriCore.Messaging
{
    public class MessageBusException : Exception
    {
        public MessageBusException(string message) : base(message)
        {
        }
    }
}