﻿using CocoriCore.Common;
using System.Collections.Generic;
using System.Diagnostics;

namespace CocoriCore.Messaging
{
    [DebuggerDisplay("{ToString()}")]
    public class MessageRule : Dictionary<string, object>
    {
        public string Name { get; }
        public string StringRepresentation { get; set; }
        public IMethodCall<MessageContext, bool> Condition { get; set; }
        public List<IMethodCall<MessageContext, object>> CallActions { get; set; }
        public List<IMethodCall<MessageContext, object>> ErrorActions { get; set; }
        public List<IMethodCall<MessageContext, object>> FinallyActions { get; set; }

        public MessageRule(string name)
        {
            Name = name;
            CallActions = new List<IMethodCall<MessageContext, object>>();
            ErrorActions = new List<IMethodCall<MessageContext, object>>();
            FinallyActions = new List<IMethodCall<MessageContext, object>>();
        }

        public override string ToString()
        {
            return StringRepresentation;
        }
    }
}