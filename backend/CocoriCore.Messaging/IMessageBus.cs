﻿using System.Threading.Tasks;

namespace CocoriCore.Messaging
{
    public interface IMessageBus
    {
        Task<object> ExecuteAsync(object message, string ruleName = null);
        Task<MessageContext> ExecuteAsync(MessageContext context, string ruleName = null);
    }
}