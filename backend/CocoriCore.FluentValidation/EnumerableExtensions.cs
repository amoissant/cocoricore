﻿using System.Collections.Generic;
using System.Linq;

namespace CocoriCore.FluentValidation
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<T> NullProof<T>(this IEnumerable<T> collection)
        {
            if (collection == null)
                return new T[0];
            else
                return collection.Where(x => x != null);
        }

        public static bool NotEmpty<T>(this IEnumerable<T> collection)
        {
            if (collection == null)
                return false;
            else
                return collection.Count() > 0;
        }

        public static bool Empty<T>(this IEnumerable<T> collection)
        {
            if (collection == null)
                return true;
            else
                return collection.Count() == 0;
        }
    }
}
