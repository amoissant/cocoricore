﻿using CocoriCore.Common;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CocoriCore.Dynamic.Extension;
using HeyRed.Mime;
using FluentValidation.Results;
using CocoriCore.Expressions.Extension;
using CocoriCore.Collection.Extensions;

namespace CocoriCore.FluentValidation
{
    public static class FluentValidationExtension
    {
        public static async Task ValidateAndThrowAsync(this IValidator validator, object instance)
        {
            var context = new ValidationContext<object>(instance);
            var result = await validator.ValidateAsync(context);
            if (!result.IsValid)
                throw new ValidationException(result.Errors);
        }

        public static ValidationResult Validate(this IValidator validator, object instance)
        {
            var context = new ValidationContext<object>(instance);
            return validator.Validate(context);
        }

        //TODO a tester
        public static IRuleBuilderOptions<T, TProperty> IsDateTime<T, TProperty>(this IRuleBuilder<T, TProperty> ruleBuilder)
        {
            return ruleBuilder.OfType(typeof(DateTime)).WithErrorCode("NOT_DATETIME");
        }

        //TODO a tester
        public static IRuleBuilderOptions<T, TProperty> IsString<T, TProperty>(this IRuleBuilder<T, TProperty> ruleBuilder)
        {
            return ruleBuilder.OfType(typeof(string)).WithErrorCode("NOT_STRING");
        }

        //TODO a tester
        public static IRuleBuilderOptions<T, TProperty> IsDynamic<T, TProperty>(this IRuleBuilder<T, TProperty> ruleBuilder)
        {
            return ruleBuilder.OfType(typeof(ExpandoObject));
        }

        //TODO a tester
        public static IRuleBuilderOptions<T, TProperty> IsInteger<T, TProperty>(this IRuleBuilder<T, TProperty> ruleBuilder)
        {
            return ruleBuilder
                .Must(x => x.IsInteger())
                .WithMessage("{PropertyName} must be an integer")
                .WithErrorCode("NOT_INTEGER");
        }

        //TODO a tester
        public static IRuleBuilderOptions<T, TProperty> IsNumber<T, TProperty>(this IRuleBuilder<T, TProperty> ruleBuilder)
        {
            return ruleBuilder
                .Must(x => DynamicUtils.IsNumber(x))
                .WithMessage("{PropertyName} must be a number");
        }

        //TODO a tester
        public static IRuleBuilderOptions<T, dynamic> MatchGuid<T>(this IRuleBuilder<T, dynamic> ruleBuilder)
        {
            return ruleBuilder
                .Must(x => DynamicUtils.IsStringMatchingGuid(x))
                .WithMessage("{PropertyName} must be a string corresponding to Guid format.")
                .WithErrorCode("NOT_GUID_FORMAT");
        }

        //TODO a tester
        public static IRuleBuilderOptions<T, TProperty> OfType<T, TProperty>(this IRuleBuilder<T, TProperty> ruleBuilder, Type type)
        {

            return ruleBuilder.Must((rootObject, obj, context) =>
            {
                context.MessageFormatter.AppendArgument("Type", type.Name);
                return obj == null || type.IsAssignableFrom(obj?.GetType());
            })
            .WithMessage("{PropertyName} must be of type {Type}.");
        }

        //TODO a tester
        public static IRuleBuilderOptions<T, IEnumerable<TProperty>> UniqueValue<T, TProperty>(this IRuleBuilder<T, IEnumerable<TProperty>> ruleBuilder,
           Expression<Func<TProperty, dynamic>> member)
        {
            return ruleBuilder
                .Must(x => x.NullProof().IsUnique(member.Compile()))
                .WithMessage($"Every {member.GetMemberInfo().Name} must be unique")
                .WithErrorCode("NOT_UNIQUE_FIELD");
        }

        //TODO a tester
        public static IRuleBuilderOptions<T, IEnumerable<TProperty>> UniqueValue<T, TProperty>(this IRuleBuilder<T, IEnumerable<TProperty>> ruleBuilder)
        {
            return ruleBuilder
                .Must(x => x?.Distinct().Count() == x?.Count())
                .WithMessage($"Every element must be unique")
                .WithErrorCode("NOT_UNIQUE_VALUE");
        }

        public static IRuleBuilderOptions<T, DateTime> DateWithoutTime<T>(this IRuleBuilder<T, DateTime> ruleBuilder)
        {
            return ruleBuilder
                .NotEmpty()
                .Must(x => x.IsDateWithoutTime())
                .WithErrorCode("INVALID_DATE")
                .WithMessage("The date cannot have a time element");
        }

        public static IRuleBuilderOptions<T, string> PasswordConfirmation<T>(this IRuleBuilder<T, string> ruleBuilder,
            Expression<Func<T, string>> passwordMember)
        {
            return ruleBuilder
                .Equal(passwordMember)
                .WithErrorCode("INVALID_PASSWORD_CONFIRMATION")
                .WithMessage("Password confirmation doesn't match password");
        }

        public static IRuleBuilderOptions<T, string> Url<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder
                .Must(x => string.IsNullOrEmpty(x) || Uri.IsWellFormedUriString(x, UriKind.Absolute))
                .WithErrorCode("INVALID_URL")
                .WithMessage("{PropertyName} must be a valid url");
        }

        public static IRuleBuilderOptions<T, string> MimeType<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder
                .Must((t, value) => value == null || MimeTypes.IsKnowMimeType(value))
                .WithMessage($"Unknow MIME type.")
                .WithErrorCode("UNKNOW_MIME_TYPE");
        }
    }
}
