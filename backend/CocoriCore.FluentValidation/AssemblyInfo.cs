﻿using System.Reflection;

namespace CocoriCore.FluentValidation
{
    public static class AssemblyInfo
    {
        public static Assembly Assembly => typeof(AssemblyInfo).Assembly;
    }
}
