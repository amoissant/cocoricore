using System.Linq;
using Autofac;
using CocoriCore.Common;
using FluentValidation;

namespace CocoriCore.FluentValidation
{
    public static class ContainerBuilderExtension
    {
        public static ContainerBuilder RegisterFluentValidators(this ContainerBuilder builder, params System.Reflection.Assembly[] assemblies)
        {
            if (assemblies.Count() == 0)
            {
                throw new ConfigurationException("You must pass at least one assembly.");
            }
            builder.RegisterAssemblyTypes(assemblies)
                .AssignableTo<IValidator>()
                .AsSelf()
                .As<IValidator>();
            return builder;
        }
    }
}