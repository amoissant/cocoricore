﻿using CocoriCore.Common;
using System.Collections.Generic;
using CocoriCore.Collection.Extensions;

namespace CocoriCore.Secret
{
    public class SecretOptions
    {
        public IList<ISecret> Secrets { get; }
        public string MasterKeyBase64 { get; set; }
        public string MasterKeyPath { get; set; }
        public bool MasterKeyPathFromConfigFile { get; set; }
        private Format _configFileFormat;
        public string EncryptionMarker { get; set; }

        public SecretOptions()
        {
            Secrets = new List<ISecret>();
            EncryptionMarker = "__";
            _configFileFormat = Format.None;
        }

        public Format ConfigFileFormat 
        {
            get => _configFileFormat;
            set
            {
                if(_configFileFormat == Format.None)
                {
                    _configFileFormat = value;
                }
                else if(value != Format.None && value != _configFileFormat)
                {
                    throw new ConfigurationException($"Mixing XPath and JsonPath methods.");
                }
            }
        }

        public virtual void ValidateConfiguration()
        {
            ValidateSecretNameUnicity();
        }

        private void ValidateSecretNameUnicity()
        {
            if (!Secrets.IsUnique(x => x.Name, out var duplicateName))
            {
                throw new ConfigurationException($"Duplicate secret name {duplicateName}.");
            }
        }
    }
}
