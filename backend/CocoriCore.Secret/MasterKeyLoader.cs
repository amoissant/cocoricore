﻿using CocoriCore.Common;
using CocoriCore.FileSystem;
using CocoriCore.Security;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace CocoriCore.Secret
{
    public class MasterKeyLoader
    {
        private readonly SecretOptions _options;
        private readonly IFileSystem _fileSystem;
        private readonly ICryptoService _cryptoService;
        private readonly ILogger _logger;
        private byte[] _masterkey;

        public MasterKeyLoader(SecretOptions options, IFileSystem fileSystem, ICryptoService cryptoService, ILogger logger)
        {
            _options = options;
            _fileSystem = fileSystem;
            _cryptoService = cryptoService;
            _logger = logger;
        }

        public byte[] MasterKey => _masterkey;

        public Task<byte[]> GetMasterKeyAsync(Path configFilePath = null)
        {
            return GetOrCreateMasterKeyAsync(configFilePath, false);
        }

        public Task<byte[]> GetOrCreateMasterKeyAsync(Path configFilePath = null)
        {
            return GetOrCreateMasterKeyAsync(configFilePath, true);
        }

        public async Task<byte[]> GetOrCreateMasterKeyAsync(Path configFilePath, bool generateIfNotPresent = false)
        {
            if (_masterkey != null)
            {
                return _masterkey;
            }
            CheckMasterKeyConfiguration();
            if (_options.MasterKeyBase64 != null)
            {
                _masterkey = Convert.FromBase64String(_options.MasterKeyBase64);
            }
            else
            {
                var masterKeyPath = await GetMasterKeyPathAsync(configFilePath);
                if (generateIfNotPresent && !await _fileSystem.FileExistsAsync(masterKeyPath))
                {
                    _logger.LogInformation("No master key found, generate one in file :\n{0}", masterKeyPath);
                    await CreateMasterKeyFileAsync(masterKeyPath);
                }
                _masterkey = await ReadMasterKeyFromFileAsync(masterKeyPath);
                _logger.LogInformation("Using master key file {0}", _fileSystem.GetAbsolutePath(masterKeyPath));
            }
            return _masterkey;
        }

        private void CheckMasterKeyConfiguration()
        {
            if (_options.MasterKeyBase64 == null && _options.MasterKeyPath == null)
            {
                throw new ConfigurationException($"Master key configuration error.\n" +
                    $"Use builder.{nameof(SecretOptionsBuilder.ForMasterKey)}() to define a way to retrive master key.");
            }
        }

        private async Task<Path> GetMasterKeyPathAsync(Path configFilePath)
        {
            Path masterKeyPath;
            if (_options.MasterKeyPathFromConfigFile)
            {
                var fileContent = await _fileSystem.ReadAsTextAsync(configFilePath);
                var handler = CreatePathHandler(_options.ConfigFileFormat);
                var matches = await handler.GetMatchesAsync(_options.MasterKeyPath, fileContent);
                CheckMatchCount(matches.Count(), configFilePath);
                masterKeyPath = matches.First();
            }
            else
            {
                masterKeyPath = _options.MasterKeyPath;
            }
            return masterKeyPath;
        }

        private IPathHandler CreatePathHandler(Format format)
        {
            if (format == Format.Json)
            {
                return new JsonPathHandler();
            }
            else if (format == Format.Xml)
            {
                return new XPathHandler();
            }
            else
            {
                throw new NotImplementedException($"{format}");
            }
        }

        private void CheckMatchCount(int matchCount, Path filePath)
        {
            if (matchCount == 0 || matchCount > 1)
            {
                throw new ConfigurationException($"No match found for master key using '{_options.MasterKeyPath}' in file {filePath}.\n" +
                        "Please review your configuration to match one and only one node.");
            }
        }

        public async Task CreateMasterKeyFileAsync(Path path)
        {
            var generatedMasterKey = _cryptoService.GenerateNewKey();
            var base64MasterKey = Convert.ToBase64String(generatedMasterKey);
            var directoryPath = path.GetParent();
            if (!await _fileSystem.DirectoryExistsAsync(directoryPath))
            {
                await _fileSystem.CreateDirectoryAsync(directoryPath);
            }
            await _fileSystem.CreateTextFileAsync(path, base64MasterKey);
        }

        public async Task<byte[]> ReadMasterKeyFromFileAsync(Path masterKeyPath)
        {
            var base64Key = await _fileSystem.ReadAsTextAsync(masterKeyPath);
            try
            {
                var masterKey = base64Key.GetBase64Bytes();
                _cryptoService.CheckKeyIsValid(masterKey);
                return masterKey;
            }
            catch(Exception e)
            {
                throw new CryptographicException($"The key in file {masterKeyPath} is not a valid key.", e);
            }
        }
    }
}
