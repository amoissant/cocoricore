﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CocoriCore.Secret
{
    public interface IPathHandler
    {
        Task<string> ApplyModificationsAsync(IEnumerable<Action> modifications);
        Task<IEnumerable<string>> GetMatchesAsync(string pathExpression, string fileContent);
        Task<IEnumerable<(ISecret secret, string value)>> GetMatchesAsync(IEnumerable<ISecret> secrets, string fileContent);
        Task<IEnumerable<Action>> GetModificationsAsync(IEnumerable<ISecret> secrets, string fileContent, Action<ISecret, int> checkMatchCount, Func<ISecret, string, string> action);
    }
}
