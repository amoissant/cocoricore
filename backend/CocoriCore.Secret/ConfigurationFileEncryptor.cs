﻿using CocoriCore.Common;
using CocoriCore.FileSystem;
using CocoriCore.Security;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CocoriCore.Secret
{
    public class ConfigurationFileEncryptor
    {
        private readonly SecretOptions _options;
        private readonly IFileSystem _fileSystem;
        private readonly ICryptoService _cryptoService;
        private readonly ILogger _logger;
        private readonly MasterKeyLoader _masterKeyLoader;

        public ConfigurationFileEncryptor(SecretOptions options, IFileSystem fileSystem, ICryptoService cryptoService, 
            ILogger logger, MasterKeyLoader masterKeyLoader)
        {
            _options = options;
            _fileSystem = fileSystem;
            _cryptoService = cryptoService;
            _logger = logger;
            _masterKeyLoader = masterKeyLoader;
        }

        public async Task EncryptSecretsAsync(Path inputFile, Path outputFile = null)
        {
            if (_options.ConfigFileFormat == Format.None)
            {
                return;
            }
            outputFile = outputFile ?? inputFile;
            _logger.LogInformation("Encrypting secrets for file {0}", _fileSystem.GetAbsolutePath(outputFile));

            var masterKey = await _masterKeyLoader.GetOrCreateMasterKeyAsync(inputFile);
            var fileContent = await _fileSystem.ReadAsTextAsync(inputFile);

            var handler = CreatePathHandler(_options.ConfigFileFormat);
            var modifications = await handler.GetModificationsAsync(_options.Secrets,
                fileContent,
                (s, c) => CheckMatchCount(s, c, inputFile),
                (s, v) => Encrypt(s, v, masterKey));
            var newContent = await handler.ApplyModificationsAsync(modifications);
            await _fileSystem.CreateTextFileAsync(outputFile, newContent);
        }

        private IPathHandler CreatePathHandler(Format format)
        {
            if (format == Format.Json)
            {
                return new JsonPathHandler();
            }
            else if (format == Format.Xml)
            {
                return new XPathHandler();
            }
            else
            {
                throw new NotImplementedException($"{format}");
            }
        }

        private void CheckMatchCount(ISecret secret, int matchCount, Path filePath)
        {
            if (matchCount == 0 && !secret.IsOptionnal)
            {
                throw new ConfigurationException($"No match found for secret '{secret}' in file {filePath}.\n" +
                    "Please review your configuration or set this secret as optionnal.");
            }
            if (matchCount > 1)
            {
                throw new ConfigurationException($"Duplicate match for secret '{secret}' in file {filePath}.\n" +
                   "Please review your configuration to have one on only one match.");
            }
        }

        private string Encrypt(ISecret secret, string value, byte[] masterKey)
        {
            if (value != null && 
                value.Length > 0 && 
                !value.StartsWith(_options.EncryptionMarker))
            {
                var unescapedSecretValue = value.Unescape();
                _logger.LogInformation("{0} value : {1}", secret.Name, value);
                var encryptedValue = _cryptoService.Encrypt(unescapedSecretValue, masterKey);
                var encryptedSecret = _options.EncryptionMarker + encryptedValue.ToBase64();
                _logger.LogInformation("{0} encrypted : {1}", secret.Name, encryptedSecret);
                return encryptedSecret;
            }
            return value;
        }

        public async Task DecryptSecretsAsync(Path inputFile, Path outputFile = null)
        {
            outputFile = outputFile ?? inputFile;
            var masterKey = await _masterKeyLoader.GetOrCreateMasterKeyAsync(inputFile, false);
            var fileContent = await _fileSystem.ReadAsTextAsync(inputFile);

            var handler = CreatePathHandler(_options.ConfigFileFormat);
            var modifications = await handler.GetModificationsAsync(_options.Secrets,
                fileContent,
                (s, c) => { },
                (s, v) => Decrypt(s, v, masterKey));
            var newContent = await handler.ApplyModificationsAsync(modifications);
            await _fileSystem.CreateTextFileAsync(outputFile, newContent);
        }

        private string Decrypt(ISecret secret, string secretValue, byte[] masterKey)
        {
            try
            {
                if (secretValue.StartsWith(_options.EncryptionMarker))
                {
                    var encryptedBase64 = secretValue.Substring(_options.EncryptionMarker.Length);
                    var encrytedBytes = encryptedBase64.GetBase64Bytes();
                    var decryptedBytes = _cryptoService.Decrypt(encrytedBytes, masterKey);
                    return decryptedBytes.ToUtf8().Escape();
                }
                return secretValue;
            }
            catch(Exception e)
            {
                var errorMessage = $"Error while decrypting secret '{secret.Name}', see inner exception for details.";
                var exception = new InvalidOperationException(errorMessage, e);
                exception.Data["SecretValue"] = secretValue;
                throw exception;
            }
        }
    }
}
