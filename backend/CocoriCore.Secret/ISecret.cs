﻿namespace CocoriCore.Secret
{
    public interface ISecret
    {
        string Name { get; }
        bool IsOptionnal { get; set; }
    }
}
