﻿using CocoriCore.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CocoriCore.Secret
{
    public class XPathHandler : IPathHandler
    {
        private XmlDocument _document;
        private bool _documentHasBOM;

        public XPathHandler()
        {
        }

        public Task<IEnumerable<string>> GetMatchesAsync(string pathExpression, string fileContent)
        {
            _document = LoadXml(fileContent);
            var nodes = _document.SelectNodes(pathExpression).Cast<XmlNode>();
            return Task.FromResult(nodes.Select(x => x.Value));
        }

        public Task<IEnumerable<(ISecret secret, string value)>> GetMatchesAsync(IEnumerable<ISecret> secrets, string fileContent)
        {
            _document = LoadXml(fileContent);
            var matches = new List<(ISecret secret, string value)>();
            foreach (var secret in secrets)
            {
                var xPathSecret = (XPathSecret)secret;
                var nodes = _document.SelectNodes(xPathSecret.PathExpression).Cast<XmlNode>();
                matches.AddRange(nodes.Select(n => (secret, n.Value)));
            }
            return Task.FromResult(matches.AsEnumerable());
        }

        public Task<IEnumerable<Action>> GetModificationsAsync(IEnumerable<ISecret> secrets, string fileContent, 
            Action<ISecret, int> checkMatchCount, Func<ISecret, string, string> action)
        {
            _document = LoadXml(fileContent);
            var modifications = new List<Action>();
            foreach (var secret in secrets)
            {
                var xPathSecret = (XPathSecret)secret;
                var nodes = _document.SelectNodes(xPathSecret.PathExpression);
                checkMatchCount(secret, nodes.Count);
                foreach(var node in nodes.Cast<XmlNode>())
                {
                    modifications.Add(() => node.Value = action(secret, node.Value));
                }
            }
            return Task.FromResult(modifications.AsEnumerable());
        }

        private XmlDocument LoadXml(string xml)
        {
            var document = new XmlDocument();
            _documentHasBOM = xml.HasUTF8BOM();
            document.LoadXml(xml.RemoveUTF8BOM());
            return document;
        }

        public async Task<string> ApplyModificationsAsync(IEnumerable<Action> modifications)
        {
            foreach (var modification in modifications)
            {
                modification();
            }
            using (var s = new System.IO.MemoryStream())
            using (var sw = new System.IO.StreamWriter(s, new UTF8Encoding(_documentHasBOM)))
            {
                _document.Save(sw);
                var newContent = await s.ReadAsUtf8StringAsync();
                if(_documentHasBOM)
                {
                    newContent = newContent.AddUTF8BOM();
                }
                return newContent;
            }
        }
    }
}
