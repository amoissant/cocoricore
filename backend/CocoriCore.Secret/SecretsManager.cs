﻿using CocoriCore.Collection.Extensions;
using CocoriCore.Common;
using CocoriCore.FileSystem;
using CocoriCore.Security;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocoriCore.Secret
{
    public class SecretManager : ISecretManager
    {
        private readonly SecretOptions _options;
        private readonly ICryptoService _cryptoService;
        private readonly IFileSystem _fileSystem;
        private readonly ILogger _logger;
        private readonly MasterKeyLoader _masterKeyLoader;
        private Dictionary<object, string> _secrets;

        public SecretManager(SecretOptions options, ICryptoService cryptoService, IFileSystem fileSystem, 
            ILogger logger, MasterKeyLoader masterKeyLoader)
        {
            _options = options;
            _cryptoService = cryptoService;
            _fileSystem = fileSystem;
            _logger = logger;
            _masterKeyLoader = masterKeyLoader;
        }

        public async Task LoadSecretsAsync(Path configFilePath)
        {
            if (_options.ConfigFileFormat == Format.None)
            {
                return;
            }
            _secrets = new Dictionary<object, string>();
            var fileContent = await _fileSystem.ReadAsTextAsync(configFilePath);
            _logger.LogInformation("Start loading secrets from file {0}", configFilePath);

            var handler = CreatePathHandler(_options.ConfigFileFormat);
            var matches = await handler.GetMatchesAsync(_options.Secrets, fileContent);
            CheckNoMatchSecrets(matches, configFilePath);
            await LoadSecretsAsync(configFilePath, matches);
        }

        private IPathHandler CreatePathHandler(Format format)
        {
            if (format == Format.Json)
            {
                return new JsonPathHandler();
            }
            else if (format == Format.Xml)
            {
                return new XPathHandler();
            }
            else
            {
                throw new NotImplementedException($"{format}");
            }
        }

        private void CheckNoMatchSecrets(IEnumerable<(ISecret secret, string value)> matches, Path configFilePath)
        {
            var noMatchSecret = _options
                .Secrets
                .Where(s => !s.IsOptionnal)
                .Except(matches.Select(m => m.secret))
                .FirstOrDefault();
            if (noMatchSecret != null)
            {
                var errorMessage = $"Secret '{noMatchSecret}' have no match in file {configFilePath}.\n" +
                    "Please review your configuration to have one match for this secret or set it as optionnal.";
                throw new ConfigurationException(errorMessage);
            }
        }

        private async Task LoadSecretsAsync(Path configFilePath, IEnumerable<(ISecret secret, string value)> matches)
        {
            foreach (var match in matches)
            {
                if (_secrets.ContainsKey(match.secret.Name))
                {
                    var errorMessage = $"A value was previously set for secret '{match.secret}'.\n" +
                        $"Please review your configuration to ensure each secret has only one match in file {configFilePath}.";
                    throw new ConfigurationException(errorMessage);
                }
                _secrets[match.secret.Name] = await GetDecryptedValueAsync(match.secret, match.value, configFilePath);
            }
        }

        private async Task<string> GetDecryptedValueAsync(ISecret secret, string secretValue, Path configFilePath)
        {
            if (secretValue.StartsWith(_options.EncryptionMarker))
            {
                var masterKey = await _masterKeyLoader.GetMasterKeyAsync(configFilePath);
                return DecryptSecretValue(secret, secretValue, masterKey);
            }
            else
            {
                _logger.LogWarning("Secret '{0}' is not encrypted, update your configuration file to encrypt it.", secret.Name);
                return secretValue.Unescape();
            }
        }

        private string DecryptSecretValue(ISecret secret, string secretValue, byte[] masterKey)
        {
            try
            {
                secretValue = secretValue.Substring(_options.EncryptionMarker.Length);
                var decryptedValue = _cryptoService.Decrypt(secretValue.GetBase64Bytes(), masterKey);
                return decryptedValue.ToUtf8();
            }
            catch (Exception e)
            {
                var errorMessage = $"Error while loading secret '{secret.Name}', see inner exception for details.";
                var exception = new InvalidOperationException(errorMessage, e);
                exception.Data["secretValue"] = secretValue;
                throw exception;
            }
        }

        public string Get(Enum secretName)
        {
            return Get(secretName.ToString());
        }

        public string Get(string secretName)
        {
            var secret = GetSecretFromName(secretName);
            return _secrets.GetValueOrDefault(secret.Name) ?? string.Empty;
        }

        private ISecret GetSecretFromName(string secretName)
        {
            if (_secrets == null)
            {
                throw new InvalidOperationException($"Secrets have not be loaded, call {nameof(SecretManager.LoadSecretsAsync)}()"
                    + " to load all secrets from configuration file.");
            }
            var secret = _options.Secrets.FirstOrDefault(x => x.Name == secretName);
            if (secret == null)
            {
                var errorMessage = $"Secret '{secretName}' not defined, use {nameof(SecretOptionsBuilder)} to define your secrets.";
                throw new ConfigurationException(errorMessage);
            }
            return secret;
        }

        public void Set(Enum secretName, string secretValue)
        {
            Set(secretName.ToString(), secretValue);
        }

        public void Set(string secretName, string secretValue)
        {
            if (secretValue.StartsWith(_options.EncryptionMarker))
            {
                var secret = GetSecretFromName(secretName);
                _secrets[secretName] = DecryptSecretValue(secret, secretValue, _masterKeyLoader.MasterKey);
            }
            else
            {
                _secrets[secretName] = secretValue;
            }
        }
    }
}
