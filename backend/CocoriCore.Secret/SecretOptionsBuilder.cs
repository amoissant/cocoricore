﻿using System;

namespace CocoriCore.Secret
{
    public class SecretOptionsBuilder
    {
        public SecretOptions Options { get; }

        public SecretOptionsBuilder()
        {
            Options = new SecretOptions();
        }

        public SecretOptions Build()//TODO uniformiser les optionsBuilder comme ceci
        {
            Options.ValidateConfiguration();
            return Options;
        }

        public SecretBuilder For(Enum secret)
        {
            return For(secret.ToString());
        }

        public SecretBuilder For(string secret)
        {
            return new SecretBuilder(Options, secret);
        }

        public MasterKeyBuilder ForMasterKey()
        {
            return new MasterKeyBuilder(Options);
        }
    }
}
