﻿using System;

namespace CocoriCore.Secret
{
    public class JsonPathSecret : ISecret
    {
        public string PathExpression { get; }
        public string Name { get; set; }
        public bool IsOptionnal { get; set; }

        public JsonPathSecret(Enum secretName, string pathExpression)
            : this(secretName.ToString(), pathExpression)
        {
        }

        public JsonPathSecret(string secretName, string pathExpression)
        {
            Name = secretName;
            PathExpression = pathExpression;
        }

        public override string ToString()
        {
            return $"{Name} - JsonPath[{PathExpression}]";
        }

    }
}
