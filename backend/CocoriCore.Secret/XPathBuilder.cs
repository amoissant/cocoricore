﻿namespace CocoriCore.Secret
{
    public class XPathBuilder
    {
        public string AppKey(string name)
        {
            return $"/configuration/appSettings/add[@key='{name}']/@value";
        }

        public string ConnectionString(string name)
        {
            return $"/configuration/connectionStrings/add[@name='{name}']/@connectionString";
        }
    }
}
