﻿using System;

namespace CocoriCore.Secret
{
    public class XPathSecret : ISecret
    {
        public string PathExpression { get; }
        public string Name { get; set; }
        public bool IsOptionnal { get; set; }

        public XPathSecret(Enum secretName, string pathExpression)
            : this(secretName.ToString(), pathExpression)
        {
        }

        public XPathSecret(string secretName, string pathExpression)
        {
            Name = secretName;
            PathExpression = pathExpression;
        }

        public override string ToString()
        {
            return $"{Name} - XPath[{PathExpression}]";
        }
    }
}
