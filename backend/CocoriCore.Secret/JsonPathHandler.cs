﻿using CocoriCore.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocoriCore.Secret
{
    public class JsonPathHandler : IPathHandler
    {
        private JsonSerializer _serializer;
        private JObject _jObject;
        private bool _documentHasBOM;

        public JsonPathHandler()
        {
            _serializer = new JsonSerializer();
            _serializer.Formatting = Newtonsoft.Json.Formatting.Indented;
        }

        public Task<IEnumerable<string>> GetMatchesAsync(string pathExpression, string fileContent)
        {
            _jObject = Parse(fileContent);
            var tokens = _jObject.SelectTokens(pathExpression);
            return Task.FromResult(tokens.Select(x => x.ToString()));
        }

        private JObject Parse(string json)
        {
            _documentHasBOM = json.HasUTF8BOM();
            using (var sr = new System.IO.StringReader(json.RemoveUTF8BOM()))
            using (var jsr = new JsonTextReader(sr))
            {
                return _serializer.Deserialize<JObject>(jsr);
            }
        }

        public Task<IEnumerable<(ISecret secret, string value)>> GetMatchesAsync(IEnumerable<ISecret> secrets, string fileContent)
        {
            _jObject = Parse(fileContent);
            var matches = new List<(ISecret secret, string value)>();
            foreach (var secret in secrets)
            {
                var jsonPathSecret = (JsonPathSecret)secret;
                var tokens = _jObject.SelectTokens(jsonPathSecret.PathExpression).Cast<JValue>();
                matches.AddRange(tokens.Select(t => (secret, t.Value?.ToString())));
            }
            return Task.FromResult(matches.AsEnumerable());
        }

        public Task<IEnumerable<Action>> GetModificationsAsync(IEnumerable<ISecret> secrets, string fileContent,
            Action<ISecret, int> checkMatchCount, Func<ISecret, string, string> action)
        {
            _jObject = Parse(fileContent);
            var modifications = new List<Action>();
            foreach (var secret in secrets)
            {
                var jsonPathSecret = (JsonPathSecret)secret;
                var tokens = _jObject.SelectTokens(jsonPathSecret.PathExpression).Cast<JValue>();
                checkMatchCount(secret, tokens.Count());
                foreach (var token in tokens)
                {
                    modifications.Add(() => token.Value = action(secret, token.Value?.ToString()));
                }
            }
            return Task.FromResult(modifications.AsEnumerable());
        }

        public Task<string> ApplyModificationsAsync(IEnumerable<Action> modifications)
        {
            foreach (var modification in modifications)
            {
                modification();
            }
            using (var sw = new System.IO.StringWriter())
            using (var jsw = new JsonTextWriter(sw))
            {
                _serializer.Serialize(jsw, _jObject);
                var newContent = sw.ToString();
                if(_documentHasBOM)
                {
                    newContent = newContent.AddUTF8BOM();
                }
                return Task.FromResult(newContent);
            }
        }
    }
}
