﻿using CocoriCore.Common;
using System;

namespace CocoriCore.Secret
{
    public class MasterKeyBuilder
    {
        private SecretOptions _options;

        public MasterKeyBuilder(SecretOptions options)
        {
            _options = options;
        }

        public void UseValue(string base64Value)
        {
            _options.MasterKeyBase64 = base64Value;
        }

        public void UsePath(Path masterKeyPath)
        {
            _options.MasterKeyPath = masterKeyPath;
        }

        public void UseXPath(Func<XPathBuilder, string> expressionBuilder)
        {
            var pathExpression = expressionBuilder(new XPathBuilder());
            UseXPath(pathExpression);
        }

        public void UseXPath(string pathExpression)
        {
            _options.MasterKeyPathFromConfigFile = true;
            _options.ConfigFileFormat = Format.Xml;
            _options.MasterKeyPath = pathExpression;
        }

        public void UseJsonPath(string pathExpression)
        {
            _options.MasterKeyPathFromConfigFile = true;
            _options.ConfigFileFormat = Format.Json;
            _options.MasterKeyPath = pathExpression;
        }
    }
}
