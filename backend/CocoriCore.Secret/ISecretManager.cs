﻿using CocoriCore.Common;
using System;
using System.Threading.Tasks;

namespace CocoriCore.Secret
{
    public interface ISecretManager
    {
        string Get(Enum secretName);
        string Get(string secretName);
        Task LoadSecretsAsync(Path configFilePath);
        void Set(Enum secretName, string secretValue);
        void Set(string secretName, string secretValue);
    }
}