﻿namespace CocoriCore.Secret
{
    public enum Format
    {
        None,
        Xml,
        Json
    }
}
