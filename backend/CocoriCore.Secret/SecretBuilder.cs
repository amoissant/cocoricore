﻿using System;

namespace CocoriCore.Secret
{
    public class SecretBuilder
    {
        private SecretOptions Options { get; }
        public string Name { get; }
        public ISecret Secret { get; private set; }

        public SecretBuilder(SecretOptions options, string name)
        {
            Options = options;
            Name = name;
        }

        public SecretBuilder UseXPath(Func<XPathBuilder, string> expressionBuilder)
        {
            var pathExpression = expressionBuilder(new XPathBuilder());
            UseXPath(pathExpression);
            return this;
        }

        public SecretBuilder UseXPath(string pathExpression)
        {
            Options.ConfigFileFormat = Format.Xml;
            Secret = new XPathSecret(Name, pathExpression);
            Options.Secrets.Add(Secret);
            return this;
        }

        public SecretBuilder UseJsonPath(string pathExpression)
        {
            Options.ConfigFileFormat = Format.Json;
            Secret = new JsonPathSecret(Name, pathExpression);
            Options.Secrets.Add(Secret);
            return this;
        }

        public SecretBuilder Optional()
        {
            if(Secret == null)
            {
                throw new InvalidOperationException($"This method should be called after {nameof(UseXPath)} or {nameof(UseJsonPath)}.");
            }
            Secret.IsOptionnal = true;
            return this;
        }
    }
}
