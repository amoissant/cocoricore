﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace CocoriCore.NHibernate
{
    public class NHibernateAsyncEnumerable<T> : IAsyncEnumerable<T>
    {
        private readonly IQueryable<T> _queryable;

        public NHibernateAsyncEnumerable(IQueryable<T> queryable)
        {
            _queryable = queryable;
        }

        public IAsyncEnumerator<T> GetAsyncEnumerator(CancellationToken cancellationToken = default)
        {
            return new NHibernateAsyncEnumerator<T>(_queryable, cancellationToken);
        }
    }
}
