﻿using Microsoft.Extensions.Logging;
using NHibernate;
using System;

namespace CocoriCore.NHibernate
{
    public class NHibernateLoggerFactoryStaticProof : INHibernateLoggerFactory
    {
        private Microsoft.Extensions.Logging.ILoggerFactory _loggerFactory { get; set; }

        public NHibernateLoggerFactoryStaticProof(Microsoft.Extensions.Logging.ILoggerFactory loggerFactory)
        {
            _loggerFactory = loggerFactory;
        }

        public INHibernateLogger LoggerFor(string keyName)
        {
            var logger = CreateLogger(keyName);
            return new NHibernateLogger(logger);
        }

        public INHibernateLogger LoggerFor(Type type)
        {
            var logger = CreateLogger(type.ToString());
            return new NHibernateLogger(logger);
        }

        private ILogger CreateLogger(string keyName)
        {
            try
            {
                return _loggerFactory.CreateLogger(keyName);
            }
            catch (ObjectDisposedException)
            {
                //when using NHibernate and paralellization for unit test
                //we can have this exception because NHibernate keep a static reference to LoggerFactory
                //so when a test complete it dispose LoggerFactory for all other tests which provoke this exception
                _loggerFactory = LoggerFactory.Create(b => { });
                return CreateLogger(keyName);
            }
        }
    }
}
