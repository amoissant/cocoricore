﻿using Microsoft.Extensions.Logging;
using NHibernate;
using System;

namespace CocoriCore.NHibernate
{
    public class NHibernateLogger : INHibernateLogger
    {
        private ILogger _logger;

        public NHibernateLogger(ILogger logger)
        {
            _logger = logger;
        }

        public bool IsEnabled(NHibernateLogLevel logLevel)
        {
            return _logger.IsEnabled(GetLogLevel(logLevel));
        }

        private LogLevel GetLogLevel(NHibernateLogLevel logLevel)
        {
            switch (logLevel)
            {
                case NHibernateLogLevel.Debug:
                    return LogLevel.Debug;
                case NHibernateLogLevel.Error:
                    return LogLevel.Error;
                case NHibernateLogLevel.Fatal:
                    return LogLevel.Critical;
                case NHibernateLogLevel.Info:
                    return LogLevel.Information;
                case NHibernateLogLevel.None:
                    return LogLevel.None;
                case NHibernateLogLevel.Trace:
                    return LogLevel.Trace;
                case NHibernateLogLevel.Warn:
                    return LogLevel.Warning;
                default:
                    throw new NotImplementedException($"{logLevel}");
            }
        }

        public void Log(NHibernateLogLevel logLevel, NHibernateLogValues state, Exception exception)
        {
            _logger.Log(GetLogLevel(logLevel), state.Format, state.Args);
        }

    }
}
