﻿using NHibernate.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CocoriCore.NHibernate
{
    public class NHibernateAsyncEnumerator<T> : IAsyncEnumerator<T>
    {
        private readonly IQueryable<T> _queryable;
        private readonly CancellationToken _cancellationToken;
        private IEnumerator<T> _enumerator;

        public NHibernateAsyncEnumerator(IQueryable<T> queryable, CancellationToken cancellationToken)
        {
            _queryable = queryable;
            _cancellationToken = cancellationToken;
        }

        public T Current => _enumerator.Current;

        public async ValueTask<bool> MoveNextAsync()
        {
            if(_enumerator == null)
            {
                var enumerable = await ((INhQueryProvider)_queryable.Provider).ExecuteAsync<IEnumerable<T>>(_queryable.Expression, _cancellationToken);
                _enumerator = enumerable.GetEnumerator();
            }
            return _enumerator.MoveNext();
        }

        public ValueTask DisposeAsync()
        {
            _enumerator.Dispose();
            return new ValueTask(Task.CompletedTask);
        }
    }
}
