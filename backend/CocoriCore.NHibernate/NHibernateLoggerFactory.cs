﻿using NHibernate;
using System;

namespace CocoriCore.NHibernate
{
    public class NHibernateLoggerFactory : INHibernateLoggerFactory
    {
        private Microsoft.Extensions.Logging.ILoggerFactory _loggerFactory { get; set; }

        public NHibernateLoggerFactory(Microsoft.Extensions.Logging.ILoggerFactory loggerFactory)
        {
            _loggerFactory = loggerFactory;
        }

        public INHibernateLogger LoggerFor(string keyName)
        {
            var logger = _loggerFactory.CreateLogger(keyName);
            return new NHibernateLogger(logger);
        }

        public INHibernateLogger LoggerFor(Type type)
        {
            var logger = _loggerFactory.CreateLogger(type.ToString());
            return new NHibernateLogger(logger);
        }
    }
}
