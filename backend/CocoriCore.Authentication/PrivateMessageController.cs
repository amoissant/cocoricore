﻿using CocoriCore.Common;
using System;

namespace CocoriCore.Authentication
{
    /// <summary>
    /// Based on <see cref="PrivateMessageOptions"/> determine if message is private of public.
    /// Use with <see cref="AuthenticationController"/> to define authentication rules.
    /// </summary>
    /// <remarks>Register this type in the scope of an unit of work.</remarks>
    public class PrivateMessageController
    {
        private readonly PrivateMessageOptions _options;
        private readonly IUnitOfWork _unitOfWork;

        public PrivateMessageController(PrivateMessageOptions options, IUnitOfWork unitOfWork)
        {
            _options = options;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Evaluate if the <paramref name="message"/> is private or public based on
        /// <see cref="PrivateMessageOptions"/>.
        /// </summary>
        /// <param name="message">The message to evaluate</param>
        /// <returns><c>true</c> is message is private or <c>false</c> for public message</returns>
        public bool IsPrivate(object message)
        {
            return IsPrivate(message?.GetType());
        }

        /// <summary>
        /// Evaluate if the <paramref name="messageType"/> is private or public based on
        /// <see cref="PrivateMessageOptions"/>.
        /// </summary>
        /// <param name="messageType">The type of the message to evaluate</param>
        /// <returns><c>true</c> is message is private or <c>false</c> for public message</returns>
        public bool IsPrivate(Type messageType)
        {
            foreach (var rule in _options.Rules)
            {
                if (rule.MatchCall.Execute(_unitOfWork, messageType))
                {
                    return rule.IsPrivate;
                }
            }
            return _options.DefaultIsPrivate;
        }
    }
}