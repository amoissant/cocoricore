﻿using System.Collections.Generic;

namespace CocoriCore.Authentication
{
    public class AuthenticationContext : Dictionary<string, object>
    {
        public AuthenticationContext(object message)
        {
            Message = message;
        }

        public object Message { get; }
    }
}