﻿using CocoriCore.Common;
using System;
using System.Threading.Tasks;

namespace CocoriCore.Authentication
{
    public class AuthenticationBuilder
    {
        public AuthenticationOptions Options { get; }
        public AuthenticationRule Rule { get; }

        public AuthenticationBuilder(AuthenticationOptions options, AuthenticationRule rule)
        {
            Options = options;
            Rule = rule;
        }

        /// <summary>
        /// Add an action to execute when rule is applied. 
        /// When all actions perfom without exception then authentication is successfull else it fail.
        /// </summary>
        /// <remarks>
        /// <para>Actions are executed in order of their definition</para>
        /// <para>A rule can have zero or many actions</para>
        /// </remarks>
        /// <param name="action">Action to execute</param>
        /// <returns><see cref="AuthenticationBuilder"/></returns>
        public AuthenticationBuilder Call(Action<AuthenticationContext> action)
        {
            Rule.Actions.Add(MethodCall.FromFunction(action));
            Rule.StringRepresentation += $".{nameof(Call)}()";
            return this;
        }

        /// <summary>
        /// Add an action to execute when rule is applied.
        /// When all actions perfom without exception then authentication is successfull else it fail.
        /// </summary>
        /// <remarks>
        /// <para>Actions are executed in order of their definition</para>
        /// <para>A rule can have zero or many actions</para>
        /// </remarks>
        /// <param name="action">Action to execute</param>
        /// <returns><see cref="AuthenticationBuilder"/></returns>
        public AuthenticationBuilder Call(Func<AuthenticationContext, Task> action)
        {
            Rule.Actions.Add(MethodCall.FromFunction(action));
            Rule.StringRepresentation += $".{nameof(Call)}()";
            return this;
        }

        /// <summary>
        /// Add an action to execute when rule is applied
        /// When all actions perfom without exception then authentication is successfull else it fail.
        /// </summary>
        /// <remarks>
        /// <para>Actions are executed in order of their definition</para>
        /// <para>A rule can have zero or many actions</para>
        /// </remarks>
        /// <param name="action">Action to execute on the <typeparamref name="TService"/></param>
        /// <typeparam name="TService">Type of the service to resolve to execute action</typeparam>
        /// <returns><see cref="AuthenticationBuilder"/></returns>
        public AuthenticationBuilder Call<TService>(Action<TService, AuthenticationContext> action)
        {
            Rule.Actions.Add(MethodCall.FromFunction(action));
            Rule.StringRepresentation += $".{nameof(Call)}<{typeof(TService).Name}>()";
            return this;
        }

        /// <summary>
        /// Add an action to execute when rule is applied.
        /// When all actions perfom without exception then authentication is successfull else it fail.
        /// </summary>
        /// <remarks>
        /// <para>Actions are executed in order of their definition</para>
        /// <para>A rule can have zero or many actions</para>
        /// </remarks>
        /// <param name="action">Action to execute on the <typeparamref name="TService"/></param>
        /// <typeparam name="TService">Type of the service to resolve to execute action</typeparam>
        /// <returns><see cref="AuthenticationBuilder"/></returns>
        public AuthenticationBuilder Call<TService>(Func<TService, AuthenticationContext, Task> action)
        {
            Rule.Actions.Add(MethodCall.FromFunction(action));
            Rule.StringRepresentation += $".{nameof(Call)}<{typeof(TService).Name}>()";
            return this;
        }

        /// <summary>
        /// Set claims method to use after a successflull authentication for this rule.
        /// <para>When calling <see cref="IAuthenticationController.GetClaims{TClaims}"/> or <see cref="IAuthenticationController.GetClaimsOrDefault{TClaims}"/> 
        /// it will call <paramref name="getClaims"/> behind.</para>
        /// </summary>
        /// <remarks>
        /// <para>Multiple call to this method will overwrite claims method</para>
        /// </remarks>
        public AuthenticationBuilder GetClaimsUsing(Func<Type, object> getClaims)
        {
            Rule.GetClaims = MethodCall.FromFunction(getClaims);
            Rule.StringRepresentation += $".{nameof(GetClaimsUsing)}()";
            return this;
        }

        /// <summary>
        /// Set claims method to use after a successflull authentication for this rule.
        /// <para>When calling <see cref="IAuthenticationController.GetClaims{TClaims}"/> or <see cref="IAuthenticationController.GetClaimsOrDefault{TClaims}"/> 
        /// it will call <paramref name="getClaims"/> behind.</para>
        /// </summary>
        /// <remarks>
        /// <para>Multiple call to this method will overwrite claims method</para>
        /// </remarks>
        public AuthenticationBuilder GetClaimsUsing<TService>(Func<TService, Type, object> getClaims)
        {
            Rule.GetClaims = MethodCall.FromFunction(getClaims);
            Rule.StringRepresentation += $".{nameof(GetClaimsUsing)}<{typeof(TService).Name}>()";
            return this;
        }

        /// <summary>
        /// Set claims method to use after a successflull authentication for this rule.
        /// <para>When calling <see cref="IAuthenticationController.GetClaims{TClaims}"/> or <see cref="IAuthenticationController.GetClaimsOrDefault{TClaims}"/> 
        /// it will call <paramref name="getClaims"/> behind.</para>
        /// </summary>
        /// <remarks>
        /// <para>Multiple call to this method will overwrite claims method</para>
        /// </remarks>
        public AuthenticationBuilder GetClaimsUsing<TService1, TService2>(Func<TService1, TService2, Type, object> getClaims)
        {
            Rule.GetClaims = MethodCall.FromFunction(getClaims);
            Rule.StringRepresentation += $".{nameof(GetClaimsUsing)}<{typeof(TService1).Name}, {typeof(TService2).Name}>()";
            return this;
        }
    }
}
  