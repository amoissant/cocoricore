using CocoriCore.Common;
using CocoriCore.Types.Extension;
using System;
using System.Collections.Generic;

namespace CocoriCore.Authentication
{
    public class PrivateMessageOptionsBuilder
    {
        public List<PrivateMessageRule> Rules { get; }
        public bool DefaultIsPrivate { get; set; }

        public PrivateMessageOptionsBuilder()
        {
            Rules = new List<PrivateMessageRule>();
            DefaultIsPrivate = true;
        }

        public PrivateMessageOptions Options
        {
            get
            {
                var optionRules = new List<PrivateMessageRule>(Rules);
                return new PrivateMessageOptions(optionRules, DefaultIsPrivate);
            }
        }
        public PrivateMessageOptionsBuilder DefaultPrivate()
        {
            DefaultIsPrivate = true;
            return this;
        }

        public PrivateMessageOptionsBuilder DefaultPublic()
        {
            DefaultIsPrivate = false;
            return this;
        }

        public PrivateMessageOptionsBuilder Public<T>()
        {
            Func<Type, bool> action = t => t.IsAssignableTo<T>();
            var matchCall = MethodCall.FromFunction(action);
            var rule = new PrivateMessageRule(matchCall, false);
            Rules.Add(rule);
            return this;
        }

        public PrivateMessageOptionsBuilder Private<T>()
        {
            Func<Type, bool> action = t => t.IsAssignableTo<T>();
            var matchCall = MethodCall.FromFunction(action);
            var rule = new PrivateMessageRule(matchCall, true);
            Rules.Add(rule);
            return this;
        }

        public PrivateMessageOptionsBuilder Public(Func<Type, bool> matchCondition)
        {
            var matchCall = MethodCall.FromFunction(matchCondition);
            var rule = new PrivateMessageRule(matchCall, false);
            Rules.Add(rule);
            return this;
        }

        public PrivateMessageOptionsBuilder Private(Func<Type, bool> matchCondition)
        {
            var matchCall = MethodCall.FromFunction(matchCondition);
            var rule = new PrivateMessageRule(matchCall, true);
            Rules.Add(rule);
            return this;
        }

        public PrivateMessageOptionsBuilder Public<TService>(Func<TService, Type, bool> matchCondition)
        {
            var matchCall = MethodCall.FromFunction(matchCondition);
            var rule = new PrivateMessageRule(matchCall, false);
            Rules.Add(rule);
            return this;
        }

        public PrivateMessageOptionsBuilder Private<TService>(Func<TService, Type, bool> matchCondition)
        {
            var matchCall = MethodCall.FromFunction(matchCondition);
            var rule = new PrivateMessageRule(matchCall, true);
            Rules.Add(rule);
            return this;
        }
    }
}