﻿using System;
using System.Threading.Tasks;

namespace CocoriCore.Authentication
{
    /// <summary>
    /// Fake implementation of <see cref="IAuthenticationController"/> 
    /// that allow to manually set authenticated user and its claims.
    /// </summary>
    public class FakeAuthenticationController : IAuthenticationController
    {
        private object _claims;

        public FakeAuthenticationController()
        {
        }

        /// <inheritdoc cref="IAuthenticationController.IsAuthenticated"/>
        public bool IsAuthenticated => _claims != null;

        /// <inheritdoc cref="IAuthenticationController.AuthenticateAsync(object)"/>
        public Task AuthenticateAsync(object message)
        {
            return Task.CompletedTask;
        }


        /// <inheritdoc cref="IAuthenticationController.GetClaims{TClaims}"/>
        public TClaims GetClaims<TClaims>()
        {
            if (_claims == null)
            {
                throw new InvalidOperationException($"No user authenticated, call {nameof(UseClaims)}() before.");
            }
            return (TClaims)_claims;
        }

        /// <inheritdoc cref="IAuthenticationController.GetClaimsOrDefault{TClaims}"/>
        public TClaims GetClaimsOrDefault<TClaims>()
        {
            if (IsAuthenticated)
            {
                return GetClaims<TClaims>();
            }
            else
            {
                return default;
            }
        }

        /// <inheritdoc cref="IAuthenticationController.UseClaims"/>
        public void UseClaims(object claims)
        {
            _claims = claims;
        }
    }
}
