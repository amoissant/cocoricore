﻿using System;
using System.Threading.Tasks;
using CocoriCore.Common;
using Microsoft.Extensions.Logging;

namespace CocoriCore.Authentication
{
    /// <summary>
    /// Perform authentication from <see cref="AuthenticationContext"/> based on rules defined using <see cref="AuthenticationBuilder"/>.
    /// After a successfull authentication use <see cref="IAuthenticationController.GetClaims{TClaims}"/> to retreive claims into client code.
    /// <para>Rules are tested in the order of their definition and only the first matching rule is applied.</para>
    /// </summary>
    /// <remarks>Register this type in the scope of an unit of work.</remarks>
    public class AuthenticationController : IAuthenticationController
    {
        private readonly AuthenticationOptions _options;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger _logger;

        private Func<Type, object> _getClaims;
        private bool _isAuthenticated;
        private bool _isAuthenticating;

        public AuthenticationController(AuthenticationOptions options, IUnitOfWork unitOfWork, ILogger logger)
        {
            _options = options;
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        /// <inheritdoc cref="IAuthenticationController.AuthenticateAsync(object)"/>
        public async Task AuthenticateAsync(object message)
        {
            if (IsAuthenticated) return;

            var context = new AuthenticationContext(message);
            if (_options.BeforeAuthenticate != null)
            {
                await _options.BeforeAuthenticate.ExecuteAsync(_unitOfWork, context);
            }

            var matchingRule = await GetFirstMatchingRuleOrDefaultAsync(context);
            if (matchingRule != null)
            {
                _getClaims = t => matchingRule.GetClaims.Execute(_unitOfWork, t);
                try
                {
                    _isAuthenticating = true;
                    await RunActionsAsync(matchingRule, context);
                    _isAuthenticated = true;
                }
                finally
                {
                    _isAuthenticating = false;
                }
            }
        }

        private async Task<AuthenticationRule> GetFirstMatchingRuleOrDefaultAsync(AuthenticationContext context)
        {
            foreach (var rule in _options.Rules)
            {
                if (await rule.Condition.ExecuteAsync(_unitOfWork, context))
                {
                    return rule;
                }
            }
            return null;
        }

        private async Task RunActionsAsync(AuthenticationRule rule, AuthenticationContext context)
        {
            foreach (var action in rule.Actions)
            {
                await action.ExecuteAsync(_unitOfWork, context);
            }
        }

        /// <inheritdoc cref="IAuthenticationController.IsAuthenticated"/>
        public bool IsAuthenticated => _isAuthenticated;

        /// <inheritdoc cref="IAuthenticationController.GetClaims{TClaims}"/>
        public TClaims GetClaims<TClaims>()
        {
            if (!_isAuthenticating && !_isAuthenticated)
            {
                throw new InvalidOperationException($"Must be authenticated to call {nameof(GetClaims)}().\n" +
                    "Possible solutions :\n" +
                    $"- Ensure at least one rule match this context and call {nameof(AuthenticateAsync)}() before\n" +
                    $"- Manually set claims using {nameof(UseClaims)}()\n" +
                    $"- Use {nameof(GetClaimsOrDefault)} to get claims or default value when not authenticated\n" +
                    $"- Ensure {nameof(IsAuthenticated)} return true before calling {nameof(GetClaims)}()\n");
            }
            return (TClaims)_getClaims(typeof(TClaims));
        }

        /// <inheritdoc cref="IAuthenticationController.GetClaimsOrDefault{TClaims}"/>
        public TClaims GetClaimsOrDefault<TClaims>()
        {
            if(_isAuthenticating || _isAuthenticated)
            {
                return (TClaims)_getClaims(typeof(TClaims));
            }
            else
            {
                return default;
            }
        }

        /// <inheritdoc cref="IAuthenticationController.UseClaims"/>
        public void UseClaims(object claims)
        {
            _isAuthenticated = true;
            _getClaims = t => claims;
        }
    }
}