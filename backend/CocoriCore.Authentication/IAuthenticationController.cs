﻿using CocoriCore.Common;
using System;
using System.Threading.Tasks;

namespace CocoriCore.Authentication
{
    /// <summary>
    /// Authenticate message and get authentication state.
    /// </summary>
    /// <remarks>
    /// Available implementations are <see cref="AuthenticationController"/> for rest api 
    /// and <see cref="FakeAuthenticationController"/> for automated tests
    /// </remarks>
    public interface IAuthenticationController
    {
        /// <summary>
        /// Get whether authenticated or not.
        /// </summary>
        bool IsAuthenticated { get; }

        /// <summary>
        /// Authenticate message or throw exception (usually <see cref="AuthenticationException"/>) if authentication failed.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        Task AuthenticateAsync(object message);

        /// <summary>
        /// Get claims for current context or throw <see cref="InvalidOperationException"/> when no claims are present.
        /// </summary>
        /// <typeparam name="TClaims">Type of the claims</typeparam>
        /// <returns>Claims associated with context</returns>
        /// <exception cref="InvalidOperationException"></exception>
        TClaims GetClaims<TClaims>();

        /// <summary>
        /// Get claims for current context or default when no claims are present.
        /// </summary>
        /// <typeparam name="TClaims">Type of the claims</typeparam>
        /// <returns>Claims associated with context</returns>
        TClaims GetClaimsOrDefault<TClaims>();

        /// <summary>
        /// Manually set claims for the lifetime of the <see cref="IAuthenticationController"/>. 
        /// Next calls to <see cref="IsAuthenticated" /> will return true.
        /// Next calls to <see cref="GetClaims{TClaims}"/> or <see cref="GetClaimsOrDefault{TClaims}"/>
        /// will then return the provided <paramref name="claims"/>.
        /// </summary>
        /// <param name="claims">Claims to register</param>
        void UseClaims(object claims);
    }
}