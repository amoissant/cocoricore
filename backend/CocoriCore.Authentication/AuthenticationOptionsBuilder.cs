using CocoriCore.Common;
using System;
using System.Threading.Tasks;

namespace CocoriCore.Authentication
{
    /// <summary>
    /// Fluent builder for <see cref="AuthenticationOptions"/> used by <see cref="AuthenticationController"/>.
    /// </summary>
    public class AuthenticationOptionsBuilder
    {
        private readonly AuthenticationOptions _options;

        public AuthenticationOptionsBuilder()
        {
            _options = new AuthenticationOptions();
        }

        /// <summary>
        /// Get validated Option instance.
        /// </summary>
        public AuthenticationOptions Options
        {
            get
            {
                foreach(var rule in _options.Rules)
                {
                    if(rule.GetClaims == null)
                    {
                        throw new ConfigurationException($"The following rule has incomplete configuration:\n" +
                            $"{rule}\n" + 
                            $"Each rule must provide claims method with {nameof(AuthenticationBuilder.GetClaimsUsing)}().");
                    }
                }
                return _options;
            }
        }

        /// <summary>
        /// Register action to call immediatly after creating <see cref="AuthenticationContext"/> and before executing authentication rules.
        /// This action is called for all rules.
        /// </summary>
        /// <param name="action">The action to execute</param>
        public void BeforeAuthenticate(Action<AuthenticationContext> action)
        {
            _options.BeforeAuthenticate = MethodCall.FromFunction(action);
        }

        /// <summary>
        /// Register action to call immediatly after creating <see cref="AuthenticationContext"/> and before executing authentication rules.
        /// This action is called for all rules.
        /// </summary>
        /// <param name="action">The async action to execute</param>
        public void BeforeAuthenticate(Func<AuthenticationContext, Task> action)
        {
            _options.BeforeAuthenticate = MethodCall.FromFunction(action);
        }

        /// <summary>
        /// Register action to call immediatly after creating <see cref="AuthenticationContext"/> and before executing authentication rules.
        /// This action is called for all rules.
        /// </summary>
        /// <typeparam name="TService">The extra service to resolve to execute action</typeparam>
        /// <param name="action">The action to execute</param>
        public void BeforeAuthenticate<TService>(Action<TService, AuthenticationContext> action)
        {
            _options.BeforeAuthenticate = MethodCall.FromFunction(action);
        }

        /// <summary>
        /// Register action to call immediatly after creating <see cref="AuthenticationContext"/> and before executing authentication rules.
        /// This action is called for all rules.
        /// </summary>
        /// <typeparam name="TService">The extra service to resolve to execute action</typeparam>
        /// <param name="action">The async action to execute</param>
        public void BeforeAuthenticate<TService>(Func<TService, AuthenticationContext, Task> action)
        {
            _options.BeforeAuthenticate = MethodCall.FromFunction(action);
        }


        /// <summary>
        /// Create a new <see cref="AuthenticationRule"/> and define when it will be applied by <see cref="AuthenticationController"/>.
        /// <para>If condition return <c>true</c> then rule is applied.</para>
        /// </summary>
        /// <param name="condition">Lambda returning <c>true</c> when rule must apply</param>
        /// <returns><see cref="AuthenticationBuilder"/></returns>
        public AuthenticationBuilder When(Func<AuthenticationContext, bool> condition)
        {
            var rule = new AuthenticationRule(_options);
            rule.Condition = MethodCall.FromFunction(condition);
            rule.StringRepresentation += $"{nameof(When)}()";
            _options.AddRule(rule);
            return new AuthenticationBuilder(_options, rule);
        }

        /// <summary>
        /// Create a new <see cref="AuthenticationRule"/> and define when it will be applied by <see cref="AuthenticationController"/>.
        /// <para>If condition return <c>true</c> then rule is applied.</para>
        /// </summary>
        /// <param name="condition">Async lambda returning <c>true</c> when rule must apply</param>
        /// <returns><see cref="AuthenticationBuilder"/></returns>
        public AuthenticationBuilder When(Func<AuthenticationContext, Task<bool>> condition)
        {
            var rule = new AuthenticationRule(_options);
            rule.Condition = MethodCall.FromFunction(condition);
            rule.StringRepresentation += $"{nameof(When)}()";
            _options.AddRule(rule);
            return new AuthenticationBuilder(_options, rule);
        }

        /// <summary>
        /// Create a new <see cref="AuthenticationRule"/> and define when it will be applied by <see cref="AuthenticationController"/>.
        /// <para>If condition return <c>true</c> then rule is applied.</para>
        /// </summary>
        /// <param name="condition">Lambda using <typeparamref name="TService"/> and returning <c>true</c> when rule must apply</param>
        /// <typeparam name="TService">Service to resolve in order to execute <paramref name="condition"/></typeparam>
        /// <returns><see cref="AuthenticationBuilder"/></returns>
        public AuthenticationBuilder When<TService>(Func<TService, AuthenticationContext, bool> condition) where TService : class
        {
            var rule = new AuthenticationRule(_options);
            rule.Condition = MethodCall.FromFunction(condition);
            rule.StringRepresentation += $"{nameof(When)}<{typeof(TService).Name}>()";
            _options.AddRule(rule);
            return new AuthenticationBuilder(_options, rule);
        }

        /// <summary>
        /// Create a new <see cref="AuthenticationRule"/> and define when it will be applied by <see cref="AuthenticationController"/>.
        /// <para>If condition return <c>true</c> then rule is applied.</para>
        /// </summary>
        /// <param name="condition">Async lambda using <typeparamref name="TService"/> and returning <c>true</c> when rule must apply</param>
        /// <typeparam name="TService">Service to resolve in order to execute <paramref name="condition"/></typeparam>
        /// <returns><see cref="AuthenticationBuilder"/></returns>
        public AuthenticationBuilder When<TService>(Func<TService, AuthenticationContext, Task<bool>> condition) where TService : class
        {
            var rule = new AuthenticationRule(_options);
            rule.Condition = MethodCall.FromFunction(condition);
            rule.StringRepresentation += $"{nameof(When)}<{typeof(TService).Name}>()";
            _options.AddRule(rule);
            return new AuthenticationBuilder(_options, rule);
        }
    }
}