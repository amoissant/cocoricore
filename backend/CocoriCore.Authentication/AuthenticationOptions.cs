using CocoriCore.Common;
using System.Collections.Generic;

namespace CocoriCore.Authentication
{
    /// <summary>
    /// Options class for <see cref="AuthenticationController"/>.
    /// Use <see cref="AuthenticationOptionsBuilder"/> to build this object.
    /// </summary>
    public class AuthenticationOptions : Dictionary<string, object>
    {
        private readonly List<AuthenticationRule> _rules;
        public IMethodCall<AuthenticationContext, object> BeforeAuthenticate { get; set; }

        public AuthenticationOptions()
        {
            _rules = new List<AuthenticationRule>();
        }

        public AuthenticationOptions AddRule(AuthenticationRule rule)
        {
            _rules.Add(rule);
            return this;
        }

        public IList<AuthenticationRule> Rules => _rules;
    }
}