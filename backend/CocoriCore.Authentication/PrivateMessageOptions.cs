using CocoriCore.Common;
using System;
using System.Collections.Generic;

namespace CocoriCore.Authentication
{
    /// <summary>
    /// Options class for <see cref="PrivateMessageController"/>.
    /// Use <see cref="PrivateMessageOptionsBuilder"/> to build this object.
    /// </summary>
    public class PrivateMessageOptions
    {
        public List<PrivateMessageRule> Rules { get; }
        public bool DefaultIsPrivate { get; }

        public PrivateMessageOptions(List<PrivateMessageRule> rules, bool defaultIsPrivate)
        {
            Rules = rules;
            DefaultIsPrivate = defaultIsPrivate;
        }
    }

    public class PrivateMessageRule
    {
        public PrivateMessageRule(IMethodCall<Type, bool> matchCall, bool isPrivate)
        {
            MatchCall = matchCall;
            IsPrivate = isPrivate;
        }

        public IMethodCall<Type, bool> MatchCall { get; }
        public bool IsPrivate { get; }
    }
}

