﻿using CocoriCore.Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace CocoriCore.Authentication
{
    [DebuggerDisplay("{ToString()}")]
    public class AuthenticationRule : Dictionary<string, object>
    {
        public AuthenticationOptions Options { get; }
        public string StringRepresentation { get; set; }
        public IMethodCall<AuthenticationContext, bool> Condition { get; set; }
        public List<IMethodCall<AuthenticationContext, object>> Actions { get; set; }
        public IMethodCall<Type, object> GetClaims { get; set; }

        public AuthenticationRule(AuthenticationOptions options)
        {
            Options = options;
            Actions = new List<IMethodCall<AuthenticationContext, object>>();
        }

        public override string ToString()
        {
            return StringRepresentation;
        }
    }
}