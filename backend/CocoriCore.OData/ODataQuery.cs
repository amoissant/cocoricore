﻿namespace CocoriCore.OData.Linq
{
    public class ODataQuery : IODataQuery
    {
        public int Top { get; set; }
        public bool Distinct { get; set; }
        public string Filter { get; set; }
        public ODataOrderBy OrderBy { get; set; }
        public ODataSelect Select { get; set; }
        public int Skip { get; set; }
    }
}