using System;
using CocoriCore.OData.Linq;
using Newtonsoft.Json;

namespace CocoriCore.OData.Converter
{
    public class ODataOrderByConverter : JsonConverter
    {
        public override bool CanWrite => false;
        public override bool CanRead => true;

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(ODataOrderBy);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, Newtonsoft.Json.JsonSerializer serializer)
        {
            var value = reader.Value;
            try
            {
                var parts = ((string)value).Split(' ');
                var result = new ODataOrderBy();
                result.Member = parts[0];
                if (parts.Length > 1)
                {
                    var direction = parts[1].ToLower();
                    result.Direction = (OrderDirection)Enum.Parse(typeof(OrderDirection), direction, true);
                }
                return result;
            }
            catch (Exception e)
            {
                var lineInfo = reader as IJsonLineInfo;
                throw new JsonSerializationException($"Error converting value \"{value}\" to type '{objectType}'."
                    + $" Path '{reader.Path}', line {lineInfo.LineNumber}, position {lineInfo.LinePosition}.", e);
            }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
        }
    }
}