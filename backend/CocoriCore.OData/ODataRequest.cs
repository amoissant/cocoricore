﻿using System;
using System.Linq;
using System.Threading.Tasks;
using CocoriCore.Common;
using CocoriCore.Linq.Async;
using CocoriCore.Reflection.Extension;
using CocoriCore.Types.Extension;

namespace CocoriCore.OData.Linq
{
    public class ODataRequest<TEntity, TResponse>
    {
        public IQueryable<TEntity> Queryable { get; set; }
        public ODataFilter<TEntity> Filter { get; set; }
        public ODataSelect<TEntity, TResponse> Select { get; set; }
        public ODataOrderBy OrderBy { get; set; }
        public int Skip { get; set; }
        public int Top { get; set; }
        public bool Distinct { get; set; }
        public ODataOptions Options { get; set; }

        public ODataRequest(IQueryable<TEntity> queryable, ODataOptions options, string filter, ODataSelect select,
            ODataOrderBy orderBy, int skip, int top, bool distinct)
            : this(queryable, options, filter, new ODataSelect<TEntity, TResponse>(select?.Members), orderBy, skip, top, distinct)
        {
        }

        public ODataRequest(IQueryable<TEntity> queryable, ODataOptions options, string filter, ODataSelect<TEntity, TResponse> select,
            ODataOrderBy orderBy, int skip, int top, bool distinct)
        {
            try
            {
                var filterExpression = new ODataParser().GetFilterExpression(queryable, filter);
                Queryable = queryable;
                Options = options ?? new ODataOptions();
                Filter = new ODataFilter<TEntity>(filterExpression);
                Select = select;
                OrderBy = GetOrderByWhenDistinct(orderBy, distinct, select);
                Skip = skip;
                Top = top == 0 ? 100 : top;//TODO paramétrable dans les options ?
                Distinct = distinct;
                if (Options.ForceDistinctWhenSelectOneField && !Distinct)
                {
                    Distinct = Select?.Members?.Length == 1;
                }
            }
            catch(TypeInitializationException e) 
            {
                if(e.Message.Contains("Microsoft.AspNet.OData.Adapters.WebApiAssembliesResolver"))
                {
                    var errorMessage = "Try add <FrameworkReference Include=\"Microsoft.AspNetCore.App\"/> " + 
                        "in your project's .csproj into <ItemGroup> section to fix the inner exception.";
                    throw new ConfigurationException(errorMessage, e);
                }
                throw;
            }
        }

        private ODataOrderBy GetOrderByWhenDistinct(ODataOrderBy orderBy, bool distinct, ODataSelect<TEntity, TResponse> select)
        {
            if (orderBy == null && distinct)
            {
                var arbitraryOrderMember = typeof(TResponse)
                    .GetPropertiesAndFields()
                    .First(x => x.GetMemberType() != typeof(bool?))
                    .Name;
                var orderField = select.Members?.First() ?? arbitraryOrderMember;
                orderBy = new ODataOrderBy(OrderDirection.Asc, orderField);
            }
            return orderBy;
        }

        public IQueryable<TResponse> CommonRequest => Queryable
            .Where(Filter)
            .Select(Select)
            .Distinct(Distinct);

        public IQueryable<TResponse> ResultRequest => CommonRequest
            .OrderBy(OrderBy)
            .Skip(Skip)
            .Top(Top);

        public ODataRequest<TEntity, TResponse> Configure(Action<ODataRequest<TEntity, TResponse>> modification)
        {
            modification.Invoke(this);
            return this;
        }

        public async Task<ODataResponse<TResponse>> GetResultAsync()
        {
            var count = await CommonRequest.CountAsync();
            var results = await ResultRequest.ToArrayAsync();
            return new ODataResponse<TResponse>()
            {
                Results = results,
                Count = count
            };
        }
    }
}