using CocoriCore.Expressions.Extension;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace CocoriCore.OData.Linq
{
    public class ODataSelect
    {
        public string[] Members { get; }

        public ODataSelect(params string[] members)
        {
            Members = members;
        }
    }

    public class ODataSelect<TSource, TDestination>
    {
        public string[] Members { get; protected set; }
        public MemberMapping[] Mappings { get; protected set; }

        public ODataSelect(params string[] members)
        {
            Members = members;
            Mappings = new MemberMapping[0];
        }

        public bool SelectAll => Members == null || Members.Length == 0;

        public ODataSelect<TSource, TDestination> Map(Expression<Func<TSource, object>> sourceMember, Expression<Func<TDestination, object>> destinationMember)
        {
            var mapping = new MemberMapping(sourceMember.GetMemberInfo(), destinationMember.GetMemberInfo());
            Mappings = Mappings.Append(mapping).ToArray();
            return this;
        }
    }
}