﻿using System.Collections;

namespace CocoriCore.OData.HttpResponse
{
    public class DevExtremODataResponse
    {
        public InnerResponse d;

        public DevExtremODataResponse(int count, IEnumerable enumerable)
        {
            d = new InnerResponse
            {
                __count = count.ToString(),
                results = enumerable
            };
        }

        public class InnerResponse
        {
            public string __count;
            public IEnumerable results;
        }
    }
}
