using System.Threading.Tasks;
using CocoriCore.OData.Linq;
using Newtonsoft.Json;

namespace CocoriCore.OData.HttpResponse
{
    public class HttpResponseWriterODataHandler : HttpResponseWriterBase, IHttpReponseWriterHandler
    {
        public HttpResponseWriterODataHandler(JsonSerializer jsonSerializer)
           : base(jsonSerializer)
        { }

        public async Task WriteResponseAsync(HttpResponseWriterContext context)
        {
            var response = (IODataResponse)context.Response;
            var devExtremResponse = new DevExtremODataResponse(response.Count, response.Results);
            await WriteResponseAsync(context.HttpResponse, devExtremResponse);
        }
    }
}