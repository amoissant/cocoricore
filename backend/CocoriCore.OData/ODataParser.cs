﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.OData.UriParser;
using Microsoft.OData.Edm;
using System.Collections;
using CocoriCore.Types.Extension;
using CocoriCore.Reflection.Extension;
using Microsoft.AspNetCore.OData.Query;
using Microsoft.OData.ModelBuilder;

namespace CocoriCore.OData
{
    public class ODataParser
    {
        public Expression<Func<TEntity, bool>> GetFilterExpression<TEntity>(IQueryable<TEntity> queryable, string filter)
        {
            if (string.IsNullOrEmpty(filter))
            {
                return null;
            }
            var entityType = typeof(TEntity);
            var model = ConstructEdmModel(entityType);
            var queryOption = ConstructQueryOption(filter, model, entityType);
            var query = queryOption.ApplyTo(queryable, new ODataQuerySettings());
            return ExtractFilterExpression<TEntity>(query);
        }

        private IEdmModel ConstructEdmModel(Type entityType)//TODO mettre en cache les modèles créés
        {
            var odataModelBuilder = new ODataModelBuilder();
            var entityModel = odataModelBuilder.AddEntityType(entityType);

            var primitiveTypes = new Type[] { typeof(string), typeof(Guid), typeof(DateTime) };

            foreach (var property in entityType.GetProperties())
            {
                //TODO a tester et rendre plus configurable ?
                var memberType = property.GetMemberType();
                var notNullMemberType = memberType.UnboxIfNullable();
                var unboxedMemberType = memberType.Unbox();
                if (unboxedMemberType.IsEnum)
                {
                    var enumType = odataModelBuilder.AddEnumType(unboxedMemberType);
                    foreach(var value in unboxedMemberType.GetEnumValues())
                    {
                        enumType.AddMember((Enum)value);
                    }
                }
                if (notNullMemberType.IsEnum)
                {
                    entityModel.AddEnumProperty(property);
                }
                else if(notNullMemberType.IsPrimitive || primitiveTypes.Contains(notNullMemberType))
                {
                    entityModel.AddProperty(property);
                }
                else if (memberType.IsAssignableTo<IEnumerable>())
                {
                    entityModel.AddCollectionProperty(property);
                }
                else
                {
                    entityModel.AddComplexProperty(property);
                }
            }
            var edmModel = odataModelBuilder.GetEdmModel();
            return edmModel;
        }

        private FilterQueryOption ConstructQueryOption(string filter, IEdmModel model, Type entityType)
        {
            var context = new ODataQueryContext(model, entityType, null);
            var parser = new ODataQueryOptionParser(
                context.Model,
                context.ElementType,
                context.NavigationSource,
                new Dictionary<string, string> { { "$filter", filter } },
                context.RequestContainer);
            parser.Resolver = new StringOrIntegerAsEnumResolver();
            parser.Resolver.EnableCaseInsensitive = true;
            var filterQueryOptions = new FilterQueryOption(filter, context, parser);
            return filterQueryOptions;
        }

        private Expression<Func<TEntity, bool>> ExtractFilterExpression<TEntity>(IQueryable queryable)
        {
            var whereExpression = (MethodCallExpression)queryable.Expression;
            var quotedFilter = (UnaryExpression)whereExpression.Arguments[1];
            return (Expression<Func<TEntity, bool>>)quotedFilter.Operand;
        }
    }
}
