using CocoriCore.Collection.Extensions;
using CocoriCore.Reflection.Extension;
using CocoriCore.Types.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace CocoriCore.OData.Linq
{
    public static class ODataQueryableExtension
    {
        private static BindingFlags _oDataBindingFlags = BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.Public;

        public static ODataRequest<TEntity, TEntity> ToODataRequest<TEntity>(this IQueryable<TEntity> queryable, IODataQuery query, ODataOptions options = null)
           where TEntity : class
        {
            return queryable.ToODataRequest<TEntity, TEntity>(query, options);
        }

        public static ODataRequest<TEntity, TResponse> ToODataRequest<TEntity, TResponse>(this IQueryable<TEntity> queryable, IODataQuery query, ODataOptions options = null)
            where TEntity : class
            where TResponse : class
        {
            return new ODataRequest<TEntity, TResponse>(queryable, options, query.Filter, query.Select, query.OrderBy, query.Skip, query.Top, query.Distinct);
        }

        public static IQueryable<T> OrderBy<T>(this IQueryable<T> queryable, ODataOrderBy order)
        {
            if (order == null)
                return queryable;
            var memberInfo = typeof(T).GetPropertyOrField(order.Member, _oDataBindingFlags);
            if (order.Direction == OrderDirection.Asc)
            {
                return queryable.OrderBy(memberInfo);
            }
            else if (order.Direction == OrderDirection.Desc)
            {
                return queryable.OrderByDescending(memberInfo);
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        public static IQueryable<T> Where<T>(this IQueryable<T> queryable, ODataFilter<T> filter)
        {
            if (filter == null || filter.IsEmpty)
                return queryable;
            return queryable.Where(filter.Expression);
        }

        public static IQueryable<T> Filter<T>(this IQueryable<T> queryable, string odataFilter)
        {
            var filterExpression = new ODataParser().GetFilterExpression(queryable, odataFilter);
            if (filterExpression != null)
            {
                return queryable.Where(filterExpression);
            }
            else
            {
                return queryable;
            }
        }

        public static IQueryable<TEntity> Select<TEntity>(this IQueryable<TEntity> queryable, ODataSelect select = null)
        {
            return queryable.Select(new ODataSelect<TEntity, TEntity>(select?.Members));
        }

        public static IQueryable<TResponse> Select<TEntity, TResponse>(this IQueryable<TEntity> queryable, ODataSelect select = null)
        {
            return queryable.Select(new ODataSelect<TEntity, TResponse>(select?.Members));
        }

        public static IQueryable<TResponse> Select<TEntity, TResponse>(this IQueryable<TEntity> queryable, ODataSelect<TEntity, TResponse> select)
        {
            var entityType = typeof(TEntity);
            var responseType = typeof(TResponse);
            var mappings = GetMemberMappings(select);

            var paramaterExpression = Expression.Parameter(entityType, "x");
            var newResponseExpression = Expression.New(responseType);
            var assignments = mappings.Select(x => ConstructAssignmentExpression(x.Source, x.Destination, paramaterExpression));
            var initExpression = Expression.MemberInit(newResponseExpression, assignments);
            var selectExpression = Expression.Lambda<Func<TEntity, TResponse>>(initExpression, paramaterExpression);

            return queryable.Select(selectExpression);
        }

        //TODO les assignations doivent �tre utilis� pour le order by aussi et les filter ?
        private static IEnumerable<MemberMapping> GetMemberMappings<TEntity, TResponse>(ODataSelect<TEntity, TResponse> select)
        {
            var selectResponseMembers = GetSelectResponseMembers(select);
            var selectMappings = new List<MemberMapping>();
            var mappings = select.Mappings.ToDictionary(x => x.Destination.Name);
            var sourceMembers = typeof(TEntity)
                .GetPropertiesAndFields(_oDataBindingFlags)
                .ToDictionary(x => x.Name, x => x);
            foreach (var responseMember in selectResponseMembers)
            {
                var selectMapping = GetSelectMapping<TEntity>(mappings, sourceMembers, responseMember);
                selectMappings.Add(selectMapping);
            }
            return selectMappings;
        }

        private static List<MemberInfo> GetSelectResponseMembers<TEntity, TResponse>(ODataSelect<TEntity, TResponse> select)
        {
            List<MemberInfo> selectResponseMembers;
            if (select.SelectAll)
            {
                //TODO ici si la réponse contient plus de champs que l'entité on voudrai que ca marche quand meme 
                //-> permet de renvoyer au front des champs calculés par exmple même si ces champs ne seront pas requêtables.
                selectResponseMembers = typeof(TResponse)
                    .GetPropertiesAndFields(_oDataBindingFlags)
                    .ToList();
            }
            else
            {
                selectResponseMembers = select
                    .Members
                    .Select(x => typeof(TResponse).GetPropertyOrField(x, _oDataBindingFlags))
                    .ToList();
            }
            return selectResponseMembers;
        }

        private static MemberMapping GetSelectMapping<TEntity>(Dictionary<string, MemberMapping> mappings,
            Dictionary<string, MemberInfo> sourceMembers, MemberInfo responseMember)
        {
            var selectMapping = mappings.GetValueOrDefault(responseMember.Name);
            if (selectMapping == null)
            {
                if (sourceMembers.TryGetValue(responseMember.Name, out MemberInfo sourceMember))
                {
                    selectMapping = new MemberMapping(sourceMember, responseMember);
                }
                else
                {
                    var errorMessage = $"Unable to select '{responseMember.Name}' from {typeof(TEntity)} : no member found matching this name.\n" +
                        $"Review your data structure or define a corresponding member in {typeof(TEntity)} using {nameof(ODataRequest<object, object>.Configure)}().";
                    throw new InvalidOperationException(errorMessage);
                }
            }
            return selectMapping;
        }

        private static MemberAssignment ConstructAssignmentExpression(MemberInfo entityMemberInfo, MemberInfo responseMemberInfo,
            ParameterExpression paramaterExpression)
        {
            var propertyExpression = (Expression)Expression.MakeMemberAccess(paramaterExpression, entityMemberInfo);
            if (entityMemberInfo.GetMemberType() != responseMemberInfo.GetMemberType())
                propertyExpression = Expression.Convert(propertyExpression, responseMemberInfo.GetMemberType());
            return Expression.Bind(responseMemberInfo, propertyExpression);
        }
        
        public static IQueryable<T> Distinct<T>(this IQueryable<T> queryable, bool distinct)
        {
            if (distinct)
            {
                return queryable.Distinct();
            }
            else
            {
                return queryable;
            }
        }

        public static IQueryable<T> Top<T>(this IQueryable<T> queryable, int count)
        {
            return queryable.Take(count > 0 ? count : 0);
        }
    }
}