﻿namespace CocoriCore.OData.Linq
{
    public class ODataOptions
    {
        public bool ForceDistinctWhenSelectOneField { get; set; }
    }
}