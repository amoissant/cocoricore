﻿namespace CocoriCore.OData.Linq
{
    public interface IODataQuery
    {
        int Top { get; }
        bool Distinct { get; }
        string Filter { get; }
        ODataOrderBy OrderBy { get; }
        ODataSelect Select { get; }
        int Skip { get; }
    }
}