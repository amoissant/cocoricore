﻿using System.Reflection;

namespace CocoriCore.OData.Linq
{
    public class MemberMapping
    {
        public MemberMapping(MemberInfo source, MemberInfo destination)
        {
            Source = source;
            Destination = destination;
        }

        public MemberInfo Source { get; }
        public MemberInfo Destination { get; }
    }
}