﻿using CocoriCore.Reflection.Extension;
using System;
using System.Linq.Expressions;

namespace CocoriCore.OData.Linq
{
    public class StringValuesVisitor : ExpressionVisitor 
    {
        private Func<string, string> _valueTransformation;

        public StringValuesVisitor(Func<string, string> valueTransformation)
        {
            _valueTransformation = valueTransformation;
        }

        protected override Expression VisitConstant(ConstantExpression node)
        {
            if(node.Value is string stringValue)
            {
                return Expression.Constant(_valueTransformation(stringValue));
            }
            return base.VisitConstant(node);
        }

        protected override Expression VisitMember(MemberExpression node)
        {
            if (node.Member.Name == "TypedProperty" && 
                node.Member.DeclaringType.Name.StartsWith("TypedLinqParameterContainer") == true &&
                node.Member.DeclaringType.GetGenericArguments()[0] == typeof(string))
            {
                var constantExpresssion = (ConstantExpression)node.Expression;
                var parameterizedValue = (string)constantExpresssion.Value.GetType().GetProperty("TypedProperty").InvokeGetter(constantExpresssion.Value);
                return Expression.Constant(_valueTransformation(parameterizedValue));
            }
            return base.VisitMember(node);
        }
    }
}