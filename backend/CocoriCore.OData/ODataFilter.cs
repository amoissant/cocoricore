using CocoriCore.Expressions.Extension;
using System;
using System.Linq.Expressions;

namespace CocoriCore.OData.Linq
{
    public class ODataFilter<T>
    {
        private Expression<Func<T, bool>> _expression;

        public ODataFilter(Expression<Func<T, bool>> expression)
        {
            _expression = expression;
        }

        public Expression<Func<T, bool>> Expression => _expression;

        public bool IsEmpty => _expression == null;

        public ODataFilter<T> TransformValues(Func<string, string> stringValuesTransformation)
        {
            var visitor = new StringValuesVisitor(stringValuesTransformation);
            _expression = visitor.VisitAndConvert(_expression, $"{nameof(ODataFilter<T>)}.{nameof(ODataFilter<T>.TransformValues)}");
            return this;
        }

        public ODataFilter<T> ReplaceField<TValue>(Expression<Func<T, TValue>> originalField, Expression<Func<T, TValue>> replacementField)
        {
            var visitor = new ReplaceFieldVisitor(originalField.GetMemberInfo(), replacementField.GetMemberInfo());
            _expression = visitor.VisitAndConvert(_expression, $"{nameof(ODataFilter<T>)}.{nameof(ODataFilter<T>.ReplaceField)}");
            return this;
        }
    }
}