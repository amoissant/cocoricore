﻿using System.Linq.Expressions;
using System.Reflection;

namespace CocoriCore.OData.Linq
{
    public class ReplaceFieldVisitor : ExpressionVisitor
    {
        private MemberInfo _originalField;
        private MemberInfo _replacementField;

        public ReplaceFieldVisitor(MemberInfo originalField, MemberInfo replacementField)
        {
            _originalField = originalField;
            _replacementField = replacementField;
        }

        protected override Expression VisitMember(MemberExpression node)
        {
            if(node.Member == _originalField)
            {
                return Expression.MakeMemberAccess(node.Expression, _replacementField);
            }
            return base.VisitMember(node);
        }
    }
}