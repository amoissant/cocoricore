using System.Threading.Tasks;
using Autofac;
using CocoriCore.AspNetCore;
using CocoriCore.Common;
using Microsoft.AspNetCore.Http;

namespace CocoriCore.Middleware
{
    public class HttpContextScopeMiddleware : IMiddleware
    {
        private ILifetimeScope _rootScope;

        public HttpContextScopeMiddleware(ILifetimeScope rootScope)
        {
            _rootScope = rootScope;
        }

        public virtual async Task InvokeAsync(HttpContext httpContext, RequestDelegate next)
        {
            using (var unitOfWork = _rootScope.BeginLifetimeScope().Resolve<IUnitOfWork>())
            {
                httpContext.SetUnitOfWork(unitOfWork);
                unitOfWork.Resolve<IHttpContextProvider>().HttpContext = httpContext;
                await next(httpContext);
                await unitOfWork.FinishAsync();
            }
        }
    }
}