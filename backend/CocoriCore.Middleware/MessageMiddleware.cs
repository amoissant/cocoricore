﻿using System.Threading.Tasks;
using CocoriCore.AspNetCore;
using CocoriCore.Messaging;
using CocoriCore.Router;
using Microsoft.AspNetCore.Http;

namespace CocoriCore.Middleware
{
    public class MessageMiddleware : IMiddleware
    {
        private IRouter _router;

        public MessageMiddleware(IRouter router)
        {
            _router = router;
        }

        public virtual async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            var unitOfWork = context.GetUnitOfWork();
            var messageType = _router.TryFindMessageType(context.Request);
            if (messageType != null)
            {
                var messageBus = unitOfWork.Resolve<IMessageBus>();
                var message = await _router.DeserializeMessageAsync<object>(context.Request);
                var response = await messageBus.ExecuteAsync(message);
                var responseWriter = unitOfWork.Resolve<IHttpResponseWriter>();
                await responseWriter.WriteResponseAsync(response, context.Response);
            }
            else
            {
                await next(context);
            }
        }
    }
}