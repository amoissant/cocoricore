﻿using CocoriCore.Expressions.Extension;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace CocoriCore.EFCore
{
    public static class EFCoreExtensions
    {
        internal static readonly MethodInfo StringIncludeMethodInfo
            = typeof(Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions)
                .GetTypeInfo().GetDeclaredMethods(nameof(Include))
                .Single(
                    mi => mi.GetParameters().Any(
                        pi => pi.Name == "navigationPropertyPath" && pi.ParameterType == typeof(string)));

        public static IQueryable<TEntity> Include<TEntity>(this IQueryable<TEntity> source, Expression<Func<TEntity, object>> navigationProperty) where TEntity : class
        {
            var navigationPropertyPath = navigationProperty.GetMemberInfo().Name;
            return source.Provider.CreateQuery<TEntity>(Expression.Call(null, StringIncludeMethodInfo.MakeGenericMethod(typeof(TEntity)), source.Expression, Expression.Constant(navigationPropertyPath)));
        }
    }
}