﻿using Microsoft.EntityFrameworkCore.Diagnostics;
using System.Data.Common;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace CocoriCore.EFCore
{
    public class RowVersionCommandInterceptor : DbCommandInterceptor
    {
        private Regex _regex = new Regex(".*?SET.*?RowVersion.*?(?<parameter>@p\\d+)", RegexOptions.Compiled);

        public override ValueTask<InterceptionResult<DbDataReader>> ReaderExecutingAsync(DbCommand command,
            CommandEventData eventData, InterceptionResult<DbDataReader> result, CancellationToken cancellationToken = default)
        {
            var match = _regex.Match(command.CommandText);
            if (match.Success)
            {
                var parameterName = match.Groups["parameter"].Value;
                command.Parameters[parameterName].Value = (int)command.Parameters[parameterName].Value + 1;
            }
            return base.ReaderExecutingAsync(command, eventData, result, cancellationToken);
        }
    }
}
