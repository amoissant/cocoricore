﻿using CocoriCore.Common;
using CocoriCore.Repository;
using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace CocoriCore.EFCore
{
    public class EFCoreAsyncQueryProvider<T> : 
        IAsyncEnumerable<T>,
        IOrderedQueryable<T>,
        IQueryProvider, 
        ICocoriCoreQueryProvider
    {
        private static MethodInfo _executeAsyncMethod = typeof(IAsyncQueryProvider)
            .GetMethods()
            .Where(x =>
                x.Name == nameof(IAsyncQueryProvider.ExecuteAsync) &&
                x.GetParameters().Count() == 2)
            .Single();

        private IQueryable _queryable;
        private Expression _expression;

        public EFCoreAsyncQueryProvider(IQueryable<T> queryable)
            : this((IQueryable)queryable)
        {
        }

        private EFCoreAsyncQueryProvider(IQueryable queryable)
        {
            _queryable = queryable;
            _expression = queryable.Expression;
        }

        IAsyncEnumerator<T> IAsyncEnumerable<T>.GetAsyncEnumerator(CancellationToken cancellationToken)
        {
            return ((IAsyncEnumerable<T>)_queryable).GetAsyncEnumerator();
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return ((IEnumerable<T>)_queryable).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _queryable.GetEnumerator();
        }

        public IQueryable CreateQuery(Expression expression)
        {
            return new EFCoreAsyncQueryProvider<T>(_queryable.Provider.CreateQuery(expression));
        }

        public IQueryable<TResult> CreateQuery<TResult>(Expression expression)
        {
            return new EFCoreAsyncQueryProvider<TResult>(_queryable.Provider.CreateQuery<TResult>(expression));
        }

        public object Execute(Expression expression)
        {
            return _queryable.Provider.Execute(expression);
        }

        public TResult Execute<TResult>(Expression expression)
        {
            return _queryable.Provider.Execute<TResult>(expression);
        }

        public IAsyncEnumerable<TResult> AsAsyncEnumerable<TResult>(Expression expression)
        {
            return (IAsyncEnumerable<TResult>)_queryable;
        }

        public async Task<object> ExecuteAsync(Expression expression, CancellationToken cancellationToken)
        {
            var task = (Task)_executeAsyncMethod
                .MakeGenericMethod(typeof(Task<>).MakeGenericType(expression.Type))
                .Invoke(_queryable.Provider, new object[] { expression, cancellationToken });
            return await task.GetResultAsync<object>();
        }

        public Task<TResult> ExecuteAsync<TResult>(Expression expression, CancellationToken cancellationToken)
        {
            //TODO ici catcher l'erreur (mais aussi dans GetAsyncEnumerableAsync()) et ajouter dans l'exception la requête exécutée
            return ((IAsyncQueryProvider)_queryable.Provider).ExecuteAsync<Task<TResult>>(expression, cancellationToken);
        }

        public Type ElementType => typeof(T);

        public Expression Expression => _expression;

        public IQueryProvider Provider => this;
    }
}
