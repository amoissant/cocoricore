using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Caching.Memory;
using System.Diagnostics;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using CocoriCore.Common;
using CocoriCore.Repository;
using CocoriCore.Types.Extension;
using CocoriCore.Reflection.Extension;
using CocoriCore.Expressions.Extension;

namespace CocoriCore.EFCore
{
    public class EFCoreBaseRepository : IBaseRepository, ITransactionHolder
    {
        private static MethodInfo _buildQueryableMethod = typeof(EFCoreBaseRepository)
            .GetMethods(BindingFlags.NonPublic | BindingFlags.Instance)
            .Where(x =>
                x.Name == nameof(BuildQueryable) &&
                x.GetParameters().Count() == 0)
            .Single();
        private static MethodInfo _queryMethod = typeof(EFCoreBaseRepository)
            .GetMethods()
            .Where(x =>
                x.Name == nameof(Query) &&
                x.GetParameters().Length == 0 &&
                x.GetGenericArguments().Length == 1)
            .Single();

        private readonly DbContext _dbContext;
        private readonly ILogger _logger;
        private readonly IUnitOfWork _unitOfWork;
        private readonly bool _detectChangesUsingStateStore;
        private readonly IMemoryCache _memoryCache;

        private IDbContextTransaction _transaction;

        public EFCoreBaseRepository(DbContext dbContext, ILogger logger, IUnitOfWork unitOfWork, 
            IMemoryCache memoryCache, bool detectChangesUsingStateStore = false)
        {
            _dbContext = dbContext;
            _unitOfWork = unitOfWork;
            _logger = logger;
            _detectChangesUsingStateStore = detectChangesUsingStateStore;
            _memoryCache = memoryCache;
            _dbContext.ChangeTracker.AutoDetectChangesEnabled = false;
            _dbContext.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        public virtual async Task<IEnumerable<IEntity>> LoadAsync(Type type, Guid id)
        {
            var entity = await FindAsync(type, id);
            if (entity != null)
            {
                return new[] { (IEntity)entity };
            }
            else
                return new IEntity[0];
        }

        protected virtual async Task<object> FindAsync(Type entityType, Guid id)
        {
            IEntity entity = null;
            if (!entityType.IsConcrete())
            {
                foreach (var type in GetConcreteTypes(entityType))
                {
                    entity = (IEntity)await _dbContext.FindAsync(type, id);
                    if (entity != null)
                    {
                        break;
                    }
                }
            }
            else
            {
                entity = (IEntity)await _dbContext.FindAsync(entityType, id);
            }
            return Detach(entity);
        }

        private IEnumerable<Type> GetConcreteTypes(Type entityType)
        {
            return _memoryCache.GetCached(this, x => x.GetConcreteTypesInternal(entityType));
        }

        protected virtual IEnumerable<Type> GetConcreteTypesInternal(Type entityType)
        {
            if (entityType.IsInterface)
            {
                var concreteTypes = _dbContext.Model
                    .GetEntityTypes()
                    .Select(x => x.ClrType)
                    .Where(x => x.IsConcrete() && x.IsAssignableTo(entityType))
                    .ToArray();
                if (concreteTypes.Count() == 0)
                {
                    throw new InvalidOperationException($"Model contains no entity type assignable to {entityType}.");
                }
                return concreteTypes;
            }
            else
            {
                return new[] { entityType };
            }
        }

        public virtual async Task<IEnumerable<IEntity>> LoadAsync(Type type, MemberInfo uniqueMember, object value)
        {
            var parameter = Expression.Parameter(type, "x");
            var predicate = Expression.MakeMemberAccess(parameter, uniqueMember).Equal(value);
            return await Query(type)
                .Where(predicate, parameter)
                .Cast<IEntity>()
                .ToArrayAsync();
        }

        public virtual async Task<IEnumerable<IEntity>> LoadAsync(Type type, IEnumerable<Guid> ids)
        {
            var iEntityType = typeof(IEntity);
            var parameter = Expression.Parameter(type, "x");
            var idMember = iEntityType.GetProperty(nameof(IEntity.Id));
            var convertedParameter = Expression.Convert(parameter, iEntityType);
            var predicate = Expression.MakeMemberAccess(convertedParameter, idMember).In(ids);
            return await Query(type)
                .Where(predicate, parameter)
                .Cast<IEntity>()
                .ToArrayAsync();
        }

        public virtual IQueryable Query(Type entityType)
        {
            return (IQueryable)_queryMethod
                .MakeGenericMethod(entityType)
                .Invoke(this, new object[0]);
        }

        public virtual IQueryable<TEntity> Query<TEntity>() where TEntity : class, IEntity
        {
            var entityType = typeof(TEntity);
            IQueryable<TEntity> queryable = null;
            if (!entityType.IsConcrete())
            {
                var sets = GetConcreteTypes(entityType)
                    .Select(x => BuildQueryable(x))
                    .ToArray();
                queryable = new MultiSourceQueryProvider<TEntity>(sets);//TODO options.UseCocoriCore(enableMultiSourceProvider: true, detechChangesUsingStateStore:true)
            }
            else
            {
                queryable = BuildQueryable(entityType).Cast<TEntity>();
            }
            return new ExtendedEnumerable<TEntity>(queryable, e => Detach(e), "detach");
        }

        private IQueryable BuildQueryable(Type entityType)
        {
            return (IQueryable)_buildQueryableMethod
                .MakeGenericMethod(entityType)
                .Invoke(this, new object[0]);
        }

        private IQueryable<TEntity> BuildQueryable<TEntity>() where TEntity : class
        {
            var dbSet = _dbContext.Set<TEntity>();
            return new EFCoreAsyncQueryProvider<TEntity>(dbSet.AsQueryable());
        }

        public virtual async Task InsertAsync(IEntity entity)
        {
            _transaction = _transaction ?? await _dbContext.Database.BeginTransactionAsync();
            _dbContext.Add(entity);
        }

        public virtual async Task UpdateAsync(IEntity entity)
        {
            _transaction = _transaction ?? await _dbContext.Database.BeginTransactionAsync();
            _dbContext.Update(entity);
        }

        public virtual async Task DeleteAsync(IEntity entity)
        {
            _transaction = _transaction ?? await _dbContext.Database.BeginTransactionAsync();
            _dbContext.Remove(entity);
        }

        public virtual async Task CommitAsync()
        {
            await FlushAsync();
            if (_transaction != null)
            {
                await _transaction.CommitAsync();
                await _transaction.DisposeAsync();
                _transaction = null;
            }
        }

        public virtual async Task RollbackAsync()
        {
            DetachAllEntities(true);
            if (_transaction != null)
            {
                await _transaction.RollbackAsync();
                await _transaction.DisposeAsync();
                _transaction = null;
            }
        }

        public virtual async Task FlushAsync()
        {
            DetectChanges();
            var entityCount = _dbContext.ChangeTracker.Entries().Count();
            if (entityCount > 0)
            {
                var sw = new Stopwatch();
                sw.Start();
                await _dbContext.SaveChangesAsync();
                sw.Stop();
                _logger.LogDebug("SaveChanges time : {0}ms for {1} entities.", sw.ElapsedMilliseconds, entityCount);
            }
            DetachAllEntities(true);
        }

        protected virtual void DetectChanges()
        {
            _dbContext.ChangeTracker.DetectChanges();
            if (_detectChangesUsingStateStore)
            {
                var stateStore = _unitOfWork.Resolve<IStateStore>();
                foreach (var entry in _dbContext.ChangeTracker.Entries())
                {
                    if (stateStore.TryGetState(entry.Entity, out var state))
                    {
                        UpdateMemberModification(entry, state);
                    }
                }
            }
        }

        private void UpdateMemberModification(EntityEntry entry, IState state)
        {
            foreach (var member in entry.Members)
            {
                var propertyInfo = member.Metadata.PropertyInfo;
                var previousValue = propertyInfo?.InvokeGetter(state.PreviousObject);
                var currentValue = propertyInfo?.InvokeGetter(state.CurrentObject);
                member.IsModified = !Equals(previousValue, currentValue);
            }
        }

        private T Detach<T>(T entity)
        {
            if (entity != null)
            {
                DetachIfNotModified(_dbContext.Entry(entity));
            }
            return entity;
        }

        private void DetachAllEntities(bool force = false)
        {
            foreach (var entry in _dbContext.ChangeTracker.Entries())
            {
                DetachIfNotModified(entry, force);
            }
        }

        private void DetachIfNotModified(EntityEntry entry, bool force = false)
        {
            entry.State = (entry.State == EntityState.Unchanged || force) ? EntityState.Detached : entry.State;
        }

        public virtual void Dispose()
        {
            _transaction?.Dispose();
            _dbContext.Dispose();
        }
    }
}