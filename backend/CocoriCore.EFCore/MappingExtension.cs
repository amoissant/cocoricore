using CocoriCore.JsonNet;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CocoriCore.EFCore
{
    public static class MappingExtension
    {
        private static ObjectSerializer _serializer = new ObjectSerializer();

        public static PropertyBuilder<TProperty> ToJson<TProperty>(this PropertyBuilder<TProperty> propertyBuilder, 
            bool useCamelCase = true, bool hasTypeInfos = true)
        {
            return propertyBuilder.HasConversion(
                v => _serializer.Serialize(v, useCamelCase, hasTypeInfos),
                v => _serializer.Deserialize<TProperty>(v, useCamelCase, hasTypeInfos));
        }
    }
}