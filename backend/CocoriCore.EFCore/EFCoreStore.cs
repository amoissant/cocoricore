using CocoriCore.Common;
using CocoriCore.Repository;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace CocoriCore.EFCore
{
    public class EFCoreStore<TDbContext> : IEFCoreDatabase, IPersistentStore where TDbContext : DbContext
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public DbContextOptions Options { get; }
        public bool UseMigrations { get; }

        public EFCoreStore(DbContextOptions options, IUnitOfWorkFactory unitOfWorkFactory)
        {
            Options = options;
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        /// <inheritdoc/>
        public virtual async Task EnsureCreatedAsync(bool useMigrations = true)
        {
            using (var uow = _unitOfWorkFactory.NewUnitOfWork())
            using (var dbContext = uow.Resolve<TDbContext>())
            {
                if (useMigrations)
                {
                    await dbContext.Database.MigrateAsync();
                }
                else
                {
                    await dbContext.Database.EnsureCreatedAsync();
                }
            }
        }

        /// <inheritdoc/>
        public virtual async Task MigrateAsync()
        {
            using (var uow = _unitOfWorkFactory.NewUnitOfWork())
            using (var dbContext = uow.Resolve<TDbContext>())
            {
                await dbContext.Database.MigrateAsync();
            }
        }

        /// <inheritdoc/>
        public virtual async Task EnsureDeletedAsync()
        {
            using(var uow = _unitOfWorkFactory.NewUnitOfWork())
            using (var dbContext = uow.Resolve<TDbContext>())
            {
                await dbContext.Database.EnsureDeletedAsync();
            }
        }

        /// <inheritdoc/>
        public async Task ClearAsync()
        {
            using (var uow = _unitOfWorkFactory.NewUnitOfWork())
            using (var dbContext = uow.Resolve<TDbContext>())
            {
                if (dbContext.Database.ProviderName.Contains("Sqlite"))
                {
                    var types = dbContext.Model.GetEntityTypes();
                    foreach (var type in types)
                    {
                        await dbContext.Database.ExecuteSqlRawAsync($"DELETE FROM {type.GetTableName()};");
                    }
                }
                else
                {
                    var types = dbContext.Model.GetEntityTypes();
                    foreach (var type in types)
                    {
                        await dbContext.Database.ExecuteSqlRawAsync($"TRUNCATE \"{type.GetTableName()}\";");
                    }
                }
            }
        }
    }
}
