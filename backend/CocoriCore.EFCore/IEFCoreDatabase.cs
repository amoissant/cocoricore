﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System.Threading.Tasks;

namespace CocoriCore.EFCore
{
    public interface IEFCoreDatabase
    {
        /// <summary>
        /// Call dbContext.Database.EnsureCreatedAsync() or dbContext.Database.MigrateAsync()
        /// on the <see cref="DbContext"/> associated to this database.
        /// <para>See <see cref="DatabaseFacade.EnsureCreatedAsync"/> for more informations.</para>
        /// <para>See <see cref="RelationalDatabaseFacadeExtensions.MigrateAsync"/> for more informations.</para>
        /// </summary>
        /// <param name="useMigration">When true call <see cref="RelationalDatabaseFacadeExtensions.MigrateAsync"/>
        /// else <see cref="DatabaseFacade.EnsureCreatedAsync"/></param>
        Task EnsureCreatedAsync(bool useMigration = true);
        /// <summary>
        /// Call dbContext.Database.MigrateAsync() on the <see cref="DbContext"/> associated to this database.
        /// <para>See <see cref="RelationalDatabaseFacadeExtensions.MigrateAsync"/> for more informations.</para>
        /// </summary>
        Task MigrateAsync();
        /// <summary>
        /// Call dbContext.Database.EnsureDeletedAsync() on the <see cref="DbContext"/> associated to this database.
        /// <para>See <see cref="DatabaseFacade.EnsureDeletedAsync"/> for more informations.</para>
        /// </summary>
        Task EnsureDeletedAsync();
        /// <summary>
        /// Execute TRUNCATE &lt;TABLE_NAME&gt; (or DELETE FROM &lt;TABLE_NAME&gt; when TRUNCATE is not available)
        /// for each table of the <see cref="DbContext"/> associated to this database.
        /// </summary>
        Task ClearAsync();
    }
}
