using CocoriCore.Types.Extension;
using System;
using System.Reflection;

namespace CocoriCore.Reflection.Extension
{
    public static class ReflectionExtension
    {
        public static Type GetMemberType(this MemberInfo memberInfo)
        {
            if (memberInfo is PropertyInfo propInfo)
            {
                return propInfo.PropertyType;
            }
            else if (memberInfo is FieldInfo fieldInfo)
            {
                return fieldInfo.FieldType;
            }
            else
            {
                throw new NotImplementedException("Unknown member info type : " + memberInfo.GetType());
            }
        }

        public static bool CanWrite(this MemberInfo memberInfo)
        {
            if (memberInfo is PropertyInfo propInfo)
            {
                return propInfo.CanWrite;
            }
            return true;
        }

        public static bool CanRead(this MemberInfo memberInfo)
        {
            if (memberInfo is PropertyInfo propInfo)
            {
                return propInfo.CanRead;
            }
            return true;
        }

        public static T InvokeGetter<T>(this MemberInfo memberInfo, object obj)
        {
            return (T)memberInfo.InvokeGetter(obj);
        }

        public static object InvokeGetter(this MemberInfo memberInfo, object obj)
        {
            if (memberInfo is PropertyInfo propInfo)
            {
                return propInfo.GetValue(obj);
            }
            else if (memberInfo is FieldInfo fieldInfo)
            {
                return fieldInfo.GetValue(obj);
            }
            else
            {
                throw new InvalidOperationException($"Only {nameof(PropertyInfo)} and {nameof(FieldInfo)} are supported.");
            }
        }

        public static void InvokeSetter(this MemberInfo memberInfo, object obj, object value)
        {
            if (memberInfo is PropertyInfo propInfo)
            {
                propInfo.SetValue(obj, value);
            }
            else if (memberInfo is FieldInfo fieldInfo)
            {
                fieldInfo.SetValue(obj, value);
            }
            else
            {
                throw new InvalidOperationException($"Only {nameof(PropertyInfo)} and {nameof(FieldInfo)} are supported.");
            }
        }

        public static object ShallowCopyTo(this object source, object destination)
        {
            var sourceType = source.GetType();
            var destinationType = destination.GetType();
            foreach (var sourceMember in sourceType.GetPropertiesAndFields())
            {
                if (sourceMember.CanRead())
                {
                    var destinationMember = destinationType.GetPropertyOrFieldOrDefault(sourceMember.Name);
                    if (destinationMember != null && destinationMember.CanWrite())
                    {
                        var value = destinationMember.InvokeGetter(source);
                        destinationMember.InvokeSetter(destination, value);
                    }
                }
            }
            return destination;
        }
    }
}