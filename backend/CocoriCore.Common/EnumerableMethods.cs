﻿using CocoriCore.Types.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CocoriCore.Common
{
    public static class EnumerableMethods
    {
        private static MethodInfo _toList;
        private static MethodInfo _toArray;
        private static MethodInfo _where;
        private static MethodInfo _contains;
        private static MethodInfo _cast;

        public static MethodInfo Where
        {
            get
            {
                if (_where == null)
                {
                    _where = typeof(Enumerable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == nameof(Enumerable.Where) &&
                            x.GetParameters().Count() == 2 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IEnumerable<>)) &&
                            x.GetParameters()[1].ParameterType.IsAssignableToGeneric(typeof(Func<,>)))
                        .Single();
                }
                return _where;
            }
        }

        public static MethodInfo ToList
        {
            get
            {
                if (_toList == null)
                {
                    _toList = typeof(Enumerable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == nameof(Enumerable.ToList) &&
                            x.GetParameters().Count() == 1 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IEnumerable<>)))
                        .Single();
                }
                return _toList;
            }
        }

        public static MethodInfo ToArray
        {
            get
            {
                if (_toArray == null)
                {
                    _toArray = typeof(Enumerable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == nameof(Enumerable.ToArray) &&
                            x.GetParameters().Count() == 1 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IEnumerable<>)))
                        .Single();
                }
                return _toArray;
            }
        }

        public static MethodInfo Contains
        {
            get
            {
                if (_contains == null)
                {
                    _contains = typeof(Enumerable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == nameof(Enumerable.Contains) &&
                            x.GetParameters().Count() == 2)
                        .Single();
                }
                return _contains;
            }
        }

        public static MethodInfo Cast
        {
            get
            {
                if (_cast == null)
                {
                    _cast = typeof(Enumerable)
                        .GetMethods()
                        .Where(x => x.Name == nameof(Enumerable.Cast))
                        .Single();
                }
                return _cast;
            }
        }
    }
}
