﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CocoriCore.Common
{
    public class ActionCall<TIn> : IMethodCall<TIn, object>
    {
        private static Task<object> _defaultResult = Task.FromResult((object)null);
        public ActionCall(Action<TIn> action)
        {
            Delegate = action;
            Func = i => { action(i); return null; };
        }

        public ActionCall(Expression<Action<TIn>> expression) : this(expression.Compile())
        {
            Expression = expression;
        }

        public LambdaExpression Expression { get; }
        public Func<TIn, object> Func { get; }
        public bool IsAsync => false;
        public object Delegate { get; }

        public object Execute(IUnitOfWork unitOfWork, TIn input)
        {
            return Func(input);
        }

        public Task<object> ExecuteAsync(IUnitOfWork unitOfWork, TIn input)
        {
            Execute(unitOfWork, input);
            return _defaultResult;
        }
    }

    public class ActionCall<TService, TIn> : IMethodCall<TIn, object>
    {
        private static Task<object> _defaultResult = Task.FromResult((object)null);
        public ActionCall(Action<TService, TIn> action)
        {
            Func = (s, i) => { action(s, i); return null; };
            Delegate = action;
        }

        public ActionCall(Expression<Action<TService, TIn>> expression) : this(expression.Compile())
        {
            Expression = expression;
        }

        public LambdaExpression Expression { get; }
        public Func<TService, TIn, object> Func { get; }
        public bool IsAsync => false;
        public object Delegate { get; }

        public object Execute(IUnitOfWork unitOfWork, TIn input)
        {
            var service = unitOfWork.Resolve<TService>();
            return Func(service, input);
        }

        public Task<object> ExecuteAsync(IUnitOfWork unitOfWork, TIn input)
        {
            Execute(unitOfWork, input);
            return _defaultResult;
        }
    }

    public class ActionCall<TService1, TService2, TIn> : IMethodCall<TIn, object>
    {
        private static Task<object> _defaultResult = Task.FromResult((object)null);
        public ActionCall(Action<TService1, TService2, TIn> action)
        {
            Func = (s1, s2, i) => { action(s1, s2, i); return null; };
            Delegate = action;
        }

        public ActionCall(Expression<Action<TService1, TService2, TIn>> expression) : this(expression.Compile())
        {
            Expression = expression;
        }

        public LambdaExpression Expression { get; }
        public Func<TService1, TService2, TIn, object> Func { get; }
        public bool IsAsync => false;
        public object Delegate { get; }

        public object Execute(IUnitOfWork unitOfWork, TIn input)
        {
            var service1 = unitOfWork.Resolve<TService1>();
            var service2 = unitOfWork.Resolve<TService2>();
            return Func(service1, service2, input);
        }

        public Task<object> ExecuteAsync(IUnitOfWork unitOfWork, TIn input)
        {
            Execute(unitOfWork, input);
            return _defaultResult;
        }
    }
}