﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;

namespace CocoriCore.Common
{
    public class ErrorResponse : ErrorResponse<object>
    {
        public ErrorResponse(Exception exception, float statusCode, bool debugMode = false) 
            : base(exception, statusCode, debugMode)
        {
        }

        public ErrorResponse(string message, Exception exception, float statusCode, bool debugMode = false)
            : base(exception, statusCode, debugMode)
        {
            Message = message;
        }
    }

    public class ErrorResponse<TData>
    {
        public Guid? Id;
        public float StatusCode;
        public string Message;
        public TData Datas;
        public DebugResponse Debug;

        public ErrorResponse(Exception exception, float statusCode, bool debugMode = false)
            : this(exception, statusCode, exception?.Message, debugMode)
        {
        }

        public ErrorResponse(Exception exception, float statusCode, string message, bool debugMode = false)
        {
            Id = exception.GetId();
            Message = message;
            StatusCode = statusCode;

            if (debugMode)
            {
                Debug = new DebugResponse
                {
                    Type = exception?.GetType()?.FullName,
                    Messages = GetMessages(exception),
                    StackTraces = GetStackTraces(exception),
                    Datas = CopyDatas(exception)
                };
            }
        }

        private string[] GetMessages(Exception exception)
        {
            var message = exception?.Message ?? string.Empty;
            var messages = new List<string>(message.Split('\n'));
            exception = exception?.InnerException;
            while (exception != null)
            {
                messages.Add("--- Inner message :");
                var innerMessage = exception?.Message ?? string.Empty;
                messages.AddRange(innerMessage.Split('\n'));
                exception = exception?.InnerException;
            }
            return messages
                .Select(x => x != null ? x.Replace("\r", string.Empty) : x)
                .ToArray();
        }

        private string[] GetStackTraces(Exception exception)
        {
            var stackTrace = exception?.StackTrace ?? string.Empty;
            var stackTraces = new List<string>(stackTrace.Split('\n'));
            exception = exception?.InnerException;
            while (exception != null)
            {
                stackTraces.Insert(0, "--- End of the stack trace.");
                var innerStackTrace = exception?.StackTrace ?? string.Empty;
                stackTraces.InsertRange(0, innerStackTrace.Split('\n'));
                exception = exception?.InnerException;
            }
            return stackTraces
                .Select(x => x != null ? x.Replace("\r", string.Empty) : x)
                .ToArray();
        }

        private dynamic CopyDatas(Exception exception)
        {
            var datas = new ExpandoObject() as IDictionary<string, object>;
            if (exception?.Data != null)
            {
                foreach (var key in exception.Data.Keys)//TODO ToExpando ici ?
                {
                    if (Equals(key, ExceptionExtension.ExceptionIdKey))
                    {
                        continue;
                    }
                    datas[key.ToString()] = exception.Data[key];
                }
                if (exception.InnerException != null && exception.InnerException.Data.Count > 0)
                {
                    datas["InnerDatas"] = CopyDatas(exception.InnerException);
                }
            }
            return datas;
        }

        public class DebugResponse
        {
            public string Type;
            public string[] Messages;
            public object Datas;
            public string[] StackTraces;
        }
    }
}