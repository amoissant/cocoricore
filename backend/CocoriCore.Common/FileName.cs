﻿using System;
using System.Text.RegularExpressions;

namespace CocoriCore.Common
{
    public class FileName
    {
        private static Regex _incrementRegex = new Regex(@"\(\d+\)$");
        private static Regex _regexIllegal = new Regex(@"[\*\\\?%]+");

        private string _nameWithoutExtension;
        private string _extension;
        private int _increment;

        private FileName(string nameWithoutExtension, string extension, int increment)
        {
            _nameWithoutExtension = nameWithoutExtension;
            _extension = extension;
            _increment = increment;
        }

        public FileName(string value)
        {
            if (string.IsNullOrEmpty(value))
                throw new ArgumentOutOfRangeException($"'{value}' must not be null or empty.");

            var extensionIndex = value.LastIndexOf('.');
            if (extensionIndex != -1)
            {
                _extension = value.Substring(extensionIndex);
                _nameWithoutExtension = value.Substring(0, extensionIndex);
            }
            else
            {
                _nameWithoutExtension = value;
            }
            var match = _incrementRegex.Match(_nameWithoutExtension);
            if (match.Success)
            {
                _increment = int.Parse(match.Value.Substring(1, match.Value.Length - 2));
                _nameWithoutExtension = _nameWithoutExtension.Substring(0, _nameWithoutExtension.Length - match.Value.Length);
            }
            else
                _increment = 0;
            if (_regexIllegal.IsMatch(value))
                throw new ArgumentException($"'{value}' is not a valid filename.");
        }

        public FileName Increment()
        {
            return new FileName(_nameWithoutExtension, _extension, _increment + 1);
        }

        public override string ToString()
        {
            var namePart = _nameWithoutExtension;
            var incrementPart = _increment > 0 ? $"({_increment})" : string.Empty;
            var extensionPart = string.IsNullOrEmpty(_extension) ? string.Empty : _extension;
            return $"{namePart}{incrementPart}{extensionPart}";
        }

        public static bool operator ==(FileName objet1, FileName objet2)
        {
            if (objet1 as object == null && objet2 as object == null)
                return true;
            else if (objet1 as object != null && objet2 as object == null)
                return false;
            else if (objet1 as object == null && objet2 as object != null)
                return false;
            return Equals(objet1.ToString(), objet2.ToString());
        }

        public static bool operator !=(FileName objet1, FileName objet2)
        {
            return !(objet1 == objet2);
        }

        public static implicit operator FileName(string value)
        {
            return value != null ? new FileName(value) : null;
        }

        public static implicit operator string(FileName value)
        {
            return value?.ToString();
        }
    }
}