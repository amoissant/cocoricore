using System;
using System.Threading.Tasks;

namespace CocoriCore.Common
{
    public interface IUnitOfWork : IDisposable
    {
        T Resolve<T>();

        object Resolve(Type type);

        Task FinishAsync();
    }
}