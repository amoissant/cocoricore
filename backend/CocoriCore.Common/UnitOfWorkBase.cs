using System;
using System.Threading.Tasks;

namespace CocoriCore.Common
{
    public abstract class UnitOfWorkBase : IUnitOfWork
    {
        protected UnitOfWorkOptions _options;

        public UnitOfWorkBase(UnitOfWorkOptions options)
        {
            _options = options;
        }

        public virtual async Task FinishAsync()
        {
            foreach (var action in _options.Actions)
            {
                var actionInstance = Resolve(action.Type);
                await action.Action(actionInstance);
            }
        }
        public abstract void Dispose();
        public abstract T Resolve<T>();
        public abstract object Resolve(Type type);
    }
}