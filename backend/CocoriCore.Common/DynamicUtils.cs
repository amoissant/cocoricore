using CocoriCore.Reflection.Extension;
using CocoriCore.Types.Extension;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;

namespace CocoriCore.Dynamic.Extension
{
    public static class DynamicUtils
    {
        /// <summary>
        /// Get wether value is a <c>string</c> representing a <see cref="Guid"/>.
        /// </summary>
        /// <param name="value">The value to evaluate</param>
        /// <returns><c>true</c> if <paramref name="value"/> is a <c>string</c> that can be parsed to <see cref="Guid"/> else <c>false</c></returns>
        public static bool IsStringMatchingGuid(dynamic value)
        {
            Guid guid;
            return value is string && Guid.TryParse(value, out guid);
        }

        /// <summary>
        /// Get wether value is an integral numeric type (signed or unsigned, whatever it's lenght).
        /// </summary>
        /// <param name="value">The value to evaluate</param>
        /// <returns><c>true</c> if <paramref name="value"/> is an integral numeric type
        /// (<c>short</c>, <c>ushort</c>, <c>int</c>, <c>uint</c>, <c>long</c>, <c>ulong</c>) else <c>false</c></returns>
        public static bool IsInteger(this object value)
        {
            return value is short || value is ushort || value is int || value is uint || value is long || value is ulong;
        }

        /// <summary>
        /// Get wether value is a floating-point numeric type (signed or unsigned, whatever it's lenght).
        /// </summary>
        /// <param name="value">The value to evaluate</param>
        /// <returns><c>true</c> if <paramref name="value"/> is a floating-point numeric type
        /// (<c>float</c>, <c>double</c>, <c>decimal</c>) else <c>false</c></returns>
        public static bool IsFloat(this object value)
        {
            return value is float || value is double || value is decimal;
        }

        /// <summary>
        /// Get wether value is a numeric type (signed or unsigned, integral or floating-point, whatever it's lenght).
        /// </summary>
        /// <param name="value">The value to evaluate</param>
        /// <returns><c>true</c> if <paramref name="value"/> is a floating-point numeric type or an integral numeric type else <c>false</c></returns>
        public static bool IsNumber(this object value)
        {
            return IsInteger(value) || IsFloat(value);
        }

        public static Type[] FINAL_TYPES = new[]
        {
            typeof(object),
            typeof(bool),
            typeof(byte),
            typeof(sbyte),
            typeof(char),
            typeof(decimal),
            typeof(double),
            typeof(float),
            typeof(int),
            typeof(uint),
            typeof(long),
            typeof(ulong),
            typeof(short),
            typeof(ushort),
            typeof(string),
            typeof(Guid),
            typeof(DateTime),
            typeof(TimeSpan),
            typeof(Uri),
        };

        /// <summary>
        /// Create a <c>dynamic</c> deep copy of <paramref name="obj"/> structure. The copy run through <paramref name="obj"/> 
        /// and its children recursively and stop when a node is a final type.
        /// </summary>
        /// <param name="obj">The instance to duplicate</param>
        /// <returns>An <see cref="ExpandoObject"/> with the same structure as <paramref name="obj"/> including all its children</returns>
        /// <remarks>See <see cref="ToDynamic(object, IEnumerable{Type}, int, int, string)"/> for more informations over deep copy process.</remarks>
        public static ExpandoObject ToExpando(this object obj)
        {
            return obj.ToDynamic().AsExpando();
        }

        /// <summary>
        /// Create a <c>dynamic</c> deep copy of <paramref name="obj"/> structure. The copy run through <paramref name="obj"/> 
        /// and its children recursively and stop when a node is a final type.
        /// </summary>
        /// <param name="obj">The instance to duplicate</param>
        /// <param name="finalTypes">An collection of leaf types to prevent recursive copy to contnue, if <c>null</c> use defaults</param>
        /// <param name="maxDeep">The max deep of the <paramref name="obj"/> grapgh structure used to prevent cyclic/infinite deep copy</param>
        /// <returns>An <see cref="ExpandoObject"/> typed as <c>object</c> with the same structure as <paramref name="obj"/> including all its children</returns>
        /// <remarks>Default leaf types are all numeric types + <see cref="Guid"/> + <see cref="DateTime"/> + <see cref="TimeSpan"/> + <see cref="Uri"/></remarks>
        public static object ToDynamic(this object obj, IEnumerable<Type> finalTypes = null, int maxDeep = 50)
        {
            finalTypes = finalTypes != null ? finalTypes : FINAL_TYPES;
            return obj.ToDynamic(finalTypes, 0, maxDeep, $"{obj?.GetType()}");
        }

        private static object ToDynamic(this object obj, IEnumerable<Type> finalTypes, int currentDeep, int maxDeep, string path)
        {
            if (currentDeep >= maxDeep)
            {
                throw new InvalidOperationException($"Max deep ({maxDeep}) reached while converting object to dynamic, "
                    + $"you can increase this value manually using optional argument '{nameof(maxDeep)}'.\n"
                    + $"Path : {path}.");
            }
            if (obj == null)
            {
                return null;
            }
            else if (finalTypes.Contains(obj.GetType()) || obj.GetType().IsEnum)
            {
                return obj;
            }
            else if (obj is IDictionary nonGenericDictionary)
            {
                var expandoObject = new ExpandoObject();
                var dictionary = (IDictionary<string, object>)expandoObject;
                foreach (var key in nonGenericDictionary.Keys)
                {
                    dictionary.Add(key.ToString(), nonGenericDictionary[key].ToDynamic(finalTypes, currentDeep + 1, maxDeep, $"{path}.{key}"));
                }
                return expandoObject;
            }
            else if (obj is IDictionary<string, object> genericDictionary)
            {
                var expandoObject = new ExpandoObject();
                var dictionary = (IDictionary<string, object>)expandoObject;
                foreach (var kvp in genericDictionary)
                {
                    dictionary.Add(kvp.Key, kvp.Value.ToDynamic(finalTypes, currentDeep + 1, maxDeep, $"{path}.{kvp.Key}"));
                }
                return expandoObject;
            }
            else if (obj is IEnumerable enumerable)
            {
                return enumerable.Cast<object>().Select((x, i) => x.ToDynamic(finalTypes, currentDeep + 1, maxDeep, $"{path}[{i}]")).ToArray();
            }
            else
            {
                var expandoObject = new ExpandoObject();
                var dictionary = (IDictionary<string, object>)expandoObject;
                foreach (var member in obj.GetType().GetPropertiesAndFields())
                {
                    dictionary.Add(member.Name, member.InvokeGetter(obj).ToDynamic(finalTypes, currentDeep + 1, maxDeep, $"{path}.{member.Name}"));
                }
                return expandoObject;
            }
        }


        /// <summary>
        /// Cast <paramref name="obj"/> to dynamic. Parameter <paramref name="obj"/> must be an <see cref="ExpandoObject"/>.
        /// </summary>
        /// <param name="obj">The instance to cast.</param>
        /// <returns>The casted instance.</returns>
        public static dynamic AsDynamic(this object obj)
        {
            return obj;
        }

        /// <summary>
        /// Cast <paramref name="obj"/> to <see cref="ExpandoObject"/>. Parameter <paramref name="obj"/> must be an <see cref="ExpandoObject"/>.
        /// </summary>
        /// <param name="obj">The instance to cast.</param>
        /// <returns>The casted instance.</returns>
        public static ExpandoObject AsExpando(this object obj)
        {
            return (ExpandoObject)obj;
        }

        /// <summary>
        /// Cast <paramref name="obj"/> to <see cref="IDictionary"/>. Parameter <paramref name="obj"/> must be an <see cref="ExpandoObject"/>.
        /// </summary>
        /// <param name="obj">The <see cref="ExpandoObject"/> to cast.</param>
        /// <returns>The casted instance.</returns>
        public static IDictionary<string, object> AsDictionary(this object obj)
        {
            return (IDictionary<string, object>)obj;
        }
    }
}
