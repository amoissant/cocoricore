using System.Collections.Generic;

namespace CocoriCore.Common
{
    public class UnitOfWorkOptions
    {
        public List<ActionDefinition> Actions { get; }

        public UnitOfWorkOptions()
        {
            Actions = new List<ActionDefinition>();
        }

    }
}