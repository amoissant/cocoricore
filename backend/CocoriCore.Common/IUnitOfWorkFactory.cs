﻿namespace CocoriCore.Common
{
    public interface IUnitOfWorkFactory
    {
        IUnitOfWork NewUnitOfWork();
    }
}