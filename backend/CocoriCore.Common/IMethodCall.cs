﻿using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CocoriCore.Common
{
    public interface IMethodCall
    {
        bool IsAsync { get; }
        object Delegate { get; }
        LambdaExpression Expression { get; }
    }

    public interface IMethodCall<TIn, TOut> : IMethodCall
    {
        TOut Execute(IUnitOfWork unitOfWork, TIn input);
        Task<TOut> ExecuteAsync(IUnitOfWork unitOfWork, TIn input);
    }
}