﻿using CocoriCore.Reflection.Extension;
using System.Linq.Expressions;
using System.Reflection;

namespace CocoriCore.Common
{
    public class Equality
    {
        public Equality(ParameterExpression dependencyParameter, MemberInfo dependencyMember,
            ParameterExpression projectionParameter, MemberInfo projectionMember)
        {
            DependencyParameter = dependencyParameter;
            DependencyMember = dependencyMember;
            ProjectionParameter = projectionParameter;
            ProjectionMember = projectionMember;
        }

        public ParameterExpression DependencyParameter { get; }
        public MemberInfo DependencyMember { get; }
        public ParameterExpression ProjectionParameter { get; }
        public MemberInfo ProjectionMember { get; }

        public void Apply(IEntity dependency, IEntity projection)
        {
            ProjectionMember.InvokeSetter(projection, DependencyMember.InvokeGetter(dependency));
        }
    }
}
