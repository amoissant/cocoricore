using CocoriCore.Reflection.Extension;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CocoriCore.Types.Extension
{
    public enum HierarchyOrder
    {
        Self_Base_Interfaces,
        Self_Interfaces_Base
    }

    public static class TypesExtension
    {
        public static IEnumerable<Type> GetTypeHierarchy(this object obj, 
            HierarchyOrder order = HierarchyOrder.Self_Base_Interfaces)
        {
            if (obj == null)
                return new Type[0];
            return obj.GetType().GetTypeHierarchy(order);
        }

        public static IEnumerable<Type> GetTypeHierarchy(this Type type,
            HierarchyOrder order = HierarchyOrder.Self_Base_Interfaces)
        {
            List<Type> types = new List<Type>();
            if(order == HierarchyOrder.Self_Base_Interfaces)
            {
                types.Add(type);
                types.AddRange(type.GetBaseTypes());
                types.AddRange(type.GetInterfaces());
            }
            else if(order == HierarchyOrder.Self_Interfaces_Base)
            {
                types.Add(type);
                types.AddRange(type.GetInterfaces());
                types.AddRange(type.GetBaseTypes());
            }
            else
            {
                throw new NotImplementedException($"{order}");
            }
            return types;
        }

        public static IEnumerable<Type> GetBaseTypes(this Type type)
        {
            List<Type> superTypes = new List<Type>();
            type = type.BaseType;
            while (type != null)
            {
                superTypes.Add(type);
                type = type.BaseType;
            }
            return superTypes;
        }

        public static Type GetFirstBaseType(this Type type, Type baseType)
        {
            return type.GetBaseTypes().First(t => t.IsGenericType && t.GetGenericTypeDefinition() == baseType);
        }

        public static bool IsAssignableToGeneric(this Type type, Type genericType)
        {
            return type
                .GetTypeHierarchy()
                .Any(i =>
                    i.IsGenericType &&
                    i.GetGenericTypeDefinition() == genericType);
        }

        public static bool IsAssignableTo(this Type currentType, Type type)
        {
            return type.IsAssignableFrom(currentType);
        }

        public static bool IsAssignableTo<T>(this Type currentType)
        {
            return typeof(T).IsAssignableFrom(currentType);
        }

        public static bool IsNullable(this Type type)
        {
            return type.IsNullable(out var _);
        }

        public static bool IsNullable(this Type type, out Type nonNullableType)
        {
            var underlyingType = Nullable.GetUnderlyingType(type);
            nonNullableType = underlyingType ?? type;
            return type.IsClass || underlyingType != null;
        }

        public static Type Unbox(this Type type)
        {
            return type?.UnboxIfEnumerable()?.UnboxIfArray()?.UnboxIfNullable();
        }

        public static Type UnboxIfNullable(this Type type)
        {
            return Nullable.GetUnderlyingType(type) ?? type;
        }

        public static Type UnboxIfArray(this Type type)
        {
            return type.IsArray ? type.GetElementType() : type;
        }

        public static Type UnboxIfEnumerable(this Type type)
        {
            return type.IsAssignableTo<IEnumerable>() && type.IsGenericType ? type.GetGenericArguments()[0] : type;
        }

        public static Type[] GetGenericArguments(this Type type, Type genericSuperType)
        {
            return type
                .GetTypeHierarchy()
                .First(i =>
                    i.IsGenericType &&
                    i.GetGenericTypeDefinition() == genericSuperType)
                .GenericTypeArguments;
        }

        public static Type GetInterface(this Type type, Type genericInterfaceType)
        {
            IEnumerable<Type> interfaces = type.GetInterfaces();
            if (type.IsInterface)
            {
                interfaces = interfaces.Append(type);
            }
            return interfaces.First(t => t.IsGenericType && t.GetGenericTypeDefinition() == genericInterfaceType);
        }

        public static ReturnType CreateGenericInstance<ReturnType>(this Type type, Type contentType, params object[] parameters)
        {
            var _genericType = type.MakeGenericType(contentType);
            return (ReturnType)Activator.CreateInstance(_genericType, parameters);
        }

        public static MemberInfo GetPropertyOrField(this Type type, string name, BindingFlags bindingFlags)
        {
            MemberInfo memberInfo = type.GetPropertyOrFieldOrDefault(name, bindingFlags);
            if (memberInfo != null)
            {
                return memberInfo;
            }
            throw new MissingMemberException($"No property neither field named '{name}' found for type {type}");
        }

        public static MemberInfo GetPropertyOrFieldOrDefault(this Type type, string name, BindingFlags? bindingFlags = null)
        {
            MemberInfo memberInfo = bindingFlags.HasValue ? type.GetProperty(name, bindingFlags.Value) : type.GetProperty(name);
            if (memberInfo != null)
            {
                return memberInfo;
            }
            memberInfo = bindingFlags.HasValue ? type.GetField(name, bindingFlags.Value) : type.GetField(name);
            if (memberInfo != null)
            {
                return memberInfo;
            }
            return null;
        }

        public static IEnumerable<MemberInfo> GetPropertiesAndFields(this Type type, BindingFlags? bindingFlags = null)
        {
            var properties = bindingFlags.HasValue ? type.GetProperties(bindingFlags.Value) : type.GetProperties();
            var fields = bindingFlags.HasValue ? type.GetFields(bindingFlags.Value) : type.GetFields();
            List<MemberInfo> propertiesAndFields = new List<MemberInfo>();
            propertiesAndFields.AddRange(properties.Where(x => x.GetIndexParameters().Length == 0));
            propertiesAndFields.AddRange(fields);
            return propertiesAndFields;
        }

        public static bool IsConcrete(this Type type)
        {
            return !type.IsAbstract && !type.IsInterface;
        }

        public static object ImplicitConvertTo(this object value, Type type)
        {
            if (!value.GetType().IsAssignableTo(type))
            {
                var method = type.GetMethod("op_Implicit", new Type[] { value.GetType() });
                if (method == null)
                {
                    throw new NotImplementedException($"No implicit operator found to convert object from {value?.GetType()?.FullName} to {type?.FullName}.");
                }
                value = method.Invoke(null, new object[] { value });
            }
            return value;
        }

        public static object GetDefault(this Type type)
        {
            if (type.IsValueType)
            {
                return Activator.CreateInstance(type);
            }
            return null;
        }

        public static string GetPrettyName(this Type type, bool full = false)
        {
            if (type is null) return "(null)";
            var typeName = full ? type.FullName : type.Name;
            if (type.IsGenericType)
            {
                typeName = typeName.Substring(0, typeName.IndexOf("`"));
                var genericParamaters = type
                    .GetGenericArguments()
                    .Select(x => x.GetPrettyName(full))
                    .ToArray();
                typeName += $"<{string.Join(", ", genericParamaters)}>";
            }
            return typeName;
        }

        public static bool HasMemberType<T>(this Type type)
        {
            return type
                .GetPropertiesAndFields()
                .Any(t => t.GetMemberType().Unbox().IsAssignableTo<T>());
        }

        public static bool HasMemberTypeRecursively<T>(this Type type, params Type[] finalTypes)
        {
            return type.HasMemberTypeRecursively<T>(finalTypes.AsEnumerable());
        }

        public static bool HasMemberTypeRecursively<T>(this Type type, IEnumerable<Type> finalTypes)
        {
            return type
                .ExtractSubTypesRecursively(finalTypes)
                .Any(x => x.HasMemberType<T>());
        }

        public static IEnumerable<Type> ExtractSubTypesRecursively(this Type type, IEnumerable<Type> finalTypes)
        {
            HashSet<Type> extractedTypes = new HashSet<Type>();
            type.ExtractTypesRecursively(extractedTypes, finalTypes);
            extractedTypes.Remove(type);
            return extractedTypes;
        }

        private static void ExtractTypesRecursively(this Type type, HashSet<Type> extractedTypes, IEnumerable<Type> finalTypes)
        {
            type = type.Unbox();
            if (type == null || finalTypes.Contains(type))
            {
                return;
            }
            else if (extractedTypes.Add(type))
            {
                var subTypes = type
                    .GetPropertiesAndFields()
                    .Select(t => t.GetMemberType());
                foreach (var subType in subTypes)
                {
                    subType.ExtractTypesRecursively(extractedTypes, finalTypes);
                }
            }
        }
    }
}