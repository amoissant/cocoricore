﻿using System;
using System.Threading.Tasks;

namespace CocoriCore.Common
{
    public interface IErrorBus
    {
        Task HandleAsync(Exception exception);
    }
}