﻿using CocoriCore.Common;
using CocoriCore.Reflection.Extension;
using CocoriCore.Types.Extension;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace CocoriCore.Collection.Extensions
{
    public static class IEnumerableExtensions
    {
        public static void ForEach<T>(this T[] array, Action<T> action)
        {
            foreach (var element in array)
            {
                action(element);
            }
        }

        public static void ForEachDouble<T1, T2>(this IEnumerable<T1> enumerable1, IEnumerable<T2> enumerable2, Action<T1, T2> action)
        {
            var enumerator1 = enumerable1.GetEnumerator();
            var enumerator2 = enumerable2.GetEnumerator();
            while (enumerator1.MoveNext() && enumerator2.MoveNext())
            {
                var value1 = enumerator1.Current;
                var value2 = enumerator2.Current;
                action(value1, value2);
            }
        }

        public static void ForEachTriple<T1, T2, T3>(this IEnumerable<T1> enumerable1, IEnumerable<T2> enumerable2,
            IEnumerable<T3> enumerable3, Action<T1, T2, T3> action)
        {
            var enumerator1 = enumerable1.GetEnumerator();
            var enumerator2 = enumerable2.GetEnumerator();
            var enumerator3 = enumerable3.GetEnumerator();
            while (enumerator1.MoveNext() && enumerator2.MoveNext() && enumerator3.MoveNext())
            {
                var value1 = enumerator1.Current;
                var value2 = enumerator2.Current;
                var value3 = enumerator3.Current;
                action(value1, value2, value3);
            }
        }

        public static bool IsUnique<T>(this IEnumerable<T> enumerable, Func<T, dynamic> member)
        {
            dynamic memberValue = null;
            return IsUnique(enumerable, member, out memberValue);
        }

        public static bool IsUnique<T>(this IEnumerable<T> enumerable, MemberInfo memberInfo)
        {
            dynamic memberValue = null;
            Func<T, dynamic> accessorFunc = x => memberInfo.InvokeGetter(x);
            return IsUnique(enumerable, accessorFunc, out memberValue);
        }

        public static bool IsUnique<TElement, TMemberValue>(this IEnumerable<TElement> enumerable,
            Func<TElement, TMemberValue> member, out TMemberValue duplicate)
        {
            HashSet<TMemberValue> set = new HashSet<TMemberValue>();
            foreach (var element in enumerable)
            {
                var memberValue = member.Invoke(element);
                if (!set.Add(memberValue))
                {
                    duplicate = memberValue;
                    return false;
                }
            }
            duplicate = default(TMemberValue);
            return true;
        }

        public static T[] RemoveAt<T>(this T[] array, int index)
            where T : class
        {
            array[index] = null;
            return array.Where(x => x != null).ToArray();
        }

        public static Dictionary<TKey, TValue> ToDictionary<TKey, TValue>(this IEnumerable<KeyValuePair<TKey, TValue>> collection)
        {
            var dictionary = new Dictionary<TKey, TValue>();
            foreach (var kvp in collection)
            {
                dictionary.Add(kvp.Key, kvp.Value);
            }
            return dictionary;
        }

        public static IEnumerable<T> OrderBy<T, TProperty>(this IEnumerable<T> enumerable,
            Func<T, TProperty> memberSelector, IEnumerable<TProperty> collection)
        {
            var order = new Dictionary<TProperty, int>();
            int position = 0;
            foreach (var element in collection)
            {
                order[element] = position++;
            }
            return enumerable.OrderBy(memberSelector, new KeyOrderComparer<TProperty>(order));
        }

        class KeyOrderComparer<TKey> : IComparer<TKey>
        {
            private Dictionary<TKey, int> _order;

            public KeyOrderComparer(Dictionary<TKey, int> order)
            {
                _order = order;
            }

            public int Compare(TKey x, TKey y)
            {
                return _order[x].CompareTo(_order[y]);
            }
        }

        public static IndexedSets<TKey, TValue> ToIndexedSets<TKey, TValue>(this IEnumerable<TValue> enumerable, Func<TValue, TKey> keySelector)
        {
            var indexedLists = new IndexedSets<TKey, TValue>();
            foreach (var element in enumerable)
            {
                var key = keySelector(element);
                indexedLists.Add(key, element);
            }
            return indexedLists;
        }

        public static bool HasSingle<T>(this IEnumerable<T> enumerable, Func<T, bool> predicate, out T result)
            where T : class
        {
            var results = enumerable.Where(predicate);
            if (results.Count() == 1)
            {
                result = results.First();
                return true;
            }
            result = null;
            return false;
        }

#if NETSTANDARD2_0
        public static TValue GetValueOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key)
        {
            dictionary.TryGetValue(key, out var result);
            return result;
        }
#endif

        public static TValue GetValueOrEmpty<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key)
        {
            dictionary.TryGetValue(key, out var result);
            if (result == null && typeof(TValue).IsAssignableToGeneric(typeof(IEnumerable<>)))
            {
                var elementType = typeof(TValue)
                    .GetInterfaces()
                    .Where(t => t.IsGenericType && t.GetGenericTypeDefinition() == typeof(IEnumerable<>))
                    .Select(t => t.GetGenericArguments()[0])
                    .FirstOrDefault();
                if (elementType == null)
                {
                    elementType = typeof(TValue).GetGenericArguments()[0];
                }
                result = (TValue)Activator.CreateInstance(elementType.MakeArrayType(), new object[] { 0 });
            }
            return result;
        }

        public static IQueryable<T> OrderBy<T>(this IQueryable<T> queryable, MemberInfo orderByMember)
        {
            return queryable.CallOrderMethod(QueryableMethods.OrderBy, orderByMember);
        }

        public static IQueryable<T> OrderByDescending<T>(this IQueryable<T> queryable, MemberInfo orderByMember)
        {
            return queryable.CallOrderMethod(QueryableMethods.OrderByDescending, orderByMember);
        }

        private static IQueryable<T> CallOrderMethod<T>(this IQueryable<T> queryable, MethodInfo orderByMethod, MemberInfo orderByMember)
        {
            var parameterExpression = Expression.Parameter(typeof(T), "x");
            Expression convertExpression = parameterExpression;
            if (typeof(T) != orderByMember.DeclaringType)
            {
                //Without this condition, with EFCore + SQLite we get a query translation error
                convertExpression = Expression.Convert(parameterExpression, orderByMember.DeclaringType);
            }
            var memberAccessExpression = Expression.MakeMemberAccess(convertExpression, orderByMember);
            var orderByLambdaType = typeof(Func<,>).MakeGenericType(typeof(T), orderByMember.GetMemberType());
            var orderByLambda = Expression.Lambda(orderByLambdaType, memberAccessExpression, parameterExpression);
            var typedOrderByMethod = orderByMethod.MakeGenericMethod(typeof(T), orderByMember.GetMemberType());
            queryable = (IQueryable<T>)typedOrderByMethod.Invoke(queryable, new object[] { queryable, orderByLambda });
            return queryable;
        }

        public static void AddRange<T>(this ISet<T> hashSet, params T[] values)
        {
            hashSet.AddRange((IEnumerable<T>)values);
        }

        public static ISet<T> AddRange<T>(this ISet<T> hashSet, IEnumerable<T> collection)
        {
            foreach (var element in collection)
            {
                hashSet.Add(element);
            }
            return hashSet;
        }

        public static ISet<T> AddRange<T>(this ISet<T> hashSet, IEnumerable collection)
        {
            return hashSet.AddRange(collection.Cast<T>());
        }

        public static Dictionary<TKey, IEnumerable<TValue>> ToIndex<T, TKey, TValue>(this IEnumerable<T> enumerable, Func<T, TKey> keySelector, Func<T, TValue> valueSelector)
        {
            return enumerable
                .GroupBy(keySelector)
                .ToDictionary(x => x.Key, x => x.Select(valueSelector));
        }

        public static Dictionary<TKey, IEnumerable<T>> ToIndex<T, TKey>(this IEnumerable<T> enumerable, Func<T, TKey> keySelector)
        {
            return enumerable
                .GroupBy(keySelector)
                .ToDictionary(x => x.Key, x => x.AsEnumerable());
        }

        public static Array CastToArray(this IEnumerable enumerable, Type elementType)
        {
            return enumerable.Cast(elementType).ToArray(elementType);

        }

        public static IEnumerable Cast(this IEnumerable enumerable, Type elementType)
        {
            var castMethod = EnumerableMethods.Cast.MakeGenericMethod(elementType);
            return (IEnumerable)castMethod.Invoke(null, new[] { enumerable });
        }

        public static object Sum(this IEnumerable enumerable)
        {
            //TODO on pourrait la mettre en cache dans EnumerableMethods dans un dictionnaire
            var sumMethod = typeof(Enumerable)
                .GetMethods()
                .Where(x =>
                    x.Name == nameof(Enumerable.Sum) &&
                    x.GetParameters().Count() == 1 &&
                    x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IEnumerable<>)) &&
                    x.ReturnType == enumerable.GetType().GenericTypeArguments[0])
                .Single();
            return sumMethod.Invoke(null, new[] { enumerable });
        }

        public static Array ToArray(this IEnumerable enumerable, Type elementType)
        {
            if (enumerable is Array array)
            {
                return array;
            }
            else
            {
                var toArrayMethod = EnumerableMethods.ToArray.MakeGenericMethod(elementType);
                return (Array)toArrayMethod.Invoke(null, new[] { enumerable });
            }
        }

        public static ICollection<T> ToCollection<T>(this IEnumerable<T> enumerable)
        {
            return enumerable.ToList();
        }

        /// <summary>
        /// Determine if <paramref name="dictionary"/> contains a key <paramref name="key"/> which value equals <paramref name="value"/> or not.
        /// </summary>
        /// <param name="dictionary">The instance of dictionary</param>
        /// <param name="key">The key to search in dictionary</param>
        /// <param name="value">The value to compare to the dictionary's value associated to <paramref name="key"/> if exists</param>
        /// <returns>True if dictionary contains key <paramref name="key"/> with value equals to <paramref name="value"/> else return false</returns>
        public static bool HasKeyWithValue<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, object value)
        {
            if (dictionary.TryGetValue(key, out var result))
            {
                return Equals(result, value);
            }
            else
            {
                return false;
            }
        }
    }
}
