﻿using System.Text.RegularExpressions;

namespace CocoriCore.Common
{
    public static class SlugExtension
    {
        public static string Normalized(this string str)
        {
            return str.Slugify(" ");
        }

        public static string Slugify(this string str)
        {
            return str.Slugify("-");
        }

        private static Regex _allButAlphanumericRegex = new Regex(@"[^a-z0-9\s]", RegexOptions.Compiled);
        private static Regex _whiteSpaceRegex = new Regex(@"\s{1,}", RegexOptions.Compiled);

        public static string Slugify(this string str, string separator)
        {
            if (str == null) return null;

            str = str.RemoveDiacritics().ToLower();
            str = _allButAlphanumericRegex.Replace(str, " ");
            str = str.Trim();
            str = _whiteSpaceRegex.Replace(str, separator);
            return str;
        }
    }
}