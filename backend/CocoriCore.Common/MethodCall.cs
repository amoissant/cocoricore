﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CocoriCore.Common
{
    public static class MethodCall
    {
        public static IMethodCall<TIn, object> FromFunction<TIn>(Action<TIn> action)
        {
            return new ActionCall<TIn>(action);
        }

        public static IMethodCall<TIn, object> FromExpression<TIn>(Expression<Action<TIn>> action)
        {
            return new ActionCall<TIn>(action);
        }

        public static IMethodCall<TIn, object> FromFunction<TIn>(Func<TIn, Task> action)
        {
            return new AsyncActionCall<TIn>(action);
        }

        public static IMethodCall<TIn, object> FromExpression<TIn>(Expression<Func<TIn, Task>> action)
        {
            return new AsyncActionCall<TIn>(action);
        }

        public static IMethodCall<TIn, object> FromFunction<TService, TIn>(Action<TService, TIn> action)
        {
            return new ActionCall<TService, TIn>(action);
        }

        public static IMethodCall<TIn, object> FromExpression<TService, TIn>(Expression<Action<TService, TIn>> action)
        {
            return new ActionCall<TService, TIn>(action);
        }

        public static IMethodCall<TIn, object> FromFunction<TService, TIn>(Func<TService, TIn, Task> action)
        {
            return new AsyncActionCall<TService, TIn>(action);
        }

        public static IMethodCall<TIn, object> FromExpression<TService, TIn>(Expression<Func<TService, TIn, Task>> action)
        {
            return new AsyncActionCall<TService, TIn>(action);
        }

        public static IMethodCall<TIn, object> FromFunction<TService1, TService2, TIn>(Action<TService1, TService2, TIn> action)
        {
            return new ActionCall<TService1, TService2, TIn>(action);
        }

        public static IMethodCall<TIn, object> FromExpression<TService1, TService2, TIn>(Expression<Action<TService1, TService2, TIn>> action)
        {
            return new ActionCall<TService1, TService2, TIn>(action);
        }

        public static IMethodCall<TIn, object> FromFunction<TService1, TService2, TIn>(Func<TService1, TService2, TIn, Task> action)
        {
            return new AsyncActionCall<TService1, TService2, TIn>(action);
        }

        public static IMethodCall<TIn, object> FromExpression<TService1, TService2, TIn>(Expression<Func<TService1, TService2, TIn, Task>> action)
        {
            return new AsyncActionCall<TService1, TService2, TIn>(action);
        }

        public static IMethodCall<TIn, TOut> FromFunction<TIn, TOut>(Func<TIn, TOut> func)
        {
            return new FuncCall<TIn, TOut>(func);
        }

        public static IMethodCall<TIn, TOut> FromExpression<TIn, TOut>(Expression<Func<TIn, TOut>> func)
        {
            return new FuncCall<TIn, TOut>(func);
        }

        public static IMethodCall<TIn, TOut> FromFunction<TIn, TOut>(Func<TIn, Task<TOut>> func)
        {
            return new AsyncFuncCall<TIn, TOut>(func);
        }

        public static IMethodCall<TIn, TOut> FromExpression<TIn, TOut>(Expression<Func<TIn, Task<TOut>>> func)
        {
            return new AsyncFuncCall<TIn, TOut>(func);
        }

        public static IMethodCall<TIn, TOut> FromFunction<TService, TIn, TOut>(Func<TService, TIn, TOut> func)
        {
            return new FuncCall<TService, TIn, TOut>(func);
        }

        public static IMethodCall<TIn, TOut> FromExpression<TService, TIn, TOut>(Expression<Func<TService, TIn, TOut>> func)
        {
            return new FuncCall<TService, TIn, TOut>(func);
        }

        public static IMethodCall<TIn, TOut> FromFunction<TService, TIn, TOut>(Func<TService, TIn, Task<TOut>> func)
        {
            return new AsyncFuncCall<TService, TIn, TOut>(func);
        }

        public static IMethodCall<TIn, TOut> FromExpression<TService, TIn, TOut>(Expression<Func<TService, TIn, Task<TOut>>> func)
        {
            return new AsyncFuncCall<TService, TIn, TOut>(func);
        }

        public static IMethodCall<TIn, TOut> FromFunction<TService1, TService2, TIn, TOut>(Func<TService1, TService2, TIn, TOut> func)
        {
            return new FuncCall<TService1, TService2, TIn, TOut>(func);
        }

        public static IMethodCall<TIn, TOut> FromExpression<TService1, TService2, TIn, TOut>(Expression<Func<TService1, TService2, TIn, TOut>> func)
        {
            return new FuncCall<TService1, TService2, TIn, TOut>(func);
        }

        public static IMethodCall<TIn, TOut> FromFunction<TService1, TService2, TIn, TOut>(Func<TService1, TService2, TIn, Task<TOut>> func)
        {
            return new AsyncFuncCall<TService1, TService2, TIn, TOut>(func);
        }

        public static IMethodCall<TIn, TOut> FromExpression<TService1, TService2, TIn, TOut>(Expression<Func<TService1, TService2, TIn, Task<TOut>>> func)
        {
            return new AsyncFuncCall<TService1, TService2, TIn, TOut>(func);
        }
    }
}