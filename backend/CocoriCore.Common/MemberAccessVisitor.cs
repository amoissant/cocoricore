﻿using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;

namespace CocoriCore.Common
{
    public class MemberAccessVisitor : ExpressionVisitor
    {
        public List<MemberInfo> DependencyMembers;
        public List<MemberInfo> ProjectionMembers;
        private ParameterExpression _dependencyParameter;
        private ParameterExpression _projectionParameter;

        public MemberAccessVisitor(ParameterExpression dependencyParameter, ParameterExpression projectionParameter)
        {
            _dependencyParameter = dependencyParameter;
            _projectionParameter = projectionParameter;
            DependencyMembers = new List<MemberInfo>();
            ProjectionMembers = new List<MemberInfo>();
        }

        protected override Expression VisitMember(MemberExpression node)
        {
            if (node.Expression == _dependencyParameter)
                DependencyMembers.Add(node.Member);
            else if (node.Expression == _projectionParameter)
                ProjectionMembers.Add(node.Member);
            return node;
        }
    }
}
