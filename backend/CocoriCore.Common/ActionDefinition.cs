using System;
using System.Threading.Tasks;

namespace CocoriCore.Common
{
    public class ActionDefinition
    {
        public Type Type;
        public Func<object, Task> Action;

        public ActionDefinition(Type type, Func<object, Task> action)
        {
            Type = type;
            Action = action;
        }
    }
}