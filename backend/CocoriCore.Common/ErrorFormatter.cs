﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;

namespace CocoriCore.Common
{
    public class ErrorFormatter
    {
        private Func<object, string> _serializeFunc;
        public ErrorFormatter(Func<object, string> serializeFunc = null)
        {
            _serializeFunc = o => 
            {
                serializeFunc = serializeFunc ?? SerializeJson;
                try
                {
                    return serializeFunc(o);
                }
                catch(Exception e)
                {
                    return $"Unable to serialize exception's datas -> {e.Message}";
                }
            };
        }

        private string SerializeJson(object obj)
        {
            var jsonBytes = Utf8Json.JsonSerializer.NonGeneric.Serialize(obj);
            return Utf8Json.JsonSerializer.PrettyPrint(jsonBytes);
        }

        public virtual string Format(Exception exception)
        {
            var formattedMessage = $"{string.Join(Environment.NewLine, GetMessages(exception))}{Environment.NewLine}";
            
            object datas = GetDatas(exception);
            var formattedDatas = string.Empty;
            if (datas != null)
            {
                formattedDatas = _serializeFunc(datas).Replace(Environment.NewLine, Environment.NewLine + "   ");
                formattedDatas = $"--- Datas:{Environment.NewLine}   {formattedDatas}{Environment.NewLine}";
            }
            
            var stackTraces = GetStackTraces(exception);
            var formattedStackTraces = string.Empty;
            if (stackTraces.Count() > 0)
            {
                formattedStackTraces = $"{string.Join(Environment.NewLine, stackTraces)}{Environment.NewLine}";
            }
            return $"{exception.GetType()}: {formattedMessage}{formattedDatas}{formattedStackTraces}";
        }

        protected virtual IEnumerable<string> GetMessages(Exception exception)
        {
            var messages = new List<string> { exception.Message };
            exception = exception.InnerException;
            while (exception != null)
            {
                messages.Add($"--- Inner message: {exception.Message}");
                exception = exception.InnerException;
            }
            return messages;
        }

        protected virtual object GetDatas(Exception exception)
        {
            //TODO utiliser méthode exception.data.ToExpando() ici
            var datas = new ExpandoObject() as IDictionary<string, object>;
            foreach (var key in exception.Data.Keys)
            {
                datas[key.ToString()] = exception.Data[key];
            }
            if (exception.InnerException != null)
            {
                var innerDatas = GetDatas(exception.InnerException);
                if (innerDatas != null)
                {
                    datas["InnerDatas"] = innerDatas;
                }
            }
            return datas.Count > 0 ? datas : null;
        }

        protected virtual IEnumerable<string> GetStackTraces(Exception exception)
        {
            var stackTraces = new List<string>();
            if (exception.StackTrace != null)
            {
                stackTraces.Add(exception.StackTrace);
            }
            exception = exception.InnerException;
            while (exception != null)
            {
                if (exception.StackTrace != null)
                {
                    stackTraces.Insert(0, "--- End of stack trace ---");
                    stackTraces.Insert(0, $"{exception.StackTrace}");
                }
                exception = exception.InnerException;
            }
            return stackTraces;
        }
    }
}
