﻿using CocoriCore.Types.Extension;
using System;

namespace CocoriCore.Common.Diagnostic
{
    public static class ObjectExtensions
    {
        public static string TrySerializeToJson(this object obj)
        {
            try
            {
                return Utf8Json.JsonSerializer.ToJsonString(obj);
            }
            catch (Exception e)
            {
                return $"Error while serializing message for debug : {e.Message}";
            }
        }

        public static string ToJson(this object obj, bool indent = false)
        {
            var jsonBytes = Utf8Json.JsonSerializer.Serialize(obj);
            if(indent)
            {
                return Utf8Json.JsonSerializer.PrettyPrint(jsonBytes);
            }
            else
            {
                return jsonBytes.ToUtf8();
            }
        }

        public static string GetPrettyType(this object obj, bool full = false)
        {
            return obj?.GetType()?.GetPrettyName(full);
        }
    }
}