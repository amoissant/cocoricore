using System;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace CocoriCore.Common
{
    public static class StringExtensions
    {
        public static string RemoveDiacritics(this string text)
        {
            if (string.IsNullOrWhiteSpace(text))
                return text;

            text = text.Normalize(NormalizationForm.FormD);
            var chars = text.Where(c => CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark).ToArray();
            return new string(chars).Normalize(NormalizationForm.FormC);
        }

        public static string GetJsonValueOrNull(this string json, string fieldName)
        {
            TryGetJsonValue(json, fieldName, out string fieldValue);
            return fieldValue;
        }

        public static bool TryGetJsonValue(this string json, string fieldName, out string fieldValue)
        {
            var regex = new Regex($"\"{fieldName}\"\\s*:\\s*(?<value>[^\"]+)\"", RegexOptions.IgnoreCase);//TODO g�rer les int ou y'a pas les ""
            var match = regex.Match(json);
            fieldValue = match.Success ? match.Groups["value"]?.Value : null;
            return match.Success;
        }

        public static byte[] GetBase64Bytes(this string base64String)
        {
            if(base64String == null)
            {
                return null;
            }
            else
            {
                return Convert.FromBase64String(base64String);
            }
        }

        public static byte[] GetUtf8Bytes(this string utf8String)
        {
            if (utf8String == null)
            {
                return null;
            }
            else
            {
                return Encoding.UTF8.GetBytes(utf8String);
            }
        }

        public static string RemoveUTF8BOM(this string utf8String)
        {
            var bytes = Encoding.UTF8.GetBytes(utf8String);
            var preamble = Encoding.UTF8.GetPreamble();
            if (bytes.HasUTF8BOM())
            {
                var bytesWithoutBOM = new byte[bytes.Length - preamble.Length];
                Array.Copy(bytes, preamble.Length, bytesWithoutBOM, 0, bytesWithoutBOM.Length);
                return Encoding.UTF8.GetString(bytesWithoutBOM);
            }
            else
            {
                return utf8String;
            }
        }

        public static bool HasUTF8BOM(this string utf8String)
        {
            return Encoding.UTF8.GetBytes(utf8String).HasUTF8BOM();
        }

        public static bool HasUTF8BOM(this byte[] bytes)
        {
            var preamble = Encoding.UTF8.GetPreamble();
            var startsWithBOM = bytes.Length >= preamble.Length;
            for (var i = 0; i < preamble.Length; i++)
            {
                startsWithBOM = startsWithBOM && preamble[i] == bytes[i];
            }
            return startsWithBOM;
        }

        public static string AddUTF8BOM(this string utf8String)
        {
            var bytes = Encoding.UTF8.GetBytes(utf8String);
            var preamble = Encoding.UTF8.GetPreamble();
            if (!bytes.HasUTF8BOM())
            {
                var bytesWithBOM = new byte[bytes.Length + preamble.Length];
                Array.Copy(preamble, 0, bytesWithBOM, 0, preamble.Length);
                Array.Copy(bytes, 0, bytesWithBOM, preamble.Length, bytes.Length);
                return Encoding.UTF8.GetString(bytesWithBOM);
            }
            else
            {
                return utf8String;
            }
        }

        public static string Unescape(this string text)
        {
            return text?.Replace(@"\\", @"\");
        }

        public static string Escape(this string text)
        {
            return text?.Replace(@"\", @"\\");
        }

        public static Regex ToWildcardRegex(this string text)
        {
            return new Regex(text.Replace(".", "\\.").Replace("*", ".*"), RegexOptions.IgnoreCase | RegexOptions.Compiled);
        }

        public static bool IsNullOrEmpty(this string value)
        {
            return string.IsNullOrEmpty(value);
        }

        public static bool IsNotNullOrEmpty(this string value)
        {
            return !string.IsNullOrEmpty(value);
        }
    }
}