﻿using System;

namespace CocoriCore.Common
{
    public static class DateTimeExtensions
    {
        public static bool IsDefault(this DateTime dateTime)
        {
            return dateTime == default;
        }

        public static bool IsDateWithoutTime(this DateTime dateTime)
        {
            return dateTime.TimeOfDay == TimeSpan.Zero;
        }

        public static DateTime AsUtc(this DateTime dateTime)
        {
            if (dateTime.Kind != DateTimeKind.Utc)
            {
                var utcDateTime = dateTime.ToUniversalTime();
                var utcHourDelta = dateTime.Hour - utcDateTime.Hour;
                var utcMinuteDelta = dateTime.Minute - utcDateTime.Minute;
                var utcSecondDelta = dateTime.Second - utcDateTime.Second;
                var utcDelta = new TimeSpan(utcHourDelta, utcMinuteDelta, utcSecondDelta);
                return utcDateTime.Add(utcDelta);
            }
            return dateTime;
        }

        public static DateTime ToLocalAsUtc(this DateTime dateTime)
        {
            if (dateTime.Kind != DateTimeKind.Local)
            {
                return dateTime.ToLocalTime().AsUtc();
            }
            return dateTime;
        }

        public static long ToUnixTimeSeconds(this DateTime dateTime)
        {
            return ((DateTimeOffset)dateTime).ToUnixTimeSeconds();
        }

        public static DateTime RoundMilliseconds(this DateTime dateTime)
        {
            var roundedMilliseconds = (int)Math.Round((double)dateTime.Millisecond, 3);
            return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, dateTime.Second, roundedMilliseconds, dateTime.Kind);
        }
    }
}
