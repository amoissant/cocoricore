﻿using System;
using System.IO;

namespace CocoriCore.Common
{
    public class FileStreamResponse
    {
        public Lazy<Stream> Content;
        public string MimeType;
        public string FileName;
    }
}