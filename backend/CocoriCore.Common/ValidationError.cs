﻿namespace CocoriCore.Common
{
    public class ValidationError
    {
        public string Code;
        public string Field;
        public string Message;
    }
}