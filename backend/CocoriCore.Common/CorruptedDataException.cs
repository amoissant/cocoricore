﻿using System;

namespace CocoriCore.Common
{
    public class CorruptedDataException : Exception
    {
        public CorruptedDataException(string message)
            : base(message)
        {
        }
    }
}
