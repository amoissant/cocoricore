﻿using System;

namespace CocoriCore.Common
{
    public interface IFactory
    {
        T Create<T>();
        object Create(Type type);
    }
}