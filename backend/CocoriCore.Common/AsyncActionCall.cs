﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CocoriCore.Common
{
    public class AsyncActionCall<TIn> : IMethodCall<TIn, object>
    {
        private static Task<object> _defaultResult = Task.FromResult((object)null);
        public AsyncActionCall(Func<TIn, Task> action)
        {
            Func = action;
        }

        public AsyncActionCall(Expression<Func<TIn, Task>> expression) : this(expression.Compile())
        {
            Expression = expression;
        }

        public LambdaExpression Expression { get; }
        public Func<TIn, Task> Func { get; }
        public bool IsAsync => false;
        public object Delegate => Func;

        public object Execute(IUnitOfWork unitOfWork, TIn input)
        {
            return ExecuteAsync(unitOfWork, input).GetAwaiter().GetResult();
        }

        public async Task<object> ExecuteAsync(IUnitOfWork unitOfWork, TIn input)
        {
            await Func(input);
            return _defaultResult;
        }
    }

    public class AsyncActionCall<TService, TIn> : IMethodCall<TIn, object>
    {
        private static Task<object> _defaultResult = Task.FromResult((object)null);
        public AsyncActionCall(Func<TService, TIn, Task> action)
        {
            Func = action;
        }

        public AsyncActionCall(Expression<Func<TService, TIn, Task>> expression) : this(expression.Compile())
        {
            Expression = expression;
        }

        public LambdaExpression Expression { get; }
        public Func<TService, TIn, Task> Func { get; }
        public bool IsAsync => false;
        public object Delegate => Func;

        public object Execute(IUnitOfWork unitOfWork, TIn input)
        {
            return ExecuteAsync(unitOfWork, input).GetAwaiter().GetResult();
        }

        public async Task<object> ExecuteAsync(IUnitOfWork unitOfWork, TIn input)
        {
            var service = unitOfWork.Resolve<TService>();
            await Func(service, input);
            return _defaultResult;
        }
    }

    public class AsyncActionCall<TService1, TService2, TIn> : IMethodCall<TIn, object>
    {
        private static Task<object> _defaultResult = Task.FromResult((object)null);
        public AsyncActionCall(Func<TService1, TService2, TIn, Task> action)
        {
            Func = action;
        }

        public AsyncActionCall(Expression<Func<TService1, TService2, TIn, Task>> expression) : this(expression.Compile())
        {
            Expression = expression;
        }

        public LambdaExpression Expression { get; }
        public Func<TService1, TService2, TIn, Task> Func { get; }
        public bool IsAsync => false;

        public object Delegate => Func;

        public object Execute(IUnitOfWork unitOfWork, TIn input)
        {
            return ExecuteAsync(unitOfWork, input).GetAwaiter().GetResult();
        }

        public async Task<object> ExecuteAsync(IUnitOfWork unitOfWork, TIn input)
        {
            var service1 = unitOfWork.Resolve<TService1>();
            var service2 = unitOfWork.Resolve<TService2>();
            await Func(service1, service2, input);
            return _defaultResult;
        }
    }
}