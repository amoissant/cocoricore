namespace CocoriCore.Common
{
    public class FileResponse
    {
        public byte[] Content;
        public string MimeType;
        public string FileName;
    }
}