﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CocoriCore.Common
{
    public static class BytesExtensions
    {
        public static string ToUtf8(this byte[] bytes)
        {
            if (bytes == null)
            {
                return null;
            }
            else
            {
                return Encoding.UTF8.GetString(bytes);
            }
        }

        public static string ToBase64(this byte[] bytes)
        {
            if (bytes == null)
            {
                return null;
            }
            else
            {
                return Convert.ToBase64String(bytes);
            }
        }

        public static string ToBase64(this IEnumerable<byte> bytes)
        {
            return bytes.ToArray().ToBase64();
        }

        public static string ToHex(this byte[] bytes, bool upperCase = false)
        {
            StringBuilder result = new StringBuilder(bytes.Length * 2);

            for (int i = 0; i < bytes.Length; i++)
                result.Append(bytes[i].ToString(upperCase ? "X2" : "x2"));

            return result.ToString();
        }
    }
}