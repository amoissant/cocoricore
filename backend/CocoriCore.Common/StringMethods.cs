﻿using System.Linq;
using System.Reflection;

namespace CocoriCore.Common
{
    public static class StringMethods
    {
        private static MethodInfo _contains;

        public static MethodInfo Contains
        {
            get
            {
                if (_contains == null)
                {
                    _contains = typeof(string)
                        .GetMethods()
                        .Where(m =>
                            m.Name == nameof(string.Contains) &&
                            m.GetParameters().Length == 1 &&
                            m.GetParameters()[0].ParameterType == typeof(string))
                        .Single();
                }
                return _contains;
            }
        }
    }
}
