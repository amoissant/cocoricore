﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CocoriCore.Common
{
    public class AsyncFuncCall<TIn, TOut> : IMethodCall<TIn, TOut>
    {
        public AsyncFuncCall(Func<TIn, Task<TOut>> func)
        {
            Func = func;
        }

        public AsyncFuncCall(Expression<Func<TIn, Task<TOut>>> expression) : this(expression.Compile())
        {
            Expression = expression;
        }

        public LambdaExpression Expression { get; }
        public Func<TIn, Task<TOut>> Func { get; }
        public bool IsAsync => true;
        public object Delegate => Func;

        public TOut Execute(IUnitOfWork unitOfWork, TIn input)
        {
            return ExecuteAsync(unitOfWork, input).GetAwaiter().GetResult();
        }

        public async Task<TOut> ExecuteAsync(IUnitOfWork unitOfWork, TIn input)
        {
            return await Func(input);
        }
    }

    public class AsyncFuncCall<TService, TIn, TOut> : IMethodCall<TIn, TOut>
    {
        public AsyncFuncCall(Func<TService, TIn, Task<TOut>> func)
        {
            Func = func;
        }

        public AsyncFuncCall(Expression<Func<TService, TIn, Task<TOut>>> expression) : this(expression.Compile())
        {
            Expression = expression;
        }

        public LambdaExpression Expression { get; }
        public Func<TService, TIn, Task<TOut>> Func { get; }
        public bool IsAsync => true;
        public object Delegate => Func;

        public TOut Execute(IUnitOfWork unitOfWork, TIn input)
        {
            return ExecuteAsync(unitOfWork, input).GetAwaiter().GetResult();
        }

        public async Task<TOut> ExecuteAsync(IUnitOfWork unitOfWork, TIn input)
        {
            var service = unitOfWork.Resolve<TService>();
            return await Func(service, input);
        }
    }

    public class AsyncFuncCall<TService1, TService2, TIn, TOut> : IMethodCall<TIn, TOut>
    {
        public AsyncFuncCall(Func<TService1, TService2, TIn, Task<TOut>> func)
        {
            Func = func;
        }

        public AsyncFuncCall(Expression<Func<TService1, TService2, TIn, Task<TOut>>> expression) : this(expression.Compile())
        {
            Expression = expression;
        }

        public LambdaExpression Expression { get; }
        public Func<TService1, TService2, TIn, Task<TOut>> Func { get; }
        public bool IsAsync => true;
        public object Delegate => Func;

        public TOut Execute(IUnitOfWork unitOfWork, TIn input)
        {
            return ExecuteAsync(unitOfWork, input).GetAwaiter().GetResult();
        }

        public async Task<TOut> ExecuteAsync(IUnitOfWork unitOfWork, TIn input)
        {
            var service1 = unitOfWork.Resolve<TService1>();
            var service2 = unitOfWork.Resolve<TService2>();
            return await Func(service1, service2, input);
        }
    }
}