﻿namespace CocoriCore.Common
{
    public interface IObjectSerializer
    {
        T Deserialize<T>(string json, bool useCamelCase = true, bool withTypeInfos = true);
        string Serialize<T>(T obj, bool useCamelCase = true, bool withTypeInfos = true);
    }
}