﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CocoriCore.Common
{
    public static class ArrayExtensions
    {
        public static T[] Append<T>(this T[] array, T[] other)
        {
            var originalLength = array.Length;
            Array.Resize(ref array, array.Length + other.Length);
            Array.Copy(other, 0, array, originalLength, other.Length);
            return array;
        }

        public static (T[], T[]) SplitAt<T>(this T[] array, int index)
        {
            if (index < 0) index = array.Length - Math.Abs(index);
            return SplitAt_(array, index);
        }

        private static (T[], T[]) SplitAt_<T>(this T[] array, int index)
        {
            var array1 = new T[index];
            var array2 = new T[array.Length - index];
            Array.Copy(array, 0, array1, 0, array1.Length);
            Array.Copy(array, index, array2, 0, array2.Length);
            return (array1, array2);
        }
    }
}
