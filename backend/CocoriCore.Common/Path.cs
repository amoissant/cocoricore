﻿using CocoriCore.Collection.Extensions;
using HeyRed.Mime;
using System;
using System.Collections;
using System.Linq;

namespace CocoriCore.Common
{
    public class Path : IComparable<Path>
    {
        public virtual string Root { get; protected set; }
        public virtual string[] Segments { get; protected set; }
        public bool IsAbsolute => Root != string.Empty;

        public Path()
        {
            Segments = new string[0];
            Root = string.Empty;
        }

        public Path(string path)
        {
            Segments = GetSegments(path);
            Root = GetRoot(path);
            if(Segments.Length > 0 && Segments.First() == Root)
            {
                Segments = Segments.RemoveAt(0);
            }
        }

        public Path(params object[] paths)
        {
            Segments = GetSegments(paths);
            Root = GetRoot(paths.FirstOrDefault()?.ToString());
            if (Segments.Length > 0 && Segments.First() == Root)
            {
                Segments = Segments.RemoveAt(0);
            }
        }

        public Path(string root, string[] segments)
        {
            Segments = segments.ToArray();
            Root = root;
        }

        private string GetRoot(string path)
        {
            if (path?.StartsWith("/") == true)
            {
                return "/";
            }
            if (path?.StartsWith("\\\\") == true)
            {
                return "\\\\";
            }
            if (path != null && path.Length >= 2 && path[1] == ':')
            {
                return $"{path[0]}{path[1]}";
            }
            return string.Empty;
        }

        private string[] GetSegments(params object[] paths)
        {
            return paths.SelectMany(x => GetSegments(x)).ToArray();
        }

        private string[] GetSegments(object obj)
        {
            if (obj == null)
            {
                return new string[0];
            }
            else if (obj is Path path)
            {
                return path.Segments;
            }
            else if (obj is IEnumerable enumerable && !(obj is string))
            {
                return enumerable
                    .Cast<object>()
                    .SelectMany(x => GetSegments(x))
                    .ToArray();
            }
            else
            {
                return obj
                    .ToString()
                    .Replace("\\", "/")
                    .Split('/')
                    .Where(x => !string.IsNullOrWhiteSpace(x))
                    .ToArray();
            }
        }

        public virtual Path Append(params object[] paths)
        {
            return new Path(Root, Segments, paths);
        }

        public string GetExtension()
        {
            var lastSegment = Segments.Last();
            var dotIndex = lastSegment.LastIndexOf('.');
            if(dotIndex != -1)
            {
                return lastSegment.Substring(dotIndex);
            }
            else
            {
                return null;
            }
        }

        public Path RemoveExtension()
        {
            var newSegments = new string[Segments.Length];
            Array.Copy(Segments, newSegments, Segments.Length);
            var lastSegment = Segments.Last();
            var dotIndex = lastSegment.LastIndexOf('.');
            if (dotIndex != -1)
            {
                newSegments[Segments.Length - 1] = lastSegment.Substring(0, dotIndex);
            }
            return new Path(newSegments);
        }

        public Path AddExtension(string extension)
        {
            if (string.IsNullOrEmpty(extension))
            {
                return this;
            }
            extension = extension.StartsWith(".") ? extension : "." + extension;
            var newFileName = GetName() + extension;
            return new Path(GetParent().Append(newFileName));
        }

        public Path AddExtensionFromFileName(string fileName)
        {
            int dotIndex = fileName.LastIndexOf('.');
            if (dotIndex < 0)
            {
                return this;
            }
            return AddExtension(fileName.Substring(dotIndex));
        }

        public Path AddExtensionFromMimeType(string mimeType)
        {
            var extension = MimeTypes.GetExtension(mimeType);
            return AddExtension(extension);
        }

        public string GetMimeType()
        {
            return MimeTypes.GetMimeType(GetExtension());
        }

        public Path RelativeTo(Path path)
        {
            var newSegments = Segments.ToList();
            foreach (var segment in path.Segments)
            {
                if (newSegments.FirstOrDefault() == segment)
                {
                    newSegments.RemoveAt(0);
                }
                else
                {
                    throw new InvalidOperationException($"Path '{path}' must be a parent of '{this}'.");
                }
            }
            return new Path(newSegments);
        }

        public virtual Path GetParent()
        {
            if (IsEmpty())
            {
                throw new InvalidOperationException("Can't get parent of an empty path.");
            }
            var newSegments = new string[Segments.Length - 1];
            Array.Copy(Segments, newSegments, Segments.Length - 1);
            return new Path(newSegments);
        }

        public virtual string GetName()
        {
            return Segments.Last();
        }

        public virtual string GetFileName()
        {
            return Segments.Last();
        }

        public virtual Path RemoveFirstSegment()
        {
            if (IsEmpty())
            {
                throw new InvalidOperationException("Can't remove segment on an empty path.");
            }
            var newSegments = new string[Segments.Length - 1];
            Array.Copy(Segments, 1, newSegments, 0, Segments.Length - 1);
            return new Path(newSegments);
        }

        public virtual bool IsEmpty()
        {
            return Segments.Length == 0;
        }

        public override string ToString()
        {
            var concatenedSegments = string.Join("/", Segments);
            var root = Root.EndsWith("/") || Root.EndsWith("\\") ? Root : Root + "/";
            return IsAbsolute ? $"{root}{concatenedSegments}" : concatenedSegments;
        }

        public override bool Equals(object objet)
        {
            return objet is Path path && path == this;
        }

        public bool Equals(object objet, bool caseSensitive)
        {
            var stringComparison = caseSensitive ? StringComparison.InvariantCulture : StringComparison.InvariantCultureIgnoreCase;
            return objet is Path path && string.Equals(path, this, stringComparison);
        }

        public override int GetHashCode()
        {
            return 13 ^ ToString().GetHashCode();
        }

        public int CompareTo(Path other)
        {
            return ToString().CompareTo(other.ToString());
        }

        public static implicit operator Path(string value)
        {
            return new Path(value);
        }

        public static implicit operator string(Path path)
        {
            return path?.ToString();
        }

        public static bool operator ==(Path path1, Path path2)
        {
            return Equals(path1?.ToString(), path2?.ToString());
        }

        public static bool operator !=(Path path1, Path path2)
        {
            return !(path1 == path2);
        }
    }
}
