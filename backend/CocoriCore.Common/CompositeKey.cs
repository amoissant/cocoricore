﻿using System.Collections.Generic;
using System.Linq;

namespace CocoriCore.Common
{
    public class CompositeKey
    {
        private object[] _keys;
        public CompositeKey(IEnumerable<object> keys)
        {
            _keys = keys.ToArray();
        }

        public override bool Equals(object obj)
        {
            if (obj is CompositeKey composite && composite._keys.Length == _keys.Length)
            {
                for (var i = 0; i < _keys.Length; i++)
                {
                    if (!object.Equals(composite._keys[i], _keys[i]))
                    {
                        return false;
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            var hashCode = 19;
            for (var i = 0; i < _keys.Length; i++)
            {
                hashCode ^= i.GetHashCode() ^ (_keys[i] == null ? 0 : _keys[i].GetHashCode());
            }
            return hashCode;
        }
    }
}
