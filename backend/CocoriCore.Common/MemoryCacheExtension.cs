﻿using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace CocoriCore.Common
{
    public static class MemoryCacheExtension
    {
        private static ConcurrentDictionary<object, SemaphoreSlim> _locks = new ConcurrentDictionary<object, SemaphoreSlim>();

        //TODO GetCached n'est pas très rapide, revoir stratégie de mise en cache
        public static TValue GetCached<T, TValue>(this IMemoryCache cache, T instance, Expression<Func<T, TValue>> methodExpression)
        {
            var cacheKey = GetCacheKey<T>(methodExpression);
            TValue cacheValue = default;
            if (!cache.TryGetValue(cacheKey, out cacheValue))
            {
                var @lock = _locks.GetOrAdd(cacheKey, X => new SemaphoreSlim(1, 1));
                @lock.Wait();
                try
                {
                    if (!cache.TryGetValue(cacheKey, out cacheValue))
                    {
                        cacheValue = methodExpression.Compile().Invoke(instance);
                        cache.Set(cacheKey, cacheValue);
                    }
                }
                finally
                {
                    @lock.Release();
                }
            }
            return cacheValue;
        }

        public static async Task<TValue> GetCachedAsync<T, TValue>(this IMemoryCache cache, T instance, Expression<Func<T, Task<TValue>>> methodExpression)
        {
            var cacheKey = GetCacheKey<T>(methodExpression);
            TValue cacheValue = default;
            if (!cache.TryGetValue(cacheKey, out cacheValue))
            {
                var @lock = _locks.GetOrAdd(cacheKey, X => new SemaphoreSlim(1, 1));
                await @lock.WaitAsync();
                try
                {
                    if (!cache.TryGetValue(cacheKey, out cacheValue))
                    {
                        cacheValue = await methodExpression.Compile().Invoke(instance);
                        cache.Set(cacheKey, cacheValue);
                    }
                }
                finally
                {
                    @lock.Release();
                }
            }
            return cacheValue;
        }

        private static string GetCacheKey<T>(LambdaExpression methodExpression)
        {
            var methodCall = (MethodCallExpression)methodExpression.Body;
            var argumentsValues = methodCall.Arguments.Select(x => ExpressionUtilities.GetValueWithoutCompiling(x));
            var cacheKey = $"{typeof(T)}.{methodCall.Method.Name}({string.Join(", ", argumentsValues)})";
            return cacheKey;
        }
    }
}
