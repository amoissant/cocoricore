﻿using Microsoft.Extensions.Logging;
using System;

namespace CocoriCore.Common
{
    public static class LoggerExtensions
    {
        private static ErrorFormatter _errorFormatter = new ErrorFormatter();
        public static void LogError(this ILogger logger, Exception exception)
        {
            logger.Log<object>(LogLevel.Error, 0, null, exception, (s, e) => _errorFormatter.Format(e));
        }
    }
}
