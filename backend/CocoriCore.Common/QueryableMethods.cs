﻿using CocoriCore.Types.Extension;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace CocoriCore.Common
{

    public static class QueryableMethods
    {
        private static MethodInfo _any;
        private static MethodInfo _single;
        private static MethodInfo _singleOrDefault;
        private static MethodInfo _first;
        private static MethodInfo _firstOrDefault;
        private static MethodInfo _count;
        private static MethodInfo _sumInt;
        private static MethodInfo _sumNullableInt;
        private static MethodInfo _sumLong;
        private static MethodInfo _sumNullableLong;
        private static MethodInfo _sumFloat;
        private static MethodInfo _sumNullableFloat;
        private static MethodInfo _sumDouble;
        private static MethodInfo _sumNullableDouble;
        private static MethodInfo _sumDecimal;
        private static MethodInfo _sumNullableDecimal;
        private static MethodInfo _min;
        private static MethodInfo _max;
        private static MethodInfo _defaultIfEmpty;
        private static MethodInfo _cast;
        private static MethodInfo _where;
        private static MethodInfo _distinct;
        private static MethodInfo _distinctComparer;
        private static MethodInfo _orderBy;
        private static MethodInfo _orderByDescending;
        private static MethodInfo _skip;
        private static MethodInfo _take;

        public static MethodInfo Any
        {
            get
            {
                if (_any == null)
                {
                    _any = typeof(Queryable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == nameof(Queryable.Any) &&
                            x.GetParameters().Count() == 1 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)))
                        .Single();
                }
                return _any;
            }
        }

        public static MethodInfo Single
        {
            get
            {
                if (_single == null)
                {
                    _single = typeof(Queryable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == nameof(Queryable.Single) &&
                            x.GetParameters().Count() == 1 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)))
                        .Single();
                }
                return _single;
            }
        }

        public static MethodInfo SingleOrDefault
        {
            get
            {
                if (_singleOrDefault == null)
                {
                    _singleOrDefault = typeof(Queryable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == nameof(Queryable.SingleOrDefault) &&
                            x.GetParameters().Count() == 1 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)))
                        .Single();
                }
                return _singleOrDefault;
            }
        }

        public static MethodInfo First
        {
            get
            {
                if (_first == null)
                {
                    _first = typeof(Queryable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == nameof(Queryable.First) &&
                            x.GetParameters().Count() == 1 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)))
                        .Single();
                }
                return _first;
            }
        }

        public static MethodInfo FirstOrDefault
        {
            get
            {
                if (_firstOrDefault == null)
                {
                    _firstOrDefault = typeof(Queryable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == nameof(Queryable.FirstOrDefault) &&
                            x.GetParameters().Count() == 1 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)))
                        .Single();
                }
                return _firstOrDefault;
            }
        }

        public static MethodInfo Count
        {
            get
            {
                if (_count == null)
                {
                    _count = typeof(Queryable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == nameof(Queryable.Count) &&
                            x.GetParameters().Count() == 1 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)))
                        .Single();
                }
                return _count;
            }
        }

        public static MethodInfo SumInt
        {
            get
            {
                if (_sumInt == null)
                {
                    _sumInt = typeof(Queryable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == nameof(Queryable.Sum) &&
                            x.GetParameters().Count() == 1 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)) &&
                            x.GetParameters()[0].ParameterType.GenericTypeArguments[0] == typeof(int))
                        .Single();
                }
                return _sumInt;
            }
        }

        public static MethodInfo SumNullableInt
        {
            get
            {
                if (_sumNullableInt == null)
                {
                    _sumNullableInt = typeof(Queryable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == nameof(Queryable.Sum) &&
                            x.GetParameters().Count() == 1 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)) &&
                            x.GetParameters()[0].ParameterType.GenericTypeArguments[0] == typeof(int?))
                        .Single();
                }
                return _sumNullableInt;
            }
        }

        public static MethodInfo SumFloat
        {
            get
            {
                if (_sumFloat == null)
                {
                    _sumFloat = typeof(Queryable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == nameof(Queryable.Sum) &&
                            x.GetParameters().Count() == 1 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)) &&
                            x.GetParameters()[0].ParameterType.GenericTypeArguments[0] == typeof(float))
                        .Single();
                }
                return _sumFloat;
            }
        }

        public static MethodInfo SumNullableFloat
        {
            get
            {
                if (_sumNullableFloat == null)
                {
                    _sumNullableFloat = typeof(Queryable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == nameof(Queryable.Sum) &&
                            x.GetParameters().Count() == 1 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)) &&
                            x.GetParameters()[0].ParameterType.GenericTypeArguments[0] == typeof(float?))
                        .Single();
                }
                return _sumNullableFloat;
            }
        }

        public static MethodInfo SumLong
        {
            get
            {
                if (_sumLong == null)
                {
                    _sumLong = typeof(Queryable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == nameof(Queryable.Sum) &&
                            x.GetParameters().Count() == 1 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)) &&
                            x.GetParameters()[0].ParameterType.GenericTypeArguments[0] == typeof(long))
                        .Single();
                }
                return _sumLong;
            }
        }

        public static MethodInfo SumNullableLong
        {
            get
            {
                if (_sumNullableLong == null)
                {
                    _sumNullableLong = typeof(Queryable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == nameof(Queryable.Sum) &&
                            x.GetParameters().Count() == 1 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)) &&
                            x.GetParameters()[0].ParameterType.GenericTypeArguments[0] == typeof(long?))
                        .Single();
                }
                return _sumNullableLong;
            }
        }

        public static MethodInfo SumDouble
        {
            get
            {
                if (_sumDouble == null)
                {
                    _sumDouble = typeof(Queryable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == nameof(Queryable.Sum) &&
                            x.GetParameters().Count() == 1 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)) &&
                            x.GetParameters()[0].ParameterType.GenericTypeArguments[0] == typeof(double))
                        .Single();
                }
                return _sumDouble;
            }
        }

        public static MethodInfo SumNullableDouble
        {
            get
            {
                if (_sumNullableDouble == null)
                {
                    _sumNullableDouble = typeof(Queryable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == nameof(Queryable.Sum) &&
                            x.GetParameters().Count() == 1 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)) &&
                            x.GetParameters()[0].ParameterType.GenericTypeArguments[0] == typeof(double?))
                        .Single();
                }
                return _sumNullableDouble;
            }
        }

        public static MethodInfo SumDecimal
        {
            get
            {
                if (_sumDecimal == null)
                {
                    _sumDecimal = typeof(Queryable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == nameof(Queryable.Sum) &&
                            x.GetParameters().Count() == 1 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)) &&
                            x.GetParameters()[0].ParameterType.GenericTypeArguments[0] == typeof(decimal))
                        .Single();
                }
                return _sumDecimal;
            }
        }

        public static MethodInfo SumNullableDecimal
        {
            get
            {
                if (_sumNullableDecimal == null)
                {
                    _sumNullableDecimal = typeof(Queryable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == nameof(Queryable.Sum) &&
                            x.GetParameters().Count() == 1 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)) &&
                            x.GetParameters()[0].ParameterType.GenericTypeArguments[0] == typeof(decimal?))
                        .Single();
                }
                return _sumNullableDecimal;
            }
        }

        public static MethodInfo Min
        {
            get
            {
                if (_min == null)
                {
                    _min = typeof(Queryable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == nameof(Queryable.Min) &&
                            x.GetParameters().Count() == 1 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)))
                        .Single();
                }
                return _min;
            }
        }

        public static MethodInfo Max
        {
            get
            {
                if (_max == null)
                {
                    _max = typeof(Queryable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == nameof(Queryable.Max) &&
                            x.GetParameters().Count() == 1 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)))
                        .Single();
                }
                return _max;
            }
        }

        public static MethodInfo DefaultIfEmpty
        {
            get
            {
                if (_defaultIfEmpty == null)
                {
                    _defaultIfEmpty = typeof(Queryable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == nameof(Queryable.DefaultIfEmpty) &&
                            x.GetParameters().Count() == 2 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)))
                        .Single();
                }
                return _defaultIfEmpty;
            }
        }

        public static MethodInfo Cast
        {
            get
            {
                if (_cast == null)
                {
                    _cast = typeof(Queryable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == nameof(Queryable.Cast) &&
                            x.GetParameters().Count() == 1 &&
                            x.GetParameters()[0].ParameterType.IsAssignableTo(typeof(IQueryable)))
                        .Single();
                }
                return _cast;
            }

        }
        public static MethodInfo Where
        {
            get
            {
                if (_where == null)
                {
                    _where = typeof(Queryable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == nameof(Queryable.Where) &&
                            x.GetParameters().Count() == 2 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)) &&
                            x.GetParameters()[1].ParameterType.IsAssignableToGeneric(typeof(Expression<>)) &&
                            x.GetParameters()[1].ParameterType.GenericTypeArguments[0].IsAssignableToGeneric(typeof(Func<,>)))
                        .Single();
                }
                return _where;
            }
        }

        public static MethodInfo Distinct
        {
            get
            {
                if (_distinct == null)
                {
                    _distinct = typeof(Queryable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == nameof(Queryable.Distinct) &&
                            x.GetParameters().Count() == 1)
                        .Single();
                }
                return _distinct;
            }
        }

        public static MethodInfo DistinctComparer
        {
            get
            {
                if (_distinctComparer == null)
                {
                    _distinctComparer = typeof(Queryable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == nameof(Queryable.Distinct) &&
                            x.GetParameters().Count() == 2)
                        .Single();
                }
                return _distinctComparer;
            }
        }

        public static MethodInfo OrderBy
        {
            get
            {
                if (_orderBy == null)
                {
                    _orderBy = typeof(Queryable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == nameof(Queryable.OrderBy) &&
                            x.GetParameters().Count() == 2)
                        .Single();
                }
                return _orderBy;
            }
        }

        public static MethodInfo OrderByDescending
        {
            get
            {
                if (_orderByDescending == null)
                {
                    _orderByDescending = typeof(Queryable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == nameof(Queryable.OrderByDescending) &&
                            x.GetParameters().Count() == 2)
                        .Single();
                }
                return _orderByDescending;
            }
        }

        public static MethodInfo Skip
        {
            get
            {
                if (_skip == null)
                {
                    _skip = typeof(Queryable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == nameof(Queryable.Skip) &&
                            x.GetParameters().Count() == 2)
                        .Single();
                }
                return _skip;
            }
        }

        public static MethodInfo Take
        {
            get
            {
                if (_take == null)
                {
                    _take = typeof(Queryable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == nameof(Queryable.Take) &&
                            x.GetParameters().Count() == 2 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)) &&
                            x.GetParameters()[1].ParameterType == typeof(int))
                        .Single();
                }
                return _take;
            }
        }
    }
}
