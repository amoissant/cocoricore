using CocoriCore.Collection.Extensions;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CocoriCore.Common
{
    public class IndexedSets<TKey, TValue>
    {
        private Dictionary<TKey, HashSet<TValue>> _dictionary;

        public IndexedSets()
        {
            _dictionary = new Dictionary<TKey, HashSet<TValue>>();
        }

        public IndexedSets<TKey, TValue> Add(IndexedSets<TKey, TValue> index)
        {   
            foreach(var (key, values) in index)
            {
                AddRange(key, values);
            }
            return this;
        }

        public IndexedSets<TKey, TValue> Add(TKey key, TValue value)
        {
            this[key].Add(value);
            return this;
        }

        public IndexedSets<TKey, TValue> AddRange(TKey key, params TValue[] values)
        {
            AddRange(key, (IEnumerable<TValue>)values);
            return this;
        }

        public IndexedSets<TKey, TValue> AddRange(TKey key, IEnumerable<TValue> values)
        {
            this[key].AddRange(values);
            return this;
        }

        public HashSet<TValue> Get(TKey key)
        {
            if (!_dictionary.ContainsKey(key))
            {
                _dictionary[key] = new HashSet<TValue>();
            }
            return _dictionary[key];
        }

        public void RemoveKey(TKey key)
        {
            _dictionary.Remove(key);
        }

        public HashSet<TValue> this[TKey key] => Get(key);

        public bool ContainsKey(TKey key)
        {
            return _dictionary.ContainsKey(key) && _dictionary[key].Count > 0;
        }

        public IEnumerable<TKey> Keys => _dictionary.Keys;
        public IEnumerable<TValue> Values => _dictionary.SelectMany(x => x.Value);
        public IEnumerable<(TKey key, ISet<TValue> values)> KeyValues => _dictionary.Select(x => (x.Key, (ISet<TValue>)x.Value));

        public void Clear(TKey key)
        {
            this[key].Clear();
        }

        public IEnumerator<(TKey key, ISet<TValue> values)> GetEnumerator()
        {
            return _dictionary
                .Where(x => x.Value.Count > 0)
                .Select(x => (x.Key, (ISet<TValue>)x.Value))
                .GetEnumerator();
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            foreach(var key in _dictionary.Keys)
            {
                sb.AppendLine($"{key} : [{string.Join(", ", _dictionary[key])}]");
            }
            return sb.ToString();
        }
    }
}