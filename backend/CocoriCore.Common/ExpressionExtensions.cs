﻿using CocoriCore.Collection.Extensions;
using CocoriCore.Common;
using CocoriCore.Reflection.Extension;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace CocoriCore.Expressions.Extension
{
    public static class ExpressionExtensions
    {
        public static Type GetMemberType(this LambdaExpression expression, bool allowDepth = false)
        {
            return expression.GetMemberInfo().GetMemberType();
        }

        public static MemberInfo GetMemberInfo<T>(this Expression<Func<T, object>> expression, bool allowDepth = false)
        {
            return GetMemberInfo((LambdaExpression)expression, allowDepth);
        }

        public static MemberInfo GetMemberInfo(this Expression expression, bool allowDepth = false)
        {
            if (expression == null)
                throw new ArgumentNullException(nameof(expression));
            if (expression is LambdaExpression)
            {
                if (((LambdaExpression)expression).Body == null)
                    throw new InvalidOperationException("expression body is null");
                return ((LambdaExpression)expression).Body.GetMemberInfo(allowDepth);
            }

            if (expression is UnaryExpression unaryBody)
            {
                var memberExpr = unaryBody.Operand as MemberExpression;
                return memberExpr.Member;
            }
            else if (expression is MemberExpression memberBody)
            {
                if (!allowDepth && expression.ToString().Split('.').Length > 2)
                    throw new InvalidOperationException("No depth allowed for expression " + expression);
                return memberBody.Member;
            }
            else
            {
                throw new NotImplementedException("Unknown expression body type " + expression.GetType());
            }
        }

        public static MethodInfo GetMethodInfo<T>(this Expression<Func<T, object>> expression)
        {
            if(expression.Body is MethodCallExpression methodExpression)
            {
                return methodExpression.Method;
            }
            else
            {
                throw new NotImplementedException("Unknown expression body type " + expression.GetType());
            }
        }

        public static bool IsMethodCall(this Expression expression, Type type, string methodName)
        {
            if (!(expression is MethodCallExpression))
                return false;

            var methodCall = (MethodCallExpression)expression;
            return (methodCall.Method.DeclaringType == type && methodCall.Method.Name == methodName);
        }

        public static ParameterExpression GetParameter(this LambdaExpression expression, Type parameterType)
        {
            return expression.Parameters.Single(x => x.Type == parameterType);
        }

        public static ParameterExpression GetParameter(this LambdaExpression expression)
        {
            return expression.Parameters.Single();
        }

        public static IQueryable Where(this IQueryable queryable, Expression predicate, ParameterExpression parameter)
        {
            if(!(predicate is LambdaExpression))
            {
                predicate = Expression.Lambda(predicate, parameter);
            }
            var whereMethod = QueryableMethods.Where.MakeGenericMethod(parameter.Type);
            var whereExp = Expression.Call(whereMethod, new Expression[] { queryable.Expression, Expression.Quote(predicate) });
            return queryable.Provider.CreateQuery(whereExp);
        }

        public static IQueryable<T> Where<T>(this IQueryable<T> queryable, Expression predicateExpression, ParameterExpression parameter)
        {
            if (predicateExpression != null)
            {
                var predicate = Expression.Lambda<Func<T, bool>>(predicateExpression, parameter);
                queryable = queryable.Where(predicate);
            }
            return queryable;
        }

        public static Expression Equal(this Expression left, object valueOrExpression)
        {
            var expressions = PrepareToCompare(left, valueOrExpression);
            return Expression.Equal(expressions.left, expressions.right);
        }

        public static Expression NotEqual(this Expression left, object valueOrExpression)
        {
            var expressions = PrepareToCompare(left, valueOrExpression);
            return Expression.NotEqual(expressions.left, expressions.right);
        }

        private static (Expression left, Expression right) PrepareToCompare(Expression left, object valueOrExpression)
        {
            Expression right = null;
            if (valueOrExpression is Expression)
            {
                right = (Expression)valueOrExpression;
            }
            else
            {
                right = Expression.Constant(valueOrExpression);
            }
            if (right.Type != left.Type)
            {
                right = Expression.Convert(right, left.Type);
            }
            return (left, right);
        }

        public static Expression AndAlso(this Expression expression1, Expression expression2)
        {
            if (expression1 == null && expression2 == null)
            {
                return null;
            }
            if (expression1 == null)
            {
                return expression2;
            }
            if (expression2 == null)
            {
                return expression1;
            }
            return Expression.AndAlso(expression1, expression2);
        }

        public static Expression OrElse(this Expression expression1, Expression expression2)
        {
            if (expression1 == null && expression2 == null)
            {
                return null;
            }
            if (expression1 == null)
            {
                return expression2;
            }
            if (expression2 == null)
            {
                return expression1;
            }
            return Expression.OrElse(expression1, expression2);
        }

        public static Expression In(this MemberExpression member, IEnumerable enumerable)
        {
            return member.In(enumerable.Cast<object>());
        }

        public static Expression In(this MemberExpression member, IEnumerable<object> enumerable)
        {
            return member.In((ICollection)enumerable.ToArray());
        }

        public static Expression In(this MemberExpression member, ICollection collection)
        {
            if (collection.Count > 0)
            {
                var memberType = member.Type;
                var array = collection.CastToArray(memberType);
                var collectionExp = Expression.Constant(array);
                return collectionExp.Contains(member);
            }
            else
            {
                return Expression.Constant(false);
            }

        }

        public static Expression Contains(this Expression collection, MemberExpression member)
        {
            var memberType = member.Type;
            var containsMethod = EnumerableMethods.Contains.MakeGenericMethod(memberType);
            return Expression.Call(null, containsMethod, new Expression[] { collection, member });
        }

        public static Expression ContainsText(this MemberExpression member, IEnumerable<object> stringCompatibleValues)
        {
            return member.ContainsText(stringCompatibleValues.Select(x => x?.ToString()));
        }

        public static Expression ContainsText(this MemberExpression member, IEnumerable<string> values)
        {
            if (values.Count() > 0)
            {
                Expression containsExp = null;
                var containsMethod = StringMethods.Contains;
                foreach (var value in values)
                {
                    var valueExp = Expression.Constant(value);
                    if (containsExp == null)
                    {
                        containsExp = Expression.Call(member, containsMethod, new Expression[] { valueExp });
                    }
                    else
                    {
                        var curr = Expression.Call(member, containsMethod, new Expression[] { valueExp });
                        containsExp = Expression.OrElse(containsExp, curr);
                    }
                }
                return containsExp;
            }
            else
            {
                return Expression.Constant(false);
            }
        }
    }
}