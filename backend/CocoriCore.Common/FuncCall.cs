﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CocoriCore.Common
{
    public class FuncCall<TIn, TOut> : IMethodCall<TIn, TOut>
    {
        public FuncCall(Func<TIn, TOut> func)
        {
            Func = func;
        }

        public FuncCall(Expression<Func<TIn, TOut>> expression) : this(expression.Compile())
        {
            Expression = expression;
        }

        public LambdaExpression Expression { get; }
        public Func<TIn, TOut> Func { get; }
        public bool IsAsync => false;
        public object Delegate => Func;

        public TOut Execute(IUnitOfWork unitOfWork, TIn input)
        {
            return Func(input);
        }

        public Task<TOut> ExecuteAsync(IUnitOfWork unitOfWork, TIn input)
        {
            return Task.FromResult(Execute(unitOfWork, input));
        }
    }

    public class FuncCall<TService, TIn, TOut> : IMethodCall<TIn, TOut>
    {
        public FuncCall(Func<TService, TIn, TOut> func)
        {
            Func = func;
        }

        public FuncCall(Expression<Func<TService, TIn, TOut>> expression) : this(expression.Compile())
        {
            Expression = expression;
        }

        public LambdaExpression Expression { get; }
        public Func<TService, TIn, TOut> Func { get; }
        public bool IsAsync => false;
        public object Delegate => Func;

        public TOut Execute(IUnitOfWork unitOfWork, TIn input)
        {
            var service = unitOfWork.Resolve<TService>();
            return Func(service, input);
        }

        public Task<TOut> ExecuteAsync(IUnitOfWork unitOfWork, TIn input)
        {
            return Task.FromResult(Execute(unitOfWork, input));
        }
    }

    public class FuncCall<TService1, TService2, TIn, TOut> : IMethodCall<TIn, TOut>
    {
        public FuncCall(Func<TService1, TService2, TIn, TOut> func)
        {
            Func = func;
        }

        public FuncCall(Expression<Func<TService1, TService2, TIn, TOut>> expression) : this(expression.Compile())
        {
            Expression = expression;
        }

        public LambdaExpression Expression { get; }
        public Func<TService1, TService2, TIn, TOut> Func { get; }
        public bool IsAsync => false;
        public object Delegate => Func;

        public TOut Execute(IUnitOfWork unitOfWork, TIn input)
        {
            var service1 = unitOfWork.Resolve<TService1>();
            var service2 = unitOfWork.Resolve<TService2>();
            return Func(service1, service2, input);
        }

        public Task<TOut> ExecuteAsync(IUnitOfWork unitOfWork, TIn input)
        {
            return Task.FromResult(Execute(unitOfWork, input));
        }
    }
}