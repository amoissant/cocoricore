using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace CocoriCore.Common
{
    public static class StreamExtension
    {
        public static async Task<string> ReadAsUtf8StringAsync(this Stream stream, bool leaveOpen = true)
        {
            stream.Position = 0;
            string result = string.Empty;
            using (var reader = new StreamReader(stream, Encoding.UTF8, true, 1024, leaveOpen))
            {
                result = await reader.ReadToEndAsync();
            }
            return result;
        }

        public static byte[] ReadAllBytes(this Stream stream)
        {
            byte[] bytes;
            int index = 0;
            long fileLength = stream.Length;
            if (fileLength > Int32.MaxValue)
                throw new IOException("File is too big to be read once a time.");
            int count = (int)fileLength;
            bytes = new byte[count];
            while (count > 0)
            {
                int n = stream.Read(bytes, index, count);
                if (n == 0)
                    throw new IOException("End of file.");
                index += n;
                count -= n;
            }
            return bytes;
        }
    }
}