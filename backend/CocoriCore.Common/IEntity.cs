using System;

namespace CocoriCore.Common
{
    public interface IEntity 
    {
        Guid Id { get; set; }
    }
}