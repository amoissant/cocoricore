﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace CocoriCore.Common
{
    public class EqualityMemberVisitor : ExpressionVisitor
    {
        private ParameterExpression _dependencyParameter;
        private ParameterExpression _projectionParameter;
        private List<Equality> _equalities;
        private List<MemberInfo> _dependencyMembers;
        private List<MemberInfo> _projectionMembers;

        public EqualityMemberVisitor(ParameterExpression dependencyParameter, ParameterExpression projectionParameter)
        {
            _dependencyParameter = dependencyParameter;
            _projectionParameter = projectionParameter;
            _dependencyMembers = new List<MemberInfo>();
            _projectionMembers = new List<MemberInfo>();
            _equalities = new List<Equality>();
        }

        public IEnumerable<Equality> Equalities => _equalities;
        public IEnumerable<MemberInfo> DependencyMembers => _dependencyMembers;
        public IEnumerable<MemberInfo> ProjectionMembers => _projectionMembers;

        protected override Expression VisitBinary(BinaryExpression node)
        {
            if (node.NodeType == ExpressionType.Equal)
            {
                var visitor = new MemberAccessVisitor(_dependencyParameter, _projectionParameter);
                visitor.Visit(node);
                if (visitor.DependencyMembers.Count == 1 && visitor.ProjectionMembers.Count == 1)
                {
                    var equality = new Equality(_dependencyParameter, visitor.DependencyMembers.Single(),
                        _projectionParameter, visitor.ProjectionMembers.Single());
                    _equalities.Add(equality);
                }
            }
            return base.VisitBinary(node);
        }

        protected override Expression VisitMember(MemberExpression node)
        {
            if (node.Expression == _dependencyParameter)
                _dependencyMembers.Add(node.Member);
            else if (node.Expression == _projectionParameter)
                _projectionMembers.Add(node.Member);
            return base.VisitMember(node);
        }
    }
}
