﻿using System;

namespace CocoriCore.Common
{
    public class ExceptionWithCode : Exception
    {
        public string FieldName { get; }
        public object Code { get; }

        public ExceptionWithCode(object errorCode, string message, string fieldName = null)
            : base(message)
        {
            Code = errorCode;
            FieldName = fieldName;
        }

        public ExceptionWithCode(object errorCode, string message, Exception inner, string fieldName = null)
            : base(message, inner)
        {
            Code = errorCode;
            FieldName = fieldName;
        }
    }
}
