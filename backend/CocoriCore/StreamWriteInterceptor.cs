﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class StreamWriteInterceptor : Stream
    {
        private Stream _writeStream;
        private Stream _interceptorStream;

        public StreamWriteInterceptor(Stream writeStream, Stream interceptorStream)
        {
            _writeStream = writeStream;
            _interceptorStream = interceptorStream;
        }

        public override bool CanRead => _writeStream.CanRead;

        public override bool CanSeek => _writeStream.CanSeek;

        public override bool CanWrite => _writeStream.CanWrite;

        public override long Length => _writeStream.Length;

        public override long Position { get => _writeStream.Position; set => _writeStream.Position = value; }

        public override void Flush()
        {
            _writeStream.Flush();
        }

        public override Task FlushAsync(CancellationToken cancellationToken)
        {
            return _writeStream.FlushAsync(cancellationToken);
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            return _writeStream.Read(buffer, offset, count);
        }

        public override Task<int> ReadAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
        {
            return _writeStream.ReadAsync(buffer, offset, count, cancellationToken);
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return _writeStream.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            _writeStream.SetLength(value);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            _writeStream.Write(buffer, offset, count);
            _interceptorStream.Write(buffer, offset, count);
        }

        public override async Task WriteAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
        {
            var task1 = _writeStream.WriteAsync(buffer, offset, count, cancellationToken);
            var task2 = _interceptorStream.WriteAsync(buffer, offset, count, cancellationToken);
            await Task.WhenAll(task1, task2);
        }
    }
}
