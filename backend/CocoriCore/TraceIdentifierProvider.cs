namespace CocoriCore
{
    public class TraceIdentifierProvider
    {
        private string _traceIdentifier;

        public string TraceIdentifier { get => _traceIdentifier; set => _traceIdentifier = value; }
    }
}