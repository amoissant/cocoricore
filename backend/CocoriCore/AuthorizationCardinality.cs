﻿namespace CocoriCore
{
    public enum AuthorizationCardinality
    {
        All = 1,
        AlLeastOne = 2,
    }
}
