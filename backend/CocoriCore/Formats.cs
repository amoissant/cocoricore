﻿using System;
using System.Collections.Generic;

namespace CocoriCore
{
    public class Formats : Dictionary<Type, string>, IFormats
    {
        public Formats()
        {
            Set<DateTime>("yyyy'-'MM'-'dd'T'HH':'mm':'ss.FFFFFFFK");//ISO 8601 standard, default NewtonSoft
            Set<TimeSpan>(@"d\.hh\:mm\:ss\.fffffff");
        }

        public bool DateTimeInUTC { get; protected set; } 

        public string Get<T>()
        {
            return this[typeof(T)];
        }

        public void Set<T>(string format)
        {
            this[typeof(T)] = format;
        }
    }
}
