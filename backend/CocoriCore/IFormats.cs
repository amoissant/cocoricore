﻿namespace CocoriCore
{
    //TODO suppr une fois FluentValidationExtension qui n'en dépend plus
    public interface IFormats
    {
        string Get<T>();
        void Set<T>(string format);
        bool DateTimeInUTC { get; }
    }
}