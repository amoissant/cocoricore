using System;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class BusOptionsBuilder<TContext, TRule, TOptions>
       where TRule : BusRule<TContext>, new()
       where TOptions : BusOptions<TContext, TRule>, new()
    {
        protected TOptions _options;

        public BusOptionsBuilder()
        {
            _options = new TOptions();
        }

        public virtual TRule AddRule<T>()
        {
            return AddRule(typeof(T));
        }

        protected virtual TRule AddRule(Type targetType)
        {
            return _options.AddRule(targetType);
        }

        public virtual BusOptionsBuilder<TContext, TRule, TOptions> SetFireAndForgetExceptionHandler<THandler>(
            Func<THandler, Exception, Task> exceptionHandler)
        {
            Func<object, Exception, Task> handlerFunc = (o, e) => exceptionHandler((THandler)o, e);
            _options.FireAndForgetExceptionHandler = new FireAndForgetExceptionHandler(typeof(THandler), handlerFunc);
            return this;
        }

        public virtual TOptions Options
        {
            get
            {
                _options.ValidateConfiguration();
                return _options;
            }
        }

        public object Get(string optionKey)
        {
            return _options[optionKey];
        }

        public void Set(string optionKey, object value)
        {
            _options[optionKey] = value;
        }

        public bool ContainsKey(string optionKey)
        {
            return _options.ContainsKey(optionKey);
        }
    }
}