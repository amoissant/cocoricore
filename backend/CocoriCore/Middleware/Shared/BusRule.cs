using CocoriCore.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class BusRule<TContext> : Dictionary<string, object>
    {
        private List<IHandlerDefinition<TContext>> _handlerDefinitions { get; }
        public Type TargetType { get; set; }
        public bool StopPropagation { get; set; }
        public bool FireAndForget { get; set; }
        public bool AllowMultipleHandler { get; set; }

        public BusRule()
        {
            _handlerDefinitions = new List<IHandlerDefinition<TContext>>();
            StopPropagation = false;
            FireAndForget = false;
            AllowMultipleHandler = true;
        }

        public void AddHandler<T>(Func<T, TContext, Task> action)
            where T : class
        {
            AddHandler(typeof(T), (h, c, u) => action(h as T, c));
        }

        public void AddHandler<T>(Func<T, TContext, IUnitOfWork, Task> action)
            where T : class
        {
            AddHandler(typeof(T), (h, c, u) => action(h as T, c, u));
        }

        public void AddHandler(Type type, Func<object, TContext, IUnitOfWork, Task> action)
        {
            CheckIfCanAddAnotherHandler(type);
            _handlerDefinitions.Add(new HandlerDefinition<TContext>(type, action));
        }

        private void CheckIfCanAddAnotherHandler(Type type)
        {
            if (!AllowMultipleHandler && _handlerDefinitions.Count > 0)
            {
                throw new ConfigurationException($"Can't add handler [{type.FullName}] "
                      + "because a previous one is already defined and this rule doesn't allow multiple handlers.");
            }
        }

        public virtual async Task ProcessAsync(TContext context, IUnitOfWork unitOfWork)
        {
            foreach (var handler in _handlerDefinitions)
            {
                await ProcessHandlerAsync(handler, context, unitOfWork);
            }
        }

        public virtual async Task ProcessHandlerAsync(IHandlerDefinition<TContext> handler, TContext context, IUnitOfWork unitOfWork)
        {
            await handler.ProcessAsync(context, unitOfWork);
        }
    }
}