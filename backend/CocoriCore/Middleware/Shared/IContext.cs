﻿using System;

namespace CocoriCore
{
    public interface IGenericContext
    {
        Type[] GetGenericParameters(Type genericHandlerType);
    }
}
