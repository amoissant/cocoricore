using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CocoriCore.Middleware;
using Microsoft.AspNetCore.Http;
using System;

namespace CocoriCore
{
    //TODO permettre désactivation CORS ? -> oui en mettant ou pas ce middleware, pouvoir configurer la liste des middleware facilement ?
    public class CorsMiddleware : IMiddleware
    {
        protected bool _enabled;
        protected List<string> _allowedHeaders;
        protected List<string> _allowedMethods;
        protected ValueAuthorizer _accessFilter;
        protected bool _allowCredentials;

        public CorsMiddleware(string[] allowedOrigins)
        {
            _enabled = true;
            _allowCredentials = true;
            _accessFilter = new ValueAuthorizer(allowedOrigins);
            _allowedHeaders = new List<string> { "Content-Type", "Authorization" };
            _allowedMethods = new List<string> { "POST", "PUT", "DELETE", "PATCH" };//TODO à rendre paramétrable
        }

        public virtual async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            var httpRequest = context.Request;
            var httpResponse = context.Response;
            string origin = GetOrigin(httpRequest);
            var isAuthorized = _accessFilter.IsAuthorised(origin);
            if (IsPreflightRequest(httpRequest))
            {
                if (isAuthorized)
                    AddCorsResponseHeadersPreflight(httpResponse, origin);
                return;
            }
            else
            {
                if (isAuthorized)
                    AddCorsResponseHeaders(httpResponse, origin);
                await next(context);
            }
        }

        protected virtual string GetOrigin(HttpRequest httpRequest)
        {
            var origin = httpRequest.Headers["Origin"].FirstOrDefault();
            if (origin == null)
                origin = string.Empty;
            return origin;
        }

        protected virtual bool IsPreflightRequest(HttpRequest httpRequest)
        {
            return httpRequest.Method == "OPTIONS" &&
                httpRequest.Headers.Keys.Any(x => x.StartsWith("Access-Control-", StringComparison.InvariantCultureIgnoreCase));
        }

        protected virtual void AddCorsResponseHeadersPreflight(HttpResponse httpResponse, string origin)
        {
            AddAllowHeadersHeader(httpResponse);
            AddAllowMethodsHeader(httpResponse);
            AddAllowOriginHeader(httpResponse, origin);
            AddAllowCredentialsHeader(httpResponse, origin);
        }

        private void AddAllowHeadersHeader(HttpResponse httpResponse)
        {
            httpResponse.Headers["Access-Control-Allow-Headers"] = string.Join(", ", _allowedHeaders);
        }

        private void AddAllowMethodsHeader(HttpResponse httpResponse)
        {
            httpResponse.Headers["Access-Control-Allow-Methods"] = string.Join(", ", _allowedMethods);
        }

        private void AddAllowOriginHeader(HttpResponse httpResponse, string origin)
        {
            httpResponse.Headers["Access-Control-Allow-Origin"] = origin;
        }

        private void AddAllowCredentialsHeader(HttpResponse httpResponse, string origin)
        {
            if(_allowCredentials)
                httpResponse.Headers["Access-Control-Allow-Credentials"] = "true";
        }

        protected virtual void AddCorsResponseHeaders(HttpResponse httpResponse, string origin)
        {
            AddAllowOriginHeader(httpResponse, origin);
            AddAllowCredentialsHeader(httpResponse, origin);
        }
    }
}