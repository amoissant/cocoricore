using System;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class ErrorBusOptionsBuilder : BusOptionsBuilder<Exception, ErrorBusRule, ErrorBusOptions>
    {
        public new ErrorBusOptionsBuilder SetFireAndForgetExceptionHandler<THandler>(Func<THandler, Exception, Task> unexpectedExceptionHandler)
        {
            base.SetFireAndForgetExceptionHandler(unexpectedExceptionHandler);
            return this;
        }

        public ErrorBusOptionsBuilder SetUnexpectedExceptionHandler(Func<Exception, Exception, Task> action)
        {
            _options.UnexceptedExceptionHandler = action;
            return this;
        }

        public ErrorBusOptionsBuilder<TException> For<TException>()
            where TException : class
        {
            var rule = AddRule<TException>();
            return new ErrorBusOptionsBuilder<TException>(rule);
        }
    }

    public class ErrorBusOptionsBuilder<TException>
        where TException : class
    {
        private ErrorBusRule _rule;

        public ErrorBusOptionsBuilder(ErrorBusRule rule)
        {
            _rule = rule;
        }

        public ErrorBusRule Call<THandler>()
            where THandler : class, IErrorHandler<TException>
        {
            return _rule.Call<THandler>((h, e) => h.HandlerErrorAsync(e as TException));
        }

        public ErrorBusRule Call<THandler>(Func<THandler, Func<TException, Task>> action)
            where THandler : class
        {
            return _rule.Call<THandler>((t, e) => action(t)(e as TException));
        }

        public ErrorBusRule Call<THandler>(Func<THandler, TException, Task> action)
            where THandler : class
        {
            return _rule.Call<THandler>((h, e) => action(h, e as TException));
        }
    }
}