﻿using CocoriCore.Common;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class LoggerErrorBus : IErrorBus
    {
        private readonly ILoggerFactory _loggerFactory;

        public LoggerErrorBus(ILoggerFactory loggerFactory)
        {
            _loggerFactory = loggerFactory;
        }

        public Task HandleAsync(Exception exception)
        {
            _loggerFactory.CreateLogger(this.GetType().Name).LogError(exception.ToString());
            return Task.CompletedTask;
        }
    }
}