using System;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class ErrorBusOptions : BusOptions<Exception, ErrorBusRule>
    {
        public Func<Exception, Exception, Task> UnexceptedExceptionHandler;
    }
}