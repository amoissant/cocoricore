using System;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class ErrorBusRule : BusRule<Exception>
    {
        public ErrorBusRule Call<THandler>(Func<THandler, Exception, Task> action)
            where THandler : class
        {
            AddHandler<THandler>(action);
            return this;
        }

        public new ErrorBusRule StopPropagation()
        {
            base.StopPropagation = true;
            return this;
        }

        public new ErrorBusRule FireAndForget()
        {
            base.FireAndForget = true;
            return this;
        }
    }
}