using System;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class DefaultErrorBusOptionsBuilder : ErrorBusOptionsBuilder
    {
        public DefaultErrorBusOptionsBuilder()
        {
            For<Exception>().Call<ConsoleLogger>((l, e) => l.LogAsync(e));

            SetUnexpectedExceptionHandler(async (ue, e) =>//TODO suppr ceci et avoir une gestion d'erreur des middleware à la place ?
            {
                var logger = new ConsoleLogger();
                await logger.LogAsync(ue);
                await logger.LogAsync(e);
            });
        }
    }

    public class ConsoleLogger
    {
        public async Task LogAsync(Exception e)
        {
            do
            {
                await Console.Error.WriteLineAsync(e.Message);
                await Console.Error.WriteLineAsync(e.StackTrace);
                e = e.InnerException;
            }
            while (e != null);
        }
    }
}