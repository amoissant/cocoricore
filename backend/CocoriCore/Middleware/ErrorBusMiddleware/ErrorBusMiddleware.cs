using System;
using System.Threading.Tasks;
using CocoriCore.AspNetCore;
using CocoriCore.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;

namespace CocoriCore
{
    public class ErrorBusMiddleware : IMiddleware
    {
        public ErrorBusMiddleware()
        {
        }

        public async Task InvokeAsync(HttpContext httpContext, RequestDelegate next)
        {
            var start = DateTime.Now;
            try
            {
                await next(httpContext);
            }
            catch (Exception exception)
            {
                var errorBus = httpContext.GetUnitOfWork().Resolve<IErrorBus>();
                exception.Data["HttpTrace"] = new
                {
                    TraceId = httpContext.TraceIdentifier,
                    Method = httpContext.Request?.Method,
                    Url = httpContext.Request?.GetDisplayUrl(),
                    Start = start,
                    End = DateTime.Now,
                    Protocol = httpContext.Request?.Protocol,
                    RequestHeaders = httpContext.Request?.Headers?.ToExpando()//TODO veiller � anonymizer les header pouvant contenir des infos sensibles
                };
                await errorBus.HandleAsync(exception);
                throw;
            }
        }
    }
}