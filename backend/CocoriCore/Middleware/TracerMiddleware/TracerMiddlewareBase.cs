﻿using CocoriCore.Common;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CocoriCore
{
    public abstract class TracerMiddlewareBase<T> where T : class, IEntity
    {
        private IUnitOfWorkFactory _uowFactory;
        private IErrorBus _errorBus;

        private ConcurrentQueue<T> _traces;
        private Regex _anonymizeRegex;

        public TracerMiddlewareBase(IUnitOfWorkFactory uowFactory, IErrorBus errorBus, params string[] anonymizeMembers)
        {
            _uowFactory = uowFactory;
            _errorBus = errorBus;
            _traces = new ConcurrentQueue<T>();

            _anonymizeRegex = new Regex($"\"({string.Join("|", anonymizeMembers)})\"\\s*:\\s*\"(?<value>[^\"]+)\"", RegexOptions.IgnoreCase);

            var task = SaveTracesAsync();
        }

        protected virtual async Task SaveTracesAsync()
        {
            while (true)
            {
                try
                {
                    await Task.Delay(30000);//TODO pouvoir configurer la durée 
                    using (var uow = _uowFactory.NewUnitOfWork())
                    {
                        var repository = uow.Resolve<ITraceRepository>();
                        T trace = null;
                        while (_traces.TryDequeue(out trace))
                        {
                            trace = SanitizeTrace(trace);
                            await repository.InsertAsync(trace);
                        }
                        await repository.CommitAsync();
                    }
                }
                catch (Exception e)
                {
                    await _errorBus.HandleAsync(e);
                }
            }
        }

        protected abstract T SanitizeTrace(T trace);

        protected virtual string Sanitize(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return text;
            }
            var valuesToReplace = new List<string>();
            var matches = _anonymizeRegex.Matches(text);
            foreach (Match match in matches)
            {
                if (match.Success)
                {
                    valuesToReplace.Add(match.Groups["value"].Value);
                }
            }
            foreach (var value in valuesToReplace)
            {
                text = text.Replace(value, "*****");
            }
            text = text.Replace("\0", string.Empty);
            return text;
        }

        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            //exclure les fichiers et les options ?
            //TODO récupérer la trace http générée ici pour l'utiliser dans ErrorBusMiddleware ?
            //TODO quand et quoi purger ?

            StreamReadInterceptor requestInterceptor = null;
            StreamWriteInterceptor responseInterceptor = null;
            MemoryStream requestStream = null;
            MemoryStream responseStream = null;
            //TODO avec du using ?
            try
            {
                requestStream = new MemoryStream();
                responseStream = new MemoryStream();
                requestInterceptor = new StreamReadInterceptor(context.Request.Body, requestStream);
                responseInterceptor = new StreamWriteInterceptor(context.Response.Body, responseStream);

                context.Request.Body = requestInterceptor;
                context.Response.Body = responseInterceptor;

                var trace = await InitTraceAsync();
                await next(context);
                trace = await FillTraceAsync(trace, context, requestStream, responseStream);

                _traces.Enqueue(trace);
            }
            catch (Exception e)
            {
                await _errorBus.HandleAsync(e);
            }
            finally
            {
                if (requestStream != null)
                {
                    requestStream.Dispose();
                }
                if (responseStream != null)
                {
                    responseStream.Dispose();
                }
                if (requestInterceptor != null)
                {
                    requestInterceptor.Dispose();
                }
                if (responseInterceptor != null)
                {
                    responseInterceptor.Dispose();
                }
            }
        }

        protected abstract Task<T> InitTraceAsync();
        protected abstract Task<T> FillTraceAsync(T trace, HttpContext context, Stream requestStream, Stream responseStream);
    }
}
