using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;

namespace CocoriCore
{
    public class RouteNotFoundException : Exception
    {
        public RouteNotFoundException(HttpContext context)
            : base("Not found uri : " + context.Request.GetDisplayUrl())
        {
        }
        
    }
}