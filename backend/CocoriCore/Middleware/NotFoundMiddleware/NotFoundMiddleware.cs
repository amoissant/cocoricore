using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace CocoriCore
{
    public class NotFoundMiddleware : IMiddleware
    {
        public NotFoundMiddleware()
        {
        }

        public Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            throw new RouteNotFoundException(context);
        }
    }
}