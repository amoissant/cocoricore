﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace CocoriCore.Middleware
{
    //TODO replace by regex like in RestrictedAccessMiddleware 
    public class ValueAuthorizer
    {
        private readonly List<string> _allowedString;
        private readonly List<Regex> _allowedRegex;

        public ValueAuthorizer(string[] allowedValues)
        {
            allowedValues = allowedValues ?? new string[0];
            _allowedString = new List<string>();
            _allowedRegex = new List<Regex>();
            Allow(allowedValues);
        }

        private void Allow(string[] values)
        {
            values = values.Select(x => x.Trim()).ToArray();
            foreach (var origin in values)
            {
                if (origin.Contains("*"))
                {
                    var originRegex = origin.Replace(".", "\\.").Replace("*", ".*");
                    _allowedRegex.Add(new Regex(originRegex, RegexOptions.IgnoreCase));
                }
                else
                    _allowedString.Add(origin);
            }
        }

        public bool IsAuthorised(string value)
        {
            if (_allowedString.Contains(value) || _allowedRegex.Any(x => x.IsMatch(value)))
            {
                return true;
            }
            return false;
        }

    }
}
