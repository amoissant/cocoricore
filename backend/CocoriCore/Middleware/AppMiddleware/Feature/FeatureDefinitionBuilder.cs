using System.Linq;

namespace CocoriCore
{
    //TODO suppr si pas utilis�
    public class FeatureDefinitionBuilder<TFeature>
    {
        public FeatureOptions Options { get; }
        public TFeature Feature { get; }

        public FeatureDefinitionBuilder(FeatureOptions options, TFeature feature)
        {
            Options = options;
            Feature = feature;
        }

        public FeatureDefinitionBuilder<TFeature> Include(TFeature feature)
        {
            var messageTypes = Options.GetMessageTypes(feature).ToArray();
            foreach (var type in messageTypes)
            {
                Options.Add(Feature, type);
            }
            return this;
        }

        public FeatureDefinitionBuilder<TFeature> Add<T>()
        {
            var messageType = typeof(T);
            Options.Add(Feature, typeof(T));
            return this;
        }
    }
}