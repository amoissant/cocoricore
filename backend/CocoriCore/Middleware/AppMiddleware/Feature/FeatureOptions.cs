using CocoriCore.Common;
using CocoriCore.Types.Extension;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CocoriCore
{
    //TODO suppr si pas utilis�
    public class FeatureOptions
    {
        private IndexedSets<Type, object> _featuresOfMessage;
        private IndexedSets<object, Type> _messagesOfFeature;
        private bool _errorWhenNoFeatureForMessage;

        public FeatureOptions()
        {
            _featuresOfMessage = new IndexedSets<Type, object>();
            _messagesOfFeature = new IndexedSets<object, Type>();
            _errorWhenNoFeatureForMessage = true;
        }

        public void Add(object feature, Type messageType)
        {
            _featuresOfMessage.Add(messageType, feature);
            _messagesOfFeature.Add(feature, messageType);
        }

        public FeatureOptions ErrorIfNoFeatureForMessage(bool enableError = true)
        {
            _errorWhenNoFeatureForMessage = enableError;
            return this;
        }

        public FeatureDefinitionBuilder<TFeature> DefineFeature<TFeature>(TFeature feature)
        {
            if (_messagesOfFeature.ContainsKey(feature))
            {
                throw new ConfigurationException($"A feature with the same key {feature} already exists.");
            }
            return new FeatureDefinitionBuilder<TFeature>(this, feature);
        }

        public bool Contains(object feature)
        {
            return _messagesOfFeature.ContainsKey(feature);
        }

        public IEnumerable<object> GetFeatures(Type messageType)
        {
            List<object> features = new List<object>();
            var typeHierarchy = messageType.GetTypeHierarchy();
            foreach (var type in typeHierarchy)
            {
                if (_featuresOfMessage.ContainsKey(type))
                {
                    features.AddRange(_featuresOfMessage[type]);
                }
            }
            if (_errorWhenNoFeatureForMessage && features.Count() == 0)
            {
                throw new ConfigurationException($"No feature defined for message of type {messageType} neither it's super types.\n"
                    + $"You can disable this behavior using {nameof(FeatureOptions)}.{nameof(ErrorIfNoFeatureForMessage)}().");
            }
            return features;
        }

        public IEnumerable<Type> GetMessageTypes(object feature)
        {
            if (!_messagesOfFeature.ContainsKey(feature))
            {
                throw new ConfigurationException($"No feature defined for value '{feature}'.");
            }
            return _messagesOfFeature[feature];
        }
    }
}