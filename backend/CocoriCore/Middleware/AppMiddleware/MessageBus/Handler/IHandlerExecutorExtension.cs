﻿using CocoriCore.Common;
using CocoriCore.Messaging;
using System;
using System.Threading.Tasks;

namespace CocoriCore
{
    public static class IHandlerExecutorExtension
    {
        /// <summary>
        /// Find handler for <see cref="MessageContext.Message"/>, call <see cref="IHandler.HandleAsync(object)"/> then set <see cref="MessageContext.Response"/> with handler result.
        /// <para>The handler is resolved using current <see cref="IUnitOfWork"/>.</para>
        /// <para>If no handler is found for <paramref name="context"/>'s message then an <see cref="ConfigurationException"/> is thrown.</para>
        /// </summary>
        /// <param name="executor">The handler executor instance</param>
        /// <param name="context">The context</param>
        [Obsolete("Use c.Response = await x.ExecuteAsync(c.Message)")]
        public static async Task ExecuteAsync(this IHandlerExecutor executor, MessageContext context)
        {
            context.Response = await executor.ExecuteAsync(context.Message);
        }
    }
}