﻿using CocoriCore.Messaging;
using CocoriCore.Types.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CocoriCore
{
    public class HandlerResponseTypeProvider
    {
        private List<Type> _handlerTypes;
        private Dictionary<Type, Type> _messageAndResponseTypes;
        private readonly Type _handlerInterfaceType;

        public HandlerResponseTypeProvider(params Assembly[] assemblies)
        {
            _handlerInterfaceType = typeof(IHandler);

            _handlerTypes = assemblies
                .SelectMany(a => a.GetTypes())
                .Where(t =>
                    t.IsConcrete() &&
                    (_handlerInterfaceType.IsGenericTypeDefinition && t.IsAssignableToGeneric(_handlerInterfaceType) ||
                     t.IsAssignableTo(_handlerInterfaceType)))
                .ToList();
            _messageAndResponseTypes = _handlerTypes
                .Select(x => GetMessageAndResponseTypes(x))
                .ToDictionary(x => x.messageType, x => x.responseType);
        }

        public virtual (Type messageType, Type responseType) GetMessageAndResponseTypes(Type handlerType)
        {
            if (handlerType.IsAssignableToGeneric(typeof(IHandler<,>)))
            {
                var genericArguments = handlerType.GetGenericArguments(typeof(IHandler<,>));
                var messageType = GetMessageType(genericArguments[0]);
                var responseType = genericArguments[1];
                return (messageType, responseType);
            }
            else
            {
                var genericArguments = handlerType.GetGenericArguments(typeof(IHandler<>));
                var messageType = GetMessageType(genericArguments[0]);
                return (messageType, null);
            }
        }

        private Type GetMessageType(Type messageType)
        {
            if (messageType.IsGenericParameter)
            {
                messageType = messageType.BaseType;
            }
            if (messageType.IsGenericType)
            {
                messageType = messageType.GetGenericTypeDefinition();
            }
            return messageType;
        }

        public virtual Type GetResponseType(Type messageType)
        {
            //TODO au propre
            if (messageType.IsAssignableToGeneric(typeof(Batch<>)))
            {
                messageType = messageType.GetGenericArguments()[0];
                var responseType = GetResponseType_(messageType);
                return (responseType ?? typeof(object)).MakeArrayType();
            }
            if (messageType.IsGenericType)
            {
                messageType = messageType.GetGenericTypeDefinition();
            }
            return GetResponseType_(messageType);
        }

        private Type GetResponseType_(Type messageType)
        {
            if (!_messageAndResponseTypes.ContainsKey(messageType))
            {
                throw new InvalidOperationException($"Could not find corresponding handler for {messageType}, among provided assemblies.");
            }
            return _messageAndResponseTypes[messageType];
        }
    }
}