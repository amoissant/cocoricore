using System.Threading.Tasks;

namespace CocoriCore
{
    public interface IHandler
    {
        Task<object> HandleAsync(object message);
    }

    public interface IHandler<TMessage> : IHandler
    {
    }

    public interface IHandler<TMessage, TResponse> : IHandler<TMessage>
    {
    }
}