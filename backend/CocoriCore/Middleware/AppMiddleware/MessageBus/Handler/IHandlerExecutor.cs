﻿using CocoriCore.Common;
using System.Threading.Tasks;

namespace CocoriCore
{
    /// <summary>
    /// Process a message using corresponding handler.
    /// </summary>
    /// <remarks>
    /// Default implementation is <see cref="HandlerExecutor"/>.
    /// </remarks>
    public interface IHandlerExecutor
    {
        /// <summary>
        /// Find handler for <paramref name="message"/> then call <see cref="IHandler.HandleAsync(object)"/>.
        /// <para>The handler is resolved using current <see cref="IUnitOfWork"/>.</para>
        /// <para>If no handler is found for <paramref name="message"/> then an <see cref="ConfigurationException"/> is thrown.</para>
        /// </summary>
        /// <param name="message">The message to be processed by corresponding handler</param>
        /// <returns>The result of <see cref="IHandler.HandleAsync(object)"/></returns>
        Task<object> ExecuteAsync(object message);
    }
}