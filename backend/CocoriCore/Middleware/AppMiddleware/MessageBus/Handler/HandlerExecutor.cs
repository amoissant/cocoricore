﻿using CocoriCore.Common;
using CocoriCore.Types.Extension;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using CocoriCore.Collection.Extensions;

namespace CocoriCore
{
    /// <summary>
    /// Process a message using corresponding handler. 
    /// </summary>
    /// <remarks>Use assemly scan to find handler corresponding message.</remarks>
    public class HandlerExecutor : IHandlerExecutor
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMemoryCache _cache;
        private readonly IEnumerable<Assembly> _assemblies;

        /// <summary>
        /// Initialize a new instance of <see cref="HandlerExecutor"/>.
        /// </summary>
        /// <param name="cache">Cache used to memorize handler types for message type</param>
        /// <param name="unitOfWork">UnitOfWork used to resolve handlers</param>
        /// <param name="assemblies">Assemblies where to search for handler types</param>
        public HandlerExecutor(IMemoryCache cache, IUnitOfWork unitOfWork, IEnumerable<Assembly> assemblies)
        {
            _unitOfWork = unitOfWork;
            _cache = cache;
            _assemblies = assemblies;
            if (!assemblies.Any())
            {
                throw new ConfigurationException($"You must provide at least one assembly containing handlers.");
            }
        }

        /// <inheritdoc/>
        public async Task<object> ExecuteAsync(object message)
        {
            var handlerType = GetHandlerTypeOrDefault(message.GetType());
            if (handlerType != null)
            {
                var handler = (IHandler)_unitOfWork.Resolve(handlerType);
                return await handler.HandleAsync(message);
            }
            else
            {
                var errorMessage = BuildErrorMessage(message);
                throw new ConfigurationException(errorMessage);
            }
        }

        /// <summary>
        /// Search for handler type corresponding to <paramref name="messageType"/> into injected assemblies.
        /// </summary>
        /// <param name="messageType">The type of message for which to find an handler type.</param>
        /// <returns>The type of the handler capable to process a message of type <paramref name="messageType"/> or <c>null</c> if no handler found.</returns>
        public Type GetHandlerTypeOrDefault(Type messageType)
        {
            return _cache.GetCached(this, x => GetHandlerTypeOrDefaultInternal(messageType));
        }

        private Type GetHandlerTypeOrDefaultInternal(Type messageType)
        {
            //TODO mettre un peu de log de débug

            if (messageType == null) return null;

            var handlerTypesByMessageType = GetHandlerTypesByMessageType(_assemblies);

            var handlerType = handlerTypesByMessageType.GetValueOrDefault(messageType);
            if(handlerType == null && messageType.GetGenericArguments().Length > 0)
            {
                handlerType = handlerTypesByMessageType.GetValueOrDefault(messageType.GetGenericTypeDefinition());
            }
            if (handlerType?.IsGenericTypeDefinition == true)
            {
                var genericParameter = GetGenericParameters(handlerType, messageType);
                handlerType = handlerType.MakeGenericType(genericParameter);
            }
            return handlerType;
        }

        private Dictionary<Type, Type> GetHandlerTypesByMessageType(IEnumerable<Assembly> assemblies)
        {
            var cacheKey = $"{nameof(HandlerExecutor)}.{nameof(GetHandlerTypesByMessageType)}";
            if (!_cache.TryGetValue<Dictionary<Type, Type>>(cacheKey, out var handlerTypeByMessage))
            {
                handlerTypeByMessage = assemblies
                    .SelectMany(x => x.GetTypes())
                    .Where(t => t.IsConcrete() && t.IsAssignableTo<IHandler>())
                    .ToDictionary(t =>
                    {
                        var messageType = t.GetGenericArguments(typeof(IHandler<>))[0];
                        if (messageType.IsGenericParameter)
                            messageType = messageType.BaseType.GetGenericTypeDefinition();
                        if (messageType.IsGenericType)
                            messageType = messageType.GetGenericTypeDefinition();
                        return messageType;
                    }, t => t);
            }
            return handlerTypeByMessage;
        }

        private Type[] GetGenericParameters(Type genericHandlerType, Type messageType)
        {
            var genericParameters = genericHandlerType.GetGenericArguments();
            var candidateTypes = messageType.GetGenericArguments().Append(messageType).ToArray();
            for (var i = 0; i < genericParameters.Length; i++)
            {
                genericParameters[i] = GetTypeForHandlerParamater(genericHandlerType, genericParameters[i], candidateTypes);
            }
            return genericParameters;
        }

        private Type GetTypeForHandlerParamater(Type handlerType, Type handlerParameter, IEnumerable<Type> candidateTypes)
        {
            foreach (var candidate in candidateTypes)
            {
                if (handlerParameter.IsGenericParameter)
                {
                    if (handlerParameter.GetInterfaces().Any(x => candidate.IsAssignableTo(x)))
                    {
                        return candidate;
                    }
                }
                if (candidate.IsAssignableTo(handlerParameter))
                {
                    return candidate;
                }
            }
            throw new InvalidOperationException($"Unable to find matching type for parameter {handlerParameter} of "
                + $"generic handler {handlerType} among [{string.Join(", ", candidateTypes)}]");
        }

        private string BuildErrorMessage(object message)
        {
            var messageType = message.GetType();
            var messageAssembly = messageType.Assembly.GetName().Name;
            var assemblyNames = _assemblies.Select(x => x.GetName().Name);
            var errorMessage = $"No handler found for message type '{message?.GetType()}' in scanned assemblies :\n";
            foreach (var assemblyName in assemblyNames)
            {
                errorMessage += $" - {assemblyName}\n";
            }
            if (!_assemblies.Contains(messageType.Assembly))
            {
                errorMessage += $"Message of type '{messageType}' is located into '{messageAssembly}', " +
                    $"configure {nameof(HandlerExecutor)} dependencies to provide all necessay assemblies.";
            }

            return errorMessage;
        }
    }
}