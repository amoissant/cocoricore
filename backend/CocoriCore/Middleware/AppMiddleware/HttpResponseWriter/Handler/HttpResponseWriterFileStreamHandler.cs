﻿using CocoriCore.Common;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class HttpResponseWriterFileStreamHandler : IHttpReponseWriterHandler
    {
        public async Task WriteResponseAsync(HttpResponseWriterContext context)
        {
            FileStreamResponse response = (FileStreamResponse)context.Response;
            context.HttpResponse.ContentType = response.MimeType;
            context.HttpResponse.Headers["Content-Disposition"] = $"attachment; filename=\"{response.FileName?.RemoveDiacritics()}\"";
            context.HttpResponse.Headers["Cache-Control"] = "max-age=31536000";//one year
            context.HttpResponse.Headers["Access-Control-Expose-Headers"] = "Content-Disposition";
            using(var content = response.Content.Value)
            {
                await response.Content.Value.CopyToAsync(context.HttpResponse.Body);
            }
        }
    }
}