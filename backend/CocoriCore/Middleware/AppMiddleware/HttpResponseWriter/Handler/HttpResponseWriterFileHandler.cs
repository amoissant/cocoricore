using CocoriCore.Common;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class HttpResponseWriterFileHandler : IHttpReponseWriterHandler
    {
        public async Task WriteResponseAsync(HttpResponseWriterContext context)
        {
            FileResponse response = (FileResponse)context.Response;
            context.HttpResponse.ContentType = response.MimeType;
            context.HttpResponse.Headers["Content-Disposition"] = $"attachment; filename=\"{response.FileName?.RemoveDiacritics()}\"";
            context.HttpResponse.Headers["Cache-Control"] = "max-age=31536000";//one year
            context.HttpResponse.Headers["Access-Control-Expose-Headers"] = "Content-Disposition";
            await context.HttpResponse.Body.WriteAsync(response.Content, 0, response.Content.Length);
        }
    }
}