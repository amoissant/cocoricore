﻿using System.Threading.Tasks;
using CocoriCore.Common;
using Microsoft.AspNetCore.Http;
using CocoriCore.JsonNet;
using Newtonsoft.Json;

namespace CocoriCore
{
    public class FootprintResponseWriter : IHttpReponseWriterHandler
    {
        private IFactory _factory;

        public FootprintResponseWriter(IFactory factory)
        {
            _factory = factory;
        }

        public async Task WriteResponseAsync(HttpResponseWriterContext context)
        {
            var jsonSerializer = _factory.Create<JsonSerializer>();
            jsonSerializer.Formatting = Formatting.Indented;
            var json = jsonSerializer.Serialize(context.Response);
            context.HttpResponse.ContentType = "application/json; charset=utf-8";
            await context.HttpResponse.WriteAsync(json);
        }

    }
}