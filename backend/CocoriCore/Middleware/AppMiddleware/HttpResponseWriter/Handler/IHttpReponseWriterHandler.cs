using System.Threading.Tasks;

namespace CocoriCore
{
    public interface IHttpReponseWriterHandler
    {
        Task WriteResponseAsync(HttpResponseWriterContext context);
    }
}