﻿using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace CocoriCore
{
    public class HttpResponseWriterBase
    {
        protected readonly JsonSerializer _jsonSerializer;

        public HttpResponseWriterBase(JsonSerializer jsonSerializer)
        {
            _jsonSerializer = jsonSerializer;
        }

        public async Task WriteResponseAsync(HttpResponse httpResponse, object obj)
        {
            httpResponse.ContentType = "application/json; charset=utf-8";

            //Newtonsoft doesn't provide async Converter so even if we call JToken.WriteAsync we have blocking writes.
            //Here we serialize response into a memoryStream synchronously then we copy asynchronously into response stream the json
            //to avoid any blocking operation on HttpResponse stream.
            using (var ms = new MemoryStream())
            {
                using (var sw = new StreamWriter(ms, new UTF8Encoding(false), 1024, true))
                {
                    _jsonSerializer.Serialize(sw, obj);
                }
                ms.Position = 0;

                await ms.CopyToAsync(httpResponse.Body);
            }
        }
    }
}
