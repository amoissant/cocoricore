using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace CocoriCore
{
    public class HttpResponseWriterDefaultHandler : HttpResponseWriterBase, IHttpReponseWriterHandler
    {
        public HttpResponseWriterDefaultHandler(JsonSerializer jsonSerializer)
            : base(jsonSerializer)
        { }

        public async Task WriteResponseAsync(HttpResponseWriterContext context)
        {
            if (context.Response == null)
            {
                //to avoid error in console with Firefox : XML Parsing Error: no element found
                context.HttpResponse.ContentType = "application/json; charset=utf-8";
                await context.HttpResponse.WriteAsync(string.Empty);
            }
            else
                await WriteResponseAsync(context.HttpResponse, context.Response);
        }
    }
}