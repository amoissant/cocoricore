using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace CocoriCore
{
    public class HttpResponseWriterContext
    {
        public object Response;
        public HttpResponse HttpResponse;
        public Dictionary<string, object> Data;
    }
}