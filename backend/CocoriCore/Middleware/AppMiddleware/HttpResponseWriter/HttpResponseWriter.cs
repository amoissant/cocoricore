﻿using System.Threading.Tasks;
using CocoriCore.Common;
using Microsoft.AspNetCore.Http;

namespace CocoriCore
{
    public class HttpResponseWriter : BusBase<HttpResponseWriterContext, HttpResponseWriterRule, HttpResponseWriterOptions>, IHttpResponseWriter
    {
        public HttpResponseWriter(IUnitOfWorkFactory unitOfWorkFactory, IUnitOfWork unitOfWork, HttpResponseWriterOptions options)
            : base(unitOfWorkFactory, unitOfWork, options)
        {
        }

        public async Task WriteResponseAsync(object response, HttpResponse httpResponse)
        {
            HttpResponseWriterContext context = new HttpResponseWriterContext()
            {
                Response = response,
                HttpResponse = httpResponse,
            };
            await base.HandleAsync(context);
        }
    }
}