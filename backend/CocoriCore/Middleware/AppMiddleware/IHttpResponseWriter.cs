using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace CocoriCore
{
    public interface IHttpResponseWriter
    {
        Task WriteResponseAsync(object response, HttpResponse httpResponse);
    }
}