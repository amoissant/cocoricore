using System;
using System.Linq;

namespace CocoriCore
{
    public class ErrorTrace : ErrorTraceBase
    {  
        public DateTime DateTime { get; set; }
        public ErrorTrace(Exception exception) : base(exception)
        {
            DateTime = DateTime.Now;
        }
    }

    public class InnerErrorTrace : ErrorTraceBase
    {
        public InnerErrorTrace(Exception exception) : base(exception)
        {
        }
    }

    public abstract class ErrorTraceBase
    {
        public string Message { get; protected set; }
        public string StackTrace { get; protected set; }
        public string Type { get; protected set; }
        public InnerErrorTrace[] InnerExceptions { get; protected set; }

        public ErrorTraceBase(Exception exception)
        {
            StackTrace = exception.StackTrace;
            Message = exception.Message;
            Type = exception.GetType().FullName;   
            if(exception is AggregateException aggregateException)
                InnerExceptions = aggregateException
                    .InnerExceptions
                    .Select(e => new InnerErrorTrace(e))
                    .ToArray();
            else if(exception.InnerException != null)
                InnerExceptions = new InnerErrorTrace[] { new InnerErrorTrace(exception.InnerException) };
        }
    }
}