﻿using CocoriCore.Common;
using System.Linq;
using System.Threading.Tasks;

namespace CocoriCore
{
    public interface ITraceRepository
    {
        Task InsertAsync<TEntity>(TEntity entity) where TEntity : class, IEntity;
        IQueryable<TEntity> Query<TEntity>() where TEntity : class, IEntity;
        Task CommitAsync();
    }
}
