using System.Threading.Tasks;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Collections.Generic;
using CocoriCore.Common;
using CocoriCore.Repository;
using CocoriCore.Linq.Async;
using CocoriCore.Resource;
using CocoriCore.Reflection.Extension;
using CocoriCore.Expressions.Extension;

namespace CocoriCore
{
    //TODO a tester
    public class MemberUnicityChecker
    {
        private IRepository _repository;
        private IRenderer _renderer;

        public MemberUnicityChecker(IRepository repository, IRenderer renderer)
        {
            _repository = repository;
            _renderer = renderer;
        }

        public Task CheckAsync<TEntity>(TEntity entity,
            Expression<Func<TEntity, object>> entityExpression)
            where TEntity : class, IEntity
        {
            return CheckAsync(_repository.Query<TEntity>(), entity, null, null, entityExpression);
        }

        public Task CheckAsync<TEntity>(IQueryable<TEntity> queryable,
            TEntity entity,
            Expression<Func<TEntity, object>> entityExpression)
            where TEntity : class, IEntity
        {
            return CheckAsync(queryable, entity, null, null, entityExpression);
        }

        public Task CheckAsync<TEntity>(IQueryable<TEntity> queryable,
            TEntity entity,
            object errorCode,
            Expression<Func<TEntity, object>> entityExpression)
            where TEntity : class, IEntity
        {
            return CheckAsync(queryable, entity, errorCode, null, entityExpression);
        }

        public Task CheckAsync<TEntity>(TEntity entity,
            object errorCode,
            Expression<Func<TEntity, object>> entityExpression)
            where TEntity : class, IEntity
        {
            return CheckAsync(_repository.Query<TEntity>(), entity, errorCode, null, entityExpression);
        }

        public Task CheckAsync<TEntity>(TEntity entity,
            object errorCode,
            string commandFieldName,
            Expression<Func<TEntity, object>> entityExpression)
            where TEntity : class, IEntity
        {
            return CheckAsync(_repository.Query<TEntity>(), entity, errorCode, null, entityExpression);
        }

        public async Task CheckAsync<TEntity>(IQueryable<TEntity> queryable,
            TEntity entity,
            object errorCode,
            string commandFieldName,
            Expression<Func<TEntity, object>> entityExpression)
            where TEntity : class, IEntity
        {
            var parameter = Expression.Parameter(typeof(TEntity), "x");
            var memberValue = entityExpression.Compile().Invoke(entity);
            var equal = ConstructEqualityExpression(entityExpression.GetMemberInfo(), parameter, memberValue);
            var predicat = Expression.Lambda<Func<TEntity, bool>>(equal, parameter);
            var request = queryable
                .Where(predicat)
                .Where(x => x.Id != entity.Id);

            if (await request.AnyAsync())
            {
                var memberInfo = entityExpression.GetMemberInfo();
                var fieldName = commandFieldName ?? memberInfo.Name;
                string errorMessage = null;
                if (errorCode == null)
                {
                    errorMessage = $"A record with same value '{memberValue}' for field '{fieldName}' already exist.";
                }
                else
                {
                    errorMessage = await _renderer.RenderAsync(errorCode, new
                    {
                        fieldName = fieldName,
                        duplicateValue = memberValue
                    });
                }
                throw new NotUniqueException(errorMessage, memberInfo);
            }
        }

        public Task CheckAsync<TEntity>(IQueryable<TEntity> queryable,
            TEntity entity,
            params Expression<Func<TEntity, object>>[] entityExpressions)
            where TEntity : class, IEntity
        {
            return CheckAsync(queryable, entity, null, null, entityExpressions);
        }

        public Task CheckAsync<TEntity>(TEntity entity,
            params Expression<Func<TEntity, object>>[] entityExpressions)
            where TEntity : class, IEntity
        {
            return CheckAsync(_repository.Query<TEntity>(), entity, null, null, entityExpressions);
        }

        public Task CheckAsync<TEntity>(TEntity entity,
            object errorCode = null,
            string commandFieldName = null,
            params Expression<Func<TEntity, object>>[] entityExpressions)
            where TEntity : class, IEntity
        {
            return CheckAsync(_repository.Query<TEntity>(), entity, errorCode, commandFieldName, entityExpressions);
        }

        public async Task CheckAsync<TEntity>(IQueryable<TEntity> queryable,
            TEntity entity,
            object errorCode = null,
            string commandFieldName = null,
            params Expression<Func<TEntity, object>>[] entityExpressions)
            where TEntity : class, IEntity
        {
            var entityMembers = entityExpressions.Select(x => x.GetMemberInfo()).ToArray();
            var membersAndValues = new List<(string, object)>();
            var predicat = ConstructPredicateExpression(entity, membersAndValues, entityMembers);
            var request = queryable
                .Where(predicat)
                .Where(x => x.Id != entity.Id);

            if (await request.AnyAsync())
            {
                var fieldNames = membersAndValues.Select(x => x.Item1);
                var duplicateValues = membersAndValues.Select(x => x.Item2);
                string errorMessage = null;
                if (errorCode == null)
                {
                    errorMessage = $"A record with same values ['{string.Join("', ", duplicateValues)}'] "
                        + $"for fields ['{string.Join("', ", fieldNames)}'] already exists.";
                }
                else
                {
                    errorMessage = await _renderer.RenderAsync(errorCode, new { fieldNames, duplicateValues });
                }
                throw new NotUniqueException(errorMessage, commandFieldName, entityMembers);
            }
        }

        private Expression<Func<TEntity, bool>> ConstructPredicateExpression<TEntity>(TEntity entity,
            List<(string, object)> membersAndValues, params MemberInfo[] uniqueMembers)
            where TEntity : class, IEntity
        {
            var parameter = Expression.Parameter(typeof(TEntity), "x");
            Expression condition = Expression.Constant(true);
            foreach (var entityMember in uniqueMembers)
            {
                var memberValue = entityMember.InvokeGetter(entity);
                var equal = ConstructEqualityExpression(entityMember, parameter, memberValue);
                condition = Expression.And(condition, equal);
                membersAndValues.Add((entityMember.Name, memberValue));
            }
            var predicat = Expression.Lambda<Func<TEntity, bool>>(condition, parameter);
            return predicat;
        }

        private BinaryExpression ConstructEqualityExpression(MemberInfo entityMemberInfo,
            ParameterExpression parameter, object memberValue)
        {
            var memberAccess = Expression.MakeMemberAccess(parameter, entityMemberInfo);
            var contant = Expression.Constant(memberValue);
            return Expression.Equal(memberAccess, contant);
        }
    }
}
