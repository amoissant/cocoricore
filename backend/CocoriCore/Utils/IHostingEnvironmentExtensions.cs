﻿using Microsoft.AspNetCore.Hosting;

namespace CocoriCore
{
    public static class IHostingEnvironmentExtensions
    {
        public static bool IsDebug(this IHostingEnvironment environment)
        {
            return environment.IsEnvironment("Debug");
        }
    }
}
