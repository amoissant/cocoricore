﻿using System;

namespace CocoriCore.Utils
{
    public static class GuidExtensions
    {
        public static Guid DefaultIfNull(this Guid? guid)
        {
            if(guid.HasValue)
            {
                return guid.Value;
            }
            else
            {
                return default(Guid);
            }
        }
    }
}
