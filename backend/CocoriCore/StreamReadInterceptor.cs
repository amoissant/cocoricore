﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class StreamReadInterceptor : Stream
    {
        private Stream _readStream;
        private Stream _interceptorStream;

        public StreamReadInterceptor(Stream readStream, Stream interceptorStream)
        {
            _readStream = readStream;
            _interceptorStream = interceptorStream;
        }

        public override bool CanRead => _readStream.CanRead;

        public override bool CanSeek => _readStream.CanSeek;

        public override bool CanWrite => _readStream.CanWrite;

        public override long Length => _readStream.Length;

        public override long Position { get => _readStream.Position; set => _readStream.Position = value; }

        public override void Flush()
        {
            _readStream.Flush();
        }

        public override Task FlushAsync(CancellationToken cancellationToken)
        {
            return _readStream.FlushAsync(cancellationToken);
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            var result = _readStream.Read(buffer, offset, count);
            _interceptorStream.Write(buffer, offset, result);
            return result;
        }

        public override async Task<int> ReadAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
        {
            var result = await _readStream.ReadAsync(buffer, offset, count, cancellationToken);
            await _interceptorStream.WriteAsync(buffer, offset, result, cancellationToken);
            return result;
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return _readStream.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            _readStream.SetLength(value);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            _readStream.Write(buffer, offset, count);
        }

        public override Task WriteAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
        {
            return _readStream.WriteAsync(buffer, offset, count, cancellationToken);
        }
    }
}
