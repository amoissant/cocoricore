﻿using CocoriCore.Common;
using System;

namespace CocoriCore.Application
{
    public class File : IEntity
    {
        public virtual Guid Id { get; set; }
        public virtual string FileName { get; set; }
        public virtual string MimeType { get; set; }
        public virtual Path FolderPath { get; set; }
        public virtual Path GetPath()
        {
            return FolderPath.Append(FileName);
        }
    }
}
