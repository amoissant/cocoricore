﻿using CocoriCore.Common;
using System;

namespace CocoriCore.Application
{
    [Obsolete]
    public class CreateFileCommand : ICommand
    {
        public string MimeType;
        public string FileName;
        public string Base64Content;
        public Path FolderPath;
    }
}
