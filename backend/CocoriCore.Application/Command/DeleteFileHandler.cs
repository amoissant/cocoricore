﻿using CocoriCore.ExtendedValidation;
using CocoriCore.FileSystem;
using CocoriCore.Repository;
using System.Threading.Tasks;

namespace CocoriCore.Application
{
    public class DeleteFileHandler : VoidCommandHandler<DeleteFileCommand>, IValidate<DeleteFileCommand>
    {
        private IRepository _repository;
        private IFileSystem _fileSystem;

        public DeleteFileHandler(IRepository repository, IFileSystem fileSystem)
        {
            _repository = repository;
            _fileSystem = fileSystem;
        }

        public void SetupValidator(ExtendedValidator<DeleteFileCommand> validator)
        {
            validator.RuleFor(x => x.Id).Identifier(typeof(File));
        }

        public override async Task ExecuteAsync(DeleteFileCommand command)
        {
            var file = await _repository.LoadAsync<File>(command.Id);
            await _fileSystem.DeleteFileAsync(file.GetPath());
            await _repository.DeleteAsync(file);
        }
    }
}
