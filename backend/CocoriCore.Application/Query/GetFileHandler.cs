﻿using AutoMapper;
using CocoriCore.Common;
using CocoriCore.ExtendedValidation;
using CocoriCore.FileSystem;
using CocoriCore.Repository;
using System.Threading.Tasks;

namespace CocoriCore.Application
{
    public class GetFileHandler : QueryHandler<GetFileQuery, FileResponse>, IValidate<GetFileQuery>
    {
        private IRepository _repository;
        private IMapper _mapper;
        private IFileSystem _fileSystem;

        public GetFileHandler(IRepository repository, IMapper mapper, IFileSystem fileSystem)
        {
            _repository = repository;
            _mapper = mapper;
            _fileSystem = fileSystem;
        }

        public void SetupValidator(ExtendedValidator<GetFileQuery> validator)
        {
            validator.RuleFor(x => x.Id).Identifier(typeof(File));
        }

        public override async Task<FileResponse> ExecuteAsync(GetFileQuery query)
        {
            var file = await _repository.LoadAsync<File>(query.Id);
            var response = _mapper.Map<FileResponse>(file);
            response.Content = await _fileSystem.ReadAsBinaryAsync(file.GetPath());
            return response;
        }
    }
}
