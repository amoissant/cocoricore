﻿using System;

namespace CocoriCore.Application
{
    [Obsolete]
    public class GetFileQuery : IQuery
    {
        public Guid Id;
    }
}
