﻿using CocoriCore.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace CocoriCore.ExtendedValidation
{
    public interface IExists
    {
        Task<bool> ExistsAsync(Type entityType, Guid id);

        Task<bool> ExistsAsync(Type entityType, MemberInfo member, object value);

        Task<bool> ExistsAsync(Type entityType, IEnumerable<Guid> collection);

        IQueryable<TEntity> Query<TEntity>() where TEntity : class, IEntity;
    }
}
