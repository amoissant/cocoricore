﻿using CocoriCore.Common;
using FluentValidation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using CocoriCore.Linq.Async;
using CocoriCore.Types.Extension;
using CocoriCore.Resource;
using CocoriCore.Reflection.Extension;
using CocoriCore.Expressions.Extension;

namespace CocoriCore.ExtendedValidation
{
    public static class FluentValidationExtension
    {
        public static IRuleBuilderOptions<T, dynamic> MatchTimeSpan<T>(this IRuleBuilder<T, dynamic> ruleBuilder)
        {
            return ruleBuilder
                .Must((i, v, c) =>
                {
                    var formats = c.Resolve<IFormats>();
                    TimeSpan timeSpan;
                    return v is string && TimeSpan.TryParseExact(v, formats.Get<TimeSpan>(), CultureInfo.InvariantCulture, out timeSpan);
                })
                .WithMessage("{PropertyName} must be a string corresponding to TimeSpan format.")
                .WithErrorCode("NOT_TIME_FORMAT");
        }

        public static IRuleBuilderOptions<T, dynamic> MatchDateTime<T>(this IRuleBuilder<T, dynamic> ruleBuilder)
        {
            return ruleBuilder
                .Must((i, v, c) =>
                {
                    var formats = c.Resolve<IFormats>();
                    DateTime dateTime;
                    return v is string && DateTime.TryParseExact(v, formats.Get<DateTime>(), CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime);//TODO DateTimeStyles.None à creuser
                })
                .WithMessage("{PropertyName} must be a string corresponding to DateTime format.")
                .WithErrorCode("NOT_DATETIME_FORMAT");
        }

        public static IRuleBuilderOptions<T, dynamic> Identifier<T>(this IRuleBuilder<T, dynamic> ruleBuilder, Type entityType)
        {
            return ruleBuilder
                    .MustAsync(async (i, v, c, t) => v is Guid id && await EntityWithIdExists(id, entityType, c.Resolve<IExists>()))
                    .WithMessage($"{{PropertyName}} with value {{PropertyValue}} must correspond to an entity of type {entityType}.")
                    .WithErrorCode("NOT_EXISTING_ID");
        }


        //TODO test si guid empty
        private static async Task<bool> EntityWithIdExists(Guid id, Type entityType, IExists exists)
        {
            return id != Guid.Empty && await exists.ExistsAsync(entityType, id);
        }

        public static IRuleBuilderOptions<T, Guid> Identifier<T>(this IRuleBuilder<T, Guid> ruleBuilder, Type entityType)
        {
            return ruleBuilder
                    .MustAsync(async (i, v, c, t) => v is Guid id && await EntityWithIdExists(id, entityType, c.Resolve<IExists>()))
                    .WithMessage($"{{PropertyName}} with value {{PropertyValue}} must correspond to an entity of type {entityType}.")
                    .WithErrorCode("NOT_EXISTING_ID");
        }

        //TODO à tester
        public static IRuleBuilderOptions<T, Guid?> Identifier<T>(this IRuleBuilder<T, Guid?> ruleBuilder, Type entityType)
        {
            return ruleBuilder
                .MustAsync(async (i, v, c, t) =>
                {
                    if (v.HasValue)
                        return await EntityWithIdExists(v.Value, entityType, c.Resolve<IExists>());
                    else
                        return true;
                })
                .WithMessage($"{{PropertyName}} with value {{PropertyValue}} must correspond to an entity of type {entityType}.")
                .WithErrorCode("NOT_EXISTING_ID");
        }

        public static IRuleBuilderOptions<T, IEnumerable<Guid>> Identifier<T>(this IRuleBuilder<T, IEnumerable<Guid>> ruleBuilder, Type entityType)
        {
            return ruleBuilder
                .NotNull()
                .MustAsync(async (i, v, c, t) =>
                {
                    if (v != null && v.Count() > 0)
                        return await c.Resolve<IExists>().ExistsAsync(entityType, v);
                    else
                        return true;
                })
                .WithMessage($"Each value in collection {{PropertyName}} must correspond to an entity of type {entityType}.")
                .WithErrorCode("NOT_EXISTING_ID");
        }

        public static IRuleBuilderOptions<T, Guid?> NotUsedIdentifier<T>(this IRuleBuilder<T, Guid?> ruleBuilder, Type entityType)
        {
            return ruleBuilder
                .MustAsync(async (i, v, c, t) =>
                {
                    if (v.HasValue)
                    {
                        if (await EntityWithIdExists(v.Value, entityType, c.Resolve<IExists>()))
                        {
                            return false;
                        }
                    }
                    return true;
                })
                .WithMessage($"{{PropertyName}} with value {{PropertyValue}} is already used for an entity of type {entityType}.")
                .WithErrorCode("ALREADY_USED_ID");
        }

        public static IRuleBuilderOptions<T, TValue> Exists<T, TValue>(this IRuleBuilder<T, TValue> ruleBuilder, Expression<Func<ExistBuilder, ExistPredicate>> predicateExpr)
        {
            var existPredicate = predicateExpr.Compile().Invoke(new ExistBuilder());
            if(typeof(TValue) != typeof(string) && typeof(TValue).IsAssignableTo<IEnumerable>())
            {
                return ruleBuilder
                .MustAsync(async (i, v, c, t) =>
                {
                    var exists = c.Resolve<IExists>();
                    var collection = ((IEnumerable)v).Cast<object>();
                    var paramater = Expression.Parameter(existPredicate.EntityType, "x");
                    var memberType = existPredicate.Member.GetMemberType();
                    var containsExpr = ConstructContainsExpression(paramater, existPredicate.Member, collection);
                    var predicate = Expression.Lambda(containsExpr, new[] { paramater });
                    //TODO simplifier appels génériques à Query
                    var queryMethod = typeof(IExists).GetMethod(nameof(IExists.Query));
                    var queryable = (IQueryable)queryMethod.MakeGenericMethod(paramater.Type).Invoke(exists, null);
                    var whereMethod = QueryableMethods.Where.MakeGenericMethod(paramater.Type);
                    var whereExp = Expression.Call(whereMethod, new Expression[] { queryable.Expression, Expression.Quote(predicate) });
                    var results = await queryable
                        .Provider
                        .CreateQuery(whereExp)
                        .Cast<IEntity>()
                        .ToArrayAsync();
                    var existingValues = results.Select(x => existPredicate.Member.InvokeGetter(x));
                    return collection.Except(existingValues).Count() == 0;
                })
                .WithMessage($"Each value must equals member {existPredicate.Member.Name} of at least one entity {existPredicate.EntityType}.")
                .WithErrorCode("NOT_EXISTING_VALUES");
            }
            else
            {
                return ruleBuilder
                .MustAsync(async (i, v, c, t) =>
                {
                    var exists = c.Resolve<IExists>();
                    var paramater = Expression.Parameter(existPredicate.EntityType, "x");
                    var memberAccessExpr = Expression.MakeMemberAccess(paramater, existPredicate.Member);
                    Expression valueExpr = Expression.Constant(v);
                    var memberType = existPredicate.Member.GetMemberType();
                    if (memberType != valueExpr.Type)
                    {
                        valueExpr = Expression.Convert(valueExpr, memberType);
                    }
                    var equalsExpr = Expression.Equal(memberAccessExpr, valueExpr);
                    var predicate = Expression.Lambda(equalsExpr, new[] { paramater });
                    //TODO simplifier appels génériques à Query
                    var queryMethod = typeof(IExists).GetMethod(nameof(IExists.Query));
                    var queryable = (IQueryable)queryMethod.MakeGenericMethod(paramater.Type).Invoke(exists, null);
                    var whereMethod = QueryableMethods.Where.MakeGenericMethod(paramater.Type);
                    var whereExp = Expression.Call(whereMethod, new Expression[] { queryable.Expression, Expression.Quote(predicate) });
                    var results = await queryable
                        .Provider
                        .CreateQuery(whereExp)
                        .Cast<IEntity>()
                        .ToArrayAsync();
                    return results.Length > 0;
                })
                .WithMessage($"Value '{{PropertyValue}}' must equals member {existPredicate.Member.Name} of at least one entity {existPredicate.EntityType}.")
                .WithErrorCode("NOT_EXISTING_VALUE");
            }
        }

        private static MethodCallExpression ConstructContainsExpression(ParameterExpression parameter,
            MemberInfo entityMember, IEnumerable<object> keys)
        {
            var memberType = entityMember.GetMemberType();
            if (!memberType.IsNullable())
            {
                memberType = typeof(Nullable<>).MakeGenericType(memberType);
            }
            var containsMethod = EnumerableMethods.Contains.MakeGenericMethod(memberType);
            Expression foreignKeyExpr = Expression.MakeMemberAccess(parameter, entityMember);
            if (!foreignKeyExpr.Type.IsNullable())
            {
                foreignKeyExpr = Expression.Convert(foreignKeyExpr, typeof(Nullable<>).MakeGenericType(foreignKeyExpr.Type));
            }
            var castMethod = EnumerableMethods.Cast.MakeGenericMethod(memberType);
            keys = keys.Distinct().ToArray();
            var idsExp = Expression.Constant(castMethod.Invoke(null, new object[] { keys }));
            return Expression.Call(null, containsMethod, new Expression[] { idsExp, foreignKeyExpr });
        }

        public static IRuleBuilderOptions<T, TProperty> WithResourceMessage<T, TProperty>(this IRuleBuilderOptions<T, TProperty> ruleBuilder, object resourceKey)
        {
            var ruleComponent = DefaultValidatorOptions.Configurable(ruleBuilder).Current;
            ruleComponent.SetErrorMessage((c, v) => c.Resolve<IResourceProvider>().ReadAsTextAsync(resourceKey).GetAwaiter().GetResult());
            ruleComponent.ErrorCode = $"{resourceKey}";
            return ruleBuilder;
        }
    }

    public class ExistBuilder
    {
        public ExistPredicate For<TEntity>(Expression<Func<TEntity, object>> member)
        {
            return new ExistPredicate(typeof(TEntity), member.GetMemberInfo());
        }
    }

    public class ExistPredicate
    {
        public ExistPredicate(Type entityType, MemberInfo member)
        {
            EntityType = entityType;
            Member = member;
        }

        public Type EntityType { get; }
        public MemberInfo Member { get; }
    }
}
