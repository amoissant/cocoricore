﻿using CocoriCore.Collection.Extensions;
using CocoriCore.Common;
using CocoriCore.FluentValidation;
using CocoriCore.Types.Extension;
using FluentValidation;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace CocoriCore.ExtendedValidation
{
    public class ValidatorExecutor : IValidatorExecutor
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMemoryCache _cache;
        private readonly IEnumerable<Assembly> _assemblies;
        private readonly ILogger _logger;

        public ValidatorExecutor(IMemoryCache cache, IUnitOfWork unitOfWork, ILogger logger, IEnumerable<Assembly> assemblies)
        {
            _unitOfWork = unitOfWork;
            _cache = cache;
            _assemblies = assemblies;
            _logger = logger;
            if (!assemblies.Any())
            {
                _logger.LogWarning("The provided assembly colletion is empty which mean no validation will be performed over incoming messages.\n" +
                    "You might review your configuration to provide assemblies containing validators for your messages.");
            }
        }

        public virtual async Task ValidateAsync(object message)
        {
            await CallAutonomousValidatorsAsync(message);
            await CallEmbedValidatorsAsync(message);
        }

        public virtual async Task CallEmbedValidatorsAsync(object message)
        {
            var messageType = message.GetType();
            var handlerTypesByMessageType = GetHandlerTypesByMessageType(_assemblies);
            foreach (var handlerType in handlerTypesByMessageType[messageType])
            {
                //TODO mettre un peu de log de débug
                var handler = _unitOfWork.Resolve(handlerType);
                var validator = BuildValidatorForHandler(handler);
                await validator.ValidateAndThrowAsync(message);
            }
        }

        private IndexedSets<Type, Type> GetHandlerTypesByMessageType(IEnumerable<Assembly> assemblies)
        {
            var cacheKey = $"{nameof(ValidatorExecutor)}.{nameof(GetHandlerTypesByMessageType)}";
            if (!_cache.TryGetValue<IndexedSets<Type, Type>>(cacheKey, out var result))
            {
                result = IndexTypesByGenericParameter(assemblies, typeof(IValidate<>));
            }
            return result;
        }

        private IndexedSets<Type, Type> IndexTypesByGenericParameter(IEnumerable<Assembly> assemblies, Type genericType)
        {
            if (!genericType.IsGenericTypeDefinition)
            {
                throw new ArgumentException($"{genericType} must be a generic type definition.");
            }
            if (genericType.GetGenericArguments().Count() != 1)
            {
                throw new ArgumentException($"{genericType} must have one and only one generic argument.");
            }
            return assemblies
                .SelectMany(a => a.GetTypes())
                .Where(t => t.IsAssignableToGeneric(genericType) && t.IsConcrete())
                .ToIndexedSets(t => t.GetGenericArguments(genericType).First());
        }

        public IValidator BuildValidatorForHandler(object handler)
        {
            var messageType = handler.GetType().GetInterface(typeof(IValidate<>)).GetGenericArguments()[0];
            var validatorType = typeof(EmbeddedValidator<>).MakeGenericType(messageType);
            var validator = (IExtendedValidator)_unitOfWork.Resolve(validatorType);
            validator = CallHandlerSetupValidatorMethod(handler, validator);
            return validator;
        }

        private IExtendedValidator CallHandlerSetupValidatorMethod(object handler, IExtendedValidator validator)
        {
            var handlerType = handler.GetType();
            var setupValidatorMethod = handlerType.GetMethod(nameof(IValidate<dynamic>.SetupValidator));
            setupValidatorMethod.Invoke(handler, new object[] { validator });
            return validator;
        }

        public virtual async Task CallAutonomousValidatorsAsync(object message)
        {
            var messageType = message.GetType();
            var validatorTypes = GetValidatorTypesForMessageType(messageType);
            foreach (var type in validatorTypes)
            {
                //TODO mettre un peu de log de débug
                var validator = (IValidator)_unitOfWork.Resolve(type);
                await validator.ValidateAndThrowAsync(message);
            }
        }

        private List<Type> GetValidatorTypesForMessageType(Type messageType)
        {
            return _cache.GetCached(this, x => GetValidatorTypesForMessageTypeInternal(messageType));
        }

        private List<Type> GetValidatorTypesForMessageTypeInternal(Type messageType)
        {
            var validators = GetValidatorTypesByMessageType(_assemblies);
            var validatorTypes = new List<Type>();
            foreach (var type in messageType.GetTypeHierarchy())
            {
                if (validators.ContainsKey(type))
                {
                    validatorTypes.AddRange(validators[type]);
                }
            }
            return validatorTypes;
        }

        private IndexedSets<Type, Type> GetValidatorTypesByMessageType(IEnumerable<Assembly> assemblies)
        {
            var cacheKey = $"{nameof(ValidatorExecutor)}.{nameof(GetValidatorTypesByMessageType)}";
            if(!_cache.TryGetValue<IndexedSets<Type, Type>>(cacheKey, out var validatorTypesByMessageType))
            {
                validatorTypesByMessageType = IndexTypesByGenericParameter(assemblies, typeof(IValidator<>));
            }
            return validatorTypesByMessageType;
        }

    }
}
