﻿using CocoriCore.Common;

namespace CocoriCore.ExtendedValidation
{
    //for autofac because specialized services override open generic one's
    //https://autofac.readthedocs.io/en/latest/register/registration.html
    public sealed class EmbeddedValidator<T> : ExtendedValidator<T>
    {
        public EmbeddedValidator(IUnitOfWork uniOfWork) : base(uniOfWork)
        {
        }
    }
}
