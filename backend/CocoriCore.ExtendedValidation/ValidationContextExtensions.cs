﻿using CocoriCore.Common;
using FluentValidation;

namespace CocoriCore.ExtendedValidation
{
    public static class ValidationContextExtensions
    {
        public static TService Resolve<TService>(this IValidationContext context)
        {
            var unitOfWork = (IUnitOfWork)context.RootContextData["unitOfWork"];
            return unitOfWork.Resolve<TService>();
        }
    }
}
