namespace CocoriCore.ExtendedValidation
{
    public interface IValidate<TMessage>
    {
        void SetupValidator(ExtendedValidator<TMessage> validator);
    }
}