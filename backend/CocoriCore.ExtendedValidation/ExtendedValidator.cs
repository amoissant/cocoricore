﻿using CocoriCore.Common;
using FluentValidation;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CocoriCore.ExtendedValidation
{
    public abstract class ExtendedValidator<T> : AbstractValidator<T>, IExtendedValidator
    {
        private readonly IUnitOfWork _unitOfWork;

        public ExtendedValidator(IUnitOfWork uniOfWork)
        {
            _unitOfWork = uniOfWork;
        }

        protected override bool PreValidate(ValidationContext<T> context, ValidationResult result)
        {
            context.RootContextData.Add("unitOfWork", _unitOfWork);
            return base.PreValidate(context, result);
        }

        public async Task ValidateWithExceptionAsync(object message)
        {
            var result = await ValidateAsync((T)message);
            if (!result.IsValid)
                throw new ValidationException(result.Errors);
        }
    }
}
