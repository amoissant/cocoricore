﻿using System.Reflection;

namespace CocoriCore.ExtendedValidation
{
    public static class AssemblyInfo
    {
        public static Assembly Assembly => typeof(AssemblyInfo).Assembly;
    }
}
