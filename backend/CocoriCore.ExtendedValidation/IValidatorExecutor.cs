﻿using System.Threading.Tasks;

namespace CocoriCore.ExtendedValidation
{
    public interface IValidatorExecutor
    {
        Task CallAutonomousValidatorsAsync(object message);
        Task CallEmbedValidatorsAsync(object message);
        Task ValidateAsync(object message);
    }
}