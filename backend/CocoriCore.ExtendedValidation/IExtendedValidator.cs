﻿using FluentValidation;
using System.Threading.Tasks;

namespace CocoriCore.ExtendedValidation
{
    public interface IExtendedValidator : IValidator
    {
        Task ValidateWithExceptionAsync(object message);
    }
}
