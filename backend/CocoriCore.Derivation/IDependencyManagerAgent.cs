﻿using System;
using System.Threading.Tasks;

namespace CocoriCore.Derivation
{
    public interface IDependencyManagerAgent
    {
        Task<int> ProcessDerivationEventsAsync(Guid treatmentId, int derivationDeep);
    }
}