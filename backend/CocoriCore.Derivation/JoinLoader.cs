﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Linq;
using CocoriCore.Common;
using CocoriCore.Repository;
using CocoriCore.Linq.Async;

namespace CocoriCore.Derivation
{
    public class JoinLoader : IAutoRegister
    {
        protected IRepository _repository;

        public JoinLoader(IRepository repository)
        {
            _repository = repository;
        }

        public virtual async Task<TEntity[]> LoadProjectionsAsync<TEntity>(DependencyInfo info, DerivationSet set)
            where TEntity : IEntity
        {
            var dependencies = set.GetEntities<IEntity>(EntityFlags.All);
            return await LoadEntitiesAsync<TEntity>(info, info.ProjectionParameter, dependencies);
        }

        public virtual async Task<TEntity[]> LoadProjectionsAsync<TEntity>(DependencyInfo info, IEnumerable<IEntity> dependencies)
            where TEntity : IEntity
        {
            return await LoadEntitiesAsync<TEntity>(info, info.ProjectionParameter, dependencies);
        }

        public virtual async Task<IEntity[]> LoadProjectionsAsync(DependencyInfo info, IEnumerable<IEntity> dependencies, int? skip = null, int? take = null)
        {
            return await LoadEntitiesAsync<IEntity>(info, info.ProjectionParameter, dependencies, skip, take);
        }

        //TODO au propre
        public virtual async Task<TEntity[]> LoadDependenciesAsync<TEntity>(DependencyInfo info, DerivationSet set)
            where TEntity : IEntity
        {
            // //TODO si on part de la joinExpression alors on pourra traiter directement des expression du genre (e, p) => e.Id == p.AnId && !e.IsArchived
            var dependencies = set.GetEntities<IEntity>(EntityFlags.All);
            var predicate = new ExpressionConverter().GetPredicate(info.DependencyParameter, info.JoinExpression, dependencies);
            var queryable = ConstructQueryableWhere(predicate.Parameters[0], predicate).Cast<TEntity>();

            try
            {
                return await queryable.ToArrayAsync();
            }
            catch (Exception e)
            {
                e.Data["Linq"] = queryable?.Expression?.ToString();
                throw;
            }
        }

        public virtual async Task<IEntity[]> LoadDependenciesAsync(DependencyInfo info, IEnumerable<IEntity> dependencies)
        {
            return await LoadEntitiesAsync<IEntity>(info, info.DependencyParameter, dependencies);
        }

        protected virtual async Task<TEntity[]> LoadEntitiesAsync<TEntity>(DependencyInfo info, 
            ParameterExpression parameterToLoad, IEnumerable<IEntity> entities, int? skip = null, int ? take = null)
            where TEntity : IEntity
        {
            if (entities.Count() == 0)
            {
                return new TEntity[0];
            }

            var predicate = new ExpressionTransformer().GetPredicate(parameterToLoad, info.JoinExpression, entities);
            var queryable = ConstructQueryableWhere(parameterToLoad, predicate, skip, take).Cast<TEntity>();

            try
            {
                return await queryable.ToArrayAsync();
            }
            catch (Exception e)
            {
                e.Data["Linq"] = queryable?.Expression?.ToString();
                throw;
            }
            //TODO la jointure donne [ids].Contains(e.Id) alors charger avec la méthode loadAsync
        }

        //TODO factoriser ce bout de code dans FluentValidationExtension
        protected virtual IQueryable ConstructQueryableWhere(ParameterExpression parameterToLoad, 
            LambdaExpression predicate, int? skip = null, int ? take = null)
        {
            var queryable = _repository.Query(parameterToLoad.Type);

            var whereMethod = QueryableMethods.Where.MakeGenericMethod(parameterToLoad.Type);
            var query = queryable.Expression;
            query = Expression.Call(whereMethod, new Expression[] { query, Expression.Quote(predicate) });
            if(skip != null)
            {
                var skipMethod = QueryableMethods.Skip.MakeGenericMethod(parameterToLoad.Type);
                query = Expression.Call(skipMethod, new Expression[] { query, Expression.Constant(skip.Value) });
            }
            if (take != null)
            {
                var takeMethod = QueryableMethods.Take.MakeGenericMethod(parameterToLoad.Type);
                query = Expression.Call(takeMethod, new Expression[] { query, Expression.Constant(take.Value) });
            }
            return queryable.Provider.CreateQuery(query);
        }
    }
}
