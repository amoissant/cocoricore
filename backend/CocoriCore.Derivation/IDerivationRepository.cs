using CocoriCore.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocoriCore.Derivation
{
    public interface IDerivationRepository
    {
        Task InsertAsync<TEntity>(TEntity entity) where TEntity : class, IEntity;

        Task UpdateAsync<TEntity>(TEntity entity) where TEntity : class, IEntity;

        Task DeleteAsync<TEntity>(TEntity entity) where TEntity : class, IEntity;

        Task<IEnumerable<IEntity>> LoadAsync(Type type, IEnumerable<Guid> ids);

        IQueryable<TEntity> Query<TEntity>() where TEntity : class, IEntity;

        IQueryable Query(Type type);

        Task FlushAsync();
    }
}