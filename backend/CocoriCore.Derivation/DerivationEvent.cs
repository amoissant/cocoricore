﻿using CocoriCore.Common;
using System;

namespace CocoriCore.Derivation
{
    public class DerivationEvent : IEntity
    {
        public Guid Id { get; set; }
        public Guid TreatmentId { get; set; }
        public Guid EntityId { get; set; }
        public string EntityTypeName { get; set; }
        public EntityOperation Operation { get; set; }
        public string EntityJson { get; set; }
        public string PreviousEntityJson { get; set; }
        public int Deep { get; set; }
        //TODO ajouter date création
        //TODO tester que quand tous les évènements on été traités alors y'a plus de DérivationEvent en bdd

        //TODO prévoir handler pour traiter les events en bdd qui nauraient pas été traités (plus vieux qu'une certaine date ou avec un treatmentId ?)
        //Attention il faudra se baser sur le dernier état en bdd pour le calcul, ou alors créer des évènements de recalcul à partir des évènements en bdd (puis les suppr ?)
        //-> faut pouvoir les éliminer ces lignes en bdd et être sûr que les projections sont à jour ensuite,
        //  - flagguer IsDirty toutes les projections associées aux events voulus
        //  - essayer de faire recalcul par batch en cas d'erreur sur un batch passer en mode ligne à ligne ? Peut-on mettre à jour toutes les lignes pas en erreur puis flagguer que celles en erreur ?
        //  - lancer un traitement de recalcul complet sur ces projections IsDirty (ligne à ligne dans une trasaction ? pour en calculer le plus possible)
        //  - suppr l'évènements associé à la ligne dans la même transaction
        //-> en cas d'erreur recharger l'entité et la mettre en IsDirty ? (on pourra avoir encore un pb d'accès concurents...)
        // - en cas de StaleStateException (à vérifier) peut-on recharger la projection pour tenter de la mettre en IsDirty ?
        //On aura besoin de savoir pourquoi l'évent est en erreur aussi, ca sera marqué dans les logs normalement, donc juste la gestion du dirty devrai suffire
    }
}
