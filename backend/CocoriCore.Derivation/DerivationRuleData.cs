using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace CocoriCore.Derivation
{
    public class DerivationRuleData
    {
        public DerivationRuleData()
        {
            //TODO si � false alors ca cr�� des erreurs 
            IsBatchPossible = true;
            IsDifferable = true;
        }

        public bool UseMapping { get; set; }
        public string Discriminator { get; set; }
        public Func<object, IEnumerable<Guid>> ManyIdsFunc { get; set; }
        public IEnumerable<MemberInfo> PropertiesCondition { get; set; }
        public Type DependencyType { get; set; }
        public Type ProjectionType { get; set; }
        public Type ManyType { get; set; }
        public Type SynchronizerType { get; set; }
        public LambdaExpression JoinExpression { get; set; }
        public bool IsProjectionCreator { get; set; }
        public MemberInfo ManyFkMember { get; set; }
        public DerivationRuleData OneRule { get; set; }
        public DerivationRuleData ManyRule { get; set; }
        public bool IsDifferable { get; set; }
        public bool IsBatchPossible { get; set; }
        public bool IgnoreOnInsert { get; set; }
        public Type HandlerType { get; set; }
        public Func<object, DerivationSet, DerivationRule, Task> HandlerAction { get; set; }
        public string StringRepresentation { get; set; }

        public void SetHandler<T>(Func<T, DerivationSet, DerivationRule, Task> action)
            where T : class
        {
            HandlerType = typeof(T);
            HandlerAction = (x, s, r) => action(x as T, s, r);
        }

        public override string ToString()
        {
            return StringRepresentation;
        }
    }
}