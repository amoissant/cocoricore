﻿using System.Threading.Tasks;
using System.Collections.Generic;
using System;
using System.Linq;
using CocoriCore.Common;
using CocoriCore.Types.Extension;
using CocoriCore.Reflection.Extension;

namespace CocoriCore.Derivation
{
    public class ProjectionDiff : IProjectionDiff, IAutoRegister
    {
        private IUnitOfWork _unitOfWork;
        private IDerivationRepository _repository;

        public ProjectionDiff(IUnitOfWork unitOfWork, IDerivationRepository repository)
        {
            _unitOfWork = unitOfWork;
            _repository = repository;
        }

        public virtual async Task ComputeDiffAndSaveAsync(Type projectionType, IEnumerable<IEntity> dependencies, params DerivationRule[] rules)
        {
            await ComputeDiffAndSaveAsync(projectionType, dependencies, rules.AsEnumerable());
        }

        public virtual async Task ComputeDiffAndSaveAsync(Type projectionType, IEnumerable<IEntity> dependencies, IEnumerable<DerivationRule> rules)
        {
            if (!dependencies.Any()) return;

            var result = await ComputeDiffAsync(projectionType, dependencies, rules);
            await _repository.DeleteManyAsync(result.ToDelete);
            await _repository.UpdateManyAsync(result.ToUpdate);
            await _repository.InsertManyAsync(result.ToInsert);
        }

        //TODO doit retourner une structure avec les 3 listes insert/update/delete
        public virtual async Task<DiffResult> ComputeDiffAsync(Type projectionType, IEnumerable<IEntity> dependencies, IEnumerable<DerivationRule> rules)
        {           
            var oldProjections = await LoadOldProjectionsAsync(rules, dependencies);
            var newProjections = await ConstructNewProjectionsAsync(rules, dependencies);

            var keysToDelete = oldProjections.Keys.Except(newProjections.Keys);
            var keysToUpdate = newProjections.Keys.Intersect(oldProjections.Keys);
            var keysToCreate = newProjections.Keys.Except(oldProjections.Keys);

            var projectionsToUpdate = new List<IEntity>();
            var projectionMembers = projectionType.GetPropertiesAndFields();
            foreach (var key in keysToUpdate)
            {
                var oldProjection = oldProjections[key];
                var newProjection = newProjections[key];
                var savedId = oldProjection.Id;
                foreach (var member in projectionMembers)
                {
                    member.InvokeSetter(oldProjection, member.InvokeGetter(newProjection));
                }
                oldProjection.Id = savedId;
                projectionsToUpdate.Add(oldProjection);
            }

            return new DiffResult(
                keysToCreate.Select(x => newProjections[x]).ToArray(),
                projectionsToUpdate,
                keysToDelete.Select(x => oldProjections[x]).ToArray());
        }

        protected virtual async Task<Dictionary<object, IEntity>> LoadOldProjectionsAsync(IEnumerable<DerivationRule> rules, IEnumerable<IEntity> dependencies)
        {
            var oldProjections = new Dictionary<object, IEntity>();
            foreach (var rule in rules)
            {
                var handlerType = rule.HandlerType;
                var handler = (IProjectionCreator)_unitOfWork.Resolve(handlerType);
                var projections = await handler.LoadProjectionsAsync(rule, dependencies);
                var keyFunc = handler.GetKeyFunc(rule);
                foreach (var projection in projections)
                {
                    oldProjections[keyFunc(projection)] = projection;
                }
            }
            return oldProjections;
        }

        protected virtual async Task<Dictionary<object, IEntity>> ConstructNewProjectionsAsync(IEnumerable<DerivationRule> rules, IEnumerable<IEntity> dependencies)
        {
            var newProjections = new Dictionary<object, IEntity>();
            foreach (var rule in rules)
            {
                var handlerType = rule.HandlerType;
                var handler = (IProjectionCreator)_unitOfWork.Resolve(handlerType);//TODO on peut economiser des resolve
                var projections = await handler.InitProjectionsAsync(rule, dependencies);
                await handler.SynchronizeNewlyCreatedProjectionsAsync(rule, projections, dependencies);
                var keyFunc = handler.GetKeyFunc(rule);
                foreach (var projection in projections)
                {
                    newProjections[keyFunc(projection)] = projection;
                }
            }
            return newProjections;
        }

        public class DiffResult
        {
            public readonly IEnumerable<IEntity> ToInsert;
            public readonly IEnumerable<IEntity> ToUpdate;
            public readonly IEnumerable<IEntity> ToDelete;

            public DiffResult(IEnumerable<IEntity> toInsert, IEnumerable<IEntity> toUpdate, IEnumerable<IEntity> toDelete)
            {
                ToInsert = toInsert;
                ToUpdate = toUpdate;
                ToDelete = toDelete;
            }
        }
    }
}
