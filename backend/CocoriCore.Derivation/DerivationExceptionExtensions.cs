﻿using CocoriCore.Common;
using System;
using System.Linq;

namespace CocoriCore.Derivation
{
    internal static class DerivationExceptionExtensions
    {
        internal static void AddDerivationDatas(this Exception exception, DerivationRule rule, DerivationSet set)
        {
            var ids = set?.Events?.Select(x => x?.Entity?.Id)?.ToArray();
            var debugDatas = new SerializableDatas
            {
                { "DependencyIds", $"[{string.Join(", ", ids)}]" },
                { "Rule", rule.StringRepresentation },
            };
            exception.Data["Derivation"] = debugDatas;
        }
    }
}
