﻿using CocoriCore.Common;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace CocoriCore.Derivation
{
    public class RuleExecutor : IRuleExecutor, IAutoRegister
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger _logger;
        private readonly IDerivationRepository _repository;

        public RuleExecutor(IUnitOfWork unitOfWork, ILogger logger, IDerivationRepository repository)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _repository = repository;
        }

        public async Task ExecuteAsync(IEnumerable<DerivationRule> rules, DerivationSet set)
        {
            foreach (var rule in rules)
            {
                _logger.LogDebug("[{0}:{1}] Start {2}", set.TreatmentId, set.Deep, rule);
                var sw = new Stopwatch();
                sw.Start();
                await ExecuteAsync(rule, set);
                sw.Stop();
                _logger.LogDebug("[{0}:{1}] Rule {2} executed in {3}ms.", set.TreatmentId, set.Deep, rule, sw.ElapsedMilliseconds);
            }
        }

        private async Task ExecuteAsync(DerivationRule rule, DerivationSet set)
        {
            try
            {
                var handler = _unitOfWork.Resolve(rule.HandlerType);
                await rule.HandlerAction(handler, set, rule);
                await _repository.FlushAsync();
            }
            catch (Exception e)
            {
                var errorMessage = $"Error while executing the following derivation rule:\n{rule},\nsee inner and datas for details.";
                var exception = new DerivationException(errorMessage, e);
                exception.AddDerivationDatas(rule, set);
                throw exception;
            }
        }
    }
}
