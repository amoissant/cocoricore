﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.Extensions.Logging;
using CocoriCore.Common;

namespace CocoriCore.Derivation
{
    public class DependencyHandlerAgent : IDependencyHandlerAgent, IAutoRegister
    {
        private readonly JoinLoader _joinLoader;
        private readonly ISynchronizerProcessor _processor;
        private readonly IDerivationRepository _repository;
        private readonly IDerivationEventPersister _eventPersister;
        private readonly ILogger _logger;

        public DependencyHandlerAgent(JoinLoader joinLoader, ISynchronizerProcessor processor, IDerivationRepository repository,
            IDerivationEventPersister eventPersister, ILogger logger)
        {
            _joinLoader = joinLoader;
            _processor = processor;
            _repository = repository;
            _eventPersister = eventPersister;
            _logger = logger;
        }

        public async Task<int> BatchResetPreviousProjectionsAsync(BatchData batch, int? skip = null, int? take = null)
        {
            var projectionCount = await ResetPreviousProjectionsAsync(batch.Rule, batch.Dependencies, skip, take);
            await _eventPersister.PersistDerivationEventsAsync(batch.TreatmentId, batch.DerivationDeep + 1);
            return projectionCount;
        }

        public async Task<int> ResetPreviousProjectionsAsync(DerivationRule rule, IEnumerable<IEntity> dependencies,
            int? skip = null, int? take = null)
        {
            var info = rule.DependencyInfo;
            var projections = await _joinLoader.LoadProjectionsAsync(info, dependencies, skip, take);
            await _processor.ResetWhileProjectionsAreModifiedAsync(rule, projections);
            await _repository.UpdateManyAsync(projections);
            _logger.LogDebug("{0} {1} reseted for {2} {3}.", projections.Count(), info.ProjectionType.Name, dependencies.Count(), info.DependencyType.Name);
            return projections.Count();
        }

        public async Task<int> BatchUpdateCurrentProjectionsAsync(BatchData batch, int? skip = null, int? take = null)
        {
            var projectionCount = await UpdateCurrentProjectionsAsync(batch.Rule, batch.Dependencies, skip, take);
            await _eventPersister.PersistDerivationEventsAsync(batch.TreatmentId, batch.DerivationDeep + 1);
            return projectionCount;
        }

        public async Task<int> UpdateCurrentProjectionsAsync(DerivationRule rule, IEnumerable<IEntity> dependencies,
            int? skip = null, int? take = null)
        {
            if(rule.DependencyInfo.DependencyType.Name == "SupplyPointProjection" && 
                rule.DependencyInfo.ProjectionType.Name == "SupplyChainProjection" &&
                dependencies.Any())
            {
                ;
            }
            var info = rule.DependencyInfo;
            var projections = await _joinLoader.LoadProjectionsAsync(info, dependencies, skip, take);
            //TODO si pas de projections alors return;
            await _processor.SynchonizeWhileProjectionsAreModifiedAsync(rule, projections);
            await _repository.UpdateManyAsync(projections);
            _logger.LogDebug("{0} {1} updated for {2} {3}.", projections.Count(), info.ProjectionType.Name, dependencies.Count(), info.DependencyType.Name);
            return projections.Count();
        }
    }
}

