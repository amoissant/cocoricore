using CocoriCore.Types.Extension;
using System;
using System.Linq;
using System.Reflection;

namespace CocoriCore.Derivation
{
    public static class DerivationBusMappingExtension
    {
        public static DerivationBusOptionsBuilder ConfigureMappings(this DerivationBusOptionsBuilder builder, Assembly assembly,
            params Assembly[] assemblies)
        {
            builder.MappingActions = assemblies
                .Append(assembly)
                .SelectMany(a => a.GetTypes())
                .Where(t => t.GetInterfaces().Any(i => i == typeof(IMappingConfiguration)) && t.IsConcrete())
                .SelectMany(t => ((IMappingConfiguration)Activator.CreateInstance(t)).GetMapRules())
                .ToList();
            return builder;
        }
    }
}