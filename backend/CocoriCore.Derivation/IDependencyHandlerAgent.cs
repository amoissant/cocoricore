﻿using CocoriCore.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CocoriCore.Derivation
{
    public interface IDependencyHandlerAgent
    {
        Task<int> BatchResetPreviousProjectionsAsync(BatchData batch, int? skip = null, int? take = null);
        Task<int> BatchUpdateCurrentProjectionsAsync(BatchData batch, int? skip = null, int? take = null);
        Task<int> ResetPreviousProjectionsAsync(DerivationRule rule, IEnumerable<IEntity> dependencies, int? skip = null, int? take = null);
        Task<int> UpdateCurrentProjectionsAsync(DerivationRule rule, IEnumerable<IEntity> dependencies, int? skip = null, int? take = null);
    }
}