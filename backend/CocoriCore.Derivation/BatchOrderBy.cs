﻿using CocoriCore.Expressions.Extension;
using System;
using System.Linq.Expressions;
using System.Reflection;

namespace CocoriCore.Derivation
{
    /// <summary>
    /// Use to load dependencies in batch mode.
    /// While recalulating projections we must ensure that if a dependency is inserted it won't change batches content.
    /// On the other hand some dependencies may not be loaded and some projections not recalulated. 
    /// </summary>
    /// <param name="member">An expression representing the filed to use to order dependencies during load.</param>
    /// <typeparam name="T">The type of dependency</typeparam>
    public class BatchOrderBy<T> : BatchOrderBy
    {
        public BatchOrderBy(Expression<Func<T, object>> member, OrderDirection direction = OrderDirection.Asc)
            : base(typeof(T), member.GetMemberInfo(), direction)
        {
        }
    }

    public class BatchOrderBy
    {
        internal BatchOrderBy(Type declaringType, MemberInfo member, OrderDirection direction = OrderDirection.Asc)
        {
            DeclaringType = declaringType;
            Member = member;
            Direction = direction;
        }

        public Type DeclaringType { get; }
        public MemberInfo Member { get; }
        public OrderDirection Direction { get; }
    }

    public enum OrderDirection
    {
        Asc,
        Desc
    }
}
