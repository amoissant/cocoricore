﻿using CocoriCore.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static CocoriCore.Derivation.ProjectionDiff;

namespace CocoriCore.Derivation
{
    public interface IProjectionDiff
    {
        Task ComputeDiffAndSaveAsync(Type projectionType, IEnumerable<IEntity> dependencies, IEnumerable<DerivationRule> rules);
        Task ComputeDiffAndSaveAsync(Type projectionType, IEnumerable<IEntity> dependencies, params DerivationRule[] rules);
        Task<DiffResult> ComputeDiffAsync(Type projectionType, IEnumerable<IEntity> dependencies, IEnumerable<DerivationRule> rules);
    }
}