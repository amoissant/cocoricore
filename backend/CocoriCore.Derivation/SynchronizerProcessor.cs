using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.Caching.Memory;
using CocoriCore.Common;
using CocoriCore.Types.Extension;
using CocoriCore.Reflection.Extension;

namespace CocoriCore.Derivation
{
    public class SynchronizerProcessor : ISynchronizerProcessor, IAutoRegister
    {
        private static int MAX_ROUNDTRIP = 100;

        protected readonly bool _handleGuidToNullableGuid;
        protected readonly JoinLoader _joinLoader;
        protected readonly DependencyCoupler _coupler;
        protected readonly IUnitOfWork _unitOfWork;
        protected readonly DerivationBusOptions _options;
        protected readonly IMemoryCache _memoryCache;

        public SynchronizerProcessor(DerivationBusOptions options, JoinLoader joinLoader, DependencyCoupler coupler, 
            IUnitOfWork unitOfWork, IMemoryCache memoryCache)
        {
            _options = options;
            _joinLoader = joinLoader;
            _coupler = coupler;
            _unitOfWork = unitOfWork;
            _handleGuidToNullableGuid = options.HandleGuidToNullableGuid;
            _memoryCache = memoryCache;
        }

        private IEnumerable<DerivationRule> GetRulesForProjectionType(Type projectionType)
        {
            return _memoryCache.GetCached(this, x => x.GetRulesForProjectionTypeInternal(projectionType));
        }

        private IEnumerable<DerivationRule> GetRulesForProjectionTypeInternal(Type projectionType)
        {
            return _options
                .Rules
                .Where(x =>
                    !x.IsProjectionCreator &&
                    x.ProjectionType == projectionType &&
                    x.SynchronizerType?.IsAssignableTo<ISynchronizer>() == true)
                .ToArray();
        }

        public virtual async Task SynchonizeNewlyCreatedProjectionsAsync(Type projectionType, IEnumerable<IEntity> dependencies, 
            IEnumerable<IEntity> projections, IEnumerable<MemberInfo> initMembers)
        {
            if (dependencies.Any())
            {
                var dependencyType = dependencies.First().GetType();
                var rules = GetMatchingRules(projectionType, initMembers);
                var tasks = await BuildSynchronizationTasksAsync(dependencies, projections, dependencyType, rules);
                var trackers = await ExecuteSynchonizationTasksAsync(projections, projectionType, tasks);
                await SynchronizeUsingTrackersAsync(projectionType, projections, trackers);//TODO ici on sauvegarde et restore les id pour rien et faudrait le faire avant
            }
        }

        private IEnumerable<DerivationRule> GetMatchingRules(Type projectionType, IEnumerable<MemberInfo> initMembers)
        {
            return GetRulesForProjectionType(projectionType)
                .Where(x =>
                    x.DependencyInfo.ProjectionMembers.Count() == 0 ||//TODO a tester, c'est pour le (e, p) => true
                    x.DependencyInfo.ProjectionMembers.Select(a => a.Name).Intersect(initMembers.Select(b => b.Name)).Count() > 0)
                .ToArray();
        }

        private async Task<List<SynchronizationTask>> BuildSynchronizationTasksAsync(IEnumerable<IEntity> dependencies, 
            IEnumerable<IEntity> projections, Type dependencyType, IEnumerable<DerivationRule> rules)
        {
            var tasks = new List<SynchronizationTask>();
            foreach (var rule in rules)
            {
                if (rule.DependencyType == dependencyType)
                {
                    var task = ConstructSynchronizationTask(rule, projections, dependencies);
                    tasks.Add(task);
                }
                else
                {
                    var task = await ConstructSynchronizationTaskAsync(rule, projections);
                    tasks.Add(task);
                }
            }
            return tasks;
        }

        protected virtual async Task<IEnumerable<DependencyTracker>> ExecuteSynchonizationTasksAsync(IEnumerable<IEntity> projections,
           Type projectionType, IEnumerable<SynchronizationTask> tasks)
        {
            var trackers = ConstructDependencyTrackers(projections, projectionType);
            await PrepareMappingsAsync(tasks);
            await ApplyResetAsync(tasks);
            await ApplySynchonizeAsync(tasks);
            //TODO pas tr�s pratique faut appeler SaveValues() pour avoir le r�sultat de HasChanged � revoir
            foreach (var tracker in trackers)
            {
                tracker.SaveValues();
            }
            return trackers.Where(x => x.HasChanged).ToArray();
        }

        private async Task SynchronizeUsingTrackersAsync(Type projectionType, IEnumerable<IEntity> projections,
           IEnumerable<DependencyTracker> trackers)
        {
            var savedIds = projections.ToDictionary(x => x, x => x.Id);
            var roundTripCount = 0;
            while (trackers.Count() > 0)
            {
                var tasks = await ConstructSynchronizationTasksAsync(trackers);
                trackers = await ExecuteSynchonizationTasksAsync(projections, projectionType, tasks);
                CheckMaxRoundTrip(++roundTripCount);
            }
            RestoreSavedIds(projections, savedIds);
            NormalizeNullableIds(projections);
        }

        //TODO factoriser code avec SynchonizeNewlyCreatedProjectionsAsync
        public virtual async Task SynchonizeWhileProjectionsAreModifiedAsync(DerivationRule rule, IEnumerable<IEntity> projections)
        {
            var projectionType = rule.ProjectionType;
            var info = rule.DependencyInfo;
            //TODO si pas de projections alors ne rien faire
            var dependencies = await _joinLoader.LoadDependenciesAsync(info, projections);//TODO est-ce qu'il faudrait paginer ici aussi ?
            var firstTask = ConstructSynchronizationTask(rule, projections, dependencies);
            var trackers = await ExecuteSynchonizationTasksAsync(projections, projectionType, new[] { firstTask });
            await SynchronizeUsingTrackersAsync(projectionType, projections, trackers);

            var rulesWithoutMember = GetRulesForProjectionType(projectionType)//TODO a tester, c'est pour le (e, p) => true
                .Where(x => x.DependencyInfo.ProjectionMembers.Count() == 0)
                .ToArray();
            var tasks = new List<SynchronizationTask>();
            foreach (var crule in rulesWithoutMember)
            {
                var task = await ConstructSynchronizationTaskAsync(crule, projections);
                tasks.Add(task);
            }
            trackers = await ExecuteSynchonizationTasksAsync(projections, projectionType, tasks);
            await SynchronizeUsingTrackersAsync(projectionType, projections, trackers);
        }

        protected virtual async Task<IEnumerable<SynchronizationTask>> ConstructSynchronizationTasksAsync(IEnumerable<DependencyTracker> trackers)
        {
            var trackersByRule = trackers
                .GroupBy(x => x.Rule)
                .ToDictionary(x => x.Key, x => x.ToArray());
            var tasks = new List<SynchronizationTask>();
            foreach (var kvp in trackersByRule)
            {
                var info = kvp.Key.DependencyInfo;
                var projections = kvp.Value.Select(x => x.Projection).Distinct(new EntityComparer());
                var dependencies = await _joinLoader.LoadDependenciesAsync(info, projections);
                var task = ConstructSynchronizationTask(kvp.Key, projections, dependencies);
                tasks.Add(task);
            }
            return tasks;
        }

        protected virtual async Task<SynchronizationTask> ConstructSynchronizationTaskAsync(DerivationRule rule, IEnumerable<IEntity> projections)
        {
            var info = rule.DependencyInfo;
            projections = projections.Distinct(new EntityComparer());
            var dependencies = await _joinLoader.LoadDependenciesAsync(info, projections);
            return ConstructSynchronizationTask(rule, projections, dependencies);
        }

        protected virtual SynchronizationTask ConstructSynchronizationTask(DerivationRule rule, 
            IEnumerable<IEntity> projections, IEnumerable<IEntity> dependencies)
        {
            var synchonizerType = rule.SynchronizerType;
            var synchronizer = (ISynchronizer)_unitOfWork.Resolve(synchonizerType);
            var info = rule.DependencyInfo;
            return new SynchronizationTask(info, dependencies, projections, synchronizer);
        }

        protected virtual async Task PrepareMappingsAsync(IEnumerable<SynchronizationTask> datas)
        {
            foreach (var data in datas)
            {
                await data.Synchronizer.PrepareAsync(data.Info, data.Dependencies, data.Projections);
            }
        }

        protected virtual IEnumerable<DependencyTracker> ConstructDependencyTrackers(IEnumerable<IEntity> projections, Type projectionType)
        {
            return GetRulesForProjectionType(projectionType)
                .SelectMany(x => projections.Select(p => new DependencyTracker(x, p)))
                .ToList();
        }

        protected virtual async Task ApplyResetAsync(IEnumerable<SynchronizationTask> datas)
        {
            foreach (var data in datas)
            {
                await data.Synchronizer.ResetAsync(data.Info, data.Dependencies, data.Projections);
            }
        }

        protected virtual async Task ApplySynchonizeAsync(IEnumerable<SynchronizationTask> datas)
        {
            foreach (var data in datas)
            {
                await data.Synchronizer.SynchonizeAsync(data.Info, data.Dependencies, data.Projections);
            }
        }

        protected virtual void CheckMaxRoundTrip(int roundTripCount)
        {
            if (roundTripCount > MAX_ROUNDTRIP)
            {
                throw new InvalidOperationException("Max roundtrip reached while applying mappings actions, " +
                    "review your mappings actions to prevent infinite loops.");
            }
        }

        protected virtual void RestoreSavedIds(IEnumerable<IEntity> projections, Dictionary<IEntity, Guid> savedIds)
        {
            foreach (var projection in projections)
            {
                projection.Id = savedIds[projection];
            }
        }

        protected virtual void NormalizeNullableIds(IEnumerable<IEntity> projections)
        {
            if (_handleGuidToNullableGuid)
            {
                foreach (var projection in projections)
                {
                    ConvertEmptyGuidToNullable(projection);
                }
            }
        }

        protected virtual void ConvertEmptyGuidToNullable(object projection)
        {
            foreach (var property in projection.GetType().GetProperties())
            {
                if (property.GetMemberType() == typeof(Guid?) && Equals(property.InvokeGetter(projection), default(Guid)))
                {
                    property.InvokeSetter(projection, null);
                }
            }
        }

        //TODO factoriser avec SynchronizeUsingTrackersAsync
        public virtual async Task ResetWhileProjectionsAreModifiedAsync(DerivationRule rule, IEnumerable<IEntity> projections)
        {
            var projectionType = rule.ProjectionType;
            var savedIds = projections.ToDictionary(x => x, x => x.Id);
            var trackers = projections.Select(x => new DependencyTracker(rule, x));
            int roundTripCount = 0;
            while (trackers.Count() > 0)
            {
                var datas = await ConstructSynchronizationTasksAsync(trackers);
                trackers = await ResetMappingsUsingTrackersAsync(projections, projectionType, datas);
                CheckMaxRoundTrip(++roundTripCount);
            }
            RestoreSavedIds(projections, savedIds);
            NormalizeNullableIds(projections);
        }

        protected virtual async Task<IEnumerable<DependencyTracker>> ResetMappingsUsingTrackersAsync(IEnumerable<IEntity> projections,
            Type projectionType, IEnumerable<SynchronizationTask> datas)
        {
            var trackers = ConstructDependencyTrackers(projections, projectionType);
            await PrepareMappingsAsync(datas);
            await ApplyResetAsync(datas);
            foreach(var tracker in trackers)
            {
                tracker.SaveValues();
            }
            return trackers.Where(x => x.HasChanged).ToArray();
        }
    }
}
