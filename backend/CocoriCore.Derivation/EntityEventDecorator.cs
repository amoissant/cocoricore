﻿using CocoriCore.Common;
using CocoriCore.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace CocoriCore.Derivation
{
    public class EntityEventDecorator : IRepositoryDecorator
    {
        private IEntityEventStore _eventStore;

        public EntityEventDecorator(IEntityEventStore eventStore)
        {
            _eventStore = eventStore;
        }

        public async Task InsertAsync(IEntity entity, InsertAsync next)
        {
            await next(entity);
            _eventStore.Add(EntityOperation.INSERT, entity);
        }

        public async Task UpdateAsync(IEntity entity, UpdateAsync next)
        {
            await next(entity);
            _eventStore.Add(EntityOperation.UPDATE, entity);
        }

        public async Task DeleteAsync(IEntity entity, DeleteAsync next)
        {
            await next(entity);
            _eventStore.Add(EntityOperation.DELETE, entity);
        }

        public async Task<IEnumerable<IEntity>> LoadAsync(Type type, Guid id, LoadIdAsync next)
        {
            return await next(type, id);
        }

        public async Task<IEnumerable<IEntity>> LoadAsync(Type type, IEnumerable<Guid> ids, LoadIdCollectionAsync next)
        {
            return await next(type, ids);
        }

        public async Task<IEnumerable<IEntity>> LoadAsync(Type type, MemberInfo uniqueMember, object value, LoadUniqueAsync next)
        {
            return await next(type, uniqueMember, value);
        }

        public IQueryable<TEntity> Query<TEntity>(Query<TEntity> next) where TEntity : IEntity
        {
            return next();
        }

        public async Task FlushAsync(FlushAsync next)
        {
            await next();
        }
    }
}