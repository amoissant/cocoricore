﻿using System.Threading.Tasks;
using System;
using System.Linq;
using CocoriCore.Common;

namespace CocoriCore.Derivation
{
    public class DerivationEventPersister : IDerivationEventPersister, IAutoRegister
    {
        private readonly IDerivationRepository _repository;
        private readonly IEntityEventStore _eventStore;
        private readonly IFactory _factory;
        private readonly IObjectSerializer _serializer;

        public DerivationEventPersister(IDerivationRepository repository, IEntityEventStore eventStore, 
            IFactory factory, IObjectSerializer serializer)
        {
            _repository = repository;
            _factory = factory;
            _serializer = serializer;
            _eventStore = eventStore;
        }

        public async Task PersistDerivationEventsAsync(Guid treatmentId, int deep = 0)
        {
            var events = _eventStore
                .GetPendingEvents()
                .Where(x => !(x.Entity is DerivationEvent))
                .Select(x =>
                {
                    var @event = _factory.Create<DerivationEvent>();
                    @event.TreatmentId = treatmentId;
                    @event.Operation = x.Operation;
                    @event.EntityId = x.Entity.Id;
                    @event.EntityTypeName = x.Entity.GetType().FullName;
                    @event.EntityJson = _serializer.Serialize<object>(x.Entity);
                    @event.PreviousEntityJson = _serializer.Serialize<object>(x.PreviousEntity);
                    @event.Deep = deep;
                    return @event;
                })
                .ToArray();
            await _repository.InsertManyAsync(events);
        }
    }
}
