﻿using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using System;
using CocoriCore.Common;

namespace CocoriCore.Derivation
{
    public abstract class ProjectionHandlerBase
    {
        protected IDerivationRepository _repository;
        protected ISynchronizerProcessor _processor;
        protected IFactory _factory;
        protected JoinLoader _joinLoader;

        public ProjectionHandlerBase(IDerivationRepository repository,
            ISynchronizerProcessor processor, IFactory factory, JoinLoader joinLoader)
        {
            _repository = repository;
            _processor = processor;
            _factory = factory;
            _joinLoader = joinLoader;
        }

        public virtual async Task HandleAsync<TDependency, TProjection>(DerivationRule rule, DerivationSet set)
            where TDependency : class, IEntity
            where TProjection : class, IEntity
        {
            await HandleInsertsAsync(rule, set);
            await HandleUpdatesAsync(rule, set);
            await HandleDeletesAsync(rule, set);
        }

        protected virtual async Task HandleInsertsAsync(DerivationRule rule, DerivationSet set)
        {
            var dependencies = set.GetEntities(EntityFlags.Insert);
            var projections = await InitProjectionsAsync(rule, dependencies);
            await SynchronizeNewlyCreatedProjectionsAsync(rule, projections, dependencies);
            await _repository.InsertManyAsync(projections);
        }

        public virtual Task<IEnumerable<IEntity>> InitProjectionsAsync(DerivationRule rule, IEnumerable<IEntity> dependencies)
        {
            var info = rule.DependencyInfo;
            var projections = new List<IEntity>();
            foreach (var dependency in dependencies)
            {
                var projection = (IEntity)_factory.Create(info.ProjectionType);
                info.ApplyEqualities(dependency, projection);
                projections.Add(projection);
            };
            return Task.FromResult(projections.AsEnumerable());
        }

        public virtual async Task SynchronizeNewlyCreatedProjectionsAsync(DerivationRule rule, IEnumerable<IEntity> projections, IEnumerable<IEntity> dependencies)
        {
            var info = rule.DependencyInfo;
            var projectionType = rule.ProjectionType;
            var initMembers = info.ProjectionMembers;
            await _processor.SynchonizeNewlyCreatedProjectionsAsync(projectionType, dependencies, projections, initMembers);
            //TODO ici on applique les règles de dépendencies tant que ca change mais elle sont appliquées encore une fois après pour rien.
        }

        //TODO revoir la signature de cette fonction qui n'est pas très explicite ?
        protected virtual Task HandleUpdatesAsync(DerivationRule rule, DerivationSet set)
        {
            //We do nothing here, the DependencyRule will update projections
            return Task.CompletedTask;
        }

        protected virtual async Task HandleDeletesAsync(DerivationRule rule, DerivationSet set)
        {
            var dependencies = set.GetEntities(EntityFlags.Delete);
            var projections = await LoadProjectionsAsync(rule, dependencies);
            await _repository.DeleteManyAsync(projections);
        }

        public virtual async Task<IEnumerable<IEntity>> LoadProjectionsAsync(DerivationRule rule, IEnumerable<IEntity> dependencies)
        {
            var info = rule.DependencyInfo;
            return await _joinLoader.LoadProjectionsAsync(info, dependencies);
        }

        public virtual Func<IEntity, object> GetKeyFunc(DerivationRule rule)
        {
            return e => e.Id;
        }
    }
}
