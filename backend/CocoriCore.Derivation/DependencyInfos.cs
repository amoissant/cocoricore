using CocoriCore.Common;
using CocoriCore.Types.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace CocoriCore.Derivation
{
    public class DependencyInfo
    {
        public DependencyInfo(LambdaExpression joinExpression, string discriminator)
        {
            JoinExpression = joinExpression;
            Discriminator = discriminator;
            DependencyParameter = JoinExpression.Parameters.ElementAt(0);
            ProjectionParameter = JoinExpression.Parameters.ElementAt(1);

            var visitor = new EqualityMemberVisitor(DependencyParameter, ProjectionParameter);
            visitor.Visit(JoinExpression.Body);
            Equalities = visitor.Equalities;
            DependencyMembers = visitor.DependencyMembers;
            ProjectionMembers = visitor.ProjectionMembers;
        }

        public LambdaExpression JoinExpression { get; }
        public string Discriminator { get; }
        public IEnumerable<MemberInfo> DependencyMembers { get; }
        public IEnumerable<MemberInfo> ProjectionMembers { get; }
        public IEnumerable<Equality> Equalities { get; }
        public ParameterExpression DependencyParameter { get; }
        public ParameterExpression ProjectionParameter { get; }
        public Type DependencyType => DependencyParameter.Type;
        public Type ProjectionType => ProjectionParameter.Type;

        public virtual void ApplyEqualities(IEntity dependency, IEntity projection)
        {
            foreach (var equality in Equalities)
            {
                equality.Apply(dependency, projection);
            }
        }

        public override string ToString()
        {
            return $"({DependencyType.GetPrettyName()}, {ProjectionType.GetPrettyName()}, "
                + $"'{Discriminator}') {JoinExpression}";
        }

        public override bool Equals(object obj)
        {
            return obj is DependencyInfo infos &&
                infos.DependencyType == DependencyType &&
                infos.ProjectionType == ProjectionType &&
                infos.Discriminator == Discriminator && 
                infos.JoinExpression.ToString() == JoinExpression.ToString();
        }

        public override int GetHashCode()
        {
            return 23 ^
                DependencyType.GetHashCode() ^
                ProjectionType.GetHashCode() ^
                (Discriminator != null ? Discriminator.GetHashCode() : string.Empty.GetHashCode()) ^
                JoinExpression.GetHashCode();
        }
    }
}