using CocoriCore.Common;
using CocoriCore.Reflection.Extension;
using CocoriCore.Types.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace CocoriCore.Derivation
{
    //TODO factoriser code avec ExpressionTransformer
    public class ExpressionConverter : ExpressionVisitor
    {
        private ParameterExpression _loadParameter;
        private ParameterExpression _valuesParameter;
        private IEnumerable<IEntity> _valueEntities;

        public LambdaExpression GetPredicate(ParameterExpression loadParameter, LambdaExpression joinExpression, IEnumerable<IEntity> valueEntities)
        {
            _loadParameter = loadParameter;
            _valuesParameter = joinExpression.Parameters.Single(x => x != loadParameter);
            _valueEntities = valueEntities;
            var predicate = StartVisit(joinExpression.Body);
            return Expression.Lambda(predicate, loadParameter);//TODO ne pas le faire ici mais au niveau de l'appelant
        }

        private Expression StartVisit(Expression expression)
        {
            if (IsValueTypeAndConstantEquality(expression) || IsValueTypeBool(expression))
            {
                return Expression.Constant(true);
            }
            else
            {
                return Visit(expression);
            }
        }

        protected override Expression VisitBinary(BinaryExpression node)
        {
            IEnumerable<object> values = null;
            ParameterExpression parameter = null;
            MemberInfo valueMember = null;
            MemberInfo compareMember = null;
            bool isNotEquals;
            if(IsValueAndLoadEquality(node, out valueMember, out compareMember, out parameter, out isNotEquals))
            {
                values = _valueEntities.Select(x => compareMember.InvokeGetter(x)).ToArray();//TODO seule cette ligne change par rapport à ExpressionTransformer, à factoriser
                Expression expression = ConstructContainsExpression(parameter, compareMember, values);
                if (isNotEquals)
                {
                    expression = Expression.Not(expression);
                }
                return expression;
            }
            else if(node.NodeType == ExpressionType.AndAlso || node.NodeType == ExpressionType.OrElse)
            {
                if(IsValueTypeAndConstantEquality(node.Left) || IsValueTypeBool(node.Left))
                {
                    return base.Visit(node.Right);
                }
                if (IsValueTypeAndConstantEquality(node.Right) || IsValueTypeBool(node.Right))
                {
                    return base.Visit(node.Left);
                }
            }
            return base.VisitBinary(node);
        }

        public bool IsValueAndLoadEquality(BinaryExpression expression, out MemberInfo valueMember, out MemberInfo compareMember, out ParameterExpression parameter, out bool isNotEquals)
        {
            valueMember = null;
            compareMember = null;
            parameter = null;
            isNotEquals = expression.NodeType == ExpressionType.NotEqual;
           if(expression.NodeType == ExpressionType.Equal || expression.NodeType == ExpressionType.NotEqual)
           {
                if (IsTypeMemberAccess(expression.Left, _loadParameter, out compareMember, out parameter) && 
                    IsTypeMemberAccess(expression.Right, _valuesParameter, out valueMember, out _))
                {
                    return true;
                }
                if (IsTypeMemberAccess(expression.Right, _loadParameter, out compareMember, out parameter) && 
                    IsTypeMemberAccess(expression.Left, _valuesParameter, out valueMember, out _))
                {
                    return true;
                }
                
            }
            return false;
        }

        public bool IsTypeMemberAccess(Expression expression, ParameterExpression parameter, out MemberInfo member, out ParameterExpression otherParameter)
        {
            member = null;
            otherParameter = null;
            if(expression.NodeType == ExpressionType.Convert)
            {
                expression = ((UnaryExpression)expression).Operand;
            }
            if (expression is MemberExpression memberExpression)
            {
                if (memberExpression.Expression == parameter)
                {
                    member = memberExpression.Member;
                    if (memberExpression.Expression is ParameterExpression parameterExpression)
                    {
                        otherParameter = parameterExpression;
                        return true;
                    }
                }
            }
            return false;
        }

        public bool IsValueTypeAndConstantEquality(Expression expression)
        {
            return expression is BinaryExpression binaryExpression && IsValueTypeAndConstantEquality(binaryExpression);
        }

        public bool IsValueTypeAndConstantEquality(BinaryExpression expression)
        {
            if (expression.NodeType == ExpressionType.Equal || expression.NodeType == ExpressionType.NotEqual)
            {
                if (!IsTypeMemberAccess(expression.Left, _loadParameter, out _, out _) &&
                    IsTypeMemberAccess(expression.Right, _valuesParameter, out _, out _))
                {
                    return true;
                }
                if (!IsTypeMemberAccess(expression.Right, _loadParameter, out _, out _) &&
                    IsTypeMemberAccess(expression.Left, _valuesParameter, out _, out _))
                {
                    return true;
                }
            }
            return false;
        }

        public bool IsValueTypeBool(Expression expression)
        {
            if (expression.NodeType == ExpressionType.Not)
            {
                expression = ((UnaryExpression)expression).Operand;
            }
            return IsTypeMemberAccess(expression, _valuesParameter, out MemberInfo member, out _) && 
                    member.GetMemberType() == typeof(bool);
        }

        //TODO factoriser code
        protected virtual MethodCallExpression ConstructContainsExpression(ParameterExpression parameter,
            MemberInfo entityMember, IEnumerable<object> keys)
        {
            var memberType = entityMember.GetMemberType();
            if (!memberType.IsNullable())
            {
                memberType = typeof(Nullable<>).MakeGenericType(memberType);
            }
            var containsMethod = EnumerableMethods.Contains.MakeGenericMethod(memberType);
            Expression foreignKeyExpr = Expression.MakeMemberAccess(parameter, entityMember);
            if (!foreignKeyExpr.Type.IsNullable())
            {
                foreignKeyExpr = Expression.Convert(foreignKeyExpr, typeof(Nullable<>).MakeGenericType(foreignKeyExpr.Type));
            }
            var castMethod = EnumerableMethods.Cast.MakeGenericMethod(memberType);
            keys = keys.Distinct().ToArray();
            var idsExp = Expression.Constant(castMethod.Invoke(null, new object[] { keys }));
            return Expression.Call(null, containsMethod, new Expression[] { idsExp, foreignKeyExpr });
        }
    }
}
