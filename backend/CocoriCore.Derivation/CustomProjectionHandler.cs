﻿using CocoriCore.Common;
using CocoriCore.Reflection.Extension;
using CocoriCore.Types.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocoriCore.Derivation
{
    //TODO ajouter tests pour cette classe (cf Ferias40)
    public abstract class CustomProjectionHandler<TDependency, TProjection> : IProjectionCreator, IAutoRegister
        where TDependency : class, IEntity
    {
        protected JoinLoader _joinLoader;
        protected IDerivationRepository _repository;
        protected ISynchronizerProcessor _processor;
        protected IFactory _factory;

        public CustomProjectionHandler(IDerivationRepository repository, IFactory factory, ISynchronizerProcessor processor, JoinLoader joinLoader)
        {
            _joinLoader = joinLoader;
            _repository = repository;
            _processor = processor;
            _factory = factory;
        }

        public virtual async Task<IEnumerable<IEntity>> LoadProjectionsAsync(DerivationRule rule, IEnumerable<IEntity> dependencies)
        {
            var info = rule.DependencyInfo;
            return await _joinLoader.LoadProjectionsAsync(info, dependencies);
        }

        public virtual async Task HandleAsync<TDep, TProj>(DerivationRule rule, DerivationSet set)
            where TDep : class, IEntity
            where TProj : class, IEntity
        {
            var projectionType = rule.ProjectionType;

            var oldProjections = await _joinLoader.LoadProjectionsAsync<IEntity>(rule.DependencyInfo, set);
            var dependencies = await _joinLoader.LoadDependenciesAsync<IEntity>(rule.DependencyInfo, set);
            var newProjections = await InitProjectionsAsync(rule, dependencies);
            await SynchronizeNewlyCreatedProjectionsAsync(rule, newProjections, dependencies);
            var keyFunc = GetKeyFunc(rule);
            var oldProjectionsByKey = oldProjections.ToDictionary(x => keyFunc(x));
            var newProjectionsByKey = newProjections.ToDictionary(x => keyFunc(x));

            var keysToDelete = oldProjectionsByKey.Keys.Except(newProjectionsByKey.Keys);
            var keysToUpdate = newProjectionsByKey.Keys.Intersect(oldProjectionsByKey.Keys);
            var keysToCreate = newProjectionsByKey.Keys.Except(oldProjectionsByKey.Keys);

            var projectionsToUpdate = new List<IEntity>();
            var projectionMembers = projectionType.GetPropertiesAndFields();
            foreach (var key in keysToUpdate)
            {
                var oldProjection = oldProjectionsByKey[key];
                var newProjection = newProjectionsByKey[key];
                var savedId = oldProjection.Id;
                foreach (var member in projectionMembers)
                {
                    member.InvokeSetter(oldProjection, member.InvokeGetter(newProjection));
                }
                oldProjection.Id = savedId;
                projectionsToUpdate.Add(oldProjection);
            }

            await _repository.DeleteManyAsync(keysToDelete.Select(x => oldProjectionsByKey[x]).ToArray());
            await _repository.UpdateManyAsync(projectionsToUpdate);
            await _repository.InsertManyAsync(keysToCreate.Select(x => newProjectionsByKey[x]).ToArray());
        }

        public virtual async Task<IEnumerable<IEntity>> InitProjectionsAsync(DerivationRule rule, IEnumerable<IEntity> dependencies)
        {
            var projections = await InitProjectionsAsync(rule, dependencies.Cast<TDependency>());
            return projections.Cast<IEntity>();
        }

        public abstract Task<IEnumerable<TProjection>> InitProjectionsAsync(DerivationRule rule, IEnumerable<TDependency> dependencies);


        public virtual async Task SynchronizeNewlyCreatedProjectionsAsync(DerivationRule rule, IEnumerable<IEntity> projections, IEnumerable<IEntity> dependencies)
        {
            var info = rule.DependencyInfo;
            var projectionType = rule.ProjectionType;
            var initMembers = info.ProjectionMembers;
            await _processor.SynchonizeNewlyCreatedProjectionsAsync(projectionType, dependencies, projections, initMembers);
        }

        public virtual Func<IEntity, object> GetKeyFunc(DerivationRule rule)
        {
            var keyMembers = rule.DependencyInfo.ProjectionMembers.ToArray();
            return e =>
            {
                var keys = new List<object>();
                keys.Add(rule.ProjectionType);
                var keyValues = keyMembers.Select(x => x.InvokeGetter(e));
                keys.AddRange(keyValues);
                return new CompositeKey(keys);
            };
        }

    }
}
