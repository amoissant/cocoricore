using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using CocoriCore.Collection.Extensions;
using CocoriCore.Common;
using CocoriCore.Reflection.Extension;

namespace CocoriCore.Derivation
{
    public class ManyProjectionHandler : IManyProjectionHandler, IProjectionCreator, IAutoRegister
    {
        protected IDerivationRepository _repository;
        protected IFactory _factory;
        protected IUnitOfWork _unitOfWork;
        protected ISynchronizerProcessor _processor;
        protected JoinLoader _joinLoader;

        public ManyProjectionHandler(IDerivationRepository repository, IFactory factory,
            ISynchronizerProcessor processor, JoinLoader joinLoader, IUnitOfWork unitOfWork)
        {
            _repository = repository;
            _factory = factory;
            _processor = processor;
            _joinLoader = joinLoader;
            _unitOfWork = unitOfWork;
        }

        public virtual async Task HandleAsync<TDependency, TProjection>(DerivationRule rule, DerivationSet set)
            where TDependency : class, IEntity
            where TProjection : class, IEntity
        {
            await HandleInsertsAsync(rule, set);
            await HandleUpdatesAsync(rule, set);
            await HandleDeletesAsync(rule, set);
        }

        protected virtual async Task HandleInsertsAsync(DerivationRule rule, DerivationSet set)
        {
            var dependencies = set.GetEntities(EntityFlags.Insert);
            var projections = await InitProjectionsAsync(rule, dependencies);
            await SynchronizeNewlyCreatedProjectionsAsync(rule, projections, dependencies);
            await _repository.InsertManyAsync(projections);
        }

        public virtual async Task<IEnumerable<IEntity>> InitProjectionsAsync(DerivationRule rule, IEnumerable<IEntity> oneDependencies)
        {
            var manyType = rule.ManyType;
            var getManyIds = rule.ManyIdsFunc;
            var manyDependencies = await _repository.LoadAsync(manyType, oneDependencies.SelectMany(x => getManyIds(x)));
            var manyDependenciesById = manyDependencies.ToDictionary(x => x.Id);

            var newProjections = new List<IEntity>();
            foreach (var oneDependency in oneDependencies)
            {
                var currentManyDependencies = GetManyDependencies(rule, manyDependenciesById, oneDependency);
                var projections = await InitProjectionsAsync(rule, oneDependency, currentManyDependencies);
                newProjections.AddRange(projections);
            }
            return newProjections;
        }

        private IEntity[] GetManyDependencies(DerivationRule rule, Dictionary<Guid, IEntity> manyDependenciesById, IEntity oneDependency)
        {
            var manyIds = rule.ManyIdsFunc(oneDependency);
            var manyDependencies = manyDependenciesById
                .Where(x => manyIds.Contains(x.Key))
                .Select(x => x.Value)
                .ToArray();
            return manyDependencies;
        }

        protected virtual Task<IEnumerable<IEntity>> InitProjectionsAsync(DerivationRule rule,
            IEntity oneDependency, IEnumerable<IEntity> manyDependencies)
        {
            var oneInfo = rule.OneRule.DependencyInfo;
            var manyInfo = rule.ManyRule.DependencyInfo;
            var projections = new List<IEntity>();
            foreach (var manyDependency in manyDependencies)
            {
                var projection = (IEntity)_factory.Create(rule.ProjectionType);
                oneInfo.ApplyEqualities(oneDependency, projection);
                manyInfo.ApplyEqualities(manyDependency, projection);
                projections.Add(projection);
            }
            return Task.FromResult(projections.AsEnumerable());
        }

        public virtual async Task SynchronizeNewlyCreatedProjectionsAsync(DerivationRule rule, IEnumerable<IEntity> projections, IEnumerable<IEntity> dependencies)
        {
            var manyType = rule.ManyType;
            var getManyIds = rule.ManyIdsFunc;
            var manyDependencies = await _repository.LoadAsync(manyType, dependencies.SelectMany(x => getManyIds(x)));
            var manyDependenciesById = manyDependencies.ToDictionary(x => x.Id);
            var projectionType = rule.ProjectionType;
            var initMembers = GetInitMembers(rule);
            await _processor.SynchonizeNewlyCreatedProjectionsAsync(projectionType, manyDependenciesById.Values, projections, initMembers);
        }

        private IEnumerable<MemberInfo> GetInitMembers(DerivationRule rule)
        {
            var oneInfo = rule.OneRule.DependencyInfo;
            var manyInfo = rule.ManyRule.DependencyInfo;
            return oneInfo.ProjectionMembers.Concat(manyInfo.ProjectionMembers);
        }

        protected virtual async Task HandleUpdatesAsync(DerivationRule rule, DerivationSet set)
        {
            var diff = _unitOfWork.Resolve<IProjectionDiff>();
            var dependencies = set.GetEntities(EntityFlags.Update);
            await diff.ComputeDiffAndSaveAsync(rule.ProjectionType, dependencies, rule);
        }

        protected virtual async Task HandleDeletesAsync(DerivationRule rule, DerivationSet set)
        {
            var oneInfo = rule.OneRule.DependencyInfo;
            var dependencies = set.GetEntities(EntityFlags.Delete);
            var existingProjections = await _joinLoader.LoadProjectionsAsync(oneInfo, dependencies);
            await _repository.DeleteManyAsync(existingProjections);
        }

        public virtual async Task<IEnumerable<IEntity>> LoadProjectionsAsync(DerivationRule rule, IEnumerable<IEntity> dependencies)
        {
            var info = rule.DependencyInfo;
            return await _joinLoader.LoadProjectionsAsync(info, dependencies);
        }

        public Func<IEntity, object> GetKeyFunc(DerivationRule rule)
        {
            var oneMembers = rule.OneRule.DependencyInfo.ProjectionMembers;
            var manyMembers = rule.ManyRule.DependencyInfo.ProjectionMembers;
            var keyMembers = oneMembers.Concat(manyMembers).ToArray();
            return e =>
            {
                var keys = new List<object>();
                var keyValues = keyMembers.Select(x => x.InvokeGetter(e));
                keys.AddRange(keyValues);
                return new CompositeKey(keys);
            };
        }

        
    }
}
