﻿using CocoriCore.Common;
using CocoriCore.Collection.Extensions;
using CocoriCore.Reflection.Extension;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CocoriCore.Derivation
{
    public class DependencyCoupler : IAutoRegister
    {
        public virtual IEnumerable<(TDependency dependency, IEnumerable<TProjection> projections)> GroupProjectionsByDependency<TDependency, TProjection>(
            DependencyInfo info, IEnumerable<TDependency> dependencies, IEnumerable<TProjection> projections)
            where TDependency : IEntity
            where TProjection : IEntity
        {
            var groupedProjections = projections
               .GroupBy(x => GetCompositeKey(x, info.Equalities.Select(e => e.ProjectionMember)))
               .ToDictionary(x => x.Key, x => x.ToArray());
            var groupedDependencies = dependencies
                .GroupBy(x => GetCompositeKey(x, info.Equalities.Select(e => e.DependencyMember)))
                .ToDictionary(x => x.Key, x => x.Single());

            var result = new List<(TDependency, IEnumerable<TProjection>)>();
            foreach (var key in groupedDependencies.Keys)
            {
                result.Add((groupedDependencies[key], groupedProjections.GetValueOrEmpty(key)));
            }
            return result;
        }

        public virtual IEnumerable<(TProjection projection, IEnumerable<TDependency> dependencies)> GroupDependenciesByProjection<TDependency, TProjection>(
            DependencyInfo info, IEnumerable<TDependency> dependencies, IEnumerable<TProjection> projections)
            where TDependency : IEntity
            where TProjection : IEntity
        {
            var groupedProjections = projections
                .GroupBy(x => GetCompositeKey(x, info.Equalities.Select(e => e.ProjectionMember)))
                .ToDictionary(x => x.Key, x => x.Single());
            var groupedDependencies = dependencies
                .GroupBy(x => GetCompositeKey(x, info.Equalities.Select(e => e.DependencyMember)))
                .ToDictionary(x => x.Key, x => x.AsEnumerable());

            var result = new List<(TProjection, IEnumerable<TDependency>)>();
            foreach (var key in groupedProjections.Keys)
            {
                result.Add((groupedProjections[key], groupedDependencies.GetValueOrEmpty(key)));
            }
            return result;
        }

        //TODO tout le temps utiliser cette méthode ou pas ? comment faire les contrôles quand doit y en avoir qu'un seul ?
        //factoriser code avec les autres méthodes de group
        public virtual IEnumerable<(IEnumerable<TDependency> dependencies, IEnumerable<TProjection> projections)> GroupDependenciesAndProjections<TDependency, TProjection>(
            DependencyInfo info, IEnumerable<TDependency> dependencies, IEnumerable<TProjection> projections)
            where TDependency : IEntity
            where TProjection : IEntity
        {
            var groupedProjections = projections
                .GroupBy(x => GetCompositeKey(x, info.Equalities.Select(e => e.ProjectionMember)))
                .ToDictionary(x => x.Key, x => x.AsEnumerable());
            var groupedDependencies = dependencies
                .GroupBy(x => GetCompositeKey(x, info.Equalities.Select(e => e.DependencyMember)))
                .ToDictionary(x => x.Key, x => x.AsEnumerable());

            var result = new List<(IEnumerable<TDependency>, IEnumerable<TProjection>)>();
            foreach (var key in groupedProjections.Keys)
            {
                result.Add((groupedDependencies.GetValueOrEmpty(key), groupedProjections.GetValueOrEmpty(key)));
            }
            return result;
        }

        protected virtual CompositeKey GetCompositeKey(IEntity entity, IEnumerable<MemberInfo> keyMembers)
        {
            var keyValues = keyMembers.Select(x => x.InvokeGetter(entity));
            return new CompositeKey(keyValues);
        }
    }
}
