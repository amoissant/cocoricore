﻿using CocoriCore.Common;
using System.Collections.Generic;

namespace CocoriCore.Derivation
{
    public interface IEntityEventStore
    {
        void Add(EntityOperation operation, IEntity entity);
        IEnumerable<EntityEvent> GetPendingEvents();
        void UseNewContainer();
    }
}