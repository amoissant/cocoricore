using System.Linq.Expressions;

namespace CocoriCore.Derivation
{
    public class EntityToConstantVisitor : ExpressionVisitor
    {
        private object _constant;
        private ParameterExpression _parameter;

        public EntityToConstantVisitor(ParameterExpression parameter, object constant)
        {
            _constant = constant;
            _parameter = parameter;
        }

        public Expression Modify(Expression expression)
        {
            return Visit(expression);
        }

        protected override Expression VisitParameter(ParameterExpression node)
        {
            if (node == _parameter)
            {
                return Expression.Constant(_constant, node.Type);
            }
            return node;
        }
    }
}