﻿using CocoriCore.Repository;
using System;
using System.Collections.Generic;
using CocoriCore.Common;
using CocoriCore.Collection.Extensions;

namespace CocoriCore.Derivation
{
    public class EntityEventStore : IEntityEventStore
    {
        private readonly IStateStore _stateStore;
        private Dictionary<(Type type, Guid id), EntityEvent> _container;

        public EntityEventStore(IStateStore stateStore)
        {
            _stateStore = stateStore;
            UseNewContainer();
        }

        public void Add(EntityOperation operation, IEntity entity)
        {
            var key = (entity.GetType(), entity.Id);
            if(operation == EntityOperation.UPDATE && !_stateStore.GetState(entity).HasChanged)
            {
                return;
            }
            var existingEvent = _container.GetValueOrDefault(key);
            if(existingEvent != null && existingEvent.Operation == EntityOperation.INSERT)
            {
                operation = EntityOperation.INSERT;
            }
            _container[key] = new EntityEvent(operation, entity, _stateStore.GetState(entity));
        }

        public IEnumerable<EntityEvent> GetPendingEvents()
        {
            return _container.Values;
        }

        public void UseNewContainer()
        {
            _container = new Dictionary<(Type type, Guid id), EntityEvent>();
        }
    }
}