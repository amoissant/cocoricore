using CocoriCore.Common;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;

namespace CocoriCore.Derivation
{
    public interface ISynchronizerProcessor
    {
        Task SynchonizeNewlyCreatedProjectionsAsync(Type projectionType, IEnumerable<IEntity> dependencies, IEnumerable<IEntity> projections, IEnumerable<MemberInfo> initMembers);
        Task SynchonizeWhileProjectionsAreModifiedAsync(DerivationRule rule, IEnumerable<IEntity> projections);
        Task ResetWhileProjectionsAreModifiedAsync(DerivationRule rule, IEnumerable<IEntity> projections);
    }
}
