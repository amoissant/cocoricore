using CocoriCore.Common;
using System;
using System.Threading.Tasks;

namespace CocoriCore.Derivation
{
    /// <summary>
    /// In a CQRS context "derivation" is the principle to synchronize the read model (also called <c>projections</c>) from the write model (origin)
    /// Derivation can be made in cascade meaning that a projection type can be the origin of another projection type
    /// Implementations of this class provide methods to synchronize origin to projections during the application lifecycle and methods
    /// to help resynchronize in case of maintenance or evolution.
    /// </summary>
    public interface IDerivationBus
    {
        /// <summary>
        /// Apply rules defined using <see cref="DerivationBusOptionsBuilder"/>
        /// wich define how projections are created, updated and deleted.
        /// </summary>
        Task ApplyRulesAsync();

        Task ApplyDifferedRulesAsync();

        /// <summary>
        /// Perform a full recalculation of all projections of type <typeparamref name="TProjection"/>.
        /// This operation will recreate missing lines and update existing ones.
        /// Recalulation is made in several independent batches.
        /// Each batch load entities from origin using <paramref name="batchSize"/> and <paramref name="originsOrderBy"/> then recalculate 
        /// associated projections.
        /// If an origin can't be established for some projections then they are not recalculated and will have to be deleted manually.
        /// </summary>  
        /// <param name="batchSize">The size of each batch. Default 1000</param>
        /// <param name="originsOrderBy">
        /// An optional collection of <see cref="BatchOrderBy{T}"/> for each origin in the write model.
        /// Used to define how they are ordered during load and so the processed order.
        /// By default all origins are ordered by <see cref="IEntity.Id"/>.
        /// </param>
        Task RecalculateAsync<TProjection>(int batchSize = 1000, params BatchOrderBy[] originsOrderBy);
        /// <summary>
        /// Perform a full recalculation of all projections of type <paramref name="projectionType"/>.
        /// This operation will recreate missing lines and update existing ones.
        /// Recalulation is made in several independent batches.
        /// Each batch load entities from origin using <paramref name="batchSize"/> and <paramref name="originsOrderBy"/> then recalculate 
        /// associated projections.
        /// If an origin can't be established for some projections then they are not recalculated and will have to be deleted manually.
        /// </summary>  
        /// <param name="projectionType">The projection type to recalculate.</param>
        /// <param name="batchSize">The size of each batch. Default 1000.</param>
        /// <param name="originsOrderBy">
        /// An optional collection of <see cref="BatchOrderBy{T}"/> for each origin in the write model.
        /// Used to define how they are ordered during load and so the processed order.
        /// By default all origins are ordered by <see cref="IEntity.Id"/>.
        /// </param>
        Task RecalculateAsync(Type projectionType, int batchSize = 1000, params BatchOrderBy[] originsOrderBy);
    }
}