﻿using CocoriCore.Common;
using System.Threading.Tasks;

namespace CocoriCore.Derivation
{
    public interface IDependencyHandler
    {
        Task HandleAsync<TDependency, TProjection>(DerivationRule rule, DerivationSet set)
            where TDependency : class, IEntity
            where TProjection : class, IEntity;
    }
}