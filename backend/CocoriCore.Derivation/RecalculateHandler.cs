﻿using System.Threading.Tasks;
using System.Collections.Generic;
using System;
using System.Linq;
using CocoriCore.Common;
using CocoriCore.Repository;
using CocoriCore.Linq.Async;
using CocoriCore.Collection.Extensions;

namespace CocoriCore.Derivation
{
    public class RecalculateHandler : IAutoRegister
    {
        protected DerivationBusOptions _options;
        protected IUnitOfWork _unitOfWork;

        public RecalculateHandler(DerivationBusOptions options, IUnitOfWork unitOfWork)
        {
            _options = options;
            _unitOfWork = unitOfWork;
        }

        public virtual async Task RecalculateAsync(Type projectionType, int batchSize, params BatchOrderBy[] originsOrderBy)
        {
            var rulesByOriginType = GetOriginRules(projectionType);
            foreach (var kvp in rulesByOriginType)
            {
                var dependencyType = kvp.Key;
                var rules = kvp.Value;
                await RecalculateProjectionsForAllDependenciesAsync(dependencyType, projectionType, batchSize, originsOrderBy, rules);
            }
        }

        protected virtual Dictionary<Type, DerivationRule[]> GetOriginRules(Type projectionType)
        {
            var originRules = _options
                .Rules
                .Where(x =>
                    x.ProjectionType == projectionType &&//TODO on pourrait améliorer et prendre en compte le polymorphisme ici
                    x.IsProjectionCreator == true)
                .ToArray();
            return originRules
                .GroupBy(x => x.DependencyType)
                .ToDictionary(x => x.Key, x => x.ToArray());
        }

        protected virtual async Task RecalculateProjectionsForAllDependenciesAsync(Type dependencyType, Type projectionType, 
            int batchSize, IEnumerable<BatchOrderBy> originsOrderBy, IEnumerable<DerivationRule> rules)
        {
            var repository = _unitOfWork.Resolve<IDerivationRepository>();
            var transactionHolder = _unitOfWork.Resolve<ITransactionHolder>();
            var orderBy = GetOrderBy(dependencyType, originsOrderBy);
            var dependencyCount = await repository.Query(dependencyType)
                .Cast<IEntity>()
                .CountAsync();
            var skip = 0;
            var diff = _unitOfWork.Resolve<IProjectionDiff>();

            while (skip <= dependencyCount)
            {
                var dependencies = await LoadDependenciesAsync(dependencyType, repository, orderBy, skip, batchSize);
                await diff.ComputeDiffAndSaveAsync(projectionType, dependencies, rules);
                await transactionHolder.CommitAsync();
                skip += batchSize;
            }
        }

        protected virtual BatchOrderBy GetOrderBy(Type dependencyType, IEnumerable<BatchOrderBy> originsOrderBy)
        {
            var orderBy = originsOrderBy.SingleOrDefault(x => x.DeclaringType == dependencyType);
            if (orderBy == null)
            {
                orderBy = new BatchOrderBy<IEntity>(x => x.Id);
            }
            return orderBy;
        }

        protected virtual async Task<IEntity[]> LoadDependenciesAsync(Type dependencyType, IDerivationRepository repository,
            BatchOrderBy orderBy, int skip, int batchSize)
        {
            var queryable = repository
                .Query(dependencyType)
                .Cast<IEntity>();
            if (orderBy.Direction == OrderDirection.Asc)
            {
                queryable = queryable.OrderBy(orderBy.Member);
            }
            else if (orderBy.Direction == OrderDirection.Desc)
            {
                queryable = queryable.OrderByDescending(orderBy.Member);
            }
            else
            {
                throw new NotImplementedException();
            }

            var dependencies = await queryable
                .Skip(skip)
                .Take(batchSize)
                .ToArrayAsync();
            return dependencies;
        }
    }
}
