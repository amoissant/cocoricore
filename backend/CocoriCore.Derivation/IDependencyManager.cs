﻿using System;
using System.Threading.Tasks;

namespace CocoriCore.Derivation
{
    public interface IDependencyManager
    {
        Task ProcessDerivationEventsAsync(Guid treatmentId);
    }
}