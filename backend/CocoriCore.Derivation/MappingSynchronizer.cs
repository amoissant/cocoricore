﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Castle.DynamicProxy;
using System.Linq;
using CocoriCore.Common;
using Microsoft.Extensions.Caching.Memory;
using CocoriCore.Collection.Extensions;

namespace CocoriCore.Derivation
{
    public class MappingSynchronizer : ISynchronizer, IAutoRegister
    {
        private readonly static ProxyGenerator _generator = new ProxyGenerator();
        protected readonly DependencyCoupler _coupler;
        protected readonly Dictionary<Type, IEntity> _emptyDependencyCache;//TODO revoir les caches si cet objet possède des états pour le traitement mais doit mettre en cache des trucs
        protected readonly Dictionary<(Type, Type, string), MappingAction> _mappingRules;

        protected IEnumerable<(IEntity dependency, IEnumerable<IEntity> projections)> _groups;
        protected MappingAction _mappingAction;
        protected Dictionary<IEntity, Guid> _savedIds;

        public MappingSynchronizer(DerivationBusOptions options, DependencyCoupler coupler, IMemoryCache memoryCache)
        {
            _coupler = coupler;
            _mappingRules = memoryCache.GetCached(this, x => GetMappingRules(options));
            _emptyDependencyCache = new Dictionary<Type, IEntity>();
        }

        private Dictionary<(Type, Type, string), MappingAction> GetMappingRules(DerivationBusOptions options)
        {
            //TODO lever exception explicite si doublons de clés
            return options
                .MappingActions
                .ToDictionary(x => (x.ProjectionType, x.EntityType, x.Discriminator), x => x);
        }

        public Task PrepareAsync(DependencyInfo info, IEnumerable<IEntity> dependencies, IEnumerable<IEntity> projections)
        {
            //TODO voir si on peut pas éviter les états sur cet objet, on enlèverait alors la phase 'prepare'
            _groups = _coupler.GroupProjectionsByDependency(info, dependencies, projections);
            _mappingAction = GetMappingAction(info.DependencyType, info.ProjectionType, info.Discriminator);
            _savedIds = projections.ToDictionary(x => x, x => x.Id);//TODO le faire (si pas déjà fait) au niveau de l'appelant
            return Task.CompletedTask;
        }

        protected virtual MappingAction GetMappingAction(Type dependencyType, Type projectionType, string discriminator)
        {
            try
            {
                return _mappingRules[(projectionType, dependencyType, discriminator)];
            }
            catch (KeyNotFoundException)
            {
                throw new ConfigurationException("No mapping defined for entity "
                    + $"{dependencyType}, projection {projectionType} and discriminator '{discriminator}'.");
            }
        }

        public Task ResetAsync(DependencyInfo info, IEnumerable<IEntity> dependencies, IEnumerable<IEntity> projections)
        {
            foreach (var projection in projections)
            {
                CallResetAction(_mappingAction, projection);
            }
            return Task.CompletedTask;
        }

        protected virtual void CallResetAction(MappingAction actions, IEntity projection)
        {
            IEntity emptyDependency = null;
            try
            {
                if (actions.ResetAction != null)
                {
                    actions.ResetAction(projection);
                    return;
                }
                else
                {
                    emptyDependency = GetEmpty(actions.EntityType);
                    actions.Map(emptyDependency, projection);
                }
            }
            catch (Exception e)//TODO a tester
            {
                throw ConstructMappingException(actions, projection, emptyDependency, e);
            }
        }

        protected virtual IEntity GetEmpty(Type entityType)
        {
            var emptyEntity = _emptyDependencyCache.GetValueOrDefault(entityType);
            if (emptyEntity == null)
            {
                emptyEntity = InstanciateEmpty(entityType);
                _emptyDependencyCache[entityType] = emptyEntity;
            }
            return emptyEntity;
        }

        protected virtual IEntity InstanciateEmpty(Type entityType)
        {
            if (entityType.IsInterface)
            {
                return (IEntity)_generator.CreateInterfaceProxyWithoutTarget(entityType, new DefaultValuePropertyInterceptor());
            }
            else if (entityType.IsAbstract)
            {
                return (IEntity)_generator.CreateClassProxy(entityType, new DefaultValuePropertyInterceptor());
            }
            else
            {
                return (IEntity)Activator.CreateInstance(entityType);
            }
        }

        protected virtual Exception ConstructMappingException(MappingAction mappingRule, object projection, object entity, Exception inner)
        {
            var exception = new DerivationException($"Derivation mapping "
                + $"with discriminator '{mappingRule.Discriminator}' from {mappingRule.EntityType} to projection {mappingRule.ProjectionType} "
                + "throw an exception, see inner for details.", inner);
            var debugDatas = new SerializableDatas
            {
                { "EntityType", mappingRule?.EntityType },
                { "Entity", entity },
                { "ProjectionType", mappingRule?.ProjectionType?.FullName },
                { "Projection", projection }
            };
            exception.Data[GetType().Name] = debugDatas;
            return exception;
        }

        public Task SynchonizeAsync(DependencyInfo info, IEnumerable<IEntity> dependencies, IEnumerable<IEntity> projections)
        {
            foreach (var group in _groups)
            {
                foreach (var projection in group.projections)
                {
                    info.ApplyEqualities(group.dependency, projection);
                    CallMapAction(_mappingAction, group.dependency, projection);
                    info.ApplyEqualities(group.dependency, projection);
                    projection.Id = _savedIds[projection];
                }
            }
            return Task.CompletedTask;
        }

        protected virtual void CallMapAction(MappingAction actions, object entity, object projection)
        {
            try
            {
                actions.Map(entity, projection);
            }
            catch (Exception e)//TODO a tester
            {
                throw ConstructMappingException(actions, projection, entity, e);
            }
        }
    }
}
