﻿using CocoriCore.Common;

namespace CocoriCore.Derivation
{
    public sealed class ProjectionHandler : ProjectionHandlerBase, IProjectionCreator, IProjectionHandler, IAutoRegister
    {
        public ProjectionHandler(IDerivationRepository repository, ISynchronizerProcessor processor, 
            IFactory factory, JoinLoader joinLoader) 
            : base(repository, processor, factory, joinLoader)
        {
        }
    }
}
