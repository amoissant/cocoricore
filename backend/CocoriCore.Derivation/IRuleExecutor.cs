﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CocoriCore.Derivation
{
    public interface IRuleExecutor
    {
        Task ExecuteAsync(IEnumerable<DerivationRule> rules, DerivationSet set);
    }
}