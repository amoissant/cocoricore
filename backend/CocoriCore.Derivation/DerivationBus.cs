using System.Threading.Tasks;
using System.Collections.Generic;
using System;
using System.Text;
using System.Linq;
using CocoriCore.Common;
using System.Diagnostics;
using Microsoft.Extensions.Logging;
using CocoriCore.Types.Extension;
using Microsoft.Extensions.Caching.Memory;

namespace CocoriCore.Derivation
{
    /// <summary>
    /// Default Implementation for <see cref="IDerivationBus"/>.
    /// </summary>
    public class DerivationBus : IDerivationBus
    {
        private readonly DerivationBusOptions _options;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRuleExecutor _ruleExecutor;
        private readonly IEntityEventStore _eventStore;
        private readonly IDerivationEventPersister _eventPersister;
        private readonly ILogger _logger;
        private readonly Guid _treatmentId;
        private readonly IMemoryCache _memoryCache;

        public DerivationBus(DerivationBusOptions options, IUnitOfWork unitOfWork, IMemoryCache memoryCache,
            IRuleExecutor ruleExecutor, IEntityEventStore eventStore, IDerivationEventPersister eventPersister, ILogger logger)
        {
            _options = options;
            _unitOfWork = unitOfWork;
            _ruleExecutor = ruleExecutor;
            _memoryCache = memoryCache;
            _eventPersister = eventPersister;
            _eventStore = eventStore;
            _logger = logger;
            _treatmentId = Guid.NewGuid();
        }

        /// <inheritDoc cref="IDerivationBus.ApplyRulesAsync"/>
        public async Task ApplyRulesAsync()
        {
            var sw = new Stopwatch();
            sw.Start();
            var derivationDeepTraces = new List<IEnumerable<Type>>();
            var events = GetEntityEvents();
            while (events.Any())
            {
                if (_options.DifferDependencyRules)
                {
                    await _eventPersister.PersistDerivationEventsAsync(_treatmentId);
                }
                _eventStore.UseNewContainer();

                derivationDeepTraces.Add(GetDerivationDeepTrace(events));

                await ProcessEventsAsync(events, _treatmentId, 0);                

                CheckMaxDerivationDeep(derivationDeepTraces);
                events = GetEntityEvents();
            }
            sw.Stop();
            _logger.LogDebug("Time to apply rules : {0}ms", sw.ElapsedMilliseconds);
        }

        private IEnumerable<EntityEvent> GetEntityEvents()
        {
            return _eventStore.GetPendingEvents();
        }

        private IEnumerable<Type> GetDerivationDeepTrace(IEnumerable<EntityEvent> events)
        {
            return events.Select(x => x.GetTartgetType()).Distinct();
        }

        private async Task ProcessEventsAsync(IEnumerable<EntityEvent> events, Guid treatmentId, int deep)
        {
            var sets = events
                .GroupBy(x => x.Entity.GetType())
                .Select(x => new DerivationSet(x.Key, x, treatmentId, deep));
            foreach (var set in sets)
            {
                var rules = GetRulesForSet(set);
                //TODO dans les traces dire les règles exécutées dans la transaction ou non
                await _ruleExecutor.ExecuteAsync(rules, set);
            }
        }

        private IEnumerable<DerivationRule> GetRulesForSet(DerivationSet set)
        {
            return _memoryCache.GetCached(this, x => x.GetRulesForSet_(set.DependencyType));
        }

        private IEnumerable<DerivationRule> GetRulesForSet_(Type dependencyType)
        {
            var rules = _options.Rules.Where(x => x.MatchDependencyType(dependencyType));
            if (_options.DifferDependencyRules)
            {
                rules = rules.Where(x => !x.IsDifferable);
            }
            return rules;
        }

        private void CheckMaxDerivationDeep(List<IEnumerable<Type>> derivationDeeps)
        {
            //TODO ici on trace les DerivationEvent alors que pas necessaire
            if (derivationDeeps.Count == _options.MaxDerivationDeep)
            {
                var sb = new StringBuilder();
                sb.AppendLine("Max dependency roundtrip reached.");
                sb.AppendLine("This mean there is a cycle in your dependencies and that the configuration can't prevent infinite treatments.");
                for (var i = 0; i < derivationDeeps.Count; i++)
                {
                    sb.AppendLine($"\tRountrip {i + 1} trace :");
                    foreach (var type in derivationDeeps[i])
                    {
                        sb.AppendLine($"\t\t{type.GetPrettyName()}");
                    }
                }
                sb.Append("You may also increase the max dependency rountrip allowed using ");
                sb.AppendLine($"{nameof(DerivationBusOptions)}.{nameof(DerivationBusOptions.MaxDerivationDeep)}.");
                throw new DerivationException(sb.ToString());
            }
        }

        /// <inheritDoc cref="IDerivationBus.ApplyDifferedRulesAsync"/>
        public async Task ApplyDifferedRulesAsync()
        {
            var sw = Stopwatch.StartNew();
            //TODO erreur si appel cette méthode et DifferDependencyRules false
            //TODO peut-on lever une exception au dispose pour dire que si on a pas appelé cette méthode et que paramétrage avec Asynchone alors erreur ?
            await _unitOfWork.Resolve<IDependencyManager>().ProcessDerivationEventsAsync(_treatmentId);
            _logger.LogDebug("Time to apply differed rules (wait={0}): {1}ms", _options.WaitDependencyRules, sw.ElapsedMilliseconds);
        }

        /// <inheritDoc cref="IDerivationBus.RecalculateAsync(Type, int, BatchOrderBy[])"/>
        public virtual async Task RecalculateAsync<TProjection>(int batchSize, params BatchOrderBy[] originsOrderBy)
        {
            await RecalculateAsync(typeof(TProjection), batchSize, originsOrderBy);
        }

        /// <inheritDoc cref="IDerivationBus.RecalculateAsync{TProjection}(int, BatchOrderBy[])"/>
        public virtual async Task RecalculateAsync(Type projectionType, int batchSize, params BatchOrderBy[] originsOrderBy)
        {
            await _unitOfWork.Resolve<RecalculateHandler>().RecalculateAsync(projectionType, batchSize, originsOrderBy);
        }
    }
}
