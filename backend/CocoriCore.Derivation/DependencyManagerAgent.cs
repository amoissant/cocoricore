﻿using CocoriCore.Common;
using CocoriCore.Repository;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CocoriCore.Linq.Async;
using Microsoft.Extensions.Caching.Memory;

namespace CocoriCore.Derivation
{
    public class DependencyManagerAgent : IDependencyManagerAgent, IAutoRegister
    {
        private readonly DerivationBusOptions _options;
        private readonly IDerivationRepository _repository;
        private readonly IRuleExecutor _ruleExecutor;
        private readonly IObjectSerializer _serializer;
        private readonly ILogger _logger;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IMemoryCache _memoryCache;

        public DependencyManagerAgent(IRuleExecutor ruleExecutor, DerivationBusOptions options, IDerivationRepository repository,
            IObjectSerializer serializer, ILogger logger, IUnitOfWorkFactory unitOfWorkFactory, IMemoryCache memoryCache)
        {
            _ruleExecutor = ruleExecutor;
            _options = options;
            _repository = repository;
            _serializer = serializer;
            _logger = logger;
            _unitOfWorkFactory = unitOfWorkFactory;
            _memoryCache = memoryCache;
        }

        public async Task<int> ProcessDerivationEventsAsync(Guid treatmentId, int derivationDeep)
        {
            var events = await LoadDerivationEventsAsync(treatmentId, _options.DependencyRulesBatchSize, derivationDeep);
            var eventCount = events.Count();
            _logger.LogDebug("[{0}:{1}] Loaded {2} events.", treatmentId, derivationDeep, eventCount);
            if(events.Any())
            {
                var sets = CreateDerivationSets(events, treatmentId, derivationDeep);
                await ExecuteRulesForSetsAsync(sets, derivationDeep);
                await _repository.DeleteManyAsync(events);
            }
            return eventCount;
        }

        private async Task<IEnumerable<DerivationEvent>> LoadDerivationEventsAsync(Guid treatmentId, int batchSize, int derivationDeep)
        {
            return await _repository//TODO optimiser en ne faisant pas de copie de ces entités au query car pas utile ou configurer les types d'entité exclus pour la copie ?
                //pouvoir configurer ceci au niveau du StateStoreDecorator (mesurer perf avant après)
                .Query<DerivationEvent>()
                .Where(x => x.TreatmentId == treatmentId && x.Deep == derivationDeep)
                .Take(batchSize)
                .ToArrayAsync();
        }

        private DerivationSet[] CreateDerivationSets(IEnumerable<DerivationEvent> events, Guid treatmentId, int derivationDeep)
        {
            var sets = events
                .GroupBy(x => x.EntityTypeName)
                .Select(x => CreateDerivationSet(x.Key, x, treatmentId, derivationDeep))
                .ToArray();
            var setsTypes = string.Join(", ", sets.Select(x => x.DependencyType.Name));
            _logger.LogDebug("[{0}:{1}] Events produced {2} sets of type : [{3}]", treatmentId, derivationDeep, sets.Count(), setsTypes);
            return sets;
        }


        private DerivationSet CreateDerivationSet(string targetTypeName, IEnumerable<DerivationEvent> events, Guid treatmentId, int derivationDeep)
        {
            var targetType = GetTargetTypeFromName(targetTypeName);
            var set = events
                .Select(x =>
                {
                    var entity = (IEntity)_serializer.Deserialize<object>(x.EntityJson);//TODO on devrait pas recharger depuis la bdd l'entité plutôt ? en cas de reprise du calcul après des erreur oui
                    //pas forcément, on peut se baser sur le rowVersion au moment de l'update pour avoir une approche optimiste ?
                    var previousEntity = x.PreviousEntityJson != null ? (IEntity)_serializer.Deserialize<object>(x.PreviousEntityJson) : null;
                    var state = x.PreviousEntityJson != null ? new State(previousEntity, entity) : null;
                    var context = new EntityEvent(x.Operation, entity, state);
                    return context;
                })
                .ToArray();
            return new DerivationSet(targetType, set, treatmentId, derivationDeep, true);
        }

        private Type GetTargetTypeFromName(string fullName)
        {
            var targetType = _options//TODO error conf si 0 assemblies fournies, à vérifier pendant validation des options
                .EntityAssemblies
                .SelectMany(x => x.GetTypes())
                .SingleOrDefault(x => x.FullName == fullName);//TODO mettre en cache
            if (targetType == null)
            {
                var errorMessage = $"Unable to find type {fullName} among provided assemblies.\n" +
                    $"Use builder.{nameof(DerivationBusOptionsBuilder.EntityAssemblies)}() to define assemblies containing your entities.";
                throw new InvalidOperationException(errorMessage);
            }
            return targetType;
        }

        private async Task ExecuteRulesForSetsAsync(IEnumerable<DerivationSet> sets, int derivationDeep)
        {
            foreach (var set in sets)
            {
                var rules = GetDifferedRules(set, derivationDeep);
                if (_options.ParallelizeDependencyRules)
                {
                    var tasks = rules
                        .GroupBy(x => x.ProjectionType)
                        .Select(x => ExecuteRuleInUnitOfWork(x, set))
                        .ToArray();
                    await Task.WhenAll(tasks);
                }
                else
                {
                    rules = rules.OrderBy(x => x.ProjectionType.Name);
                    await _ruleExecutor.ExecuteAsync(rules, set);
                }
            }
        }

        private async Task ExecuteRuleInUnitOfWork(IEnumerable<DerivationRule> rules, DerivationSet set)
        {
            using (var uow = _unitOfWorkFactory.NewUnitOfWork())
            {
                var ruleExecutor = uow.Resolve<IRuleExecutor>();
                await ruleExecutor.ExecuteAsync(rules, set);
                await uow.Resolve<ITransactionHolder>().CommitAsync();
            }
            //TODO ajouter gestion d'erreur
        }

        private IEnumerable<DerivationRule> GetDifferedRules(DerivationSet set, int deep)
        {
            return _memoryCache.GetCached(this, x => x.GetDifferedRules_(set.DependencyType, deep));
        }

        private IEnumerable<DerivationRule> GetDifferedRules_(Type dependencyType, int deep)
        {
            var rules = _options.Rules.Where(x => x.MatchDependencyType(dependencyType));
            if (deep == 0)
            {
                rules = rules.Where(x => x.IsDifferable);
            }
            return rules;
        }
    }
}
