using System.Collections.Generic;
using System.Linq.Expressions;

namespace CocoriCore.Derivation
{
    public class MemberAndParameterVisitor : ExpressionVisitor
    {
        public List<MemberExpression> Members;
        public List<ParameterExpression> Parameters;

        public MemberAndParameterVisitor()
        {
            Members = new List<MemberExpression>();
            Parameters = new List<ParameterExpression>();
        }

        public Expression Modify(Expression expression)
        {
            return Visit(expression);
        }

        protected override Expression VisitMember(MemberExpression node)
        {
            Members.Add(node);
            return base.VisitMember(node);
        }

        protected override Expression VisitParameter(ParameterExpression node)
        {
            Parameters.Add(node);
            return base.VisitParameter(node);
        }
    }
}