﻿using CocoriCore.Common;
using System;
using System.Linq.Expressions;

namespace CocoriCore.Derivation
{
    public class ModifyDerivationRuleBuilder<TDependency>
        where TDependency : class, IEntity
    {
        public DerivationBusOptionsBuilder Builder { get; }
        public DerivationRuleData Rule { get; }

        public ModifyDerivationRuleBuilder(DerivationBusOptionsBuilder builder, DerivationRuleData rule)
        {
            Builder = builder;
            Rule = rule;
        }

        public ModifyDerivationRuleBuilder<TDependency> Configure(Action<DerivationRuleData> configureActions)
        {
            configureActions(Rule);
            return this;
        }

        public ModifyDerivationRuleBuilder<TDependency, TProjection> ForProjection<TProjection>(Expression<Func<TDependency, TProjection, bool>> joinExpression)
             where TProjection : class, IEntity
        {
            var projectionType = typeof(TProjection);
            Rule.JoinExpression = joinExpression;
            Rule.ProjectionType = projectionType;
            Rule.IsDifferable = false;
            Rule.StringRepresentation += $"{nameof(ForProjection)}<{projectionType.Name}>()";
            return new ModifyDerivationRuleBuilder<TDependency, TProjection>(Builder, Rule);
        }
    }

    public class ModifyDerivationRuleBuilder<TDependency, TProjection>
        where TDependency : class, IEntity
        where TProjection : class, IEntity
    {
        public DerivationBusOptionsBuilder Builder { get; }
        public DerivationRuleData Rule { get; }

        public ModifyDerivationRuleBuilder(DerivationBusOptionsBuilder builder, DerivationRuleData rule)
        {
            Builder = builder;
            Rule = rule;
        }

        public ModifyDerivationRuleBuilder<TDependency, TProjection> Use<THandler>()
            where THandler : class, IProjectionCreator
        {
            var handlerType = typeof(THandler);
            Rule.IsProjectionCreator = true;
            Rule.SetHandler<THandler>((x, s, r) => x.HandleAsync<TDependency, TProjection>(r, s));
            Rule.StringRepresentation += $".{nameof(Use)}<{handlerType.Name}>()";
            return this;
        }
    }
}
