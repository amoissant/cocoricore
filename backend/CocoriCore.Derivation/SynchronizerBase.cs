﻿using CocoriCore.Common;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocoriCore.Derivation
{
    public abstract class SynchronizerBase<TDependency, TProjection> : ISynchronizer<TDependency, TProjection>
        where TDependency : IEntity
        where TProjection : IEntity
    {
        protected DependencyCoupler Coupler;

        protected SynchronizerBase(DependencyCoupler coupler)
        {
            Coupler = coupler;
        }

        public Task PrepareAsync(DependencyInfo info, IEnumerable<IEntity> dependencies, IEnumerable<IEntity> projections)
        {
            return PrepareAsync(info, dependencies.Cast<TDependency>(), projections.Cast<TProjection>());
        }

        public virtual Task PrepareAsync(DependencyInfo info, IEnumerable<TDependency> dependencies, IEnumerable<TProjection> projections)
        {
            return Task.CompletedTask;
        }

        public Task ResetAsync(DependencyInfo info, IEnumerable<IEntity> dependencies, IEnumerable<IEntity> projections)
        {
            return ResetAsync(info, dependencies.Cast<TDependency>(), projections.Cast<TProjection>());
        }

        public virtual Task ResetAsync(DependencyInfo info, IEnumerable<TDependency> dependencies, IEnumerable<TProjection> projections)
        {
            return SynchonizeAsync(info, dependencies.Cast<TDependency>(), projections.Cast<TProjection>());
        }

        public Task SynchonizeAsync(DependencyInfo info, IEnumerable<IEntity> dependencies, IEnumerable<IEntity> projections)
        {
            return SynchonizeAsync(info, dependencies.Cast<TDependency>(), projections.Cast<TProjection>());
        }

        public abstract Task SynchonizeAsync(DependencyInfo info, IEnumerable<TDependency> dependencies, IEnumerable<TProjection> projections);
    }
}