using CocoriCore.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CocoriCore.Derivation
{

    public interface ISynchronizer<TDependency, TProjection> : ISynchronizer, IAutoRegister
        where TDependency : IEntity
        where TProjection : IEntity
    {
        Task PrepareAsync(DependencyInfo info, IEnumerable<TDependency> dependencies, IEnumerable<TProjection> projections);
        Task ResetAsync(DependencyInfo info, IEnumerable<TDependency> dependencies, IEnumerable<TProjection> projections);
        Task SynchonizeAsync(DependencyInfo info, IEnumerable<TDependency> dependencies, IEnumerable<TProjection> projections);
    }

    public interface ISynchronizer
    {
        Task PrepareAsync(DependencyInfo info, IEnumerable<IEntity> dependencies, IEnumerable<IEntity> projections);
        Task ResetAsync(DependencyInfo info, IEnumerable<IEntity> dependencies, IEnumerable<IEntity> projections);
        Task SynchonizeAsync(DependencyInfo info, IEnumerable<IEntity> dependencies, IEnumerable<IEntity> projections);
    }
}