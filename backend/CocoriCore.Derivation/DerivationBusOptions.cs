using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CocoriCore.Derivation
{
    public class DerivationBusOptions
    {
        public bool ExceptionIfNoRuleDefined { get; }
        public bool HandleGuidToNullableGuid { get; }
        public int MaxDerivationDeep { get; }
        public int DependencyRulesBatchSize { get; }
        public bool DifferDependencyRules { get; }
        public bool WaitDependencyRules { get; }
        public bool ParallelizeDependencyRules { get; }
        public IEnumerable<Assembly> EntityAssemblies { get; }
        public IEnumerable<MappingAction> MappingActions { get; }
        public IEnumerable<DerivationRule> Rules { get; }

        public DerivationBusOptions(bool exceptionIfNoRuleDefined, bool handleGuidToNullableGuid,
            int maxDerivationDeep, bool differDependencyRules, int dependencyRulesBatchSize, bool waitDependencyRules,
            bool parallelizeDependencyRules, IEnumerable<Assembly> entityAssemblies, 
            IEnumerable<MappingAction> mappingActions, IEnumerable<DerivationRule> rules)
        {
            ExceptionIfNoRuleDefined = exceptionIfNoRuleDefined;
            HandleGuidToNullableGuid = handleGuidToNullableGuid;
            MaxDerivationDeep = maxDerivationDeep;
            DifferDependencyRules = differDependencyRules;
            DependencyRulesBatchSize = dependencyRulesBatchSize;
            WaitDependencyRules = waitDependencyRules;
            ParallelizeDependencyRules = parallelizeDependencyRules;
            EntityAssemblies = entityAssemblies ?? Enumerable.Empty<Assembly>();
            MappingActions = mappingActions ?? Enumerable.Empty<MappingAction>();
            Rules = rules;
        }
    }
}