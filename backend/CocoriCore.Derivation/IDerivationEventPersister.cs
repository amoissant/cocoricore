﻿using System;
using System.Threading.Tasks;

namespace CocoriCore.Derivation
{
    public interface IDerivationEventPersister
    {
        Task PersistDerivationEventsAsync(Guid treatmentId, int deep = 0);
    }
}