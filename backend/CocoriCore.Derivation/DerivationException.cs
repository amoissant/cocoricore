using System;

namespace CocoriCore.Derivation
{
    public class DerivationException : Exception
    {
        public DerivationException(string message)
            : base(message)
        {
        }

        public DerivationException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}