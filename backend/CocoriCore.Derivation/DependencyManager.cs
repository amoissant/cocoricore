﻿using CocoriCore.Common;
using CocoriCore.Repository;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace CocoriCore.Derivation
{
    public class DependencyManager : IDependencyManager, IAutoRegister
    {
        private readonly DerivationBusOptions _options;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly ILoggerFactory _loggerFactory;

        //TODO se faire injecter une uowFactory dédié à la dérivation sinon ca interfère avec le code métier,
        //on pourra paramétrer cette uow pour faire un commit à la fin et ainsi ne pas se coupler au transactionHolder
        public DependencyManager(IUnitOfWorkFactory unitOfWorkFactory, DerivationBusOptions options, ILoggerFactory loggerFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _options = options;
            _loggerFactory = loggerFactory;
        }

        public async Task ProcessDerivationEventsAsync(Guid treatmentId)
        {
            if (_options.WaitDependencyRules)
            {
                await ProcessDerivationEventsInBatchesAsync(treatmentId);
            }
            else
            {
                _ = Task.Run(() => ProcessDerivationEventsAndCatchErrorsAsync(treatmentId));
            }
        }


        private async Task ProcessDerivationEventsInBatchesAsync(Guid treatmentId)
        {
            var derivationDeep = 0;
            var totalForDeep = 0;
            var sw = Stopwatch.StartNew();
            while (true)
            {
                using (var uow = _unitOfWorkFactory.NewUnitOfWork())
                {
                    var agent = uow.Resolve<IDependencyManagerAgent>();
                    var logger = _loggerFactory.CreateLogger(GetType().FullName);
                    var transactionHolder = uow.Resolve<ITransactionHolder>();
                    logger.LogDebug("[{0}:{1}] Process derivation existings events for deep {1}.", treatmentId, derivationDeep, derivationDeep);
                    var eventCount = await agent.ProcessDerivationEventsAsync(treatmentId, derivationDeep);
                    await transactionHolder.CommitAsync();
                    totalForDeep += eventCount;
                    if (totalForDeep == 0)
                    {
                        break;
                    }
                    if (eventCount < _options.DependencyRulesBatchSize)
                    {
                        sw.Stop();
                        logger.LogDebug("[{0}] Derivation events for deep {1} processed in {2}ms.", treatmentId, derivationDeep, sw.ElapsedMilliseconds);
                        sw.Reset();
                        derivationDeep++;
                        totalForDeep = 0;
                    }
                    if (derivationDeep == _options.MaxDerivationDeep)
                    {
                        throw new DerivationException("Max roundtrip reached.");//TODO améliorer message d'erreur
                    }
                }
            }
        }

        private async Task ProcessDerivationEventsAndCatchErrorsAsync(Guid treatmentId)
        {
            try
            {
                await ProcessDerivationEventsInBatchesAsync(treatmentId);
            }
            catch (Exception e)
            {
                var errorMessage = new ErrorFormatter().Format(e);
                _loggerFactory.CreateLogger(GetType().FullName).LogError(errorMessage);
                //TODO en cas d'erreur appeler le errorBus ici ?
            }
        }
    }
}
