﻿using System.Collections.Generic;
using System;
using CocoriCore.Common;

namespace CocoriCore.Derivation
{
    public class BatchData
    {
        public IEnumerable<IEntity> Dependencies;
        public DerivationRule Rule;
        public Guid TreatmentId;
        public int DerivationDeep;

        public BatchData(IEnumerable<IEntity> dependencies, DerivationRule rule, Guid treatmentId, int derivationDeep)
        {
            Dependencies = dependencies;
            Rule = rule;
            TreatmentId = treatmentId;
            DerivationDeep = derivationDeep;
        }
    }
}

