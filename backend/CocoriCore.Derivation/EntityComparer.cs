﻿using CocoriCore.Common;
using System.Collections.Generic;

namespace CocoriCore.Derivation
{
    public class EntityComparer : IEqualityComparer<IEntity>
    {
        public bool Equals(IEntity x, IEntity y)
        {
            return x?.Id == y?.Id;
        }

        public int GetHashCode(IEntity obj)
        {
            return 21 ^ obj.Id.GetHashCode();
        }
    }
}
