using CocoriCore.Common;
using CocoriCore.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CocoriCore.Derivation
{
    public class EntityEvent
    {
        private readonly IState _state;
        public EntityOperation Operation { get; }
        public IEntity Entity { get; }
        public IEntity PreviousEntity => (IEntity)_state?.PreviousObject;

        public EntityEvent(EntityOperation operation, IEntity entity, IState state)
        {
            Operation = operation;
            Entity = entity;
            _state = state;
        }

        public IState<TEntity> GetState<TEntity>()
            where TEntity : class, IEntity
        {
            return _state?.Cast<TEntity>();
        }

        public Type GetTartgetType()
        {
            return Entity.GetType();
        }

        public bool AtleastOneHasChanged(IEnumerable<MemberInfo> properties)
        {
            if (properties.Count() == 0)
            {
                return true;
            }
            return _state.ModifiedMembers.Any(p => properties.Contains(p));
        }

        public override string ToString()
        {
            if (Operation == EntityOperation.UPDATE)
            {
                return $"{Operation} {Entity.GetType().Name} : [{string.Join(", ", _state.ModifiedMembers.Select(x => x.Name))}]";
            }
            return $"{Operation} {Entity.GetType().Name}";
        }
    }
}