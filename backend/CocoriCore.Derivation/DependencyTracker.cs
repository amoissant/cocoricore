using CocoriCore.Common;
using CocoriCore.Reflection.Extension;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CocoriCore.Derivation
{
    public class DependencyTracker
    {
        private TrackedMember[] _trackedMembers;

        public IEntity Projection { get; }
        public DerivationRule Rule { get; }
        public bool HasChanged => _trackedMembers.Any(x => x.HasChanged);

        public DependencyTracker(DerivationRule rule, IEntity projection)
        {
            Projection = projection;
            Rule = rule;
            _trackedMembers = Rule
                .DependencyInfo
                .ProjectionMembers
                .Select(x => new TrackedMember(x))
                .ToArray();
            SaveValues();
        }

        public void SaveValues()
        {
            foreach (var trackedValue in _trackedMembers)
            {
                trackedValue.SaveCurrent(Projection);
            }
        }

        public override string ToString()
        {
            return $"[{string.Join(", ", _trackedMembers.Select(x => x.ToString()))}]";
        }

        public IEnumerable<object> PreviousValues => _trackedMembers.Select(x => x.PreviousValue);
        public IEnumerable<object> CurrentValues => _trackedMembers.Select(x => x.CurrentValue);

        public class TrackedMember
        {
            public TrackedMember(MemberInfo memberInfo)
            {
                Member = memberInfo;
            }

            public bool HasChanged => !Equals(CurrentValue, PreviousValue);

            public void Init(object entity)
            {
                CurrentValue = Member.InvokeGetter(entity);
                PreviousValue = CurrentValue;
            }

            public void SaveCurrent(object entity)
            {
                PreviousValue = CurrentValue;
                CurrentValue = Member.InvokeGetter(entity);
            }

            public MemberInfo Member { get; private set; }

            public object PreviousValue { get; private set; }

            public object CurrentValue { get; private set; }

            public override string ToString()
            {
                if (HasChanged)
                {
                    return $"{Member.Name} : {(PreviousValue == null ? "(null)" : PreviousValue)} -> {(CurrentValue == null ? "(null)" : CurrentValue)}";
                }
                else
                {
                    return $"{Member.Name} : {(CurrentValue == null ? "(null)" : CurrentValue)}";
                }
            }
        }
    }
}
