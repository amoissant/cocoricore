﻿using System.Threading.Tasks;
using System.Collections.Generic;
using CocoriCore.Common;

namespace CocoriCore.Derivation
{
    public static class DerivationRepositoryExtensions
    {
        public static async Task InsertManyAsync(this IDerivationRepository repository, IEnumerable<IEntity> entities)
        {
            foreach (var entity in entities)
            {
                await repository.InsertAsync(entity);
            }
        }

        public static async Task UpdateManyAsync(this IDerivationRepository repository, IEnumerable<IEntity> entities)
        {
            foreach (var entity in entities)
            {
                await repository.UpdateAsync(entity);
            }
        }

        public static async Task DeleteManyAsync(this IDerivationRepository repository, IEnumerable<IEntity> entities)
        {
            foreach (var entity in entities)
            {
                await repository.DeleteAsync(entity);
            }
        }
    }
}
