﻿using System.Collections.Generic;

namespace CocoriCore.Derivation
{
    public interface IMappingConfiguration
    {
        IEnumerable<MappingAction> GetMapRules();
    }

}