﻿using CocoriCore.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CocoriCore.Derivation
{
    public interface IProjectionCreator
    {
        Task HandleAsync<TDependency, TProjection>(DerivationRule rule, DerivationSet set)
           where TDependency : class, IEntity
           where TProjection : class, IEntity;
        Task<IEnumerable<IEntity>> LoadProjectionsAsync(DerivationRule rule, IEnumerable<IEntity> dependencies);
        Task<IEnumerable<IEntity>> InitProjectionsAsync(DerivationRule rule, IEnumerable<IEntity> dependencies);
        Task SynchronizeNewlyCreatedProjectionsAsync(DerivationRule rule, IEnumerable<IEntity> projections, IEnumerable<IEntity> dependencies);
        Func<IEntity, object> GetKeyFunc(DerivationRule rule);
    }
}
