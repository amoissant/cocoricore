﻿using CocoriCore.Types.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace CocoriCore.Derivation
{
    public class DerivationRule
    {
        public DerivationRule(bool useMapping, string discriminator, Func<object, IEnumerable<Guid>> manyIdsFunc, 
            IEnumerable<MemberInfo> propertiesCondition, Type dependencyType, Type projectionType, Type manyType, 
            Type synchronizerType, LambdaExpression joinExpression, bool isProjectionCreator, MemberInfo manyFkMember, 
            DerivationRule oneRule, DerivationRule manyRule, bool isDifferable, bool isBatchPossible, bool ignoreOnInsert, 
            Type handlerType, Func<object, DerivationSet, DerivationRule, Task> handlerAction, string stringRepresentation)
        {
            DependencyType = dependencyType;
            UseMapping = useMapping;
            ProjectionType = projectionType;
            Discriminator = discriminator;
            PropertiesCondition = propertiesCondition ?? Enumerable.Empty<MemberInfo>();
            SynchronizerType = synchronizerType;
            JoinExpression = joinExpression;
            IsProjectionCreator = isProjectionCreator;
            IsDifferable = isDifferable;
            IsBatchPossible = isBatchPossible;
            IgnoreOnInsert = ignoreOnInsert;
            HandlerType = handlerType;
            HandlerAction = handlerAction;
            StringRepresentation = stringRepresentation;
            ManyIdsFunc = manyIdsFunc;
            ManyType = manyType;
            ManyFkMember = manyFkMember;
            OneRule = oneRule;
            ManyRule = manyRule;
            DependencyInfo = new DependencyInfo(joinExpression, discriminator);
        }

        public bool UseMapping { get; }
        public string Discriminator { get; }
        public Func<object, IEnumerable<Guid>> ManyIdsFunc { get; }
        public IEnumerable<MemberInfo> PropertiesCondition { get; }
        public Type DependencyType { get; }
        public Type ProjectionType { get; }
        public Type ManyType { get; }
        public Type SynchronizerType { get; }
        public LambdaExpression JoinExpression { get; }
        public DependencyInfo DependencyInfo { get; }
        public bool IsProjectionCreator { get; }
        public MemberInfo ManyFkMember { get; }
        public DerivationRule OneRule { get; }
        public DerivationRule ManyRule { get; }
        public bool IsDifferable { get; }
        public bool IsBatchPossible { get; }
        public bool IgnoreOnInsert { get; }
        public Type HandlerType { get; }
        public Func<object, DerivationSet, DerivationRule, Task> HandlerAction { get; }
        public string StringRepresentation { get; }

        public bool MatchDependencyType(Type dependencyType)
        {
            return dependencyType.IsAssignableTo(DependencyType) || dependencyType.IsAssignableToGeneric(DependencyType);
        }

        public override string ToString()
        {
            return StringRepresentation;
        }
    }
}