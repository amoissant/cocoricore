using CocoriCore.Common;
using System;
using System.Linq.Expressions;

namespace CocoriCore.Derivation
{
    public class DependencyOptionBuilder<TDependency>
         where TDependency : class, IEntity
    {
        private Expression<Func<TDependency, object>>[] _propertiesExpressions;
        public string Discriminator;
        public DerivationBusOptionsBuilder Builder { get; }
        public DerivationRuleData Rule { get; set; }

        public DependencyOptionBuilder(DerivationBusOptionsBuilder builder, DerivationRuleData rule, string discriminator, 
            Expression<Func<TDependency, object>>[] propertiesExpressions)
        {
            Rule = rule;
            _propertiesExpressions = propertiesExpressions;
            Discriminator = discriminator;
            Builder = builder;
        }

        public GroupDependencyOptionBuilder<TDependency, TProjection> Update<TProjection>(
            Expression<Func<TDependency, TProjection, bool>> joinExpression)
            where TProjection : class, IEntity
        {
            var projectionType = typeof(TProjection);
            Rule.JoinExpression = joinExpression;
            Rule.ProjectionType = projectionType;
            Rule.StringRepresentation += $".{nameof(Update)}<{projectionType.Name}>({joinExpression})";
            return new GroupDependencyOptionBuilder<TDependency, TProjection>(Builder, Rule);
        }
    }
}
