using System;

namespace CocoriCore.Derivation
{
    public class MappingAction
    {
        public Type EntityType;
        public Type ProjectionType;
        public string Discriminator;
        public Action<object, object> Map;
        public Action<object> ResetAction;

        public MappingAction(Type entityType)
        {
            EntityType = entityType;
        }
    }
}