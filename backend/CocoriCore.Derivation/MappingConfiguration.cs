using CocoriCore.Reflection.Extension;
using CocoriCore.Types.Extension;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CocoriCore.Derivation
{
    public class MappingConfigurationBase : IMappingConfiguration
    {
        private List<MappingAction> _mapRules;

        public MappingConfigurationBase()
        {
            _mapRules = new List<MappingAction>();
        }

        public IEnumerable<MappingAction> GetMapRules()
        {
            return _mapRules;
        }

        protected virtual void AutoMap<TDependency, TProjection>(TDependency entity, TProjection projection)
        {
            var entityType = typeof(TDependency);
            var projectionType = typeof(TProjection);
            foreach (var entityProperty in entityType.GetProperties())
            {
                var projectionProperty = projectionType.GetProperty(entityProperty.Name);
                if (projectionProperty != null)
                {
                    var entityPropertyValue = entityProperty.InvokeGetter(entity);
                    projectionProperty.InvokeSetter(projection, entityPropertyValue);
                }
            }
        }

        protected void ClearAll<TProjection>(TProjection projection)
        {
            var projectionType = typeof(TProjection);
            foreach (var property in projectionType.GetProperties())
            {
                var defaultValue = property.GetMemberType().GetDefault();
                property.InvokeSetter(projection, defaultValue);
            }
        }

        protected void CallMap<TDependency, TProjection>(TDependency entity, TProjection projection, string discriminator = null)
        {
            _mapRules
                .Single(x =>
                    x.EntityType == typeof(TDependency) &&
                    x.ProjectionType == typeof(TProjection) &&
                    x.Discriminator == discriminator)
                .Map(entity, projection);
        }

        protected Guid GetIdOrDefault<TEntity>(TEntity entity, Func<TEntity, Guid> memberAccessor)
        {
            if (entity == null)
            {
                return default(Guid);
            }
            else
            {
                return memberAccessor.Invoke(entity);
            }
        }

        protected Guid GetIdOrDefault<TEntity>(TEntity entity, Func<TEntity, Guid?> memberAccessor)
        {
            Guid? id = null;
            if (entity != null)
            {
                id = memberAccessor.Invoke(entity);
            }
            if (id == null)
            {
                id = default(Guid);
            }
            return id.Value;
        }

        protected Guid? GetIdOrNull<TEntity>(TEntity entity, Func<TEntity, Guid> memberAccessor)
        {
            Guid? id = null;
            if (entity != null)
                id = (Guid?)memberAccessor.Invoke(entity);
            if (id == Guid.Empty)
                id = null;
            return id;
        }

        protected Guid? GetIdOrNull<TEntity>(TEntity entity, Func<TEntity, Guid?> memberAccessor)
        {
            if (entity != null)
            {
                return memberAccessor.Invoke(entity);
            }
            else
            {
                return null;
            }
        }

        public MapRuleBuilder<TDependency> Map<TDependency>(string discriminator = null)
        {
            var rule = new MappingAction(typeof(TDependency));
            rule.Discriminator = discriminator;
            _mapRules.Add(rule);
            return new MapRuleBuilder<TDependency>(rule);
        }
    }

    public class MapRuleBuilder<TDependency>
    {
        private MappingAction _rule;

        public MapRuleBuilder(MappingAction rule)
        {
            _rule = rule;
        }

        public MapRuleBuilder<TDependency, TProjection> To<TProjection>()
        {
            _rule.ProjectionType = typeof(TProjection);
            return new MapRuleBuilder<TDependency, TProjection>(_rule);
        }
    }

    public class MapRuleBuilder<TDependency, TProjection>
    {
        private MappingAction _rule;

        public MapRuleBuilder(MappingAction rule)
        {
            _rule = rule;
        }

        public MapRuleBuilder<TDependency, TProjection> Using(Action<TDependency, TProjection> mapAction)
        {
            _rule.Map = (e, p) => mapAction((TDependency)e, (TProjection)p);
            return this;
        }

        public void ResetUsing(Action<TProjection> resetAction)
        {
            _rule.ResetAction = p => resetAction((TProjection)p);
        }
    }

}