using CocoriCore.Common;
using System;
using System.Collections.Generic;

namespace CocoriCore.Derivation
{
    public class DerivationSet
    {
        public IEnumerable<EntityEvent> Events { get; }
        public Type DependencyType { get; }
        public Guid TreatmentId { get; }
        public int Deep { get; }
        public bool UseSeparatedUnitOfWork { get; }

        public DerivationSet(Type entityType, IEnumerable<EntityEvent> events, Guid treatmentId, int deep, bool useSeparatedUnitOfWork = false)
        {
            Events = events;
            DependencyType = entityType;
            TreatmentId = treatmentId;
            Deep = deep;
            UseSeparatedUnitOfWork = useSeparatedUnitOfWork;
        }

        public IEnumerable<IEntity> GetEntities(EntityFlags flags)
        {
            return GetEntities<IEntity>(flags);
        }

        public IEnumerable<TEntity> GetEntities<TEntity>(EntityFlags flags)
            where TEntity : IEntity
        {
            var insert = (flags & EntityFlags.Insert) == EntityFlags.Insert;
            var update = (flags & EntityFlags.Update) == EntityFlags.Update;
            var delete = (flags & EntityFlags.Delete) == EntityFlags.Delete;
            var previousUpdate = (flags & EntityFlags.PreviousUpdate) == EntityFlags.PreviousUpdate;
            var previousDelete = (flags & EntityFlags.PreviousDelete) == EntityFlags.PreviousDelete;
            var entities = new List<TEntity>();
            foreach(var context in Events)
            {
                if(insert && context.Operation == EntityOperation.INSERT)
                {
                    entities.Add((TEntity)context.Entity);
                }
                if (update && context.Operation == EntityOperation.UPDATE)
                {
                    entities.Add((TEntity)context.Entity);
                }
                if (delete && context.Operation == EntityOperation.DELETE)
                {
                    entities.Add((TEntity)context.Entity);
                }
                if (previousUpdate && context.Operation == EntityOperation.UPDATE)
                {
                    entities.Add((TEntity)context.PreviousEntity);
                }
                if (previousDelete && context.Operation == EntityOperation.DELETE)
                {
                    entities.Add((TEntity)context.PreviousEntity);
                }
            }
            return entities;
        }
    }

    public enum EntityFlags
    {
        Insert = 1 << 0,
        Update = 1 << 1,
        Delete = 1 << 2,
        PreviousUpdate = 1 << 3,
        PreviousDelete = 1 << 4,
        All = Insert | Update | Delete | PreviousUpdate | PreviousDelete,
    }
}