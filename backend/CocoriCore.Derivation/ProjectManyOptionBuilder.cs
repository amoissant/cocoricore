using CocoriCore.Common;
using CocoriCore.Expressions.Extension;
using CocoriCore.Reflection.Extension;
using CocoriCore.Types.Extension;
using System;
using System.Linq.Expressions;

namespace CocoriCore.Derivation
{
    public class ProjectManyOptionBuilder<TOneDependency, TManyDependency>
         where TOneDependency : class, IEntity
         where TManyDependency : class, IEntity
    {
        public DerivationBusOptionsBuilder Builder { get; }
        public DerivationRuleData OriginRule { get; }
        public DerivationRuleData OneRule { get; set; }
        public DerivationRuleData ManyRule { get; set; }

        public ProjectManyOptionBuilder(DerivationBusOptionsBuilder builder, DerivationRuleData originRule)
        {
            Builder = builder;
            OriginRule = originRule;
        }

        public ProjectManyOptionBuilder<TOneDependency, TManyDependency, TProjection> IntoMany<TProjection>(
            Expression<Func<TProjection, Guid?>> oneFkMember,
            Expression<Func<TProjection, Guid?>> manyFkMember)
            where TProjection : class, IEntity
        {
            var oneJoinExpression = CreateJoinExpression<TOneDependency, TProjection>(oneFkMember);
            var manyJoinExpression = CreateJoinExpression<TManyDependency, TProjection>(manyFkMember);
            var originJoinExpression = CreateJoinExpressionExcludeId<TOneDependency, TProjection>(oneFkMember);

            var projectionType = typeof(TProjection);
            var oneType = typeof(TOneDependency);
            var manyType = typeof(TManyDependency);
            OneRule = Builder.AddRule(oneType);
            OneRule.JoinExpression = oneJoinExpression;
            OneRule.ProjectionType = projectionType;
            OneRule.IsDifferable = false;
            OneRule.IgnoreOnInsert = true;
            OneRule.StringRepresentation += $"{nameof(DerivationBusOptionsBuilder.ForDependency)}<{oneType.Name}>()";
            OneRule.StringRepresentation += $".{nameof(DependencyOptionBuilder<IEntity>.Update)}<{projectionType.Name}>({oneJoinExpression})";

            ManyRule = Builder.AddRule(typeof(TManyDependency));
            ManyRule.JoinExpression = manyJoinExpression;
            ManyRule.ProjectionType = projectionType;
            ManyRule.IsDifferable = false;
            ManyRule.StringRepresentation += $"{nameof(DerivationBusOptionsBuilder.ForDependency)}<{manyType.Name}>()";
            ManyRule.StringRepresentation += $".{nameof(DependencyOptionBuilder<IEntity>.Update)}<{projectionType.Name}>({manyJoinExpression})";

            OriginRule.ProjectionType = projectionType;
            OriginRule.ManyType = typeof(TManyDependency);
            OriginRule.ManyFkMember = manyFkMember.GetMemberInfo();
            OriginRule.OneRule = OneRule;
            OriginRule.ManyRule = ManyRule;
            OriginRule.JoinExpression = originJoinExpression;
            OriginRule.IsDifferable = false;
            OriginRule.StringRepresentation += $".{ nameof(IntoMany)}<{projectionType.Name}>()";

            return new ProjectManyOptionBuilder<TOneDependency, TManyDependency, TProjection>(Builder, OriginRule, OneRule, ManyRule);
        }

        private Expression<Func<TDependency, TProjection, bool>> CreateJoinExpression<TDependency, TProjection>(
            Expression<Func<TProjection, Guid?>> fkMemberExpression)
        {
            var projectionParameter = Expression.Parameter(typeof(TProjection), "p");
            var dependencyParameter = Expression.Parameter(typeof(TDependency), "e");
            var dependencyId = Expression.MakeMemberAccess(dependencyParameter, typeof(TDependency).GetProperty(nameof(IEntity.Id)));
            var nullableDependencyId = Expression.Convert(dependencyId, typeof(Guid?));
            var fkMemberInfo = fkMemberExpression.GetMemberInfo();
            Expression fkMember = Expression.MakeMemberAccess(projectionParameter, fkMemberInfo);
            if (!fkMemberInfo.GetMemberType().IsNullable())
            {
                fkMember = Expression.Convert(fkMember, typeof(Guid?));
            }
            var fkEquals = Expression.Equal(nullableDependencyId, fkMember);
            return Expression.Lambda<Func<TDependency, TProjection, bool>>(fkEquals, dependencyParameter, projectionParameter);
        }

        //TODO factoriser code
        private Expression<Func<TDependency, TProjection, bool>> CreateJoinExpressionExcludeId<TDependency, TProjection>(
            Expression<Func<TProjection, Guid?>> fkMemberExpression)
        {
            var projectionParameter = Expression.Parameter(typeof(TProjection), "p");
            var dependencyParameter = Expression.Parameter(typeof(TDependency), "e");
            var dependencyId = Expression.MakeMemberAccess(dependencyParameter, typeof(TDependency).GetProperty(nameof(IEntity.Id)));
            var projectionId = Expression.MakeMemberAccess(projectionParameter, typeof(TProjection).GetProperty(nameof(IEntity.Id)));
            var nullableDependencyId = Expression.Convert(dependencyId, typeof(Guid?));
            var fkMemberInfo = fkMemberExpression.GetMemberInfo();
            Expression fkMember = Expression.MakeMemberAccess(projectionParameter, fkMemberInfo);
            if (!fkMemberInfo.GetMemberType().IsNullable())
            {
                fkMember = Expression.Convert(fkMember, typeof(Guid?));
            }
            var fkEquals = Expression.Equal(nullableDependencyId, fkMember);
            var idNotEquals = Expression.NotEqual(projectionId, dependencyId);
            var joinExpr = Expression.AndAlso(idNotEquals, fkEquals);
            return Expression.Lambda<Func<TDependency, TProjection, bool>>(joinExpr, dependencyParameter, projectionParameter);
        }
    }


    public class ProjectManyOptionBuilder<TOneDependency, TManyDependency, TProjection>
        where TOneDependency : class, IEntity
        where TManyDependency : class, IEntity
        where TProjection : class, IEntity
    {
        public DerivationBusOptionsBuilder Builder { get; }
        public DerivationRuleData OriginRule { get; }
        public DerivationRuleData OneRule { get; }
        public DerivationRuleData ManyRule { get; }

        public ProjectManyOptionBuilder(DerivationBusOptionsBuilder builder, DerivationRuleData origineRule, DerivationRuleData oneRule, DerivationRuleData manyRule)
        {
            Builder = builder;
            OriginRule = origineRule;
            OneRule = oneRule;
            ManyRule = manyRule;
        }

        public ProjectManyOptionBuilder<TOneDependency, TManyDependency, TProjection> UsingMapping(string discriminator = null)
        {
            OriginRule.UseMapping = true;
            OriginRule.Discriminator = discriminator;
            OriginRule.SynchronizerType = typeof(MappingSynchronizer);
            OriginRule.SetHandler<IManyProjectionHandler>((x, s, r) => x.HandleAsync<TOneDependency, TProjection>(r, s));
            OriginRule.StringRepresentation += $".{nameof(UsingMapping)}({discriminator ?? string.Empty})";

            OneRule.UseMapping = true;
            OneRule.Discriminator = discriminator;
            OneRule.SynchronizerType = typeof(MappingSynchronizer);
            OneRule.SetHandler<IDependencyHandler>((x, s, r) => x.HandleAsync<TOneDependency, TProjection>(r, s));
            OneRule.StringRepresentation += $".{nameof(UsingMapping)}({discriminator ?? string.Empty})";

            ManyRule.UseMapping = true;
            ManyRule.Discriminator = discriminator;
            ManyRule.SynchronizerType = typeof(MappingSynchronizer);
            ManyRule.SetHandler<IDependencyHandler>((x, s, r) => x.HandleAsync<TManyDependency, TProjection>(r, s));
            ManyRule.StringRepresentation += $".{nameof(UsingMapping)}({discriminator ?? string.Empty})";
            return this;
        }
    }
}
