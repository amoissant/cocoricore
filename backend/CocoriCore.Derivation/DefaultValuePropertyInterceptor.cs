﻿using Castle.DynamicProxy;
using CocoriCore.Types.Extension;

namespace CocoriCore.Derivation
{
    public class DefaultValuePropertyInterceptor : IInterceptor
    {
        public DefaultValuePropertyInterceptor()
        {
        }

        public void Intercept(IInvocation invocation)
        {
            invocation.ReturnValue = invocation.Method.ReturnType.GetDefault();
        }
    }
}
