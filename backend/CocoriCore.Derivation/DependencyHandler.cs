using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System;
using Microsoft.Extensions.Logging;
using CocoriCore.Repository;
using CocoriCore.Common;

namespace CocoriCore.Derivation
{
    public class DependencyHandler : IDependencyHandler, IAutoRegister
    {
        private readonly DerivationBusOptions _options;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly ILogger _logger;

        public DependencyHandler(DerivationBusOptions options, IUnitOfWork unitOfWork, IUnitOfWorkFactory unitOfWorkFactory, ILogger logger)
        {
            _options = options;
            _unitOfWork = unitOfWork;
            _unitOfWorkFactory = unitOfWorkFactory;
            _logger = logger;
        }

        public virtual async Task HandleAsync<TDependency, TProjection>(DerivationRule rule, DerivationSet set)
            where TDependency : class, IEntity
            where TProjection : class, IEntity
        {
            _logger.LogDebug("[{0}:{1}:{2}] Start update {3}.", 
                set.TreatmentId, set.Deep, set.DependencyType.Name, rule.ProjectionType.Name);
            var previousCount = await UpdatePreviouslyDependingProjectionsAsync(rule, set);
            var currentCount = await UpdateCurrentlyDependingProjectionsAsync(rule, set);
            _logger.LogDebug("[{0}:{1}:{2}] {3} {4} updated.", 
                set.TreatmentId, set.Deep, set.DependencyType.Name, previousCount + currentCount, rule.ProjectionType.Name);
        }

        private async Task<int> UpdatePreviouslyDependingProjectionsAsync(DerivationRule rule, DerivationSet set)
        {
            var dependencies = GetPreviousDependencies(rule, set);
            if (dependencies.Count() > 0)
            {
                _logger.LogDebug("[{0}:{1}] Previous dependencies count : {2}", set.TreatmentId, set.Deep, dependencies.Count());
                if (set.UseSeparatedUnitOfWork)
                {
                    var batch = new BatchData(dependencies, rule, set.TreatmentId, set.Deep);
                    return await RunInSeparatedUnitOfWorkAsync<IDependencyHandlerAgent>(rule,
                        (handler, skip, take) => handler.BatchResetPreviousProjectionsAsync(batch, skip, take));
                }
                else
                {
                    return await _unitOfWork.Resolve<IDependencyHandlerAgent>().ResetPreviousProjectionsAsync(rule, dependencies);
                }
            }
            return 0;
        }

        private IEnumerable<IEntity> GetPreviousDependencies(DerivationRule rule, DerivationSet set)
        {
            var info = rule.DependencyInfo;
            return set
                .Events
                .Where(x =>
                    x.Operation == EntityOperation.DELETE ||
                    (x.Operation == EntityOperation.UPDATE && x.AtleastOneHasChanged(info.DependencyMembers)))
                .Select(x => x.PreviousEntity)
                .ToArray();
        }   

        private async Task<int> RunInSeparatedUnitOfWorkAsync<T>(DerivationRule rule, Func<T, int?, int?, Task<int>> action)
        {
            var totalCount = 0;
            var skip = rule.IsBatchPossible ? (int?)0 : null;
            var take = rule.IsBatchPossible ? (int?)_options.DependencyRulesBatchSize : null;
            bool mustContinue = true;
            while (mustContinue)
            {
                using (var uow = _unitOfWorkFactory.NewUnitOfWork())
                {
                    var service = uow.Resolve<T>();
                    var count = await action(service, skip, take);
                    mustContinue = count == take;
                    skip += count;
                    totalCount += count;
                    await uow.Resolve<ITransactionHolder>().CommitAsync();
                }
            }
            return totalCount;
        }

        private async Task<int> UpdateCurrentlyDependingProjectionsAsync(DerivationRule rule, DerivationSet set)
        {
            var dependencies = GetCurrentDependencies(rule, set);
            //TODO si pas de dépendencies alors ne rien faire ca évitera des appels en bdd.
            _logger.LogDebug("[{0}:{1}] Current dependencies count : {2}", set.TreatmentId, set.Deep, dependencies.Count());
            if (set.UseSeparatedUnitOfWork)
            {
                var batch = new BatchData(dependencies, rule, set.TreatmentId, set.Deep);
                return await RunInSeparatedUnitOfWorkAsync<IDependencyHandlerAgent>(rule,
                    (handler, skip, take) => handler.BatchUpdateCurrentProjectionsAsync(batch, skip, take));
            }
            else
            {
                return await _unitOfWork.Resolve<IDependencyHandlerAgent>().UpdateCurrentProjectionsAsync(rule, dependencies);
            }
        }

        private IEnumerable<IEntity> GetCurrentDependencies(DerivationRule rule, DerivationSet set)
        {
            var info = rule.DependencyInfo;
            var conditionnalProperties = rule.PropertiesCondition;
            return set
                .Events
                .Where(x =>
                    (x.Operation == EntityOperation.INSERT && !rule.IgnoreOnInsert) ||
                    x.Operation == EntityOperation.DELETE ||
                    (x.Operation == EntityOperation.UPDATE && x.AtleastOneHasChanged(conditionnalProperties)))
                .Select(x => x.Entity)
                .ToArray();
        }
    }
}

