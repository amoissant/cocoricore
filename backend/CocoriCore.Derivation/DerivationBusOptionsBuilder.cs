using CocoriCore.Common;
using CocoriCore.Expressions.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace CocoriCore.Derivation
{
    public class DerivationBusOptionsBuilder
    {
        public bool ExceptionIfNoRuleDefined { get; set; }
        public bool HandleGuidToNullableGuid { get; set; }
        public int MaxDerivationDeep { get; set; }
        public int DependencyRulesBatchSize { get; set; }
        public bool DifferDependencyRules { get; set; }
        public bool WaitDependencyRules { get; set; }
        public bool ParallelizeDependencyRules { get; set; }
        public IList<DerivationRuleData> RulesData { get; }
        public IEnumerable<Assembly> EntityAssemblies { get; set; }
        public IEnumerable<MappingAction> MappingActions { get; set; }
        public IList<Action<DerivationBusOptionsBuilder>> Validators { get; set; }

        public DerivationBusOptionsBuilder()
        {
            RulesData = new List<DerivationRuleData>();
            Validators = new List<Action<DerivationBusOptionsBuilder>>();
            ExceptionIfNoRuleDefined = false;
            HandleGuidToNullableGuid = true;
            MaxDerivationDeep = 10;
            DifferDependencyRules = false;
            DependencyRulesBatchSize = 1000;
            WaitDependencyRules = true;
            ParallelizeDependencyRules = false;

            Validators.Add(ValidateRuleAreComplete);
            Validators.Add(ValidateMappingActionsUnicity);
            Validators.Add(ValidateMappingForEachRule);
        }

        public virtual void ValidateMappingActionsUnicity(DerivationBusOptionsBuilder builder)
        {
            if (builder.MappingActions == null)
            {
                builder.MappingActions = new MappingAction[0];
            }
            var mappingGroups = builder.MappingActions
                .GroupBy(x => (x.EntityType, x.ProjectionType, x.Discriminator));
            foreach (var group in mappingGroups)
            {
                if (group.Count() > 1)
                {
                    throw new ConfigurationException($"Duplicate mapping defined for entity {group.Key.EntityType}, "
                       + $"projection {group.Key.ProjectionType} and discriminator '{group.Key.Discriminator}'.");
                }
            }
        }

        public virtual void ValidateMappingForEachRule(DerivationBusOptionsBuilder builder)
        {
            var rulesWithMapping = builder.RulesData.Where(x => x.UseMapping);
            var mappingActions = builder.MappingActions
                .GroupBy(x => (x.EntityType, x.ProjectionType, x.Discriminator))
                .ToDictionary(x => x.Key, x => x.ToArray());
            foreach (var rule in rulesWithMapping)
            {
                var ruleKey = (TargetType: rule.DependencyType, ProjectionType: rule.ProjectionType, Discriminator: rule.Discriminator);
                if (!mappingActions.ContainsKey(ruleKey))
                {
                    var errorMessage = $"No mapping defined from entity {ruleKey.TargetType} to projection {ruleKey.ProjectionType} " +
                        $"with discriminator '{ruleKey.Discriminator}'.\n" +
                        "Ensure you have configured mappings using " +
                        $"{nameof(DerivationBusMappingExtension.ConfigureMappings)}() and defined a mapping action for " +
                        $"this entity, projection and discriminator within an implementation of {typeof(IMappingConfiguration)}.";
                    throw new ConfigurationException(errorMessage);
                }
            }
        }

        public virtual void ValidateRuleAreComplete(DerivationBusOptionsBuilder builder)
        {
            var incompleteRules = builder.RulesData
                .Where(x => (!x.IsProjectionCreator && x.SynchronizerType == null) || x.ProjectionType == null);
            if (incompleteRules.Any())
            {
                var rule = incompleteRules.First();
                var errorMessage = $"Rule for entity {rule.DependencyType} is not complete.\n" +
                    $"Ensure you have define a projection type and a synchronizer type.";
                throw new ConfigurationException(errorMessage);
            }
        }

        public DerivationBusOptions Build()
        {
            foreach (var validator in Validators)
            {
                validator(this);
            }
            return new DerivationBusOptions(
                exceptionIfNoRuleDefined: ExceptionIfNoRuleDefined,
                handleGuidToNullableGuid: HandleGuidToNullableGuid,
                maxDerivationDeep: MaxDerivationDeep,
                differDependencyRules: DifferDependencyRules,
                dependencyRulesBatchSize: DependencyRulesBatchSize,
                waitDependencyRules: WaitDependencyRules,
                parallelizeDependencyRules: ParallelizeDependencyRules,
                entityAssemblies: EntityAssemblies,
                mappingActions: MappingActions,
                rules: RulesData.Select(x => CreateReadOnlyDerivationRule(x)).ToArray());
        }

        private DerivationRule CreateReadOnlyDerivationRule(DerivationRuleData rule)
        {
            return new DerivationRule(
                useMapping: rule.UseMapping,
                discriminator: rule.Discriminator,
                manyIdsFunc: rule.ManyIdsFunc,
                propertiesCondition: rule.PropertiesCondition,
                dependencyType: rule.DependencyType,
                projectionType: rule.ProjectionType,
                manyType: rule.ManyType,
                synchronizerType: rule.SynchronizerType,
                joinExpression: rule.JoinExpression,
                isProjectionCreator: rule.IsProjectionCreator,
                manyFkMember: rule.ManyFkMember,
                oneRule: rule.OneRule != null ? CreateReadOnlyDerivationRule(rule.OneRule) : null,
                manyRule: rule.ManyRule != null ? CreateReadOnlyDerivationRule(rule.ManyRule) : null,
                isDifferable: rule.IsDifferable,
                isBatchPossible: rule.IsBatchPossible,
                ignoreOnInsert: rule.IgnoreOnInsert,
                handlerType: rule.HandlerType,
                handlerAction: rule.HandlerAction,
                stringRepresentation: rule.StringRepresentation
            );
        }

        public virtual DerivationRuleData AddRule(Type dependencyType)
        {
            var rule = new DerivationRuleData();
            rule.DependencyType = dependencyType;
            RulesData.Add(rule);
            return rule;
        }

        public ProjectOptionBuilder<TEntity> Project<TEntity>()
            where TEntity : class, IEntity
        {
            var entityType = typeof(TEntity);
            var rule = AddRule(entityType);
            rule.IsProjectionCreator = true;
            rule.StringRepresentation += $"{ nameof(Project)}<{entityType.Name}>()";
            return new ProjectOptionBuilder<TEntity>(this, rule);
        }

        public ProjectManyOptionBuilder<TOneEntity, TManyEntity> Project<TOneEntity, TManyEntity>(Func<TOneEntity, IEnumerable<Guid>> manyIdsFunc)
            where TOneEntity : class, IEntity
            where TManyEntity : class, IEntity
        {
            var oneType = typeof(TOneEntity);
            var manyType = typeof(TManyEntity);
            var originRule = AddRule(oneType);
            originRule.ManyIdsFunc = e => manyIdsFunc((TOneEntity)e);
            originRule.IsProjectionCreator = true;
            originRule.StringRepresentation += $"{ nameof(Project)}<{oneType.Name},{manyType.Name}>()";
            return new ProjectManyOptionBuilder<TOneEntity, TManyEntity>(this, originRule);
        }

        public DependencyOptionBuilder<TDependency> ForDependency<TDependency>(
            params Expression<Func<TDependency, object>>[] propertiesExpressions)
            where TDependency : class, IEntity
        {
            var dependencyType = typeof(TDependency);
            var rule = AddRule(dependencyType);
            rule.PropertiesCondition = propertiesExpressions.Select(x => x.GetMemberInfo());
            rule.StringRepresentation += $"{nameof(ForDependency)}<{dependencyType.Name}>()";
            return new DependencyOptionBuilder<TDependency>(this, rule, null, propertiesExpressions);
        }

        public ModifyDerivationRuleBuilder<TDependency> WhenModify<TDependency>()
            where TDependency : class, IEntity
        {
            var dependencyType = typeof(TDependency);
            var rule = AddRule(dependencyType);
            rule.StringRepresentation += $"{nameof(WhenModify)}<{dependencyType.Name}>()";
            return new ModifyDerivationRuleBuilder<TDependency>(this, rule);
        }
    }
}
