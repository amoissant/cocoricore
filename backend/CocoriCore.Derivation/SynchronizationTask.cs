﻿using CocoriCore.Common;
using System.Collections.Generic;

namespace CocoriCore.Derivation
{
    public class SynchronizationTask
    {
        public SynchronizationTask(DependencyInfo info, IEnumerable<IEntity> dependencies, 
            IEnumerable<IEntity> projections, ISynchronizer synchonizer)
        {
            Info = info;
            Dependencies = dependencies;
            Projections = projections;
            Synchronizer = synchonizer;
        }

        public DependencyInfo Info { get; }
        public IEnumerable<IEntity> Dependencies { get; }
        public IEnumerable<IEntity> Projections { get; }
        public ISynchronizer Synchronizer { get; }
    }
}
