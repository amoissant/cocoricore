using CocoriCore.Common;

namespace CocoriCore.Derivation
{
    public class GroupDependencyOptionBuilder<TDependency, TProjection>
       where TDependency : class, IEntity
       where TProjection : class, IEntity
    {
        public DerivationBusOptionsBuilder Builder { get; }
        public DerivationRuleData Rule { get; }

        public GroupDependencyOptionBuilder(DerivationBusOptionsBuilder builder, DerivationRuleData rule)
        {
            Builder = builder;
            Rule = rule;
        }

        public GroupDependencyOptionBuilder<TDependency, TProjection> UsingMapping(string discriminator = null)
        {
            Rule.SynchronizerType = typeof(MappingSynchronizer);
            Rule.UseMapping = true;
            Rule.Discriminator = discriminator;
            Rule.SetHandler<IDependencyHandler>((x, s, r) => x.HandleAsync<TDependency, TProjection>(r, s));
            Rule.StringRepresentation += $".{nameof(UsingMapping)}({discriminator ?? string.Empty})";
            return this;
        }

        public GroupDependencyOptionBuilder<TDependency, TProjection> Using<TSynchronizer>(string discriminator = null)
            where TSynchronizer : class, ISynchronizer<TDependency, TProjection>
        {
            var synchronizerType = typeof(TSynchronizer);
            Rule.SynchronizerType = synchronizerType;
            Rule.Discriminator = discriminator;
            Rule.SetHandler<IDependencyHandler>((x, s, r) => x.HandleAsync<TDependency, TProjection>(r, s));
            Rule.IsBatchPossible = false;
            Rule.StringRepresentation += $".{nameof(Using)}<{synchronizerType.Name}>({discriminator ?? string.Empty})";
            return this;
        }
    }
}
