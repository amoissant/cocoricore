using CocoriCore.Common;
using System;
using System.Linq.Expressions;

namespace CocoriCore.Derivation
{
    public class ProjectOptionBuilder<TDependency>
         where TDependency : class, IEntity
    {
        public DerivationBusOptionsBuilder Builder { get; }
        public DerivationRuleData OriginRule { get; }
        public DerivationRuleData DependencyRule { get; set; }

        public ProjectOptionBuilder(DerivationBusOptionsBuilder builder, DerivationRuleData originRule)
        {
            Builder = builder;
            OriginRule = originRule;
        }

        public ProjectOptionBuilder<TDependency, TProjection> Into<TProjection>(Expression<Func<TDependency, TProjection, bool>> joinExpression = null)
            where TProjection : class, IEntity
        {
            if(joinExpression == null)
            {
                joinExpression = (e, p) => e.Id == p.Id;
            }
            var dependencyType = typeof(TDependency);
            var projectionType = typeof(TProjection);

            DependencyRule = Builder.AddRule(dependencyType);
            DependencyRule.JoinExpression = joinExpression;
            DependencyRule.IsDifferable = false;
            DependencyRule.StringRepresentation += $"{nameof(DerivationBusOptionsBuilder.ForDependency)}<{dependencyType.Name}>()";
            DependencyRule.StringRepresentation += $".{nameof(DependencyOptionBuilder<IEntity>.Update)}<{projectionType.Name}>({joinExpression})";

            OriginRule.ProjectionType = projectionType;
            OriginRule.JoinExpression = joinExpression;
            OriginRule.IsDifferable = false;
            OriginRule.StringRepresentation += $".{nameof(Into)}<{projectionType.Name}>()";

            return new ProjectOptionBuilder<TDependency, TProjection>(Builder, OriginRule, DependencyRule);
        }
    }


    public class ProjectOptionBuilder<TDependency, TProjection>
        where TDependency : class, IEntity
        where TProjection : class, IEntity
    {
        public DerivationBusOptionsBuilder Builder { get; }
        public DerivationRuleData OriginRule { get; }
        public DerivationRuleData DependencyRule { get; set; }

        public ProjectOptionBuilder(DerivationBusOptionsBuilder builder, DerivationRuleData originRule, DerivationRuleData dependencyRule)
        {
            Builder = builder;
            OriginRule = originRule;
            DependencyRule = dependencyRule;
        }

        public ProjectOptionBuilder<TDependency, TProjection> UsingMapping(string discriminator = null)
        {
            DependencyRule.ProjectionType = typeof(TProjection);
            DependencyRule.UseMapping = true;
            DependencyRule.IgnoreOnInsert = true;
            DependencyRule.SynchronizerType = typeof(MappingSynchronizer);
            DependencyRule.Discriminator = discriminator;
            DependencyRule.SetHandler<IDependencyHandler>((x, s, r) => x.HandleAsync<TDependency, TProjection>(r, s));
            DependencyRule.StringRepresentation += $".{nameof(UsingMapping)}({discriminator ?? string.Empty})";

            OriginRule.SetHandler<IProjectionHandler>((x, s, r) => x.HandleAsync<TDependency, TProjection>(r, s));
            OriginRule.StringRepresentation += $".{nameof(UsingMapping)}({discriminator ?? string.Empty})";
            return this;
        }

        public ProjectOptionBuilder<TDependency, TProjection> Using<TSynchronizer>()
            where TSynchronizer : class, IProjectionHandler
        {
            //TODO tester DependencyRule car si on l'enl�ve aucun test ne p�te
            Expression<Func<TDependency, TProjection, bool>> joinExpression = (e, p) => e.Id == p.Id;
            DependencyRule = Builder.AddRule(typeof(TDependency));
            DependencyRule.ProjectionType = typeof(TProjection);
            DependencyRule.UseMapping = true;
            DependencyRule.JoinExpression = joinExpression;
            DependencyRule.SynchronizerType = typeof(MappingSynchronizer);
            DependencyRule.SetHandler<IDependencyHandler>((x, s, r) => x.HandleAsync<TDependency, TProjection>(r, s));
            DependencyRule.StringRepresentation += $".{nameof(UsingMapping)}()";

            OriginRule.JoinExpression = joinExpression;//TODO aucun test ne p�te si on enl�ve cette ligne, � tester
            OriginRule.UseMapping = false;//TODO le synchonizerProcessor fait qu'on ne devrait pas avoir � pr�ciser si mapping ou non
            OriginRule.SetHandler<TSynchronizer>((x, s, r) => x.HandleAsync<TDependency, TProjection>(r, s));
            OriginRule.StringRepresentation += $".{nameof(Using)}<{typeof(TSynchronizer).Name}>()";
            return this;
        }
    }
}
