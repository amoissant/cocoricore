﻿using AutoMapper;
using AutoMapper.Configuration;

namespace CocoriCore.AutoMapper
{
    public class AutoMapperOptionsBuilder
    {
        public IMapperConfigurationExpression Expression => _expression;

        private MapperConfigurationExpression _expression;

        public AutoMapperOptionsBuilder()
        {
            _expression = new MapperConfigurationExpression();
        }

        public MapperConfiguration Configuration => new MapperConfiguration(_expression);
    }
}
