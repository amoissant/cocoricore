﻿using Microsoft.AspNetCore.Http;
using System;

namespace CocoriCore.AspNetCore
{
    public class HttpContextProvider : IHttpContextProvider
    {
        private HttpContext _httpContext;

        public bool HasHttpContext => _httpContext != null;

        public HttpContext HttpContextOrDefault => _httpContext;

        public HttpContext HttpContext
        {
            get
            {
                if (_httpContext == null)
                {
                    throw new InvalidOperationException($"{nameof(HttpContext)} is null, you may be out of an http call context.");
                }
                return _httpContext;
            }
            set
            {
                if (_httpContext != null)
                {
                    throw new InvalidOperationException($"Modification of {nameof(HttpContext)} not allowed.");
                }
                _httpContext = value;
            }
        }
    }
}
