﻿using CocoriCore.Types.Extension;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Configuration;
using CocoriCore.Reflection.Extension;

namespace CocoriCore.AspNetCore
{
    public static class CommandLineWebHostBuilderExtensions
    {
        public static IWebHostBuilder UseCommandLine<T>(this IWebHostBuilder builder, T options)
        {
            return builder.ConfigureAppConfiguration((c, b) => b.AddInMemoryCollection(options.FlattenToKeyValuePairs()));
        }
    }

    public static class ObjectConfigurationExtensions
    {
        public static IEnumerable<KeyValuePair<string, string>> FlattenToKeyValuePairs(this object obj)
        {
            var finalTypes = new[]
            {
                typeof(object),
                typeof(bool),
                typeof(byte),
                typeof(sbyte),
                typeof(char),
                typeof(decimal),
                typeof(double),
                typeof(float),
                typeof(int),
                typeof(uint),
                typeof(long),
                typeof(ulong),
                typeof(short),
                typeof(ushort),
                typeof(string),
                typeof(Guid),
                typeof(DateTime),
                typeof(TimeSpan),
            };

            return FlattenObject(obj, new string[0], finalTypes)
                .Select(x => new KeyValuePair<string, string>(string.Join(":", x.Key), x.Value));
        }

        private static IEnumerable<KeyValuePair<IEnumerable<string>, string>> FlattenObject(this object obj, IEnumerable<string> parentPaths, IEnumerable<Type> finalTypes)
        {
            if (obj == null)
            {
                return new[] { new KeyValuePair<IEnumerable<string>, string>(parentPaths, null) };
            }
            else if (finalTypes.Contains(obj.GetType()) || obj.GetType().IsEnum)
            {
                return new[] { new KeyValuePair<IEnumerable<string>, string>(parentPaths, obj?.ToString()) };
            }
            else if (obj is IEnumerable enumerable)
            {
                return enumerable.Cast<object>().SelectMany((x, i) => FlattenObject(x, parentPaths.Append(i.ToString()), finalTypes));
            }
            else
            {
                var results = new List<KeyValuePair<IEnumerable<string>, string>>();
                foreach (var member in obj.GetType().GetPropertiesAndFields())
                {
                    var memberValue = member.InvokeGetter(obj);
                    results.AddRange(FlattenObject(memberValue, parentPaths.Append(member.Name), finalTypes));
                }
                return results;
            }
        }
    }
}
