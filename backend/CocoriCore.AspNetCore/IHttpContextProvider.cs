﻿using Microsoft.AspNetCore.Http;

namespace CocoriCore.AspNetCore
{
    public interface IHttpContextProvider
    {
        HttpContext HttpContext { get; set; }
        bool HasHttpContext { get; }
        HttpContext HttpContextOrDefault { get; }
    }
}
