﻿using CocoriCore.Common;
using Microsoft.Extensions.Configuration;
using System;
using System.Text.RegularExpressions;

namespace CocoriCore.AspNetCore
{
    public static class ConfigurationExtension
    {
        private static readonly Regex _timeSpanRegex = new Regex("^(?<value>\\d+)(?<unit>[smhd])$");

        public static TimeSpan? GetDuration(this IConfiguration configuration, string key)
        {
            var stringValue = configuration.GetValue<string>(key);
            if (stringValue == null) return null;

            var match = _timeSpanRegex.Match(stringValue);
            if (match.Success)
            {
                var value = int.Parse(match.Groups["value"].Value);
                var unit = match.Groups["unit"].Value;
                switch (unit)
                {
                    case "s": return TimeSpan.FromSeconds(value);
                    case "m": return TimeSpan.FromMinutes(value);
                    case "h": return TimeSpan.FromHours(value);
                    case "d": return TimeSpan.FromDays(value);
                }
            }
            throw new ConfigurationException($"Unable to convert '{stringValue}' to {nameof(TimeSpan)}.\n" +
                "Use format : <number><{s|m|h|d}>.\nAvailable units : (s = seconds) (m = minutes) (h = hours) (d = days)");
        }

        /// <summary>
        /// Append value of environment variable ASPNETCORE_ENVIRONMENT to <paramref name="configFileName"/> before extension.
        /// <para>Ex : ASPNETCORE_ENVIRONMENT=local + appsettings.json -> appsettings.local.json</para>
        /// </summary>
        /// <param name="configFileName">File name to build for environment</param>
        /// <returns>The built file name for environment</returns>
        public static string ForEnvironment(this string configFileName)
        {
            var envName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var suffix = string.IsNullOrEmpty(envName) ? string.Empty : $".{envName}";
            var extension = System.IO.Path.GetExtension(configFileName);
            return System.IO.Path.ChangeExtension(configFileName, $"{suffix}{extension}");
        }
    }
}
