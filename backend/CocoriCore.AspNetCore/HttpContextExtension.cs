﻿using CocoriCore.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Net.Http.Headers;
using System.Text;

namespace CocoriCore.AspNetCore
{
    public static class HttpContextExtension
    {
        public const string HTTP_CONTEXT_UNIT_OF_WORK = "HTTP_CONTEXT_UNIT_OF_WORK";

        public static void SetUnitOfWork(this HttpContext httpContext, IUnitOfWork unitOfWork)
        {
            httpContext.Items[HTTP_CONTEXT_UNIT_OF_WORK] = unitOfWork;
        }

        public static IUnitOfWork GetUnitOfWork(this HttpContext httpContext)
        {
            return (IUnitOfWork)httpContext.Items[HTTP_CONTEXT_UNIT_OF_WORK];
        }

        public static ExpandoObject ToExpando(this IHeaderDictionary headerDictionary)
        {
            if (headerDictionary == null)
                return null;
            var expando = new ExpandoObject();
            var headers = expando as IDictionary<string, object>;
            foreach (var header in headerDictionary)
            {
                headers[header.Key] = header.Value.ToString();
            }
            return expando;
        }

        public static ExpandoObject ToExpando(this HttpHeaders headerDictionary)
        {
            if (headerDictionary == null)
                return null;
            var expando = new ExpandoObject();
            var headers = expando as IDictionary<string, object>;
            foreach (var header in headerDictionary)
            {
                headers[header.Key] = string.Join("; ", header.Value);
            }
            return expando;
        }

        public static bool IsOData(this HttpContext context)
        {
            return context.Request.Headers["Accept"].ToString().ToLower().Contains("odata");
        }

        public static string GetJwtPayloadOrNull(this HttpRequest request)
        {
            var authorization = request.Headers["Authorization"].ToString();
            var authorizationSplitted = authorization.Split('.');
            if (authorizationSplitted.Length > 1)
            {
                var payload = authorizationSplitted[1];
                payload = payload.PadRight(4 * ((payload.Length + 3) / 4), '=');
                var binaryPayload = Convert.FromBase64String(payload);
                return Encoding.UTF8.GetString(binaryPayload);
            }
            else
            {
                return null;
            }
        }

        public static string ERROR_ID = nameof(ERROR_ID);

        public static string GetErrorId(this HttpContext context)
        {
            context.Items.TryGetValue(ERROR_ID, out var errorId);
            return $"{errorId}";
        }

        public static void SetErrorId(this HttpContext context, object errorId)
        {
            context.Items[ERROR_ID] = errorId;
        }

        /// <summary>
        ///  Tries to get the header value associated with the specified key in headers.
        /// </summary>
        /// <param name="headers">A header dictionary</param>
        /// <param name="name">The name of the header value to get.</param>
        /// <returns>The <see cref="StringValues"/> for this key or <see cref="StringValues.Empty"/> if no header found for this key.
        /// </returns>
        public static StringValues GetValueOrDefault(this IHeaderDictionary headers, string name)
        {
            if(headers.TryGetValue(name, out var value))
            {
                return value;
            }
            return StringValues.Empty;
        }
    }
}
