﻿using CocoriCore.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace CocoriCore.Repository
{
    public class PersistenceCallDecorator : IRepositoryDecorator
    {
        public int TotalCallCount => LoadCount + QueryCount + InsertCount + UpdateCount + DeleteCount;
        public int LoadCount { get; private set; }
        public int QueryCount { get; private set; }
        public int InsertCount { get; private set; }
        public int UpdateCount { get; private set; }
        public int DeleteCount { get; private set; }

        public virtual async Task<IEnumerable<IEntity>> LoadAsync(Type type, Guid id, LoadIdAsync next)
        {
            var entities = await next(type, id);
            LoadCount++;
            return entities;
        }

        public virtual async Task<IEnumerable<IEntity>> LoadAsync(Type type, IEnumerable<Guid> ids, LoadIdCollectionAsync next)
        {
            var entities = await next(type, ids);
            LoadCount++;
            return entities;
        }

        public virtual async Task<IEnumerable<IEntity>> LoadAsync(Type type, MemberInfo uniqueMember, object value, LoadUniqueAsync next)
        {
            var entities = await next(type, uniqueMember, value);
            LoadCount++;
            return entities;
        }

        public virtual IQueryable<TEntity> Query<TEntity>(Query<TEntity> next) where TEntity : IEntity
        {
            var queryable = next();
            return new ExtendedEnumerable<TEntity>(queryable, () => QueryCount++);
        }

        public virtual async Task InsertAsync(IEntity entity, InsertAsync next)
        {
            await next(entity);
            InsertCount++;
        }

        public virtual async Task UpdateAsync(IEntity entity, UpdateAsync next)
        {
            await next(entity);
            UpdateCount++;
        }

        public virtual async Task DeleteAsync(IEntity entity, DeleteAsync next)
        {
            await next(entity);
            DeleteCount++;
        }

        public virtual Task FlushAsync(FlushAsync next)
        {
            return next();
        }

        public virtual void Reset()
        {
            LoadCount = 0;
            QueryCount = 0;
            InsertCount = 0;
            UpdateCount = 0;
            DeleteCount = 0;
        }
    }
}