﻿using CocoriCore.Common;
using CocoriCore.Expressions.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace CocoriCore.Repository
{
    public class DecorableRepository : IDisposable
    {
        private static MethodInfo _queryMethod = typeof(DecorableRepository)
            .GetMethods()
            .Where(x =>
                x.Name == nameof(Query) &&
                x.GetParameters().Length == 0 &&
                x.GetGenericArguments().Length == 1)
            .Single();

        private readonly IBaseRepository _baseRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly RepositoryOptions _options;
        private Lazy<List<IRepositoryDecorator>> _decorators;

        public DecorableRepository(IBaseRepository baseRepository, IUnitOfWork unitOfWork, IEnumerable<Type> decoratorTypes) :
            this(baseRepository, unitOfWork, new RepositoryOptions().AddDecorators(decoratorTypes))
        {
        }

        public DecorableRepository(IBaseRepository baseRepository, IUnitOfWork unitOfWork, RepositoryOptions options = null)
        {
            _baseRepository = baseRepository;
            _unitOfWork = unitOfWork;
            _decorators = new Lazy<List<IRepositoryDecorator>>(() =>
            {
                var decorators = new List<IRepositoryDecorator>();
                foreach (var decoratorType in _options.DecoratorTypes)
                {
                    decorators.Add((IRepositoryDecorator)_unitOfWork.Resolve(decoratorType));
                }
                decorators.Reverse();
                return decorators;
            });
            _options = options ?? new RepositoryOptions();
        }

        protected DecorableRepository AddDecorator<T>()
        {
            _options.AddDecorator<T>();
            return this;
        }

        public async Task<bool> ExistsAsync(Type type, Guid id)
        {
            var entities = await LoadAsync_(type, id);
            return entities.Any();
        }

        public async Task<bool> ExistsAsync<TEntity>(Guid id) where TEntity : class, IEntity
        {
            var entities = await LoadAsync_(typeof(TEntity), id);
            return entities.Any();
        }

        public Task<bool> ExistsAsync<TEntity>(Expression<Func<TEntity, object>> uniqueMember, object value) where TEntity : class, IEntity
        {
            return ExistsAsync(typeof(TEntity), uniqueMember.GetMemberInfo(), value);
        }

        public async Task<bool> ExistsAsync(Type type, MemberInfo uniqueMember, object value)
        {
            var entities = await LoadAsync_(type, uniqueMember, value);
            return entities.Any();
        }

        public Task<bool> ExistsAsync<TEntity>(IEnumerable<Guid> ids) where TEntity : class, IEntity
        {
            return ExistsAsync(typeof(TEntity), ids);
        }

        public async Task<bool> ExistsAsync(Type type, IEnumerable<Guid> ids)
        {
            ids = ids.Distinct();
            var entities = await LoadAsync_(type, ids);
            return entities.Select(x => x.Id).Distinct().Count() == ids.Count();
        }

        public async Task<TEntity> LoadAsync<TEntity>(Guid id) where TEntity : class, IEntity
        {
            var entity = await LoadAsync(typeof(TEntity), id);
            return (TEntity)entity;
        }

        public async Task<IEntity> LoadAsync(Type type, Guid id)
        {
            var results = await LoadAsync_(type, id);
            if (!results.Any())
            {
                throw new InvalidOperationException($"There is no entity of type {type} with id '{id}'.");
            }
            else if (results.Count() > 1)
            {
                throw new InvalidOperationException($"There are several entities of type {type} with id '{id}', "
                    + $"use {nameof(Query)}() method instead.");
            }
            return results.First();
        }

        private async Task<IEnumerable<IEntity>> LoadAsync_(Type type, Guid id)
        {
            LoadIdAsync loadAsync = (t, i) => _baseRepository.LoadAsync(type, i);
            foreach (var decorator in _decorators.Value)
            {
                var next = loadAsync;
                loadAsync = (t, i) => decorator.LoadAsync(t, i, next);
            }
            return await loadAsync(type, id);
        }

        public async Task<TEntity> LoadAsync<TEntity>(Expression<Func<TEntity, object>> uniqueMember, object value) where TEntity : class, IEntity
        {
            var entity = await LoadAsync(typeof(TEntity), uniqueMember.GetMemberInfo(), value);
            return (TEntity)entity;
        }

        public async Task<IEntity> LoadAsync(Type type, MemberInfo uniqueMember, object value)
        {
            var results = await LoadAsync_(type, uniqueMember, value);
            if (!results.Any())
            {
                throw new InvalidOperationException($"There is no entity of type {type} with value '{value}' "
                    + $"for unique field {uniqueMember.Name}.");
            }
            else if (results.Count() > 1)
            {
                throw new InvalidOperationException($"There are several entities of type {type} with value '{value}' "
                    + $"for unique field {uniqueMember.Name}, "
                    + $"use {nameof(Query)}() method instead.");
            }
            return results.First();
        }

        private async Task<IEnumerable<IEntity>> LoadAsync_(Type type, MemberInfo uniqueMember, object value)
        {
            LoadUniqueAsync loadUniqueAsync = (t, m, v) => _baseRepository.LoadAsync(t, m, v);
            foreach (var decorator in _decorators.Value)
            {
                var next = loadUniqueAsync;
                loadUniqueAsync = (t, m, v) => decorator.LoadAsync(t, m, v, next);
            }
            return await loadUniqueAsync(type, uniqueMember, value);
        }

        public async Task<IEnumerable<TEntity>> LoadAsync<TEntity>(IEnumerable<Guid> ids) where TEntity : class, IEntity
        {
            var entities = await LoadAsync(typeof(TEntity), ids);
            return entities.Cast<TEntity>();
        }

        public async Task<IEnumerable<IEntity>> LoadAsync(Type type, IEnumerable<Guid> ids)
        {
            ids = ids.Distinct();
            var entities = await LoadAsync_(type, ids);
            if (entities.Select(x => x.Id).Distinct().Count() != ids.Count())//TODO a tester
            {
                throw new InvalidOperationException($"Could not find an entity of type '{type}'for each provided id :\n"
                    + $"{string.Join("\t\n", ids)}");
            }
            return entities;
        }

        private async Task<IEnumerable<IEntity>> LoadAsync_(Type type, IEnumerable<Guid> ids)
        {
            LoadIdCollectionAsync loadManyAsync = (t, c) => _baseRepository.LoadAsync(type, c);
            foreach (var decorator in _decorators.Value)
            {
                var next = loadManyAsync;
                loadManyAsync = (t, c) => decorator.LoadAsync(t, c, next);
            }
            var entities = await loadManyAsync(type, ids);
            return entities;
        }

        public IQueryable<TEntity> Query<TEntity>() where TEntity : class, IEntity
        {
            Query<TEntity> query = () => _baseRepository.Query<TEntity>();
            foreach (var decorator in _decorators.Value)
            {
                var next = query;
                query = () => decorator.Query(next);
            }
            return query();
        }

        public IQueryable Query(Type entityType)
        {
            return (IQueryable)_queryMethod
                .MakeGenericMethod(entityType)
                .Invoke(this, new object[0]);
        }

        public async Task InsertAsync<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            InsertAsync insertAsync = e => _baseRepository.InsertAsync(entity);
            foreach (var decorator in _decorators.Value)
            {
                var next = insertAsync;
                insertAsync = e => decorator.InsertAsync(e, next);
            }
            await insertAsync(entity);
        }

        public async Task UpdateAsync<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            UpdateAsync updateAsync = e => _baseRepository.UpdateAsync(entity);
            foreach (var decorator in _decorators.Value)
            {
                var next = updateAsync;
                updateAsync = e => decorator.UpdateAsync(e, next);
            }
            await updateAsync(entity);
        }

        public async Task DeleteAsync<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            DeleteAsync deleteAsync = e => _baseRepository.DeleteAsync(entity);
            foreach (var decorator in _decorators.Value)
            {
                var next = deleteAsync;
                deleteAsync = e => decorator.DeleteAsync(e, next);
            }
            await deleteAsync(entity);
        }

        public async Task FlushAsync()
        {
            FlushAsync flushAsync = () => _baseRepository.FlushAsync();
            foreach (var decorator in _decorators.Value)
            {
                var next = flushAsync;
                flushAsync = () => decorator.FlushAsync(next);
            }
            await flushAsync();
        }

        public void Dispose()
        {
            _baseRepository.Dispose();
        }
    }
}
