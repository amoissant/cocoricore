﻿using CocoriCore.Common;
using CocoriCore.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace CocoriCore.Linq.Async
{
    public static class AsyncQueryableExtensions
    {
        public static async Task<List<T>> ToListAsync<T>(this IQueryable<T> queryable, CancellationToken cancellationToken = default)
        {
            var queryProvider = GetQueryProvider(queryable);
            IAsyncEnumerator<T> enumerator = null;
            var result = new List<T>();
            try
            {
                var enumerable = queryProvider.AsAsyncEnumerable<T>(queryable.Expression);
                enumerator = enumerable.GetAsyncEnumerator(cancellationToken);
                while (await enumerator.MoveNextAsync())
                {
                    result.Add(enumerator.Current);
                }
            }
            finally
            {
                if (enumerator != null)
                {
                    await enumerator.DisposeAsync();
                }
            }
            return result;
        }

        public static async Task<T[]> ToArrayAsync<T>(this IQueryable<T> queryable, CancellationToken cancellationToken = default)
        {
            return (await queryable.ToListAsync(cancellationToken)).ToArray();
        }

        public static async Task<Dictionary<TKey, TElement>> ToDictionaryAsync<T, TKey, TElement>(this IQueryable<T> queryable,
            Func<T, TKey> keySelector, Func<T, TElement> elementSelector, CancellationToken cancellationToken = default)
        {
            var queryProvider = GetQueryProvider(queryable);
            var enumerable = queryProvider.AsAsyncEnumerable<T>(queryable.Expression);
            var enumerator = enumerable.GetAsyncEnumerator(cancellationToken);
            var result = new List<T>();
            while (await enumerator.MoveNextAsync())
            {
                result.Add(enumerator.Current);
            }
            if (enumerator is IAsyncDisposable asyncDisposable)
            {
                await asyncDisposable.DisposeAsync();
            }
            return result.ToDictionary(keySelector, elementSelector);
        }

        public static async Task<bool> AnyAsync<T>(this IQueryable<T> queryable, CancellationToken cancellationToken = default)
        {
            var queryProvider = GetQueryProvider(queryable);
            var anyMethod = QueryableMethods.Any.MakeGenericMethod(typeof(T));
            var expression = Expression.Call(null, anyMethod, new[] { queryable.Expression });
            return await queryProvider.ExecuteAsync<bool>(expression, cancellationToken);
        }

        public static async Task<int> CountAsync<T>(this IQueryable<T> queryable, CancellationToken cancellationToken = default)
        {
            var queryProvider = GetQueryProvider(queryable);
            var countMethod = QueryableMethods.Count.MakeGenericMethod(typeof(T));
            var expression = Expression.Call(null, countMethod, new[] { queryable.Expression });
            return await queryProvider.ExecuteAsync<int>(expression, cancellationToken);
        }

        public static async Task<T> FirstAsync<T>(this IQueryable<T> queryable, CancellationToken cancellationToken = default)
        {
            var queryProvider = GetQueryProvider(queryable);
            var firstMethod = QueryableMethods.First.MakeGenericMethod(typeof(T));
            var expression = Expression.Call(null, firstMethod, new[] { queryable.Expression });
            return await queryProvider.ExecuteAsync<T>(expression, cancellationToken);
        }

        public static async Task<T> FirstOrDefaultAsync<T>(this IQueryable<T> queryable, CancellationToken cancellationToken = default)
        {
            var queryProvider = GetQueryProvider(queryable);
            var firstOrDefaultMethod = QueryableMethods.FirstOrDefault.MakeGenericMethod(typeof(T));
            var expression = Expression.Call(null, firstOrDefaultMethod, new[] { queryable.Expression });
            return await queryProvider.ExecuteAsync<T>(expression, cancellationToken);
        }

        public static async Task<T> SingleAsync<T>(this IQueryable<T> queryable, CancellationToken cancellationToken = default)
        {
            var queryProvider = GetQueryProvider(queryable);
            var singleMethod = QueryableMethods.Single.MakeGenericMethod(typeof(T));
            var expression = Expression.Call(null, singleMethod, new[] { queryable.Expression });
            return await queryProvider.ExecuteAsync<T>(expression, cancellationToken);
        }

        public static async Task<T> SingleOrDefaultAsync<T>(this IQueryable<T> queryable, CancellationToken cancellationToken = default)
        {
            var queryProvider = GetQueryProvider(queryable);
            var singleOrDefaultMethod = QueryableMethods.SingleOrDefault.MakeGenericMethod(typeof(T));
            var expression = Expression.Call(null, singleOrDefaultMethod, new[] { queryable.Expression });
            return await queryProvider.ExecuteAsync<T>(expression, cancellationToken);
        }

        public static async Task<int> SumAsync(this IQueryable<int> queryable, CancellationToken cancellationToken = default)
        {
            var queryProvider = GetQueryProvider(queryable);
            var sumMethod = QueryableMethods.SumInt;
            var expression = Expression.Call(null, sumMethod, new[] { queryable.Expression });
            return await queryProvider.ExecuteAsync<int>(expression, cancellationToken);
        }

        public static async Task<int?> SumAsync(this IQueryable<int?> queryable, CancellationToken cancellationToken = default)
        {
            var queryProvider = GetQueryProvider(queryable);
            var sumMethod = QueryableMethods.SumNullableInt;
            var expression = Expression.Call(null, sumMethod, new[] { queryable.Expression });
            return await queryProvider.ExecuteAsync<int?>(expression, cancellationToken);
        }

        public static async Task<long> SumAsync(this IQueryable<long> queryable, CancellationToken cancellationToken = default)
        {
            var queryProvider = GetQueryProvider(queryable);
            var sumMethod = QueryableMethods.SumLong;
            var expression = Expression.Call(null, sumMethod, new[] { queryable.Expression });
            return await queryProvider.ExecuteAsync<long>(expression, cancellationToken);
        }

        public static async Task<long?> SumAsync(this IQueryable<long?> queryable, CancellationToken cancellationToken = default)
        {
            var queryProvider = GetQueryProvider(queryable);
            var sumMethod = QueryableMethods.SumNullableLong;
            var expression = Expression.Call(null, sumMethod, new[] { queryable.Expression });
            return await queryProvider.ExecuteAsync<long?>(expression, cancellationToken);
        }

        public static async Task<float> SumAsync(this IQueryable<float> queryable, CancellationToken cancellationToken = default)
        {
            var queryProvider = GetQueryProvider(queryable);
            var sumMethod = QueryableMethods.SumFloat;
            var expression = Expression.Call(null, sumMethod, new[] { queryable.Expression });
            return await queryProvider.ExecuteAsync<float>(expression, cancellationToken);
        }

        public static async Task<float?> SumAsync(this IQueryable<float?> queryable, CancellationToken cancellationToken = default)
        {
            var queryProvider = GetQueryProvider(queryable);
            var sumMethod = QueryableMethods.SumNullableFloat;
            var expression = Expression.Call(null, sumMethod, new[] { queryable.Expression });
            return await queryProvider.ExecuteAsync<float?>(expression, cancellationToken);
        }

        public static async Task<double> SumAsync(this IQueryable<double> queryable, CancellationToken cancellationToken = default)
        {
            var queryProvider = GetQueryProvider(queryable);
            var sumMethod = QueryableMethods.SumDouble;
            var expression = Expression.Call(null, sumMethod, new[] { queryable.Expression });
            return await queryProvider.ExecuteAsync<double>(expression, cancellationToken);
        }

        public static async Task<double?> SumAsync(this IQueryable<double?> queryable, CancellationToken cancellationToken = default)
        {
            var queryProvider = GetQueryProvider(queryable);
            var sumMethod = QueryableMethods.SumNullableDouble;
            var expression = Expression.Call(null, sumMethod, new[] { queryable.Expression });
            return await queryProvider.ExecuteAsync<double?>(expression, cancellationToken);
        }

        public static async Task<decimal> SumAsync(this IQueryable<decimal> queryable, CancellationToken cancellationToken = default)
        {
            var queryProvider = GetQueryProvider(queryable);
            var sumMethod = QueryableMethods.SumDecimal;
            var expression = Expression.Call(null, sumMethod, new[] { queryable.Expression });
            return await queryProvider.ExecuteAsync<decimal>(expression, cancellationToken);
        }

        public static async Task<decimal?> SumAsync(this IQueryable<decimal?> queryable, CancellationToken cancellationToken = default)
        {
            var queryProvider = GetQueryProvider(queryable);
            var sumMethod = QueryableMethods.SumNullableDecimal;
            var expression = Expression.Call(null, sumMethod, new[] { queryable.Expression });
            return await queryProvider.ExecuteAsync<decimal?>(expression, cancellationToken);
        }

        public static async Task<T> MinAsync<T>(this IQueryable<T> queryable, CancellationToken cancellationToken = default)
        {
            var queryProvider = GetQueryProvider(queryable);
            var minMethod = QueryableMethods.Min.MakeGenericMethod(typeof(T));
            var expression = Expression.Call(null, minMethod, new[] { queryable.Expression });
            return await queryProvider.ExecuteAsync<T>(expression, cancellationToken);
        }

        public static async Task<T> MaxAsync<T>(this IQueryable<T> queryable, CancellationToken cancellationToken = default)
        {
            var queryProvider = GetQueryProvider(queryable);
            var maxMethod = QueryableMethods.Max.MakeGenericMethod(typeof(T));
            var expression = Expression.Call(null, maxMethod, new[] { queryable.Expression });
            return await queryProvider.ExecuteAsync<T>(expression, cancellationToken);
        }

        private static ICocoriCoreQueryProvider GetQueryProvider(IQueryable queryable)
        {
            var queryProvider = queryable as ICocoriCoreQueryProvider;
            if (queryProvider == null)
            {
                throw new InvalidOperationException($"{typeof(AsyncQueryableExtensions)} methods are only compatible with queryable implementing {nameof(ICocoriCoreQueryProvider)}.");
            }
            return queryProvider;
        }
    }
}