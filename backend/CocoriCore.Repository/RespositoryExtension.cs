﻿using CocoriCore.Common;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocoriCore.Repository
{
    public static class RespositoryExtension
    {
        public static async Task InsertManyAsync<TEntity>(this IRepository repository, params TEntity[] entities)
            where TEntity : class, IEntity
        {
            await repository.InsertManyAsync<IEntity>(entities.AsEnumerable());
        }

        public static async Task InsertManyAsync(this IRepository repository, params IEntity[] entities)
        {
            await repository.InsertManyAsync(entities.AsEnumerable());
        }

        public static async Task InsertManyAsync<TEntity>(this IRepository repository, IEnumerable<TEntity> entities)
             where TEntity : class, IEntity
        {
            foreach (var entity in entities)
            {
                await repository.InsertAsync(entity);
            }
        }

        public static async Task UpdateManyAsync<TEntity>(this IRepository repository, params TEntity[] entities)
            where TEntity : class, IEntity
        {
            await repository.UpdateManyAsync(entities.AsEnumerable());
        }

        public static async Task UpdateManyAsync(this IRepository repository, params IEntity[] entities)
        {
            await repository.UpdateManyAsync(entities.AsEnumerable());
        }

        public static async Task UpdateManyAsync<TEntity>(this IRepository repository, IEnumerable<TEntity> entities)
             where TEntity : class, IEntity
        {
            foreach (var entity in entities)
            {
                await repository.UpdateAsync(entity);
            }
        }

        public static async Task DeleteManyAsync<TEntity>(this IRepository repository, params TEntity[] entities)
            where TEntity : class, IEntity
        {
            await repository.DeleteManyAsync(entities.AsEnumerable());
        }

        public static async Task DeleteManyAsync(this IRepository repository, params IEntity[] entities)
        {
            await repository.DeleteManyAsync(entities.AsEnumerable());
        }

        public static async Task DeleteManyAsync<TEntity>(this IRepository repository, IEnumerable<TEntity> entities)
             where TEntity : class, IEntity
        {
            foreach (var entity in entities)
            {
                await repository.DeleteAsync(entity);
            }
        }
    }
}
