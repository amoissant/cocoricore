using CocoriCore.Reflection.Extension;
using CocoriCore.Types.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace CocoriCore.Repository
{
    public class State<T> : IState<T>, IState
    {
        private IState _state;

        public State(IState state)
        {
            _state = state;
        }

        public virtual T PreviousObject => (T)_state.PreviousObject;
        public virtual T CurrentObject => (T)_state.CurrentObject;
        public virtual bool HasChanged => _state.HasChanged;
        public virtual IEnumerable<MemberInfo> ModifiedMembers => _state.ModifiedMembers;

        object IState.PreviousObject => _state.PreviousObject;

        object IState.CurrentObject => _state.CurrentObject;

        public IState<TCast> Cast<TCast>() where TCast : class
        {
            return _state.Cast<TCast>();
        }

        public virtual TProperty PreviousValue<TProperty>(Expression<Func<T, TProperty>> propertyExpression)
        {
            return _state.PreviousValue(propertyExpression);
        }

        public TProperty PreviousValue<T1, TProperty>(Expression<Func<T1, TProperty>> propertyExpression)
        {
            return _state.PreviousValue(propertyExpression);
        }
    }

    public class State : IState
    {
        protected object _previousObject;
        protected object _currentObject;
        protected IEnumerable<MemberInfo> _modifiedMembers;
        protected bool? _hasChanged;

        public State(object previousObject, object currentObject)
        {
            _previousObject = previousObject;
            _currentObject = currentObject;
        }

        public virtual object PreviousObject => _previousObject;

        public virtual object CurrentObject => _currentObject;

        public virtual bool HasChanged => ModifiedMembers.Count() > 0;

        public virtual IEnumerable<MemberInfo> ModifiedMembers
        {
            get
            {
                if (_modifiedMembers == null)
                {
                    var modifiedMembers = new List<MemberInfo>();
                    foreach (var member in CurrentObject.GetType().GetPropertiesAndFields())
                    {
                        //TODO les égalités sont toujours fausses pour un tableau par exemple.
                        //utiliser Utf8Json pour les types non primitifs afin de déterminer si égalité ou non ?
                        if (!Equals(member.InvokeGetter(CurrentObject), member.InvokeGetter(PreviousObject)))
                        {
                            modifiedMembers.Add(member);
                        }
                    }
                    _modifiedMembers = modifiedMembers.ToArray();
                }
                return _modifiedMembers;
            }
        }

        public virtual IState<TCast> Cast<TCast>()
            where TCast : class
        {
            return new State<TCast>(this);
        }

        public virtual TProperty PreviousValue<T, TProperty>(Expression<Func<T, TProperty>> propertyExpression)
        {
            return propertyExpression.Compile()((T)PreviousObject);
        }
    }
}