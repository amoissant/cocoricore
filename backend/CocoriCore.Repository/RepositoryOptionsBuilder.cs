﻿using System;
using System.Collections.Generic;

namespace CocoriCore.Repository
{
    public class RepositoryOptionsBuilder
    {
        private RepositoryOptions _options;

        public virtual RepositoryOptions Options
        {
            get
            {
                return _options;
            }
        }

        public RepositoryOptionsBuilder()
        {
            _options = new RepositoryOptions();
        }

        public RepositoryOptionsBuilder AddDecorator<T>()
        {
            _options.AddDecorator<T>();
            return this;
        }

        public RepositoryOptionsBuilder AddDecorators(params Type[] types)
        {
            _options.AddDecorators(types);
            return this;
        }

        public RepositoryOptionsBuilder AddDecorators(IEnumerable<Type> types)
        {
            _options.AddDecorators(types);
            return this;
        }
    }
}
