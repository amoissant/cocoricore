﻿using CocoriCore.Types.Extension;
using System.Linq;
using System.Linq.Expressions;

namespace CocoriCore.Repository
{
    public class InterfaceVisitor : ExpressionVisitor
    {
        private IQueryable _queryable;

        public InterfaceVisitor(IQueryable queryable)
        {
            _queryable = queryable;
        }

        protected override Expression VisitConstant(ConstantExpression node)
        {
            var constantType = node?.Value?.GetType();
            if (constantType != null && constantType.IsAssignableTo<ICocoriCoreQueryProvider>())
            {
                return _queryable.Expression;
            }
            else
            {
                return base.VisitConstant(node);
            }
        }
    }
}
