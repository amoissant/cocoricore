using CocoriCore.Common;
using CocoriCore.Expressions.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace CocoriCore.Repository
{
    public class InMemoryBaseRepository : IBaseRepository
    {
        private static MethodInfo _queryMethod = typeof(InMemoryBaseRepository)
            .GetMethods()
            .Where(x =>
                x.Name == nameof(Query) &&
                x.GetParameters().Length == 0 &&
                x.GetGenericArguments().Length == 1)
            .Single();

        private IInMemoryEntityStore _entityStore;

        public InMemoryBaseRepository(IInMemoryEntityStore entityStore)
        {
            _entityStore = entityStore;
        }

        public virtual Task<IEnumerable<IEntity>> LoadAsync(Type entityType, MemberInfo uniqueMember, object value)
        {
            if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }
            var parameter = Expression.Parameter(entityType, "x");
            var predicate = Expression.MakeMemberAccess(parameter, uniqueMember).Equal(value);
            var loadedEntities = _entityStore
                .Query(entityType)
                .Where(predicate, parameter)
                .Cast<IEntity>();
            return Task.FromResult(loadedEntities.AsEnumerable());
        }

        public virtual Task<IEnumerable<IEntity>> LoadAsync(Type type, Guid id)
        {
            return Task.FromResult(_entityStore.Get(type, id));
        }

        public virtual Task<IEnumerable<IEntity>> LoadAsync(Type type, IEnumerable<Guid> ids)
        {
            var result = ids.SelectMany(x => _entityStore.Get(type, x)).Cast<IEntity>().ToArray();
            return Task.FromResult(result.AsEnumerable());
        }

        public virtual IQueryable<TEntity> Query<TEntity>()
            where TEntity : class, IEntity
        {
            var queryable = _entityStore.Query(typeof(TEntity)).Cast<TEntity>();
            return new CollectionAsyncQueryProvider<TEntity>(queryable);
        }

        public Task InsertAsync(IEntity entity)
        {
            if (entity.Id == Guid.Empty)
                entity.Id = Guid.NewGuid();//TODO rendre paramétrable ?
            _entityStore.Add(entity);
            return Task.CompletedTask;
        }

        public Task UpdateAsync(IEntity entity)
        {
            _entityStore.Set(entity);
            return Task.CompletedTask;
        }

        public Task DeleteAsync(IEntity entity)
        {
            _entityStore.Remove(entity);
            return Task.CompletedTask;
        }

        public Task FlushAsync()
        {
            return Task.CompletedTask;
        }

        public void Dispose()
        {
        }
    }
}