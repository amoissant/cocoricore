using CocoriCore.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace CocoriCore.Repository
{
    public interface IInMemoryEntityStore
    {
        void AddUniqueConstraint<TEntity>(Expression<Func<TEntity, dynamic>> member);
        void Add(IEntity entity);

        void Set(IEntity entity);

        bool Contains(Type entityType, Guid entityId);

        void Remove(IEntity entity);

        IQueryable Query(Type entityType);

        IEnumerable<IEntity> Get(Type entityType, Guid entityId);

        void CheckUniqueConstraints(Type entityType);
    }
}