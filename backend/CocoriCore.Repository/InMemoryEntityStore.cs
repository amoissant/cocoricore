using CocoriCore.Collection.Extensions;
using CocoriCore.Common;
using CocoriCore.Expressions.Extension;
using CocoriCore.Types.Extension;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace CocoriCore.Repository
{
    public class InMemoryEntityStore : IInMemoryEntityStore, IPersistentStore
    {
        private ConcurrentDictionary<Type, List<EntityReference>> _typeHierarchyDictionary;
        private ConcurrentDictionary<(Type type, Guid id), EntityReference> _keyDictionary;
        private ConcurrentDictionary<Type, MemberInfo> _uniqueConstraints;

        public InMemoryEntityStore()
        {
            _typeHierarchyDictionary = new ConcurrentDictionary<Type, List<EntityReference>>();
            _keyDictionary = new ConcurrentDictionary<(Type type, Guid id), EntityReference>();
            _uniqueConstraints = new ConcurrentDictionary<Type, MemberInfo>();
        }

        public virtual void AddUniqueConstraint<TEntity>(Expression<Func<TEntity, dynamic>> member)
        {
            if (!_uniqueConstraints.ContainsKey(typeof(TEntity)))
            {
                _uniqueConstraints[typeof(TEntity)] = member.GetMemberInfo();
            }
        }

        public virtual void Add(IEntity entity)
        {
            var key = (entity.GetType(), entity.Id);
            if (!_keyDictionary.ContainsKey(key))
            {
                var entityReference = new EntityReference(entity, key);
                _keyDictionary[key] = entityReference;

                var entityTypeHierarchy = entity.GetTypeHierarchy();
                foreach (Type currentType in entityTypeHierarchy)
                {
                    lock (_typeHierarchyDictionary)
                    {
                        if (!_typeHierarchyDictionary.ContainsKey(currentType))
                        {
                            _typeHierarchyDictionary[currentType] = new List<EntityReference>();
                        }
                    }
                    _typeHierarchyDictionary[currentType].Add(entityReference);
                }
            }
            else
            {
                throw new InvalidOperationException($"There is already an entity of type {entity.GetType()} with id {entity.Id}");
            }
        }

        public virtual void Set(IEntity entity)
        {
            var key = (entity.GetType(), entity.Id);
            if (_keyDictionary.ContainsKey(key))
            {
                _keyDictionary[key].Entity = entity;
            }
            else
            {
                throw new InvalidOperationException($"There is no entity of type {entity.GetType()} with id {entity.Id}");
            }
        }

        public virtual bool Contains(Type entityType, Guid entityId)
        {
            var key = (entityType, entityId);
            if (_keyDictionary.ContainsKey(key))
            {
                return true;
            }
            else
            {
                return _typeHierarchyDictionary.ContainsKey(entityType) &&
                    _typeHierarchyDictionary[entityType].Any(x => Equals(x.Entity.Id, entityId));
            }
        }

        public virtual void Remove(IEntity entity)
        {
            var entityType = entity.GetType();
            var key = (entityType, entity.Id);
            if (_keyDictionary.ContainsKey(key))
            {
                EntityReference entityReference = null;
                _keyDictionary.TryRemove(key, out entityReference);
                var entityTypeHierarchy = entityType.GetTypeHierarchy();
                foreach (Type currentType in entityTypeHierarchy)
                {
                    if (_typeHierarchyDictionary.ContainsKey(currentType))
                    {
                        var keysToRemove = _typeHierarchyDictionary[currentType].RemoveAll(x => Equals(x.Key, key));
                    }
                }
            }
            else
            {
                throw new InvalidOperationException($"There is no entity of type {entityType} with id {key.Id}");
            }
        }

        public virtual IQueryable Query(Type entityType)
        {
            if (_typeHierarchyDictionary.ContainsKey(entityType))
            {
                return _typeHierarchyDictionary[entityType]
                    .Select(x => x.Entity)
                    .Cast(entityType)
                    .AsQueryable();
            }
            else
            {
                var emptyArray = (Array)Activator.CreateInstance(entityType.MakeArrayType(), new object[] { 0 });
                return emptyArray.AsQueryable();
            }
        }

        public virtual IEnumerable<IEntity> Get(Type entityType, Guid entityId)
        {
            var key = (entityType, entityId);
            if (_keyDictionary.ContainsKey(key))
            {
                return new IEntity[] { _keyDictionary[key].Entity };
            }
            else if (_typeHierarchyDictionary.ContainsKey(entityType))
            {
                return _typeHierarchyDictionary[entityType]
                    .Where(x => Equals(x.Entity.Id, entityId))
                    .Select(x => x.Entity);
            }
            else
            {
                return new IEntity[0];
            }
        }

        public virtual Task ClearAsync()
        {
            _typeHierarchyDictionary.Clear();
            _keyDictionary.Clear();
            return Task.CompletedTask;
        }

        public virtual void CheckUniqueConstraints(Type entityType)
        {
            var entityTypeHierarchy = entityType.GetTypeHierarchy();
            foreach (Type currentType in entityTypeHierarchy)
            {
                if (_uniqueConstraints.ContainsKey(currentType))
                {
                    var memberInfo = _uniqueConstraints[currentType];
                    var entities = Query(entityType).Cast<IEntity>().ToArray();
                    if (!entities.IsUnique(memberInfo))
                    {
                        throw new UniqueConstraintException(memberInfo);
                    }
                }
            }
        }

        class EntityReference
        {
            public EntityReference(IEntity entity, (Type type, Guid id) key)
            {
                Entity = entity;
                Key = key;
            }

            public IEntity Entity { get; set; }

            public (Type type, Guid id) Key { get; }
        }
    }
}