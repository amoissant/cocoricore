using CocoriCore.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace CocoriCore.Repository
{
    public class TransactionalInMemoryBaseRepository : IBaseRepository, ITransactionHolder 
    {
        private static MethodInfo _queryMethod = typeof(TransactionalInMemoryBaseRepository)
            .GetMethods()
            .Where(x =>
                x.Name == nameof(Query) &&
                x.GetParameters().Length == 0 &&
                x.GetGenericArguments().Length == 1)
            .Single();

        private InMemoryBaseRepository _repository;
        private TransactionDecorator _decorator;

        public TransactionalInMemoryBaseRepository(IInMemoryEntityStore entityStore, ICopier copier)
        {
            _repository = new InMemoryBaseRepository(entityStore);
            _decorator = new TransactionDecorator(_repository, copier);
        }

        public virtual Task<IEnumerable<IEntity>> LoadAsync(Type type, Guid id)
        {
            return _decorator.LoadAsync(type, id);
        }

        public virtual Task<IEnumerable<IEntity>> LoadAsync(Type type, MemberInfo uniqueMember, object value)
        {
            return _decorator.LoadAsync(type, uniqueMember, value);
        }

        public virtual Task<IEnumerable<IEntity>> LoadAsync(Type type, IEnumerable<Guid> ids)
        {
            return _decorator.LoadAsync(type, ids);
        }

        public virtual IQueryable<TEntity> Query<TEntity>()
            where TEntity : class, IEntity
        {
            return _decorator.Query<TEntity>();
        }

        public virtual Task InsertAsync(IEntity entity)
        {
            return _decorator.InsertAsync(entity);
        }

        public virtual Task UpdateAsync(IEntity entity)
        {
            return _decorator.UpdateAsync(entity);
        }

        public virtual Task DeleteAsync(IEntity entity)
        {
            return _decorator.DeleteAsync(entity);
        }

        public virtual Task CommitAsync()
        {
            return _decorator.CommitAsync();
        }

        public virtual Task RollbackAsync()
        {
            return _decorator.RollbackAsync();
        }

        public virtual void Dispose()
        {
            _decorator.Dispose();
        }

        public Task FlushAsync()
        {
            return _decorator.FlushAsync();
        }
    }
}