﻿using CocoriCore.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace CocoriCore.Repository
{
    public interface IReadRepository : IDisposable
    {
        Task<bool> ExistsAsync<TEntity>(Guid id) where TEntity : class, IEntity;

        Task<bool> ExistsAsync<TEntity>(IEnumerable<Guid> ids) where TEntity : class, IEntity;

        Task<bool> ExistsAsync<TEntity>(Expression<Func<TEntity, object>> uniqueMember, object value) where TEntity : class, IEntity;

        Task<TEntity> LoadAsync<TEntity>(Guid id) where TEntity : class, IEntity;

        Task<TEntity> LoadAsync<TEntity>(Expression<Func<TEntity, object>> uniqueMember, object value) where TEntity : class, IEntity;

        Task<IEntity> LoadAsync(Type type, MemberInfo uniqueMember, object value);

        Task<IEntity> LoadAsync(Type type, Guid id);

        Task<IEnumerable<IEntity>> LoadAsync(Type type, IEnumerable<Guid> ids);

        Task<IEnumerable<TEntity>> LoadAsync<TEntity>(IEnumerable<Guid> ids) where TEntity : class, IEntity;

        IQueryable<TEntity> Query<TEntity>() where TEntity : class, IEntity;

        IQueryable Query(Type entityType);
    }
}