﻿using CocoriCore.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace CocoriCore.Repository
{
    public interface IBaseRepository : IDisposable
    {
        Task<IEnumerable<IEntity>> LoadAsync(Type type, Guid id);
        
        Task<IEnumerable<IEntity>> LoadAsync(Type type, IEnumerable<Guid> ids);
        
        Task<IEnumerable<IEntity>> LoadAsync(Type type, MemberInfo uniqueMember, object value);

        IQueryable<TEntity> Query<TEntity>() where TEntity : class, IEntity;
        
        Task InsertAsync(IEntity entity);
        
        Task UpdateAsync(IEntity entity);
        
        Task DeleteAsync(IEntity entity);

        Task FlushAsync();
    }
}