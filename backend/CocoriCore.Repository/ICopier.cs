﻿namespace CocoriCore.Repository
{
    public interface ICopier
    {
        object CreateCopy(object obj);
        IState CreateState(object obj);
    }
}