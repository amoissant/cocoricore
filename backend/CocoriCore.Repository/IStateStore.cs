﻿using System.Collections.Generic;

namespace CocoriCore.Repository
{
    public interface IStateStore
    {
        void Clear();
        IEnumerator<IState> GetEnumerator();
        IState GetState(object obj);
        IState<T> GetState<T>(T obj) where T : class;
        bool HasState(object obj);
        T SaveState<T>(T obj);
        bool TryGetState(object obj, out IState state);
    }
}