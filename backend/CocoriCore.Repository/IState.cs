using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;

namespace CocoriCore.Repository
{
    public interface IState
    {
        IState<TCast> Cast<TCast>() where TCast : class;
        object PreviousObject { get; }
        object CurrentObject { get; }
        bool HasChanged { get; }
        TProperty PreviousValue<T, TProperty>(Expression<Func<T, TProperty>> propertyExpression);
        IEnumerable<MemberInfo> ModifiedMembers { get; }
    }

    public interface IState<T> : IState
    {
        new T PreviousObject { get; }
        new T CurrentObject { get; }
        TProperty PreviousValue<TProperty>(Expression<Func<T, TProperty>> propertyExpression);
    }
}