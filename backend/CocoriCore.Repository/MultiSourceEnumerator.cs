﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CocoriCore.Repository
{
    public class MultiSourceEnumerator<T> : IEnumerator<T>, IAsyncEnumerator<T>
    {
        private readonly IEnumerator<T>[] _enumerators;
        private int _index;

        public MultiSourceEnumerator(IEnumerator<T>[] enumerators)
        {
            _enumerators = enumerators ?? throw new ArgumentNullException();
        }

        public T Current => _enumerators[_index].Current;

        object IEnumerator.Current => Current;



        public bool MoveNext()
        {
            var enumerator = _enumerators[_index];
            bool success = enumerator.MoveNext();
            if (!success)
            {
                if (_index == _enumerators.Length - 1)
                {
                    return false;
                }
                else
                {
                    enumerator.Dispose();
                    _index++;
                    return MoveNext();
                }
            }
            return success;
        }

        public async ValueTask<bool> MoveNextAsync()
        {
            bool success = false;
            var enumerator = _enumerators[_index];
            if (enumerator is IAsyncEnumerator<T> asyncEnumerator)
            {
                success = await asyncEnumerator.MoveNextAsync();
            }
            else
            {
                success = enumerator.MoveNext();
            }
            if (!success)
            {
                if (_index == _enumerators.Length - 1)
                {
                    return false;
                }
                else
                {
                    await DisposeAsync(enumerator);
                    _index++;
                    return await MoveNextAsync();
                }
            }
            return success;
        }

        public void Reset()
        {
            _index = 0;
            for (var i = 0; i < _enumerators.Length; i++)
            {
                _enumerators[i].Reset();
            }
        }

        public void Dispose()
        {
            foreach (var enumerator in _enumerators)
            {
                enumerator.Dispose();
            }
        }

        public async ValueTask DisposeAsync()
        {
            foreach (var enumerator in _enumerators)
            {
                await DisposeAsync(enumerator);
            }
        }

        private async Task DisposeAsync(IEnumerator<T> enumerator)
        {
            if (enumerator is IAsyncEnumerator<T> asyncEnumerator)
            {
                await asyncEnumerator.DisposeAsync();
            }
            else
            {
                enumerator.Dispose();
            }
        }
    }
}
