using CocoriCore.Common;
using System.Threading.Tasks;

namespace CocoriCore.Repository
{
    public interface IRepository : IReadRepository
    {
        Task InsertAsync<TEntity>(TEntity entity) where TEntity : class, IEntity;

        Task UpdateAsync<TEntity>(TEntity entity) where TEntity : class, IEntity;

        Task DeleteAsync<TEntity>(TEntity entity) where TEntity : class, IEntity;

        Task FlushAsync();
    }
}