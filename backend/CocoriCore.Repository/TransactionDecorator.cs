using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using CocoriCore.Common;
using CocoriCore.Collection.Extensions;

namespace CocoriCore.Repository
{
    public class TransactionDecorator : IBaseRepository
    {
        private readonly IBaseRepository _repository;
        private readonly ICopier _copier;
        private readonly List<(CUDOperation operation, IEntity entity)> _transactionOperations;
        private readonly List<(CUDOperation operation, IEntity entity)> _flushOperations;
        private Dictionary<IEntity, IEntity> _copyEntities;
        private Dictionary<IEntity, IEntity> _originalEntities;
        private HashSet<IEntity> _knownReferences;

        public TransactionDecorator(IBaseRepository repository, ICopier copier)
        {
            _repository = repository;
            _transactionOperations = new List<(CUDOperation, IEntity)>();
            _flushOperations = new List<(CUDOperation, IEntity)>();
            _copyEntities = new Dictionary<IEntity, IEntity>();
            _originalEntities = new Dictionary<IEntity, IEntity>();
            _knownReferences = new HashSet<IEntity>();
            _copier = copier;
        }

        public Task InsertAsync(IEntity entity)
        {
            if (!_knownReferences.Contains(entity))
            {
                _knownReferences.Add(entity);
            }
            _flushOperations.Add((CUDOperation.CREATE, entity));
            _transactionOperations.Add((CUDOperation.CREATE, entity));
            return Task.CompletedTask;
        }

        public Task UpdateAsync(IEntity entity)
        {
            if (!_knownReferences.Contains(entity))
            {
                throw new ArgumentException("Unknown entity reference, ensure to pass the same instance returned by Query or Load methods.");
            }
            _flushOperations.Add((CUDOperation.UPDATE, entity));
            _transactionOperations.Add((CUDOperation.UPDATE, entity));
            return Task.CompletedTask;
        }

        public Task DeleteAsync(IEntity entity)
        {
            if (!_knownReferences.Contains(entity))
            {
                throw new ArgumentException("Unknown entity reference, ensure to pass the same instance returned by Query or Load methods.");
            }
            _flushOperations.Add((CUDOperation.DELETE, entity));
            _transactionOperations.Add((CUDOperation.DELETE, entity));
            return Task.CompletedTask;
        }

        public async Task<IEnumerable<IEntity>> LoadAsync(Type type, Guid id)
        {
            var entities = await _repository.LoadAsync(type, id);
            return entities.Select(e => SaveOriginalAndReturnCopy(e));
        }

        public async Task<IEnumerable<IEntity>> LoadAsync(Type type, MemberInfo uniqueMember, object value)
        {
            var entities = await _repository.LoadAsync(type, uniqueMember, value);
            return entities.Select(e => SaveOriginalAndReturnCopy(e));
        }

        public async Task<IEnumerable<IEntity>> LoadAsync(Type type, IEnumerable<Guid> ids)
        {
            var entities = await _repository.LoadAsync(type, ids);
            return entities.Select(e => SaveOriginalAndReturnCopy(e));
        }

        public IQueryable<TEntity> Query<TEntity>()
            where TEntity : class, IEntity
        {
            var queryable = _repository.Query<TEntity>();
            return new ExtendedEnumerable<TEntity>(queryable, e => SaveOriginalAndReturnCopy(e), "transaction");
        }

        private TEntity SaveOriginalAndReturnCopy<TEntity>(TEntity original)
            where TEntity : class, IEntity
        {
            if(original == null)
            {
                return null;
            }
            if (!_copyEntities.ContainsKey(original))
            {
                var copy = (TEntity)_copier.CreateCopy(original);
                _originalEntities[copy] = original;
                _copyEntities[original] = copy;
                _knownReferences.Add(copy);
            }
            return (TEntity)_copyEntities[original];
        }

        public async Task CommitAsync()
        {
            await FlushAsync();
            _originalEntities.Clear();
            _copyEntities.Clear();
            //_knownReferences.Clear();//TODO ajouter test si on change une entit�, la modifie, rollback, puis on la recharge (depuis cache) mais � l'update reference inconnue
            _transactionOperations.Clear();
        }

        public async Task RollbackAsync()
        {
            await RollbackDeletesAsync();
            await RollbackUpdatesAsync();
            await RollbackInsertsAsync();
            _originalEntities.Clear();
            _copyEntities.Clear();
            //_knownReferences.Clear();//TODO ajouter test
            _transactionOperations.Clear();
        }

        private async Task RollbackDeletesAsync()
        {
            var operations = _transactionOperations.Where(x => x.operation == CUDOperation.DELETE);
            foreach (var (operation, entity) in operations)
            {
                var existingEntities = await _repository.LoadAsync(entity.GetType(), entity.Id);
                if (!existingEntities.Any())
                {
                    var initialEntity = _originalEntities.GetValueOrDefault(entity);
                    if (initialEntity != null)
                    {
                        await _repository.InsertAsync(initialEntity);
                    }
                }
            }
        }

        private async Task RollbackUpdatesAsync()
        {
            var operations = _transactionOperations.Where(x => x.operation == CUDOperation.UPDATE);
            foreach (var (operation, entity) in operations)
            {
                var initialEntity = _originalEntities.GetValueOrDefault(entity);
                if (initialEntity != null)
                {
                    await _repository.UpdateAsync(initialEntity);
                }
            }
        }

        private async Task RollbackInsertsAsync()
        {
            var operations = _transactionOperations.Where(x => x.operation == CUDOperation.CREATE);
            foreach (var (operation, entity) in operations)
            {
                var existingEntities = await _repository.LoadAsync(entity.GetType(), entity.Id);
                if (existingEntities.Any())
                {
                    await _repository.DeleteAsync(entity);
                }
            }
        }

        public async Task FlushAsync()
        {
            foreach (var element in _flushOperations)
            {
                if (element.operation == CUDOperation.CREATE)
                {
                    await _repository.InsertAsync(element.entity);
                }
                else if (element.operation == CUDOperation.UPDATE)
                {
                    await _repository.UpdateAsync(element.entity);
                }
                else if (element.operation == CUDOperation.DELETE)
                {
                    await _repository.DeleteAsync(element.entity);
                }
                else
                {
                    throw new NotSupportedException($"Operation not supported : {element.operation}.");
                }
            }
            _flushOperations.Clear();
        }

        public void Dispose()
        {
            if(_transactionOperations.Count > 0)
            {
                RollbackAsync().GetAwaiter().GetResult();
            }
            _repository.Dispose();
        }
    }
}