using System;
using System.Collections;
using System.Collections.Generic;

namespace CocoriCore.Repository
{
    public class StateStore : IEnumerable<IState>, IStateStore
    {
        private readonly Dictionary<object, IState> _stateDictionary;
        private readonly ICopier _copier;

        public StateStore(ICopier copier)
        {
            _stateDictionary = new Dictionary<object, IState>();
            _copier = copier;
        }

        public virtual T SaveState<T>(T obj)
        {
            //TODO ici si on charge une premi�re fois l'entit�, puis la modifie, puis la recharge alors on va enregistrer son dernier state au lieu du premier
            if (obj != null)
            {
                _stateDictionary[obj] = _copier.CreateState(obj);
            }
            return obj;
        }

        public virtual IState<T> GetState<T>(T obj)
            where T : class
        {
            return GetState((object)obj).Cast<T>();
        }

        public virtual bool TryGetState(object obj, out IState state)
        {
            return _stateDictionary.TryGetValue(obj, out state);
        }

        public virtual IState GetState(object obj)
        {
            if (!_stateDictionary.ContainsKey(obj))
            {
                throw new InvalidOperationException("There is no state associated with this instance, ensure to use the same instance returned by load/query.");
            }
            return _stateDictionary[obj];
        }

        protected virtual bool StateExistsForUID(object uid)
        {
            return _stateDictionary.ContainsKey(uid);
        }

        public virtual bool HasState(object obj)
        {
            return _stateDictionary.ContainsKey(obj);
        }

        public virtual void Clear()
        {
            _stateDictionary.Clear();
        }

        public virtual IEnumerator<IState> GetEnumerator()
        {
            return _stateDictionary.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _stateDictionary.Values.GetEnumerator();
        }
    }
}