﻿using System;
using System.Threading.Tasks;

namespace CocoriCore.Repository
{
    public interface ITransactionHolder : IDisposable
    {
        Task CommitAsync();
        Task RollbackAsync();
    }
}
