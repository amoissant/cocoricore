﻿using System;
using System.Text;

namespace CocoriCore.Repository
{
    public class Utf8JsonCopier : ICopier
    {
        public virtual object CreateCopy(object obj)
        {
            var objectType = obj?.GetType() ?? typeof(object);
            var bytes = Utf8Json.JsonSerializer.NonGeneric.Serialize(objectType, obj);
            return Utf8Json.JsonSerializer.NonGeneric.Deserialize(objectType, bytes);
        }

        public virtual string SerializeJson(object obj)
        {
            var objectType = obj?.GetType() ?? typeof(object);
            var bytes = Utf8Json.JsonSerializer.NonGeneric.Serialize(objectType, obj);
            return Encoding.UTF8.GetString(bytes);
        }

        public virtual object DeserializeJson(Type type, string json)
        {
            var bytes = Encoding.UTF8.GetBytes(json);
            return Utf8Json.JsonSerializer.NonGeneric.Deserialize(type, bytes);
        }

        public virtual IState CreateState(object obj)
        {
            if(obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }
            return new State(CreateCopy(obj), obj);
        }
    }
}