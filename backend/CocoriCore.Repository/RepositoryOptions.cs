﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CocoriCore.Repository
{
    public class RepositoryOptions
    {
        public List<Type> DecoratorTypes { get; }

        public RepositoryOptions()
        {
            DecoratorTypes = new List<Type>();
        }

        public RepositoryOptions AddDecorator<T>()
        {
            DecoratorTypes.Add(typeof(T));
            return this;
        }

        public RepositoryOptions AddDecorators(params Type[] types)
        {
            return AddDecorators(types.AsEnumerable());
        }

        public RepositoryOptions AddDecorators(IEnumerable<Type> types)
        {
            DecoratorTypes.AddRange(types);
            return this;
        }
    }
}