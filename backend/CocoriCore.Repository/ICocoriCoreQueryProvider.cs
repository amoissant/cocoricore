﻿using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace CocoriCore.Repository
{
    public interface ICocoriCoreQueryProvider
    {
        IAsyncEnumerable<TResult> AsAsyncEnumerable<TResult>(Expression expression);
        Task<TResult> ExecuteAsync<TResult>(Expression expression, CancellationToken cancellationToken);
        Task<object> ExecuteAsync(Expression expression, CancellationToken cancellationToken);
    }
}