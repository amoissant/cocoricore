﻿using CocoriCore.Types.Extension;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CocoriCore.Repository
{
    public class ExtendedEnumerator<T> : IEnumerator<T>, IAsyncEnumerator<T>
    {
        private readonly IEnumerator<T> _enumerator;
        private Func<object, object> _elementAction;
        private Type _actionType;
        private string _debugName;

        public ExtendedEnumerator(IEnumerator<T> enumerator, Func<object, object> elementAction, Type actionType, string debugName)
        {
            _debugName = debugName;
            _elementAction = elementAction;
            _actionType = actionType;
            _enumerator = enumerator ?? throw new ArgumentNullException();
        }

        public T Current
        {
            get
            {
                T result = _enumerator.Current;
                if(_actionType.IsAssignableTo<T>())
                {
                    result = (T)_elementAction(result);
                }
                return result;
            }
        }

        object IEnumerator.Current => Current;

        public bool MoveNext()
        {
            return _enumerator.MoveNext();
        }

        public async ValueTask<bool> MoveNextAsync()
        {
            if (_enumerator is IAsyncEnumerator<T> asyncEnumerator)
            {
                return await asyncEnumerator.MoveNextAsync();
            }
            else
            {
                return _enumerator.MoveNext();
            }
        }

        public void Reset()
        {
            _enumerator.Reset();
        }

        public void Dispose()
        {
            _enumerator.Dispose();
        }

        public async ValueTask DisposeAsync()
        {
            if (_enumerator is IAsyncDisposable asyncDisposable)
            {
                await asyncDisposable.DisposeAsync();
            }
            else
            {
                _enumerator.Dispose();
            }
        }
    }
}
