﻿using CocoriCore.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace CocoriCore.Repository
{
    public class StateStoreDecorator : IRepositoryDecorator
    {
        private readonly IStateStore _stateStore;

        public StateStoreDecorator(IStateStore stateStore)
        {
            _stateStore = stateStore;
        }

        public async Task<IEnumerable<IEntity>> LoadAsync(Type type, Guid id, LoadIdAsync next)
        {
            var entities = await next(type, id);
            return entities.Select(e => _stateStore.SaveState(e));
        }

        public async Task<IEnumerable<IEntity>> LoadAsync(Type type, IEnumerable<Guid> ids, LoadIdCollectionAsync next)
        {
            var entities = await next(type, ids);
            return entities.Select(e => _stateStore.SaveState(e));
        }

        public async Task<IEnumerable<IEntity>> LoadAsync(Type type, MemberInfo uniqueMember, object value, LoadUniqueAsync next)
        {
            var entities = await next(type, uniqueMember, value);
            return entities.Select(e => _stateStore.SaveState(e));
        }

        public IQueryable<TEntity> Query<TEntity>(Query<TEntity> next) where TEntity : IEntity
        {
            var queryable = next();
            return new ExtendedEnumerable<TEntity>(queryable, x => _stateStore.SaveState(x));
        }

        public async Task InsertAsync(IEntity entity, InsertAsync next)
        {
            _stateStore.SaveState(entity);
            await next(entity);
        }

        public async Task UpdateAsync(IEntity entity, UpdateAsync next)
        {
            await next(entity);
        }

        public async Task DeleteAsync(IEntity entity, DeleteAsync next)
        {
            await next(entity);
        }

        public Task FlushAsync(FlushAsync next)
        {
            return next();
        }
    }
}