﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using CocoriCore.Types.Extension;

namespace CocoriCore.Repository
{
    public class ExtendedEnumerable<T> : IAsyncEnumerable<T>, IOrderedQueryable<T>, ICocoriCoreQueryProvider, IQueryProvider
    {
        private IQueryable<T> _queryable;
        private Expression _expression;
        private Func<object, object> _elementAction;
        private Action _enumerateAction;
        private Type _actionType;
        private string _debugName;

        public ExtendedEnumerable(IQueryable<T> queryable, Action enumerateAction, string debugName = null)
            : this(queryable, x => x, enumerateAction, debugName)
        {
        }

        public ExtendedEnumerable(IQueryable<T> queryable, Func<T, T> elementAction, string debugName = null)
            : this(queryable, elementAction, () => { }, debugName)
        {
        }

        public ExtendedEnumerable(IQueryable<T> queryable, Func<T, T> elementAction, Action enumerateAction, string debugName = null)
        {
            _debugName = debugName;
            _actionType = typeof(T);
            _elementAction = x => elementAction((T)x);
            _enumerateAction = enumerateAction;
            _expression = queryable.Expression;
            _queryable = queryable;
        }

        private ExtendedEnumerable(IQueryable<T> queryable,
            Func<object, object> elementAction, Type actionParameterType, Action enumerateAction, string debugName)
        {
            _debugName = debugName;
            _elementAction = elementAction;
            _enumerateAction = enumerateAction;
            _actionType = actionParameterType;
            _expression = queryable.Expression;
            _queryable = queryable;
        }

        IAsyncEnumerator<T> IAsyncEnumerable<T>.GetAsyncEnumerator(CancellationToken cancellationToken)
        {
            var queryable = _queryable.Provider.CreateQuery<T>(_expression);
            _enumerateAction();
            return new ExtendedEnumerator<T>(queryable.GetEnumerator(), _elementAction, _actionType, _debugName);
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            var queryable = _queryable.Provider.CreateQuery<T>(_expression);
            _enumerateAction();
            return new ExtendedEnumerator<T>(queryable.GetEnumerator(), _elementAction, _actionType, _debugName);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            var queryable = _queryable.Provider.CreateQuery(_expression);
            _enumerateAction();
            return new ExtendedEnumerator<T>((IEnumerator<T>)queryable.GetEnumerator(), _elementAction, _actionType, _debugName);
        }

        public IQueryable CreateQuery(Expression expression)
        {
            var queryable = _queryable.Provider.CreateQuery<T>(expression);
            return new ExtendedEnumerable<T>(queryable, _elementAction, _actionType, _enumerateAction, _debugName);
        }

        public IQueryable<TResult> CreateQuery<TResult>(Expression expression)
        {
            var queryable = _queryable.Provider.CreateQuery<TResult>(expression);
            return new ExtendedEnumerable<TResult>(queryable, _elementAction, _actionType, _enumerateAction, _debugName);
        }

        public object Execute(Expression expression)
        {
            var result = _queryable.Provider.Execute(expression);
            return TryRunAction(result);
        }

        public TResult Execute<TResult>(Expression expression)
        {
            var result = _queryable.Provider.Execute<TResult>(expression);
            return TryRunAction(result);
        }

        public IAsyncEnumerable<TResult> AsAsyncEnumerable<TResult>(Expression expression)
        {
            var queryable = _queryable.Provider.CreateQuery<TResult>(expression);
            return new ExtendedEnumerable<TResult>(queryable, _elementAction, _actionType, _enumerateAction, _debugName);
        }

        public async Task<object> ExecuteAsync(Expression expression, CancellationToken cancellationToken)
        {
            var result = await ((ICocoriCoreQueryProvider)_queryable.Provider).ExecuteAsync(expression, cancellationToken);
            return TryRunAction(result);
        }

        public async Task<TResult> ExecuteAsync<TResult>(Expression expression, CancellationToken cancellationToken)
        {
            var result = await ((ICocoriCoreQueryProvider)_queryable.Provider).ExecuteAsync<TResult>(expression, cancellationToken);
            return TryRunAction(result);
        }

        private TResult TryRunAction<TResult>(TResult result)
        {
            if (result != null && result.GetType().IsAssignableTo(_actionType))
            {
                result = (TResult)_elementAction(result);
            }
            return result;
        }

        public Type ElementType => typeof(T);

        public Expression Expression { get => _expression; }

        public IQueryProvider Provider => this;
    }
}
