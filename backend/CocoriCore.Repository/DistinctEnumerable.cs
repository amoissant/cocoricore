﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace CocoriCore.Repository
{
    public class DistinctEnumerable<T> : IEnumerable<T>, IOrderedQueryable<T>, IQueryProvider
    {
        private IQueryable<T> _queryable;
        private Expression _expression;
        private DistinctVisitor _visitor;

        public DistinctEnumerable(Expression expression)
        {
            _expression = expression;
            _visitor = new DistinctVisitor();
        }

        public DistinctEnumerable(IQueryable<T> queryable)
        {
            _expression = queryable.Expression;
            _visitor = new DistinctVisitor();
        }

        public IEnumerator<T> GetEnumerator()
        {
            return (IEnumerator<T>)this.AsEnumerable().GetEnumerator();
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            if (_queryable == null) _queryable = CompileExpressionItem<IQueryable<T>>(_expression);
            return _queryable.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            if (_queryable == null) _queryable = CompileExpressionItem<IQueryable<T>>(_expression);
            return _queryable.GetEnumerator();
        }

        public IQueryable CreateQuery(Expression expression)
        {
            return new DistinctEnumerable<T>(expression);
        }

        public IQueryable<TEntity> CreateQuery<TEntity>(Expression expression)
        {
            return new DistinctEnumerable<TEntity>(expression);
        }

        public object Execute(Expression expression)
        {
            return CompileExpressionItem<object>(expression);
        }

        public TResult Execute<TResult>(Expression expression)
        {
            return CompileExpressionItem<TResult>(expression);
        }

        public Type ElementType => typeof(T);

        public Expression Expression { get => _expression; }

        public IQueryProvider Provider => this;

        private TResult CompileExpressionItem<TResult>(Expression expression)
        {
            expression = _visitor.Visit(expression);
            return Expression
                .Lambda<Func<TResult>>(expression, (IEnumerable<ParameterExpression>)null)
                .Compile()();
        }
    }
}
