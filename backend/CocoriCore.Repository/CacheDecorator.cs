﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using CocoriCore.Common;
using CocoriCore.Types.Extension;

namespace CocoriCore.Repository
{
    public class CacheDecorator : IRepositoryDecorator
    {
        private readonly Dictionary<(Type, Guid), IEntity> _idCache;
        private readonly Dictionary<(Type, MemberInfo, object), IEntity> _uniqueMemberCache;

        public CacheDecorator()
        {
            _idCache = new Dictionary<(Type, Guid), IEntity>();
            _uniqueMemberCache = new Dictionary<(Type, MemberInfo, object), IEntity>();
        }

        public async Task<IEnumerable<IEntity>> LoadAsync(Type type, Guid id, LoadIdAsync next)
        {
            var key = (type, id);
            if (_idCache.TryGetValue(key, out var entity))
            {
                return new[] { entity };
            }
            else
            {
                var entities = await next(type, id);
                return entities.Select(e => Cache(e));
            }
        }

        public async Task<IEnumerable<IEntity>> LoadAsync(Type type, IEnumerable<Guid> ids, LoadIdCollectionAsync next)
        {
            var notInCacheIds = new List<Guid>();
            var entities = new List<IEntity>();
            foreach(var id in ids)
            {
                if(_idCache.TryGetValue((type, id), out var entity))
                {
                    entities.Add(entity);
                }
                else
                {
                    notInCacheIds.Add(id);
                }
            }
            if (notInCacheIds.Count > 0)
            {
                var notInCacheEntities = await next(type, notInCacheIds);
                foreach (var entity in notInCacheEntities)
                {
                    Cache(entity);
                    entities.Add(entity);
                }
            }
            return entities;
        }

        public async Task<IEnumerable<IEntity>> LoadAsync(Type type, MemberInfo uniqueMember, object value, LoadUniqueAsync next)
        {
            var key = (type, uniqueMember, value);
            if (_uniqueMemberCache.TryGetValue(key, out var entity))
            {
                return new[] { entity };
            }
            else
            {
                var entities = await next(type, uniqueMember, value);
                return entities.Select(e => 
                {
                    _uniqueMemberCache[key] = e;
                    return Cache(e);
                });
            }
        }

        public IQueryable<TEntity> Query<TEntity>(Query<TEntity> next) where TEntity : IEntity
        {
            var queryable = next();
            return new ExtendedEnumerable<TEntity>(queryable, e => Cache(e), "cache");
        }

        public async Task InsertAsync(IEntity entity, InsertAsync next)
        {
            await next(entity);
            Cache(entity);
        }

        public async Task UpdateAsync(IEntity entity, UpdateAsync next)
        {
            //TODO si on update une copie d'une entité en cache alors péter erreur car ca doit etre toujours la même instance
            await next(entity);
        }

        public async Task DeleteAsync(IEntity entity, DeleteAsync next)
        {
            //TODO si on delete une copie d'une entité en cache alors péter erreur car ca doit etre toujours la même instance
            await next(entity);
        }

        private TEntity Cache<TEntity>(TEntity entity) where TEntity : IEntity
        {
            if (_idCache.TryGetValue((entity.GetType(), entity.Id), out var cachedEntity))
            {
                return (TEntity)cachedEntity;
            }
            else
            {
                var typeHierarchy = entity.GetTypeHierarchy();
                foreach (var type in typeHierarchy)
                {
                    _idCache[(type, entity.Id)] = entity;
                }
                return entity;
            }
        }

        public Task FlushAsync(FlushAsync next)
        {
            return next();
        }
    }
}