using System.Threading.Tasks;

namespace CocoriCore.Repository
{
    /// <summary>
    /// Represent a store able to persist data, typically a database.
    /// </summary>
    public interface IPersistentStore
    {
        /// <summary>
        /// Remove all datas from the store preserving its structure (schema).
        /// <para>An in memory store will clear all its collections containing datas.</para>
        /// <para>A database store will run DELETE FROM &lt;TABLE_NAME&gt; or TRUNCATE &lt;TABLE_NAME&gt;</para>  
        /// </summary>
        Task ClearAsync();
    }
}
