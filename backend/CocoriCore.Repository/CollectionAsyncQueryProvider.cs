﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace CocoriCore.Repository
{
    public class CollectionAsyncQueryProvider<T> : IAsyncEnumerable<T>, IOrderedQueryable<T>, ICocoriCoreQueryProvider, IQueryProvider
    {
        private IQueryable<T> _queryable;
        private DistinctVisitor _visitor;//TODO rendre configurable le(s) visitors pour retransformer l'expression ?

        public CollectionAsyncQueryProvider(IEnumerable<T> enumerable)
             : this(enumerable.AsQueryable())
        {
        }

        public CollectionAsyncQueryProvider(IQueryable<T> queryable)
             : this(queryable.Expression)
        {
        }

        public CollectionAsyncQueryProvider(IQueryable queryable)
             : this(queryable.Expression)
        {
        }

        public CollectionAsyncQueryProvider(Expression expression)
        {
            Expression = expression;
            _visitor = new DistinctVisitor();
        }

        public Type ElementType => typeof(T);

        public Expression Expression { get; }

        public IQueryProvider Provider => this;

        private IQueryable<T> Queryable
        {
            get
            {
                if (_queryable == null)
                {
                    _queryable = (IQueryable<T>)CompileExpressionItem(Expression);
                }
                return _queryable;
            }
        }

        IAsyncEnumerator<T> IAsyncEnumerable<T>.GetAsyncEnumerator(CancellationToken cancellationToken)
        {
            return (IAsyncEnumerator<T>)this.AsEnumerable().GetEnumerator();
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return new FakeEnumerator<T>(Queryable.GetEnumerator());
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new FakeEnumerator<T>(Queryable.GetEnumerator());
        }

        public IQueryable CreateQuery(Expression expression)
        {
            return new CollectionAsyncQueryProvider<T>(expression);
        }

        public IQueryable<TEntity> CreateQuery<TEntity>(Expression expression)
        {
            return new CollectionAsyncQueryProvider<TEntity>(expression);
        }

        public object Execute(Expression expression)
        {
            return CompileExpressionItem(expression);
        }

        public TResult Execute<TResult>(Expression expression)
        {
            return (TResult)CompileExpressionItem(expression);
        }

        public IAsyncEnumerable<TResult> AsAsyncEnumerable<TResult>(Expression expression)
        {
            return new CollectionAsyncQueryProvider<TResult>(expression);
        }

        public Task<object> ExecuteAsync(Expression expression, CancellationToken cancellationToken)
        {
            var result = CompileExpressionItem(expression);
            return Task.FromResult(result);
        }

        public Task<TResult> ExecuteAsync<TResult>(Expression expression, CancellationToken cancellationToken)
        {
            var result = (TResult)CompileExpressionItem(expression);
            return Task.FromResult(result);
        }

        private object CompileExpressionItem(Expression expression)
        {
            expression = _visitor.Visit(expression);
            var @delegate = Expression.Lambda(expression).Compile();
            try
            {
                var result = @delegate.DynamicInvoke();
                return result;
            }
            catch (Exception e)
            {
                throw e.InnerException;
            }
        }
    }
}
