﻿using CocoriCore.Collection.Extensions;
using CocoriCore.Common;
using CocoriCore.Types.Extension;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace CocoriCore.Repository
{
    public class MultiSourceQueryProvider<T> : IAsyncEnumerable<T>, IOrderedQueryable<T>, ICocoriCoreQueryProvider, IQueryProvider
    {
        private Expression _expression;
        private IQueryable[] _queryables;

        protected MultiSourceQueryProvider(Expression expression, params IQueryable[] queryables)
        {
            _queryables = queryables;
            _expression = expression;
        }

        public MultiSourceQueryProvider(params IQueryable[] queryables)
        {
            _queryables = queryables;
            Expression<Func<IQueryable<T>>> thisExp = () => this;
            _expression = thisExp.Body;
        }

        IAsyncEnumerator<T> IAsyncEnumerable<T>.GetAsyncEnumerator(CancellationToken cancellationToken)
        {
            return (IAsyncEnumerator<T>)this.AsEnumerable().GetEnumerator();
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return ConstructMultiSourceEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ConstructMultiSourceEnumerator();
        }

        private MultiSourceEnumerator<T> ConstructMultiSourceEnumerator()
        {
            var enumerators = ConstructQueryables(_expression)
                .Select(q => q.GetEnumerator())
                .ToArray();
            return new MultiSourceEnumerator<T>(enumerators);
        }

        private List<IQueryable<T>> ConstructQueryables(Expression expression)
        {
            List<IQueryable<T>> queryables = new List<IQueryable<T>>();
            foreach (var queryable in _queryables)
            {
                var visitor = new InterfaceVisitor(queryable);
                var specializedExpression = visitor.Visit(expression);
                queryables.Add(queryable.Provider.CreateQuery<T>(specializedExpression));
            }
            return queryables;
        }

        public IQueryable CreateQuery(Expression expression)
        {
            return new MultiSourceQueryProvider<T>(expression, _queryables);
        }

        public IQueryable<TResult> CreateQuery<TResult>(Expression expression)
        {
            return new MultiSourceQueryProvider<TResult>(expression, _queryables);
        }

        public TResult Execute<TResult>(Expression expression)
        {
            return (TResult)Execute(expression);
        }

        public object Execute(Expression expression)
        {
            Func<object, Expression, Task<object>> executeAction = (p, e) =>
            {
                var result = ((IQueryProvider)p).Execute(e);
                return Task.FromResult(result);
            };
            return ExecuteAsync(expression, executeAction).GetAwaiter().GetResult();
        }

        public IAsyncEnumerable<TResult> AsAsyncEnumerable<TResult>(Expression expression)
        {
            return new MultiSourceQueryProvider<TResult>(expression, _queryables);
        }

        public async Task<TResult> ExecuteAsync<TResult>(Expression expression, CancellationToken cancellationToken)
        {
            return (TResult)await ExecuteAsync(expression, cancellationToken);
        }

        public async Task<object> ExecuteAsync(Expression expression, CancellationToken cancellationToken)
        {
            Func<object, Expression, Task<object>> executeAction = (p, e) => 
                ((ICocoriCoreQueryProvider)p).ExecuteAsync(e, cancellationToken);
            return await ExecuteAsync(expression, executeAction);
        }

        private async Task<object> ExecuteAsync(Expression expression, Func<object, Expression, Task<object>> executeAction)
        {
            List<object> results = new List<object>();
            var newExpression = RewriteExpression(expression);
            foreach (var queryable in _queryables)
            {
                var visitor = new InterfaceVisitor(queryable);
                var specializedExpression = visitor.Visit(newExpression);
                var result = await executeAction(queryable.Provider, specializedExpression);
                if (result != null)
                {
                    results.Add(result);
                }
            }
            return CheckResults(expression, results);
        }

        private Expression RewriteExpression(Expression expression)
        {
            if (expression is MethodCallExpression methodCall)
            {
                var methodDefinition = GetMethodDefinition(methodCall);
                if (methodDefinition == QueryableMethods.First)
                {
                    var newMethodCall = QueryableMethods.FirstOrDefault.MakeGenericMethod(methodCall.Method.GetGenericArguments());
                    return Expression.Call(null, newMethodCall, methodCall.Arguments);
                }
                else if (methodDefinition == QueryableMethods.FirstOrDefault)
                {
                    return expression;
                }
                else if (methodDefinition == QueryableMethods.Single)
                {
                    var newMethodCall = QueryableMethods.SingleOrDefault.MakeGenericMethod(methodCall.Method.GetGenericArguments());
                    return Expression.Call(null, newMethodCall, methodCall.Arguments);
                }
                else if (methodDefinition == QueryableMethods.SingleOrDefault)
                {
                    return expression;
                }
                else if (methodDefinition == QueryableMethods.Count)
                {
                    return expression;
                }
                else if (methodDefinition == QueryableMethods.Min)
                {
                    return TransformMinMaxExpression(methodCall, QueryableMethods.Min);
                }
                else if (methodDefinition == QueryableMethods.Max)
                {
                    return TransformMinMaxExpression(methodCall, QueryableMethods.Max);
                }
                else if (methodDefinition.Name == nameof(Queryable.Sum))
                {
                    return expression;
                }
            }
            throw new NotImplementedException($"{expression}");
        }


        private MethodInfo GetMethodDefinition(MethodCallExpression methodCall)
        {
            return methodCall.Method.IsGenericMethod ? methodCall.Method.GetGenericMethodDefinition() : methodCall.Method;
        }

        private Expression TransformMinMaxExpression(MethodCallExpression methodCall, MethodInfo minMaxGenericMethod)
        {
            var elementType = methodCall.Method.GetGenericArguments()[0];
            if (!elementType.IsNullable())
            {
                elementType = typeof(Nullable<>).MakeGenericType(elementType);
            }
            var castMethod = QueryableMethods.Cast.MakeGenericMethod(elementType);
            var defaultIfEmptyMethod = QueryableMethods.DefaultIfEmpty.MakeGenericMethod(elementType);
            var minMaxMethod = minMaxGenericMethod.MakeGenericMethod(elementType);
            var castExpression = Expression.Call(null, castMethod, methodCall.Arguments);
            var defaultIfEmpyExpression = Expression.Call(null, defaultIfEmptyMethod, castExpression, Expression.Constant(null, elementType));
            return Expression.Call(null, minMaxMethod, defaultIfEmpyExpression);
        }

        private TResult CheckResults<TResult>(Expression expression, IEnumerable<TResult> results)
        {
            if (expression is MethodCallExpression methodCall)
            {
                var methodDefinition = GetMethodDefinition(methodCall);
                if (methodDefinition== QueryableMethods.First)
                {
                    return results.First();
                }
                else if (methodDefinition== QueryableMethods.FirstOrDefault)
                {
                    return results.FirstOrDefault();
                }
                else if (methodDefinition== QueryableMethods.Single)
                {
                    return results.Single();
                }
                else if (methodDefinition== QueryableMethods.SingleOrDefault)
                {
                    return results.SingleOrDefault();
                }
                else if (methodDefinition== QueryableMethods.Count)
                {
                    return (TResult)results.Cast(typeof(int)).Sum();
                }
                else if (methodDefinition== QueryableMethods.Min)
                {
                    return results.Min();
                }
                else if (methodDefinition == QueryableMethods.Max)
                {
                    return results.Max();
                }
                else if (methodDefinition.Name == nameof(Queryable.Sum))
                {
                    return (TResult)results.Cast(methodDefinition.ReturnType).Sum();
                }
            }
            throw new NotImplementedException($"{expression}");
        }

        public Type ElementType => typeof(T);

        public Expression Expression { get => _expression; }

        public IQueryProvider Provider => this;
    }
}
