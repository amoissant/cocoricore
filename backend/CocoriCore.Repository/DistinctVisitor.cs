﻿using CocoriCore.Common;
using System.Linq;
using System.Linq.Expressions;

namespace CocoriCore.Repository
{
    public class DistinctVisitor : ExpressionVisitor
    {
        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            if (node.Method.IsGenericMethod && node.Method.GetGenericMethodDefinition() == QueryableMethods.Distinct)
            {
                var genericArgument = node.Method.GetGenericArguments().Single();
                var comparerType = typeof(PropertyOfFieldComparer<>).MakeGenericType(genericArgument);
                var newComparerExpression = Expression.New(comparerType);
                var distinctWithEqualityComparerType = QueryableMethods.DistinctComparer.MakeGenericMethod(genericArgument);
                return Expression.Call(node.Object, distinctWithEqualityComparerType, node.Arguments[0], newComparerExpression);
            }
            return base.VisitMethodCall(node);
        }
    }
}
