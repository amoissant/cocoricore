﻿using CocoriCore.Reflection.Extension;
using CocoriCore.Types.Extension;
using System.Collections.Generic;

namespace CocoriCore.Repository
{
    public class PropertyOfFieldComparer<T> : IEqualityComparer<T>
    {
        public bool Equals(T x, T y)
        {
            var members = typeof(T).GetPropertiesAndFields();
            foreach (var member in members)
            {
                var xValue = member.InvokeGetter(x);
                var yValue = member.InvokeGetter(y);
                if (!Equals(xValue, yValue))
                    return false;
            }
            return true;
        }

        public int GetHashCode(T obj)
        {
            if (typeof(T).IsValueType)
                return obj.GetHashCode();

            var hashCode = 7;
            var members = typeof(T).GetPropertiesAndFields();
            foreach (var member in members)
            {
                var val = member.InvokeGetter(obj);
                if (val != null)
                    hashCode ^= val.GetHashCode();
            }
            return hashCode;
        }
    }
}
