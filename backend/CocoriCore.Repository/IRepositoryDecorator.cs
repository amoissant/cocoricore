﻿using CocoriCore.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace CocoriCore.Repository
{
    public delegate Task<IEnumerable<IEntity>> LoadIdAsync(Type type, Guid id);
    public delegate Task<IEnumerable<IEntity>> LoadIdCollectionAsync(Type type, IEnumerable<Guid> ids);
    public delegate Task<IEnumerable<IEntity>> LoadUniqueAsync(Type type, MemberInfo uniqueMember, object value);
    public delegate IQueryable<TEntity> Query<TEntity>();
    public delegate Task InsertAsync(IEntity entity);
    public delegate Task UpdateAsync(IEntity entity);
    public delegate Task DeleteAsync(IEntity entity);
    public delegate Task FlushAsync();

    public interface IRepositoryDecorator
    {
        Task<IEnumerable<IEntity>> LoadAsync(Type type, Guid id, LoadIdAsync next);

        Task<IEnumerable<IEntity>> LoadAsync(Type type, IEnumerable<Guid> ids, LoadIdCollectionAsync next);

        Task<IEnumerable<IEntity>> LoadAsync(Type type, MemberInfo uniqueMember, object value, LoadUniqueAsync next);

        IQueryable<TEntity> Query<TEntity>(Query<TEntity> next) where TEntity : IEntity;

        Task InsertAsync(IEntity entity, InsertAsync next);

        Task UpdateAsync(IEntity entity, UpdateAsync next);

        Task DeleteAsync(IEntity entity, DeleteAsync next);

        Task FlushAsync(FlushAsync next);
    }
}
