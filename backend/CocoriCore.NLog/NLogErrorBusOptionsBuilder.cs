using System;
using System.Threading.Tasks;
using CocoriCore.Common;
using NLog;

namespace CocoriCore.NLog
{
    public class NLogErrorBusOptionsBuilder : ErrorBusOptionsBuilder
    {
        private LogFactory _logFactory;

        public NLogErrorBusOptionsBuilder(LogFactory factory)
        {
            _logFactory = factory;

            For<Exception>().Call<ErrorFormatter>((f, e) =>
            {
                var logger = factory.GetLogger(typeof(ErrorBus).FullName);
                logger.Error(f.Format(e));
                return Task.CompletedTask;

            });

            SetUnexpectedExceptionHandler((ue, e) =>
            {
                var logger = _logFactory.GetLogger("UnexpectedError");
                logger.Fatal(ue);
                if (e != null)
                    logger.Error(e);
                return Task.CompletedTask;
            });
        }

        
    }
}