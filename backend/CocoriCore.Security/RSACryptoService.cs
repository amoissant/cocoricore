﻿using CocoriCore.Common;
using System;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace CocoriCore.Security
{
    public class RSACryptoService : ICryptoService
    {
        private string _keyContainerName;

        public RSACryptoService(string keyContainerName)
        {
            _keyContainerName = keyContainerName;
        }

        public byte[] GenerateNewKey()
        {
            using (var rsa = new RSACryptoServiceProvider(2048))//TODO utiliser RSA.Create()
            {
                var key = rsa.ExportCspBlob(true);
                return key;
            }
        }

        public virtual async Task<byte[]> InstallNewKeyAsync()
        {
            byte[] key = GenerateNewKey();
            await InstallKeyAsync(key);
            return key;
        }

        public async Task InstallKeyAsync(byte[] binaryKey)
        {
            var aKeyIsInstalled = await AKeyIsAlreadyInstalledAsync();
            if (aKeyIsInstalled)
            {
                throw new ConfigurationException($"A key is already installed in container '{_keyContainerName}'.\n"
                    + $"Clear container using {nameof(RSACryptoService)}.{nameof(UninstallKeyAsync)}() first if you want to install another key.");
            }
            else
            {
                using (var rsa = CreateCryptoServiceProvider(CspProviderFlags.NoFlags))
                {
                    rsa.ImportCspBlob(binaryKey);
                }
            }
        }

        protected virtual RSACryptoServiceProvider CreateCryptoServiceProvider(CspProviderFlags flags)
        {
            try
            {
                var containerParameters = new CspParameters();
                containerParameters.Flags = flags;
                containerParameters.KeyContainerName = _keyContainerName;
                return new RSACryptoServiceProvider(containerParameters);
            }
            catch (CryptographicException e)
            {
                if (e.Message.Contains("Le jeu de clés n’existe pas") ||
                    e.Message.Contains("Keyset does not exist"))
                    throw new ConfigurationException($"Key container '{_keyContainerName}' is empty, you must install a key into this container before encrypt/decrypt datas.\n"
                        + $"You can install an existing key using {nameof(RSACryptoService)}.{nameof(InstallKeyAsync)}() "
                        + $"or you can generate and install a new one using {nameof(RSACryptoService)}.{nameof(InstallNewKeyAsync)}().", e);
                else if (e.Message.Contains("System cannot find specified file") ||
                    e.Message.Contains("Le fichier spécifié est introuvable"))
                    throw new ConfigurationException("You have to configure application pool to load user profil.\n"
                        + "On IIS Manager open application pool then into advanced settings/Process Model set Load User Profile to True", e);
                else
                    throw;
            }
        }

        public virtual Task<bool> AKeyIsAlreadyInstalledAsync()
        {
            try
            {
                var containerParameters = new CspParameters();
                containerParameters.KeyContainerName = _keyContainerName;
                containerParameters.Flags = CspProviderFlags.UseExistingKey;
                using (var rsa = new RSACryptoServiceProvider(containerParameters)) { }
                return Task.FromResult(true);
            }
            catch (CryptographicException)
            {
                return Task.FromResult(false);
            }
        }

        public virtual async Task UninstallKeyAsync()
        {
            var aKeyIsInstalled = await AKeyIsAlreadyInstalledAsync();
            if (aKeyIsInstalled)
            {
                using (var rsa = CreateCryptoServiceProvider(CspProviderFlags.UseExistingKey))
                {
                    rsa.PersistKeyInCsp = false;
                }
            }
        }

        public Task<byte[]> GetInstalledKeyAsync()
        {
            using (var rsa = CreateCryptoServiceProvider(CspProviderFlags.NoFlags))
            {
                var key = rsa.ExportCspBlob(true);
                return Task.FromResult(key);
            }
        }

        public byte[] Decrypt(byte[] bytes)
        {
            if (bytes == null)
            {
                return null;
            }
            using (var rsa = CreateCryptoServiceProvider(CspProviderFlags.UseExistingKey))
            {
                return rsa.Decrypt(bytes, true);
            }
        }

        public byte[] Encrypt(byte[] bytes)
        {
            if (bytes == null)
            {
                return null;
            }
            using (var rsa = CreateCryptoServiceProvider(CspProviderFlags.UseExistingKey))
            {
                return rsa.Encrypt(bytes, true);
            }
        }

        public byte[] Decrypt(byte[] bytes, byte[] key)
        {
            throw new NotImplementedException();
        }

        public byte[] Encrypt(byte[] bytes, byte[] key)
        {
            throw new NotImplementedException();
        }
        public void CheckKeyIsValid(byte[] key)
        {
            throw new NotImplementedException();
        }
    }
}
