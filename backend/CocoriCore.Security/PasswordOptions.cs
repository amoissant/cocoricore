﻿namespace CocoriCore.Security
{
    public class PasswordOptions
    {
        public PasswordOptions()
        {
            MinimumLength = 8;
            RequireDigits = true;
            RequireLowercase = true;
            RequireSpecialCharacters = true;
            RequireUppercase = true;

            AllowedDigits = "1234567890";
            AllowedLowercase = "abcdefghijklmnopqrstuvwxyz";
            AllowedUppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            AllowedSpecialCharacters = " !\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~";
        }

        public int MinimumLength { get; set; }
        public bool RequireDigits { get; set; }
        public bool RequireLowercase { get; set; }
        public bool RequireUppercase { get; set; }
        public bool RequireSpecialCharacters { get; set; }
        public string AllowedLowercase { get; set; }
        public string AllowedUppercase { get; set; }
        public string AllowedDigits { get; set; }
        public string AllowedSpecialCharacters { get; set; }
    }
}