namespace CocoriCore.Security
{
    public interface IHashService
    {
        string Hash(string value);
        bool ValueMatchHash(string value, string correctHash);
    }
}