using System;

namespace CocoriCore.Security
{
    public class HashService : IHashService
    {
        public string Hash(string value)
        {
            if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }
            return PasswordHash.PasswordHash.CreateHash(value);
        }

        public bool ValueMatchHash(string value, string correctHash)
        {
            if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }
            if (correctHash == null)
            {
                throw new ArgumentNullException(nameof(correctHash));
            }
            return PasswordHash.PasswordHash.ValidatePassword(value, correctHash);
        }
    }
}