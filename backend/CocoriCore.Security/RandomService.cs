using System.Linq;
using System.Security.Cryptography;

namespace CocoriCore.Security
{
    public class RandomService : IRandomService
    {
        public string GenerateRandomString(int length, string allowedCharacters = "abcdefghijklmnopqrstuvwxyz")
        {
            using (var provider = new RNGCryptoServiceProvider())
            {
                byte[] bytes = new byte[length];
                provider.GetBytes(bytes);
                char[] result = new char[length];
                for(var i = 0; i < length; i++)
                {
                    var nextCharIndex = (bytes[i]) % allowedCharacters.Length;
                    result[i] = allowedCharacters[nextCharIndex];
                }
                return new string(result);
            }
        }

        public byte[] GenerateRandomBytes(int length)
        {
            using (var provider = new RNGCryptoServiceProvider())
            {
                byte[] bytes = new byte[length];
                provider.GetBytes(bytes);
                return bytes;
            }
        }

        public int[] GenerateRandomNumbers(int length)
        {
            using (var provider = new RNGCryptoServiceProvider())
            {
                byte[] bytes = new byte[length];
                provider.GetBytes(bytes);
                return bytes.Select(x => (int)x).ToArray();
            }
        }
    }
}