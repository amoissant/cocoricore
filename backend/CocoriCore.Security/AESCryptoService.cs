﻿using CocoriCore.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace CocoriCore.Security
{
    public class AESCryptoService : ICryptoService
    {
        private readonly byte[] _key;
        private readonly int _keySize;

        public AESCryptoService(string base64Key = null, int keySize = 256)
        {
            _key = base64Key != null ? Convert.FromBase64String(base64Key) : null;
            _keySize = keySize;
        }

        public byte[] Decrypt(byte[] cipherData, byte[] key = null)
        {
            try
            {
                if (cipherData == null) return null;
                key = key ?? _key;

                using (AesCryptoServiceProvider cryptoProvider = new AesCryptoServiceProvider())
                {
                    var (cipherPayload, iv) = cipherData.SplitAt(-16);//With AES IV has only one size wich is 16 bytes (128 bits)
                    cryptoProvider.Key = key;
                    cryptoProvider.IV = iv;
                    var decryptor = cryptoProvider.CreateDecryptor(cryptoProvider.Key, cryptoProvider.IV);

                    using (MemoryStream ms = new MemoryStream(cipherPayload))
                    using (CryptoStream cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Read))
                    using (MemoryStream rs = new MemoryStream())
                    {
                        cs.CopyTo(rs);
                        return rs.ToArray();
                    }
                }
            }
            catch (CryptographicException e)
            {
                throw new CryptographicException("Unable to decrypt datas.\n" +
                    "Ensure data is encrypted and to use the same key for encryption and decryption.\n" +
                    "See inner exception for more informations.", e);
            }
        }

        public byte[] Encrypt(byte[] plainData, byte[] key = null)
        {
            try
            {
                if (plainData == null) return null;
                key = key ?? _key;

                using (AesCryptoServiceProvider cryptoProvider = new AesCryptoServiceProvider())
                {
                    cryptoProvider.Key = key;
                    cryptoProvider.GenerateIV();
                    var iv = cryptoProvider.IV;

                    var encryptor = cryptoProvider.CreateEncryptor(cryptoProvider.Key, cryptoProvider.IV);

                    using (MemoryStream ms = new MemoryStream())
                    using (CryptoStream cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
                    {
                        cs.Write(plainData, 0, plainData.Length);
                        cs.FlushFinalBlock();
                        return ms.ToArray().Append(iv);
                    }
                }
            }
            catch (CryptographicException e)
            {
                throw new CryptographicException("Unable to encrypt datas.\n" +
                    "See inner exception for more informations.", e);
            }
        }

        public byte[] GenerateNewKey()
        {
            using (var aes = new AesCryptoServiceProvider())
            {
                aes.KeySize = _keySize;
                aes.GenerateKey();
                return aes.Key;
            }
        }

        public void CheckKeyIsValid(byte[] key)
        {
            if(key == null)
            {
                return;
            }
            Encrypt("test".GetUtf8Bytes(), key);
        }
    }
}
