namespace CocoriCore.Security
{
    public interface IRandomService
    {
        byte[] GenerateRandomBytes(int length);
        int[] GenerateRandomNumbers(int length);
        string GenerateRandomString(int length, string allowedCharacters = "abcdefghijklmnopqrstuvwxyz");
    }
}