﻿namespace CocoriCore.Security
{
    public interface ICryptoService
    {
        byte[] Decrypt(byte[] bytes, byte[] key = null);
        byte[] Encrypt(byte[] bytes, byte[] key = null);
        byte[] GenerateNewKey();
        void CheckKeyIsValid(byte[] key);
    }
}