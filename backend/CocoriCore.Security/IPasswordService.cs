namespace CocoriCore.Security
{
    public interface IPasswordService
    {
        string GeneratePassword();
        bool PasswordIsValid(string password, string correctHash);
        string HashPassword(string password);
        bool PasswordIsWeak(string password);
    }
}