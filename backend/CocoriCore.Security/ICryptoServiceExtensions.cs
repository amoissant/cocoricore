﻿using CocoriCore.Common;

namespace CocoriCore.Security
{
    public static class ICryptoServiceExtensions
    {
        public static byte[] Encrypt(this ICryptoService cryptoService, string utf8text, byte[] key = null)
        {
            return cryptoService.Encrypt(utf8text.GetUtf8Bytes(), key);
        }
    }
}