using System;
using System.Linq;
using System.Text.RegularExpressions;
using PasswordGenerator;

namespace CocoriCore.Security
{
    public class PasswordService : IPasswordService
    {
        private readonly IHashService _hashService;
        private readonly Regex _lowercase;
        private readonly Regex _uppercase;
        private readonly Regex _number;
        private readonly PasswordOptions _options;

        public PasswordService(IHashService hashService, PasswordOptions options = null)
        {
            _options = options ?? new PasswordOptions();
            _hashService = hashService;
            _lowercase = new Regex($"[{_options.AllowedLowercase}]");
            _uppercase = new Regex($"[{_options.AllowedUppercase}]");
            _number = new Regex($"[{_options.AllowedDigits}]");
        }

        public virtual string GeneratePassword()
        {
            IPassword builder = new Password(_options.MinimumLength);
            if (_options.RequireDigits)
            {
                builder = builder.IncludeNumeric();
            }
            if (_options.RequireLowercase)
            {
                builder = builder.IncludeLowercase();
            }
            if (_options.RequireSpecialCharacters)
            {
                builder = builder.IncludeSpecial(_options.AllowedSpecialCharacters);
            }
            if (_options.RequireUppercase)
            {
                builder = builder.IncludeUppercase();
            }
            return builder.Next();
        }

        public virtual bool PasswordIsValid(string password, string correctHash)
        {
            return _hashService.ValueMatchHash(password, correctHash);
        }

        public virtual string HashPassword(string password)
        {
            return _hashService.Hash(password);
        }

        public bool PasswordIsWeak(string password)
        {
            return string.IsNullOrEmpty(password) ||
                _options.MinimumLength > password.Length ||
                (_options.RequireLowercase && !_lowercase.IsMatch(password)) ||
                (_options.RequireUppercase &&!_uppercase.IsMatch(password)) ||
                (_options.RequireSpecialCharacters && !password.Any(c => _options.AllowedSpecialCharacters.Contains(c))) ||
                (_options.RequireDigits && !_number.IsMatch(password));
        }
    }
}