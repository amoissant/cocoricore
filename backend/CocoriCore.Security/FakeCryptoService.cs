﻿using System;
using System.Text;
using System.Threading.Tasks;

namespace CocoriCore.Security
{
    public class FakeCryptoService : ICryptoService
    {
        public byte[] Decrypt(byte[] bytes, byte[] key = null)
        {
            if (bytes == null)
            {
                return null;
            }
            key = key ?? new byte[0];
            var result = new byte[bytes.Length - key.Length];
            Array.Copy(bytes, result, bytes.Length - key.Length);
            Array.Reverse(result);
            return result;
        }

        public byte[] Encrypt(byte[] bytes, byte[] key)
        {
            if (bytes == null)
            {
                return null;
            }
            key = key ?? new byte[0];
            var result = new byte[key.Length + bytes.Length];
            Array.Copy(key, result, key.Length);
            Array.Copy(bytes, 0, result, key.Length, bytes.Length);
            Array.Reverse(result);
            return result;
        }

        public byte[] GenerateNewKey()
        {
            return Encoding.UTF8.GetBytes("a fake key");
        }

        public void CheckKeyIsValid(byte[] key)
        {
            if(key == null  || key.Length <= 4)
            {
                throw new Exception("Invalid key");
            }
        }

        public Task InstallKeyAsync(byte[] key)
        {
            return Task.CompletedTask;
        }

        public Task<byte[]> InstallNewKeyAsync()
        {
            return Task.FromResult(Encoding.UTF8.GetBytes("a fake key"));
        }

        public Task UninstallKeyAsync()
        {
            return Task.CompletedTask;
        }
    }
}
