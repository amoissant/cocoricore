$localPackageDirectory = "C:\Users\AMoissant\Documents\LocalNuget\"
$localNugetCache = "C:\Users\amoissant\.nuget\packages\"
$remotePackageDirectory = "\\TFSBUILD2017\Partage\Nuget"
$remoteNugetCache = "\\TFSBUILD2017\Users\Bewise\.nuget\packages\"


dotnet pack --output $localPackageDirectory

get-childitem -Path ($localNugetCache+"cocoricore*") -Attributes Directory | Remove-Item -Force -Recurse

Copy-Item -Path ($localPackageDirectory+"*0.0.0-last.nupkg") -Destination $remotePackageDirectory

get-childitem -Path ($remoteNugetCache+"cocoricore*") -Attributes Directory | Remove-Item -Force -Recurse
