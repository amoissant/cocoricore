﻿using System;

namespace CocoriCore.Clock
{
    public interface IClock
    {
        DateTime Now { get; }
        DateTime Today { get; }
        DateTime UtcNow { get; }
        DateTime UtcToday { get; }
    }
}
