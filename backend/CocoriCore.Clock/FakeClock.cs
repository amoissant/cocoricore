﻿using CocoriCore.Common;
using System;

namespace CocoriCore.Clock
{
    public class FakeClock : IClock
    {
        private TimeSpan _delta;
        private bool _timeStopped;
        private DateTimeOffset? _fixedNow;

        public FakeClock()
        {
            _delta = new TimeSpan(0);
        }

        public DateTime Now
        {
            get
            {
                if(!_timeStopped)
                {
                    _delta += TimeSpan.FromMilliseconds(1);
                }
                return GetNowDateTimeOffset().Add(_delta).LocalDateTime;
            }
            set
            {
                _delta = value - GetNowDateTimeOffset();
            }
        }

        private DateTimeOffset GetNowDateTimeOffset()
        {
            if (_timeStopped)
            {
                return _fixedNow.Value;
            }
            else
            {
                return DateTime.UtcNow.RoundMilliseconds();
            }
        }

        public DateTime UtcNow { get => Now.ToUniversalTime(); set => Now = value; }

        public DateTime Today => Now.Date;

        public DateTime UtcToday => UtcNow.Date;

        public FakeClock StopTimeAt(DateTime dateTime)
        {
            _fixedNow = dateTime;
            _timeStopped = true;
            return this;
        }

        public FakeClock StopTime()
        {
            _fixedNow = GetNowDateTimeOffset();
            _timeStopped = true;
            return this;
        }

        public FakeClock ContinueTime()
        {
            _fixedNow = null;
            _timeStopped = false;
            return this;
        }
    }
}
