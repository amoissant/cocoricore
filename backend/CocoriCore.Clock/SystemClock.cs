﻿using System;

namespace CocoriCore.Clock
{
    public class SystemClock : IClock
    {
        public DateTime Now => DateTime.Now;

        public DateTime Today => Now.Date;

        public DateTime UtcNow => DateTime.UtcNow;

        public DateTime UtcToday => DateTime.UtcNow.Date;
    }
}
