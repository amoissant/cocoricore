git pull

$lastTag = git tag | select -last 1
$lastVersion = "0.0.0-last"
$localPackageDirectory = "C:\Users\AMoissant\Documents\LocalNuget\"
$localNugetCache = "C:\Users\amoissant\.nuget\packages\"
$remotePackageDirectory = "\\TFSBUILD2017\Partage\Nuget"
$remoteNugetCache = "\\TFSBUILD2017\Users\Bewise\.nuget\packages\"

function Replace-Version ($a, $b) {
	Get-ChildItem -Path "." -Recurse -include "*.csproj" -exclude "*Test" |
	ForEach-Object {
		(Get-Content $_) |
		ForEach-Object {
			if($_ -match "<Version>" + $a + "</Version>")
			{
				$_ | % { $_ -replace $a, $b }
			}
			else
			{
				$_
			}
		} |
		Set-Content -Path $_.FullName
	}
}

Replace-Version $lastVersion $lastTag 

dotnet pack --output $localPackageDirectory

Replace-Version $lastTag $lastVersion 

get-childitem -Path ($localNugetCache+"cocoricore*") -Attributes Directory | Remove-Item -Force -Recurse

#Copy-Item -Path ($localPackageDirectory+"*"+$lastTag+".nupkg") -Destination $remotePackageDirectory

#get-childitem -Path ($remoteNugetCache+"cocoricore*") -Attributes Directory | Remove-Item -Force -Recurse



