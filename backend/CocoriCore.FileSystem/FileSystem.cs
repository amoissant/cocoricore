﻿using CocoriCore.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace CocoriCore.FileSystem
{
    public class FileSystem : IFileSystem
    {
        //TODO écrire ce code en version async car ici on utilise les appels bloquants
        private string _rootPath;
        public Path TempPath { get; }

        public FileSystem(string rootPath = null)
        {
            _rootPath = rootPath;
            TempPath = new Path("tmp");//TODO à rendre configurable
        }

        public string GetAbsolutePath(Path path)
        {
            var separator = System.IO.Path.DirectorySeparatorChar.ToString();
            var concatenatedSegments = string.Join(separator, path.Segments);
            var absolutePath = string.Empty;
            if (_rootPath != null)
            {
                absolutePath = _rootPath + separator + concatenatedSegments;
            }
            else if(path.IsAbsolute)
            {
                absolutePath = path.ToString();
            }
            else
            {
                absolutePath = concatenatedSegments;
            }
            absolutePath = Environment.ExpandEnvironmentVariables(absolutePath);
            return System.IO.Path.GetFullPath(absolutePath);
        }

        public async Task CreateBinaryFileAsync(Path path, string base64Content)
        {
            var binaryContent = Convert.FromBase64String(base64Content);
            await CreateBinaryFileAsync(path, binaryContent);
        }

        public async Task CreateBinaryFileAsync(Path path, byte[] binaryContent)
        {
            var absolutePath = GetAbsolutePath(path);
            using (var stream = System.IO.File.Create(absolutePath))
            {
                await stream.WriteAsync(binaryContent, 0, binaryContent.Length);
            }
        }

        public Task CreateTextFileAsync(Path path, string textContent)
        {
            var binaryContent = System.Text.Encoding.UTF8.GetBytes(textContent);
            return CreateBinaryFileAsync(path, binaryContent);
        }

        public async Task AppendContentToFileAsync(Path contentPath, Path destinationPath)
        {
            var absoluteContentPath = GetAbsolutePath(contentPath);
            var absoluteDestinationPath = GetAbsolutePath(destinationPath);
            using (var sr = System.IO.File.OpenRead(absoluteContentPath))
            using (var sw = System.IO.File.OpenWrite(absoluteDestinationPath))
            {
                sw.Position = sw.Length;
                await sr.CopyToAsync(sw);
            }
        }

        public Task CreateDirectoryAsync(Path path)
        {
            var absolutePath = GetAbsolutePath(path);
            System.IO.Directory.CreateDirectory(absolutePath);
            return Task.CompletedTask;
        }

        public Task DeleteDirectoryAsync(Path path)
        {
            var absolutePath = GetAbsolutePath(path);
            System.IO.Directory.Delete(absolutePath);//TODO il manque les opérations récursives
            return Task.CompletedTask;
        }

        public Task<bool> DeleteFileAsync(Path path)
        {
            var absolutePath = GetAbsolutePath(path);
            System.IO.File.Delete(absolutePath);
            return Task.FromResult(true);
        }

        public Task<bool> DirectoryExistsAsync(Path path)
        {
            var absolutePath = GetAbsolutePath(path);
            var directoryExists = System.IO.Directory.Exists(absolutePath);
            return Task.FromResult(directoryExists);
        }

        public Task<bool> FileExistsAsync(Path path)
        {
            var absolutePath = GetAbsolutePath(path);
            var fileExists = System.IO.File.Exists(absolutePath);
            return Task.FromResult(fileExists);
        }

        public Task<DateTime> GetCreationDateTimeAsync(Path path)
        {
            var absolutePath = GetAbsolutePath(path);
            if (System.IO.Directory.Exists(absolutePath))
            {
                return Task.FromResult(System.IO.Directory.GetCreationTime(absolutePath));
            }
            if (System.IO.File.Exists(absolutePath))
            {
                return Task.FromResult(System.IO.File.GetCreationTime(absolutePath));
            }
            throw new InvalidOperationException();
        }

        public Path GetUniqueTempPath()
        {
            return System.IO.Path.Combine(TempPath, Guid.NewGuid().ToString());
        }

        public Task<IEnumerable<Path>> ListFilesAsync(Path path)
        {
            var rootAbsolutePath = System.IO.Path.GetFullPath(_rootPath);
            var absolutePath = GetAbsolutePath(path);
            var files = System.IO.Directory.GetFiles(absolutePath);
            var pathCollection = files.Select(x => new Path(x.Substring(rootAbsolutePath.Length)));//TODO utiliserr méthode relativeTo ici 
            return Task.FromResult(pathCollection);
        }

        public Task<IEnumerable<Path>> ListDirectoriesAsync(Path path)
        {
            var rootAbsolutePath = System.IO.Path.GetFullPath(_rootPath);
            var absolutePath = GetAbsolutePath(path);
            var directories = System.IO.Directory.GetDirectories(absolutePath);
            var pathCollection = directories.Select(x => new Path(x.Substring(rootAbsolutePath.Length)));//TODO utiliser méthode relativeTo ici 
            return Task.FromResult(pathCollection);
        }

        public Task MoveFileAsync(Path sourcePath, Path destinationFile)
        {
            var absoluteSourcePath = GetAbsolutePath(sourcePath);
            var absoluteDestinationPath = GetAbsolutePath(destinationFile);
            System.IO.File.Move(absoluteSourcePath, absoluteDestinationPath);
            return Task.CompletedTask;
        }

        public async Task<string> ReadAsBase64Async(Path path)
        {
            var binaryContent = await ReadAsBinaryAsync(path);
            return Convert.ToBase64String(binaryContent);
        }

        public Task<byte[]> ReadAsBinaryAsync(Path path)
        {
            var absolutePath = GetAbsolutePath(path);
            using (var stream = System.IO.File.OpenRead(absolutePath))
            {
                return Task.FromResult(stream.ReadAllBytes());
            }
        }

        public async Task<string> ReadAsTextAsync(Path path)
        {
            var bynaryContent = await ReadAsBinaryAsync(path);
            var textContent = System.Text.Encoding.UTF8.GetString(bynaryContent);
            return textContent;
        }

        public Lazy<System.IO.Stream> GetReadStream(Path path)
        {
            var absolutePath = GetAbsolutePath(path);
            return new Lazy<System.IO.Stream>(() => System.IO.File.OpenRead(absolutePath));
        }

        public Task<string> ComputeChecksumAsync(Path path)
        {
            var absolutePath = GetAbsolutePath(path);
            using (var sr = System.IO.File.OpenRead(absolutePath))
            {
                var md5 = MD5.Create().ComputeHash(sr);//TODO utiliser autre chose car algo cassé en 2003, voir ce qui existe côté front sha256
                return Task.FromResult(md5.ToBase64());
            }
        }

        public Task ClearAsync()
        {
            System.IO.Directory.Delete(_rootPath, true);
            System.IO.Directory.CreateDirectory(_rootPath);
            return Task.CompletedTask;
        }
    }
}
