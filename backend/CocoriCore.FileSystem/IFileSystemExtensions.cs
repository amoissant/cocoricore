﻿using CocoriCore.Common;
using System.Threading.Tasks;

namespace CocoriCore.FileSystem
{
    public static class IFileSystemExtensions
    {
        public static async Task EnsureDirectoryExistsAsync(this IFileSystem fileSystem, Path path)
        {
            if (!await fileSystem.DirectoryExistsAsync(path))
            {
                await fileSystem.CreateDirectoryAsync(path);
            }
        }

        public static async Task CreateBinaryFileAsync(this IFileSystem fileSystem, Path path, byte[] binaryContent, bool ensureParentExists = false)
        {
            if(ensureParentExists)
            {
                await fileSystem.EnsureDirectoryExistsAsync(path.GetParent());
            }
            await fileSystem.CreateBinaryFileAsync(path, binaryContent);
        }

        public static async Task CreateBinaryFileAsync(this IFileSystem fileSystem, Path path, string base64Content, bool ensureParentExists = false)
        {
            if (ensureParentExists)
            {
                await fileSystem.EnsureDirectoryExistsAsync(path.GetParent());
            }
            await fileSystem.CreateBinaryFileAsync(path, base64Content);
        }

        public static async Task CreateTextFileAsync(this IFileSystem fileSystem, Path path, string textContent, bool ensureParentExists = false)
        {
            if (ensureParentExists)
            {
                await fileSystem.EnsureDirectoryExistsAsync(path.GetParent());
            }
            await fileSystem.CreateTextFileAsync(path, textContent);
        }

        public static async Task MoveFileAsync(this IFileSystem fileSystem, Path sourcePath, Path destinationPath, bool ensureParentExists = false)
        {
            if (ensureParentExists)
            {
                await fileSystem.EnsureDirectoryExistsAsync(destinationPath.GetParent());
            }
            await fileSystem.MoveFileAsync(sourcePath, destinationPath);
        }
    }
}
