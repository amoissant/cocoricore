﻿using CocoriCore.Clock;
using CocoriCore.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CocoriCore.FileSystem
{
    public class InMemoryFileSystem : IFileSystem
    {
        private Dictionary<Path, byte[]> _files;
        private HashSet<Path> _directories;
        private Dictionary<Path, DateTime> _fileCreationDateTimes;
        private Dictionary<Path, DateTime> _directoryCreationDateTimes;
        private IClock _clock;
        private bool _isCaseSensitive;

        public Path TempPath => "tmp";

        public InMemoryFileSystem(IClock clock, bool isCaseSensitive = true)
        {
            _files = new Dictionary<Path, byte[]>();
            _directories = new HashSet<Path>();
            _fileCreationDateTimes = new Dictionary<Path, DateTime>();
            _directoryCreationDateTimes = new Dictionary<Path, DateTime>();
            _clock = clock;
            _directories.Add(new Path());
            _isCaseSensitive = isCaseSensitive;
        }

        public Task<bool> FileExistsAsync(Path path)
        {
            if (path == null)
                throw new ArgumentNullException(nameof(path));
            return Task.FromResult(_files.ContainsKey(path));
        }

        public async Task CreateBinaryFileAsync(Path path, byte[] binaryContent)
        {
            if (path == null)
                throw new ArgumentNullException(nameof(path));
            if (binaryContent == null)
                throw new ArgumentNullException(nameof(binaryContent));
            if (!await DirectoryExistsAsync(path.GetParent()))
                throw new System.IO.DirectoryNotFoundException($"Can't create file {path} because parent directory doesn't exist.");
            if (await DirectoryExistsAsync(path))
                throw new UnauthorizedAccessException($"Can't create file {path} because a directory with same name already exists.");
            //in System.IO.File existing files are overwritten
            _files[path] = binaryContent;
            _fileCreationDateTimes[path] = _clock.Now;
        }

        public Task CreateBinaryFileAsync(Path path, string base64Content)
        {
            return CreateBinaryFileAsync(path, Convert.FromBase64String(base64Content));
        }

        public async Task AppendContentToFileAsync(Path contentPath, Path destinationPath)
        {
            var sourceBinaryContent = await ReadAsBinaryAsync(contentPath);
            var destinationBinaryContent = await ReadAsBinaryAsync(destinationPath);
            _files[destinationPath] = destinationBinaryContent.Concat(sourceBinaryContent).ToArray();
        }

        public Task<byte[]> ReadAsBinaryAsync(Path path)
        {
            if (path == null)
                throw new ArgumentNullException(nameof(path));
            if (!_files.ContainsKey(path))
                throw new System.IO.FileNotFoundException($"File {path} does not exist.");
            return Task.FromResult(_files[path]);
        }

        public async Task<string> ReadAsBase64Async(Path path)
        {
            if (path == null)
                throw new ArgumentNullException(nameof(path));
            var binaryContent = await ReadAsBinaryAsync(path);
            return Convert.ToBase64String(binaryContent);
        }

        public Task CreateTextFileAsync(Path path, string textContent)
        {
            return CreateBinaryFileAsync(path, Encoding.UTF8.GetBytes(textContent));
        }

        public async Task<string> ReadAsTextAsync(Path path)
        {
            var binaryContent = await ReadAsBinaryAsync(path);
            return Encoding.UTF8.GetString(binaryContent);
        }

        public Lazy<System.IO.Stream> GetReadStream(Path path)
        {
            if (path == null)
                throw new ArgumentNullException(nameof(path));
            if (!_files.ContainsKey(path))
                throw new System.IO.FileNotFoundException($"File {path} does not exist.");
            return new Lazy<System.IO.Stream>(() => new System.IO.MemoryStream(_files[path]));
        }

        public Task CreateDirectoryAsync(Path path)
        {
            _directories.Add(path);
            while (!path.IsEmpty())
            {
                path = path.GetParent();
                _directories.Add(path);
                _directoryCreationDateTimes[path] = _clock.Now;
            }
            return Task.CompletedTask;
        }

        public Task<bool> DirectoryExistsAsync(Path path)
        {
            return Task.FromResult(_directories.Any(x => x.Equals(path, _isCaseSensitive)));
        }

        public Task<bool> DeleteFileAsync(Path path)
        {
            var file = _files.Keys.FirstOrDefault(x => x.Equals(path, _isCaseSensitive));
            if(file != null)
            {
                Task.FromResult(_files.Remove(path));
            }
            return Task.FromResult(false);
        }

        public Task<IEnumerable<Path>> ListFilesAsync(Path path)
        {
            if (_directories.Contains(path))
            {
                var result = _files.Where(x => x.Key.GetParent().Equals(path, _isCaseSensitive)).Select(x => x.Key);
                return Task.FromResult(result);
            }
            throw new InvalidOperationException($"Path {path} does not exists.");
        }

        public Task<IEnumerable<Path>> ListDirectoriesAsync(Path path)
        {
            var result = _directories.Where(x => !x.IsEmpty() && x.GetParent().Equals(path, _isCaseSensitive));
            return Task.FromResult(result);
        }

        public Task<DateTime> GetCreationDateTimeAsync(Path path)
        {
            if (_files.ContainsKey(path))
                return Task.FromResult(_fileCreationDateTimes[path]);
            if (_directories.Contains(path))
                return Task.FromResult(_directoryCreationDateTimes[path]);
            throw new InvalidOperationException($"Path {path} does not exists.");
        }

        public Path GetUniqueTempPath()
        {
            var uniqueDirectoryName = System.IO.Path.GetRandomFileName().Replace(".", string.Empty);
            return TempPath.Append(uniqueDirectoryName);
        }

        public async Task MoveFileAsync(Path sourceFile, Path destinationFile)
        {
            if (sourceFile == destinationFile)
                throw new ArgumentException($"{nameof(sourceFile)} and {nameof(destinationFile)} must be different.");
            var fileContent = _files[sourceFile];
            _files.Remove(sourceFile);
            _fileCreationDateTimes.Remove(sourceFile);
            if (!await DirectoryExistsAsync(destinationFile.GetParent()))//TODO TECH voir si l'api File de Microsoft fonctionne pareil
                throw new System.IO.DirectoryNotFoundException($"Can't create file {destinationFile} because parent directory doesn't exist.");
            _files[destinationFile] = fileContent;
            _fileCreationDateTimes[destinationFile] = _clock.Now;
        }

        public Task DeleteDirectoryAsync(Path folderPath)
        {
            _directories.Remove(folderPath);
            _directoryCreationDateTimes.Remove(folderPath);
            return Task.CompletedTask;
        }

        public Task<string> ComputeChecksumAsync(Path path)
        {
            if(_files.ContainsKey(path))
            {
                var md5 = MD5.Create().ComputeHash(_files[path]);
                return Task.FromResult(md5.ToBase64());
            }
            throw new InvalidOperationException($"Path {path} does not exists or is not a file.");
        }

        public string GetAbsolutePath(Path path)
        {
            return path;
        }
    }
}
