﻿using CocoriCore.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CocoriCore.FileSystem
{
    public interface IFileSystem
    {
        Path TempPath { get; }

        Task<bool> FileExistsAsync(Path path);

        Task CreateBinaryFileAsync(Path path, byte[] binaryContent);

        Task CreateBinaryFileAsync(Path path, string base64Content);

        Task CreateTextFileAsync(Path path, string textContent);

        Task AppendContentToFileAsync(Path contentPath, Path destinationPath);

        Task<bool> DeleteFileAsync(Path path);

        Task<byte[]> ReadAsBinaryAsync(Path path);

        Task<string> ReadAsBase64Async(Path path);

        Task<string> ReadAsTextAsync(Path path);

        Lazy<System.IO.Stream> GetReadStream(Path path);

        Task CreateDirectoryAsync(Path path);

        Task<bool> DirectoryExistsAsync(Path path);

        Task<IEnumerable<Path>> ListFilesAsync(Path path);

        Task<IEnumerable<Path>> ListDirectoriesAsync(Path path);

        Task<DateTime> GetCreationDateTimeAsync(Path path);

        Path GetUniqueTempPath();
        Task MoveFileAsync(Path path, Path destinationFile);
        Task DeleteDirectoryAsync(Path folderPath);
        string GetAbsolutePath(Path path);
        Task<string> ComputeChecksumAsync(Path path);
    }
}
