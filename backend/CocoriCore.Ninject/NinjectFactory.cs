using System;
using CocoriCore.Common;
using Ninject;
using Ninject.Syntax;

namespace CocoriCore.Ninject
{
    public class NinjectFactory : IFactory
    {
        private readonly IResolutionRoot kernel;

        public NinjectFactory(IResolutionRoot kernel)
        {
            this.kernel = kernel;
        }

        public T Create<T>()
        {
            return kernel.Get<T>();
        }

        public object Create(Type type)
        {
            return kernel.Get(type);
        }
    }
}