﻿using CocoriCore.Common;
using System;

namespace CocoriCore.Regularization
{
    public class Regularization : IEntity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime AppliedAt { get; set; }
        public string Version { get; set; }
    }
}
