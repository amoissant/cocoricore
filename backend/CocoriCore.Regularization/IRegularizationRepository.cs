﻿using CocoriCore.Common;
using System.Linq;
using System.Threading.Tasks;

namespace CocoriCore.Regularization
{
    public interface IRegularizationRepository
    {
        IQueryable<TEntity> Query<TEntity>() where TEntity : class, IEntity;
        Task InsertAsync(IEntity entity);
        Task CommitAsync();
    }
}
