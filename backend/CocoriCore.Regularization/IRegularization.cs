﻿using System;
using System.Threading.Tasks;

namespace CocoriCore.Regularization
{
    public interface IRegularizationHandler
    {
        Task ExecuteAsync();
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class RegularizationAttribute : Attribute
    {
        public string Name { get; }

        public RegularizationAttribute(string name)
        {
            Name = name;
        }

        public RegularizationAttribute()
        {
        }
    }
}
