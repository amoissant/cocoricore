﻿using CocoriCore.Common;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using CocoriCore.Linq.Async;
using CocoriCore.Clock;

namespace CocoriCore.Regularization
{
    public class RegularizationRunner
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IFactory _factory;
        private readonly IClock _clock;
        private readonly string _version;
        private readonly Assembly _regularizationHandlersAssembly;
        private readonly ILogger _logger;

        public RegularizationRunner(IUnitOfWorkFactory unitOfWorkFactory, IFactory factory, IClock clock, 
            ILogger logger, string version, Assembly regularizationHandlersAssembly)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _factory = factory;
            _clock = clock;
            _version = version;
            _regularizationHandlersAssembly = regularizationHandlersAssembly;
            _logger = logger;
        }

        public async Task ApplyRegularizationsAsync()
        {
            using (var unitOfWork = _unitOfWorkFactory.NewUnitOfWork())
            {
                var repository = unitOfWork.Resolve<IRegularizationRepository>();
                var availableHandlerTypes = FindRegularizationHandlers(_regularizationHandlersAssembly);
                var notAppliedRegularizationNames = await GetNotAppliedRegularizationsNamesAsync(repository, availableHandlerTypes);
                await ApplyRegularizationsAsync(repository, availableHandlerTypes, notAppliedRegularizationNames);
            }
        }

        private IEnumerable<Type> FindRegularizationHandlers(Assembly assembly)
        {
            return assembly
                .GetTypes()
                .Where(x => x.GetCustomAttribute<RegularizationAttribute>() != null)
                .ToArray();
        }

        private async Task<IEnumerable<string>> GetNotAppliedRegularizationsNamesAsync(IRegularizationRepository repository, 
            IEnumerable<Type> availableRegularizationType)
        {
            var availableRegularizationNames = availableRegularizationType
                .Select(x => GetRegularizationName(x));
            var appliedRegularizationNames = await repository
                .Query<Regularization>()
                .Select(x => x.Name)
                .ToArrayAsync();
            var notAppliedRegularizationNames = availableRegularizationNames
                .Except(appliedRegularizationNames)
                .OrderBy(x => x);
            return notAppliedRegularizationNames;
        }

        private string GetRegularizationName(Type type)
        {
            return type.GetCustomAttribute<RegularizationAttribute>().Name ?? type.Name;
        }

        private async Task ApplyRegularizationsAsync(IRegularizationRepository repository, 
            IEnumerable<Type> availableHandlerTypes, IEnumerable<string> notAppliedRegularizationNames)
        {
            foreach (var name in notAppliedRegularizationNames)
            {
                _logger.LogInformation($"Start regularization {name}");
                var handlerType = availableHandlerTypes.Single(x => GetRegularizationName(x) == name);
                await RunRegularizationAsync(handlerType);
                await InsertRegularizationAsync(repository, name);
                _logger.LogInformation($"Regularization {name} ended.");
                //TODO pouvoir lancer des regul en asynchrone et insérer la ligne en bdd que si elle a bien abouti
            }
        }

        private async Task RunRegularizationAsync(Type handlerType)
        {
            using (var unitOfWork = _unitOfWorkFactory.NewUnitOfWork())
            {
                var handler = (IRegularizationHandler)unitOfWork.Resolve(handlerType);
                await handler.ExecuteAsync();
            }
        }

        private async Task InsertRegularizationAsync(IRegularizationRepository repository, string name)
        {
            var regularization = _factory.Create<Regularization>();
            regularization.Name = name;
            regularization.AppliedAt = _clock.Now;
            regularization.Version = _version;
            await repository.InsertAsync(regularization);
            await repository.CommitAsync();
        }
    }
}
