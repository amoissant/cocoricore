﻿using CocoriCore.Common;
using CocoriCore.Repository;
using System.Linq;
using System.Threading.Tasks;

namespace CocoriCore.Regularization
{
    public class RegularizationRepository : IRegularizationRepository
    {
        private IBaseRepository _baseRepository;
        private ITransactionHolder _transactionHolder;

        public RegularizationRepository(IBaseRepository baseRepository, ITransactionHolder transactionHolder)
        {
            _baseRepository = baseRepository;
            _transactionHolder = transactionHolder;
        }

        public IQueryable<TEntity> Query<TEntity>() where TEntity : class, IEntity
        {
            return _baseRepository.Query<TEntity>();
        }

        public Task InsertAsync(IEntity entity)
        {
            return _baseRepository.InsertAsync(entity);
        }

        public Task CommitAsync()
        {
            return _transactionHolder.CommitAsync();
        }
    }
}
