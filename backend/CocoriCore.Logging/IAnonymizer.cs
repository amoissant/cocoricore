﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace CocoriCore.Logging
{
    public interface IAnonymizer
    {
        void Anonymize(object obj, ISet<MemberInfo> membersToAnonymize, IEnumerable<Type> finalTypes = null, int maxDeep = 50);
        object GetAnonymizedCopy(object obj, ISet<MemberInfo> membersToAnonymize, IEnumerable<Type> finalTypes = null, int maxDeep = 50);
    }
}