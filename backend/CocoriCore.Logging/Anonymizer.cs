﻿using CocoriCore.Dynamic.Extension;
using CocoriCore.Reflection.Extension;
using CocoriCore.Types.Extension;
using Microsoft.Extensions.Logging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CocoriCore.Logging
{
    public class Anonymizer : IAnonymizer
    {
        private readonly ILogger _logger;

        public Anonymizer(ILogger logger)
        {
            _logger = logger;
        }

        public object GetAnonymizedCopy(object obj, ISet<MemberInfo> membersToAnonymize, 
            IEnumerable<Type> finalTypes = null, int maxDeep = 50)
        {
            finalTypes = finalTypes ?? DynamicUtils.FINAL_TYPES;
            var objectType = obj?.GetType() ?? typeof(object);
            var bytes = Utf8Json.JsonSerializer.NonGeneric.Serialize(objectType, obj);
            var copy = Utf8Json.JsonSerializer.NonGeneric.Deserialize(objectType, bytes);
            Anonymize(copy, membersToAnonymize, finalTypes, maxDeep);
            return copy;
        }

        public void Anonymize(object obj, ISet<MemberInfo> membersToAnonymize,
            IEnumerable<Type> finalTypes = null, int maxDeep = 50)
        {
            finalTypes = finalTypes ?? DynamicUtils.FINAL_TYPES;
            Anonymize(obj, membersToAnonymize, finalTypes, 0, maxDeep, $"{obj?.GetType()}");
        }

        private void Anonymize(object obj, ISet<MemberInfo> membersToAnonymize, 
            IEnumerable<Type> finalTypes, int currentDeep, int maxDeep, string path)
        {
            if (currentDeep >= maxDeep)
            {
                throw new InvalidOperationException($"Max deep ({maxDeep}) reached while anonymizing message, "
                    + $"you can increase this value manually using optional argument '{nameof(maxDeep)}'.\n"
                    + $"Path : {path}.");
            }

            else if (obj == null) return;

            else if (finalTypes.Contains(obj.GetType()) || obj.GetType().IsEnum) return;

            else if (obj is IEnumerable enumerable)
            {
                var elements = enumerable.Cast<object>().ToArray();
                for (var i = 0; i < elements.Length; i++)
                {
                    var nextDeep = currentDeep + 1;
                    var nextPath = $"{path}[{i}]";
                    Anonymize(elements[i], membersToAnonymize, finalTypes, nextDeep, maxDeep, nextPath);
                }
            }
            else
            {
                foreach (var member in obj.GetType().GetPropertiesAndFields())
                {
                    if (membersToAnonymize.Contains(member))
                    {
                        AnonymizeValue(obj, member);
                    }
                    else
                    {
                        var memberValue = member.InvokeGetter(obj);
                        var nextDeep = currentDeep + 1;
                        var nextPath = $"{path}.{member.Name}";
                        Anonymize(memberValue, membersToAnonymize, finalTypes, nextDeep, maxDeep, nextPath);
                    }
                }
            }
        }

        private void AnonymizeValue(object obj, MemberInfo member)
        {
            var memberType = member.GetMemberType();
            if (memberType == typeof(string))
            {
                member.InvokeSetter(obj, "****");
            }
            else
            {
                _logger.LogWarning($"Anonymization not available for {memberType.DeclaringType}.{memberType.Name}, " +
                    "currently only string fields are supported.");
            }
        }

    }
}