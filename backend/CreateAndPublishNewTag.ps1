git push
git pull

$(git tag | select -last 1) -match '(?<major>\d)\.(?<minor>\d)\.(?<patch>\d+)'
$newTag = "$($matches["major"]).$($matches["minor"]).$([int]($matches["patch"])+1)"
git tag -a $newTag -m "tag $($newTag)"
git push origin --tags

$lastVersion = "0.0.0-last"
$localPackageDirectory = "C:\Users\antho\.nuget\local\"
$localNugetCache = "C:\Users\antho\.nuget\packages\"

function Replace-Version ($a, $b) {
	Get-ChildItem -Path "." -Recurse -include "*.csproj" -exclude "*Test" |
	ForEach-Object {
		(Get-Content $_) |
		ForEach-Object {
			if($_ -match "<Version>" + $a + "</Version>")
			{
				$_ | % { $_ -replace $a, $b }
			}
			else
			{
				$_
			}
		} |
		Set-Content -Path $_.FullName
	}
}

Replace-Version $lastVersion $newTag 

dotnet pack --output $localPackageDirectory

Replace-Version $newTag $lastVersion 

get-childitem -Path ($localNugetCache+"cocoricore*") -Attributes Directory | Remove-Item -Force -Recurse




