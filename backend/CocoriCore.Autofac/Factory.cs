using Autofac;
using CocoriCore.Common;
using System;

namespace CocoriCore.Autofac
{
    public class Factory : IFactory
    {
        protected ILifetimeScope _scope;
        protected Func<Guid> _idGenerationFunc;

        public Factory(ILifetimeScope scope, Func<Guid> idGenerationFunc = null)
        {
            _scope = scope;
            _idGenerationFunc = idGenerationFunc ?? Guid.NewGuid;
        }

        public virtual T Create<T>()
        {
            T instance;
            if (_scope.IsRegistered<T>())
            {
                instance = _scope.Resolve<T>();
            }
            else
            {
                instance = Activator.CreateInstance<T>();
            }
            if (instance is IEntity entity)
            {
                entity.Id = _idGenerationFunc();
            }
            return instance;
        }

        public virtual object Create(Type type)
        {
            object instance;
            if (_scope.IsRegistered(type))
            {
                instance = _scope.Resolve(type);
            }
            else
            {
                instance = Activator.CreateInstance(type);
            }
            if (instance is IEntity entity)
            {
                entity.Id = _idGenerationFunc();
            }
            return instance;
        }
    }
}
