﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Autofac;
using Autofac.Builder;
using Autofac.Core;
using Moq;

namespace CocoriCore.Autofac
{
    public class MockRegistrationSource : IRegistrationSource
    {
        public bool IsAdapterForIndividualComponents => false;

        public IEnumerable<IComponentRegistration> RegistrationsFor(Service service, Func<Service, IEnumerable<ServiceRegistration>> registrationAccessor)
        {
            if ((service is TypedService typedService) &&
                typedService.ServiceType.IsInterface &&
                !IsIEnumerable(typedService) &&
                !IsInsideAutofac(typedService) &&
                !registrationAccessor(service).Any())
            {
                return new IComponentRegistration[]
                {
                    RegistrationBuilder.ForDelegate((c, p) => this.CreateMock(c, typedService))
                        .As(service)
                        .SingleInstance()
                        .ExternallyOwned()
                        .CreateRegistration()
                };
            }
            return Enumerable.Empty<IComponentRegistration>();
        }

        private object CreateMock(IComponentContext c, TypedService typedService)
        {
            var mockType = typeof(Mock<>);
            var typedMockType = mockType.MakeGenericType(typedService.ServiceType);
            var mock = (Mock)Activator.CreateInstance(typedMockType);
            return mock.Object;
        }

        private bool IsInsideAutofac(IServiceWithType typedService)
        {
            return typeof(IRegistrationSource).Assembly == typedService.ServiceType.Assembly;
        }

        private bool IsIEnumerable(IServiceWithType typedService)
        {
            return typedService.ServiceType.GetTypeInfo().IsGenericType &&
                   typedService.ServiceType.GetTypeInfo().GetGenericTypeDefinition() == typeof(IEnumerable<>);
        }
    }
}