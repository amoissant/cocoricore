using Autofac;
using Autofac.Core;
using System;

namespace CocoriCore.Autofac
{
    public class TypedParameter<T> : ResolvedParameter
    {
        public TypedParameter(T value)
            : base((i, c) => i.ParameterType == typeof(T), (i, c) => value)
        {
        }

        public TypedParameter(Func<IComponentContext, T> expression)
            : base((i, c) => i.ParameterType == typeof(T), (i, c) => expression(c))
        {
        }

        public TypedParameter(Type serviceType)
            : base((i, c) => i.ParameterType == typeof(T), (i, c) => c.Resolve(serviceType))
        {
        }
    }
}