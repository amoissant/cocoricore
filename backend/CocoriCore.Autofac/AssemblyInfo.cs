﻿using System.Reflection;

namespace CocoriCore.Autofac
{
    public static class AssemblyInfo
    {
        public static Assembly Assembly => typeof(AssemblyInfo).Assembly;
    }
}
