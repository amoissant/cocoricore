using System;
using Autofac;
using CocoriCore.Common;

namespace CocoriCore.Autofac
{
    public class AutofacUnitOfWork : UnitOfWorkBase
    {
        protected ILifetimeScope _scope;

        public AutofacUnitOfWork(UnitOfWorkOptions options, ILifetimeScope scope)
            : base(options)
        {
            _scope = scope;
        }

        public override T Resolve<T>()
        {
            return (T)Resolve(typeof(T));
        }

        public override object Resolve(Type type)
        {
            return _scope.Resolve(type);
        }

        public override void Dispose()
        {
            _scope.Dispose();
        }
    }
}