﻿using Autofac;
using Autofac.Core;
using Autofac.Core.Registration;
using Autofac.Core.Resolving.Pipeline;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace CocoriCore.Autofac
{
    public class LoggerModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            builder.Register((c, p) =>
            {
                var typeParameter = p.OfType<TypedParameter>().FirstOrDefault();
                var entryNamespace = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
                var loggerName = $"{entryNamespace}.DefaultLogger";
                if (typeParameter != null && typeParameter.Value is Type typeValue)
                {
                    loggerName = typeValue.FullName;
                }
                return c.Resolve<ILoggerFactory>().CreateLogger(loggerName);
            }).As<ILogger>();
        }

        protected override void AttachToComponentRegistration(
            IComponentRegistryBuilder registry, IComponentRegistration registration)
        {
            registration.PipelineBuilding += (sender, pipeline) =>
            {
                pipeline.Use(new LoggingMiddleware());
            };
        }
    }

    public class LoggingMiddleware : IResolveMiddleware
    {
        public PipelinePhase Phase => PipelinePhase.ParameterSelection;

        public void Execute(ResolveRequestContext context, Action<ResolveRequestContext> next)
        {
            context.ChangeParameters(context.Parameters.Union(
                new[] {
                    new ResolvedParameter(
                        (p, c) => p.ParameterType == typeof(ILogger),
                        (p, c) => c.Resolve<ILogger>(TypedParameter.From(p.Member.DeclaringType))
                ),
            }));
            next(context);
        }
    }
}