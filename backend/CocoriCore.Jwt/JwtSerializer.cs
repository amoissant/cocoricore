using System;
using System.Reflection;
using CocoriCore.Common;
using CocoriCore.JsonNet;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace CocoriCore.Jwt
{
    public class JwtSerializer : JsonSerializer, IJwtSerializer
    {
        public JwtSerializer(string expirationDateMember)
        {
            ContractResolver = new JwtContractResolver(expirationDateMember);
            Converters.Add(new DynamicObjectConverter());
        }

        public object Deserialize(string jsonPayload, Type type)
        {
            return JsonSerializerExtension.Deserialize(this, jsonPayload, type);
        }

        public string Serialize(object payload)
        {
            return JsonSerializerExtension.Serialize(this, payload);
        }
    }

    public class JwtContractResolver : CamelCasePropertyNamesContractResolver
    {
        private readonly string _expirationDateMember;

        public JwtContractResolver(string expirationDateMember)
        {
            _expirationDateMember = expirationDateMember;
        }

        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            var jsonProperty = base.CreateProperty(member, memberSerialization);
            if (member.Name == _expirationDateMember)
            {
                jsonProperty.PropertyName = "exp";
                jsonProperty.Converter = new UnixDateTimeConverter();
            }
            return jsonProperty;
        }
    }

    public class GuidConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(Guid);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var base64Guid = (string)reader.Value;
            return new Guid(Convert.FromBase64String(base64Guid));
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var guid = (Guid)value;
            var guidBytes = guid.ToByteArray();
            writer.WriteValue(Convert.ToBase64String(guidBytes));
        }
    }

    public class UnixDateTimeConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(DateTime);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return DateTimeOffset.FromUnixTimeSeconds((long)reader.Value).LocalDateTime;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var dateTime = (DateTime)value;
            writer.WriteValue(dateTime.ToUnixTimeSeconds());
        }
    }
}