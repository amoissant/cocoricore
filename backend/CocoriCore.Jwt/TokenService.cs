﻿
using CocoriCore.Clock;
using Jose;
using System;
using System.Linq;
using System.Text;

namespace CocoriCore.Jwt
{
    public class TokenService : ITokenService
    {
        private readonly IClock _clock;
        private readonly TokenConfiguration _configuration;
        private readonly IJwtSerializer _jsonSerializer;

        public TokenService(IClock clock, TokenConfiguration configuration, IJwtSerializer jsonSerializer)
        {
            _clock = clock;
            _configuration = configuration;
            _jsonSerializer = jsonSerializer;
        }

        public virtual string GenerateToken<T>(T payload, string extraSalt = null)
        {
            var key = _configuration.SecretBytes;
            if (extraSalt != null)
            {
                key = key.Concat(Encoding.UTF8.GetBytes(extraSalt)).ToArray();
            }
            var token = JWT.Encode(_jsonSerializer.Serialize(payload), key, _configuration.Algorithm);
            return token;
        }

        public string CheckAuthenticityAndExpiration(string token, string salt = null)
        {
            CheckAuthenticity(token, salt);
            CheckExpiration(token);
            return token;
        }

        public void CheckAuthenticity(string token, string salt = null)
        {
            var key = _configuration.SecretBytes;
            if (salt != null)
            {
                key = key.Concat(Encoding.UTF8.GetBytes(salt)).ToArray();
            }
            var algorithm = _configuration.Algorithm;
            try
            {
                JWT.Decode(token, key, algorithm);
            }
            catch (Exception e)
            {
                var exception = new InvalidTokenException("Can't decode token.", e);
                exception.Data["Token"] = token;
                throw exception;
            }
        }

        public void CheckExpiration(string token)
        {
            if (IsExpired(token))
            {
                throw new InvalidTokenException("Token is expired.");
            }
        }

        public bool IsExpired(string token)
        {
            var jsonPayload = JWT.Payload(token);
            dynamic payload = _jsonSerializer.Deserialize(jsonPayload, typeof(object));
            return payload.exp <= ((DateTimeOffset)_clock.Now).ToUnixTimeSeconds();
        }

        public bool IsAuthenticAndNotExpired(string token, string salt = null)
        {
            return IsAuthentic(token, salt) && !IsExpired(token);
        }

        public bool IsAuthentic(string token, string salt = null)
        {
            try
            {
                CheckAuthenticity(token, salt);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public T GetPayload<T>(string token)
        {
            return (T)_jsonSerializer.Deserialize(JWT.Payload(token), typeof(T));
        }

        public object GetPayload(string token, Type type)
        {
            return _jsonSerializer.Deserialize(JWT.Payload(token), type);
        }
    }
}
