using System;
using System.Collections.Generic;
using System.Text;
using CocoriCore.Common;
using Jose;

namespace CocoriCore.Jwt
{
    public class TokenConfiguration
    {
        private readonly Dictionary<string, TimeSpan> _tokenDurations;
        private byte[] _secretBytes;
        private JwsAlgorithm _algorithm;

        public TokenConfiguration()
        {
            _tokenDurations = new Dictionary<string, TimeSpan>();
        }

        public byte[] SecretBytes
        {
            get
            {
                if(_secretBytes == null)
                {
                    throw new ConfigurationException($"No JWT secret configured, use configuration.{nameof(UseSecret)}(<your_secret>) to define one.");
                }
                return _secretBytes;
            }
        }
        public JwsAlgorithm Algorithm => _algorithm;

        public TimeSpan GetTokenDuration(object tokenKey)
        {
            var key = tokenKey?.ToString();
            if(_tokenDurations.TryGetValue(key, out var duration))
            {
                return duration;
            }
            else
            {
                throw new ConfigurationException($"No duration found for key '{key}', " + 
                    $"you can define one using {nameof(TokenConfiguration)}.{nameof(SetTokenDuration)}({key}, <duration value>).");
            }
        }

        public TokenConfiguration SetTokenDuration(object tokenKey, TimeSpan duration)
        {
            var key = tokenKey?.ToString();
            _tokenDurations[key] = duration;
            return this;
        }

        public TokenConfiguration UseSecret(string secret)
        {
            _secretBytes = Encoding.UTF8.GetBytes(secret);
            return this;
        }

        public TokenConfiguration UseAlgorithm(JwsAlgorithm algorithm)
        {
            _algorithm = algorithm;
            return this;
        }
    }
}