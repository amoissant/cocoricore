﻿using System;

namespace CocoriCore.Jwt
{
    public interface IJwtSerializer
    {
        string Serialize(object payload);
        object Deserialize(string jsonPayload, Type type);
    }
}
