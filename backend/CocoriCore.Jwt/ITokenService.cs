﻿
using System;

namespace CocoriCore.Jwt
{
    public interface ITokenService
    {
        string CheckAuthenticityAndExpiration(string token, string salt = null);
        void CheckAuthenticity(string token, string salt = null);
        void CheckExpiration(string token);
        string GenerateToken<T>(T payload, string extraSalt = null);
        bool IsAuthenticAndNotExpired(string token, string salt = null);
        bool IsAuthentic(string token, string salt = null);
        bool IsExpired(string token);
        T GetPayload<T>(string token);
        object GetPayload(string token, Type type);
    }
}