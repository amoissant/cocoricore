﻿using System.Collections.Generic;

namespace CocoriCore.Router
{
    public interface IFootprintGenerator
    {
        ApiFootprint Generate(IEnumerable<RouteDescriptor> descriptors, int maxDeep = 1);
    }
}