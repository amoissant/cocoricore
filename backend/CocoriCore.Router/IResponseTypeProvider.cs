﻿using System;

namespace CocoriCore.Router
{
    public interface IResponseTypeProvider
    {
        Type GetResponseType(Type messageType);
    }
}