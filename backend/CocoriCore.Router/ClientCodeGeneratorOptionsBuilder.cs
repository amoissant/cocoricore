using System;
using System.Collections.Generic;
using System.Linq;

namespace CocoriCore.Router
{
    public class ClientCodeGeneratorOptionsBuilder
    {
        public ClientCodeGeneratorOptions Options { get; }

        public ClientCodeGeneratorOptionsBuilder()
        {
            Options = new ClientCodeGeneratorOptions();
        }

        public ClientCodeGeneratorOptionsBuilder AddClientTypeCorrespondence(Type type, string typeStr)
        {
            Options.ClientTypeCorrespondences.Add(type, typeStr);
            return this;
        }

        public ClientCodeGeneratorOptionsBuilder TransformClientTypes(Func<string, string> transformation)
        {
            Options.TransformClientType = transformation;
            return this;
        }

        public ClientCodeGeneratorOptionsBuilder AddFieldTypeCorrespondence(Type type, string typeStr)
        {
            Options.FieldTypeCorrespondences.Add(type, typeStr);
            return this;
        }

        public ClientCodeGeneratorOptionsBuilder TransformFieldTypes(Func<string, string> transformation)
        {
            Options.TransformFieldType = transformation;
            return this;
        }

        public ClientCodeGeneratorOptionsBuilder TransformFieldNames(Func<string, string> transformation)
        {
            Options.TransformFieldName = transformation;
            return this;
        }

        public ClientCodeGeneratorOptionsBuilder AddConditionalImport(Func<RouteDescriptor, Type, bool> condition, Func<RouteDescriptor, Type, object> createImport)
        {
            Options.ConditionalImports.Add(new ConditionnalImport(condition, createImport));
            return this;
        }

        public ClientCodeGeneratorOptionsBuilder ExcludeRouteWhen(Func<RouteDescriptor, bool> condition)
        {
            Options.ExcludeRoute = condition;
            return this;
        }

        public ClientCodeGeneratorOptionsBuilder SetFinalTypes(IEnumerable<Type> finalTypes)
        {
            Options.FinalTypes = finalTypes.ToList();
            return this;
        }

        public ClientCodeGeneratorOptionsBuilder AddFinalTypes(params Type[] finaleTypes)
        {
            Options.FinalTypes.AddRange(finaleTypes);
            return this;
        }

        public ClientCodeGeneratorOptionsBuilder RenderPathParameter(Func<string, string, string> render)
        {
            Options.RenderPathParameter = render;
            return this;
        }

        public ClientCodeGeneratorOptionsBuilder UseMessageTemplate(Func<Type, string> getTemplate)
        {
            Options.GetMessageTemplate = getTemplate;
            return this;
        }

        public ClientCodeGeneratorOptionsBuilder UseResponseTemplate(Func<Type, string> getTemplate)
        {
            Options.GetResponseTemplate = getTemplate;
            return this;
        }
    }
}