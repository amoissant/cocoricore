using CocoriCore.Common;
using System;

namespace CocoriCore.Router
{
    public class ClientType
    {
        public Type ClrType;
        public string Name;
        public string Method;
        public string Url;
        public object[] Imports;
        public Field[] Fields;
        public Field[] QueryStringFields;
        public Field[] BodyFields;
        public EnumValue[] Values;
        public bool IsEnum;
        public bool IsArray;
        public ClientType[] InnerTypes;
        public string ParameterizedUrl;
        public Path RoutePath;
        public string Template;
    }
}