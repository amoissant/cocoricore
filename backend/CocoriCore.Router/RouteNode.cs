﻿using System.Collections.Generic;
using System.Linq;

namespace CocoriCore.Router
{
    public class RouteNode
    {
        public IRouteSegment Segment { get; }
        public List<Route> Routes { get; }
        public Dictionary<IRouteSegment, RouteNode> SubRoutes { get; }

        public RouteNode(IRouteSegment segment)
        {
            Segment = segment;
            Routes = new List<Route>();
            SubRoutes = new Dictionary<IRouteSegment, RouteNode>();
        }

        protected void AddRoute(Route route, Queue<IRouteSegment> segments)
        {
            if (segments.Count == 0)
            {
                Routes.Add(route);
            }
            else
            {
                var segment = segments.Dequeue();
                if (!SubRoutes.ContainsKey(segment))
                {
                    SubRoutes[segment] = new RouteNode(segment);
                }
                SubRoutes[segment].AddRoute(route, segments);
            }
        }

        public IEnumerable<Route> FindRoutes(Queue<string> segments)
        {
            if (segments.Count == 0)
            {
                return Routes;
            }
            else
            {
                var segment = segments.Dequeue();
                return SubRoutes
                    .Where(x => x.Value.Segment.Match(segment))
                    .SelectMany(x => x.Value.FindRoutes(new Queue<string>(segments)));
            }
        }
    }
}