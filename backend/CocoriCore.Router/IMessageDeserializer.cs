﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace CocoriCore.Router
{
    public interface IMessageDeserializer
    {
        Task<object> DeserializeMessageAsync(HttpRequest request, Route route);
        Task<TMessage> DeserializeMessageAsync<TMessage>(HttpRequest request, Route route);
        string GetFirstFieldNameFromBody(HttpRequest request);
    }
}