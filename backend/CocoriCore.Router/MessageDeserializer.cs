
using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CocoriCore.Router
{
    public class MessageDeserializer : IMessageDeserializer
    {
        private JsonSerializer _jsonSerializer;

        public MessageDeserializer(JsonSerializer jsonSerializer)
        {
            _jsonSerializer = jsonSerializer;
        }

        public string GetFirstFieldNameFromBody(HttpRequest request)
        {
            if (!request.Body.CanSeek)
            {
                //TODO à tester mais HttpFake a revoir pour pouvoir simuler une stream readOnly
                //et vérifier que cela ne pose pas de soucis avec le TracerMiddleware 
                //(a priori non car ce dernier passe en premier et renvoie 'false' à CanSeak() donc on fera quand meme une copie du stream ici)
                var requestBodyCopy = new MemoryStream();
                request.Body.CopyToAsync(requestBodyCopy);//TODO plutôt que tout copier on pourrait faire uen proxy qui lit le début (préalablement copié) puis de lire directement le requestBody
                request.Body = requestBodyCopy;
                request.Body.Position = 0;
            }

            string firstFieldName = null;
            using (var sr = new StreamReader(request.Body, Encoding.UTF8, true, 1024, true))
            using (var jtr = new JsonTextReader(sr))
            {
                while (jtr.Read() && jtr.TokenType != JsonToken.PropertyName)
                {
                    ;
                }
                if (jtr.TokenType == JsonToken.PropertyName)
                {
                    firstFieldName = (string)jtr.Value;
                }
            }
            request.Body.Position = 0;
            return firstFieldName;
        }

        public virtual async Task<TMessage> DeserializeMessageAsync<TMessage>(HttpRequest request, Route route)
        {
            var message = await DeserializeMessageAsync(request, route);
            return (TMessage)message;
        }

        public virtual async Task<object> DeserializeMessageAsync(HttpRequest request, Route route)
        {
            try
            {
                var jToken = await InitJsonTokenAsync(request, route);
                if (jToken is JObject jObject)
                {
                    AddValuesFromQuery(jObject, request, route);
                    AddValuesFromHeader(jObject, request, route);
                    AddValuesFromCookies(jObject, request, route);
                    AddValuesFromUrl(jObject, request, route);
                }
                return jToken.ToObject(route.MessageType, _jsonSerializer);
            }
            catch (Exception e)
            {
                throw new DeserializeMessageException("Cant' deserialize message.", e);
            }
        }

        protected virtual async Task<JToken> InitJsonTokenAsync(HttpRequest request, Route route)
        {
            JToken result = null;
            if (route.HasBodyParameters)
            {
                using (var sr = new StreamReader(request.Body, Encoding.UTF8, true, 1024, true))
                using (var jtr = new JsonTextReader(sr))
                {
                    try
                    {
                        result = await JToken.LoadAsync(jtr);
                    }
                    catch (Exception)
                    {
                        if (jtr.LinePosition > 0)
                        {
                            throw;
                        }
                    }
                }
            }
            return result ?? new JObject();
        }


        protected virtual void AddValuesFromQuery(JObject jsonObject, HttpRequest request, Route route)
        {
            if (route.HasQueryParameters)
            {
                foreach (string key in request.Query.Keys)
                {
                    string value = request.Query[key];
                    var normalizedKey = key.StartsWith("$") ? key.Substring(1) : key;
                    if (key.EndsWith("[]"))
                    {
                        AddJsonArrayFromKey(jsonObject, normalizedKey, value);
                    }
                    else if (value.StartsWith("[") && value.EndsWith("]"))
                    {
                        AddJsonArrayFromValue(jsonObject, normalizedKey, value);
                    }
                    else
                    {
                        AddJsonValue(jsonObject, normalizedKey, value);
                    }
                }
            }
        }

        private void AddJsonArrayFromKey(JObject jsonObject, string key, string value)
        {
            var jarray = new JArray();
            var array = value.Split(',');
            foreach (var element in array)
            {
                jarray.Add(JToken.FromObject(element));
            }
            jsonObject[key.Substring(0, key.Length - 2)] = jarray;
        }

        private void AddJsonArrayFromValue(JObject jsonObject, string key, string value)
        {
            using (var sr = new StringReader(value))
            using (var jtr = new JsonTextReader(sr))
            {
                jsonObject[key] = _jsonSerializer.Deserialize<JArray>(jtr);
            }
        }

        private void AddJsonValue(JObject jsonObject, string key, string value)
        {
            jsonObject[key] = JToken.FromObject(value);
        }


        protected virtual void AddValuesFromHeader(JObject jsonObject, HttpRequest request, Route route)
        {
            foreach (var header in route.Headers)
            {
                StringValues headerValue;
                if (request.Headers.TryGetValue(header.Key, out headerValue))
                {
                    AddJsonValue(jsonObject, header.Value, headerValue);
                }
            }
        }

        protected virtual void AddValuesFromCookies(JObject jsonObject, HttpRequest request, Route route)
        {
            if (route.HasCookieParameters)
            {
                foreach (var cookie in request.Cookies)
                {
                    string value = cookie.Value;
                    if (value.StartsWith("[") && value.EndsWith("]"))
                    {
                        AddJsonArrayFromValue(jsonObject, cookie.Key, value);
                    }
                    else
                    {
                        AddJsonValue(jsonObject, cookie.Key, value);
                    }
                }
            }
        }

        private void AddValuesFromUrl(JObject jsonObject, HttpRequest request, Route route)
        {
            if (route.HasPathParameters)
            {
                Match match = route.GetMatch(request);
                foreach (Group group in match.Groups)
                {
                    if (route.HasUrlParameter(group.Name))
                    {
                        var decodedValue = HttpUtility.UrlDecode(group.Value);
                        var jtoken = JToken.FromObject(decodedValue);
                        jsonObject[group.Name] = jtoken;
                    }
                }
            }
        }
    }
}