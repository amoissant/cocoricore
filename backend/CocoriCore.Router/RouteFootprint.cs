using System.Collections.Generic;

namespace CocoriCore.Router
{

    public class ApiFootprint
    {
        public IEnumerable<RouteFootprint> Routes;
        public Dictionary<string, Dictionary<string, int>> Enums;
        public Dictionary<string, object> Types;
    }

    public class RouteFootprint
    {
        public string Url;
        public string Method;
        public string Description;
        public object QueryString;
        public object Response;
        public object Body;
    }
}