using CocoriCore.Common;
using System.Collections.Generic;
using System.Linq;

namespace CocoriCore.Router
{
    public static class RouteDescriptorExtension
    {
        public static IEnumerable<string> GetRoutePathSegments(this RouteDescriptor descriptor)
        {
            return descriptor.UrlSegments.ToRoutePath().Segments;
        }

        public static Path ToRoutePath(this UrlSegment[] segments)
        {
            return new Path(segments.Where(x => !x.IsParameter).Select(s => s.Value));
        }

        public static string GetUrlBeforeFirstParameter(this RouteDescriptor descriptor)
        {
            var segments = new List<string>();
            foreach (var segment in descriptor.UrlSegments)
            {
                if (segment.IsParameter)
                {
                    break;
                }
                else
                {
                    segments.Add(segment.Value);
                }
            }
            return $"/{string.Join("/", segments)}";
        }
    }
}