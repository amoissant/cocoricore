using System;

namespace CocoriCore.Router
{
    public class DuplicateRoutesException : Exception
    {
        public DuplicateRoutesException(string message)
            : base(message)
        {
        }
    }
}