using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CocoriCore.Collection.Extensions;
using CocoriCore.Reflection.Extension;
using CocoriCore.Types.Extension;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;

namespace CocoriCore.Router
{
    public class Router : IRouter
    {
        public const string HTTP_CONTEXT_ROUTE = "HTTP_CONTEXT_ROUTE";

        private readonly RouterOptions _options;
        private readonly IMessageDeserializer _messageDeserializer;
        private readonly IEnumerable<MethodRoutes> _routesSets;
        private readonly IResponseTypeProvider _responseTypeProvider;

        public Router(RouterOptions options, IMessageDeserializer messageDeserializer, IResponseTypeProvider responseTypeProvider)
        {
            _options = options;
            _messageDeserializer = messageDeserializer;
            _routesSets = _options
                .AllRoutes
                .GroupBy(x => x.Method)
                .Select(x => new MethodRoutes(x.Key).AddRoutes(x));
            _responseTypeProvider = responseTypeProvider;
        }

        public Type TryFindMessageType(HttpRequest request)
        {
            var route = TryFindRoute(request);
            request.HttpContext.Items[HTTP_CONTEXT_ROUTE] = route;
            return route?.MessageType;
        }

        private Route TryFindRoute(HttpRequest request)
        {
            var routes = FindRoutesMatchingMethodAndPath(request);
            if (routes.Any())
            {
                return GetUniqueMatchingRoute(request, routes);
            }
            return null;
        }

        private IEnumerable<Route> FindRoutesMatchingMethodAndPath(HttpRequest request)
        {
            return _routesSets
                .Where(x => x.Segment.Match(request.Method))
                .SelectMany(x => x.FindRoutes(request))
                .ToArray();
        }

        protected virtual Route GetUniqueMatchingRoute(HttpRequest request, IEnumerable<Route> routes)
        {
            if (routes.Count() == 1)
            {
                return routes.First();
            }
            //TODO revoir checkDuplicateRoute qui n'a pas d�tect� le doublon /channels/{id:string} vs /channels/program
            var exactMatch = routes.Where(x => x.ParameterizedUrl == request.Path).ToArray();
            if (exactMatch.Length == 1)
            {
                return exactMatch[0];
            }
            if (request.QueryString == QueryString.Empty &&
                routes.HasSingle(x => !x.HasQueryParameters, out var route))
            {
                return route;
            }
            if (request.QueryString != QueryString.Empty &&
                routes.HasSingle(x => x.HasQueryParameters, out route))
            {
                return route;
            }
            if(request.Method == HttpMethods.Patch)
            {
                var firstFieldName = _messageDeserializer.GetFirstFieldNameFromBody(request)?.ToLower();
                routes = routes
                    .Where(x => x.MessageType
                        .GetPropertiesAndFields()
                        .Any(m => m.Name.ToLower() == firstFieldName))
                    .ToArray();
                if(routes.Count() == 1)
                {
                    return routes.First();
                }
            }
            var errorMessage = $"Several routes match url {request.GetDisplayUrl()} : \n"
                + $"{string.Join("\n", routes.Select(x => "\t" + x.ToString()))}\n"
                + "Please review your configuration to avoid duplicate routes.\n"
                + $"Also method router.{nameof(CheckDuplicatesRoutes)}() can help you detect duplicate routes "
                + "when called at application startup or in automated test.";
            throw new DuplicateRoutesException(errorMessage);
        }

        public IEnumerable<Route> AllRoutes => _options.AllRoutes;

        public Task<TMessage> DeserializeMessageAsync<TMessage>(HttpRequest request)
        {
            var route = (Route)request.HttpContext.Items[HTTP_CONTEXT_ROUTE];
            return _messageDeserializer.DeserializeMessageAsync<TMessage>(request, route);
        }

        //TODO � lancer quand on construis les options au moment de la validation
        public void CheckDuplicatesRoutes()
        {
            Dictionary<int, Route> routeDictionary = new Dictionary<int, Route>();
            foreach (var route in AllRoutes)
            {
                var key = route.HashCode;
                if (routeDictionary.ContainsKey(key))
                {
                    throw new DuplicateRoutesException("At least two routes produce same pattern :\n"
                        + "\troute 1 : " + route + "\n"
                        + "\troute 2 : " + routeDictionary[key]);
                }
                else
                {
                    routeDictionary.Add(key, route);
                }
            }
        }

        public IEnumerable<RouteDescriptor> GetRouteDescriptors()
        {
            return AllRoutes.Select(x => new RouteDescriptor
            {
                UrlSegments = x.GetUrlSegments().ToArray(),
                ParameterizedUrl = x.ParameterizedUrl,
                UrlParameters = x.UrlParameters.ToDictionary(x => x.Name, x => x.GetMemberType()),
                Method = x.Method,
                MessageType = x.MessageType,
                UseBody = x.HasBodyParameters,
                UseQuery = x.HasQueryParameters,
                Description = x.Description,
                ResponseType = _responseTypeProvider.GetResponseType(x.MessageType),
            });
        }
    }
}