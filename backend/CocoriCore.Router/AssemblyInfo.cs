﻿using System.Reflection;

namespace CocoriCore.Router
{
    public static class AssemblyInfo
    {
        public static Assembly Assembly => typeof(AssemblyInfo).Assembly;
    }
}
