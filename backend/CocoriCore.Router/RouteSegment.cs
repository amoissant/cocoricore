using System;

namespace CocoriCore.Router
{
    public class UrlSegment
    {
        public string Value;
        public Type Type;
        public bool IsParameter;

        public UrlSegment(string value, bool isParameter, Type type = null)
        {
            Value = value;
            IsParameter = isParameter;
            Type = type;
        }
    }
}