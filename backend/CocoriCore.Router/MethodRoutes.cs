using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Microsoft.AspNetCore.Http;

namespace CocoriCore.Router
{
    public class MethodRoutes : RouteNode
    {
        public MethodRoutes(HttpMethod method)
            : base(new MethodRouteSegment(method))
        {
        }

        public MethodRoutes(IRouteSegment segment) : base(segment)
        {
        }

        public MethodRoutes AddRoutes(IEnumerable<Route> routes)
        {
            foreach (var route in routes)
            {
                var routeSegments = new Queue<IRouteSegment>(route.RouteSegments);
                AddRoute(route, routeSegments);
            }
            return this;
        }

        public IEnumerable<Route> FindRoutes(HttpRequest request)
        {
            var pathSegments = request
                .Path
                .Value
                .Split('/')
                .Where(x => !string.IsNullOrEmpty(x))
                .ToArray();
            var segments = new Queue<string>(pathSegments);
            return base.FindRoutes(segments);
        }
    }
}