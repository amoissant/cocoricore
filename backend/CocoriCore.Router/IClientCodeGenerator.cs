﻿using System.Collections.Generic;

namespace CocoriCore.Router
{
    public interface IClientCodeGenerator
    {
        IEnumerable<ClientRouteDescriptor> GenerateClientDescriptors(IEnumerable<RouteDescriptor> routeDescriptors);
    }
}