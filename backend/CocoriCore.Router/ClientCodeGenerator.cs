using CocoriCore.Collection.Extensions;
using CocoriCore.Reflection.Extension;
using CocoriCore.Types.Extension;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CocoriCore.Router
{
    public class ClientCodeGenerator : IClientCodeGenerator
    {
        private ClientCodeGeneratorOptions _options;

        public ClientCodeGenerator(ClientCodeGeneratorOptions options)
        {
            _options = options;
        }

        public virtual IEnumerable<ClientRouteDescriptor> GenerateClientDescriptors(IEnumerable<RouteDescriptor> routeDescriptors)
        {
            var clientRouteDescriptors = new List<ClientRouteDescriptor>();
            foreach (var descriptor in routeDescriptors)
            {
                if (_options.ExcludeRoute(descriptor))
                {
                    continue;
                }
                var clientDescriptor = ConstructClientRouteDescriptor(descriptor);
                clientRouteDescriptors.Add(clientDescriptor);
            }
            return clientRouteDescriptors;
        }

        protected virtual (Type type, bool isArray) UnboxAndGetIsArray(Type type)
        {
            var unboxedType = type.Unbox();
            if (_options.FinalTypes.Contains(unboxedType))
            {
                unboxedType = null;
            }
            var isArray = type.IsAssignableTo<IEnumerable>() && type != typeof(string);
            return (unboxedType, isArray);
        }

        protected virtual ClientRouteDescriptor ConstructClientRouteDescriptor(RouteDescriptor descriptor)
        {
            var clientDescriptor = new ClientRouteDescriptor();
            clientDescriptor.MessageType = ConstructClientType(descriptor, descriptor.MessageType);
            clientDescriptor.MessageType.ParameterizedUrl = descriptor.ParameterizedUrl;
            clientDescriptor.MessageType.Method = descriptor.Method.ToString();
            clientDescriptor.MessageType.Url = ConstructUrl(descriptor.UrlSegments);
            clientDescriptor.MessageType.RoutePath = descriptor.UrlSegments.ToRoutePath();
            if (descriptor.UseQuery)
            {
                clientDescriptor.MessageType.QueryStringFields = ConstructFieldsExceptParameters(descriptor.MessageType, descriptor.UrlParameters.Keys);
            }
            if (descriptor.UseBody)
            {
                clientDescriptor.MessageType.BodyFields = ConstructFieldsExceptParameters(descriptor.MessageType, descriptor.UrlParameters.Keys);
            }
            clientDescriptor.MessageType.Template = _options.GetMessageTemplate(clientDescriptor.MessageType.ClrType);

            clientDescriptor.ResponseType = ConstructClientType(descriptor, descriptor.ResponseType);
            if (clientDescriptor.ResponseType != null)
            {
                clientDescriptor.ResponseType.ParameterizedUrl = descriptor.ParameterizedUrl;
                clientDescriptor.ResponseType.RoutePath = descriptor.UrlSegments.ToRoutePath();
                clientDescriptor.ResponseType.Template = _options.GetResponseTemplate(clientDescriptor.ResponseType.ClrType);
            }

            return clientDescriptor;
        }

        protected virtual string ConstructUrl(UrlSegment[] segments)
        {
            var textSegments = segments.Select(s => ConstructParameterSegment(s));
            return "/" + string.Join("/", textSegments);
        }

        protected virtual string ConstructParameterSegment(UrlSegment segment)
        {
            if (segment.IsParameter)
            {
                var clientFieldName = GetClientFieldName(segment.Value);
                var clientFieldType = GetClientFieldType(segment.Type);
                return _options.RenderPathParameter(clientFieldType, clientFieldName);
            }
            else
            {
                return segment.Value;
            }
        }

        protected virtual Field[] ConstructFieldsExceptParameters(Type type, IEnumerable<string> parameterNames)
        {
            var memberInfos = type.GetPropertiesAndFields();
            var fields = new List<Field>();
            foreach (var memberInfo in memberInfos)
            {
                if (!parameterNames.Contains(memberInfo.Name))
                {
                    var field = ConstructField(memberInfo);
                    fields.Add(field);
                }
            }
            return fields.ToArray();
        }

        protected virtual ClientType ConstructClientType(RouteDescriptor descriptor, Type type, bool includeInnerTypes = true)
        {
            var (unboxedType, isArray) = UnboxAndGetIsArray(type);
            if (unboxedType == null)
            {
                return null;
            }
            var clientType = new ClientType();
            clientType.ClrType = unboxedType;
            clientType.IsEnum = unboxedType.IsEnum;
            clientType.Name = GetTypeName(unboxedType);
            clientType.Fields = ConstructFields(unboxedType);
            clientType.Values = ConstructValues(unboxedType);
            clientType.IsArray = isArray;
            if (includeInnerTypes)
            {
                clientType.InnerTypes = unboxedType.ExtractSubTypesRecursively(_options.FinalTypes)
                    .Select(x => ConstructClientType(descriptor, x, false))
                    .Where(x => x != null)
                    .ToArray();
                clientType.InnerTypes = clientType.InnerTypes.Length > 0 ? clientType.InnerTypes : null;
            }
            clientType.Imports = ConstructImports(descriptor, type);
            return clientType;
        }

        protected virtual string GetTypeName(Type type)
        {
            var clientType = type.Name;
            if (_options.ClientTypeCorrespondences.ContainsKey(type))
            {
                clientType = _options.ClientTypeCorrespondences.GetValueOrDefault(type) ?? clientType;
            }
            else
            {
                if (type.IsGenericType)
                {
                    clientType = type.Name.Substring(0, type.Name.IndexOf("`"));
                    var genericParamaters = type
                        .GetGenericArguments()
                        .Select(x => GetClientFieldType(x))
                        .ToArray();
                    clientType += $"<{string.Join(", ", genericParamaters)}>";
                }
            }
            return _options.TransformClientType(clientType);
        }

        protected virtual Field[] ConstructFields(Type type)
        {
            if (type.IsEnum)
            {
                return new Field[0];
            }
            return type
                .GetPropertiesAndFields()
                .Select(x => ConstructField(x))
                .ToArray();
        }

        protected virtual Field ConstructField(MemberInfo memberInfo)
        {
            var memberType = memberInfo.GetMemberType();
            var isNullable = memberType.IsAssignableToGeneric(typeof(Nullable<>));
            var isArray = memberType.IsArray;
            return new Field
            {
                Name = GetClientFieldName(memberInfo.Name),
                Type = GetClientFieldType(memberType),
                IsNullable = isNullable,
                IsArray = isArray,
            };
        }

        protected virtual string GetClientFieldName(string memberName)
        {
            return _options.TransformFieldName(memberName);
        }

        protected virtual string GetClientFieldType(Type memberType)
        {
            var unboxedType = memberType.Unbox();
            var fieldType = _options.FieldTypeCorrespondences.GetValueOrDefault(unboxedType) ?? unboxedType.Name;
            return _options.TransformFieldType(fieldType);
        }

        protected virtual EnumValue[] ConstructValues(Type type)
        {
            if (!type.IsEnum)
            {
                return new EnumValue[0];
            }
            return Enum.GetValues(type)
                .Cast<object>()
                .Select(x => new EnumValue(Enum.GetName(type, x), (int)x))
                .ToArray();
        }

        protected virtual object[] ConstructImports(RouteDescriptor descriptor, Type type)
        {
            var imports = new List<object>();
            foreach (var conditionnalImport in _options.ConditionalImports)
            {
                if (conditionnalImport.Condition(descriptor, type))
                {
                    var import = conditionnalImport.CreateImport(descriptor, type);
                    imports.Add(import);
                }
            }
            return imports.ToArray();
        }
    }
}