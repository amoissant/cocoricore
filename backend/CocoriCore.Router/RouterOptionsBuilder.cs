using System;

namespace CocoriCore.Router
{
    public class RouterOptionsBuilder
    {
        public RouterOptions Options { get; }

        public RouterOptionsBuilder()
        {
            Options = new RouterOptions();
        }

        public RouterOptionsBuilder SetPatternForType(string pattern, Type type)
        {
            Options.ParametersPattern[type] = pattern;
            return this;
        }

        public RouteBuilder<TMessage> Get<TMessage>()
                    where TMessage : class, new()
        {
            var route = new Route(Options);
            Options.AddRoute(route);
            return new RouteBuilder<TMessage>(route).Get();
        }

        public RouteBuilder<TMessage> Post<TMessage>()
            where TMessage : class, new()
        {
            var route = new Route(Options);
            Options.AddRoute(route);
            return new RouteBuilder<TMessage>(route).Post();
        }

        public RouteBuilder<TMessage> Put<TMessage>()
            where TMessage : class, new()
        {
            var route = new Route(Options);
            Options.AddRoute(route);
            return new RouteBuilder<TMessage>(route).Put();
        }

        public RouteBuilder<TMessage> Patch<TMessage>()
            where TMessage : class, new()
        {
            var route = new Route(Options);
            Options.AddRoute(route);
            return new RouteBuilder<TMessage>(route).Patch();
        }

        public RouteBuilder<TMessage> Delete<TMessage>()
            where TMessage : class, new()
        {
            var route = new Route(Options);
            Options.AddRoute(route);
            return new RouteBuilder<TMessage>(route).Delete();
        }
    }
}