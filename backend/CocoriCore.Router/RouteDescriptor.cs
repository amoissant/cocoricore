using System;
using System.Collections.Generic;
using System.Net.Http;

namespace CocoriCore.Router
{
    public class RouteDescriptor
    {
        public UrlSegment[] UrlSegments;
        public string ParameterizedUrl;
        public HttpMethod Method;
        public Type MessageType;
        public Dictionary<string, Type> UrlParameters;
        public bool UseBody;
        public bool UseQuery;
        //TODO ajouter useCookie et générer empreinte avec cookie
        public string Description;
        public Type ResponseType;
    }
}