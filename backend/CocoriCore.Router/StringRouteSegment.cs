using System;

namespace CocoriCore.Router
{
    public class StringRouteSegment : IRouteSegment
    {
        public string Segment { get; }
        public string Value => Segment;

        public StringRouteSegment(string segment)
        {
            Segment = segment;
        }

        public bool Match(string value)
        {
            return string.Equals(Segment, value, StringComparison.InvariantCultureIgnoreCase);
        }

        public override string ToString()
        {
            return Value;
        }

        public override int GetHashCode()
        {
            return Segment.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return obj is StringRouteSegment stringSegment && stringSegment.Value == Value;
        }
    }
}