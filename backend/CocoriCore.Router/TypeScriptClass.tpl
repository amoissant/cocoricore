{{#Imports}}
import { {{Name}} } from "{{From}}";
{{/Imports}}
//Generated code, do not modify

{{#IsEnum}}
export enum {{ClassName}} {
    {{#Values}}
        {{Name}} = {{Value}};
    {{/Values}}
}
{{/IsEnum}}
{{^IsEnum}}
export class {{ClassName}} {
    {{#Fields}}
        {{Name}}!: {{Type}}{{#IsArray}}[]{{/IsArray}}{{#IsNullable}} | null{{/IsNullable}};
    {{/Fields}}

    public static Fields = {
        {{#Fields}}
            {{Name}}: { name: "{{Name}}", type: "{{Type}}{{#IsArray}}[]{{/IsArray}}" },
        {{/Fields}}
    }
}
{{/IsEnum}}