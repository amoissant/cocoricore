namespace CocoriCore.Router
{
    public class Import
    {
        public string Name;
        public string From;

        public Import()
        {
        }

        public Import(string name, string from)
        {
            Name = name;
            From = from;
        }

        public override bool Equals(object obj)
        {
            return obj is Import import &&
            @import.Name == Name &&
            @import.From == From;
        }

        public override int GetHashCode()
        {
            return 5 ^
                Name.GetHashCode() ^
                From.GetHashCode();
        }
    }
}