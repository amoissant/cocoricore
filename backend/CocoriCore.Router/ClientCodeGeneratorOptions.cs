using System;
using System.Collections.Generic;
using System.Linq;

namespace CocoriCore.Router
{
    public class ClientCodeGeneratorOptions
    {
        public static Type[] FINAL_TYPES = new[]
        {
            typeof(object),
            typeof(bool),
            typeof(byte),
            typeof(sbyte),
            typeof(char),
            typeof(decimal),
            typeof(double),
            typeof(float),
            typeof(int),
            typeof(uint),
            typeof(long),
            typeof(ulong),
            typeof(short),
            typeof(ushort),
            typeof(string),
            typeof(Guid),
            typeof(DateTime),
            typeof(TimeSpan),
        };

        public string Extension { get; set; }
        public Dictionary<Type, string> ClientTypeCorrespondences { get; }
        public Func<string, string> TransformClientType { get; set; }
        public Dictionary<Type, string> FieldTypeCorrespondences { get; }
        public Func<string, string> TransformFieldType { get; set; }
        public Func<string, string> TransformFieldName { get; set; }
        public Func<RouteDescriptor, bool> ExcludeRoute { get; set; }
        public List<ConditionnalImport> ConditionalImports { get; }
        public List<Type> FinalTypes { get; set; }
        public Func<string, string, string> RenderPathParameter { get; set; }
        public Func<Type, string> GetMessageTemplate { get; set; }
        public Func<Type, string> GetResponseTemplate { get; set; }

        public ClientCodeGeneratorOptions()
        {
            Extension = ".ts";
            ClientTypeCorrespondences = new Dictionary<Type, string>();
            TransformClientType = t => t;
            FieldTypeCorrespondences = new Dictionary<Type, string>();
            TransformFieldType = t => t;
            TransformFieldName = f => f;
            ExcludeRoute = d => false;
            ConditionalImports = new List<ConditionnalImport>();
            FinalTypes = FINAL_TYPES.ToList();
            RenderPathParameter = (t, n) => $"{n}:{t}";
            GetMessageTemplate = t => null;
            GetResponseTemplate = t => null;
        }
    }
}