using System;

namespace CocoriCore.Router
{
    public class ConditionnalImport
    {
        public ConditionnalImport(Func<RouteDescriptor, Type, bool> condition, Func<RouteDescriptor, Type, object> createImport)
        {
            Condition = condition;
            CreateImport = createImport;
        }

        public Func<RouteDescriptor, Type, bool> Condition { get; }
        public Func<RouteDescriptor, Type, object> CreateImport { get; }
    }
}