﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using CocoriCore.Collection.Extensions;
using CocoriCore.Common;
using CocoriCore.Reflection.Extension;
using CocoriCore.Types.Extension;
using Soltys.ChangeCase;

namespace CocoriCore.Router
{
    public class FootprintGenerator : IFootprintGenerator
    {
        public static Dictionary<Type, string> DEFAULT_FINAL_TYPES = new Dictionary<Type, string>
            {
                { typeof(bool), null },
                { typeof(byte), null },
                { typeof(sbyte), null },
                { typeof(char), null },
                { typeof(decimal), null },
                { typeof(double), null },
                { typeof(float), null },
                { typeof(int), null },
                { typeof(uint), null },
                { typeof(long), null },
                { typeof(ulong), null },
                { typeof(short), null },
                { typeof(ushort), null },
                { typeof(Guid), null },
                { typeof(DateTime), null },
                { typeof(TimeSpan), null },
                { typeof(string), null },
                { typeof(object), null },
            };
        private Dictionary<Type, string> _finalTypes;
        private ApiFootprint _footprint;
        private IResponseTypeProvider _responseTypeProvider;

        //TODO Option/OptionBuilder
        public FootprintGenerator(IResponseTypeProvider responseTypeProvider, Dictionary<Type, string> finalTypes = null)
        {
            _finalTypes = finalTypes ?? DEFAULT_FINAL_TYPES;
            _responseTypeProvider = responseTypeProvider;
        }

        public ApiFootprint Generate(IEnumerable<RouteDescriptor> descriptors, int maxDeep = 1)
        {
            _footprint = new ApiFootprint();
            _footprint.Enums = new Dictionary<string, Dictionary<string, int>>();
            _footprint.Types = new Dictionary<string, object>();
            _footprint.Routes = descriptors
                .Select(x => ConstructRouteFootprint(x, maxDeep))
                .OrderBy(x => x.Url, StringComparer.InvariantCulture)
                .ThenBy(x => x.Method)
                .ToList();
            return _footprint;
        }

        private RouteFootprint ConstructRouteFootprint(RouteDescriptor descriptor, int maxDeep)
        {
            try
            {
                var routeFootprint = new RouteFootprint();
                routeFootprint.Url = ConstructUrl(descriptor);
                routeFootprint.Method = descriptor.Method.ToString();
                routeFootprint.QueryString = ConstructQueryString(descriptor);
                routeFootprint.Body = ConstructBody(descriptor, maxDeep);
                routeFootprint.Response = ConstructResponse(descriptor, maxDeep);
                routeFootprint.QueryString = RemoveUrlParameters(descriptor, routeFootprint.QueryString);
                routeFootprint.Body = RemoveUrlParameters(descriptor, routeFootprint.Body);
                routeFootprint.Description = descriptor.Description;
                return routeFootprint;
            }
            catch (Exception e)
            {
                throw new Exception($"Error during footprint generation for route : {descriptor.Method} {descriptor.ParameterizedUrl}", e);
            }
        }

        private string ConstructUrl(RouteDescriptor descriptor)
        {
            foreach (var parameterType in descriptor.UrlParameters.Values)
            {
                AddTypeReference(parameterType);
            }
            return descriptor.ParameterizedUrl;
        }

        private object ConstructQueryString(RouteDescriptor descriptor)
        {
            if (descriptor.UseQuery)
            {
                return ConstructDescription(descriptor.MessageType, 0, 1);
            }
            return null;
        }

        private object ConstructBody(RouteDescriptor descriptor, int maxDeep)
        {
            if (descriptor.UseBody)
            {
                return ConstructDescription(descriptor.MessageType, 0, maxDeep);
            }
            return null;
        }

        private object ConstructResponse(RouteDescriptor descriptor, int maxDeep)
        {
            var responseType = _responseTypeProvider.GetResponseType(descriptor.MessageType);
            if (responseType == null)
            {
                return null;
            }
            return ConstructDescription(responseType, 0, maxDeep);
        }

        private object ConstructDescription(Type type, int currentDeep, int maxDeep)
        {
            if (IsFinal(type) || currentDeep == maxDeep)
            {
                AddTypeReference(type);
                return GetStringRepresentation(type);
            }
            if (type.IsAssignableToGeneric(typeof(IEnumerable<>)))
            {
                var elementType = type.GetGenericArguments(typeof(IEnumerable<>))[0];
                if (IsFinal(elementType) || currentDeep == maxDeep)
                {
                    AddTypeReference(elementType);
                    return GetStringRepresentation(type);
                }
                else
                {
                    return new object[] { GetPropertiesAndFields(elementType, currentDeep++, maxDeep) };
                }
            }
            return GetPropertiesAndFields(type, currentDeep++, maxDeep);
        }

        private Dictionary<string, object> GetPropertiesAndFields(Type type, int currentDeep, int maxDeep)
        {
            var propertiesAndFields = type.GetPropertiesAndFields();
            CheckNoDuplicatePropertiesAndField(type, propertiesAndFields);
            return propertiesAndFields
                .ToDictionary(x => x.Name, x => ConstructDescription(x.GetMemberType(), currentDeep + 1, maxDeep));//TODO tester que si on fait currentDeep++ ca bug
        }

        private void CheckNoDuplicatePropertiesAndField(Type type, IEnumerable<MemberInfo> propertiesAndFields)
        {
            if (!propertiesAndFields.IsUnique(x => x.Name))
            {
                throw new InvalidOperationException($"Duplicate field or property name for type {type} : \n"
                    + $"{string.Join("\n", propertiesAndFields.Select(x => x.Name))}");
            }
        }

        private void AddTypeReference(Type type)
        {
            if (type.IsEnum)
            {
                if (!_footprint.Enums.ContainsKey(type.Name))
                {
                    _footprint.Enums[type.Name] = ConstructEnumDescription(type);
                }
            }
            else
            {
                if (!_footprint.Types.ContainsKey(type.Name))
                {
                    if (!IsFinal(type) && CocoriCore.Types.Extension.TypesExtension.IsAssignableToGeneric(type, typeof(IEnumerable<>)))
                    {
                        var elementType = CocoriCore.Types.Extension.TypesExtension.GetGenericArguments(type, typeof(IEnumerable<>))[0];
                        AddTypeReference(elementType);
                    }
                    else if (!IsFinal(type))
                    {
                        var propertiesAndFields = CocoriCore.Types.Extension.TypesExtension.GetPropertiesAndFields(type);
                        CheckNoDuplicatePropertiesAndField(type, propertiesAndFields);
                        _footprint.Types[type.Name] = null;//reserve key to prevent infinite recursion
                        _footprint.Types[type.Name] = ConstructDescription(type, 0, 1);
                    }
                    else if (_finalTypes.ContainsKey(type) && _finalTypes[type] != null)
                    {
                        _footprint.Types[type.Name] = _finalTypes[type];
                    }
                }
            }
        }

        private Dictionary<string, int> ConstructEnumDescription(Type type)
        {
            return Enum.GetValues(type).Cast<object>().ToDictionary(x => Enum.GetName(type, x), x => (int)x);
        }

        private string GetStringRepresentation(Type type)
        {
            if (type.IsAssignableToGeneric(typeof(Nullable<>)))
            {
                var innerType = type.GetGenericArguments(typeof(Nullable<>))[0];
                return $"{innerType.Name.CamelCase()}?";
            }
            if (!IsFinal(type) && type.IsAssignableToGeneric(typeof(IEnumerable<>)))
            {
                type = type.GetGenericArguments(typeof(IEnumerable<>))[0];
                return $"{type.Name.CamelCase()}[]";
            }
            return $"{type.Name.CamelCase()}";
        }

        private bool IsFinal(Type type)
        {
            if (type.IsAssignableToGeneric(typeof(Nullable<>)))
            {
                type = type.GetGenericArguments(typeof(Nullable<>))[0];
            }
            return (type.IsPrimitive || type.IsEnum || _finalTypes.ContainsKey(type));
        }

        private object RemoveUrlParameters(RouteDescriptor descriptor, object routeFootprint)
        {
            if (routeFootprint is Dictionary<string, object> propertiesAndFields)
            {
                foreach (var urlParameter in descriptor.UrlParameters)
                {
                    if (propertiesAndFields.ContainsKey(urlParameter.Key))
                    {
                        propertiesAndFields.Remove(urlParameter.Key);
                    }
                }
                if (propertiesAndFields.Count == 0)
                {
                    if (descriptor.UseQuery)
                    {
                        throw new ConfigurationException($"Query string is empty for route {descriptor.Method} {descriptor.ParameterizedUrl}, remove call to .UseQuery().");
                    }
                    else
                    {
                        propertiesAndFields = null;
                    }
                }
                return propertiesAndFields;
            }
            return routeFootprint;
        }
    }
}