namespace CocoriCore.Router
{
    public interface IRouteSegment
    {
        bool Match(string value);

        string Value { get; }
    }
}