using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace CocoriCore.Router
{
    public interface IRouter
    {
        Type TryFindMessageType(HttpRequest request);
        Task<TMessage> DeserializeMessageAsync<TMessage>(HttpRequest request);
        IEnumerable<RouteDescriptor> GetRouteDescriptors();
    }
}