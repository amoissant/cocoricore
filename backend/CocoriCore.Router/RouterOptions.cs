using System;
using System.Collections.Generic;

namespace CocoriCore.Router
{
    public class RouterOptions
    {
        private List<Route> _routes;

        public RouterOptions()
        {
            //TODO a tester
            ParametersPattern = new Dictionary<Type, string>()
            {
                { typeof(Guid),      "[0-9a-f]{8}-([0-9a-f]{4}-){3}[0-9a-f]{12}" },
                { typeof(Guid?),     "[0-9a-f]{8}-([0-9a-f]{4}-){3}[0-9a-f]{12}" },
                { typeof(string),    "[^/\\?]+" },
                { typeof(decimal),   "(-?)\\d+(\\.\\d+)" },
                { typeof(double),    "(-?)\\d+(\\.\\d+)" },
                { typeof(float),     "(-?)\\d+(\\.\\d+)" },
                { typeof(int),       "(-?)\\d+" },
                { typeof(uint),      "\\d+" },
                { typeof(long),      "(-?)\\d+" },
                { typeof(ulong),     "\\d+" },
                { typeof(short),     "(-?)\\d+" },
                { typeof(ushort),    "\\d+" },
            };
            _routes = new List<Route>();
        }

        public Dictionary<Type, string> ParametersPattern { get; }

        public RouterOptions AddRoute(Route route)
        {
            _routes.Add(route);
            return this;
        }

        public IEnumerable<Route> AllRoutes => _routes;
    }
}