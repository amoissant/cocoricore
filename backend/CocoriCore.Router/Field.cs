namespace CocoriCore.Router
{
    public class Field
    {
        public string Name;
        public string Type;
        public bool IsNullable;
        public bool IsArray;

        public Field()
        {
        }

        public Field(string name, string type, bool isNullable = false, bool isArray = false)
        {
            Name = name;
            Type = type;
            IsNullable = isNullable;
            IsArray = isArray;
        }

        public override bool Equals(object obj)
        {
            return obj is Field field &&
            field.Name == Name &&
            field.Type == Type &&
            field.IsNullable == IsNullable &&
            field.IsArray == IsArray;
        }

        public override int GetHashCode()
        {
            return 5 ^
                Name.GetHashCode() ^
                Type.GetHashCode() ^
                IsNullable.GetHashCode() ^
                IsArray.GetHashCode();
        }
    }
}