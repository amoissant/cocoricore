using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Reflection;
using System.Text.RegularExpressions;
using CocoriCore.Expressions.Extension;
using CocoriCore.Reflection.Extension;
using CocoriCore.Types.Extension;
using Microsoft.AspNetCore.Http;
using Soltys.ChangeCase;

namespace CocoriCore.Router
{
    public class Route
    {
        private RouterOptions _options;
        private Type _messageType;
        private List<IRouteSegment> _routeSegments;
        private List<string> _parameterizedSegments;
        private HttpMethod _method;
        private List<MemberInfo> _urlParameters;
        private Dictionary<string, string> _headers;
        private Regex _routeRegex;
        private bool _useBody;
        private bool _useCookies;
        private bool _useQuery;
        private bool _usePathParameters;
        private string _description;

        public Route(RouterOptions options)
        {
            _options = options;
            _routeSegments = new List<IRouteSegment>();
            _parameterizedSegments = new List<string>();
            _urlParameters = new List<MemberInfo>();
            _headers = new Dictionary<string, string>();
        }

        public Route SetMessageType(Type messageType)
        {
            _messageType = messageType;
            return this;
        }

        public Route SetMethod(HttpMethod method)
        {
            _method = method;
            return this;
        }

        public Route AddPathParameter<T>(Expression<Func<T, object>> expression)
        {
            MemberInfo memberInfo = expression.GetMemberInfo();
            return AddPathParameter(memberInfo);
        }

        public Route AddPathParameter(MemberInfo memberInfo)
        {
            _usePathParameters = true;
            Type memberType = memberInfo.GetMemberType();
            var memberName = memberInfo.Name.CamelCase();
            string parameterPattern = $"(?<{memberName}>{GetParameterPattern(memberType)})";
            _routeSegments.Add(new RegexRouteSegment(parameterPattern, memberType, memberName));
            _parameterizedSegments.Add($"{memberName}:{GetStringRepresentation(memberType)}");
            _urlParameters.Add(memberInfo);
            return this;
        }

        //TODO factoriser code avec FootprintGenerator
        private string GetStringRepresentation(Type type)
        {
            if (type.IsAssignableToGeneric(typeof(Nullable<>)))
            {
                var innerType = type.GetGenericArguments(typeof(Nullable<>))[0];
                return $"{innerType.Name.CamelCase()}?";
            }
            return $"{type.Name.CamelCase()}";
        }

        private string GetParameterPattern(Type parameterType)
        {
            if(parameterType.IsAssignableTo<Enum>())
            {
                return BuildPatternForEnum(parameterType);
            }
            else if (_options.ParametersPattern.TryGetValue(parameterType, out string pattern))
            {
                return pattern;
            }
            else
            {
                throw new NotSupportedException($"No pattern provided for type {parameterType}.\n" 
                    + $"Use {nameof(RouterOptionsBuilder)}.{nameof(RouterOptionsBuilder.SetPatternForType)} to define a url pattern for this type.");
            }
        }

        private string BuildPatternForEnum(Type enumType)
        {
            var allStringValues = Enum.GetNames(enumType).Cast<string>();
            var allIntegerValues = Enum.GetValues(enumType).Cast<int>().Select(x => x.ToString());
            var allEnumValues = allStringValues.Concat(allIntegerValues);
            return string.Join("|", allEnumValues);
        }

        public void SetParameterPattern(Type parameterType, string pattern)
        {
            _options.ParametersPattern[parameterType] = pattern;
        }

        public Route SetDescription(string description)
        {
            _description = description;
            return this;
        }

        public Route AddPath(string path)
        {
            _routeSegments.Add(new StringRouteSegment(path));
            _parameterizedSegments.Add(path);
            return this;
        }

        public Route UseQuery()
        {
            _useQuery = true;
            return this;
        }

        public Route AddHeader<T>(string headerName, Expression<Func<T, object>> expression)
        {
            var memberInfo = expression.GetMemberInfo();
            _headers.Add(headerName, memberInfo.Name);
            return this;
        }

        public Route UseBody()
        {
            _useBody = true;
            return this;
        }

        public Route UseCookies()
        {
            _useCookies = true;
            return this;
        }

        public bool CanHandle(HttpRequest request)
        {
            return GetMatch(request).Success;
        }

        public Match GetMatch(HttpRequest request)
        {
            return GetRouteRegex().Match(request.Path);
        }

        private Regex GetRouteRegex()
        {
            if (_routeRegex == null)
            {
                _routeRegex = new Regex($"^{GetRoutePattern()}$", RegexOptions.IgnoreCase);
            }
            return _routeRegex;
        }

        private string GetRoutePattern()
        {
            var routePattern = $"/{string.Join("/", _routeSegments)}";
            return routePattern;
        }

        public int HashCode => _method.GetHashCode() ^
            RoutePatternWithoutGroupName.GetHashCode() ^
            _useQuery.GetHashCode();

        private string RoutePatternWithoutGroupName => new Regex("(\\?\\<[a-zA-Z]+\\>)").Replace(GetRoutePattern(), "");
        public Dictionary<string, string> Headers => _headers;
        public bool HasBodyParameters => _useBody;
        public bool HasCookieParameters => _useCookies;
        public bool HasQueryParameters => _useQuery;
        public bool HasPathParameters => _usePathParameters;
        public string ParameterizedUrl => $"/{string.Join("/", _parameterizedSegments)}";
        public HttpMethod Method => _method;
        public IEnumerable<IRouteSegment> RouteSegments => _routeSegments;
        public IEnumerable<MemberInfo> UrlParameters => _urlParameters;
        public Type MessageType => _messageType;
        public string Description => _description;

        public bool HasUrlParameter(string parameterName)
        {
            return _urlParameters.Any(x => x.Name.CamelCase() == parameterName);
        }


        public IEnumerable<UrlSegment> GetUrlSegments()
        {
            foreach (var segment in RouteSegments)
            {
                if (segment is StringRouteSegment)
                    yield return new UrlSegment(segment.Value, false);
                if (segment is RegexRouteSegment regexSegment)
                    yield return new UrlSegment(regexSegment.FieldName, true, regexSegment.FieldType);
            }
        }

        public override string ToString()
        {
            return $"{ParameterizedUrl} - {MessageType.FullName}";
        }
    }
}