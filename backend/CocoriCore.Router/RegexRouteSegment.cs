using System;
using System.Text.RegularExpressions;

namespace CocoriCore.Router
{
    public class RegexRouteSegment : IRouteSegment
    {
        public Regex Segment { get; }
        public string Value { get; }
        public string FieldName { get; }
        public Type FieldType { get; }

        public RegexRouteSegment(string value, Type fieldType, string fieldName)
        {
            Segment = new Regex($"^{value}$", RegexOptions.IgnoreCase);
            Value = value;
            FieldName = fieldName;
            FieldType = fieldType;
        }

        public bool Match(string value)
        {
            return Segment.IsMatch(value);
        }

        public override string ToString()
        {
            return Value;
        }

        public override int GetHashCode()
        {
            return Segment.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return obj is RegexRouteSegment regexSegment && regexSegment.Value == Value;
        }
    }
}