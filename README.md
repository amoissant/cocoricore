# CocoriCore #

Source code of CQRS framework based on [.NET core](https://www.microsoft.com/net/core#linuxubuntu) for back-end.

## Getting source code ##

To get the source code clone the repository with git, using the following instructions.

```console
git clone https://bitbucket.org/llafitte007/CocoriCore.git
```

## Editor - Visual studio code ##

List of requires extensions

- ms-vscode.csharp
- rahulsahay.Csharp-ASPNETCore
- jchannon.csharpextensions
- jmrog.vscode-nuget-package-manager
- formulahendry.dotnet-test-explorer
- PeterJausovec.vscode-docker

List installed extensions

```console
code --list-extensions
```
